var Nodes = {
	
	'init': function() {

		if($("#filter_active").length > 0)
		{
			$("#filter_active").select2({
				width: null,
				theme: 'bootstrap',
				placeholder: 'Select Active Status',
				allowClear: true
			});
		}

        if($("#filter_modules").length > 0)
        {
            $("#filter_modules").select2({
                width: null,
                theme: 'bootstrap',
                placeholder: 'Select Modules',
                allowClear: true
            })
        }

		if($("#filter_level").length > 0)
		{
			$("#filter_level").select2({
				width: null,
				theme: 'bootstrap',
				placeholder: 'Select Max Level',
				allowClear: true
			})
		}

		if($("#is_active").length > 0)
		{
			$("#is_active").select2({
				width: null,
				theme: 'bootstrap',
			});
		}

		if($("#access_level_id").length > 0)
		{
			$("#access_level_id").select2({
				width: null,
				theme: 'bootstrap',
			});
		}

		if($("#on_menu").length > 0)
		{
			$("#on_menu").select2({
				width: null,
				theme: 'bootstrap',
			});
		}

		if($("#parent_id").length > 0)
		{
			$("#parent_id").select2({
				width: null,
				theme: 'bootstrap'
			});
		}

		if($("#module_id").length > 0)
		{
			$("#module_id").select2({
				width: null,
				theme: 'bootstrap'
			});
		}

		$(document)
			.on('change', '#filter_active, #filter_description, #filter_title, #filter_function, #filter_modules, #filter_level', function(){
				$('#search_list').submit();
			})
			.on('click', '#select_all', function(e){
				if($(this).is(':checked'))
				{
					$('.item-checkbox').each(function(index, value) {
						if($(this).prop('checked') == false)
						{
							$(this).prop('checked', true);
							$(this).trigger('change');
						}
					})
				}
				else
				{
					$('.item-checkbox').each(function(index, value) {
						if($(this).prop('checked') == true)
						{
							$(this).prop('checked', false);
							$(this).trigger('change');
						}
					})
				}
			});
	}
};

Nodes.init();