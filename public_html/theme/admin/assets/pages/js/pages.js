var Pages = {

	'init': function() {

		//Pages
		$(document).on('change', '#language_id', function() {
			$("#parent_id").val('');
			$("#parent_id").select2({
				width: null,
				theme: 'bootstrap',
				placeholder: 'Select Parent Page',
				ajax: {
					url: "?action=getPages",
					dataType: 'json',
					method: 'GET',
					delay: 250,
					data: function (params) {
						return {
							q: params.term, // search term
							type: 3,
							backend: 0,
							language: $('#language_id').val(),
                            getHomepage: 1
						};
					},
					processResults: function (data, params) {

						var categories = [];

						$.each(data, function(index, element) {
							categories.push(element);
						});

						return {
							results: categories
						};
					},
					cache: false
				}
			});
		});
	}
};

Pages.init();