var Banners = {

    'languages': '',

	'selectChilds': false,

	'getLanguages': function () {
        $.ajax({
            url:"?action=getLanguages",
            method: "GET",
            success:function(data)
            {
            	Banners.languages = data;

                $.each( Banners.languages, function( key, value )
                {
                    $("#tree_"+key).jstree({
                        "core" : {
                            "themes" : {
                                "responsive": true
                            },
                            "check_callback" : true
                        },
                        "state": { "opened" : false },
                        "plugins" : ["checkbox"],
                        "checkbox": { "three_state": Banners.selectChilds }
                    });

                });
            }
        });
    },

	'init': function() {

    	if($("#filter_active").length > 0)
		{
			$("#filter_active").select2({
				width: null,
				theme: 'bootstrap',
				placeholder: 'Select Active Status',
				allowClear: true
			});
		}

		if($("#filter_clients").length > 0)
		{
			$("#filter_clients").select2({
				width: null,
				theme: 'bootstrap',
				placeholder: 'Select Client',
				allowClear: true
			});
		}

		if($("#filter_positions").length > 0)
		{
			$("#filter_positions").select2({
				width: null,
				theme: 'bootstrap',
				placeholder: 'Select Position',
				allowClear: true
			});
		}

		if($("#position_id").length > 0)
		{
			$("#position_id").select2({
				width: null,
				theme: 'bootstrap',
				placeholder: 'Select Position'
			});
		}

		if($("#client_id").length > 0)
		{
			$("#client_id").select2({
				width: null,
				theme: 'bootstrap',
				placeholder: 'Select Client'
			});
		}

		if($("#type").length > 0)
		{
			$("#type").select2({
				width: null,
				theme: 'bootstrap',
				placeholder: 'Select Type'
			});

			// FileManager Modal
			$('.iframe-btn').fancybox({
				type:           'iframe',
				autoDimensions: false,
				'autoSize':     false,
				'width':        '900px',
				'height':       '600px'
			});
		}

        if($(".date-picker").length > 0)
		{
            // Datepickers
            $('.date-picker').datetimepicker({
                format: 'yyyy-mm-dd hh:ii:ss',
                autoclose: true
            });

            Banners.getLanguages();
		}

		if($("#categories").length > 0)
		{
            $("#categories").select2({
                width: null,
                theme: 'bootstrap',
                multiple: true,
                placeholder: 'Select Categories'
            });

            if($('#type').val() == 'articles-feed') {
                $("#categories").parent().removeClass('hide');
            }
		}

        $(document).ready(function() {
            if(window.location.href.indexOf("s=banners_order") > -1 && $('#filter_positions').val() != '') {
                $("#banners-table").tableDnD({
                    onDragClass: 'dragged-row',
					onDrop: function(table, row) {
                    	$('#banners-table').css('opacity', '0.5');

                    	var banners = {};
                        $.each( table.tBodies[0].rows, function( key, value ) {
                            banners[value.id] = key;
                        });

                        $.ajax({
                            url:"?action=updateBannersOrder",
                            method: "GET",
                            data: {'banners': banners},
                            success:function(response) {

                                toastr.options = {
                                    "timeOut": "2000",
                                    "positionClass": "toast-top-full-width"
                                };

                                toastr[response.type](response.message);

                                if(response.success == true) {
                                    $.each( response.banners, function( key, value ) {
                                        $('#order-'+key).html(value)
                                    });
								}

                                $('#banners-table').css('opacity', '1');
                            }
                        });
                    }
                });
            }
        });

        $(document)
			.on('change', '#filter_active, #filter_clients, #filter_positions', function() {
				$('#search_list').submit();
			})
            .on('change', '#select_children', function() {
                if($('#select_children').prop('checked'))
				{
                    $("#tree_"+$(this).attr('data-lang')).jstree('check_all');
				}
                else
				{
                    $("#tree_"+$(this).attr('data-lang')).jstree('uncheck_all');
				}

                Banners.getLanguages();
            })
			.on('change', '#type', function(){
				$.ajax({
					url:"?action=getBannerType",
					method: "GET",
					data: {'id': $('#id').val(), 'type': $('#type').val()},
					success:function(data) {
						$('#field_id').html(data);
					}
				});

                if($('#type').val() == 'articles-feed') {
                    $("#categories").parent().removeClass('hide');
                }
                else {
                    $("#categories").parent().addClass('hide');
                }
			})
            .on('click', '#submit', function() {
            	var selectedElmsIds = [];
                Banners.getLanguages();

                $.each( Banners.languages, function( key, value )
                {
                	var selectedElms = $("#tree_"+key).jstree("get_selected", true);
                    $.each(selectedElms, function() {
                        selectedElmsIds.push(this.id);
                    });
                });

                $('#selected_nodes').val(selectedElmsIds);
            })
			.on('click', '#select_all', function(e){
				if($(this).is(':checked'))
				{
					$('.item-checkbox').each(function(index, value) {
						if($(this).prop('checked') == false)
						{
							$(this).prop('checked', true);
							$(this).trigger('change');
						}
					})
				}
				else
				{
					$('.item-checkbox').each(function(index, value) {
						if($(this).prop('checked') == true)
						{
							$(this).prop('checked', false);
							$(this).trigger('change');
						}
					})
				}
			});

	}
};

Banners.init();