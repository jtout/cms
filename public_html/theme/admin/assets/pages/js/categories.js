var Categories = {

	'init': function() {

		// Select Boxes
		$("#extra_fields_group_id").select2({
			width: null,
			theme: 'bootstrap',
			placeholder: 'Select Extra field group'
		});

		//Categories
		$(document).on('change', '#language_id', function() {
			$("#parent_id").val('');
			$("#parent_id").select2({
				width: null,
				theme: 'bootstrap',
				placeholder: 'Select Category',
				ajax: {
					url: "?action=getCategories",
					dataType: 'json',
					method: 'GET',
					delay: 250,
					data: function (params) {
						return {
							q: params.term, // search term
							type: 2,
							backend: 0,
							language: $('#language_id').val(),
							getHomepage: 1
						};
					},
					processResults: function (data, params) {

						var categories = [];

						$.each(data, function(index, element) {
							categories.push(element);
						});

						return {
							results: categories
						};
					},
					cache: false
				}
			});
		});
	}
};

Categories.init();