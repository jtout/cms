var Roles = {
	
	'init': function()
	{
        if ($("#filter_access_level").length > 0)
        {
            $("#filter_access_level").select2({
                width: null,
                theme: 'bootstrap',
                placeholder: 'Select Access Level',
                allowClear: true
            });
        }

        if ($("#filter_backend").length > 0)
        {
            $("#filter_backend").select2({
                width: null,
                theme: 'bootstrap',
                placeholder: 'Select Roles',
                allowClear: true
            });
        }

        if ($("#in_backend").length > 0)
        {
            $("#in_backend").select2({
                width: null,
                theme: 'bootstrap',
                placeholder: 'Select Roles',
            });
        }

        if ($("#access_level_id").length > 0)
        {
            $("#access_level_id").select2({
                width: null,
                theme: 'bootstrap',
                placeholder: 'Select Access Level',
            });
        }

        if ($("#parent_id").length > 0)
        {
            $("#parent_id").select2({
                width: null,
                theme: 'bootstrap',
                placeholder: 'Select Parent',
            });
        }

        if ($("#roles_tree").length > 0)
        {
            $('#roles_tree')
                .jstree({
                    "plugins" : [ "checkbox" ]
                });
        }

        $(document)
            .on('change', '#filter_access_level, #filter_backend', function(){
                $('#search_list').submit();
            })
            .on('click', '#select_all', function(e){
                if ($(this).is(':checked'))
                {
                    $('.item-checkbox').each(function(index, value) {
                        if ($(this).prop('checked') == false)
                        {
                            $(this).prop('checked', true);
                            $(this).trigger('change');
                        }
                    })
                }
                else
                {
                    $('.item-checkbox').each(function(index, value) {
                        if ($(this).prop('checked') == true)
                        {
                            $(this).prop('checked', false);
                            $(this).trigger('change');
                        }
                    })
                }
            })
            .on('click', '#submit', function() {

                var selectedElmsIds = [];
                var selectedElms = $('#roles_tree').jstree("get_selected", true);
                $.each(selectedElms, function() {
                    selectedElmsIds.push(this.data.id);
                });

                $('#selected_permissions').val(selectedElmsIds);
            });
	}
};

Roles.init();