var Users = {
	
	'init': function() {

		
		$("#filter_role").select2({
			allowClear: true,
			width: null,
			theme: 'bootstrap',
			placeholder: 'Select a role'
		});

		$("#filter_active").select2({
			allowClear: true,
			width: null,
			theme: 'bootstrap',
			placeholder: 'Select active status'
		});

		$("#filter_activated").select2({
			allowClear: true,
			width: null,
			theme: 'bootstrap',
			placeholder: 'Select activated status'
		});

		$("#filter_backend").select2({
			allowClear: true,
			width: null,
			theme: 'bootstrap',
			placeholder: 'Select backend status'
		});

		if ($("#roles_tree").length > 0)
		{
			$('#roles_tree')
				.jstree({
					"plugins" : [ "checkbox" ]
				});

			$('#roles_tree').jstree('open_all');
		}

		if ($("#role_id").length > 0)
		{
			$("#role_id").select2({
				width: null,
				theme: 'bootstrap',
				placeholder: 'Select role'
			});
		}

		if ($("#gender").length > 0)
		{
			$("#gender").select2({
				width: null,
				theme: 'bootstrap',
				placeholder: 'Select Gender'
			});
		}

        if ($("#birthdate").length > 0)
        {
            $("#birthdate").datetimepicker({
                format: "dd-mm-yyyy",
                autoclose: true,
                pickTime: false,
                minView: 2,
                maxView: 4,
				clearBtn: true
            });
        }

        $(document)
            .on('click', '#select_all', function(e){
                if ($(this).is(':checked'))
                {
                    $('.item-checkbox').each(function(index, value) {
                        if ($(this).prop('checked') == false)
                        {
                            $(this).prop('checked', true);
                            $(this).trigger('change');
                        }
                    })
                }
                else
                {
                    $('.item-checkbox').each(function(index, value) {
                        if ($(this).prop('checked') == true)
                        {
                            $(this).prop('checked', false);
                            $(this).trigger('change');
                        }
                    })
                }
            })
			.on('change', '#filter_role, #filter_active, #filter_activated, #filter_backend', function(){
            	$('#search_list').submit();
        	})
			.on('click', '#submit', function() {

				var selectedElmsIds = [];
				var selectedElms = $('#roles_tree').jstree("get_selected", true);
				$.each(selectedElms, function() {
					selectedElmsIds.push(this.data.id);
				});

				$('#selected_permissions').val(selectedElmsIds);
			})
			.on('change', '#role_id', function(){
				var role_id = $(this).val();

				$.ajax({
					url:"?action=getRoles",
					method: "GET",
					data: {'role_id': role_id},
					success:function(data) {
						if (data.success === false)
						{
							console.log(data);

							if ($('#error_msg').length)
								$('#error_msg').remove();

							$('#tab2').prepend(data.message);
						}
						else
						{
							if ($('#error_msg').length)
								$('#error_msg').remove();

							$('#roles_tree').jstree('deselect_all');

							$.each(data, function(key, value){
								$('#roles_tree').jstree('select_node', value);
							});
						}
					}
				});
			});
	}
};

Users.init();