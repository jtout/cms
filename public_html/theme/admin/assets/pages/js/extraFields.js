var ExtraFields = {

	'init': function() {

		// Select Boxes
		$("#filter_group").select2({
			width: null,
			theme: 'bootstrap',
			placeholder: 'Select Group',
			allowClear: true
		});

        $("#type").select2({
            width: null,
            theme: 'bootstrap',
            placeholder: 'Select Type'
        });

        $("#group_id").select2({
            width: null,
            theme: 'bootstrap',
            placeholder: 'Select Group'
        });

		$(document).ready(function() {
			if (window.location.href.indexOf("s=field_order") > -1 && $('#filter_group').val() != '') {
				$("#extra-fields-table").tableDnD({
					onDragClass: 'dragged-row',
					onDrop: function(table, row) {
						$('#extra-fields-table').css('opacity', '0.5');

						var extraFields = {};
						$.each( table.tBodies[0].rows, function( key, value ) {
							extraFields[value.id] = key;
						});

						$.ajax({
							url:"?action=updateExtraFieldsOrder",
							method: "GET",
							data: {'extraFields': extraFields},
							success:function(response) {

								toastr.options = {
									"timeOut": "2000",
									"positionClass": "toast-top-full-width"
								};

								toastr[response.type](response.message);

								if (response.success == true) {
									$.each( response.extraFields, function( key, value ) {
										$('#order-'+key).html(value)
									});
								}

								$('#extra-fields-table').css('opacity', '1');
							}
						});
					}
				});
			}
		});

		$(document)
			.on('change', '#filter_group', function(){
				$('#search_list').submit();
			})
			.on('click', '#select_all', function(e){
				if ($(this).is(':checked'))
				{
					$('.item-checkbox').each(function(index, value) {
						if ($(this).prop('checked') == false)
						{
							$(this).prop('checked', true);
							$(this).trigger('change');
						}
					})
				}
				else
				{
					$('.item-checkbox').each(function(index, value) {
						if ($(this).prop('checked') == true)
						{
							$(this).prop('checked', false);
							$(this).trigger('change');
						}
					})
				}
			})
            .on('change', '#type', function(){
                $('#info').addClass('hide').empty();

            	$.ajax({
                    url:"?action=renderExtraFieldType",
                    method: "GET",
                    data: {'type': $(this).val(), 'id': $('#id').val(), 'group_id': $('#group_id').val(), 'view': $('#view').val()},
					context: this,
                    success:function(data)
					{
						if (data.success === true)
						{
                            if (data.message != '')
							{
								$('#info').removeClass('hide').append(data.message);
								$('.repeater').repeater({
									initEmpty: true,
									show: function () {
										$(this).slideDown();
									},
									hide: function (deleteElement) {
										if (confirm('Are you sure you want to delete this element?')) {
											$(this).slideUp(deleteElement);
										}
									}
								})
							}
						}
						else
						{
							$('#error_msg').html(data.message);
							$('#type').val('0').trigger('change.select2');
						}
                    }
                });
            });
	}
};

ExtraFields.init();
