var Modules = {
	
	'init': function() {

		if($("#filter_active").length > 0)
		{
			$("#filter_active").select2({
				width: null,
				theme: 'bootstrap',
				placeholder: 'Select Active Status',
				allowClear: true
			});
		}

		if($("#filter_function").length > 0)
		{
			$("#filter_function").select2({
				width: null,
				theme: 'bootstrap',
				placeholder: 'Select Function Status',
				allowClear: true
			});
		}

        if($("#filter_modules").length > 0)
        {
            $("#filter_modules").select2({
                width: null,
                theme: 'bootstrap',
                placeholder: 'Select Modules',
                allowClear: true
            })
        }

		if($("#filter_level").length > 0)
		{
			$("#filter_level").select2({
				width: null,
				theme: 'bootstrap',
				placeholder: 'Select Max Level',
				allowClear: true
			})
		}

		if($("#is_active").length > 0)
		{
			$("#is_active").select2({
				width: null,
				theme: 'bootstrap',
				placeholder: 'Select Status',
				allowClear: true
			});
		}

		if($("#is_function").length > 0)
		{
			$("#is_function").select2({
				width: null,
				theme: 'bootstrap',
				placeholder: 'Select Status',
				allowClear: true
			});
		}

		if($("#parent_id").length > 0)
		{
			$("#parent_id").select2({
				width: null,
				theme: 'bootstrap'
			});
		}

		if($("#class_directory").length > 0)
		{
			$("#class_directory").select2({
				width: null,
				theme: 'bootstrap'
			});
		}


		$(document)
			.on('change', '#filter_active, #filter_description, #filter_title, #filter_function, #filter_modules, #filter_level', function(){
				$('#search_list').submit();
			})
			.on('click', '#select_all', function(e){
				if($(this).is(':checked'))
				{
					$('.item-checkbox').each(function(index, value) {
						if($(this).prop('checked') == false)
						{
							$(this).prop('checked', true);
							$(this).trigger('change');
						}
					})
				}
				else
				{
					$('.item-checkbox').each(function(index, value) {
						if($(this).prop('checked') == true)
						{
							$(this).prop('checked', false);
							$(this).trigger('change');
						}
					})
				}
			});
	}
};

Modules.init();