var Contacts = {
	
	'init': function() {

		if ($("#filter_active").length > 0)
		{
			$("#filter_active").select2({
				width: null,
				theme: 'bootstrap',
				placeholder: 'Select Active Status',
				allowClear: true
			});
		}

		if($("#filter_clients").length > 0)
		{
			$("#filter_clients").select2({
				width: null,
				theme: 'bootstrap',
				placeholder: 'Select Client',
				allowClear: true
			});
		}

		$(document)
			.on('change', '#filter_active, #filter_clients', function(){
				$('#search_list').submit();
			})
			.on('click', '#select_all', function(e){
				if($(this).is(':checked'))
				{
					$('.item-checkbox').each(function(index, value) {
						if($(this).prop('checked') == false)
						{
							$(this).prop('checked', true);
							$(this).trigger('change');
						}
					})
				}
				else
				{
					$('.item-checkbox').each(function(index, value) {
						if($(this).prop('checked') == true)
						{
							$(this).prop('checked', false);
							$(this).trigger('change');
						}
					})
				}
			});
	}
};

Contacts.init();