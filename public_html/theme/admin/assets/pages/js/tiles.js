var Tiles = {
	
	'ordering': [],
	
	'languages': '',
	
	'init': function() {

        $(document)
            .on('change', '#filter_on_tiles', function(){
                $('#tiles_search_form').submit();
            });

        $("#filter_on_tiles").select2({
            width: null,
            theme: 'bootstrap',
            placeholder: 'Select On tiles status',
        });

		$("#tree_tiles").jstree({
			"core" : {
				"themes" : {
					"responsive": true
				},
				'check_callback' : function (operation, node, node_parent, node_position, more) {
					if (operation === 'move_node' && node.parent !== node_parent.id) {
						return false;
					}

					return true;
				}
			},
			"state": { "opened" : true },
			"plugins" : ["dnd", "state", "types"]
		}).bind("move_node.jstree", function (e, data) {

			$('.alert.alert-success').remove();

			var inst = $('#tree_tiles').jstree(true);
			parent = inst.get_node(data.parent);

			$.each( parent.children, function( key, value ) {
				Tiles.ordering[key] = value;
			});

			$.ajax({
				url:"?action=updateTilesOrder",
				method: "GET",
				data: {
					'id' : data.node.id,
					'parent': data.parent,
					'old_parent': data.old_parent,
					'ordering': Tiles.ordering
				},
				success:function(response) {

                    toastr.options = {
                        "timeOut": "2000",
                        "positionClass": "toast-top-full-width"
                    };

                    toastr[response.type](response.message);
				}
			});
		});

		$(document).on('click', '#select_all', function(e){
			if ($(this).is(':checked'))
			{
				$('.item-checkbox').each(function(index, value) {
					if ($(this).prop('checked') == false)
					{
						$(this).prop('checked', true);
						$(this).trigger('change');
					}
				})
			}
			else
			{
				$('.item-checkbox').each(function(index, value) {
					if ($(this).prop('checked') == true)
					{
						$(this).prop('checked', false);
						$(this).trigger('change');
					}
				})
			}
		});
	}
};

Tiles.init();
