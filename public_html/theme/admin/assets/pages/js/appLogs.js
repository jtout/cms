var AppLogs = {

    'init': function() {

        $("#filter_type").select2({
            width: null,
            theme: 'bootstrap',
            placeholder: 'Select Log type',
            allowClear: true
        });

        $(document)
            .on('change', '#filter_type', function(){
                $('#search_list').submit();
            });

        // Info Modal
        $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner = '<div class="loading-spinner" style="width: 200px; margin-left: -100px;"><div class="progress progress-striped active"><div class="progress-bar" style="width: 100%;"></div></div></div>', $.fn.modalmanager.defaults.resize = !0, $(".dynamic .demo").click(function() {
            $(a).modal()
        });

        $(".ajax-demo").on("click", function() {
            $("body").modalmanager("loading");
            var element = $(this);
            var id = element.attr("data-id");
            var a = $("#ajax-modal-"+id);

            var data = {
                'csrf_name': $('input[name="csrf_name"]').val(),
                'csrf_value': $('input[name="csrf_value"]').val(),
                'id': id
            };

            a.load(element.attr("data-url"), data, function(response) {
                a.modal();
            })
        });

        $(document)
            .on('click', '#select_all', function(e){
                if($(this).is(':checked'))
                {
                    $('.item-checkbox').each(function(index, value) {
                        if($(this).prop('checked') == false)
                        {
                            $(this).prop('checked', true);
                            $(this).trigger('change');
                        }
                    })
                }
                else
                {
                    $('.item-checkbox').each(function(index, value) {
                        if($(this).prop('checked') == true)
                        {
                            $(this).prop('checked', false);
                            $(this).trigger('change');
                        }
                    })
                }
            });

	}
};

AppLogs.init();