var Comments = {
	
	'init': function() {

		$("#filter_active").select2({
			allowClear: true,
			width: null,
			theme: 'bootstrap',
			placeholder: 'Select active status'
		});

		$(document)
			.on('click', '#select_all', function(e){
				if ($(this).is(':checked'))
				{
					$('.item-checkbox').each(function(index, value) {
						if ($(this).prop('checked') == false)
						{
							$(this).prop('checked', true);
							$(this).trigger('change');
						}
					})
				}
				else
				{
					$('.item-checkbox').each(function(index, value) {
						if ($(this).prop('checked') == true)
						{
							$(this).prop('checked', false);
							$(this).trigger('change');
						}
					})
				}
			})
			.on('change', '#filter_active', function(){
				$('#search_list').submit();
			})
	}
};

Comments.init();