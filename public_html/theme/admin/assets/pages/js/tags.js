var Tags = {

    'tagsInMemory': [],

    'isJson': function(str) {
        try {
            JSON.parse(str);
            return true;
        } catch (err) {
            return false;
        }
    },

    'rememberTags' :function (tags, action) {
        var data = {
            'csrf_name': $('input[name="csrf_name"]').val(),
            'csrf_value': $('input[name="csrf_value"]').val(),
            'tags': tags,
            'action': action
        };

        $.ajax({
            url:"?action=showTagsList",
            method: "POST",
            data: data,
            success:function(data)
            {}
        });
    },

    'init': function() {
        // Tags Modal
        $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner = '<div class="loading-spinner" style="width: 200px; margin-left: -100px;"><div class="progress progress-striped active"><div class="progress-bar" style="width: 100%;"></div></div></div>', $.fn.modalmanager.defaults.resize = !0, $(".dynamic .demo").click(function() {
            $(a).modal()
        });

        var a = $("#ajax-modal");

        $("#ajax-demo").on("click", function() {
            toastr.options = {
                "timeOut": "2000",
                "positionClass": "toast-top-full-width"
            };

            $("body").modalmanager("loading");
            var d = $(this);

            var data = {
                'csrf_name': $('input[name="csrf_name"]').val(),
                'csrf_value': $('input[name="csrf_value"]').val(),
            };

            a.load(d.attr("data-url"), data, function(response) {
                if (Tags.isJson(response)) {
                    var decodedResponse = $.parseJSON(response);

                    toastr[decodedResponse.type](decodedResponse.message);
                    $(".modal-scrollable").trigger('click');
                }
                else {
                    a.modal();
                }
            })
        });

        $(document)
            .on('click', '.item-checkbox', function(){
                if ($(this).val() != 'on') {
                    Tags.tagsInMemory.push({id: $(this).val(), title: $(this).attr('data-title') });

                    if ($(this).is(':checked')) {
                        Tags.rememberTags(Tags.tagsInMemory, 1);
                    }
                    else {
                        Tags.rememberTags(Tags.tagsInMemory, 0);
                    }
                }
            })
            .on('click', '#select_all', function(e){
                if ($(this).is(':checked'))
                {
                    $('.item-checkbox').each(function(index, value) {
                        Tags.tagsInMemory.push({id: $(this).val(), title: $(this).attr('data-title') });

                        if ($(this).prop('checked') == false) {
                            $(this).prop('checked', true);
                            $(this).trigger('change');
                        }
                    });

                    Tags.rememberTags(Tags.tagsInMemory, 1);
                }
                else
                {
                    $('.item-checkbox').each(function(index, value) {
                        Tags.tagsInMemory.push({id: $(this).val(), title: $(this).attr('data-title') });

                        if ($(this).prop('checked') == true) {
                            $(this).prop('checked', false);
                            $(this).trigger('change');
                        }
                    });

                    Tags.rememberTags(Tags.tagsInMemory, 0);
                }
            });

        a.on("click", ".merge", function() {
            // Submit Form
            $('#merge-tags-form').submit();
        });
	}
};

Tags.init();