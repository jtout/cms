var Articles = {
	
	'initSelect2' :function() {

		$("#extra_fields_group_id").select2({
			width: null,
			theme: 'bootstrap',
			placeholder: 'Select Extra field group'
		});

		$(".extra_field_select").select2({
			width: null,
			theme: 'bootstrap'
		});

		$(".extra_field_select_multiple").select2({
			width: null,
			theme: 'bootstrap',
			multiple: true,
			tags: true
		});

		$('.extra-field-date-picker').datetimepicker({
			format: 'dd-mm-yyyy',
			minView: 2,
			autoclose: true
		});
	},

	'init': function() {
		Articles.initSelect2();
		$(document)
			.on('change', '#language_id', function() {
				$("#parent_id").val('');
				$("#parent_id").select2({
					width: null,
					theme: 'bootstrap',
					placeholder: 'Select Category',
					ajax: {
						url: "?action=getCategories",
						dataType: 'json',
						method: 'GET',
						delay: 250,
						data: function (params) {
							return {
								q: params.term, // search term
								backend: 0,
								language: $('#language_id').val(),
								type: 2
							};
						},
						processResults: function (data, params) {

							var categories = [];

							$.each(data, function (index, element) {
								categories.push(element);
							});

							return {
								results: categories
							};
						},
						cache: false
					}
				})
			})
			.on('change', '#parent_id', function() {
				$.ajax({
					url:"?action=getExtraFields",
					method: "GET",
					data: {'id': $('#id').val(), 'parent_id': $('#parent_id').val()},
					success:function(data) {
						$('#extra_fields').html(data.message);
						Articles.initSelect2();
					}
				});
			});

		// Tags
		$("#tags").select2({
			width: null,
			theme: 'bootstrap',
			multiple: true,
			tags: true,
			placeholder: 'Select Tags',
			minimumInputLength: 2,
			ajax: {
				url: "?action=searchTags",
				dataType: 'json',
				method: 'GET',
				delay: 250,
				data: function (params) {
				  return {
					  q: params.term, // search term
					  language: $('#language_id').val(),
					  type: 4,
					  backend: 0
				  };
				},
				processResults: function (data, params) {

					var tags = [];

					$.each(data, function(index, element) {
						tags.push(element);
					});

					return {
						results: tags
					};
				},
				cache: false
			}
		})
		.on('select2:unselect', function (e) {
            $("option[value='"+e.params.data.id+"']").remove();
        });
	}
};

Articles.init();