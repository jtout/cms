var Languages = {

	'init': function() {

		// Select Boxes
		$("#filter_active").select2({
			width: null,
			theme: 'bootstrap',
			placeholder: 'Select Publish status',
			allowClear: true
		});


		$(document)
			.on('change', '#filter_active', function(){
				$('#search_list').submit();
			});

		$(document)
			.on('click', '#select_all', function(e){
				if ($(this).is(':checked'))
				{
					$('.item-checkbox').each(function(index, value) {
						if ($(this).prop('checked') == false)
						{
							$(this).prop('checked', true);
							$(this).trigger('change');
						}
					})
				}
				else
				{
					$('.item-checkbox').each(function(index, value) {
						if ($(this).prop('checked') == true)
						{
							$(this).prop('checked', false);
							$(this).trigger('change');
						}
					})
				}
			});

	}
};

Languages.init();