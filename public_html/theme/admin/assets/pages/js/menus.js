var Menus = {
	
	'ordering': [],
	
	'languages': '',
	
	'init': function() {
        const domain = window.location.protocol + "//" + window.location.host + "/panel";

		$.ajax({
			url:domain+"?action=getLanguages",
			method: "GET",
			success:function(data)
			{
				Menus.languages = data;

				$.each( Menus.languages, function( key, value )
				{
					$("#tree_"+key).jstree({
						"core" : {
							"themes" : {
								"responsive": true
							},
							"check_callback" : true
						},
						"state": { "opened" : true },
						"plugins" : ["dnd", "state", "types"]
					}).bind("move_node.jstree", function (e, data) {

						var inst = $('#tree_'+key).jstree(true);
						parent = inst.get_node(data.parent);

						$.each( parent.children, function( key, value ) {
							Menus.ordering[key] = value;
						});

						$('.response-msg').html('');

						$.ajax({
							url:"?action=updateMenuOrder",
							method: "GET",
							data: {
								'id' : data.node.id,
								'parent': data.parent,
								'ordering': Menus.ordering,
								'language': data.node.data.language
							},
							success:function(data) {
                                toastr.options = {
                                    "timeOut": "2000",
                                    "positionClass": "toast-top-full-width"
                                };

                                toastr[data.type](data.message);
							}
						});
					});
				});
			}
		});
		
		$("#access_level").select2({
			width: null,
			theme: 'bootstrap',
			placeholder: 'Select an access level'
		});
		
		$(document).on('click', '.portlet-body .tab-content li a', function(){
			
			var id = $(this).attr('id').replace('_anchor', '');
			
			$.ajax({
				url:domain+"?action=getMenuInfo",
				method: "GET",
				data: {
					'id' : id
				},
				success:function(data)
				{
					$('.quick-edit input').prop('disabled', false);
					$('#on_menu').bootstrapSwitch('disabled', false);
					$('#is_active').bootstrapSwitch('disabled', false);
					$('#update').prop('disabled', false);
					$('#access_level').prop('disabled', false);
					$('#cancel').removeClass('disabled');

					$("#id").val(id);
					$("#old_name").val(data.name);
					$("#title").val(data.title);
					$('#access_level').val(data.access_level_id).trigger('change');

					if (data.on_menu == 1)
					{
						$("#on_menu").bootstrapSwitch('state', true);
						$("#on_menu").val(1);
					}
					else
						{
						$("#on_menu").bootstrapSwitch('state', false);
						$("#on_menu").val(0);
					}

					if (data.is_active == 1)
					{
						$("#is_active").bootstrapSwitch('state', true);
						$("#is_active").val(1);
					}
					else {
						$("#is_active").bootstrapSwitch('state', false);
						$("#is_active").val(0);
					}
				}
			});
		});
		
	}
};

Menus.init();
