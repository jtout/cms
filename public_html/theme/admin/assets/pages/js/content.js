var Content = {

	'init': function() {

		// Select Boxes
		$("#filter_featured").select2({
			width: null,
			theme: 'bootstrap',
			placeholder: 'Select Featured status',
			allowClear: true
		});

		$("#filter_active").select2({
			width: null,
			theme: 'bootstrap',
			placeholder: 'Select Publish status',
			allowClear: true
		});

		$("#filter_language").select2({
			width: null,
			theme: 'bootstrap',
			placeholder: 'Select Language',
			allowClear: true
		});

		$("#filter_parent").select2({
			width: null,
			theme: 'bootstrap',
			placeholder: 'Select Category',
			allowClear: true
		});

		$("#filter_author").select2({
			width: null,
			theme: 'bootstrap',
			placeholder: 'Select Author',
			allowClear: true
		});

		$("#created_by").select2({
			width: null,
			theme: 'bootstrap',
			placeholder: 'Select author'
		});

		$("#access_level_id").select2({
			width: null,
			theme: 'bootstrap',
			placeholder: 'Select access level'
		});

		$("#language_id").select2({
			width: null,
			theme: 'bootstrap',
			placeholder: 'Select language'
		});

		$("#robots").select2({
			width: null,
			theme: 'bootstrap'
		});

		$("#parent_id").select2({
			width: null,
			theme: 'bootstrap',
			placeholder: 'Select Parent'
		});

		$("#view").select2({
			width: null,
			theme: 'bootstrap'
		});

		$("#comments_lock").select2({
			width: null,
			theme: 'bootstrap'
		});

        $("#sponsored_by_id").select2({
            width: null,
            theme: 'bootstrap',
            placeholder: 'Select Client',
			allowClear: true
        });

        $("#source").select2({
            width: 150,
            theme: 'bootstrap'
        });

		$(document)
			.on('change', '#filter_active, #filter_level, #filter_featured, #filter_language, #filter_parent, #filter_author', function(){
				$('#search_list').submit();
			});

		// Remove Image
		$(document).on('click', '.remove-image', function(){
			$('.article-image').attr('src', 'https://placeholdit.imgix.net/~text?txtsize=33&txt=no+image&w=400&h=250');
			$('.select-main-image').text('Select Image');
		});

		// Datepickers
		$('.date-picker').datetimepicker({
			format: 'yyyy-mm-dd hh:ii:ss'
		});

		$(document).on('click', '.minute', function(){
			$('.date-picker').datetimepicker('hide');
		});

		// Image Gallery
        $("a.grouped_elements").fancybox({
            fitToView   : true,
            autoSize    : true,
            closeClick  : false,
            openEffect  : 'none',
            closeEffect : 'none',
            showArrows  : true,
            arrows      : true,
        });

		// FileManager Modal
		$('.iframe-btn').fancybox({
			type:           'iframe',
			autoDimensions: false,
			'autoSize':     false,
			'width':        '900px',
			'height':       '600px'
		});

		$(document)
			.on('click', '#select_all', function(e){
				if($(this).is(':checked'))
				{
					$('.item-checkbox').each(function(index, value) {
						if($(this).prop('checked') == false)
						{
							$(this).prop('checked', true);
							$(this).trigger('change');
						}
					})
				}
				else
				{
					$('.item-checkbox').each(function(index, value) {
						if($(this).prop('checked') == true)
						{
							$(this).prop('checked', false);
							$(this).trigger('change');
						}
					})
				}
			});

	}
};

Content.init();