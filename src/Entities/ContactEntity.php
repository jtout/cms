<?php

namespace App\Entities;

use App\Events\ContactEvents;
use App\Models\Backend\Contacts;

class ContactEntity extends Entity
{
    /**
     * @var string
     */
    protected $model = Contacts::class;

    /**
     * @var string
     */
    protected $entity_events = ContactEvents::class;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $client_id;

    /**
     * @var string
     */
    protected $client_title;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var int
     */
    protected $is_active;

    /**
     * @var int
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $created_on;

    /**
     * @var int
     */
    protected $updated_by;

    /**
     * @var string
     */
    protected $updated_on;


    /* GETTERS */

    /**
     * @return int
     */
    public function id() :int
    {
        if (empty($this->id))
            $this->id = (int) 0;

        return $this->id;
    }

    /**
     * @return int
     */
    public function client_id() :int
    {
        return $this->client_id;
    }

    /**
     * @return string
     */
    public function client_title() :string
    {
        return $this->client_title;
    }

    /**
     * @return string
     */
    public function name() :string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function email() :string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function phone() :string
    {
        return $this->phone;
    }

    /**
     * @return int
     */
    public function is_active() :int
    {
        return $this->is_active;
    }

    /**
     * @return int
     */
    public function created_by() :int
    {
        return $this->created_by;
    }

    /**
     * @return string
     */
    public function created_on() :string
    {
        return $this->created_on;
    }

    /**
     * @return int
     */
    public function updated_by() :int
    {
        return $this->updated_by;
    }

    /**
     * @return string
     */
    public function updated_on() :string
    {
        return $this->updated_on;
    }

    /* SETTERS */

    /**
     * @param $value
     */
    public function setId($value)
    {
        $this->id = $value;
    }

    /**
     * @param $value
     */
    public function setClientId($value)
    {
        $this->client_id = $value;
    }

    /**
     * @param $value
     */
    public function setClientTitle($value)
    {
        $this->client_title = $value;
    }

    /**
     * @param $value
     */
    public function setName($value)
    {
        $this->name = $value;
    }

    /**
     * @param $value
     */
    public function setEmail($value)
    {
        $this->email = $value;
    }

    /**
     * @param $value
     */
    public function setPhone($value)
    {
        $this->phone = $value;
    }

    /**
     * @param $value
     */
    public function setIsActive($value)
    {
        $this->is_active = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedBy($value)
    {
        $this->created_by = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedOn($value)
    {
        $this->created_on = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedBy($value)
    {
        $this->updated_by = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedOn($value)
    {
        $this->updated_on = $value;
    }
}