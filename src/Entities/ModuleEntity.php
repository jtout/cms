<?php

namespace App\Entities;

use App\Events\ModuleEvents;
use App\Models\Backend\Modules;

class ModuleEntity extends Entity
{
    /**
     * @var string
     */
    protected $model = Modules::class;

    /**
     * @var string
     */
    protected $entity_events = ModuleEvents::class;


    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $parent_id;

    /**
     * @var int
     */
    protected $is_function;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var
     */
    protected $class;

    /**
     * @var string
     */
    protected $action;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var int
     */
    protected $is_active;

    /**
     * @var int
     */
    protected $depth;

    /**
     * @var int
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $created_on;

    /**
     * @var int
     */
    protected $updated_by;

    /**
     * @var string
     */
    protected $updated_on;

    /* GETTERS */

    /**
     * @return mixed
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function parent_id()
    {
        return $this->parent_id;
    }

    /**
     * @return mixed
     */
    public function is_function()
    {
        return $this->is_function;
    }

    /**
     * @return mixed
     */
    public function title()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function class()
    {
        return $this->class;
    }

    /**
     * @return mixed
     */
    public function action()
    {
        return $this->action;
    }

    /**
     * @return mixed
     */
    public function description()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function is_active()
    {
        return $this->is_active;
    }

    /**
     * @return mixed
     */
    public function depth()
    {
        return $this->depth;
    }

    /**
     * @return mixed
     */
    public function created_by()
    {
        return $this->created_by;
    }

    /**
     * @return mixed
     */
    public function created_on()
    {
        return $this->created_on;
    }

    /**
     * @return mixed
     */
    public function updated_by()
    {
        return $this->updated_by;
    }

    /**
     * @return mixed
     */
    public function updated_on()
    {
        return $this->updated_on;
    }

    /* SETTERS */

    /**
     * @param $value
     */
    public function setId($value)
    {
        $this->id = $value;
    }

    /**
     * @param $value
     */
    public function setParentId($value)
    {
        $this->parent_id = $value;
    }

    /**
     * @param $value
     */
    public function setIsFunction($value)
    {
        $this->is_function = $value;
    }

    /**
     * @param $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }

    /**
     * @param $value
     */
    public function setClass($value)
    {
        $this->class = $value;
    }

    /**
     * @param $value
     */
    public function setAction($value)
    {
        $this->action = $value;
    }

    /**
     * @param $value
     */
    public function setDescription($value)
    {
        $this->description = $value;
    }

    /**
     * @param $value
     */
    public function setDepth($value)
    {
        $this->depth = $value;
    }

    /**
     * @param $value
     */
    public function setIsActive($value)
    {
        $this->is_active = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedBy($value)
    {
        $this->created_by = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedOn($value)
    {
        $this->created_on = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedBy($value)
    {
        $this->updated_by = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedOn($value)
    {
        $this->updated_on = $value;
    }
}