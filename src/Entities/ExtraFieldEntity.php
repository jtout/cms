<?php

namespace App\Entities;

use App\Events\ExtraFieldEvents;
use App\Models\Backend\ExtraFields;

class ExtraFieldEntity extends Entity
{
    /**
     * @var string
     */
    protected $model = ExtraFields::class;

    /**
     * @var string
     */
    protected $entity_events = ExtraFieldEvents::class;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $group_id;

    /**
     * @var string
     */
    protected $group_title;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $info;

    /**
     * @var int
     */
    protected $is_active;

    /**
     * @var int
     */
    protected $ordering;

    /**
     * @var int
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $created_on;

    /**
     * @var int
     */
    protected $updated_by;

    /**
     * @var string
     */
    protected $updated_on;


    /* GETTERS */

    /**
     * @return int
     */
    public function id() :int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function group_id() :int
    {
        return $this->group_id;
    }

    /**
     * @return string
     */
    public function group_title() :string
    {
        return $this->group_title;
    }

    /**
     * @return string
     */
    public function title() :string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function type() :string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function info() :string
    {
        if ($this->info == null)
            $this->info = '';

        return $this->info;
    }

    /**
     * @return int
     */
    public function is_active() :int
    {
        return $this->is_active;
    }

    /**
     * @return int
     */
    public function ordering() :int
    {
        return $this->ordering;
    }

    /**
     * @return int
     */
    public function created_by() :int
    {
        return $this->created_by;
    }

    /**
     * @return string
     */
    public function created_on() :string
    {
        return $this->created_on;
    }

    /**
     * @return int
     */
    public function updated_by() :int
    {
        return $this->updated_by;
    }

    /**
     * @return string
     */
    public function updated_on() :string
    {
        return $this->updated_on;
    }

    /* SETTERS */

    /**
     * @param $value
     */
    public function setId($value)
    {
        $this->id = $value;
    }

    /**
     * @param $value
     */
    public function setGroupId($value)
    {
        $this->group_id = $value;
    }

    /**
     * @param $value
     */
    public function setGroupTitle($value)
    {
        $this->group_title = $value;
    }

    /**
     * @param $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }

    /**
     * @param $value
     */
    public function setType($value)
    {
        $this->type = $value;
    }

    /**
     * @param $value
     */
    public function setInfo($value)
    {
        $this->info = $value;
    }

    /**
     * @param $value
     */
    public function setIsActive($value)
    {
        $this->is_active = $value;
    }

    /**
     * @param $value
     */
    public function setOrdering($value)
    {
        $this->ordering = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedBy($value)
    {
        $this->created_by = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedOn($value)
    {
        $this->created_on = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedBy($value)
    {
        $this->updated_by = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedOn($value)
    {
        $this->updated_on = $value;
    }
}