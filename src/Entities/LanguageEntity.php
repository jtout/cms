<?php

namespace App\Entities;

use App\Events\LanguageEvents;
use App\Models\Backend\Languages;

class LanguageEntity extends Entity
{
    /**
     * @var string
     */
    protected $model = Languages::class;

    /**
     * @var string
     */
    protected $entity_events = LanguageEvents::class;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $iso_code;

    /**
     * @var int
     */
    protected $is_active;

    /**
     * @var int
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $created_on;

    /**
     * @var int
     */
    protected $updated_by;

    /**
     * @var string
     */
    protected $updated_on;


    /* GETTERS */

    /**
     * @return int
     */
    public function id() :int
    {
        if (is_null($this->id)) {
            $this->id = 0;
        }

        return $this->id;
    }

    /**
     * @return string
     */
    public function title() :string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function iso_code() :string
    {
        return $this->iso_code;
    }

    /**
     * @return int
     */
    public function active() :int
    {
        return $this->is_active;
    }

    /**
     * @return int
     */
    public function created_by() :int
    {
        return $this->created_by;
    }

    /**
     * @return string
     */
    public function created_on() :string
    {
        return $this->created_on;
    }

    /**
     * @return int
     */
    public function updated_by() :int
    {
        return $this->updated_by;
    }

    /**
     * @return string
     */
    public function updated_on() :string
    {
        return $this->updated_on;
    }

    /* SETTERS */

    /**
     * @param $value
     */
    public function setId($value)
    {
        $this->id = $value;
    }

    /**
     * @param $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }

    /**
     * @param $value
     */
    public function setIsoCode($value)
    {
        $this->iso_code = $value;
    }

    /**
     * @param $value
     */
    public function setIsActive($value)
    {
        $this->is_active = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedBy($value)
    {
        $this->created_by = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedOn($value)
    {
        $this->created_on = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedBy($value)
    {
        $this->updated_by = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedOn($value)
    {
        $this->updated_on = $value;
    }
}