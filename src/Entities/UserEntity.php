<?php

namespace App\Entities;

use Carbon\Carbon;
use App\Observers\Joomla;
use App\Events\UserEvents;
use App\Models\Backend\Roles;
use App\Models\Backend\Users;

class UserEntity extends Entity
{
    /**
     * @var string
     */
    protected $model = Users::class;

    /**
     * @var string
     */
    protected $entity_events = UserEvents::class;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $user_name;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $last_name;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var int
     */
    protected $role_id;

    /**
     * @var string
     */
    protected $access_level;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var int
     */
    protected $is_enabled;

    /**
     * @var int
     */
    protected $in_backend;

    /**
     * @var int
     */
    protected $is_activated;

    /**
     * @var string
     */
    protected $activation_token;

    /**
     * @var int
     */
    protected $privacy_policy;

    /**
     * @var int
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $created_on;

    /**
     * @var int
     */
    protected $updated_by;

    /**
     * @var string
     */
    protected $updated_on;

    /**
     * @var string
     */
    protected $activated_on;

    /**
     * @var string
     */
    protected $last_visit_date;

    /**
     * @var string
     */
    protected $avatar;

    /**
     * @var int
     */
    protected $gender;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $birthdate;

    /**
     * @var string
     */
    protected $location;

    /**
     * @var string
     */
    protected $twitch;

    /**
     * @var string
     */
    protected $instagram;

    /**
     * @var string
     */
    protected $twitter;

    /**
     * @var string
     */
    protected $facebook;

    /**
     * @var string
     */
    protected $psn_id;

    /**
     * @var string
     */
    protected $xbox_live_gamertag;

    /**
     * @var string
     */
    protected $nintendo_id;

    /**
     * @var string
     */
    protected $steam_id;

    /**
     * @var string
     */
    protected $pc;

    /**
     * @var string
     */
    protected $tablet;

    /**
     * @var string
     */
    protected $smartphone;

    /**
     * @var string
     */
    protected $signature;

    /**
     * @var int
     */
    protected $like_notifications;


    /* GETTERS */

    /**
     * @return bool
     */
    public function can_access_backend()
    {
        if ($this->in_backend() && $this->role()->in_backend()) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function is_developer()
    {
        if ($this->role_id() == 1) {
            return true;
        }

        return false;
    }

    /**
     * @param string $action
     * @return bool
     */
    public function has_permission_to(string $action) :bool
    {
        if (in_array($action, array_keys($this->permissions()->toArray()))) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function is_guest() :bool
    {
        if ($this->id() == 0) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function user_path() :string
    {
        return env('DOMAIN').'/users?profile='.$this->user_name;
    }

    /**
     * @return RoleEntity
     */
    public function role() :RoleEntity
    {
        return Roles::findById($this->role_id());
    }

    /**
     * @return int
     */
    public function id() :int
    {
        if ($this->id == null)
            $this->id = (int) 0;

        return $this->id;
    }

    /**
     * @return string
     */
    public function username() :string
    {
        $username = empty($this->user_name) ? 'guest' : $this->user_name;

        return (string)$username;
    }

    /**
     * @return string
     */
    public function name() :string
    {
        if ($this->name == null)
            $this->name = '';

        return (string)$this->name;
    }

    /**
     * @return string
     */
    public function last_name() :string
    {
        if ($this->last_name == null)
            $this->last_name = '';

        return $this->last_name;
    }

    /**
     * @return string
     */
    public function fullName() :string
    {
        if (empty($this->name) && empty($this->name)) {
            return $this->username();
        }

        return (string)$this->name.' '.$this->last_name;
    }

    /**
     * @return string|null
     */
    public function password() :?string
    {
        return $this->password;
    }

    /**
     * @return int
     */
    public function role_id() :int
    {
        return $this->role_id;
    }

    /**
     * @return string
     */
    public function access_level() :?string
    {
        return $this->access_level;
    }

    /**
     * @return string
     */
    public function email() :string
    {
        return $this->email;
    }

    /**
     * @return int|null
     */
    public function is_enabled() :?int
    {
        return $this->is_enabled;
    }

    /**
     * @return int
     */
    public function in_backend() :int
    {
        return $this->in_backend;
    }

    /**
     * @return int
     */
    public function is_activated() :int
    {
        return $this->is_activated;
    }

    /**
     * @return string
     */
    public function activation_token() :string
    {
        if ($this->activation_token == null)
            $this->activation_token = '';

        return $this->activation_token;
    }

    /**
     * @return mixed
     */
    public function permissions()
    {
        return $this->relations->permissions;
    }

    /**
     * @return int
     */
    public function privacy_policy() :int
    {
        return $this->privacy_policy;
    }

    /**
     * @return int
     */
    public function created_by() :int
    {
        return $this->created_by;
    }

    /**
     * @return string
     */
    public function created_on() :string
    {
        return $this->created_on;
    }

    /**
     * @return int
     */
    public function updated_by() :int
    {
        return $this->updated_by;
    }

    /**
     * @return string
     */
    public function updated_on() :string
    {
        return $this->updated_on;
    }

    /**
     * @return string
     */
    public function activated_on() :?string
    {
        return $this->activated_on;
    }

    /**
     * @return string
     */
    public function last_visit_date() :?string
    {
        if (empty($this->last_visit_date)) {
            $this->last_visit_date = null;
        }

        return $this->last_visit_date;
    }

    /**
     * @param bool $getPath
     * @return string
     */
    public function avatar($getPath = false) :string
    {
        $avatar = $this->avatar;

        if ($getPath) {
            $file = env('PROFILE_PICTURE_DIR').$this->id().'/images/originalImage/'.$this->avatar;

            if (file_exists($file)) {
                $avatar = env('DOMAIN').env('PROFILE_IMG_PATH').$this->id().'/images/originalImage/'.$this->avatar;
            }
            else {
                $avatar = env('DOMAIN').env('DEFAULT_AVATAR');
            }

            if ($this->activation_token == 'from-facebook' && strpos($this->avatar, 'graph.facebook.com')) {
                $avatar = $this->avatar;
            }
        }

        if ($avatar == null) {
            $avatar = '';
        }

        return $avatar;
    }

    /**
     * @return int
     */
    public function gender() :int
    {
        if ($this->gender == null)
            $this->gender = 0;

        return $this->gender;
    }

    /**
     * @return string
     */
    public function description() :string
    {
        if ($this->description == null)
            $this->description = '';

        return $this->description;
    }

    /**
     * @return null|string
     */
    public function birthdate($format = false) :?string
    {
        if (empty($this->birthdate)) {
            $this->birthdate = null;
        }

        if ($format && !empty($this->birthdate)) {
            $this->birthdate = Carbon::parse($this->birthdate)->format('d-m-Y');
        }

        return $this->birthdate;
    }

    /**
     * @return string
     */
    public function location() :string
    {
        if ($this->location == null)
            $this->location = '';

        return $this->location;
    }

    /**
     * @return string
     */
    public function twitch() :string
    {
        if ($this->twitch == null)
            $this->twitch = '';

        return $this->twitch;
    }

    /**
     * @return string
     */
    public function instagram() :string
    {
        if ($this->instagram == null)
            $this->instagram = '';

        return $this->instagram;
    }

    /**
     * @return string
     */
    public function twitter() :string
    {
        if ($this->twitter == null)
            $this->twitter = '';

        return $this->twitter;
    }

    /**
     * @return string
     */
    public function facebook() :string
    {
        if ($this->facebook == null)
            $this->facebook = '';

        return $this->facebook;
    }

    /**
     * @return string
     */
    public function psn_id() :string
    {
        if ($this->psn_id == null)
            $this->psn_id = '';

        return $this->psn_id;
    }

    /**
     * @return string
     */
    public function xbox_live_gamertag() :string
    {
        if ($this->xbox_live_gamertag == null)
            $this->xbox_live_gamertag = '';

        return $this->xbox_live_gamertag;
    }

    /**
     * @return string
     */
    public function nintendo_id() :string
    {
        if ($this->nintendo_id == null)
            $this->nintendo_id = '';

        return $this->nintendo_id;
    }

    /**
     * @return string
     */
    public function steam_id() :string
    {
        if ($this->steam_id == null)
            $this->steam_id = '';

        return $this->steam_id;
    }

    /**
     * @return string
     */
    public function pc() :string
    {
        if ($this->pc == null)
            $this->pc = '';

        return $this->pc;
    }

    /**
     * @return string
     */
    public function tablet() :string
    {
        if ($this->tablet == null)
            $this->tablet = '';

        return $this->tablet;
    }

    /**
     * @return string
     */
    public function smartphone() :string
    {
        if ($this->smartphone == null)
            $this->smartphone = '';

        return $this->smartphone;
    }

    /**
     * @return string
     */
    public function signature() :string
    {
        if ($this->signature == null)
            $this->signature = '';

        return $this->signature;
    }

    /**
     * @return int
     */
    public function like_notifications() :int
    {
        if ($this->like_notifications == null)
            $this->like_notifications = 0;

        return $this->like_notifications;
    }

    /* SETTERS */

    /**
     * @param $value
     */
    public function setId($value)
    {
        $this->id = $value;
    }

    /**
     * @param $value
     */
    public function setUserName($value)
    {
        $this->user_name = $value;
    }

    /**
     * @param $value
     */
    public function setName($value)
    {
        $this->name = $value;
    }

    /**
     * @param $value
     */
    public function setLastName($value)
    {
        $this->last_name = $value;
    }

    /**
     * @param $value
     */
    public function setPassword($value)
    {
        $this->password = $value;
    }

    /**
     * @param $value
     */
    public function setRoleId($value)
    {
        $this->role_id = $value;
    }

    /**
     * @param $value
     */
    public function setAccessLevel($value)
    {
        $this->access_level = $value;
    }

    /**
     * @param $value
     */
    public function setEmail($value)
    {
        $this->email = $value;
    }

    /**
     * @param $value
     */
    public function setIsEnabled($value)
    {
        $this->is_enabled = $value;
    }

    /**
     * @param $value
     */
    public function setInBackend($value)
    {
        $this->in_backend = $value;
    }

    /**
     * @param $value
     */
    public function setIsActivated($value)
    {
        $this->is_activated = $value;
    }

    /**
     * @param $value
     */
    public function setActivationToken($value)
    {
        $this->activation_token = $value;
    }

    /**
     * @param $value
     */
    public function setPrivacyPolicy($value)
    {
        $this->privacy_policy = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedBy($value)
    {
        $this->created_by = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedOn($value)
    {
        $this->created_on = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedBy($value)
    {
        $this->updated_by = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedOn($value)
    {
        $this->updated_on = $value;
    }

    /**
     * @param $value
     */
    public function setActivatedOn($value)
    {
        $this->activated_on = $value;
    }

    /**
     * @param $value
     */
    public function setLastVisitDate($value)
    {
        $this->last_visit_date = $value;
    }

    /**
     * @param $value
     */
    public function setAvatar($value)
    {
        $this->avatar = $value;
    }

    /**
     * @param $value
     */
    public function setGender($value)
    {
        $this->gender = $value;
    }

    /**
     * @param $value
     */
    public function setDescription($value)
    {
        $this->description = $value;
    }

    /**
     * @param $value
     */
    public function setBirthdate($value)
    {
        $this->birthdate = $value;
    }

    /**
     * @param $value
     */
    public function setLocation($value)
    {
        $this->location = $value;
    }

    /**
     * @param $value
     */
    public function setTwitch($value)
    {
        $this->twitch = $value;
    }

    /**
     * @param $value
     */
    public function setInstagram($value)
    {
        $this->instagram = $value;
    }

    /**
     * @param $value
     */
    public function setTwitter($value)
    {
        $this->twitter = $value;
    }

    /**
     * @param $value
     */
    public function setFacebook($value)
    {
        $this->facebook = $value;
    }

    /**
     * @param $value
     */
    public function setPsnId($value)
    {
        $this->psn_id = $value;
    }

    /**
     * @param $value
     */
    public function setXboxLiveGamertag($value)
    {
        $this->xbox_live_gamertag = $value;
    }

    /**
     * @param $value
     */
    public function setNintendoId($value)
    {
        $this->nintendo_id = $value;
    }

    /**
     * @param $value
     */
    public function setSteamId($value)
    {
        $this->steam_id = $value;
    }

    /**
     * @param $value
     */
    public function setPc($value)
    {
        $this->pc = $value;
    }

    /**
     * @param $value
     */
    public function setTablet($value)
    {
        $this->tablet = $value;
    }

    /**
     * @param $value
     */
    public function setSmartPhone($value)
    {
        $this->smartphone = $value;
    }

    /**
     * @param $value
     */
    public function setSignature($value)
    {
        $this->signature = $value;
    }

    /**
     * @param $value
     */
    public function setLikeNotifications($value)
    {
        $this->like_notifications = $value;
    }
}