<?php

namespace App\Entities;

use App\Facades\Cache;
use Illuminate\Support\Arr;
use App\Models\Backend\Nodes;
use App\Models\Backend\Menus;
use App\Models\Backend\MenusLinks;

class MenuLinkEntity
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $menu_id;

    /**
     * @var MenuEntity
     */
    protected $menu;

    /**
     * @var int
     */
    protected $parent_id;

    /**
     * @var MenuLinkEntity
     */
    protected $parent;

    /**
     * @var array
     */
    protected $childs;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @var int
     */
    protected $node_id;

    /**
     * @var NodeEntity
     */
    protected $node;

    /**
     * @var int
     */
    protected $is_active;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var
     */
    protected $class;

    /**
     * @var int
     */
    protected $ordering;

    /**
     * @var int
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $created_on;

    /**
     * @var int
     */
    protected $updated_by;

    /**
     * @var string
     */
    protected $updated_on;


    /* GETTERS */

    /**
     * @return MenuLinkEntity
     */
    public function parent() :MenuLinkEntity
    {
        $cachedMenuLinks = Cache::get('menu-links');
        $this->parent = Arr::get($cachedMenuLinks, $this->id().'.parent');

        if (empty($this->parent)) {
            $menusLinks = new MenusLinks();
            $this->parent = $menusLinks->getEntity($this->parent_id());
            Arr::set($cachedMenuLinks, $this->id().'.parent', $this->parent);
            Cache::set('menu-links', $cachedMenuLinks);
        }

        return $this->parent;
    }

    /**
     * @return NodeEntity
     */
    public function node() :NodeEntity
    {
        $cachedMenuLinks = Cache::get('menu-links');
        $this->node = Arr::get($cachedMenuLinks, $this->id().'.node');

        if (empty($this->node)) {
            $this->node = Nodes::findById($this->node_id());
            Arr::set($cachedMenuLinks, $this->id().'.node', $this->node);
            Cache::set('menu-links', $cachedMenuLinks);
        }

        return $this->node;
    }

    /**
     * @return MenuEntity
     */
    public function menu() :MenuEntity
    {
        $this->menu = Menus::findById($this->menu_id);

        return $this->menu;
    }

    /**
     * @return int
     */
    public function id() :int
    {
        if (empty($this->id))
            $this->id = (int) 0;

        return $this->id;
    }

    /**
     * @return int
     */
    public function menu_id() :int
    {
        if (empty($this->id))
            $this->id = (int) 0;

        return $this->id;
    }

    /**
     * @return int
     */
    public function parent_id() :int
    {
        if (empty($this->parent_id))
            $this->parent_id = (int) 0;

        return $this->parent_id;
    }

    /**
     * @return array
     */
    public function childs() :array
    {
        return $this->childs;
    }

    /**
     * @return string
     */
    public function title() :string
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function alias() :?string
    {
        return $this->alias;
    }

    /**
     * @return int
     */
    public function node_id() :int
    {
        if (is_null($this->node_id))
            $this->node_id = 0;

        return $this->node_id;
    }

    /**
     * @return int
     */
    public function is_active() :int
    {
        return $this->is_active;
    }

    /**
     * @return string
     */
    public function url() :string
    {
        if (empty($this->url))
            $this->url = '';

        return $this->url;
    }

    /**
     * @return string
     */
    public function class() :string
    {
        if (empty($this->class))
            $this->class = '';

        return $this->class;
    }

    /**
     * @return array
     */
    public function ordering() :array
    {
        return $this->ordering;
    }

    /**
     * @return int
     */
    public function created_by() :int
    {
        return $this->created_by;
    }

    /**
     * @return string
     */
    public function created_on() :string
    {
        return $this->created_on;
    }

    /**
     * @return int
     */
    public function updated_by() :int
    {
        return $this->updated_by;
    }

    /**
     * @return string
     */
    public function updated_on() :string
    {
        return $this->updated_on;
    }

    /* SETTERS */

    /**
     * @param $value
     */
    public function setId($value)
    {
        $this->id = $value;
    }

    /**
     * @param $value
     */
    public function setMenuId($value)
    {
        $this->menu_id = $value;
    }

    /**
     * @param $value
     */
    public function setParentId($value)
    {
        $this->parent_id = $value;
    }

    /**
     * @param $value
     */
    public function setChilds($value)
    {
        $this->childs = $value;
    }

    /**
     * @param $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }

    /**
     * @param $value
     */
    public function setAlias($value)
    {
        $this->alias = $value;
    }

    /**
     * @param $value
     */
    public function setNodeId($value)
    {
        $this->node_id = $value;
    }

    /**
     * @param $value
     */
    public function setIsActive($value)
    {
        $this->is_active = $value;
    }

    /**
     * @param $value
     */
    public function setUrl($value)
    {
        $this->url = $value;
    }

    /**
     * @param $value
     */
    public function setClass($value)
    {
        $this->class = $value;
    }

    /**
     * @param $value
     */
    public function setOrdering($value)
    {
        $this->ordering = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedBy($value)
    {
        $this->created_by = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedOn($value)
    {
        $this->created_on = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedBy($value)
    {
        $this->updated_by = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedOn($value)
    {
        $this->updated_on = $value;
    }
}