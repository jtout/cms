<?php

namespace App\Entities;

use App\Events\RoleEvents;
use App\Models\Backend\Roles;

class RoleEntity extends Entity
{
    /**
     * @var string
     */
    protected $model = Roles::class;

    /**
     * @var string
     */
    protected $entity_events = RoleEvents::class;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $parent_id;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var int
     */
    protected $in_backend;

    /**
     * @var int
     */
    protected $access_level_id;

    /**
     * @var string
     */
    protected $access_level;

    /**
     * @var string
     */
    protected $access_level_label;

    /**
     * @var mixed
     */
    protected $permissions;

    /**
     * @var int
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $created_on;

    /**
     * @var int
     */
    protected $updated_by;

    /**
     * @var string
     */
    protected $updated_on;

    /**
     * @var int
     */
    protected $depth;


    /* GETTERS */

    /**
     * @return int
     */
    public function id() :int
    {
        if (empty($this->id))
            $this->id = (int) 0;

        return $this->id;
    }

    /**
     * @return int
     */
    public function parent_id() :int
    {
        if ($this->parent_id == null)
            $this->parent_id = (int) 0;

        return $this->parent_id;
    }

    /**
     * @return string
     */
    public function title() :string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function in_backend() :int
    {
        return $this->in_backend;
    }

    /**
     * @return int
     */
    public function access_level_id() :int
    {
        return $this->access_level_id;
    }

    /**
     * @return string
     */
    public function access_level() :string
    {
        return $this->access_level;
    }

    /**
     * @return string
     */
    public function access_level_label() :string
    {
        return $this->access_level_label;
    }

    /**
     * @return string
     */
    public function permissions() :string
    {
        if ($this->permissions == null)
            $this->permissions = '';

        return $this->permissions;
    }

    /**
     * @return int
     */
    public function created_by() :int
    {
        return $this->created_by;
    }

    /**
     * @return string
     */
    public function created_on() :string
    {
        return $this->created_on;
    }

    /**
     * @return int
     */
    public function updated_by() :int
    {
        return $this->updated_by;
    }

    /**
     * @return string
     */
    public function updated_on() :string
    {
        return $this->updated_on;
    }

    /**
     * @return int
     */
    public function depth() :int
    {
        return $this->depth;
    }

    /* SETTERS */

    /**
     * @param $value
     */
    public function setId($value)
    {
        $this->id = $value;
    }

    /**
     * @param $value
     */
    public function setParentId($value)
    {
        $this->parent_id = $value;
    }

    /**
     * @param $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }

    /**
     * @param $value
     */
    public function setInBackend($value)
    {
        $this->in_backend = $value;
    }

    /**
     * @param $value
     */
    public function setAccessLevelId($value)
    {
        $this->access_level_id = $value;
    }

    /**
     * @param $value
     */
    public function setAccessLevel($value)
    {
        $this->access_level = $value;
    }

    /**
     * @param $value
     */
    public function setAccessLevelLabel($value)
    {
        $this->access_level_label = $value;
    }

    /**
     * @param $value
     */
    public function setPermissions($value)
    {
        $this->permissions = $value;
    }

    /**
     * @param $value
     */
    public function setDepth($value)
    {
        $this->depth = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedBy($value)
    {
        $this->created_by = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedOn($value)
    {
        $this->created_on = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedBy($value)
    {
        $this->updated_by = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedOn($value)
    {
        $this->updated_on = $value;
    }
}