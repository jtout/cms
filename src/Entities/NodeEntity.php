<?php

namespace App\Entities;

use App\Traits\App;
use App\Traits\Media;
use App\Facades\Cache;
use App\Events\NodeEvents;
use Illuminate\Support\Arr;
use App\Models\Backend\Nodes;
use App\Factories\NodeFactory;
use App\Factories\UserFactory;
use App\Factories\ClientFactory;
use App\Factories\ModuleFactory;
use App\Traits\ExtraFieldsHelper;
use App\Factories\LanguageFactory;
use Illuminate\Support\Collection;
use App\Models\Backend\ExtraFields;
use App\Factories\ContentTypeFactory;
use App\Factories\AccessLevelFactory;
use App\Observers\Cache as CacheObserver;

class NodeEntity extends Entity
{
    use ExtraFieldsHelper;

    /**
     * @var string
     */
    protected $model = Nodes::class;

    /**
     * @var string
     */
    protected $entity_events = NodeEvents::class;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $parent_id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $view;

    /**
     * @var int
     */
    protected $module_id;

    /**
     * @var string
     */
    protected $module_title;

    /**
     * @var string
     */
    protected $module_class;

    /**
     * @var string
     */
    protected $module_action;

    /**
     * @var string
     */
    protected $module_request_type;

    /**
     * @var int
     */
    protected $in_backend;

    /**
     * @var int
     */
    protected $is_active;

    /**
     * @var int
     */
    protected $on_menu;

    /**
     * @var int
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $created_on;

    /**
     * @var int
     */
    protected $updated_by;

    /**
     * @var string
     */
    protected $updated_on;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $intro_text;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $image;

    /**
     * @var int
     */
    protected $gallery;

    /**
     * @var string
     */
    protected $video;

    /**
     * @var string
     */
    protected $icon;

    /**
     * @var int
     */
    protected $language_id;

    /**
     * @var string
     */
    protected $language_iso_code;

    /**
     * @var int
     */
    protected $content_type_id;

    /**
     * @var string
     */
    protected $content_type;

    /**
     * @var string
     */
    protected $content_type_media_folder;

    /**
     * @var int
     */
    protected $is_featured;

    /**
     * @var int
     */
    protected $is_sticky;

    /**
     * @var int
     */
    protected $sponsored_by_id;

    /**
     * @var string
     */
    protected $social_media_title;

    /**
     * @var string
     */
    protected $meta_title;

    /**
     * @var string
     */
    protected $meta_description;

    /**
     * @var string
     */
    protected $robots;

    /**
     * @var string
     */
    protected $published_on;

    /**
     * @var string
     */
    protected $finished_on;

    /**
     * @var int
     */
    protected $homepage;

    /**
     * @var int
     */
    protected $extra_fields_group_id;

    /**
     * @var string
     */
    protected $extra_fields_values;

    /**
     * @var int
     */
    protected $comments_lock;

    /**
     * @var int
     */
    protected $depth;

    /**
     * @var int
     */
    protected $access_level_id;

    /**
     * @var int
     */
    protected $order;

    /**
     * @var array
     */
    protected $childs;

    /**
     * @var int
     */
    protected $from_id;

    /**
     * @var
     */
    protected $tags_articles_count;

    /**
     * @var array
     */
    protected $comments;


    /**
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        parent::registerObservers(new CacheObserver);
    }

	/* RELATIONS */

    /**
     * @return UserEntity
     */
    public function author() :UserEntity
	{
        $author = $this->getCachedField('author');

        return UserFactory::build($author);
	}

    /**
     * @return UserEntity
     */
    public function updated_by_author() :UserEntity
	{
        $updatedByAuthor = $this->getCachedField('updated_by_author');

        return UserFactory::build($updatedByAuthor);
	}

    /**
     * @return NodeEntity
     */
    public function parent() :NodeEntity
    {
        $parent = $this->getCachedField('parent');

        return NodeFactory::build($parent);
    }

    /**
     * @return LanguageEntity
     */
    public function language() :LanguageEntity
    {
        $language = $this->getCachedField('language');

        return LanguageFactory::build($language);
    }

    /**
     * @return AccessLevelEntity
     */
    public function access_level() :AccessLevelEntity
    {
        $accessLevel = $this->getCachedField('access_level');

        return AccessLevelFactory::build($accessLevel);
    }

    /**
     * @return ClientEntity
     */
    public function sponsored_by() :ClientEntity
    {
        $sponsoredBy = $this->getCachedField('sponsored_by');

        return ClientFactory::build($sponsoredBy);
    }

    /**
     * @return ModuleEntity
     */
    public function module() :ModuleEntity
    {
        if (is_null($this->relations->module)) {
            $this->loadRelations(['module']);
            $this->updateCache();
        }

        return ModuleFactory::build($this->relations->module);
    }

    /**
     * @return ContentTypeEntity
     */
    public function content_type() :ContentTypeEntity
    {
        if (is_null($this->relations->content_type)) {
            $this->loadRelations(['content_type']);
            $this->updateCache();
        }

        return ContentTypeFactory::build($this->relations->content_type);
    }

    /**
     * @return Collection
     */
    public function related() :Collection
	{
        $data = $this->getCache();
        $related = Arr::get($data, 'related');

        if (empty($related) && $this->tags()->count() > 0) {
            $nodes = new \App\Models\Frontend\Nodes();
            $related = $nodes->getRelatedNodes($this->tags()->first()->id(), $this->language_id(), 3, $this->id());

            Arr::set($data, 'related', $related);
            $this->updateCache($data);
        }

        return empty($related) ? new Collection() : $related;
	}

    /**
     * @return array
     */
    public function extra_fields() :array
    {
        $data = $this->getCache();
        $extraFieldsValues = Arr::get($data, 'extra_fields');

        if ($this->extra_fields_group_id() && empty($extraFieldsValues)) {
            $extraFieldsModel = new ExtraFields();
            $extraFields = $extraFieldsModel->getExtraFields(['group_id' => $this->extra_fields_group_id(), 'is_active' => 1]);
            $extraFieldsValues =  $this->getNodeExtraFieldsValues($extraFields, $this->extra_fields_values());

            Arr::set($data, 'extra_fields', $extraFieldsValues);
            $this->updateCache($data);
        }

        return $extraFieldsValues ?? [];
    }

    /**
     * @return NodeEntity
     */
    public function next_node() :NodeEntity
    {
        $data = $this->getCache();
        $nextNode = Arr::get($data, 'next-node');

        if (empty($nextNode)) {
            $nodes = new \App\Models\Frontend\Nodes();
            $nextNode = $nodes->getCloserArticle('next', $this->id(), $this->parent_id());

            Arr::set($data, 'next-node', $nextNode);
            $this->updateCache($data);
        }

        return $nextNode;
    }

    /**
     * @return NodeEntity
     */
    public function previous_node() :NodeEntity
    {
        $data = $this->getCache();
        $previousNode = Arr::get($data, 'previous-node');

        if (empty($previousNode)) {
            $nodes = new \App\Models\Frontend\Nodes();
            $previousNode = $nodes->getCloserArticle('previous', $this->id(), $this->parent_id());

            Arr::set($data, 'previous-node', $previousNode);
            $this->updateCache($data);
        }

        return $previousNode;
    }

    /**
     * @return Collection
     */
    public function tags() :Collection
    {
        if (is_null($this->relations->tags)) {
            $this->loadRelations(['tags']);
            $this->updateCache();
        }

        return $this->relations->tags ?? new Collection();
    }

    /**
     * @return int
     */
    public function comments_count() :int
    {
        if (!$this->relations->comments_count) {
            $this->loadRelations(['comments_count']);
            $this->updateCache();
        }

        return $this->relations->comments_count->count ?? 0;
    }

    /**
     * @return bool
     */
    public function show_access_level_label() :bool
    {
        if (user()->role_id() == 6 || user()->role_id() == 7 || empty(user()->role_id()))
            return false;

        return true;
    }

    /**
     * @return bool
     */
    public function is_article() :bool
    {
        if ($this->content_type_id() == 1) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function is_tag() :bool
    {
        if ($this->content_type_id() == 4) {
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function getCache() :array
    {
        return Arr::wrap(Cache::get($this->path(true)));
    }

    /**
     * @param $data
     */
    public function updateCache($data = null) :void
    {
        $cacheKey = !empty($this->path) ? $this->path(true) : null;

        if (!is_null($cacheKey)) {
            if (is_null($data) && !empty($this->module_class) && !empty($this->module_action)) {
                $data = $this->getCache();
                $data['node'] = $this;
            }
            else if (!is_null($data)) {
                Cache::set($cacheKey, $data);
            }
        }
    }

    /**
     * @param string $field
     * @return mixed
     */
    public function getCachedField(string $field)
    {
        $cachedData = $this->getCache();
        $cachedField = Arr::get($cachedData, $field);

        if (empty($cachedField)) {
            if (is_null($this->relations->{$field})) {
                $this->loadRelations([$field]);
                $this->updateCache();
            }

            $cachedField = $this->relations->{$field};
            Arr::set($cachedData, $field, $cachedField);
            $this->updateCache($cachedData);
        }

        return $cachedField;
    }

    /* GETTERS */

    /**
     * @return int
     */
    public function id() :int
	{
		return $this->id ?? 0;
	}

    /**
     * @return int
     */
    public function parent_id() :int
	{
		return $this->parent_id ?? 1;
	}

    /**
     * @return string
     */
    public function name() :string
	{
		return $this->name ?? '';
	}

    /**
     * @param bool $parse
     * @param bool $relative
     * @return string
     */
    public function path($parse = true, $relative = false) :string
	{
		if ($parse) {
            $path = env('DOMAIN').App::parsePath($this->path);

            if ($relative) {
                $path = str_replace(env('DOMAIN'), '', $path);
            }

		    return $path;
        }
		else {
            return $this->path;
        }
	}

    /**
     * @return string
     */
    public function view() :string
	{
		return $this->view;
	}

    /**
     * @return int
     */
    public function module_id() :int
	{
		return $this->module_id;
	}

    /**
     * @return string|null
     */
    public function module_title() :string
    {
        return $this->module_title ?? '';
    }

    /**
     * @return string|null
     */
    public function module_class() :string
    {
        return $this->module_class ?? '';
    }

    /**
     * @return string|null
     */
    public function module_action() :string
    {
        return $this->module_action ?? '';
    }

    /**
     * @return string|null
     */
    public function module_request_type() :string
    {
        return $this->module_request_type ?? '';
    }

    /**
     * @return int
     */
    public function in_backend() :int
    {
        return $this->in_backend ?? 0;
    }

    /**
     * @return int
     */
    public function is_active() :int
    {
        return $this->is_active ?? 0;
    }

    /**
     * @return int
     */
    public function on_menu() :int
    {
        return $this->on_menu;
    }

    /**
     * @return int
     */
    public function created_by() :int
    {
        return $this->created_by ?? 0;
    }

    /**
     * @return string
     */
    public function created_on() :string
    {
        return $this->created_on;
    }

    /**
     * @return int
     */
    public function updated_by() :int
    {
    	return $this->updated_by ?? 0;
    }

    /**
     * @return string
     */
    public function updated_on() :string
    {
        return $this->updated_on;
    }

    /**
     * @param bool $removeSpecialChars
     * @param bool $encodeHtmlCharacters
     * @return string
     */
    public function title($removeSpecialChars = false, $encodeHtmlCharacters =  false) :string
    {
        if ($removeSpecialChars) {
            $this->title = str_replace('|', '', strip_tags($this->title));
        }

        if ($encodeHtmlCharacters) {
            $this->title = htmlspecialchars($this->title);
        }

        if (is_null($this->title)) {
            $this->title = '';
        }

        return $this->title;
    }

    /**
     * @param bool $encodeHtmlCharacters
     * @return string
     */
    public function intro_text($encodeHtmlCharacters = false) :string
    {
        if ($this->intro_text == null) {
            $this->intro_text = '';
        }

        if ($encodeHtmlCharacters) {
            $this->intro_text = htmlspecialchars($this->intro_text);
        }

        return $this->intro_text;
    }

    /**
     * @param bool $parse
     * @return string
     */
    public function description($parse = false) :string
	{
		$description = $this->description;

	    if ($parse) {
			$dom = new \DOMDocument('1.0', 'UTF-8');
			@$dom->loadHTML(mb_convert_encoding($this->description, 'HTML-ENTITIES', 'UTF-8'));
			$images = $dom->getElementsByTagName('img');

			foreach ($images as $image){
				$image->setAttribute('class', 'lazyload');
                $image->setAttribute('data-src', $image->getAttribute('src'));
                $image->setAttribute('src', env('DEFAULT_IMAGE'));
            }

            $description = $dom->saveHTML();
		}

		return $description;
	}

    /**
     * @param bool $getImagePath
     * @param bool $type
     * @param bool $size
     * @return string
     */
    public function image($getImagePath = false, $type = false, $size = false) :string
	{
		if ($getImagePath === true) {
            $path = $this->image_path($type, $size);
		    return !empty($path) ? $path : env('DEFAULT_IMAGE');
		}

		if (empty($this->image)) {
            $this->image = '';
        }

		return $this->image;
	}

    /**
     * @param $type
     * @param $size
     * @return string|null
     */
    private function image_path($type, $size)
    {
        $path = env('DOMAIN').'/media/source/'.$type.'/'.$this->id.'/images/'.$size;
        $image = !empty($this->image) ? $path.'/'.$this->image : '';

        return $image;
    }

    /**
     * @param bool $parse
     * @return array|int
     */
    public function gallery($parse = false)
	{
		if ($parse) {
            $images = array();
            $gallery = env('NEW_GALLERY_DIR').$this->id.'/gallery';

            if (file_exists($gallery)) {
                $photos = array_diff(scandir($gallery), array('..', '.'));
                foreach($photos as $photo) {
                    if (preg_match('/(\.jpg|\.png|\.bmp)$/', $photo)) {
                        $images['source'][] = env('DOMAIN').'/media/source/Articles/'.$this->id.'/gallery/'.$photo;
                        $images['thumbs'][] = env('DOMAIN').'/media/source/Articles/'.$this->id.'/gallery/'.$photo;
                    }
                }
            }
            else {
                $photos = null;
            }

            $this->gallery = $images;
        }

		return $this->gallery ?? 0;
	}

    /**
     * @param null $parse
     * @return string
     */
    public function video($parse = null) :string
    {
        if ($parse) {
            $videoSource = Media::checkVideoSource($this->video);
            $video = Media::stripVideoSourcesTags($this->video, $videoSource);
            $this->video = replace_characters(Media::videoSources($video)[$videoSource], "\n", '<br />');
        }

        return $this->video ?? '';
    }

    /**
     * @return string
     */
    public function video_source() :string
    {
        $source =  !empty($this->video()) ? Media::checkVideoSource($this->video) : '';

        return $source;
    }

    /**
     * @return string
     */
    public function video_stripped() :string
    {
        return !empty($this->video_source()) ? Media::stripVideoSourcesTags($this->video, $this->video_source()) : '';
    }

    /**
     * @param bool $removeSpecialChars
     * @return string
     */
    public function social_media_title($removeSpecialChars = false) :string
    {
        if ($removeSpecialChars) {
            $this->social_media_title = App::clean($this->social_media_title);
        }

        return $this->social_media_title ?? '';
    }

    /**
     * @return string
     */
    public function meta_title() :string
	{
		return $this->meta_title ?? '';
	}

    /**
     * @param bool $encodeHtmlCharacters
     * @return string
     */
    public function meta_description($encodeHtmlCharacters = false) :string
	{
		if ($this->meta_description == null) {
            $this->meta_description = '';
        }

        if ($encodeHtmlCharacters) {
            $this->meta_description = htmlspecialchars($this->meta_description);
        }

		return $this->meta_description;
	}

    /**
     * @return string
     */
    public function robots() :string
	{
		return $this->robots ?? '';
	}

    /**
     * @param bool $format
     * @return string
     */
    public function published_on($format = false) :string
	{
		if ($format) {
            $publishedOn = \DateTime::createFromFormat('Y-m-d H:i:s', $this->published_on);
            $publishedOnFormatted = $publishedOn ? $publishedOn->format('d M Y H:i') : '';

            return $publishedOnFormatted;
        }

		return $this->published_on ?? '0000-00-00 00:00:00';
	}

    /**
     * @return string
     */
    public function finished_on() :string
	{
		return $this->finished_on ?? '0000-00-00 00:00:00';
	}

    /**
     * @return string
     */
    public function icon() :string
    {
		return $this->icon ?? '';
    }

    /**
     * @return int
     */
    public function language_id() :int
	{
		return $this->language_id;
	}

    /**
     * @return string
     */
    public function language_iso_code() :string
	{
		return $this->language_iso_code;
	}

    /**
     * @return int
     */
    public function content_type_id() :int
	{
		return $this->content_type_id;
	}

    /**
     * @return string
     */
    public function content_type_media_folder() :string
	{
		return $this->content_type_media_folder ?? '';
	}

    /**
     * @return int
     */
    public function is_featured() :int
	{
		return $this->is_featured ?? 0;
	}

    /**
     * @return int
     */
    public function is_sticky() :int
	{
		return $this->is_sticky ?? 0;
	}

    /**
     * @return int
     */
    public function sponsored_by_id() :int
    {
        return $this->sponsored_by_id ?? 0;
    }

    /**
     * @return int
     */
    public function homepage() :int
	{
		return $this->homepage ?? 0;
	}

    /**
     * @return int
     */
    public function extra_fields_group_id() :int
	{
		return $this->extra_fields_group_id ?? 0;
	}

    /**
     * @return string
     */
    public function extra_fields_values() :string
	{
		return $this->extra_fields_values ?? '';
	}

    /**
     * @return int
     */
    public function comments_lock() :int
	{
		return $this->comments_lock ?? 0;
	}

    /**
     * @return int
     */
    public function depth() :int
	{
		return $this->depth;
	}

    /**
     * @return int
     */
    public function access_level_id() :int
	{
	    return $this->access_level_id;
	}

    /**
     * @return int
     */
    public function order() :int
    {
		return $this->order ?? 0;
    }

    /**
     * @return mixed
     */
    public function childs()
    {
        return $this->childs;
    }

    /**
     * @return Collection
     */
    public function translations() :Collection
	{
        return $this->relations->translations ?? collect();
	}

    /**
     * @return int
     */
    public function from_id() :int
	{
		return $this->from_id ?? 0;
	}

    /**
     * @return int
     */
    public function tags_articles_count() :int
    {
        return $this->tags_articles_count ?? 0;
    }

    /**
     * @return array
     */
    public function comments() :array
    {
        return $this->comments ?? array();
    }

    /**
     * @return bool
     */
    public function is_visible() :bool
    {
        if ((in_array($this->access_level_id(), explode(',', user()->access_level()))) &&
            ($this->published_on() <= now()))
        {
            return true;
        }

        return false;
    }

    /* SETTERS */

    /**
     * @param $value
     */
    public function setId($value)
    {
        $this->id = $value;
    }

    /**
     * @param $value
     */
    public function setName($value)
    {
        $this->name = $value;
    }

    /**
     * @param $value
     */
    public function setPath($value)
    {
        $this->path = $value;
    }

    /**
     * @param $value
     */
    public function setParentId($value)
    {
        $this->parent_id = $value;
    }

    /**
     * @param $value
     */
    public function setView($value)
    {
        $this->view = $value;
    }

    /**
     * @param $value
     */
    public function setModuleId($value)
    {
        $this->module_id = $value;
    }

    /**
     * @param $value
     */
    public function setModuleTitle($value)
    {
        $this->module_title = $value;
    }

    /**
     * @param $value
     */
    public function setModuleAction($value)
    {
        $this->module_action = $value;
    }

    /**
     * @param $value
     */
    public function setModuleClass($value)
    {
        $this->module_class = $value;
    }

    /**
     * @param $value
     */
    public function setModuleRequestType($value)
    {
        $this->module_request_type = $value;
    }

    /**
     * @param $value
     */
    public function setIsActive($value)
    {
        $this->is_active = $value;
    }

    /**
     * @param $value
     */
    public function setOnMenu($value)
    {
        $this->on_menu = $value;
    }

    /**
     * @param $value
     */
    public function setInBackend($value)
    {
        $this->in_backend = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedBy($value)
    {
        $this->created_by = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedOn($value)
    {
        $this->created_on = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedBy($value)
    {
        $this->updated_by = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedOn($value)
    {
        $this->updated_on = $value;
    }

    /**
     * @param $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }

    /**
     * @param $value
     */
    public function setIntroText($value)
    {
        $this->intro_text = $value;
    }

    /**
     * @param $value
     */
    public function setDescription($value)
    {
        $this->description = $value;
    }

    /**
     * @param $value
     */
    public function setImage($value)
    {
        $this->image = $value;
    }

    /**
     * @param $value
     */
    public function setGallery($value)
    {
        $this->gallery = $value;
    }

    /**
     * @param $value
     */
    public function setVideo($value)
    {
        $this->video = $value;
    }

    /**
     * @param $value
     */
    public function setIcon($value)
    {
        $this->icon = $value;
    }

    /**
     * @param $value
     */
    public function setLanguageId($value)
    {
        $this->language_id = $value;
    }

    /**
     * @param $value
     */
    public function setLanguageIsoCode($value)
    {
        $this->language_iso_code = $value;
    }

    /**
     * @param $value
     */
    public function setContentTypeId($value)
    {
        $this->content_type_id = $value;
    }

    /**
     * @param $value
     */
    public function setMediaFolder($value)
    {
        $this->content_type_media_folder = $value;
    }

    /**
     * @param $value
     */
    public function setIsFeatured($value)
    {
        $this->is_featured = $value;
    }

    /**
     * @param $value
     */
    public function setIsSticky($value)
    {
        $this->is_sticky = $value;
    }

    /**
     * @param $value
     */
    public function setSponsoredById($value)
    {
        $this->sponsored_by_id = $value;
    }

    /**
     * @param $value
     */
    public function setSocialMediaTitle($value)
    {
        $this->social_media_title = $value;
    }

    /**
     * @param $value
     */
    public function setMetaTitle($value)
    {
        $this->meta_title = $value;
    }

    /**
     * @param $value
     */
    public function setMetaDescription($value)
    {
        $this->meta_description = $value;
    }

    /**
     * @param $value
     */
    public function setRobots($value)
    {
        $this->robots = $value;
    }

    /**
     * @param $value
     */
    public function setPublishedOn($value)
    {
        $this->published_on = $value;
    }

    /**
     * @param $value
     */
    public function setFinishedOn($value)
    {
        $this->finished_on = $value;
    }

    /**
     * @param $value
     */
    public function setHomepage($value)
    {
        $this->homepage = $value;
    }

    /**
     * @param $value
     */
    public function setExtraFieldsGroupId($value)
    {
        $this->extra_fields_group_id = $value;
    }

    /**
     * @param $value
     */
    public function setExtraFieldsValues($value)
    {
        $this->extra_fields_values = $value;
    }

    /**
     * @param $value
     */
    public function setCommentLock($value)
    {
        $this->comments_lock = $value;
    }

    /**
     * @param $value
     */
    public function setDepth($value)
    {
        $this->depth = $value;
    }

    /**
     * @param $value
     */
    public function setAccessLevelId($value)
    {
        $this->access_level_id = $value;
    }

    /**
     * @param $value
     */
    public function setOrder($value)
    {
        $this->order = $value;
    }

    /**
     * @param $value
     */
    public function setChilds($value)
    {
        $this->childs = $value;
    }

    /**
     * @param $value
     */
    public function setTranslations($value)
    {
        $this->relations->translations = $value;
    }

    /**
     * @param $value
     */
    public function setFromId($value)
    {
        $this->from_id = $value;
    }

    /**
     * @param $value
     */
    public function setTagsArticlesCount($value)
    {
        $this->tags_articles_count = $value;
    }

    /**
     * @param $value
     */
    public function setComments($value)
    {
        $this->comments = $value;
    }
}