<?php

namespace App\Entities;

use JsonSerializable;
use Illuminate\Support\Arr;
use App\Interfaces\Jsonable;
use App\Interfaces\Observer;
use App\Interfaces\Arrayable;
use App\Interfaces\Observable;
use Illuminate\Support\Collection;

class Entity implements Observable, Arrayable, Jsonable, JsonSerializable
{
    /**
     * @var string
     */
    protected $model;

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $original_values;

    /**
     * @var array
     */
    protected $observers = [];

    /**
     * @var string
     */
    protected $observer_event_fired;

    /**
     * @var string
     */
    protected $entity_events;

    /**
     * @var string
     */
    protected $event_fired;

    /**
     * @var mixed
     */
    protected static $instance;

    /**
     * @var array
     */
    protected static $bootedEntities;

    /**
     * @var \stdClass
     */
    protected $relations;

    /**
     * Entity constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->process($attributes);

        $this->syncOriginal();

        $this->bootEntity();
    }

    /**
     * @return void
     */
    public function syncOriginal()
    {
        $this->setOriginal($this->toArray());
    }

    /**
     * @return void
     */
    protected function bootEntity()
    {
        static::$instance = $this;

        static::boot();
    }

    /**
     * @return void
     */
    protected static function boot()
    {
        if(!isset(static::$bootedEntities[static::class])) {
            static::$bootedEntities[static::class] = true;
        }
    }

    /**
     * @param $observers
     */
    protected static function registerObservers($observers)
    {
        foreach (Arr::wrap($observers) as $observer) {
            $observer::observe(static::$instance);
        }
    }

    /**
     * @param string $string
     */
    public function fireEvent(string $string)
    {
        if (!empty($this->entity_events)) {
            $events = new $this->entity_events();

            if (method_exists($events, $string)) {
                $this->event_fired = $string;
                $events->$string($this);
            }
        }
    }

    /**
     * @return array
     */
    public function getBootedEntities()
    {
        return static::$bootedEntities;
    }

    /**
     * @return mixed
     */
    public function reload()
    {
        $model = $this->getModel();

        return $model::findById($this->id(), ['*']);
    }

    /**
     * @param array
     */
    public function setOriginal($values)
    {
        $this->original_values = collect($values);
    }

    /**
     * @param string $value
     * @return mixed
     */
    public function getOriginal(string $value)
    {
        if ($value == '*') {
            return $this->original_values;
        }

        return $this->original_values->get($value);
    }

    /**
     * @param string $value
     * @return mixed
     */
    public function isDirty(string $value)
    {
        return $this->getOriginal($value) != $this->$value();
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return new $this->model();
    }

    /**
     * @return array
     */
    public function getObservers()
    {
        return $this->observers;
    }

    /**
     * @param $data
     * @param null $options
     * @return mixed
     */
    public function process($data, $options = null)
    {
        if (!empty($this->model)) {
            $factory = $this->getModel()->getFactory();

            return $factory::build($data, $options, $this);
        }

        return null;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }

    /**
     * @return Collection
     */
    public function toCollection() :Collection
    {
        return collect($this->toArray());
    }

    /**
     * @param int $options
     * @return false|string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->jsonSerialize(), $options);
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * @param Observer $observer
     */
    public function attach(Observer $observer)
    {
        $this->observers[get_class($observer)] = $observer;
    }

    /**
     * @param Observer $observer
     */
    public function detach(Observer $observer)
    {
        foreach ($this->observers as $key => $val) {
            if ($val == $observer) {
                unset($this->observers[$key]);
            }
        }
    }

    /**
     * @param string $event
     * @param null $observer
     * @return mixed|void
     */
    public function fireObserverEvent(string $event, $observer = null)
    {
        $this->observer_event_fired = $event;
        $this->notify($observer);
    }

    /**
     * @return mixed
     */
    public function getFiredObserverEvent()
    {
        return $this->observer_event_fired;
    }

    /**
     * @param null $observer
     * @return mixed|void
     */
    public function notify($observer = null)
    {
        $event = $this->getFiredObserverEvent();

        if ($observer && isset($this->observers[$observer])) {
            $this->observers[$observer]->$event($this);
        }
        else {
            foreach ($this->observers as $observer) {
                $observer->$event($this);
            }
        }
    }

    /**
     * @param array $relations
     * @return void
     */
    public function loadRelations(array $relations)
    {
        $model = $this->getModel();
        $collection = collect([(object)$this->toArray()]);
        $relationsToSearch = $model->matchRelations($relations);
        $relationsData = $model->loadDataRelations($collection, array_keys($relationsToSearch));
        $collection = $model->mapRelationsDataToEntity($collection, $relationsToSearch, $relationsData);

        $mergedRelations = (object) array_merge((array) $this->getRelations(), (array) $collection->first()->relations);
        $this->setRelations($mergedRelations);
    }

    /**
     * @param $value
     */
    public function setRelations($value)
    {
        $this->relations = $value;
    }

    /**
     * @return \stdClass
     */
    public function getRelations()
    {
        return $this->relations;
    }

    /**
     * @return bool
     */
    public function insert() :bool
    {
        $insert = false;
        $model = $this->getModel();

        $model->transaction(function () use (&$insert, $model) {
            $this->fireEvent('inserting');

            if ($insert = $model->insert($this)) {
                $this->fireEvent('inserted');
            }

            return $insert;
        });

        return $insert;
    }

    /**
     * @return bool
     */
    public function update() :bool
    {
        $update = false;
        $model = $this->getModel();

        $model->transaction(function () use (&$update, $model) {
            $this->fireEvent('updating');

            if ($update = $model->update($this)) {
                $this->fireEvent('updated');
            }

            return $update;
        });

        return $update;
    }

    /**
     * @return bool
     */
    public function delete() :bool
    {
        $delete = false;
        $model = $this->getModel();

        $model->transaction(function () use (&$delete, $model) {
            $this->fireEvent('deleting');

            if ($delete = $model->delete($this)) {
                $this->fireEvent('deleted');
            }

            return $delete;
        });

        return $delete;
    }
}