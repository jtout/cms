<?php

namespace App\Entities;

use App\Facades\Cache;
use Illuminate\Support\Arr;
use App\Events\CommentEvents;
use App\Models\Backend\Users;
use App\Models\Backend\Nodes;
use App\Models\Frontend\Comments;
use App\Observers\Cache as CacheObserver;

class CommentEntity extends Entity
{
    /**
     * @var string
     */
    protected $model = Comments::class;

    /**
     * @var string
     */
    protected $entity_events = CommentEvents::class;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $parent_id;

    /**
     * @var string
     */
    protected $text;

    /**
     * @var int
     */
    protected $node_id;

    /**
     * @var int
     */
    protected $is_active;

    /**
     * @var int
     */
    protected $likes_count;

    /**
     * @var int
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $created_on;

    /**
     * @var int
     */
    protected $updated_by;

    /**
     * @var string
     */
    protected $updated_on;

    /**
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        parent::registerObservers(new CacheObserver);
    }

    /* GETTERS */

    /**
     * @return UserEntity
     */
    public function author() :UserEntity
    {
        $cachedCommentsInfo = Cache::get('comments-info');
        $author = Arr::get($cachedCommentsInfo, $this->id().'.comment-author');

        if (empty($author)) {
            $author = Users::find(['id' => $this->created_by()], ['fields']);
            Arr::set($cachedCommentsInfo, $this->id().'.comment-author', $author);
            Cache::set('comments-info', $cachedCommentsInfo);
        }

        return $author;
    }

    /**
     * @return NodeEntity
     */
    public function article() :NodeEntity
    {
        $cachedCommentsInfo = Cache::get('comments-info');
        $article = Arr::get($cachedCommentsInfo, $this->id().'comment-article');

        if (empty($article)) {
            $article = Nodes::findById($this->node_id());
            Arr::set($cachedCommentsInfo, $this->id().'.comment-article', $article);
            Cache::set('comments-info', $cachedCommentsInfo);
        }

        return $article;
    }

    /**
     * @return bool
     */
    public function is_liked() :bool
    {
        $comments = new Comments();
        $likers = $comments->getLikers($this->id(), true);

        if (in_array(user()->id(), array_keys($likers)))
            return true;

        return false;
    }

    /**
     * @return int
     */
    public function has_children() :int
    {
        $comments = new Comments();
        $children = $comments->getCommentsCount(null, $this->id());

        return $children;
    }

    /**
     * @return int
     */
    public function id() :int
    {
        if (empty($this->id))
            $this->id = (int) 0;

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function parent_id()
    {
        return $this->parent_id;
    }

    /**
     * @param bool $entityDecode
     * @param bool $replaceLineBreak
     * @param string $replaceWith
     * @return string
     */
    public function text($entityDecode = false, $replaceLineBreak = false, $replaceWith = '<br />') :string
    {
        if ($entityDecode) {
            $this->text = html_entity_decode($this->text);
        }
        else {
            $this->text = mb_convert_encoding($this->text, 'HTML-ENTITIES', 'UTF-8');
        }

        if ($replaceLineBreak) {
            $this->text = replace_characters($this->text, "\n", $replaceWith);
        }

        return $this->text;
    }

    /**
     * @return int
     */
    public function node_id() :int
    {
        return $this->node_id;
    }

    /**
     * @return int
     */
    public function is_active() :int
    {
        if (is_null($this->is_active))
            $this->is_active = (int) 0;

        return $this->is_active;
    }

    /**
     * @return int
     */
    public function likes_count() :int
    {
        return $this->likes_count ?? 0;
    }


    /**
     * @return int
     */
    public function created_by() :int
    {
        return $this->created_by;
    }

    /**
     * @param bool $formatted
     * @return string
     */
    public function created_on($formatted = false) :string
    {
        if ($formatted)
        {
            $createdOn = \DateTime::createFromFormat('Y-m-d H:i:s', $this->created_on);
            $this->created_on = $createdOn->format('d M Y H:i:s');
        }

        return $this->created_on;
    }

    /**
     * @return int
     */
    public function updated_by() :int
    {
        return $this->updated_by;
    }

    /**
     * @return string
     */
    public function updated_on() :string
    {
        return $this->updated_on;
    }

    /* SETTERS */

    /**
     * @param $value
     */
    public function setId($value)
    {
        $this->id = $value;
    }

    /**
     * @param $value
     */
    public function setParentId($value)
    {
        $this->parent_id = $value;
    }

    /**
     * @param $value
     */
    public function setText($value)
    {
        $this->text = $value;
    }

    /**
     * @param $value
     */
    public function setNodeId($value)
    {
        $this->node_id = $value;
    }

    /**
     * @param $value
     */
    public function setIsActive($value)
    {
        $this->is_active = $value;
    }

    /**
     * @param $value
     */
    public function setLikesCount($value)
    {
        $this->likes_count = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedBy($value)
    {
        $this->created_by = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedOn($value)
    {
        $this->created_on = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedBy($value)
    {
        $this->updated_by = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedOn($value)
    {
        $this->updated_on = $value;
    }
}