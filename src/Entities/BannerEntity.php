<?php

namespace App\Entities;

use App\Traits\App;
use App\Facades\Cache;
use Illuminate\Support\Arr;
use App\Events\BannerEvents;
use App\Models\Frontend\Nodes;
use App\Models\Backend\Banners;
use App\Observers\Cache as CacheObserver;

class BannerEntity extends Entity
{
    /**
     * @var string
     */
    protected $model = Banners::class;

    /**
     * @var string
     */
    protected $entity_events = BannerEvents::class;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var int
     */
    protected $position_id;

    /**
     * @var string
     */
    protected $position_title;

    /**
     * @var string
     */
    protected $position_alias;

    /**
     * @var int
     */
    protected $client_id;

    /**
     * @var string
     */
    protected $client_title;

    /**
     * @var string
     */
    protected $click_url;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $field;

    /**
     * @var string
     */
    protected $mobile_field;

    /**
     * @var string
     */
    protected $settings;

    /**
     * @var mixed
     */
    protected $nodes;

    /**
     * @var int
     */
    protected $is_active;

    /**
     * @var
     */
    protected $ordering;

    /**
     * @var int
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $created_on;

    /**
     * @var int
     */
    protected $updated_by;

    /**
     * @var string
     */
    protected $updated_on;

    /**
     * @var string
     */
    protected $published_on;

    /**
     * @var string
     */
    protected $finished_on;

    /**
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        parent::registerObservers(new CacheObserver);
    }

    /* GETTERS */

    /**
     * @return int
     */
    public function id() :int
    {
        if (empty($this->id))
            $this->id = (int) 0;

        return $this->id;
    }

    /**
     * @return string
     */
    public function title() :string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function alias() :string
    {
        return $this->alias;
    }

    /**
     * @return string
     */
    public function description() :string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function position_id() :int
    {
        return $this->position_id;
    }

    /**
     * @return string
     */
    public function position_title() :string
    {
        return $this->position_title;
    }

    /**
     * @return string
     */
    public function position_alias() :string
    {
        return $this->position_alias;
    }

    /**
     * @return int
     */
    public function client_id() :int
    {
        return $this->client_id;
    }

    /**
     * @return string
     */
    public function client_title() :string
    {
        return $this->client_title;
    }

    /**
     * @return string
     */
    public function click_url() :string
    {
        return $this->click_url;
    }

    /**
     * @param bool $decode
     * @param bool $timestamp
     * @return string|null
     */
    public function field($decode = false, $timestamp = false) :?string
    {
        if (device_is_mobile() && !device_is_tablet()) {
            $field = $this->mobile_field($decode, $timestamp);
        }

        if (empty($field)) {
            $field = $this->field;

            if ($decode) {
                $field = json_decode($this->field);
            }

            if ($timestamp) {
                $field = App::addTimeStamp($field);
            }
        }

        return empty($field) ? null : $field;
    }

    /**
     * @param bool $decode
     * @param bool $timestamp
     * @return string|null
     */
    public function mobile_field($decode = false, $timestamp = false) :?string
    {
        $field = $this->mobile_field;

        if ($decode) {
            $field = json_decode($this->mobile_field);
        }

        if ($timestamp) {
            $field = App::addTimeStamp($field);
        }

        return empty($field) ? null : $field;
    }

    /**
     * @param bool $decode
     * @return mixed
     */
    public function settings($decode = false)
    {
        $settings = $this->settings;

        if ($decode)
            $settings = json_decode($this->settings, 1);

        return empty($settings) ? null : $settings;
    }

    /**
     * @return string
     */
    public function type() :?string
    {
        return $this->type ?? '';
    }

    /**
     * @return string
     */
    public function nodes() :string
    {
        if ($this->nodes == null)
            $this->nodes = '';

        return $this->nodes;
    }

    /**
     * @return int
     */
    public function is_active() :int
    {
        return $this->is_active;
    }

    /**
     * @return int
     */
    public function ordering() :int
    {
        return $this->ordering;
    }

    /**
     * @return int
     */
    public function created_by() :int
    {
        return $this->created_by;
    }

    /**
     * @return string
     */
    public function created_on() :string
    {
        return $this->created_on;
    }

    /**
     * @return int
     */
    public function updated_by() :int
    {
        return $this->updated_by;
    }

    /**
     * @return string
     */
    public function updated_on() :string
    {
        return $this->updated_on;
    }

    /**
     * @return string
     */
    public function published_on() :string
    {
        return $this->published_on;
    }

    /**
     * @return string
     */
    public function finished_on() :string
    {
        return $this->finished_on;
    }

    /**
     * @return bool
     */
    public function show_title() :bool
    {
        $settings = $this->settings(true);
        $show = !empty($settings) && isset($settings['title']) ? $settings['title'] : false;

        return $show == 'on' ? true : false;
    }

    /**
     * @return string|null
     */
    public function class() :?string
    {
        $settings = $this->settings(true);
        $class = !empty($settings) && isset($settings['class']) ? $settings['class'] : null;

        return $class;
    }

    /**
     * @return bool|null
     */
    public function show_only_in_greece() :?bool
    {
        $settings = $this->settings(true);
        $show = !empty($settings) && isset($settings['show_in_greece']) ? $settings['show_in_greece'] : false;

        return $show == 'on' ? true : false;
    }

    /**
     * @return bool
     */
    public function is_visible() :bool
    {
        if ($this->published_on() <= now() && now() < $this->finished_on() ) {
            return true;
        }

        return  false;
    }

    /**
     * @return BannerEntity
     */
    public function is_visible_by_country() :BannerEntity
    {
        if ($this->show_only_in_greece()) {
            if (get_user_info_by_ip()->iso_code == 'GR') {
                return $this;
            }

            return $this->default();
        }

        return $this;
    }

    /**
     * @return BannerEntity
     */
    public function default() :BannerEntity
    {
        $cachedBannerDefault = Cache::get('banner-default');
        $bannerDefault = Arr::wrap(Arr::get($cachedBannerDefault, $this->position_id()));

        if (empty($bannerDefault)) {
            $bannerDefault = Banners::find(['position_id' => $this->position_id(), 'alias' => 'google', 'client_id' => 4]);

            if ($bannerDefault->id()) {
                Arr::set($cachedBannerDefault, $this->position_id(), $bannerDefault);
                Cache::set('banner-default', $bannerDefault);
            }
        }

        return $bannerDefault;
    }

    /**
     * @param $languageId
     * @return array
     */
    public function article_feed($languageId) :array
    {
        $cachedFeatured = Cache::get('featured');
        $articleFeed = Arr::get($cachedFeatured, $this->id());

        if (empty($articleFeed)) {
            $settings = $this->settings(true);
            $categories = !isset($settings['categories']) ? [0] : $settings['categories'];

            $articleFeed = (new Nodes)->getFeaturedNodes($languageId,  $categories ,3)->toArray();
            Arr::set($cachedFeatured, $this->id(), $articleFeed);
            Cache::set('featured', $cachedFeatured);
        }

        if (empty($articleFeed)) {
            $articleFeed = [];
        }

        return $articleFeed;
    }

    /* SETTERS */
    /**
     * @param $value
     */
    public function setId($value)
    {
        $this->id = $value;
    }

    /**
     * @param $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }

    /**
     * @param $value
     */
    public function setAlias($value)
    {
        $this->alias = $value;
    }

    /**
     * @param $value
     */
    public function setDescription($value)
    {
        $this->description = $value;
    }

    /**
     * @param $value
     */
    public function setPositionId($value)
    {
        $this->position_id = $value;
    }

    /**
     * @param $value
     */
    public function setPositionTitle($value)
    {
        $this->position_title = $value;
    }

    /**
     * @param $value
     */
    public function setPositionAlias($value)
    {
        $this->position_alias = $value;
    }

    /**
     * @param $value
     */
    public function setClientId($value)
    {
        $this->client_id = $value;
    }

    /**
     * @param $value
     */
    public function setClientTitle($value)
    {
        $this->client_title = $value;
    }

    /**
     * @param $value
     */
    public function setClickUrl($value)
    {
        $this->click_url = $value;
    }

    /**
     * @param $value
     */
    public function setField($value)
    {
        $this->field = $value;
    }

    /**
     * @param $value
     */
    public function setMobileField($value)
    {
        $this->mobile_field = $value;
    }

    /**
     * @param $value
     */
    public function setSettings($value)
    {
        $this->settings = $value;
    }

    /**
     * @param $value
     */
    public function setType($value)
    {
        $this->type = $value;
    }

    /**
     * @param $value
     */
    public function setNodes($value)
    {
        $this->nodes = $value;
    }

    /**
     * @param $value
     */
    public function setIsActive($value)
    {
        $this->is_active = $value;
    }

    /**
     * @param $value
     */
    public function setOrdering($value)
    {
        $this->ordering = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedBy($value)
    {
        $this->created_by = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedOn($value)
    {
        $this->created_on = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedBy($value)
    {
        $this->updated_by = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedOn($value)
    {
        $this->updated_on = $value;
    }

    /**
     * @param $value
     */
    public function setPublishedOn($value)
    {
        $this->published_on = $value;
    }

    /**
     * @param $value
     */
    public function setFinishedOn($value)
    {
        $this->finished_on = $value;
    }
}