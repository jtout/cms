<?php

namespace App\Entities;

class SettingsEntity extends Entity
{
    /**
     * @var string
     */
    protected $site_name;

    /**
     * @var string
     */
    protected $language;

    /**
     * @var array
     */
    protected $languages;

    /**
     * @var int
     */
    protected $site_offline;

    /**
     * @var string
     */
    protected $offline_message;

    /**
     * @var string
     */
    protected $system_email;

    /**
     * @var string
     */
    protected $system_email_from_name;

    /**
     * @var string
     */
    protected $system_administrator_email;

    /**
     * @var string
     */
    protected $logo_white;

    /**
     * @var string
     */
    protected $logo_black;

    /**
     * @var string
     */
    protected $allowed_ips;


    /* GETTERS */

    /**
     * @return string
     */
    public function site_name() :string
    {
        return $this->site_name;
    }

    /**
     * @return string
     */
    public function language() :string
    {
        return $this->language;
    }

    /**
     * @return array
     */
    public function languages() :array
    {
        return $this->languages;
    }

    /**
     * @return string
     */
    public function logo_white() :string
    {
        return $this->logo_white;
    }

    /**
     * @return string
     */
    public function logo_black() :string
    {
        return $this->logo_black;
    }

    /**
     * @return int
     */
    public function site_offline() :int
    {
        return $this->site_offline;
    }

    /**
     * @return string
     */
    public function offline_message() :string
    {
        return $this->offline_message ?? '';
    }

    /**
     * @return string
     */
    public function system_email()
    {
        return $this->system_email;
    }

    /**
     * @return string
     */
    public function system_email_from_name()
    {
        return $this->system_email_from_name;
    }

    /**
     * @return string
     */
    public function system_administrator_email()
    {
        return $this->system_administrator_email;
    }

    /**
     * @return string
     */
    public function allowed_ips()
    {
        return $this->allowed_ips;
    }

    /* SETTERS */

    /**
     * @param string $site_name
     */
    public function setSiteName(string $site_name): void
    {
        $this->site_name = $site_name;
    }

    /**
     * @param string $language
     */
    public function setLanguage(string $language): void
    {
        $this->language = $language;
    }

    /**
     * @param array $languages
     */
    public function setLanguages(array $languages): void
    {
        $this->languages = $languages;
    }

    /**
     * @param int $site_offline
     */
    public function setSiteOffline(int $site_offline): void
    {
        $this->site_offline = $site_offline;
    }

    /**
     * @param string $offline_message
     */
    public function setOfflineMessage(string $offline_message): void
    {
        $this->offline_message = $offline_message;
    }

    /**
     * @param string $system_email
     */
    public function setSystemEmail(string $system_email): void
    {
        $this->system_email = $system_email;
    }

    /**
     * @param string $system_email_from_name
     */
    public function setSystemEmailFromName(string $system_email_from_name): void
    {
        $this->system_email_from_name = $system_email_from_name;
    }

    /**
     * @param string $system_administrator_email
     */
    public function setSystemAdministratorEmail(string $system_administrator_email): void
    {
        $this->system_administrator_email = $system_administrator_email;
    }

    /**
     * @param string $logo_white
     */
    public function setLogoWhite(string $logo_white): void
    {
        $this->logo_white = $logo_white;
    }

    /**
     * @param string $logo_black
     */
    public function setLogoBlack(string $logo_black): void
    {
        $this->logo_black = $logo_black;
    }

    /**
     * @param string $allowd_ips
     */
    public function setAllowedIps(string $allowed_ips): void
    {
        $this->allowed_ips = $allowed_ips;
    }
}