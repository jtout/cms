<?php

namespace App\Entities;

use App\Events\AccessLevelEvents;
use App\Models\Backend\AccessLevels;

class AccessLevelEntity extends Entity
{
    /**
     * @var string
     */
    protected $model = AccessLevels::class;

    /**
     * @var string
     */
    protected $entity_events = AccessLevelEvents::class;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $parent_id;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $label;

    /**
     * @var int
     */
    protected $depth;

    /**
     * @var int
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $created_on;

    /**
     * @var int
     */
    protected $updated_by;

    /**
     * @var string
     */
    protected $updated_on;

    /**
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
    }


    /* GETTERS */
    /**
     * @return int
     */
    public function id() :int
    {
        if (empty($this->id))
            $this->id = (int) 0;

        return $this->id;
    }

    /**
     * @return int
     */
    public function parent_id() :int
    {
        if ($this->parent_id == null)
            $this->parent_id = (int) 0;

        return $this->parent_id;
    }

    /**
     * @return string
     */
    public function title() :string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function label() :string
    {
        return $this->label;
    }

    /**
     * @return int
     */
    public function depth() :int
    {
        return $this->depth;
    }

    /**
     * @return int
     */
    public function created_by() :int
    {
        return $this->created_by;
    }

    /**
     * @return string
     */
    public function created_on() :string
    {
        return $this->created_on;
    }

    /**
     * @return int
     */
    public function updated_by() :int
    {
        return $this->updated_by;
    }

    /**
     * @return string
     */
    public function updated_on() :string
    {
        return $this->updated_on;
    }

    /* SETTERS */
    /**
     * @param $value
     */
    public function setId($value)
    {
        $this->id = $value;
    }

    /**
     * @param $value
     */
    public function setParentId($value)
    {
        $this->parent_id = $value;
    }

    /**
     * @param $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }

    /**
     * @param $value
     */
    public function setLabel($value)
    {
        $this->label = $value;
    }

    /**
     * @param $value
     */
    public function setDepth($value)
    {
        $this->depth = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedBy($value)
    {
        $this->created_by = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedOn($value)
    {
        $this->created_on = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedBy($value)
    {
        $this->updated_by = $value;
    }

    /**
     * @param $value
     */
    public function setUpdatedOn($value)
    {
        $this->updated_on = $value;
    }
}