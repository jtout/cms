<?php

namespace App\Entities;

use App\Events\AppLogsEvents;
use App\Models\Backend\Users;
use App\Models\Backend\AppLogs;

class AppLogEntity extends Entity
{
    /**
     * @var string
     */
    protected $model = AppLogs::class;

    /**
     * @var string
     */
    protected $entity_events = AppLogsEvents::class;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $model_instance;

    /**
     * @var string
     */
    protected $info;

    /**
     * @var int
     */
    protected $created_by;

    /**
     * @var string
     */
    protected $created_on;

    /**
     * @return UserEntity
     */
    public function causer() :UserEntity
    {
        $user = Users::findById($this->created_by());

        return $user;
    }

    /* GETTERS */
    /**
     * @return int
     */
    public function id() :int
    {
        if (empty($this->id)) {
            $this->id = (int) 0;
        }

        return $this->id;
    }

    /**
     * @return string
     */
    public function type() :string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function model_instance() :string
    {
        return $this->model_instance;
    }

    /**
     * @return string
     */
    public function info() :string
    {
        return $this->info;
    }

    /**
     * @return int
     */
    public function created_by() :int
    {
        if (empty($this->created_by)) {
            $this->created_by = (int) 0;
        }

        return $this->created_by;
    }

    /**
     * @param bool $format
     * @return string
     */
    public function created_on($format = false) :string
    {
        if ($format) {
            $createdOn = \DateTime::createFromFormat('Y-m-d H:i:s', $this->created_on);
            $createdOnFormatted = $createdOn ? $createdOn->format('d M Y H:i:s') : null;

            $this->created_on = $createdOnFormatted;
        }

        return $this->created_on;
    }


    /* SETTERS */
    /**
     * @param $value
     */
    public function setId($value)
    {
        $this->id = $value;
    }

    /**
     * @param $value
     */
    public function setType($value)
    {
        $this->type = $value;
    }

    /**
     * @param $value
     */
    public function setModelInstance($value)
    {
        $this->model_instance = $value;
    }

    /**
     * @param $value
     */
    public function setInfo($value)
    {
        $this->info = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedBy($value)
    {
        $this->created_by = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedOn($value)
    {
        $this->created_on = $value;
    }
}