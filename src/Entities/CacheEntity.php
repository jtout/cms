<?php

namespace App\Entities;

use App\Models\Backend\Cache;

class CacheEntity extends Entity
{
    /**
     * @var string
     */
    protected $model = Cache::class;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $encoded_key;

    /**
     * @var string
     */
    protected $decoded_key;

    /**
     * @var string
     */
    protected $created_at;


    /* GETTERS */

    /**
     * @return int
     */
    public function id() :int
    {
        if (empty($this->id)) {
            $this->id = (int) 0;
        }

        return $this->id;
    }

    /**
     * @return string
     */
    public function encoded_key() :string
    {
        return $this->encoded_key;
    }

    /**
     * @return string
     */
    public function decoded_key() :string
    {
        return $this->decoded_key;
    }

    /**
     * @return string
     */
    public function created_at() :string
    {
        return $this->created_at;
    }


    /* SETTERS */

    /**
     * @param $value
     */
    public function setId($value)
    {
        $this->id = $value;
    }

    /**
     * @param $value
     */
    public function setEncodedKey($value)
    {
        $this->encoded_key = $value;
    }

    /**
     * @param $value
     */
    public function setDecodedKey($value)
    {
        $this->decoded_key = $value;
    }

    /**
     * @param $value
     */
    public function setCreatedAt($value)
    {
        $this->created_at = $value;
    }
}