<?php

namespace App\Entities;

class ConsoleEntity extends Entity
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var int
     */
    protected $active;


    /* SETTERS */

    /**
     * @return int
     */
    public function id() :int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function title() :string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function active() :int
    {
        return $this->active;
    }

    /* GETTERS */

    /**
     * @param $value
     */
    public function setId($value)
    {
        $this->id = $value;
    }

    /**
     * @param $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }

    /**
     * @param $value
     */
    public function setActive($value)
    {
        $this->active = $value;
    }
}