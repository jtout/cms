<?php

namespace App\Factories;

use App\Entities\CommentEntity;

class CommentFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = CommentEntity::class;

    /**
     * @param null $entity
     * @return CommentEntity
     */
    public static function bootEntity($entity = null) :CommentEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param mixed $options
     * @return CommentEntity
     */
    public static function build($data = array(), $options = null, $commentEntity = null) :CommentEntity
    {
        $data = parent::build($data);
        $comment = self::bootEntity($commentEntity);

        if (isset($data['id'])) {
            $comment->setId((int)$data['id']);
        }

        if (isset($data['parent_id'])) {
            $comment->setParentId((int)$data['parent_id']);
        }

        if (isset($data['text'])) {
            $comment->setText((string)$data['text']);
        }

        if (isset($data['node_id'])) {
            $comment->setNodeId((int)$data['node_id']);
        }

        if (isset($data['is_active'])) {
            $comment->setIsActive((int)$data['is_active']);
        }

        if (isset($data['likes_count'])) {
            $comment->setLikesCount((int)$data['likes_count']);
        }

        if (isset($data['created_by'])) {
            $comment->setCreatedBy((int)$data['created_by']);
        }

        if (isset($data['created_on'])) {
            $comment->setCreatedOn((string)$data['created_on']);
        }

        if (isset($data['updated_by'])) {
            $comment->setUpdatedBy((int)$data['updated_by']);
        }

        if (isset($data['updated_on'])) {
            $comment->setUpdatedOn((string)$data['updated_on']);
        }

        return $comment;
    }
}