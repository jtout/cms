<?php

namespace App\Factories;

use App\Entities\SettingsEntity;

class SettingsFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = SettingsEntity::class;

    /**
     * @param null $entity
     * @return SettingsEntity
     */
    public static function bootEntity($entity = null) :SettingsEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param null $options
     * @param null $settingsEntity
     * @return SettingsEntity
     */
    public static function build($data = array(), $options = null, $settingsEntity = null) :SettingsEntity
    {
        $data = parent::build($data);
        $settings = self::bootEntity($settingsEntity);

        if (isset($data['site_name'])) {
            $settings->setSiteName($data['site_name']);
        }

        if (isset($data['language'])) {
            $settings->setLanguage($data['language']);
        }

        if (isset($data['languages'])) {
            $settings->setLanguages($data['languages']);
        }

        if (isset($data['site_offline'])) {
            $settings->setSiteOffline($data['site_offline']);
        }

        if (isset($data['offline_message'])) {
            $settings->setOfflineMessage($data['offline_message']);
        }

        if (isset($data['system_email'])) {
            $settings->setSystemEmail($data['system_email']);
        }

        if (isset($data['system_email_from_name'])) {
            $settings->setSystemEmailFromName($data['system_email_from_name']);
        }

        if (isset($data['system_administrator_email'])) {
            $settings->setSystemAdministratorEmail($data['system_administrator_email']);
        }

        if (isset($data['logo_white'])) {
            $settings->setLogoWhite($data['logo_white']);
        }

        if (isset($data['logo_black'])) {
            $settings->setLogoBlack($data['logo_black']);
        }

        if (isset($data['allowed_ips'])) {
            $settings->setAllowedIps($data['allowed_ips']);
        }

        return $settings;
    }
}