<?php

namespace App\Factories;

use App\Entities\NodeEntity;

class CategoryFactory extends NodeFactory
{
    /**
     * @return NodeEntity
     */
    public static function create(NodeEntity $node) :NodeEntity
    {
        $node->setModuleId(60);
        $node->setContentTypeId(2);
        $node->setOrder(0);
        $node->setHomepage(0);
        $node->setInBackend(0);
        $node->setIntroText('');

        return $node;
    }
}