<?php

namespace App\Factories;

use App\Entities\LanguageEntity;

class LanguageFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = LanguageEntity::class;

    /**
     * @param null $entity
     * @return LanguageEntity
     */
    public static function bootEntity($entity = null) :LanguageEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param null $options
     * @param null $languageEntity
     * @return LanguageEntity
     */
    public static function build($data = array(), $options = null, $languageEntity = null) :LanguageEntity
    {
        $data = parent::build($data);
        $language = self::bootEntity($languageEntity);

        if (isset($data['id'])) {
            $language->setId((int)$data['id']);
        }

        if (isset($data['title'])) {
            $language->setTitle((string)$data['title']);
        }

        if (isset($data['iso_code'])) {
            $language->setIsoCode((string)$data['iso_code']);
        }

        if (isset($data['is_active'])) {
            $language->setIsActive((int)$data['is_active']);
        }

        if (isset($data['created_by'])) {
            $language->setCreatedBy((int)$data['created_by']);
        }

        if (isset($data['created_on'])) {
            $language->setCreatedOn((string)$data['created_on']);
        }

        if (isset($data['updated_by'])) {
            $language->setUpdatedBy((int)$data['updated_by']);
        }

        if (isset($data['updated_on'])) {
            $language->setUpdatedOn((string)$data['updated_on']);
        }

        return $language;
    }
}