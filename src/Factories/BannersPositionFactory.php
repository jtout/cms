<?php

namespace App\Factories;

use App\Entities\BannersPositionEntity;

class BannersPositionFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = BannersPositionEntity::class;

    /**
     * @param null $entity
     * @return BannersPositionEntity
     */
    public static function bootEntity($entity = null) :BannersPositionEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param null $options
     * @param null $bannerPositionEntity
     * @return BannersPositionEntity
     */
    public static function build($data = array(), $options = null, $bannerPositionEntity = null) :BannersPositionEntity
    {
        $data = parent::build($data);
        $bannerPosition = self::bootEntity($bannerPositionEntity);

        if (isset($data['id'])) {
            $bannerPosition->setId((int)$data['id']);
        }

        if (isset($data['title'])) {
            $bannerPosition->setTitle((string)$data['title']);
        }

        if (isset($data['alias'])) {
            $bannerPosition->setAlias((string)$data['alias']);
        }

        if (isset($data['created_by'])) {
            $bannerPosition->setCreatedBy((int)$data['created_by']);
        }

        if (isset($data['created_on'])) {
            $bannerPosition->setCreatedOn((string)$data['created_on']);
        }

        if (isset($data['updated_by'])) {
            $bannerPosition->setUpdatedBy((int)$data['updated_by']);
        }

        if (isset($data['updated_on'])) {
            $bannerPosition->setUpdatedOn((string)$data['updated_on']);
        }

        return $bannerPosition;
    }
}