<?php

namespace App\Factories;

use App\Entities\UserEntity;

class UserFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = UserEntity::class;

    /**
     * @param null $entity
     * @return UserEntity
     */
    public static function bootEntity($entity = null) :UserEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param null $options
     * @param null $userEntity
     * @return UserEntity
     */
    public static function build($data = array(), $options = null, $userEntity = null) :UserEntity
    {
        $data = parent::build($data);
        $user = self::bootEntity($userEntity);

        if (isset($data['id'])) {
            $user->setId((int)$data['id']);
        }

        if (isset($data['user_name'])) {
            $user->setUserName((string)$data['user_name']);
        }

        if (isset($data['name'])) {
            $user->setName((string)$data['name']);
        }

        if (isset($data['last_name'])) {
            $user->setLastName((string)$data['last_name']);
        }

        if (isset($data['password'])) {
            $user->setPassword((string)$data['password']);
        }

        if (isset($data['role_id'])) {
            $user->setRoleId((int)$data['role_id']);
        }

        if (isset($data['access_level'])) {
            $user->setAccessLevel((string)$data['access_level']);
        }

        if (isset($data['email'])) {
            $user->setEmail((string)$data['email']);
        }

        if (isset($data['is_enabled'])) {
            $user->setIsEnabled((int)$data['is_enabled']);
        }

        if (isset($data['in_backend'])) {
            $user->setInBackend((int)$data['in_backend']);
        }

        if (isset($data['is_activated'])) {
            $user->setIsActivated((int)$data['is_activated']);
        }

        if (isset($data['activation_token'])) {
            $user->setActivationToken($data['activation_token']);
        }

        if (isset($data['privacy_policy'])) {
            $user->setPrivacyPolicy((int)$data['privacy_policy']);
        }

        if (isset($data['created_by'])) {
            $user->setCreatedBy((int)$data['created_by']);
        }

        if (isset($data['created_on'])) {
            $user->setCreatedOn((string)$data['created_on']);
        }

        if (isset($data['updated_by'])) {
            $user->setUpdatedBy((int)$data['updated_by']);
        }

        if (isset($data['updated_on'])) {
            $user->setUpdatedOn((string)$data['updated_on']);
        }

        if (isset($data['activated_on'])) {
            $user->setActivatedOn((string)$data['activated_on']);
        }

        if (isset($data['last_visit_date'])) {
            $user->setLastVisitDate((string)$data['last_visit_date']);
        }

        if (isset($data['avatar'])) {
            $user->setAvatar((string)$data['avatar']);
        }

        if (isset($data['gender'])) {
            $user->setGender((int)$data['gender']);
        }

        if (isset($data['description'])) {
            $user->setDescription((string)$data['description']);
        }

        if (isset($data['birthdate'])) {
            $user->setBirthdate((string)$data['birthdate']);
        }

        if (isset($data['location'])) {
            $user->setLocation((string)$data['location']);
        }

        if (isset($data['twitch'])) {
            $user->setTwitch((string)$data['twitch']);
        }

        if (isset($data['instagram'])) {
            $user->setInstagram((string)$data['instagram']);
        }

        if (isset($data['twitter'])) {
            $user->setTwitter((string)$data['twitter']);
        }

        if (isset($data['facebook'])) {
            $user->setFacebook((string)$data['facebook']);
        }

        if (isset($data['psn_id'])) {
            $user->setPsnId((string)$data['psn_id']);
        }

        if (isset($data['xbox_live_gamertag'])) {
            $user->setXboxLiveGamertag((string)$data['xbox_live_gamertag']);
        }

        if (isset($data['nintendo_id'])) {
            $user->setNintendoId((string)$data['nintendo_id']);
        }

        if (isset($data['steam_id'])) {
            $user->setSteamId((string)$data['steam_id']);
        }

        if (isset($data['pc'])) {
            $user->setPc((string)$data['pc']);
        }

        if (isset($data['tablet'])) {
            $user->setTablet((string)$data['tablet']);
        }

        if (isset($data['smartphone'])) {
            $user->setSmartPhone((string)$data['smartphone']);
        }

        if (isset($data['signature'])) {
            $user->setSignature((string)$data['signature']);
        }

        if (isset($data['like_notifications'])) {
            $user->setLikeNotifications((int)$data['like_notifications']);
        }

        return $user;
    }
}