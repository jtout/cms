<?php

namespace App\Factories;

use App\Entities\ContentTypeEntity;

class ContentTypeFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = ContentTypeEntity::class;

    /**
     * @param null $entity
     * @return ContentTypeEntity
     */
    public static function bootEntity($entity = null) :ContentTypeEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param null $options
     * @param null $contentTypeEntity
     * @return ContentTypeEntity
     */
    public static function build($data = array(), $options = null, $contentTypeEntity = null) :ContentTypeEntity
    {
        $data = parent::build($data);
        $contentType = self::bootEntity($contentTypeEntity);

        if (isset($data['id'])) {
            $contentType->setId((int)$data['id']);
        }

        if (isset($data['title'])) {
            $contentType->setTitle((string)$data['title']);
        }

        if (isset($data['media_folder'])) {
            $contentType->setMediaFolder((string)$data['media_folder']);
        }

        if (isset($data['created_by'])) {
            $contentType->setCreatedBy((int)$data['created_by']);
        }

        if (isset($data['created_on'])) {
            $contentType->setCreatedOn((string)$data['created_on']);
        }

        if (isset($data['updated_by'])) {
            $contentType->setUpdatedBy((int)$data['updated_by']);
        }

        if (isset($data['updated_on'])) {
            $contentType->setUpdatedOn((string)$data['updated_on']);
        }

        return $contentType;
    }
}