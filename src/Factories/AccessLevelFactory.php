<?php

namespace App\Factories;

use App\Entities\AccessLevelEntity;

class AccessLevelFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = AccessLevelEntity::class;

    /**
     * @param null $entity
     * @return AccessLevelEntity
     */
    public static function bootEntity($entity = null) :AccessLevelEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param null $options
     * @param null $accessLevelEntity
     * @return AccessLevelEntity
     */
    public static function build($data = array(), $options = null, $accessLevelEntity = null) :AccessLevelEntity
    {
        $data = parent::build($data);
        $accessLevel = self::bootEntity($accessLevelEntity);

        if (isset($data['id'])) {
            $accessLevel->setId($data['id']);
        }

        if (isset($data['parent_id'])) {
            $accessLevel->setParentId($data['parent_id']);
        }

        if (isset($data['title'])) {
            $accessLevel->setTitle($data['title']);
        }

        if (isset($data['label'])) {
            $accessLevel->setLabel($data['label']);
        }

        if (isset($data['depth'])) {
            $accessLevel->setDepth($data['depth']);
        }

        if (isset($data['created_by'])) {
            $accessLevel->setCreatedBy($data['created_by']);
        }

        if (isset($data['created_on'])) {
            $accessLevel->setCreatedOn($data['created_on']);
        }

        if (isset($data['updated_by'])) {
            $accessLevel->setUpdatedBy($data['updated_by']);
        }

        if (isset($data['updated_on'])) {
            $accessLevel->setUpdatedOn($data['updated_on']);
        }

        return $accessLevel;
    }
}