<?php

namespace App\Factories;

use App\Entities\ClientEntity;

class ClientFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = ClientEntity::class;

    /**
     * @param null $entity
     * @return ClientEntity
     */
    public static function bootEntity($entity = null) :ClientEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param null $options
     * @param null $clientEntity
     * @return ClientEntity
     */
    public static function build($data = array(), $options = null, $clientEntity = null) :ClientEntity
    {
        $data = parent::build($data);
        $client = self::bootEntity($clientEntity);

        if (isset($data['id'])) {
            $client->setId((int)$data['id']);
        }

        if (isset($data['title'])) {
            $client->setTitle((string)$data['title']);
        }

        if (isset($data['description'])) {
            $client->setDescription((string)$data['description']);
        }

        if (isset($data['created_by'])) {
            $client->setCreatedBy((int)$data['created_by']);
        }

        if (isset($data['created_on'])) {
            $client->setCreatedOn((string)$data['created_on']);
        }

        if (isset($data['updated_by'])) {
            $client->setUpdatedBy((int)$data['updated_by']);
        }

        if (isset($data['updated_on'])) {
            $client->setUpdatedOn((string)$data['updated_on']);
        }

        return $client;
    }
}