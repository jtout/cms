<?php

namespace App\Factories;

use App\Entities\NodeEntity;

class ArticleFactory extends NodeFactory
{
    /**
     * @return NodeEntity
     */
    public static function create(NodeEntity $node) :NodeEntity
    {
        $node->setModuleId(79);
        $node->setContentTypeId(1);
        $node->setOrder(0);
        $node->setHomepage(0);
        $node->setInBackend(0);
        $node->setOnMenu(0);

        return $node;
    }
}