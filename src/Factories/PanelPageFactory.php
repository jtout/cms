<?php

namespace App\Factories;

use App\Entities\NodeEntity;

class PanelPageFactory extends NodeFactory
{
    /**
     * @return NodeEntity
     */
    public static function create(NodeEntity $node) :NodeEntity
    {
        $node->setHomepage(0);
        $node->setContentTypeId(3);
        $node->setInBackend(1);
        $node->setLanguageId(1);

        return $node;
    }
}