<?php

namespace App\Factories;

use App\Entities\ModuleEntity;

class ModuleFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = ModuleEntity::class;

    /**
     * @param null $entity
     * @return ModuleEntity
     */
    public static function bootEntity($entity = null) :ModuleEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param mixed $options
     * @return ModuleEntity
     */
    public static function build($data = array(), $options = null, $moduleEntity = null) :ModuleEntity
    {
        $data = parent::build($data);
        $module = self::bootEntity($moduleEntity);

        if (isset($data['id'])) {
            $module->setId((int)$data['id']);
        }

        if (isset($data['parent_id'])) {
            $module->setParentId((int)$data['parent_id']);
        }

        if (isset($data['is_function'])) {
            $module->setIsFunction((int)$data['is_function']);
        }

        if (isset($data['title'])) {
            $module->setTitle((string)$data['title']);
        }

        if (isset($data['class'])) {
            $module->setClass((string)$data['class']);
        }

        if (isset($data['action'])) {
            $module->setAction((string)$data['action']);
        }

        if (isset($data['description'])) {
            $module->setDescription((string)$data['description']);
        }

        if (isset($data['is_active'])) {
            $module->setIsActive((int)$data['is_active']);
        }

        if (isset($data['depth'])) {
            $module->setDepth((int)$data['depth']);
        }

        if (isset($data['created_by'])) {
            $module->setCreatedBy((int)$data['created_by']);
        }

        if (isset($data['created_on'])) {
            $module->setCreatedOn((string)$data['created_on']);
        }

        if (isset($data['updated_by'])) {
            $module->setUpdatedBy((int)$data['updated_by']);
        }

        if (isset($data['updated_on'])) {
            $module->setUpdatedOn((string)$data['updated_on']);
        }

        return $module;
    }
}