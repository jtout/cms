<?php

namespace App\Factories;

use App\Entities\ExtraFieldEntity;

class ExtraFieldFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = ExtraFieldEntity::class;

    /**
     * @param null $entity
     * @return ExtraFieldEntity
     */
    public static function bootEntity($entity = null) :ExtraFieldEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param null $options
     * @param null $extraFieldEntity
     * @return ExtraFieldEntity
     */
    public static function build($data = array(), $options = null, $extraFieldEntity = null) :ExtraFieldEntity
    {
        $data = parent::build($data);
        $extraField = self::bootEntity($extraFieldEntity);

        if (isset($data['id'])) {
            $extraField->setId((int)$data['id']);
        }

        if (isset($data['group_id'])) {
            $extraField->setGroupId((int)$data['group_id']);
        }

        if (isset($data['group_title'])) {
            $extraField->setGroupTitle((string)$data['group_title']);
        }

        if (isset($data['title'])) {
            $extraField->setTitle((string)$data['title']);
        }

        if (isset($data['type'])) {
            $extraField->setType((string)$data['type']);
        }

        if (isset($data['info'])) {
            $extraField->setInfo((string)$data['info']);
        }

        if (isset($data['is_active'])) {
            $extraField->setIsActive((int)$data['is_active']);
        }

        if (isset($data['ordering'])) {
            $extraField->setOrdering((string)$data['ordering']);
        }

        if (isset($data['created_by'])) {
            $extraField->setCreatedBy((int)$data['created_by']);
        }

        if (isset($data['created_on'])) {
            $extraField->setCreatedOn((string)$data['created_on']);
        }

        if (isset($data['updated_by'])) {
            $extraField->setUpdatedBy((int)$data['updated_by']);
        }

        if (isset($data['updated_on'])) {
            $extraField->setUpdatedOn((string)$data['updated_on']);
        }

        return $extraField;
    }
}