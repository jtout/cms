<?php

namespace App\Factories;

use App\Entities\ExtraFieldGroupEntity;

class ExtraFieldGroupFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = ExtraFieldGroupEntity::class;

    /**
     * @param null $entity
     * @return ExtraFieldGroupEntity
     */
    public static function bootEntity($entity = null) :ExtraFieldGroupEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param null $options
     * @param null $extraFieldEntity
     * @return ExtraFieldGroupEntity
     */
    public static function build($data = array(), $options = null, $extraFieldEntity = null) :ExtraFieldGroupEntity
    {
        $data = parent::build($data);
        $extraFieldGroup = self::bootEntity($extraFieldEntity);

        if (isset($data['id'])) {
            $extraFieldGroup->setId((int)$data['id']);
        }

        if (isset($data['title'])) {
            $extraFieldGroup->setTitle((string)$data['title']);
        }

        if (isset($data['created_by'])) {
            $extraFieldGroup->setCreatedBy((int)$data['created_by']);
        }

        if (isset($data['created_on'])) {
            $extraFieldGroup->setCreatedOn((string)$data['created_on']);
        }

        if (isset($data['updated_by'])) {
            $extraFieldGroup->setUpdatedBy((int)$data['updated_by']);
        }

        if (isset($data['updated_on'])) {
            $extraFieldGroup->setUpdatedOn((string)$data['updated_on']);
        }

        return $extraFieldGroup;
    }
}