<?php

namespace App\Factories;

use App\Entities\NodeEntity;

class NodeFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = NodeEntity::class;

    /**
     * @param null $entity
     * @return NodeEntity
     */
    public static function bootEntity($entity = null) :NodeEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param string $contentType
     * @return NodeEntity
     */
    public static function build($data = [], $contentType = '', $nodeEntity = null) :NodeEntity
    {
        $data = parent::build($data);
        $entity = self::bootEntity($nodeEntity);

        if (!empty($contentType)) {
            $factory = '\App\Factories\\'.remove_spaces($contentType).'Factory';
            $node = $factory::create($entity);
        }
        else {
            $node = $entity;
        }

        return self::prepareContent($node, $data);
    }

    /**
     * @param NodeEntity $node
     * @param array $data
     * @return NodeEntity
     */
    public static function prepareContent(NodeEntity $node, $data = []) :NodeEntity
    {
        if (isset($data['id'])) {
            $node->setId((int)$data['id']);
        }

        if (isset($data['name'])) {
            $node->setName((string)$data['name']);
        }

        if (isset($data['path'])) {
            $node->setPath((string)$data['path']);
        }

        if (isset($data['parent_id'])) {
            $node->setParentId((int)$data['parent_id']);
        }

        if (isset($data['view'])) {
            $node->setView((string)$data['view']);
        }

        if (isset($data['module_id'])) {
            $node->setModuleId((int)$data['module_id']);
        }

        if (isset($data['module_title'])) {
            $node->setModuleTitle((string)$data['module_title']);
        }

        if (isset($data['module_action'])) {
            $node->setModuleAction((string)$data['module_action']);
        }

        if (isset($data['module_class'])) {
            $node->setModuleClass((string)$data['module_class']);
        }

        if (isset($data['module_request_type'])) {
            $node->setModuleRequestType((string)$data['module_request_type']);
        }

        if (isset($data['is_active'])) {
            $node->setIsActive((int)$data['is_active']);
        }

        if (isset($data['on_menu'])) {
            $node->setOnMenu((int)$data['on_menu']);
        }

        if (isset($data['in_backend'])) {
            $node->setInBackend((int)$data['in_backend']);
        }

        if (isset($data['created_by'])) {
            $node->setCreatedBy((int)$data['created_by']);
        }

        if (isset($data['created_on'])) {
            $node->setCreatedOn((string)$data['created_on']);
        }

        if (isset($data['updated_by'])) {
            $node->setUpdatedBy((int)$data['updated_by']);
        }

        if (isset($data['updated_on'])) {
            $node->setUpdatedOn((string)$data['updated_on']);
        }

        if (isset($data['title'])) {
            $node->setTitle((string)$data['title']);
        }

        if (isset($data['intro_text'])) {
            $node->setIntroText((string)$data['intro_text']);
        }

        if (isset($data['description'])) {
            $node->setDescription((string)$data['description']);
        }

        if (isset($data['image'])) {
            $node->setImage((string)$data['image']);
        }

        if (isset($data['gallery'])) {
            $node->setGallery((int)$data['gallery']);
        }

        if (isset($data['video'])) {
            $node->setVideo((string)$data['video']);
        }

        if (isset($data['icon'])) {
            $node->setIcon((string)$data['icon']);
        }

        if (isset($data['language_id'])) {
            $node->setLanguageId(((int)$data['language_id']));
        }

        if (isset($data['language_iso_code'])) {
            $node->setLanguageIsoCode(((string)$data['language_iso_code']));
        }

        if (isset($data['content_type_id'])) {
            $node->setContentTypeId((int)$data['content_type_id']);
        }

        if (isset($data['content_type_media_folder'])) {
            $node->setMediaFolder((string)$data['content_type_media_folder']);
        }

        if (isset($data['is_featured'])) {
            $node->setIsFeatured((int)$data['is_featured']);
        }

        if (isset($data['is_sticky'])) {
            $node->setIsSticky((int)$data['is_sticky']);
        }

        if (isset($data['sponsored_by_id'])) {
            $node->setSponsoredById((int)$data['sponsored_by_id']);
        }

        if (isset($data['social_media_title'])) {
            $node->setSocialMediaTitle((string)$data['social_media_title']);
        }

        if (isset($data['meta_title'])) {
            $node->setMetaTitle((string)$data['meta_title']);
        }

        if (isset($data['meta_description'])) {
            $node->setMetaDescription((string)$data['meta_description']);
        }

        if (isset($data['robots'])) {
            $node->setRobots((string)$data['robots']);
        }

        if (isset($data['published_on'])) {
            $node->setPublishedOn((string)$data['published_on']);
        }

        if (isset($data['finished_on'])) {
            $node->setFinishedOn((string)$data['finished_on']);
        }

        if (isset($data['homepage'])) {
            $node->setHomepage((int)$data['homepage']);
        }

        if (isset($data['extra_fields_group_id'])) {
            $node->setExtraFieldsGroupId((int)$data['extra_fields_group_id']);
        }

        if (isset($data['extra_fields_values'])) {
            $node->setExtraFieldsValues((string)$data['extra_fields_values']);
        }

        if (isset($data['comments_lock'])) {
            $node->setCommentLock((int)$data['comments_lock']);
        }

        if (isset($data['depth'])) {
            $node->setDepth((int)$data['depth']);
        }

        if (isset($data['access_level_id'])) {
            $node->setAccessLevelId((int)$data['access_level_id']);
        }

        if (isset($data['order'])) {
            $node->setOrder((int)$data['order']);
        }

        if (isset($data['childs'])) {
            $node->setChilds((array)$data['childs']);
        }

        if (isset($data['translations'])) {
            $node->setTranslations((array)$data['translations']);
        }

        if (isset($data['from_id'])) {
            $node->setFromId((int)$data['from_id']);
        }

        if (isset($data['tags_articles_count'])) {
            $node->setTagsArticlesCount((int) $data['tags_articles_count']);
        }

        if (isset($data['comments'])) {
            $node->setComments((array) $data['comments']);
        }

        return $node;
    }
}