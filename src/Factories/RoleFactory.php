<?php

namespace App\Factories;

use App\Entities\RoleEntity;

class RoleFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = RoleEntity::class;

    /**
     * @param null $entity
     * @return RoleEntity
     */
    public static function bootEntity($entity = null) :RoleEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param null $options
     * @param null $roleEntity
     * @return RoleEntity
     */
    public static function build($data = array(), $options = null, $roleEntity = null) :RoleEntity
    {
        $data = parent::build($data);
        $role = self::bootEntity($roleEntity);

        if (isset($data['id'])) {
            $role->setId((int)$data['id']);
        }

        if (isset($data['parent_id'])) {
            $role->setParentId((int)$data['parent_id']);
        }

        if (isset($data['title'])) {
            $role->setTitle((string)$data['title']);
        }

        if (isset($data['in_backend'])) {
            $role->setInBackend((int)$data['in_backend']);
        }

        if (isset($data['access_level_id'])) {
            $role->setAccessLevelId((int)$data['access_level_id']);
        }

        if (isset($data['access_level'])) {
            $role->setAccessLevel((string)$data['access_level']);
        }

        if (isset($data['access_level_label'])) {
            $role->setAccessLevelLabel((string)$data['access_level_label']);
        }

        if (isset($data['permissions'])) {
            $role->setPermissions($data['permissions']);
        }

        if (isset($data['depth'])) {
            $role->setDepth((int)$data['depth']);
        }

        if (isset($data['created_by'])) {
            $role->setCreatedBy((int)$data['created_by']);
        }

        if (isset($data['created_on'])) {
            $role->setCreatedOn((string)$data['created_on']);
        }

        if (isset($data['updated_by'])) {
            $role->setUpdatedBy((int)$data['updated_by']);
        }

        if (isset($data['updated_on'])) {
            $role->setUpdatedOn((string)$data['updated_on']);
        }

        return $role;
    }
}