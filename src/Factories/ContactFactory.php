<?php

namespace App\Factories;

use App\Entities\ContactEntity;

class ContactFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = ContactEntity::class;

    /**
     * @param null $entity
     * @return ContactEntity
     */
    public static function bootEntity($entity = null) :ContactEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param null $options
     * @param null $contactEntity
     * @return ContactEntity
     */
    public static function build($data = array(), $options = null, $contactEntity = null) :ContactEntity
    {
        $data = parent::build($data);
        $contact = self::bootEntity($contactEntity);

        if (isset($data['id'])) {
            $contact->setId((int)$data['id']);
        }

        if (isset($data['client_id'])) {
            $contact->setClientId((int)$data['client_id']);
        }

        if (isset($data['client_title'])) {
            $contact->setClientTitle((string)$data['client_title']);
        }

        if (isset($data['name'])) {
            $contact->setName((string)$data['name']);
        }

        if (isset($data['email'])) {
            $contact->setEmail((string)$data['email']);
        }

        if (isset($data['phone'])) {
            $contact->setPhone((string)$data['phone']);
        }

        if (isset($data['is_active'])) {
            $contact->setIsActive((int)$data['is_active']);
        }

        if (isset($data['created_by'])) {
            $contact->setCreatedBy((int)$data['created_by']);
        }

        if (isset($data['created_on'])) {
            $contact->setCreatedOn((string)$data['created_on']);
        }

        if (isset($data['updated_by'])) {
            $contact->setUpdatedBy((int)$data['updated_by']);
        }

        if (isset($data['updated_on'])) {
            $contact->setUpdatedOn((string)$data['updated_on']);
        }

        return $contact;
    }
}