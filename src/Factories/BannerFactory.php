<?php

namespace App\Factories;

use App\Entities\BannerEntity;

class BannerFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = BannerEntity::class;

    /**
     * @param null $entity
     * @return BannerEntity
     */
    public static function bootEntity($entity = null) :BannerEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param null $options
     * @param null $bannerEntity
     * @return BannerEntity
     */
    public static function build($data = array(), $options = null, $bannerEntity = null) :BannerEntity
    {
        $data = parent::build($data);
        $banner = self::bootEntity($bannerEntity);

        if (isset($data['id'])) {
            $banner->setId((int)$data['id']);
        }

        if (isset($data['title'])) {
            $banner->setTitle((string)$data['title']);
        }

        if (isset($data['alias'])) {
            $banner->setAlias((string)$data['alias']);
        }

        if (isset($data['description'])) {
            $banner->setDescription((string)$data['description']);
        }

        if (isset($data['position_id'])) {
            $banner->setPositionId((int)$data['position_id']);
        }

        if (isset($data['position_title'])) {
            $banner->setPositionTitle((string)$data['position_title']);
        }

        if (isset($data['position_alias'])) {
            $banner->setPositionAlias((string)$data['position_alias']);
        }

        if (isset($data['client_id'])) {
            $banner->setClientId((int)$data['client_id']);
        }

        if (isset($data['client_title'])) {
            $banner->setClientTitle((string)$data['client_title']);
        }

        if (isset($data['click_url'])) {
            $banner->setClickUrl((string)$data['click_url']);
        }

        if (isset($data['type'])) {
            $banner->setType((string)$data['type']);
        }

        if (isset($data['field'])) {
            $banner->setField((string)$data['field']);
        }

        if (isset($data['mobile_field'])) {
            $banner->setMobileField((string)$data['mobile_field']);
        }

        if (isset($data['settings'])) {
            $banner->setSettings((string)$data['settings']);
        }

        if (isset($data['nodes'])) {
            $banner->setNodes((string)$data['nodes']);
        }

        if (isset($data['is_active'])) {
            $banner->setIsActive((int)$data['is_active']);
        }

        if (isset($data['ordering'])) {
            $banner->setOrdering((int)$data['ordering']);
        }

        if (isset($data['created_by'])) {
            $banner->setCreatedBy((int)$data['created_by']);
        }

        if (isset($data['created_on'])) {
            $banner->setCreatedOn((string)$data['created_on']);
        }

        if (isset($data['updated_by'])) {
            $banner->setUpdatedBy((int)$data['updated_by']);
        }

        if (isset($data['updated_on'])) {
            $banner->setUpdatedOn((string)$data['updated_on']);
        }

        if (isset($data['published_on'])) {
            $banner->setPublishedOn((string)$data['published_on']);
        }

        if (isset($data['finished_on'])) {
            $banner->setFinishedOn((string)$data['finished_on']);
        }

        return $banner;
    }
}