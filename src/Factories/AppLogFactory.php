<?php

namespace App\Factories;

use App\Entities\AppLogEntity;

class AppLogFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = AppLogEntity::class;

    /**
     * @param null $entity
     * @return AppLogEntity
     */
    public static function bootEntity($entity = null) :AppLogEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param null $options
     * @param null $appLogEntity
     * @return AppLogEntity
     */
    public static function build($data = array(), $options = null, $appLogEntity = null) :AppLogEntity
    {
        $data = parent::build($data);
        $appLog = self::bootEntity($appLogEntity);

        if (isset($data['id'])) {
            $appLog->setId($data['id']);
        }

        if (isset($data['type'])) {
            $appLog->setType($data['type']);
        }

        if (isset($data['model_instance'])) {
            $appLog->setModelInstance($data['model_instance']);
        }

        if (isset($data['info'])) {
            $appLog->setInfo($data['info']);
        }

        if (isset($data['created_by'])) {
            $appLog->setCreatedBy($data['created_by']);
        }

        if (isset($data['created_on'])) {
            $appLog->setCreatedOn($data['created_on']);
        }

        return $appLog;
    }
}