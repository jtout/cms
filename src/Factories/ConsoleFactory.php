<?php

namespace App\Factories;

use App\Entities\ConsoleEntity;

class ConsoleFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = ConsoleEntity::class;

    /**
     * @param null $entity
     * @return ConsoleEntity
     */
    public static function bootEntity($entity = null) :ConsoleEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param null $options
     * @param null $consoleEntity
     * @return ConsoleEntity
     */
    public static function build($data = array(), $options = null, $consoleEntity = null) :ConsoleEntity
    {
        $data = parent::build($data);
        $console = self::bootEntity($consoleEntity);

        if (isset($data['id'])) {
            $console->setId((int)$data['id']);
        }

        if (isset($data['title'])) {
            $console->setTitle((string)$data['title']);
        }

        if (isset($data['active'])) {
            $console->setActive((int)$data['active']);
        }

        return $console;
    }
}