<?php

namespace App\Factories;

use App\Entities\NodeEntity;

class UserPageFactory extends NodeFactory
{
    /**
     * @return NodeEntity
     */
    public static function create(NodeEntity $node) :NodeEntity
    {
        $node->setModuleId(205);
        $node->setContentTypeId(6);

        return $node;
    }
}