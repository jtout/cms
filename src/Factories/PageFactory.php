<?php

namespace App\Factories;

use App\Entities\NodeEntity;

class PageFactory extends NodeFactory
{
    /**
     * @return NodeEntity
     */
    public static function create(NodeEntity $node) :NodeEntity
    {
        $node->setModuleId(75);
        $node->setContentTypeId(3);
        $node->setInBackend(0);
        $node->setExtraFieldsGroupId(0);
        $node->setExtraFieldsValues('');
        $node->setIntroText('');

        return $node;
    }
}