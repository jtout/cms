<?php

namespace App\Factories;

use App\Entities\MenuLinkEntity;

class MenuLinkFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = MenuLinkEntity::class;

    /**
     * @param null $entity
     * @return MenuLinkEntity
     */
    public static function bootEntity($entity = null) :MenuLinkEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param mixed $options
     * @return MenuLinkEntity
     */
    public static function build($data = array(), $options = null, $menuLinkEntity = null) :MenuLinkEntity
    {
        $data = parent::build($data);
        $menuLink = self::bootEntity($menuLinkEntity);

        if (isset($data['id'])) {
            $menuLink->setId((int)$data['id']);
        }

        if (isset($data['menu_id'])) {
            $menuLink->setMenuId((int)$data['menu_id']);
        }

        if (isset($data['parent_id'])) {
            $menuLink->setParentId((int)$data['parent_id']);
        }

        if (isset($data['childs'])) {
            $menuLink->setChilds($data['childs']);
        }

        if (isset($data['title'])) {
            $menuLink->setTitle((string)$data['title']);
        }

        if (isset($data['alias'])) {
            $menuLink->setAlias((string)$data['alias']);
        }

        if (isset($data['node_id'])) {
            $menuLink->setNodeId((int)$data['node_id']);
        }

        if (isset($data['is_active'])) {
            $menuLink->setIsActive((int)$data['is_active']);
        }

        if (isset($data['url'])) {
            $menuLink->setUrl((string)$data['url']);
        }

        if (isset($data['class'])) {
            $menuLink->setClass((string)$data['class']);
        }

        if (isset($data['ordering'])) {
            $menuLink->setOrdering((int)$data['ordering']);
        }

        if (isset($data['created_by'])) {
            $menuLink->setCreatedBy((int)$data['created_by']);
        }

        if (isset($data['created_on'])) {
            $menuLink->setCreatedOn((string)$data['created_on']);
        }

        if (isset($data['updated_by'])) {
            $menuLink->setUpdatedBy((int)$data['updated_by']);
        }

        if (isset($data['updated_on'])) {
            $menuLink->setUpdatedOn((string)$data['updated_on']);
        }

        return $menuLink;
    }
}