<?php

namespace App\Factories;

use App\Entities\CacheEntity;

class CacheFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = CacheEntity::class;

    /**
     * @param null $entity
     * @return CacheEntity
     */
    public static function bootEntity($entity = null) :CacheEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param null $options
     * @param null $cacheEntity
     * @return CacheEntity
     */
    public static function build($data = array(), $options = null, $cacheEntity = null) :CacheEntity
    {
        $data = parent::build($data);
        $cache = self::bootEntity($cacheEntity);

        if (isset($data['id'])) {
            $cache->setId((int)$data['id']);
        }

        if (isset($data['encoded_key'])) {
            $cache->setEncodedKey((string)$data['encoded_key']);
        }

        if (isset($data['decoded_key'])) {
            $cache->setDecodedKey((string)$data['decoded_key']);
        }

        if (isset($data['created_at'])) {
            $cache->setCreatedAt((string)$data['created_at']);
        }

        return $cache;
    }
}