<?php

namespace App\Factories;

use App\Entities\NodeEntity;

class SitemapFactory extends NodeFactory
{
    /**
     * @return NodeEntity
     */
    public static function create(NodeEntity $node) :NodeEntity
    {
        $node->setContentTypeId(5);

        return $node;
    }
}