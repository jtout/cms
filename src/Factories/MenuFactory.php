<?php

namespace App\Factories;

use App\Entities\MenuEntity;

class MenuFactory extends EntityFactory
{
    /**
     * @var string
     */
    protected static $entity = MenuEntity::class;

    /**
     * @param null $entity
     * @return MenuEntity
     */
    public static function bootEntity($entity = null) :MenuEntity
    {
        return parent::bootEntity($entity);
    }

    /**
     * @param array $data
     * @param null $options
     * @param null $menuEntity
     * @return MenuEntity
     */
    public static function build($data = array(), $options = null, $menuEntity = null) :MenuEntity
    {
        $data = parent::build($data);
        $menu = self::bootEntity($menuEntity);

        if (isset($data['id'])) {
            $menu->setId((int)$data['id']);
        }

        if (isset($data['title'])) {
            $menu->setTitle((string)$data['title']);
        }

        if (isset($data['alias'])) {
            $menu->setAlias((string)$data['alias']);
        }

        if (isset($data['language_id'])) {
            $menu->setLanguageId((int)$data['language_id']);
        }

        if (isset($data['is_active'])) {
            $menu->setIsActive((int)$data['is_active']);
        }

        if (isset($data['links'])) {
            $menu->setLinks($data['links']);
        }

        if (isset($data['created_by'])) {
            $menu->setCreatedBy((int)$data['created_by']);
        }

        if (isset($data['created_on'])) {
            $menu->setCreatedOn((string)$data['created_on']);
        }

        if (isset($data['updated_by'])) {
            $menu->setUpdatedBy((int)$data['updated_by']);
        }

        if (isset($data['updated_on'])) {
            $menu->setUpdatedOn((string)$data['updated_on']);
        }

        return $menu;
    }
}