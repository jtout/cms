<?php

namespace App\Factories;

use App\Entities\NodeEntity;

class TagFactory extends NodeFactory
{
    /**
     * @return NodeEntity
     */
    public static function create(NodeEntity $node) :NodeEntity
    {
        $node->setModuleId(88);
        $node->setAccessLevelId(5);
        $node->setContentTypeId(4);
        $node->setHomepage(0);
        $node->setInBackend(0);
        $node->setIntroText('');
        $node->setOrder(0);
        $node->setExtraFieldsGroupId(0);
        $node->setExtraFieldsValues('');
        $node->setGallery(0);
        $node->setCommentLock(0);
        $node->setOnMenu(0);
        $node->setIsSticky(0);

        return $node;
    }
}