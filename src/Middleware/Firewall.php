<?php

namespace App\Middleware;

use App\Interfaces\Request;
use App\Facades\DetectAttacks;
use \Psr\Http\Message\ResponseInterface as Response;

final class Firewall
{
    /**
     * @param Request $request
     * @param Response $response
     * @param callable $next
     * @return Response
     */
    public function __invoke(Request $request, Response $response, callable $next) :Response
    {
        if (DetectAttacks::detect()) {
            $result['type'] = $result['info']['type'] = 'warning';
            $result['message'] = $result['info']['message'] = 'Invalid Input.';

            if($request->isXhr()) {
                return $response->withJson($result, 200);
            }
            else {
                flash_message($result['type'], $result['message']);
                return redirect(env('DOMAIN'));
            }
        }

        return $next($request, $response);
    }
}