<?php

namespace App\Middleware;

use App\Traits\App;
use App\Facades\Auth;
use App\Interfaces\Request;
use App\Models\Backend\Users;
use App\Facades\Cookies as SystemCookies;
use \Psr\Http\Message\ResponseInterface as Response;

final class Cookies
{
    /**
     * @param Request $request
     * @param Response $response
     * @param callable $next
     * @return Response
     */
    public function __invoke(Request $request, Response $response, callable $next) :Response
    {
        if ($request->getAttribute('type') != env('ADMIN_NAME') && !empty(SystemCookies::get(SITENAME.'_remember_me'))) {
            if (empty(user()->id())) {
                $cookie = explode("#~#", SystemCookies::get(SITENAME.'_remember_me'));
                $cookie_id    = isset($cookie[0]) ? $cookie[0] : null;
                $cookie_hash  = isset($cookie[1]) ? $cookie[1] : null;
                $hash = hash('sha512', SITENAME.$cookie_id);

                if (!is_numeric($cookie_id)) {
                    SystemCookies::delete(SITENAME.'_remember_me');
                    return $next($request, $response);
                }

                $user = Users::findById(App::sanitizeInput($cookie_id, 'INT'), ['*']);

                if ($cookie_hash === $hash && !empty($user->id())) {
                    $options = array('remember_me' => true);
                    Auth::setUserSession($user, $options);
                }
                else {
                    SystemCookies::delete(SITENAME.'_remember_me');
                }
            }
        }

        return $next($request, $response);
    }
}