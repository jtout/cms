<?php

namespace App\Middleware;

use App\Traits\App;
use App\Facades\Auth;
use App\Interfaces\Request;
use \Psr\Http\Message\ResponseInterface as Response;

final class Authorization
{
    /**
     * @param Request $request
     * @param Response $response
     * @param callable $next
     * @return Response
     */
    public function __invoke(Request $request, Response $response, callable $next) :Response
    {
        $checkPath = App::checkRequestedPath($request);

        if($checkPath['optional'] === env('ADMIN_NAME')) {
            Auth::initializeUserSession(env('ADMIN_NAME'));
            $request = $request->withAttribute('type', env('ADMIN_NAME'));

            if ($request->isAjax()) {
                if (empty(user()->id())) {
                    return $response->withStatus(401)->write('Unauthorized');
                }
            }

            if(empty(user()->id()) && !empty($checkPath['path'])) {
                return $response->withRedirect(env('DOMAIN').'/'.env('ADMIN_NAME'));
            }
        }
        else {
            Auth::initializeUserSession('site');
            $request = $request->withAttribute('type', 'site');

            $allowedIps = explode(',', config()->get('allowed_ips'));
            if(SITEOFFLINE === 1 && !in_array($request->getServerParam('REMOTE_ADDR'), $allowedIps)) {
                return view('offline')->render();
            }
        }

        return $next($request, $response);
    }
}