<?php

namespace App\Middleware;

use App\Traits\App;
use App\Interfaces\Request;
use \Psr\Http\Message\ResponseInterface as Response;

final class GDPR
{
    use App;

    /**
     * @param Request $request
     * @param Response $response
     * @param callable $next
     * @return Response
     */
    public function __invoke(Request $request, Response $response, callable $next) :Response
    {
        if($request->getAttribute('type') != env('ADMIN_NAME')) {
            $path = $request->getUri()->getPath();

            if(!in_array($path, App::gdprPages())) {
                if(isset($request->getQueryParams()['action']) && $request->getQueryParams()['action'] == 'logout') {
                    return $next($request, $response);
                }

                if(!empty(user()->id()) && user()->privacy_policy() == 0) {
                    return $response->withRedirect(env('DOMAIN').'/privacy-policy');
                }
            }
        }

        return $next($request, $response);
    }
}