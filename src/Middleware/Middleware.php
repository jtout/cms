<?php

namespace App\Middleware;

use App\Facades\App;

final class Middleware
{
    public static function boot()
    {
        // Middlewares on routes are executed from bottom to top

        $middlewares = array(
            new CSRF(),
            new GDPR(),
            new Cookies(),
            new Authorization(),
            new Firewall(),
        );

        foreach ($middlewares as $middleware) {
            App::add($middleware);
        }
    }
}