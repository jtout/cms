<?php

namespace App\Traits;

use App\Entities\NodeEntity;

trait Nodes
{
    /**
     * @param NodeEntity $node
     * @param int $backend
     * @return array
     */
    public function getViews(NodeEntity $node, int $backend) :array
    {
        if (strpos($node->module_class(), 'Backend') !== false) {
            $module = str_replace('\App\Controllers\Backend\\', '', $node->module_class());
        }
        else {
            $module = str_replace('\App\Controllers\Frontend\\', '', $node->module_class());
        }

        $directory 	= $backend == 1 ? DIR.'/src/Views/Backend/'.$module : DIR.'/src/Views/Frontend/'.$module;
        $rawViews 	= file_exists($directory) ? array_diff(scandir($directory), array('..', '.')) : array();
        $views		= array();

        foreach($rawViews as $rawView) {
            $views[] = str_replace('.view.php', '', $rawView);
        }

        return $views;
    }

    public function createMenuTree(array $nodes, $selectedNodes = array()) :string
    {
        $html = '';

        foreach ($nodes as $node) {
            if (empty($node->childs())) {
                $html .= '<li data-language="'.$node->language_id().'" data-jstree=\'{"opened":false, "selected":' . ((in_array($node->id(), $selectedNodes) ) ? 'true' : 'false') . '}\' id="'.$node->id().'"><a href="#" >'.$node->title().'</a></li>';
            }
            else {
                $html .= '<li data-language="'.$node->language_id().'" data-jstree=\'{"opened":false, "selected":' . ((in_array($node->id(), $selectedNodes) ) ? 'true' : 'false') . '}\' id="'.$node->id().'"><a href="#" >'.$node->title().'</a>';
                $html .= '<ul role="group" class="jstree-children">';
                $html .= $this->createMenuTree($node->childs()->toArray(), $selectedNodes);
                $html .= '</ul></li>';
            }
        }

        return $html;
    }
}