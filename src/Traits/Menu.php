<?php

namespace App\Traits;

use App\Facades\Cache;
use App\Entities\NodeEntity;
use App\Models\Backend\Nodes;
use App\Models\Frontend\Menus;

trait Menu
{
    /**
     * @param NodeEntity $currentNode
     * @param bool $backend
     * @return array
     */
    protected function getMainMenu(NodeEntity $currentNode, $backend = true) :array
    {
        $nodes = new Nodes();
        $parentId = ($backend === true || $currentNode->view() == 'Backend/login') ? 7 : $nodes->getLanguageParent($currentNode->language_id())->id();
        $userId = user()->id();
        $mainMenu = array();

        if ($backend === true) {
            $backendNodes = Cache::get('backend_nodes');
            if (!isset($backendNodes[$userId]) || empty($backendNodes[$userId])) {
                $backendNodes[$userId] = $nodes->getNodesOnMenu($parentId, $backend);
                Cache::set('backend_nodes', $backendNodes);
            }

            $mainMenu['main_menu'] = $this->createBackendMenuTree($backendNodes[$userId], $currentNode);
        }
        else {
            $frontendNodes = Cache::get('frontend_nodes');
            if (empty($frontendNodes)) {
                $menus = new Menus();
                $frontendNodes = $menus->getMenuLinks(1);
                Cache::set('frontend_nodes', $frontendNodes);
            }

            $mainMenu['main_menu'] = $this->createFrontendMenuTree($frontendNodes, $currentNode);
            $mainMenu['mobile_menu'] = $this->createFrontendMobileMenuTree($frontendNodes, $currentNode);
        }

        return $mainMenu;
    }

    /**
     * @param array $nodes
     * @param NodeEntity $currentNode
     * @return string
     */
    protected function createBackendMenuTree(array $nodes, NodeEntity $currentNode) :string
    {
        $html = '';

        foreach ($nodes as $node) {
            $active = '';
            $arrow = 'arrow';

            if (preg_match('%' . $node->path() . '%', $currentNode->path())) {
                $arrow = 'arrow open';
                $active = 'active open';
            }

            if (empty($node->childs())) {
                if (user()->has_permission_to(($node->module_action())))
                {
                    $html .= '
                        <li class="nav-item ' . $active . '">
                            <a href="' . $node->path() . '" class="nav-link nav-toggle">
                                <i class="' . $node->icon() . '"></i>
                                <span class="title">' . $node->title() . '</span>
                            </a>
                        </li>
                    ';
                }
            }
            else {
                if (user()->has_permission_to(($node->module_action())))
                {
                    $html .= '
                            <li class="nav-item ' . $active . '">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="' . $node->icon() . '"></i>
                                    <span class="title">' . $node->title() . '</span>
                                    <span class="' . $arrow . '"></span>
                                </a>
                        ';

                    $html .= '      <ul class="sub-menu">';
                    $html .= $this->createBackendMenuTree($node->childs(), $currentNode);
                    $html .= '		</ul>';
                    $html .= '</li>';
                }
            }
        }

        return $html;
    }

    /**
     * @param array $menuLinks
     * @param $currentNode
     * @return string
     */
    protected function createFrontendMenuTree(array $menuLinks, $currentNode) :string
    {
        $html = '';

        foreach ($menuLinks as $menuLink) {
            $active = '';

            $aliases = explode('-', $menuLink->alias());
            foreach ($aliases as $alias) {
                if (preg_match('%' . $alias . '%', $currentNode->path())) {
                    $active = 'active';
                    break;
                }
            }

            if (empty($menuLink->childs())) {
                $html .= '
                            <li class="'. $active . ' '.$menuLink->class().'">
                                <a href="' . (empty($menuLink->url()) ? $menuLink->node()->path() : $menuLink->url()) . '">'. $menuLink->title().'</a>
                            </li>
                        ';
            }
            else {
                $html .= '
                        <li class="nav__dropdown ' . $active . ' '.$menuLink->class().'">
                            <a href="javascript:void(0)">'. $menuLink->title() .'</a>
                    ';

                $html .= '      <ul class="nav__dropdown-menu">';
                $html .= $this->createFrontendMenuTree($menuLink->childs(), $currentNode);
                $html .= '		</ul>';
                $html .= '</li>';
            }
        }

        return $html;
    }

    /**
     * @param array $menuLinks
     * @param NodeEntity $currentNode
     * @return string
     */
    protected function createFrontendMobileMenuTree(array $menuLinks, NodeEntity $currentNode) :string
    {
        $html = '';

        foreach ($menuLinks as $menuLink) {
            $active = '';

            if (preg_match('%' . ($menuLink->alias()) . '%', $currentNode->path())) {
                $active = 'active';
            }

            if (empty($menuLink->childs())) {
                $html .= '
                            <li class=" '. $active . '">
                                <a class="sidenav__menu-url" href="' . (empty($menuLink->url()) ? $menuLink->node()->path() : $menuLink->url()) . '">'. $menuLink->title().'</a>
                            </li>
                        ';
            }
            else {
                $html .= '
                        <li class="' . $active . '">
                            <a href="#" class="sidenav__menu-url">'. $menuLink->title() .'</a>
                            <button class="sidenav__menu-toggle" aria-haspopup="true" aria-label="Open dropdown"><i class="ui-arrow-down"></i></button>
                    ';

                $html .= '      <ul class="sidenav__menu-dropdown">';
                $html .= $this->createFrontendMobileMenuTree($menuLink->childs(), $currentNode);
                $html .= '		</ul>';
                $html .= '</li>';

            }
        }

        return $html;
    }
}