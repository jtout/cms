<?php

namespace App\Traits;

use App\Facades\Auth;
use App\Entities\UserEntity;
use App\Exceptions\UnauthorizedAccessException;

trait AuthorizeRequests
{
    /**
     * @param string $action
     * @return bool
     */
    public function authorize(string $action) :bool
    {
       if (!Auth::user()->has_permission_to($action)) {
           throw new UnauthorizedAccessException('Action not allowed!');
       }

       return true;
    }

    /**
     * @param string $action
     * @param UserEntity $user
     * @return bool
     */
    public function authorizeForUser(string $action, UserEntity $user) :bool
   {
       if (!$user->has_permission_to($action)) {
           throw new UnauthorizedAccessException('Action not allowed for user '.$user->username().'!');
       }

       return true;
   }
}