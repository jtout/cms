<?php

namespace App\Traits;

use App\Facades\Validator;
use App\Interfaces\Request;
use App\Requests\FormRequest;
use Illuminate\Validation\Factory;
use App\Exceptions\RequestNotValidException;
use Illuminate\Validation\ValidationException;
use App\Exceptions\UnauthorizedRequestTypeException;

trait ValidatesRequests
{
    /**
     * @param array $data
     * @param array $rules
     * @param array $messages
     * @param array $customAttributes
     * @return array
     */
    public function validate(array $data, array $rules, array $messages = [], array $customAttributes = [])
    {
        try {
            if (empty($rules)) {
                throw new RequestNotValidException('Rules cannot be empty!');
            }

            $factory = $this->getValidationFactory();
            return $factory->validate($data, $rules, $messages, $customAttributes);

        } catch (ValidationException $e) {
            throw new RequestNotValidException($e->validator->errors()->all());
        }
    }

    /**
     * @param FormRequest $request
     * @return bool
     */
    public function validateFormRequest(FormRequest $request)
    {
        if ($request instanceof FormRequest) {
            return $request->validate();
        }
        else {
            throw new RequestNotValidException(['No FormRequest instance detected!']);
        }
    }

    /**
     * @param Request $request
     * @param string $requestedMethod
     * @return bool
     */
    public function validateRequestType(Request $request, string $requestedMethod)
    {
        if ($request->getMethod() != $requestedMethod) {
            throw new UnauthorizedRequestTypeException();
        }

        return true;
    }

    /**
     * @return Factory
     */
    public function getValidationFactory() :Factory
    {
        return Validator::getFactory();
    }
}