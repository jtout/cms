<?php

namespace App\Traits;

trait BannersHelper
{
    /**
     * @param string $type
     * @param array $values
     * @return string
     */
    public function renderBannerType(string $type, array $values) :string
    {
        $html = '';
        $values['field'] = isset($values['field']) ? json_decode($values['field'], 1) : '';
        $values['mobile_field'] = isset($values['mobile_field']) ? json_decode($values['mobile_field'], 1) : '';

        $html .= '<div class="form-group"><br>';

        switch($type)
        {
            case 'image':
                $html .= '<label class="control-label">Desktop</label> <p />
                                  <input id="field" name="field" type="text" value="'.$values['field'].'">
                                  <a href="'.env('DOMAIN').'/media/filemanager/dialog.php?akey='.env('MEDIAKEY').'&amp;type=1&amp;field_id=field\'&amp;fldr=" class="btn default btn-file iframe-btn" type="button">Browse Server</a>
                        <p />';

                $html .= '<label class="control-label">Mobile</label> <p />
                                  <input id="mobile_field" name="mobile_field" type="text" value="'.$values['mobile_field'].'">
                                  <a href="'.env('DOMAIN').'/media/filemanager/dialog.php?akey='.env('MEDIAKEY').'&amp;type=1&amp;field_id=mobile_field\'&amp;fldr=" class="btn default btn-file iframe-btn" type="button">Browse Server</a>
                        <p />';
                break;

            case 'code':
                $html .= '<label class="control-label">Desktop</label> <p />
                                  <textarea rows="11" id="field" class="form-control" name="field">'.$values['field'].'</textarea>
                        <p />';

                $html .= '<label class="control-label">Mobile</label> <p />
                                  <textarea rows="11" id="mobile_field" class="form-control" name="mobile_field">'.$values['mobile_field'].'</textarea>
                        ';
                break;

            default:
                break;
        }

        $html .= '</div>';

        return $html;
    }
}