<?php

namespace App\Traits;

use Illuminate\Support\Collection;

trait ExtraFieldsHelper
{
    /**
     * @param string $type
     * @param $options
     * @return string
     */
    public function renderExtraFieldsTypes(string $type, $options) :string
    {
        $html = '';
        $values = json_decode($options, 1);

        if ($type == 'radio' || $type == 'select' || $type == 'select-multiple')
            $type = 'options';

        switch ($type)
        {
            case 'options':
                $html .= '<div class="repeater form-group mt-repeater">
                                    <div data-repeater-list="options" class="mt-repeater-item mt-overflow">';

                if (!empty($values))
                    foreach ($values as $value)
                        $html .= '
                                 <div style="padding-top:20px;" data-repeater-item>
                                    <label class="control-label">Option</label>
                                    <div class="mt-repeater-cell">
                                        <input type="text" name="text-input" value="'.$value.'" class="form-control mt-repeater-input-inline"/>
                                        <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete mt-repeater-del-right mt-repeater-btn-inline">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </div>
                                 </div>    
                            ';
                else
                    $html .= '
                                 <div style="padding-top:20px;" data-repeater-item>
                                    <label class="control-label">Option</label>
                                    <div class="mt-repeater-cell">
                                        <input type="text" name="text-input" value="" class="form-control mt-repeater-input-inline"/>
                                        <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete mt-repeater-del-right mt-repeater-btn-inline">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </div>
                                 </div>    
                            ';

                $html .= '</div><a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add">
                                <i class="fa fa-plus"></i> Add new option</a>
                              </div>';

                break;

            default:
                $html = '';
                break;
        }


        return $html;
    }

    /**
     * @param Collection $extraFields
     * @param string $values
     * @return string
     */
    public function renderExtraFields(Collection $extraFields, string $values) :string
    {
        $html = '';
        $values = !empty($values) ? json_decode($values, 1) : array();

        foreach ($extraFields as $extraField)
        {
            $html .= '<div class="form-group" style="margin-bottom:30px"><strong>'.$extraField->title().'</strong><br>';

            switch($extraField->type())
            {
                case 'text':
                    $html .= '<label class="control-label"></label>
                                  <input class="form-control" type="text" name="extra_fields_values['.$extraField->id().']" value="'.(isset($values[$extraField->id()]) ? $values[$extraField->id()] : '').'"/>
                        ';
                    break;

                case 'textarea':
                    $html .= '<label class="control-label"></label>
                                  <textarea rows="8" class="form-control" name="extra_fields_values['.$extraField->id().']">'.(isset($values[$extraField->id()]) ? $values[$extraField->id()] : '').'</textarea>
                        ';
                    break;

                case 'radio':
                    $options = json_decode($extraField->info(), 1);

                    $html .= '<div class="btn-group" data-toggle="buttons"><br>';

                    foreach ($options as $option => $value)
                        $html .= '<label class="btn btn-default '.(isset($values[$extraField->id()]) && $option == $values[$extraField->id()] ? 'active' : '').'">'.$value.'
                                        <input type="radio" class="toggle" name="extra_fields_values['.$extraField->id().']" value="'.$option.'" '.(isset($values[$extraField->id()]) && $option == $values[$extraField->id()] ? 'checked' : '').'/>
                                      </label>
                            ';

                    $html .= '</div>';
                    break;

                case 'select':
                    $options = json_decode($extraField->info(), 1);

                    $html .= '<label class="control-label"></label>
                                  <select class="extra_field_select" name="extra_fields_values['.$extraField->id().'][]">
                        ';

                    foreach ($options as $option => $value)
                        $html .= '<option value="'.$option.'" '.(isset($values[$extraField->id()]) && $option == $values[$extraField->id()] ? 'selected' : '').'>'.$value.'</option>';

                    $html .= '</select>';

                    break;

                case 'select-multiple':
                    $options = json_decode($extraField->info(), 1);

                    $html .= '<label class="control-label"></label>
                                  <select class="extra_field_select_multiple" name="extra_fields_values['.$extraField->id().'][]" multiple>
                        ';

                    foreach ($options as $option => $value)
                        $html .= '<option value="'.$option.'" '.(isset($values[$extraField->id()]) && in_array($option, $values[$extraField->id()]) ? 'selected' : '').'>'.$value.'</option>';

                    $html .= '</select>';
                    break;

                case 'image':
                    $html .= '<label class="control-label"></label>
                                  <input id="extra_field_image" name="extra_fields_values['.$extraField->id().']" type="text" value="'.(isset($values[$extraField->id()]) ? $values[$extraField->id()] : '').'">
                                  <a href="'.env('DOMAIN').'/media/filemanager/dialog.php?akey='.env('MEDIAKEY').'&amp;type=1&amp;field_id=extra_field_image\'&amp;fldr=" class="btn default btn-file iframe-btn" type="button">Browse Server</a>
                        ';
                    break;

                case 'date':
                    $html .= '<label class="control-label"></label>
                                  <input class="form-control form-control-inline input-medium extra-field-date-picker" name="extra_fields_values['.$extraField->id().']" size="16" type="text" value="'.(isset($values[$extraField->id()]) ? $values[$extraField->id()] : date('d-m-Y',time())).'">
                        ';
                    break;

                default:
                    break;
            }

            $html .= '</div>';
        }

        return $html;
    }

    /**
     * @param Collection $extraFields
     * @param string $values
     * @return array
     */
    public function getNodeExtraFieldsValues(Collection $extraFields, string $values) :array
    {
        $values = !empty($values) ? json_decode($values, 1) : array();
        $results = array();

        foreach ($extraFields as $extraField) {
            if ($extraField && !is_null($extraField->id())) {
                $results[$extraField->id()]['id'] = $extraField->id();
                $results[$extraField->id()]['class'] = str_replace(' ','-', strtolower($extraField->title()));
                $results[$extraField->id()]['title'] = $extraField->title();
                $results[$extraField->id()]['value'] = '';

                if ($extraField->type() == 'radio')
                {
                    $options = json_decode($extraField->info(), 1);
                    foreach ($options as $option => $value)
                    {
                        if ((isset($values[$extraField->id()]) && $option == $values[$extraField->id()]))
                        {
                            if ($extraField->id() == 25)
                            {
                                $badge = App::badgesRating()[$values[$extraField->id()]];

                                $results[$extraField->id()]['value'] = $badge;
                            }
                            else
                            {
                                $results[$extraField->id()]['value'] = $value;
                            }
                        }
                    }
                }
                else if ($extraField->type() == 'select-multiple' || $extraField->type() == 'select')
                {
                    $options = json_decode($extraField->info(), 1);
                    $multiValues = array();

                    foreach ($options as $option => $value)
                        if ((isset($values[$extraField->id()]) && in_array($option, $values[$extraField->id()])))
                            $multiValues[] = $value;

                    $results[$extraField->id()]['value'] = implode(',', $multiValues);
                }
                else {
                    $results[$extraField->id()]['value'] = isset($values[$extraField->id()]) ? str_replace("\n", '<br />', $values[$extraField->id()]) : '';
                }
            }
        }

        return $results;
    }

    /**
     * @return array
     */
    public function extraFieldsTypes() :array
    {
        $types = array(
            'text' => 'Text Field',
            'textarea' => 'Textarea Field',
            'radio' => 'Radio Buttons',
            'select' => 'Select Box',
            'select-multiple' => 'Multi Select Box',
            'image' => 'Image Field',
            'date' => 'Date Field'
        );

        return $types;
    }
}