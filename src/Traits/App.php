<?php

namespace App\Traits;

use App\Interfaces\Request;
use App\Models\Backend\Roles;
use App\Models\Backend\Modules;
use App\Models\Backend\AccessLevels;
use Illuminate\Pagination\LengthAwarePaginator;
use Spatie\ImageOptimizer\OptimizerChainFactory;

trait App
{
    /**
     * @param Request $request
     * @return array
     */
    public static function checkRequestedPath(Request $request) :array
    {
        $response['optional']   = isset($request->getAttribute('routeInfo')[2]['optional']) ? $request->getAttribute('routeInfo')[2]['optional'] : '';
        $response['path']  		= isset($request->getAttribute('routeInfo')[2]['path']) ? $request->getAttribute('routeInfo')[2]['path'] : '';
        $response['parameters'] = $request->getQueryParams();

        return $response;
    }

    /**
     * @param $accessLevel
     * @return string
     */
    public static function authorizedAccessLevels($accessLevel) :string
    {
        $accessLevelsModel = new AccessLevels();
        $accessLevels = $accessLevelsModel->getAuthorizedAccessLevels($accessLevel);

        return $accessLevels;
    }

    /**
     * @param int $role
     * @return array
     */
    public static function authorizedRoles(int $role) :array
    {
        $rolesModel = new Roles();
        $roles = $rolesModel->getAuthorizedRoles($role);

        return explode('/', rtrim($roles, '/'));
    }

    /**
     * @param $items
     * @param int $perPage
     * @param int $page
     * @return LengthAwarePaginator
     */
    public static function paginateResults($items, $perPage = 15, $page = 1) :LengthAwarePaginator
    {
        $collection = collect($items);
        $page = session()->get('page') ?? $page;
        $limit = session()->get('limit') ?? $perPage;

        $paginator = (new LengthAwarePaginator(
            $collection->forPage($page, $limit), count($items), $limit, $page, ['pageName' => 'p'])
        )
            ->setPath(request()->getUri()->getPath())
            ->appends(request()->getQueryParams());

        $paginator = self::renderPagination($paginator);

        return $paginator;
    }

    /**
     * @param LengthAwarePaginator $paginator
     * @return LengthAwarePaginator
     */
    public static function renderPagination(LengthAwarePaginator $paginator) :LengthAwarePaginator
    {
        $paginator->fullPagination = view('Backend/System/pagination.full.view', ['paginator' => $paginator])->raw();
        $paginator->simplePagination = view('Backend/System/pagination.simple.view', ['paginator' => $paginator])->raw();
        $paginator->onlyNextPagination = view('Backend/System/pagination.only-next.view', ['paginator' => $paginator])->raw();

        return $paginator;
    }

    /**
     * @param Request $request
     * @return string
     */
    public static function buildPath(Request $request) :string
    {
        $language = LANGUAGE;
        $arguments = $request->getAttribute('route')->getArguments();
        $checkOptional  = isset($request->getAttribute('routeInfo')[2]['optional']) ? $request->getAttribute('routeInfo')[2]['optional'] : '';

        if ($checkOptional === env('ADMIN_NAME')) {
            $language = 'en';
        }

        $optional =  isset($arguments['optional']) ? $arguments['optional'] : '';
        $path	  =  !empty($request->getAttribute('path')) ? $request->getAttribute('path')	: '';

        if (empty($optional)) {
            $path = '/'.$language.'/';
        }
        else if (!empty($optional) && empty($path)) {
            $path = in_array($optional, array_keys(LANGUAGES)) && $optional != $language ? '/'.$optional.'/' : '/'.$language.'/'.$optional.'/';
        }
        else if (!empty($optional) && !empty($path)) {
            $path = in_array($optional, array_keys(LANGUAGES)) && $optional != $language ? '/'.$optional.'/'.$path.'/' : '/'.$language.'/'.$optional.'/'.$path.'/';
        }

        return self::sanitizeInput($path);
    }

    /**
     * @param $path
     * @return string|null
     */
    public static function parsePath($path)
    {
        $parsedPath = null;
        $restOfPath = null;
        $language = LANGUAGE;
        $explodedPath = explode('/', $path);
        $optional = isset($explodedPath[1]) ? $explodedPath[1] : LANGUAGE;

        if (count($explodedPath) > 3) {
            for($i = 2; $i < count($explodedPath); $i++) {
                $restOfPath .= '/'.$explodedPath[$i];
            }
        }

        if (isset($explodedPath[2]) && $explodedPath[2] === env('ADMIN_NAME')) {
            $language = 'en';
        }

        if ($optional == $language && empty($restOfPath)) {
            $parsedPath = '/';
        }
        else if ($optional == $language && !empty($restOfPath)) {
            $parsedPath = rtrim($restOfPath, '/');
        }
        else if (in_array($optional, array_keys(LANGUAGES)) && $optional != $language && empty($restOfPath)) {
            $parsedPath = rtrim('/'.$optional, '/');
        }
        else if (in_array($optional, array_keys(LANGUAGES)) && $optional != $language && !empty($restOfPath)) {
            $parsedPath = rtrim('/'.$optional.$restOfPath, '/');
        }

        return $parsedPath;
    }

    /**
     * @param $input
     * @param string|null $type
     * @return mixed
     */
    public static function sanitizeInput($input, $type = null)
    {
        switch ($type)
        {
            case 'INT':
                $input = filter_var($input, FILTER_SANITIZE_NUMBER_INT);
                break;

            case 'EMAIL':
                $input = filter_var($input, FILTER_SANITIZE_EMAIL);
                break;

            default:
                $input = filter_var($input, FILTER_SANITIZE_STRING);
                break;
        }

        $input = self::clean($input, false);

        return $input;
    }

    /**
     * @param $input
     * @param bool $allowTags
     * @return string
     */
    public static function clean($input, $allowTags = true)
    {
        $allowableTags = array (
            '<div>',
            '<p>',
            '<a>',
            '<br>',
            '<i>',
            '<b>',
            '<strong>',
            '<img>',
            '<em>',
            '<blockquote>',
            '<span>',
            '<ul>',
            '<li>',
            '<ol>',
            '<h1>',
            '<h2>',
            '<h3>',
            '<h4>',
            '<h5>',
            '<h6>',
            '<sup>',
            '<sub>',
            '<code>',
            '<pre>',
            '<table>',
            '<tbody>',
            '<thead>',
            '<th>',
            '<tr>',
            '<td>',
            '<object>',
            '<iframe>',
            '<hr>',
            '<button>'
        );

        $detagged 	= $allowTags ? strip_tags($input, implode('', $allowableTags)) : strip_tags($input);
        $deslashed 	= stripslashes($detagged);
        $pattern 	= '/(?:%|\\\\)u([0-9a-f]{3,4})/i';
        $deslashed  = str_replace( "+", "&#43;", $deslashed );
        $deslashed 	= preg_replace( $pattern, "&#x\\1;", urldecode( $deslashed ));

        return html_entity_decode( $deslashed, null, 'UTF-8' );
    }

    /**
     * @param $str
     * @param array $options
     * @return false|mixed|string|string[]|null
     */
    public static function makeSefUrl($str, $options = array())
    {
        // Make sure string is in UTF-8 and strip invalid UTF-8 characters
        $str = mb_convert_encoding((string)$str, 'UTF-8', mb_list_encodings());

        $defaults = array(
            'delimiter' => '-',
            'limit' => null,
            'lowercase' => true,
            'replacements' => array(),
            'transliterate' => true,
        );

        // Merge options
        $options = array_merge($defaults, $options);

        $char_map = array(
            // Latin
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
            'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
            'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
            'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
            'ß' => 'ss',
            'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
            'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
            'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
            'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
            'ÿ' => 'y',

            // symbols
            '©'  => '(c)',
            ':'  => '-',
            '-'  => '-',
            '_'  => '-',
            '/'  => '-',
            '\\' => '-',
        );

        // Make custom replacements
        //$str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

        // Transliterate characters to ASCII
        if ($options['transliterate']) {
            $str = str_replace(array_keys($char_map), $char_map, $str);
            $str = preg_replace(
                array(
                    "%\&amp\;|(\s+)|,|_|/|\\\|:%ui","%\.|\&\#039\;|\&\#39\;|\&\#34\;|\&quot\;|\§%",
                    '%γγ%ui', '%γξ%ui', '%γκ%ui', '%γχ%ui', '%-μπ%ui', '%ντ%ui',
                    '%αι|αί%ui',
                    '%α((υ|ύ|ϋ|ΰ)(β|γ|δ|ζ|λ|μ|ν|ρ|α|ε|η|ι|ο|υ))%ui',
                    '%((α|ά)υ(θ|κ|ξ|π|σ|τ|φ|χ|ψ|-))%ui',
                    '%ει|εί%ui',
                    '%ε((υ|ύ|ϋ|ΰ)(β|γ|δ|ζ|λ|μ|ν|ρ|α|ε|η|ι|ο|υ))%ui',
                    '%((ε|έ)υ(θ|κ|ξ|π|σ|τ|φ|χ|ψ|-))%ui',
                    '%οι|οί%ui', '%ου|ού%ui', '%υι|υί%ui',
                    '%α|ά%ui','%β%ui','%γ%ui','%δ%ui','%ε|έ%ui','%ζ%ui','%η|ή%ui','%θ%ui','%ι|ί|ϊ|ΐ%ui',
                    '%κ%ui','%λ%ui','%μ%ui','%ν%ui','%ξ%ui','%ο|ό%ui','%π%ui','%ρ%ui','%σ|ς%ui','%τ%ui',
                    '%υ|ύ|ϋ|ΰ%ui','%φ%ui','%χ%ui','%ψ%ui','%ω|ώ%ui'
                ),
                array(
                    '-', '',
                    'ng', 'nx', 'gk', 'nch', '-b', 'nt',
                    'ai', 'av$3', 'af$3', 'ei', 'ev$3', 'ef$3', 'oi', 'ou', 'yi',
                    'a','v','g','d','e','z','i','th','i','k','l','m','n','x','o','p','r','s','t',
                    'y','f','ch','ps','o'
                ),
                '-'.$str
            );
            $str = mb_strtolower( preg_replace(
                array( "%[^\w\-]+%ui", "%(^-+)|(-+$)%", '%--+%' ),
                array( '','','-' ),
                iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $str)),
                'UTF-8');
        }

        // Replace non-alphanumeric characters with our delimiter
        $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

        // Remove duplicate delimiters
        $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

        // Truncate slug to max. characters
        $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

        // Remove delimiter from ends
        $str = trim($str, $options['delimiter']);

        return $test = $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
    }

    /**
     * @param $key
     * @param $value
     * @param $type
     */
    public static function setFilter($key, $value, $type)
    {
        $key = session()->getNode()->name().".".$key;

        if (!empty($value) && !is_array($value)) {
            switch ($type) {
                case 'INT':
                    $value = self::sanitizeInput((int)$value, 'INT');
                    break;
                case 'STR':
                    $value = self::sanitizeInput(trim($value), 'STRING');
                    break;
                default:
                    $value = self::sanitizeInput(trim($value));
                    break;
            }

            session()->set($key, $value);
        }
        elseif (empty($value)) {
            session()->unset($key);
        }
    }

    /**
     * @param array $permissions
     * @return string
     */
    public static function createPermissionsHtmlList($permissions = array()) :string
    {
        $modules = new Modules();
        $items = $modules->getModules();

        $data = function($item, array $permissions) {
            $dt = 'data-id="' . $item->id() . '" data-jstree=\'{"opened": false, "selected":' . ((in_array($item->id(), $permissions) && $item->is_function() == 1) ? 'true' : 'false') . ', "icon" : "fa fa-bolt"}\'';

            return $dt;
        };

        $value = function($item) {
            return '<a id="' . $item->id() . '" href="#">' . htmlentities($item->title()) . '</a>';
        };

        $html = App::createHtmlTree($items, $permissions, $data, $value);

        return $html;
    }

    /**
     * @param $dir
     */
    public static function removeDirectory($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir") {
                        App::removeDirectory($dir . "/" . $object);
                    }
                    else
                        unlink($dir . "/" . $object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    /**
     * @param $imagePath
     */
    public static function optimizeImage($imagePath)
    {
        $optimizerChain = OptimizerChainFactory::create();
        $optimizerChain->optimize($imagePath);
    }

    /**
     * @param $imagePath
     * @param $width
     * @param $height
     * @throws \ImagickException
     */
    public static function imageResize($imagePath, $width, $height)
    {
        $thumb = new \Imagick(realpath($imagePath));
        $thumb->resizeImage($width, $height, \Imagick::FILTER_LANCZOS, 1, TRUE);
        $thumb->setImageFormat("jpg");
        $thumb->setImageCompression(\Imagick::COMPRESSION_JPEG2000);
        $thumb->setImageCompressionQuality(80);
        $thumb->stripImage();
        $thumb->writeImage($imagePath);
        $thumb->destroy();
    }

    /**
     * @param $items
     * @param $selected_items
     * @param $fnData
     * @param $fnValue
     * @return string
     */
    public static function createHtmlTree($items, $selected_items, $fnData, $fnValue)
    {
        $html = '';
        $depth = 0;

        foreach ($items as $index => $item) {

            if ($item->id() == 1) continue;

            $newDepth = intval($item->depth());

            $data = $fnData($item, $selected_items);
            $value = $fnValue($item, $selected_items);

            if ($newDepth > $depth) {
                while ($newDepth > $depth) {
                    $html .= '<ul class="list-unstyled"><li ' . $data . '>';
                    $depth++;
                }
            }
            else if ($newDepth < $depth) {
                while ($newDepth < $depth) {
                    $html .= '</li></ul>';
                    $depth--;
                }

                if ($index === 0)
                    $html .= '<ul class="list-unstyled"><li ' . $data . '>';
                else
                    $html .= '</li><li ' . $data . '>';

            }
            else if ($newDepth === $depth) {
                if ($index === 0)
                    $html .= '<ul class="list-unstyled"><li ' . $data . '>';
                else
                    $html .= '</li><li ' . $data . '>';
            }

            $html .= $value;
            $depth = $newDepth;
        }

        if (count($items) > 0) {
            while ($depth-- >= 0)
                $html .= '</li></ul>';
        }

        return $html;
    }

    /**
     * @return array
     */
    public static function appLogsTypes() :array
    {
        return array(
            0 => 'SQL Injection',
            1 => 'XSS Attack',
            2 => 'Spam',
            3 => 'Changes'
        );
    }

    /**
     * @return array
     */
    public static function gdprPages() :array
    {
        return array(
            '/privacy-policy',
            '/oroi-xrisis'
        );
    }

    /**
     * @return array
     */
    public static function genders() :array
    {
        return array(
            0 => '',
            1 => 'Male',
            2 => 'Female'
        );
    }

    /**
     * @param $field
     * @return mixed
     */
    public static function addTimeStamp($field)
    {
        $field = str_replace('timestamp', timestamp(), $field);

        return $field;
    }

    /**
     * @param $string
     * @param string $to
     * @param string $from
     * @return false|string|string[]|null
     */
    public static function convertEncoding($string, $to = 'HTML-ENTITIES', $from = 'UTF-8')
    {
        return mb_convert_encoding($string, $to, $from);
    }
}