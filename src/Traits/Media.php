<?php

namespace App\Traits;

use App\Interfaces\Request;
use App\Entities\UserEntity;
use App\Entities\NodeEntity;
use App\Exceptions\RequestNotValidException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

trait Media
{
    public $moved = false;

    /**
     * @param Request $request
     * @param NodeEntity $node
     * @throws \ImagickException
     */
    public function uploadFiles(Request $request, NodeEntity $node)
    {
        $uploadedFiles = $request->getUploadedFiles();

        if (isset($uploadedFiles['image']) && $uploadedFiles['image']->getError() === UPLOAD_ERR_OK || $request->inputs('imageServer')) {
            $image = $request->inputs('imageServer') ? $this->createImageUploadObject($request->inputs('imageServer')) : $uploadedFiles['image'];
            $upload = $request->inputs('imageServer') ? false : true;

            $uploadImage = $this->uploadImage($image, $node, $upload);

            if ($uploadImage === false) {
                flash_message('danger', 'Image could not be uploaded!');
            }
        }

        if (isset($uploadedFiles['gallery']) && $uploadedFiles['gallery']->getError() === UPLOAD_ERR_OK) {
            $uploadGallery = $this->uploadGallery($uploadedFiles['gallery'], $node);

            if ($uploadGallery['success'] === false) {
                flash_message('danger', $uploadGallery['message']);
            }
        }
    }

    /**
     * @param UploadedFile $image
     * @param NodeEntity $node
     * @param bool $upload
     * @return bool
     * @throws \ImagickException
     */
    public function uploadImage(UploadedFile $image, NodeEntity $node, bool $upload) :bool
    {
        $nodeId = $node->id();
        $folder = $node->content_type()->media_folder();
        $sourceDir  = env('MEDIA_DIRECTORY').'/source/'.$folder.'/'.$nodeId.'/images/';
        $thumbsDir  = env('MEDIA_DIRECTORY').'/thumbs/'.$folder.'/'.$nodeId.'/images/';
        App::removeDirectory($sourceDir);
        App::removeDirectory($thumbsDir);

        $directoryName  	= env('MEDIA_DIRECTORY').'/source/'.$folder.'/'.$nodeId.'/images/originalImage';
        $smallImgPath   	= env('MEDIA_DIRECTORY').'/source/'.$folder.'/'.$nodeId.'/images/smallImage';
        $bigImgPath 		= env('MEDIA_DIRECTORY').'/source/'.$folder.'/'.$nodeId.'/images/bigImage';
        $bigTileImgPath 	= env('MEDIA_DIRECTORY').'/source/'.$folder.'/'.$nodeId.'/images/bigTileImage';
        $recentNewsImgPath 	= env('MEDIA_DIRECTORY').'/source/'.$folder.'/'.$nodeId.'/images/recentNewsImage';

        mkdir($directoryName, 0755, true);
        mkdir($smallImgPath,  0755, true);
        mkdir($bigImgPath,	  0755, true);
        mkdir($bigTileImgPath,0755, true);
        mkdir($recentNewsImgPath,	  0755, true);
        $imageName  = str_replace(' ','-', trim($image->getClientOriginalName()));
        $imageName = str_replace('"', "", $imageName);
        $imageName = str_replace("'", "", $imageName);
        $imageName = App::sanitizeInput($imageName, 'STRING');
        $destination = $directoryName .'/'. $imageName;

        if ($upload === true) {
            try {
                $image->move(realpath($directoryName), $imageName);
                App::optimizeImage($destination);
                $this->moved = true;
            }
            catch (\Exception $e) {
                flash_message('danger', $e->getMessage());
            }
        }
        else {
            if (copy($image->getPath().'/'.$image->getFilename(), $destination)) {
                App::optimizeImage($destination);
                $this->moved = true;
            }
        }

        if ($this->moved === true) {
            // small image width 300
            if (copy($destination, $smallImgPath.'/'.$imageName)) {
                App::imageResize($smallImgPath.'/'.$imageName, 300, 165);
            }

            // big tile image width 600px
            if (copy($destination, $bigTileImgPath.'/'.$imageName)) {
                App::imageResize($bigTileImgPath.'/'.$imageName, 600, 500);
            }

            // big image width 600px
            if (copy($destination, $bigImgPath.'/'.$imageName)) {
                App::imageResize($bigImgPath.'/'.$imageName, 800, 465);
            }

            // recent news image width 380px
            if (copy($destination, $recentNewsImgPath.'/'.$imageName)) {
                App::imageResize($recentNewsImgPath.'/'.$imageName, 480, 240);
            }
        }

        return $this->moved;
    }

    /**
     * @param UploadedFile $gallery
     * @param NodeEntity $node
     * @return array
     * @throws \ImagickException
     */
    public function uploadGallery(UploadedFile $gallery, NodeEntity $node) :array
    {
        $nodeId = $node->id();
        $folder = $node->content_type()->media_folder();
        $filename = App::sanitizeInput($gallery->getClientOriginalName(), 'STRING');
        $response = array(
            'success' => true
        );

        $targetDir = env('MEDIA_DIRECTORY').'/source/'.$folder.'/'.$nodeId.'/'.'gallery_temp';
        if (!is_dir($targetDir)) {
            mkdir($targetDir, 0755, true);
        }

        $target_path = env('MEDIA_DIRECTORY').'/source/'.$folder.'/'.$nodeId.'/'.'gallery_temp/'.$filename;

        if ($gallery->move(realpath($targetDir), $filename)) {
            $zip 	 = new \ZipArchive();
            $openZip = $zip->open($target_path);

            if ($openZip === true) {
                $allowedExtensions  = array('jpg', 'jpeg', 'png', 'gif', '');
                for ($i = 0; $i < $zip->numFiles; $i++) {
                    $stat 		= $zip->statIndex($i);
                    $extension  = pathinfo($stat['name'], PATHINFO_EXTENSION);

                    if (!in_array($extension, $allowedExtensions)) {
                        $zip->close();
                        unlink($target_path);

                        // Delete gallery_temp
                        $sourceDirTemp  = env('MEDIA_DIRECTORY').'source/'.$folder.'/'.$nodeId.'/'.'gallery_temp';
                        $thumbsDirTemp  = env('MEDIA_DIRECTORY').'/thumbs/'.$folder.'/'.$nodeId.'/'.'gallery_temp';
                        App::removeDirectory($sourceDirTemp);
                        App::removeDirectory($thumbsDirTemp);

                        $response['success'] = false;
                        $response['message']= 'Gallery not uploaded. Allowed extensions are: jpg, jpeg, png and gif';

                        return $response;
                    }
                }

                // Remove existing gallery photos
                $sourceDir  	= env('MEDIA_DIRECTORY').'/source/'.$folder.'/'.$nodeId.'/gallery/';
                $thumbsDir  	= env('MEDIA_DIRECTORY').'/thumbs/'.$folder.'/'.$nodeId.'/gallery/';
                App::removeDirectory($sourceDir);
                App::removeDirectory($thumbsDir);

                // Extract zip to gallery
                $zip->extractTo($sourceDir);
                $zip->extractTo($thumbsDir);
                $images = scandir($thumbsDir);

                if (!empty($images[2])) {
                    for($i = 2; $i < count($images); $i++ ) {
                        if ($images[$i] != '__MACOSX') {
                            $images[$i]= $thumbsDir.'/'.$images[$i];
                            App::imageResize($images[$i], 150, 150);
                        }
                    }

                    $emptyDirThumbs  = $sourceDir.'__MACOSX';
                    $emptyDirSource  = $thumbsDir.'__MACOSX';
                    App::removeDirectory($emptyDirThumbs);
                    App::removeDirectory($emptyDirSource);
                }

                $zip->close();
                unlink($target_path);

                // Delete gallery_temp
                $sourceDirTemp  = env('MEDIA_DIRECTORY').'/source/'.$folder.'/'.$nodeId.'/gallery_temp';
                $thumbsDirTemp  = env('MEDIA_DIRECTORY').'/thumbs/'.$folder.'/'.$nodeId.'/gallery_temp';
                App::removeDirectory($sourceDirTemp);
                App::removeDirectory($thumbsDirTemp);
            }
            else {
                $response['success'] = false;
                $response['message']= 'Could not open zip file!';
            }
        }
        else {
            $response['success'] = false;
            $response['message']= 'Could not upload zip file.';
        }

        return $response;
    }

    /**
     * @param string $file
     * @return UploadedFile
     */
    public function createImageUploadObject(string $file) :UploadedFile
    {
        $fileLocation = str_replace(env('DOMAIN'), DIR.'/public_html', $file);
        $fileLocation = str_replace('%20',' ', $fileLocation);
        $imageArray = explode('/', $file);
        $image = str_replace('%20',' ', end($imageArray));
        $size  = getimagesize($fileLocation);

        $uploadedFile = new UploadedFile(realpath($fileLocation), $image, $size['mime'], 0);

        return $uploadedFile;
    }

    /**
     * @param UploadedFile $avatar
     * @param UserEntity $user
     * @return bool
     * @throws \ImagickException
     */
    public function uploadUserImage(UploadedFile $avatar, UserEntity $user) :bool
    {
        $image = str_replace(' ','-', trim($avatar->getClientOriginalName()));
        $image = str_replace('"', "", $image);
        $image = str_replace("'", "", $image);
        $image = App::sanitizeInput($image, 'STRING');
        $directoryName = env('MEDIA_DIRECTORY').'/source/Profiles/'.$user->id().'/images/originalImage';
        $directoryNameThumbs  = env('MEDIA_DIRECTORY').'/thumbs/Profiles/'.$user->id().'/images/originalImage';

        App::removeDirectory($directoryName);
        App::removeDirectory($directoryNameThumbs);
        mkdir($directoryName, 0755, true);
        mkdir($directoryNameThumbs, 0755, true);

        try {
            $avatar->move(realpath($directoryName), $image);
            $this->moved = true;
        }
        catch (\Exception $e) {
            throw new RequestNotValidException($e->getMessage());
        }

        if (copy($directoryName.'/'.$image, $directoryNameThumbs.'/'.$image)) {
            App::imageResize($directoryNameThumbs.'/'.$image, 50, 50);
        }
        else {
            throw new RequestNotValidException('Image could not be copied to thumbs directory!');
        }

        return $this->moved;
    }

    /**
     * @param int $id
     */
    public function deleteGallery(int $id)
    {
        $sourceDirTemp  = env('MEDIA_DIRECTORY').'/source/Articles/'.$id.'/'.'gallery';
        $thumbsDirTemp  = env('MEDIA_DIRECTORY').'/thumbs/Articles/'.$id.'/'.'gallery';
        App::removeDirectory($sourceDirTemp);
        App::removeDirectory($thumbsDirTemp);
    }

    /**
     * @return array
     */
    public function imageAllowedTypes()
    {
        return array(
            'image/png',
            'image/jpg',
            'image/jp2',
            'image/jpeg',
            'image/gif'
        );
    }

    /**
     * @return array
     */
    public function fileAllowedTypes()
    {
        return array(
            'application/zip',
            'application/x-zip-compressed',
            'multipart/x-zip',
            'application/x-compressed',
            'application/octet-stream',
            'application/x-rar-compressed'
        );
    }

    /**
     * @param null $source
     * @return array
     */
    public static function videoSources($source = null) :array
    {
        return array(
            'HTML' => $source,
            'YouTube' => "<div class=\"embed-responsive embed-responsive-16by9\"><iframe class=\"embed-responsive-item\" src=\"https://www.youtube.com/embed/{$source}\" allowfullscreen=\"true\" frameborder=\"0\" scrolling=\"no\" title=\"Unboxholics Player\"></iframe></div>",
            'Facebook' => "<div class=\"embed-responsive embed-responsive-16by9\"><iframe class=\"embed-responsive-item\" src=\"https://www.facebook.com/plugins/video.php?href={$source}&show_text=0\"  allowfullscreen=\"true\" frameborder=\"0\" scrolling=\"no\" title=\"Unboxholics Player\"></iframe></div>",
            "Vimeo" => "<div class=\"embed-responsive embed-responsive-16by9\"><iframe class=\"embed-responsive-item\" src=\"https://player.vimeo.com/video/{$source}\" allowfullscreen=\"true\" frameborder=\"0\" scrolling=\"no\" title=\"Unboxholics Player\"></iframe></div>",
            "Dailymotion" => "<div class=\"embed-responsive embed-responsive-16by9\"><iframe class=\"embed-responsive-item\" src=\"https://www.dailymotion.com/embed/video/{$source}\" allowfullscreen=\"true\" frameborder=\"0\" scrolling=\"no\" title=\"Unboxholics Player\"></iframe></div>",
        );
    }

    /**
     * @param $source
     * @param $video
     * @return string
     */
    public static function encodeVideoSource($source, $video) :string
    {
        if ($source != 'HTML') {
            $encoded =  '{'.$source.'}'.$video.'{/'.$source.'}';
        }
        else {
            $encoded = $video;
        }

        return $encoded;
    }

    /**
     * @param $video
     * @param $source
     * @return string
     */
    public static function stripVideoSourcesTags($video, $source) :string
    {
        $stripped = str_replace('{'.$source.'}','', $video);
        return str_replace('{/'.$source.'}', '', $stripped);
    }

    /**
     * @param $video
     * @return string
     */
    public static function checkVideoSource($video)
    {
        $source = 'HTML';
        foreach (array_keys(self::videoSources()) as $videoSource) {
            if (strpos($video, $videoSource) !== false) {
                $source =  $videoSource;
                break;
            }
        }

        return $source;
    }
}