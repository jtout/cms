<?php

namespace App\Requests;

class InsertAccessLevel extends FormRequest
{
    /**
     * @var string
     */
    protected $session = 'insert_access_level';

    /**
     * @var string
     */
    protected $message = 'Access Level added!';

    /**
     * @return array
     */
    public function data() :array
    {
        return [
            'parent_id' => $this->inputs('parent_id'),
            'title' => $this->inputs('title'),
            'label' => $this->inputs('label'),
            'created_by' => user()->id(),
            'updated_by' => user()->id(),
            'updated_on' => now(),
            'created_on' => now()
        ];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
          'title' => 'required',
          'label' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'title.required' => 'Title cannot be empty!',
            'label.required' => 'Label cannot be empty!',
        ];
    }
}