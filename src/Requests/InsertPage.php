<?php

namespace App\Requests;


class InsertPage extends InsertNode
{
    /**
     * @var string
     */
    protected $session = 'insert_page';

    /**
     * @var string
     */
    protected $message = 'Page added!';

    /**
     * @return array
     */
    public function data() :array
    {
        $data = parent::data();

        $data['view'] = 'Frontend/Pages/'.$this->inputs('view');

        return $data;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = parent::rules();

        $rules['parent_id'] = 'required|not_in:0';

        return $rules;
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        $messages = parent::messages();

        $messages['parent_id.required'] = 'Category cannot be empty!';
        $messages['parent_id.not_in'] = 'Category cannot be empty!';

        return $messages;
    }
}