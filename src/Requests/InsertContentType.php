<?php

namespace App\Requests;


class InsertContentType extends FormRequest
{
    /**
     * @var string
     */
    protected $session = 'insert_content_type';

    /**
     * @var string
     */
    protected $message = 'Content Type added!';

    /**
     * @return array
     */
    public function data() :array
    {
        return [
            'title' => $this->inputs('title'),
            'updated_by' => user()->id(),
            'updated_on' => now(),
            'created_on' => now(),
            'created_by' => user()->id()
        ];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'title.required' => 'Title cannot be empty!'
        ];
    }
}