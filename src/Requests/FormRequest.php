<?php

namespace App\Requests;

use App\Facades\Validator;
use App\Interfaces\Validation;
use App\Services\RequestService as Request;
use App\Exceptions\RequestNotValidException;

abstract class FormRequest extends Request implements Validation
{
    /**
     * @var string
     */
    protected $session;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var string
     */
    protected $messageType;

    /**
     * @var mixed
     */
    protected $validatedData;

    /**
     * @return bool
     */
    protected $galleryUploaded;

    /**
     * @return bool
     */
    protected $imageUploaded;

    /**
     * @return array
     */
    abstract public function data() :array;

    /**
     * @return array
     */
    abstract public function rules() :array;

    /**
     * @return array
     */
    abstract public function messages() :array;

    /**
     * @return bool
     */
    public function validate() :bool
    {
        $data = $this->data();
        $this->validatedData = $data;

        if (!empty($this->session)) {
            session()->set($this->session, $this->getValidatedData());
        }

        $validator = Validator::make($data, $this->rules(), $this->messages());

        if ($validator->fails()) {
            throw new RequestNotValidException($validator->errors()->all());
        }

        if (!empty($this->message) && !$this->isAjax()) {
            $type = empty($this->messageType) ? 'success' : $this->messageType;
            flash_message($type, $this->message);
        }

        if (!empty($this->session)) {
           session()->unset($this->session);
        }

        return true;
    }

    /**
     * @return mixed
     */
    public function getValidatedData()
    {
        return $this->validatedData;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getMessageType()
    {
        return $this->messageType;
    }

    /**
     * @return string
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @return bool
     */
    public function getImageUploaded()
    {
        return $this->imageUploaded;
    }

    /**
     * @return bool
     */
    public function getGalleryUploaded()
    {
        return $this->galleryUploaded;
    }
}