<?php

namespace App\Requests;

use Carbon\Carbon;

class InsertUser extends FormRequest
{
    /**
     * @var string
     */
    protected $session = 'insert_user';

    /**
     * @var string
     */
    protected $message = 'User added!';

    /**
     * @return array
     */
    public function data() :array
    {
        return  [
            'user_name' => $this->inputs('user_name'),
            'name' => $this->inputs('name'),
            'last_name' => $this->inputs('last_name'),
            'email' => $this->inputs('email', 'EMAIL'),
            'password' => $this->inputs('password'),
            'password_confirmation' => $this->inputs('password_confirmation'),
            'in_backend' => $this->has('in_backend') ? 1 : 0,
            'is_enabled' => $this->has('is_enabled') ? 1 : 0,
            'is_activated' => $this->has('is_activated') ? 1 : 0,
            'privacy_policy' => $this->has('privacy_policy') ? 1 : 0,
            'role_id' => $this->inputs('role_id'),
            'gender' => $this->inputs('gender', 'INT'),
            'location' => $this->inputs('location'),
            'birthdate' => !empty($this->inputs('birthdate')) ? Carbon::parse($this->inputs('birthdate'))->format('Y-m-d') : '',
            'description' => $this->inputs('description'),
            'facebook' => $this->inputs('facebook'),
            'twitter' => $this->inputs('twitter'),
            'instagram' => $this->inputs('instagram'),
            'twitch' => $this->inputs('twitch'),
            'psn_id' => $this->inputs('psn_id'),
            'xbox_live_gamertag' => $this->inputs('xbox_live_gamertag'),
            'nintendo_id' => $this->inputs('nintendo_id'),
            'steam_id' => $this->inputs('steam_id'),
            'pc' => $this->inputs('pc'),
            'tablet' => $this->inputs('tablet'),
            'smartphone' => $this->inputs('smartphone'),
            'relations'  => ['permissions' => $this->inputs('selected_permissions')],
            'signature'  => $this->inputs('signature'),
            'updated_by' => user()->id(),
            'updated_on' => now(),
            'created_on' => now(),
            'created_by' => user()->id(),
            'last_visit_date' => now()
        ];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_name' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'role_id' => 'required',
            'relations.permissions' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'user_name.required' => 'Username cannot be empty!',
            'user_name.unique' => 'Username already exists in our database!',
            'email.required' => 'Email cannot be empty!',
            'email.unique' => 'Email already exists in our database!',
            'email.email' => 'Email not valid!',
            'password.required' => 'Password cannot be empty!',
            'password.confirmed' => 'Passwords do not match!',
            'role_id.required' => 'Role cannot be empty!',
            'relations.permissions.required' => 'Permissions cannot be empty!'
        ];
    }
}