<?php

namespace App\Requests;

use Illuminate\Support\Arr;

class UpdateContact extends InsertContact
{
    /**
     * @var string
     */
    protected $session = '';

    /**
     * @var string
     */
    protected $message = 'Contact updated!';

    /**
     * @return array
     */
    public function data() :array
    {
        $data = parent::data();

        Arr::forget($data, ['created_on', 'created_by']);
        Arr::set($data, 'id', $this->inputs('contact_id', 'INT'));

        return $data;
    }
}