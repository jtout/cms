<?php

namespace App\Requests;

use Carbon\Carbon;
use App\Traits\App;
use App\Traits\Media;

class UpdateSiteProfile extends FormRequest
{
    use Media;

    /**
     * @var string
     */
    protected $session = '';

    /**
     * @var string
     */
    protected $message = 'Profile updated!';

    /**
     * @var true
     */
    private $profileDeleted = false;

    /**
     * @return array
     */
    public function data() :array
    {
        if ($this->has('delete_profile') && $this->inputs('delete_profile') == true) {

            $this->profileDeleted = true;
            $this->message = 'You successfully deleted your account!';

            return [
                'user_name' => uniqid(),
                'name' => '',
                'last_name' => '',
                'email' => uniqid(),
                'current_email' => '',
                'password' => '',
                'repeat_password' => '',
                'in_backend' => 0,
                'is_enabled' => 0,
                'is_activated' => 0,
                'activation_token' => '',
                'privacy_policy' => 0,
                'role_id' => 6,
                'gender' => 0,
                'signature' => '',
                'avatar' => '',
                'new_avatar' => '',
                'facebook' => '',
                'twitter' => '',
                'instagram' => '',
                'twitch' => '',
                'psn_id' => '',
                'xbox_live_gamertag' => '',
                'nintendo_id' => '',
                'steam_id' => '',
                'updated_by' => user()->id()
            ];
        }
        else {
            $this->imageUploaded = isset($this->uploadedFiles['new_avatar']) && $this->uploadedFiles['new_avatar']->getError() === UPLOAD_ERR_OK ? true : false;

            $data =  [
                'id' => $this->inputs('id', 'INT'),
                'name' => $this->inputs('name'),
                'last_name' => $this->inputs('last_name'),
                'email' => $this->inputs('email', 'EMAIL'),
                'password' => $this->inputs('password'),
                'password_confirmation' => $this->inputs('password_confirmation'),
                'privacy_policy' => $this->has('privacy_policy') ? 1 : 0,
                'gender' => $this->inputs('gender', 'INT'),
                'location' => $this->inputs('location'),
                'birthdate' => !empty($this->inputs('birthdate')) ? Carbon::parse($this->inputs('birthdate'))->format('Y-m-d') : '',
                'description' => $this->inputs('description'),
                'facebook' => $this->inputs('facebook'),
                'twitter' => $this->inputs('twitter'),
                'instagram' => $this->inputs('instagram'),
                'twitch' => $this->inputs('twitch'),
                'psn_id' => $this->inputs('psn_id'),
                'xbox_live_gamertag' => $this->inputs('xbox_live_gamertag'),
                'nintendo_id' => $this->inputs('nintendo_id'),
                'steam_id' => $this->inputs('steam_id'),
                'signature'  => $this->inputs('signature'),
                'updated_by' => user()->id(),
                'updated_on' => now()
            ];

            if ($this->imageUploaded) {
                $data['avatar'] = $this->uploadedFiles['new_avatar'];
            }

            return $data;
        }
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        if ($this->profileDeleted) {
            return [];
        }

        $data = $this->data();

        $rules = [
            'email' => 'required|email|unique:users,email,'.$data['id'],
        ];

        if (!empty($this->inputs('password'))) {
            $rules['password'] = 'required|confirmed';
        }

        if ($this->imageUploaded) {
            $rules['avatar'] = [
                'max:1024',
                'mimetypes:'.implode(',', $this->imageAllowedTypes())
            ];
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'email.required' => 'Email cannot be empty!',
            'email.unique' => 'Email already exists in our database!',
            'email.email' => 'Email not valid!',
            'password.required' => 'Password cannot be empty!',
            'password.confirmed' => 'Passwords do not match!',
            'avatar.max' => 'Image cannot be more than 1mb!',
            'avatar.mimetypes' => 'Only jpg, jpeg, png and gifs are allowed!'
        ];
    }

    /**
     * @return mixed
     */
    public function getValidatedData()
    {
        // Set avatar name if image upload is true
        if ($this->getImageUploaded()) {
            $image = App::clean($this->uploadedFiles['new_avatar']->getClientOriginalName());
            $image = replace_characters($image,' ','-');
            $this->validatedData['avatar'] = $image;
        }

        return $this->validatedData;
    }
}