<?php

namespace App\Requests;

class InsertExtraFieldGroup extends FormRequest
{
    /**
     * @param string
     */
   protected $session = 'insert_extra_fields_groups';

    /**
     * @param string
     */
    protected $message = 'Extra Fields Group added!';

    /**
     * @return array
     */
    public function data() :array
    {
        return [
            'title' =>  $this->inputs('title'),
            'updated_by' =>  user()->id(),
            'updated_on' => now(),
            'created_on' => now(),
            'created_by' => user()->id()
        ];
    }

    /**
     * @return array
     */
    public function rules() :array
    {
        return [
            'title' =>  'required'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'title.required' =>  'Title cannot be empty!',
        ];
    }
}