<?php

namespace App\Requests;

class InsertClient extends FormRequest
{
    /**
     * @var string
     */
    protected $session = 'insert_client';

    /**
     * @var string
     */
    protected $message = 'Client added!';

    /**
     * @return array
     */
    public function data() :array
    {
        return array(
            'title' => $this->inputs('title'),
            'description' => $this->inputs('description'),
            'updated_by' => user()->id(),
            'updated_on' => now(),
            'created_on' => now(),
            'created_by' => user()->id()
        );
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'title.required' => 'Title cannot be empty!'
        ];
    }
}