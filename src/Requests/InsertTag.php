<?php

namespace App\Requests;

use Illuminate\Support\Arr;
use App\Models\Backend\Nodes;

class InsertTag extends InsertNode
{
    /**
     * @var string
     */
    protected $session = 'insert_tag';

    /**
     * @var string
     */
    protected $message = 'Tag added!';

    /**
     * @return array
     */
    public function data() :array
    {
        $nodes = new Nodes();
        $data = parent::data();

        $data['view'] = 'Frontend/Tags/'.$this->inputs('view');
        $data['parent_id'] = $nodes->getLanguageParent($this->inputs('language_id'))->id();

        Arr::forget($data, ['access_level_id']);

        return $data;
    }
}