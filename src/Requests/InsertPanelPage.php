<?php

namespace App\Requests;

class InsertPanelPage extends InsertNode
{
    /**
     * @var string
     */
    protected $session = 'insert_panel_page';

    /**
     * @var string
     */
    protected $message = 'Panel page added!';

    /**
     * @return array
     */
    public function data() :array
    {
        $data = parent::data();
        $data['view'] = 'Backend/'.$this->inputs('view');
        $data['module_id'] = $this->inputs('module_id', 'INT');

        return $data;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = parent::rules();

        $rules['parent_id'] = 'required|not_in:0';

        return $rules;
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        $messages = parent::messages();

        $messages['parent_id.required'] = 'Category cannot be empty!';
        $messages['parent_id.not_in'] = 'Category cannot be empty!';

        return $messages;
    }
}