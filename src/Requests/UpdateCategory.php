<?php

namespace App\Requests;

use Illuminate\Support\Arr;

class UpdateCategory extends InsertCategory
{
    /**
     * @var string
     */
    protected $session = '';

    /**
     * @var string
     */
    protected $message = 'Category updated!';

    /**
     * @return array
     */
    public function data() :array
    {
        $data = parent::data();

        Arr::forget($data, ['created_on', 'created_by']);
        Arr::set($data, 'id', $this->inputs('id', 'INT'));

        if ($this->has('delete_gallery')) {
            $this->deleteGallery($data['id']);
        }

        return $data;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        $rules['name'] = 'required|unique:nodes,name,'.$this->inputs('id');

        return $rules;
    }
}