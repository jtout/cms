<?php

namespace App\Requests;


class InsertCategory extends InsertNode
{
    /**
     * @var string
     */
    protected $session = 'insert_category';

    /**
     * @var string
     */
    protected $message = 'Category added!';

    /**
     * @return array
     */
    public function data() :array
    {
        $data = parent::data();

        $data['extra_fields_group_id'] = $this->inputs('extra_fields_group_id');
        $data['view'] = 'Frontend/Categories/'.$this->inputs('view');

        return $data;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = parent::rules();

        $rules['parent_id'] = 'required|not_in:0';

        return $rules;
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        $messages = parent::messages();

        $messages['parent_id.required'] = 'Category cannot be empty!';
        $messages['parent_id.not_in'] = 'Category cannot be empty!';

        return $messages;
    }
}