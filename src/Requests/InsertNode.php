<?php

namespace App\Requests;

use App\Traits\App;
use App\Traits\Media;

class InsertNode extends FormRequest
{
    use Media;

    /**
     * @return array
     */
    public function data(): array
    {
        $this->imageUploaded = isset($this->uploadedFiles['image']) && $this->uploadedFiles['image']->getError() === UPLOAD_ERR_OK ? true : false;
        $this->galleryUploaded = isset($this->uploadedFiles['gallery']) && $this->uploadedFiles['gallery']->getError() === UPLOAD_ERR_OK ? true : false;

        $data = [
            'parent_id' => $this->inputs('parent_id', 'INT'),
            'name' => !$this->inputs('alias') ? App::makeSefUrl($this->inputs('title')) : App::makeSefUrl($this->inputs('alias')),
            'access_level_id' => $this->inputs('access_level_id'),
            'is_active' => $this->has('is_active') ? 1 : 0,
            'is_featured' => $this->has('is_featured') ? 1 : 0,
            'is_sticky' => $this->has('is_sticky') ? 1 : 0,
            'on_menu' => $this->has('on_menu') ? 1 : 0,
            'title' => $this->inputs('title'),
            'intro_text' => $this->inputs('intro_text'),
            'description' => App::clean($this->inputs('description', false)),
            'video' => '',
            'created_on' => empty($this->inputs('created_on')) ? now() : $this->inputs('created_on'),
            'published_on' => $this->inputs('published_on'),
            'finished_on' => $this->inputs('finished_on'),
            'created_by' => $this->inputs('created_by'),
            'updated_by' => user()->id(),
            'updated_on' => now(),
            'social_media_title' => $this->inputs('social_media_title') ? $this->inputs('social_media_title') : $this->inputs('title'),
            'meta_title' => $this->inputs('meta_title') ? $this->inputs('meta_title') : $this->inputs('title'),
            'meta_description' => $this->inputs('meta_description') ? $this->inputs('meta_description') : $this->inputs('description'),
            'robots' => $this->inputs('robots'),
            'view' => $this->inputs('view'),
            'extra_fields_values' => $this->inputs('extra_fields_values') ? json_encode($this->inputs('extra_fields_values'), JSON_HEX_TAG) : '',
            'sponsored_by_id' => $this->inputs('sponsored_by_id'),
            'comments_lock' => $this->inputs('comments_lock'),
            'language_id'=> $this->inputs('language_id'),
            'from_id' => $this->inputs('from_id'),
            'extra_fields_group_id' => null,
            'icon' => $this->inputs('icon'),
            'order' => $this->inputs('order', 'INT')
        ];

        if ($this->imageUploaded) {
            $data['image'] = $this->uploadedFiles['image'];
        }
        else if ($this->inputs('imageServer')) {
            $data['image'] = $this->inputs('imageServer');
        }

        if ($this->galleryUploaded) {
            $data['gallery'] = $this->uploadedFiles['gallery'];
        }

        return $data;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'title' => 'required',
            'name' => 'required|unique:nodes',
            'language_id' => 'required',
        ];

        if ($this->imageUploaded) {
            $rules['image'] = [
                'max:1024',
                'mimetypes:'.implode(',', $this->imageAllowedTypes())
            ];
        }

        if ($this->galleryUploaded) {
            $rules['gallery'] = 'file|mimetypes:'.implode(',', $this->fileAllowedTypes());
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'title.required' => 'Title cannot be empty!',
            'name.required' => 'Alias cannot be empty!',
            'name.unique' => 'Alias already exists in our database!',
            'language_id.required' => 'Language cannot be empty!',
            'image.required' => 'Image cannot be empty!',
            'image.max' => 'Image cannot be more than 1mb!',
            'image.mimetypes' => 'Only jpg, jpeg, png and gifs are allowed!',
            'gallery.mimetypes' => 'The file you are trying to upload is not a .zip file!'
        ];
    }

    public function getValidatedData()
    {
        $this->validatedData = parent::getValidatedData();

        // Set image name if image upload is true
        if ($this->getImageUploaded()) {
            $image = App::clean($this->uploadedFiles['image']->getClientOriginalName());
            $image = replace_characters($image,' ','-');
            $this->validatedData['image'] = $image;
        }
        else if ($this->inputs('imageServer')) {
            $imageArray = explode('/', $this->inputs('imageServer'));
            $image = replace_characters(end($imageArray),'%20','-');
            $this->validatedData['image'] = $image;
        }

        // Set gallery if gallery upload is true
        if ($this->getGalleryUploaded()) {
            $this->validatedData['gallery'] = 1;
        }

        return  $this->validatedData;
    }
}