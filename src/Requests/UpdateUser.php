<?php

namespace App\Requests;

use Illuminate\Support\Arr;

class UpdateUser extends InsertUser
{
    /**
     * @var string
     */
    protected $session = '';

    /**
     * @var string
     */
    protected $message = 'User updated!';

    /**
     * @return array
     */
    public function data() :array
    {
        $data = parent::data();

        Arr::forget($data, ['created_on', 'created_by']);
        Arr::set($data, 'id', $this->inputs('id', 'INT'));

        if ($this->has('remove_avatar')) {
            $data['avatar'] = '';
        }

        return $data;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        $data = $this->data();
        $rules = parent::rules();

        Arr::forget($rules, ['password']);

        $rules['email'] = 'required|email|unique:users,email,'.$data['id'];
        $rules['user_name'] = 'required|unique:users,user_name,'.$data['id'];

        if (!empty($this->inputs('password'))) {
            $rules['password'] = 'required|confirmed';
        }

        return $rules;
    }
}