<?php

namespace App\Requests;

use App\Traits\App;
use App\Traits\Media;
use App\Models\Backend\Nodes;

class InsertArticle extends InsertNode
{
    /**
     * @var string
     */
    protected $session = 'insert_article';

    /**
     * @var string
     */
    protected $message = 'Article added!';

    /**
     * @return array
     */
    public function data() :array
    {
        $data = parent::data();
        $data['view'] = 'Frontend/Articles/'.$data['view'];
        $data['video'] = Media::encodeVideoSource($this->inputs('source'),  App::clean($this->inputs('video', false)));
        $data['relations'] = ['tags' => $this->inputs('tags') ? collect($this->inputs('tags')) : array()];
        $parentNode = Nodes::findById($this->inputs('parent_id', 'INT'));

        // Check settings that inherit values from category
        if ($parentNode->id()) {
            if($this->inputs('view') == 'from_category') {
                $view = str_replace('Frontend/Categories/', '', $parentNode->view());
                $data['view'] = 'Frontend/Articles/'.$view;
            }

            if($this->inputs('comments_lock') == 'from_category') {
                $data['comments_lock'] = $parentNode->comments_lock();
            }

            $data['extra_fields_group_id'] = $parentNode->extra_fields_group_id();
        }

        return $data;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = parent::rules();

        $rules['parent_id'] = 'required|not_in:0';
        $rules['intro_text'] = 'required';
        $rules['relations.tags'] = 'required';
        $rules['image'][] = 'required';

        return $rules;
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        $messages = parent::messages();

        $messages['parent_id.required'] = 'Category cannot be empty!';
        $messages['parent_id.not_in'] = 'Category cannot be empty!';
        $messages['intro_text.required'] = 'Intro text cannot be empty!';
        $messages['relations.tags.required'] = 'Tags cannot be empty!';

        return $messages;
    }
}