<?php

namespace App\Requests;

use App\Models\Backend\Roles;
use App\Rules\GoogleRecaptcha;

class UserRegistration extends FormRequest
{
    /**
     * @var string
     */
    protected $message = 'Your account has been created and an email was sent to verify your email address. Please click the link in the email to start using your account.';

    /**
     * @return array
     */
    public function data() :array
    {
        $users = new Roles();
        $permissions = $users->getRolesPermissions(6)->pluck('id')->toArray();

        return [
            'token' => $this->inputs('token'),
            'user_name' => $this->inputs('username'),
            'email' => $this->inputs('email'),
            'password' => $this->inputs('password'),
            'password_confirmation' => $this->inputs('password_confirmation'),
            'privacy_policy' => $this->has('privacy_policy') ? 1 : 0,
            'role_id' => 6,
            'relations'  => ['permissions' => implode(',', $permissions)],
            'created_by' => 1,
            'updated_by' => 1,
            'in_backend' => 0,
            'is_enabled' => 0,
            'is_activated' => 0,
            'activation_token' => hash_string((string)md5(uniqid(rand(), true))),
            'created_on' => now(),
            'updated_on' => now(),
            'last_visit_date' => now()
        ];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'token' => ['required', new GoogleRecaptcha],
            'user_name' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'privacy_policy' => 'required|not_in:0',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'token.required' => 'Please try again, Google Recaptcha token does not exist!',
            'user_name.required' => 'Username cannot be empty!',
            'user_name.unique' => 'Username already exists in our database!',
            'email.required' => 'Email cannot be empty!',
            'email.unique' => 'Email already exists in our database!',
            'email.email' => 'Email not valid!',
            'password.required' => 'Password cannot be empty!',
            'password.confirmed' => 'Passwords do not match!',
            'privacy_policy.required' => 'You have to accept our privacy policy to create an account!',
            'privacy_policy.not_in' => 'You have to accept our privacy policy to create an account!',
        ];
    }
}