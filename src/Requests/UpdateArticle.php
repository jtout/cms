<?php

namespace App\Requests;

use Illuminate\Support\Arr;

class UpdateArticle extends InsertArticle
{
    /**
     * @var string
     */
    protected $session = '';

    /**
     * @var string
     */
    protected $message = 'Article updated!';

    /**
     * @return array
     */
    public function data() :array
    {
        $data = parent::data();
        $data['id'] = $this->inputs('id', 'INT');

        if ($this->has('delete_gallery')) {
            $this->deleteGallery($data['id']);
        }

        return $data;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = parent::rules();

        Arr::where($rules['image'], function ($value, $key) use(&$rules) {
            if ($value == 'required') {
                Arr::forget($rules['image'], [$key]);
            }
        });

        $rules['name'] = 'required|unique:nodes,name,'.$this->inputs('id');

        return $rules;
    }
}