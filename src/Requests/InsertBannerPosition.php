<?php

namespace App\Requests;

use App\Traits\App;

class InsertBannerPosition extends FormRequest
{
    /**
     * @var string
     */
    protected $session = 'insert_banners_position';

    /**
     * @var string
     */
    protected $message = 'Position added!';

    /**
     * @return array
     */
    public function data() :array
    {
        return array(
            'title' => $this->inputs('title'),
            'alias' => App::makeSefUrl($this->inputs('title')),
            'updated_by' => user()->id(),
            'updated_on' => now(),
            'created_on' => now(),
            'created_by' => user()->id()
        );
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'title.required' => 'Title cannot be empty!'
        ];
    }
}