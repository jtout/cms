<?php

namespace App\Requests;

class InsertRole extends FormRequest
{
    /**
     * @var string
     */
    protected $session = 'insert_role';

    /**
     * @var string
     */
    protected $message = 'Role added!';

    /**
     * @return array
     */
    public function data() :array
    {
        return [
            'parent_id' => $this->inputs('parent_id'),
            'title' => $this->inputs('title'),
            'in_backend' => $this->inputs('in_backend'),
            'access_level_id' => $this->inputs('access_level_id'),
            'permissions'  => $this->inputs('selected_permissions'),
            'updated_by' => user()->id(),
            'updated_on' => now(),
            'created_on' => now(),
            'created_by' => user()->id()

        ];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'required',
            'permissions' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'title.required' => 'Title cannot be empty!',
            'permissions.required' => 'Permissions cannot be empty!'
        ];
    }
}