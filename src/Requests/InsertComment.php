<?php

namespace App\Requests;

class InsertComment extends FormRequest
{
    /**
     * @var string
     */
    protected $message = 'Comment added!';

    /**
     * @return array
     */
    public function data() :array
    {
        return [
            'text' => $this->inputs('text'),
            'parent_id' => empty($this->inputs('parent')) ? null : $this->inputs('parent'),
            'node_id' => $this->inputs('node'),
            'is_active' => $this->has('is_active') || !session()->getNode()->in_backend() ? 1 : 0,
            'updated_by' => user()->id(),
            'updated_on' => now(),
            'created_on' => now(),
            'created_by' => user()->id()
        ];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'text' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'text.required' => 'Comment can not be empty',
        ];
    }
}