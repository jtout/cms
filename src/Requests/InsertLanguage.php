<?php

namespace App\Requests;

class InsertLanguage extends FormRequest
{
    /**
     * @var string
     */
    protected $session = 'insert_language';

    /**
     * @var string
     */
    protected $message = 'Language added!';

    /**
     * @return array
     */
    public function data() :array
    {
        return [
            'title' =>  $this->inputs('title'),
            'iso_code' =>  $this->inputs('iso_code'),
            'is_active' => $this->has('is_active') ? 1 : 0,
            'updated_by' =>  user()->id(),
            'updated_on' => now(),
            'created_on' => now(),
            'created_by' => user()->id()
        ];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'required',
            'iso_code' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'title.required' => 'Title cannot be empty!',
            'iso_code.required' => 'Iso Code cannot be empty!'
        ];
    }
}