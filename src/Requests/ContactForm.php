<?php

namespace App\Requests;

use App\Facades\Cookies;
use App\Rules\CookiesConsent;
use App\Rules\GoogleRecaptcha;

class ContactForm extends FormRequest
{
    /**
     * @var string
     */
    protected $message = 'Your email was sent. Thank you for your contact!';

    /**
     * @return array
     */
    public function data() :array
    {
        return [
            'cookies_consent' => Cookies::get(SITENAME.'_cookies_consent'),
            'token' => $this->inputs('token'),
            'name' => $this->inputs('name'),
            'email' => $this->inputs('email', 'EMAIL'),
            'subject' => $this->inputs('subject'),
            'message' => replace_characters($this->inputs('message'),"\n", '<br />')
        ];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'cookies_consent' => new CookiesConsent,
            'token' => ['required', new GoogleRecaptcha],
            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'token.required' => 'Please try again, Google Recaptcha token does not exist!',
            'name.required' => 'Name cannot be empty!',
            'email.required' => 'Email cannot be empty!',
            'email.email' => 'Email is not valid!',
            'subject.required' => 'Subject cannot be empty!',
            'message.required' => 'Message cannot be empty!'
        ];
    }
}