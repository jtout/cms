<?php

namespace App\Requests;

class InsertContact extends FormRequest
{
    /**
     * @var string
     */
    protected $session = 'insert_contact';

    /**
     * @var string
     */
    protected $message = 'Contact added!';

    /**
     * @return array
     */
    public function data() :array
    {
        return array(
            'client_id' => $this->inputs('client_id', 'INT'),
            'name' => $this->inputs('name'),
            'email' => $this->inputs('email'),
            'phone' => $this->inputs('phone'),
            'is_active' => $this->has('is_active') ? 1 : 0,
            'updated_by' => user()->id(),
            'updated_on' => now(),
            'created_on' => now(),
            'created_by' => user()->id()
        );
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'name.required' => 'Name cannot be empty!',
            'email.required' => 'Email cannot be empty!',
            'email.email' => 'Email is in wrong format!',
        ];
    }
}