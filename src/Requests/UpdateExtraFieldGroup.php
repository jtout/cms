<?php

namespace App\Requests;

use Illuminate\Support\Arr;

class UpdateExtraFieldGroup extends InsertExtraFieldGroup
{
    /**
     * @param string
     */
    protected $session = '';

    /**
     * @param string
     */
    protected $message = 'Extra Fields Group updated!';

    /**
     * @return array
     */
    public function data() :array
    {
        $data = parent::data();

        Arr::forget($data, ['created_on', 'created_by']);
        Arr::set($data, 'id', $this->inputs('id', 'INT'));

        return $data;
    }
}