<?php

namespace App\Requests;

use App\Traits\App;

class InsertBanner extends FormRequest
{
    /**
     * @var string
     */
    protected $session = 'insert_banner';

    /**
     * @var string
     */
    protected $message = 'Banner added!';

    /**
     * @return array
     */
    public function data() :array
    {
        return array(
            'title' => $this->inputs('title'),
            'alias' => App::makeSefUrl($this->inputs('title')),
            'description' => $this->inputs('description'),
            'position_id' => $this->inputs('position_id'),
            'client_id' => $this->inputs('client_id'),
            'click_url' => $this->inputs('click_url'),
            'type' => $this->inputs('type'),
            'field' => !empty($this->inputs('field')) ? json_encode($this->inputs('field', false), JSON_HEX_TAG) : '',
            'mobile_field' => !empty($this->inputs('mobile_field')) ? json_encode($this->inputs('mobile_field', false), JSON_HEX_TAG) : '',
            'settings' => !empty($this->inputs('settings')) ? json_encode($this->inputs('settings'), JSON_HEX_TAG) : '',
            'nodes' => $this->inputs('selected_nodes'),
            'is_active' => $this->has('is_active') ? 1 : 0,
            'ordering' => $this->inputs('ordering'),
            'published_on' => $this->inputs('published_on'),
            'finished_on' =>  $this->inputs('finished_on'),
            'updated_by' => user()->id(),
            'updated_on' => now(),
            'created_on' => now(),
            'created_by' => user()->id()
        );
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'required',
            'published_on' => 'required',
            'finished_on' => 'required',
            'position_id' => 'required',
            'client_id' => 'required',
            'nodes' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'title.required' => 'Title cannot be empty!',
            'published_on.required' => 'Published on cannot be empty!',
            'finished_on.required' => 'Finished on cannot be empty!',
            'position_id.required' => 'Position cannot be empty!',
            'client_id.required' => 'Client cannot be empty!',
            'nodes.required' => 'Please select at least one page for the banner to be visible!'
        ];
    }
}