<?php

namespace App\Requests;

use Illuminate\Support\Arr;

class UpdateComment extends InsertComment
{
    /**
     * @var string
     */
    protected $message = 'Comment updated!';

    /**
     * @return array
     */
    public function data() :array
    {
        $data = parent::data();

        Arr::forget($data, ['created_on', 'created_by']);
        Arr::set($data, 'id', $this->inputs('id', 'INT'));

        return $data;
    }
}