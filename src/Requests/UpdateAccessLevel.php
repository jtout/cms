<?php

namespace App\Requests;

use Illuminate\Support\Arr;

class UpdateAccessLevel extends InsertAccessLevel
{
    /**
     * @var string
     */
    protected $session = '';

    /**
     * @var string
     */
    protected $message = 'Access Level updated!';

    /**
     * @return array
     */
    public function data() :array
    {
        $data = parent::data();

        Arr::forget($data, ['created_on', 'created_by']);
        Arr::set($data, 'id', $this->inputs('id', 'INT'));

        return $data;
    }
}