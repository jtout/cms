<?php

namespace App\Requests;

use App\Traits\App;

class InsertExtraField extends FormRequest
{
    /**
     * @var string
     */
    protected $session = 'insert_extra_field';

    /**
     * @var string
     */
    protected $message = 'Extra field added!';

    /**
     * @return array
     */
    public function data() :array
    {
        $options = array();
        $data = $this->inputs();

        if ($this->has('options')) {
            for($i = 0; $i < count($data['options']); $i++) {
                if(!empty($data['options'][$i]['text-input'])) {
                    $options['option'.$i] = App::sanitizeInput($data['options'][$i]['text-input'], 'STRING');
                }
            }
        }

        return [
            'title' =>  $this->inputs('title'),
            'type' =>  $this->inputs('type'),
            'info' => json_encode($options, JSON_HEX_TAG),
            'is_active' => isset($data['is_active']) ? 1 : 0,
            'ordering' =>  $this->inputs('ordering'),
            'group_id' => $this->inputs('group_id'),
            'updated_by' => user()->id(),
            'updated_on' => now(),
            'created_on' => now(),
            'created_by' => user()->id()
        ];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'required',
            'type' => 'required',
            'group_id' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'title.required' => 'Title cannot be empty!',
            'type.required' => 'Type cannot be empty!',
            'group_id.required' => 'Group cannot be empty!'
        ];
    }
}