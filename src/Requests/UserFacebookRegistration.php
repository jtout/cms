<?php

namespace App\Requests;

use App\Traits\App;
use App\Facades\Cookies;
use Illuminate\Support\Arr;
use App\Rules\CookiesConsent;

class UserFacebookRegistration extends UserRegistration
{
    /**
     * @var string
     */
    protected $message = '';

    /**
     * @return array
     */
    public function data() :array
    {
        $data = parent::data();
        $data['cookies_consent'] = Cookies::get(SITENAME.'_cookies_consent');
        $data['user_name'] = App::makeSefUrl($this->inputs('first_name').'_'.$this->inputs('last_name'));
        $data['name'] = $this->inputs('first_name');
        $data['last_name'] = $this->inputs('last_name');
        $data['email'] = $this->inputs('email', 'EMAIL');
        $data['avatar'] = $this->inputs('picture');
        $data['privacy_policy'] = 0;
        $data['activation_token'] = 'from-facebook';
        $data['is_enabled'] = 1;
        $data['is_activated'] = 1;
        $data['activated_on'] = now();

        return $data;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = parent::rules();

        Arr::forget($rules, ['password', 'privacy_policy', 'email', 'user_name']);

        $rules['cookies_consent'] = new CookiesConsent;
        $rules['name'] = 'required';
        $rules['last_name'] = 'required';

        return $rules;
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        $messages = parent::messages();

        $messages['first_name.required'] = 'First name cannot be empty!';
        $messages['last_name.required'] = 'Last name cannot be empty!';

        return  $messages;
    }
}