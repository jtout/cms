<?php

namespace App\Requests;

class InsertModule extends FormRequest
{
    /**
     * @var string
     */
    protected $session = 'insert_module';

    /**
     * @var string
     */
    protected $message = 'Module added!';

    /**
     * @return array
     */
    public function data() :array
    {
        $class = '';

        if (!empty($this->inputs('class_directory')) && !empty($this->inputs('class_name'))) {
            $class_directory = $this->inputs('class_directory');
            $class_name = $this->inputs('class_name');
            $class = '\App\Controllers\\'.$class_directory.'\\'.$class_name;
        }

        $moduleAction = !empty($class) ? $this->inputs('action') : '';

        return [
            'parent_id' => $this->inputs('parent_id'),
            'title' => $this->inputs('title'),
            'description' => $this->inputs('description'),
            'class' => $class,
            'action' => $moduleAction,
            'is_function' => $this->inputs('is_function'),
            'is_active' => $this->inputs('is_active'),
            'updated_by' => user()->id(),
            'updated_on' => now(),
            'created_on' => now(),
            'created_by' => user()->id()
        ];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'title.required' => 'Title cannot be empty!'
        ];
    }
}