<?php

namespace App\Requests;

use App\Traits\App;
use App\Traits\Media;

class UpdateProfile extends UpdateUser
{
    use Media;

    /**
     * @var string
     */
    protected $session = '';

    /**
     * @var string
     */
    protected $message = 'Profile updated!';

    /**
     * @return array
     */
    public function data() :array
    {
        $this->imageUploaded = isset($this->uploadedFiles['new_avatar']) && $this->uploadedFiles['new_avatar']->getError() === UPLOAD_ERR_OK ? true : false;

        $data = parent::data();

        $data['id'] = user()->id();
        $data['role_id'] = user()->role_id();
        $data['is_activated'] = user()->is_activated();
        $data['activation_token'] = user()->activation_token();
        $data['in_backend'] = user()->in_backend();
        $data['is_enabled'] = user()->is_enabled();
        $data['permissions'] = user()->permissions();
        $data['privacy_policy'] = $this->has('privacy_policy') ? 1 : user()->privacy_policy();
        $data['avatar'] = user()->avatar();

        if ($this->imageUploaded) {
            $data['avatar'] = $this->uploadedFiles['new_avatar'];
        }

        return $data;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = parent::rules();

        if ($this->imageUploaded) {
            $rules['avatar'] = [
                'max:1024',
                'mimetypes:'.implode(',', $this->imageAllowedTypes())
            ];
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        $messages = parent::messages();

        $messages['avatar.max'] = 'Image cannot be more than 1mb!';
        $messages['avatar.mimetypes'] = 'Only jpg, jpeg, png and gifs are allowed!';

        return $messages;
    }

    /**
     * @return mixed
     */
    public function getValidatedData()
    {
        $this->validatedData = parent::getValidatedData();

        // Set avatar name if image upload is true
        if ($this->getImageUploaded()) {
            $image = App::clean($this->uploadedFiles['new_avatar']->getClientOriginalName());
            $image = replace_characters($image,' ','-');
            $this->validatedData['avatar'] = $image;
        }

        return $this->validatedData;
    }
}