<?php

namespace App\Interfaces;

interface Validation
{
    /**
     * @return bool
     */
    public function validate() :bool;

    /**
     * @return array
     */
    public function data() :array ;

    /**
     * @return array
     */
    public function rules() :array;

    /**
     * @return array
     */
    public function messages() :array;

    /**
     * @return mixed
     */
    public function getValidatedData();

    /**
     * @return string
     */
    public function getMessage();

    /**
     * @return string
     */
    public function getMessageType();

    /**
     * @return string
     */
    public function getSession();
}