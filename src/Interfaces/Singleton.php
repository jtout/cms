<?php

namespace App\Interfaces;

interface Singleton
{
    /**
     * @return mixed
     */
    public static function initialize();
}