<?php

namespace App\Interfaces;

interface Exception extends \Throwable
{
    /**
     * @return string
     */
    public function getView();

    /**
     * @return array
     */
    public function getData();
}