<?php

namespace App\Interfaces;

interface Observer
{
    /**
     * @param Observable $observable
     * @return mixed
     */
    public static function observe(Observable $observable);

    /**
     * @param Observable $observable
     * @return mixed
     */
    public function update(Observable $observable);

    /**
     * @param Observable $observable
     * @return mixed
     */
    public function delete(Observable $observable);

    /**
     * @param Observable $observable
     * @return mixed
     */
    public function detachObserver(Observable $observable);
}