<?php

namespace App\Interfaces;

interface Cache
{
    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key);

    /**
     * @param string $key
     * @param $data
     * @param null $minutes
     * @return bool
     */
    public function set(string $key, $data, $minutes = null) :bool;

    /**
     * @param string $key
     * @return bool
     */
    public function delete(string $key) :bool;

    /**
     * @return array
     */
    public function getKeys() :array;

    /**
     * @return void
     */
    public function flush() :void;

    /**
     * @return int
     */
    public function getResultCode();

    /**
     * @return string
     */
    public function getResultMessage();

    /**
     * @param $time
     * @return mixed
     */
    public function setExpirationTime($time);
}