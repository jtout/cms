<?php

namespace App\Interfaces;

interface Observable
{
    /**
     * @param Observer $observer
     * @return mixed
     */
    public function attach(Observer $observer);

    /**
     * @param Observer $observer
     * @return mixed
     */
    public function detach(Observer $observer);

    /**
     * @return mixed
     */
    public function notify();

    /**
     * @param string $event
     * @return mixed
     */
    public function fireObserverEvent(string $event);

    /**
     * @return mixed
     */
    public function getFiredObserverEvent();
}