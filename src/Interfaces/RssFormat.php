<?php

namespace App\Interfaces;

use \Psr\Http\Message\ResponseInterface as Response;

interface RssFormat
{
    /**
     * @return Response
     */
    public function toRss() :Response;
}