<?php

namespace App\Interfaces;

use Psr\Http\Message\ServerRequestInterface;

interface Request extends ServerRequestInterface
{
    /**
     * @return array
     */
    public function getInfo() :array;

    /**
     * @param array $data
     */
    public function set(array $data) :void;

    /**
     * @param string $key
     * @param string $sanitize
     * @return array|int|mixed|object|null
     */
    public function inputs($key = '*', $sanitize = 'STRING');

    /**
     * @return string
     */
    public function referrer() :string;

    /**
     * @return string
     */
    public function getFullUri() :string;

    /**
     * @return string
     */
    public function getParsedUri() :string;

    /**
     * @return bool
     */
    public function isAjax() :bool;

    /**
     * @return bool
     */
    public function isGet();

    /**
     * @return bool
     */
    public function isPost();

    /**
     * @return void
     */
    public function sync();

    /**
     * @param string $key
     * @return bool
     */
    public function has(string $key) :bool;

    /**
     * @param string $key
     * @return mixed
     */
    public function queryParams(string $key = '*');

    /**
     * @param $controller
     * @return Request
     */
    public function checkFormRequest($controller) :Request;
}