<?php

namespace App\Interfaces;

interface Jsonable
{
    /**
     * @return string
     */
    public function toJson();
}