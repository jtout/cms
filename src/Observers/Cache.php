<?php

namespace App\Observers;

use App\Interfaces\Observer;
use App\Interfaces\Observable;

class Cache implements Observer
{
    /**
     * @param Observable $observable
     */
    public static function observe(Observable $observable)
    {
        $observable->attach(new self);
    }

    /**
     * @param Observable $observable
     * @return mixed|void
     */
    public function delete(Observable $observable)
    {
        // YOUR LOGIC

        $this->detachObserver($observable);
    }

    /**
     * @param Observable $observable
     * @return mixed|void
     */
    public function detachObserver(Observable $observable)
    {
        $observable->detach($this);
    }

    /**
     * @param Observable $observable
     * @return mixed|void
     */
    public function update(Observable $observable)
    {
        // YOUR LOGIC

        $this->detachObserver($observable);
    }
}