<?php

namespace App\Services;

use App\Interfaces\Services;

class AssetsCompressionService implements Services
{
    /**
     * @return AssetsCompressionService
     */
    public static function boot() :AssetsCompressionService
    {
        return new self();
    }

    public function minifyAndCombineCommonAssets()
    {
        // Minify CSS assets
        $bootstrap = DIR . '/public_html/theme/site/css/bootstrap.min.css';
        $minifier = new \MatthiasMullie\Minify\CSS($bootstrap);

        $fonts = DIR . '/public_html/theme/site/css/font-icons.css';
        $minifier->add($fonts);

        $style = DIR . '/public_html/theme/site/css/style.css';
        $minifier->add($style);

        $custom = DIR . '/public_html/theme/site/css/custom.css';
        $minifier->add($custom);

        $minifier->minify(DIR . '/public_html/theme/site/css/combined/common.min.css');

        // Minify JS assets
        $scripts = DIR . '/public_html/theme/site/js/scripts.js';
        $minifier = new \MatthiasMullie\Minify\JS($scripts);

        $custom = DIR . '/public_html/theme/site/js/custom.js';
        $minifier->add($custom);

        $recentNews = DIR . '/public_html/theme/site/js/recent-news.js';
        $minifier->add($recentNews);

        $minifier->minify(DIR . '/public_html/theme/site/js/combined/common.min.js');
    }

    public function minifyAndCombineArticlesAssets()
    {
        // Minify CSS assets
        $comments = DIR.'/public_html/theme/site/css/comments.css';
        $minifier = new \MatthiasMullie\Minify\CSS($comments);

        $gallery = DIR.'/public_html/theme/site/css/gallery.css';
        $minifier->add($gallery);

        $fancybox = DIR.'/public_html/theme/site/plugins/fancybox/jquery.fancybox.min.css';
        $minifier->add($fancybox);

        $reviews = DIR.'/public_html/theme/site/css/reviews.css';
        $minifier->add($reviews);

        $minifier->minify(DIR.'/public_html/theme/site/css/combined/articles.min.css');

        // Minify JS assets
        $fancyboxJs = DIR.'/public_html/theme/site/plugins/fancybox/jquery.fancybox.min.js';
        $minifierJs = new \MatthiasMullie\Minify\JS($fancyboxJs);

        $articlesJs = DIR.'/public_html/theme/site/js/articles.js';
        $minifierJs->add($articlesJs);

        $minifierJs->minify(DIR.'/public_html/theme/site/js/combined/articles.min.js');
    }
}