<?php

namespace App\Services;

use App\Entities\MailEntity;
use App\Interfaces\Services;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

class MailService implements Services
{
    /**
     * @return MailService
     */
    public static function boot() :MailService
    {
        return new self();
    }

    /**
     * @param MailEntity $mailEntity
     * @return bool
     * @throws Exception
     */
    public function send(MailEntity $mailEntity) :bool
    {
        $mail = new PHPMailer(true);                // Passing `true` enables exceptions

        //Server settings
        $mail->CharSet = 'UTF-8';
        $mail->SMTPDebug = 0;                                 	// Enable verbose debug output 2 = debug
        $mail->isSMTP();                                      	// Set mailer to use SMTP
        $mail->Host = 'mail.domain.com';                      	// Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               	// Enable SMTP authentication
        $mail->Username = 'username';   	                    // SMTP username
        $mail->Password = 'password';                        	// SMTP password
        $mail->Port = 25;                                    	// TCP port to connect to

        //Recipients
        $mail->setFrom(config()->get('system_email'), config()->get('system_email_from_name'));
        $mail->addAddress($mailEntity->to());                   // Add a recipient

        //Content
        $mail->isHTML(true);                           	// Set email format to HTML
        $mail->Subject = $mailEntity->subject();
        $mail->Body = $mailEntity->body();

        $mail->send();

        return true;
    }
}