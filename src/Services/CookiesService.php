<?php

namespace App\Services;

use Illuminate\Support\Arr;
use App\Interfaces\Services;

class CookiesService implements Services
{
    /**
     * @return CookiesService
     */
    public static function boot() :CookiesService
    {
        return new self();
    }

    /**
     * @param string $key
     * @return bool|mixed
     */
    public function get(string $key)
    {
        return Arr::get($_COOKIE, $key);
    }

    /**
     * @param string $key
     * @param $value
     * @param string $expire
     * @return bool
     */
    public function set(string $key, $value, $expire = '') :bool
    {
        if (empty($expire)) {
            $expire = time()+2592000;
        }

        setcookie($key, $value, $expire, '/', '.'.env('COOKIE_DOMAIN'), true, true);

        return true;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function delete(string $key) :bool
    {
        if(isset($_COOKIE[$key])) {
            unset($_COOKIE[$key]);
            setcookie($key, '', -1, '/', '.'.env('COOKIE_DOMAIN'), true, true);

            return true;
        }

        return false;
    }
}