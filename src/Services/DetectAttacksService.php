<?php

namespace App\Services;

use App\Traits\App;
use Illuminate\Support\Arr;
use App\Interfaces\Request;
use App\Interfaces\Services;
use App\Entities\AppLogEntity;

class DetectAttacksService implements Services
{
    /**
     * @var array
     */
    protected $method = array();

    /**
     * @var null
     */
    protected $suspect = null;

    /**
     * @var Request|null
     */
    protected $request = null;

    /**
     * @var null
     */
    protected $attack = null;

    /**
     * @var array
     */
    protected $rules = [];

    /**
     * @var array
     */
    public $options = [
        'logToFile' => false,
        'logToDatabase' => true,
        'unset'  => true,
        'exit'   => true,
        'injection' => true
    ];

    /**
     * @return DetectAttacksService
     */
    public static function boot()
    {
        return new self(request());
    }

    /**
     * DetectAttacks constructor.
     * @param Request $request
     */
    private function __construct(Request $request)
    {
        $this->request = $request;
        $this->rules['xss'] = "/\s ".$this->xssRules()." \s/i";
        $this->rules['sqli'] = "/\s ".$this->sqlInjectionRules()." \s/i";
    }

    /**
     * @return bool|mixed
     */
    public function detect()
    {
        $this->setMethod();
        $result = $this->parseQuery();

        if ($result) {
            if ($this->options['logToFile']) {
                $this->logQueryToFile();
            }

            if ($this->options['logToDatabase']) {
                $this->logQueryToDatabase();
            }

            if ($this->options['unset']){
                unset($_GET, $_POST);
            }

            if ($this->options['exit']){
                return $this->options['injection'];
            }
        }

        return false;
    }

    /**
     * @return void
     */
    private function setMethod()
    {
        if ($this->request->isGet()) {
            $this->method = $_GET;
        }

        if ($this->request->isPost()) {
            $this->method = $_POST;
        }
    }

    /**
     * @return bool
     */
    private function parseQuery()
    {
        $inputs = $this->method;

        if (count($inputs) > 0) {
            foreach(Arr::flatten($inputs) as $key => $val) {
                $val = $this->replaceCharacters($val);

                if ($this->request->isPost()) {
                    if (isset($this->_method['token']) && (empty($this->method['token']) || strpos($this->method['token'], 'token') !== false)) {
                        $this->suspect = 'token:'.$this->method['token'];
                        $this->attack = App::appLogsTypes()[2];
                        return true;
                    }
                }

                if (preg_match($this->rules['xss'], $val)) {
                    $inputKey = array_search($val, $inputs);
                    $this->suspect = $inputKey.':'.$val;
                    $this->attack = App::appLogsTypes()[1];
                    return true;
                }

                if (preg_match($this->rules['sqli'], $val)) {
                    $inputKey = array_search($val, $inputs);
                    $this->suspect = $inputKey.':'.$val;
                    $this->attack = App::appLogsTypes()[0];
                    return true;
                }
            }
        }
        else {
            $uri = urldecode($this->request->getFullUri());

            if (preg_match($this->rules['xss'], $uri)) {
                $this->suspect = $uri;
                $this->attack = App::appLogsTypes()[1];
                return true;
            }

            if (preg_match($this->rules['sqli'], $uri)) {
                $this->suspect = $uri;
                $this->attack = App::appLogsTypes()[0];
                return true;
            }
        }

        return false;
    }

    /**
     * @return void
     */
    private function logQueryToFile()
    {
        $data = [
            'date' => now(),
            'ip' => $this->request->getServerParam('REMOTE_ADDR'),
            'causer' => user()->id(),
            'value' => $this->suspect,
            'inputs' => $this->method,
            'server_params' => $_SERVER
        ];

        file_put_contents(DIR.'/src/Logs/sql.injection.txt', json_encode($data) . PHP_EOL, FILE_APPEND);
    }

    /**
     * @return void
     */
    private function logQueryToDatabase()
    {
        $info = [
            'date' => now(),
            'ip' => $this->request->getServerParam('REMOTE_ADDR'),
            'causer' => user()->id(),
            'value' => App::sanitizeInput($this->suspect, 'STRING'),
            'inputs' => $this->method,
            'server_params' => $_SERVER
        ];

        $appLogEntity = new AppLogEntity();
        $appLogEntity->setType($this->attack);
        $appLogEntity->setModelInstance('\App\Models\Backend\AppLogs');
        $appLogEntity->setInfo(json_encode($info));
        $appLogEntity->setCreatedBy(user()->id());
        $appLogEntity->insert();
    }

    /**
     * @param string $string
     * @return mixed|string
     */
    private function replaceCharacters(string $string)
    {
        $string = replace_characters($string, "+", '');
        $string = replace_characters($string, "\n", '');
        $string = replace_characters($string, "\r", '');
        $string = replace_characters($string, "\t", '');
        $string = replace_characters($string, "<br />", '');
        $string = replace_characters($string, "/**/", '');
        $string = replace_characters($string, "  ", "");

        $string = replace_characters($string, "x00", ""); // \0  [This is a zero, not the letter O]
        $string = replace_characters($string, "x08", ""); // \b
        $string = replace_characters($string, "x0", ""); // \t
        $string = replace_characters($string, "x0a", ""); // \n
        $string = replace_characters($string, "x0d", ""); // \r
        $string = replace_characters($string, "x1a", ""); // \Z
        $string = replace_characters($string, "x22", ""); // \"
        $string = replace_characters($string, "x25", ""); // \%
        $string = replace_characters($string, "x27", ""); // \'
        $string = replace_characters($string, "x5c", ""); // \\
        $string = replace_characters($string, "x5f", "");  // \_

        return $string;
    }

    /**
     * @return string
     */
    private function xssRules() :string
    {
        $reserved = "|wvstest=(.*)|";
        $reserved .= "javascript:(.*)|";
        $reserved .= "domxssExecutionSink(.*)|";
        $reserved .= "wvstest=javascript:domxssExecutionSink((.*))|";
        $reserved .= "javascript:alert((.*))|";
        $reserved .= "locxss|";
        $reserved .= "<META HTTP-EQUIV=(.*)|";
        $reserved .= "document.cookie|";
        $reserved .= "String.fromCharCode|";
        $reserved .= "<BODY onload|";
        $reserved .= "<svg onload|";
        $reserved .= "fullhdfilmizle(.*)|";
        $reserved .= "Türkçe|";

        return $reserved;
    }

    /**
     * @return string
     */
    private function sqlInjectionRules() :string
    {
        $reserved = "|select\[\*\]from|select \* from|select\*from|'or'1'=1|";
        $reserved .= "or1=1|update set|insert into|delete from|";
        $reserved .= "order by|1'1|select count\(\[\*\]\)|select count\(\*\)|1 and 1=1|";
        $reserved .= "1 UNI\/\[\*\*\]\/ON SELECT ALL FROM WHERE(.*)|";
        $reserved .= "1 AND ASCII\(LOWER\(SUBSTRING\(\(SELECT TOP 1 name FROM sysobjects WHERE xtype='U'\), 1, 1\)\)\) > 116|";
        $reserved .= "1' AND 1=(SELECT COUNT\(\[\*\]\) FROM tablenames); --|";
        $reserved .= "&#x31|&#x27|&#x20|&#x4F|&#x52|&#x3D|";
        $reserved .= "' OR user_name IS NOT NULL OR user_name = '|'; DESC users; --|1' AND non_existant_table = '1|";
        $reserved .= "1 AND USER_NAME\(\) = 'dbo'|1 EXEC XP_|";
        $reserved .= "drop table(.*)|";
        $reserved .= "UNION SELECT(.*)|";
        $reserved .= "UNION(.*)SELECT(.*)|";
        $reserved .= "ORDER BY (.*)|";
        $reserved .= "AND ASCII(.*) (.*)|";
        $reserved .= "AND LENGTH((.*)) (.*)|";
        $reserved .= "declare @((.*))|";
        $reserved .= "OR 'SQLi' = 'SQL'+(.*)|";
        $reserved .= "OR 'SQLi' > 'S'|";
        $reserved .= "or 20 > 1|";
        $reserved .= "OR 2 between 3 and 1|";
        $reserved .= "OR 'SQLi' = N'SQLi'|";
        $reserved .= "1 and 1 = 1|";
        $reserved .= "1 && 1 = 1|";
        $reserved .= "Table_schema,0x3e,|";
        $reserved .= "fullhdfilmizle(.*)|";
        $reserved .= "Türkçe|";

        return $reserved;
    }
}