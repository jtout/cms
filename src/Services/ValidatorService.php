<?php

namespace App\Services;

use App\Interfaces\Services;
use Illuminate\Validation\Factory;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Translation\FileLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\DatabasePresenceVerifier;

class ValidatorService implements Services
{
    /**
     * @var Factory
     */
    protected $factory;

    /**
     * @return ValidatorService
     */
    public static function boot() :ValidatorService
    {
        return new self();
    }

    /**
     * ValidatorService constructor.
     */
    private function __construct()
    {
        $this->factory = new Factory(
            $this->loadTranslator()
        );

        $dbManager = app('db')->getDatabaseManager();
        $presenceVerifier = new DatabasePresenceVerifier($dbManager);
        $this->factory->setPresenceVerifier($presenceVerifier);
    }

    /**
     * @return Translator
     */
    protected function loadTranslator() :Translator
    {
        $filesystem = new Filesystem();
        $loader = new FileLoader(
            $filesystem, app_dir() . '/src/System/lang');

        $loader->addNamespace(
            'lang',
            app_dir() . '/src/System/lang'
        );

        $loader->load('en', 'validation', 'lang');

        return new Translator($loader, 'en');
    }

    /**
     * @return Factory
     */
    public function getFactory() :Factory
    {
        return $this->factory;
    }

    /**
     * @param $method
     * @param $args
     * @return mixed
     */
    public function __call($method, $args)
    {
        return call_user_func_array(
            [$this->factory, $method],
            $args
        );
    }
}