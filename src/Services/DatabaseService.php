<?php

namespace App\Services;

use App\Interfaces\Services;
use Illuminate\Database\Capsule\Manager;

final class DatabaseService implements Services
{
    /**
     * @var null
     */
    static private $__instance  = NULL;

    /**
     * @return \Illuminate\Database\Capsule\Manager|mixed
     */
    public static function boot() :Manager
    {
        if(self::$__instance == NULL) {
            self::$__instance = self::__connect();
            $pdo = self::$__instance->getConnection()->getPdo();

            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(\PDO::MYSQL_ATTR_INIT_COMMAND, "SET sql_mode = NO_BACKSLASH_ESCAPES");
            $pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
        }

        return self::$__instance;
    }


    /**
     * @return Manager
     */
    private static function __connect() :Manager
    {
        $settings = [
            'driver' => 'mysql',
            'host' => env('DB_HOST'),
            'database' => env('DB_NAME'),
            'username' => env('DB_USER'),
            'password' => env('DB_PASSWORD'),
            'charset' => env('DB_CHARSET'),
            'collation' => 'utf8mb4_general_ci',
            'prefix' => '',
        ];

        $capsule = new Manager;
        $capsule->addConnection($settings);
        $capsule->setAsGlobal();
        $capsule->bootEloquent();

        return $capsule;
    }
}