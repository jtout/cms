<?php

namespace App\Services;

use Sentry\State\Scope;
use function Sentry\init;
use Slim\Handlers\PhpError;
use App\Interfaces\Services;
use App\Interfaces\Exception;
use function Sentry\configureScope;
use App\Exceptions\NotFoundException;
use App\Exceptions\AppErrorException;
use function Sentry\captureException;
use App\Exceptions\RequestNotValidException;
use Psr\Http\Message\ServerRequestInterface;
use App\Exceptions\UnauthorizedAccessException;
use Psr\Http\Message\ResponseInterface as Response;

class ErrorHandlerService extends PhpError implements Services
{
    /**
     * @param Exception
     */
    protected $exception;

    /**
     * @return ErrorHandlerService
     */
    public static function boot() :ErrorHandlerService
    {
        return new self();
    }

    /**
     * @param ServerRequestInterface $request
     * @param Response $response
     * @param \Throwable $exception
     * @return mixed
     */
    public function __invoke(ServerRequestInterface $request, Response $response, \Throwable $exception)
    {
        if (env('DEBUG') && !$exception instanceof Exception) {
            parent::__construct(true);
            return parent::__invoke($request, $response, $exception);
        }
        else {
            $this->exception = $exception;
            if (!$exception instanceof Exception || $exception instanceof \Error) {
                $this->informSentry($exception);
                $this->exception = new AppErrorException();
            }

            if ($this->exception instanceof RequestNotValidException) {
                if (request()->isAjax()) {
                    $res = $this->render();
                }
                else {
                    foreach ($this->exception->getData()['errors'] as $message) {
                        flash_message('danger', $message);
                    }

                    $res = redirect()->back();
                }
            }
            else {
                if (session()->getNode()->in_backend()) {
                    if (!empty($this->exception->getMessage())) {
                        flash_message('danger', $this->exception->getMessage());
                    }

                    $res = redirect()->back();
                }
                else {
                    $res = $this->render();
                }
            }

            return $res;
        }
    }

    public function render()
    {
        $exception = $this->exception;

        if (request()->isAjax()) {
            $response = response()->withJson(['message' => $exception->getMessage(), 'data' => $exception->getData()], $exception->getCode());
        }
        else {
            $response = view('Errors/'.$exception->getView(), $exception->getData(), $exception->getCode())
                ->render();
        }

        return $response;
    }

    /**
     * @param int $code
     * @param string $message
     * @param null $exception
     */
    public function generateManualException(int $code, string $message = '', $exception = null)
    {
        switch ($code) {
            case 404:
                throw new NotFoundException($message, $exception);
                break;

            case 401:
                throw new UnauthorizedAccessException($message, $exception);
                break;

            default:
                throw new AppErrorException($message, $exception);
                break;
        }
    }

    /**
     * @param \Throwable $exception
     */
    public function informSentry(\Throwable $exception)
    {
        init(['dsn' => env('SENTRY')]);

        configureScope(function (Scope $scope): void {
            $scope->setUser([
                'user' => empty(user()->id()) ? 'guest' : user()->id(),
                'user_ip' => get_user_info_by_ip()->ip,
                'url' => request()->getFullUri(),
                'node' => session()->getNode()->toJson()
            ]);
        });

        captureException($exception);
    }
}