<?php

namespace App\Services;

use App\Traits\App;
use App\Interfaces\Services;
use App\Entities\NodeEntity;
use Elasticsearch\ClientBuilder;

class ElasticSearchService implements Services
{
    /**
     * @var array
     */
    static private $__hosts = ['10.0.10.43:9200'];

    /**
     * @var string
     */
    static private $__index = 'cms';

    /**
     * @var null
     */
    static private $__instance  = null;

    /**
     * ElasticSearchConnection constructor.
     */
    private function __construct() {}

    /**
     * @return \Elasticsearch\Client
     */
    public static function boot() :\Elasticsearch\Client
    {
        if(self::$__instance == null)
            self::$__instance = self::__connect();

        return self::$__instance;
    }

    /**
     * @return \Elasticsearch\Client
     */
    private static function __connect() :\Elasticsearch\Client
    {
        return ClientBuilder::create()->setHosts(self::$__hosts)->build();
    }

    /**
     * @return string
     */
    public static function getIndex() :string
    {
        return self::$__index;
    }

    /**
     * @param string $method
     * @param NodeEntity $node
     * @param string $query
     * @return array
     */
    public static function setParams(string $method, NodeEntity $node, string $query = '') :array
    {
        $params = array();

        switch ($method)
        {
            case 'index':
                $params = [
                    'index' => self::$__index,
                    'type' => $node->content_type_id(),
                    'id' => $node->id(),
                    'body' => [
                        'title' => $node->title(),
                        'parent_id' => $node->parent_id(),
                        'is_active' => $node->is_active(),
                        'is_featured' => $node->is_featured(),
                        'is_sticky' => $node->is_sticky(),
                        'intro_text' => $node->intro_text(),
                        'published_on' => $node->published_on(),
                        'finished_on' => $node->finished_on(),
                        'access_level_id' => $node->access_level_id(),
                    ]
                ];

                break;

            case 'delete':
                $params = [
                    'index' => self::$__index,
                    'type' => $node->content_type_id(),
                    'id' => $node->id()
                ];

                break;

            case 'search':
                $params = [
                    'index' => self::$__index,
                    'type' => 1,
                    'body' => [
                        'query' => [
                            'query_string' => [
                                'query' => '*'.App::sanitizeInput($query, 'STRING').'*',
                            ]
                        ]
                    ]
                ];
                break;

            default:
                break;
        }

        return $params;
    }
}