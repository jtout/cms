<?php

namespace App\Services;

use App\Traits\App;
use App\Facades\Container;
use Illuminate\Support\Arr;
use App\Entities\NodeEntity;
use App\Interfaces\Services;
use App\Factories\NodeFactory;

class SessionService implements Services
{
    /**
     * @var null|SessionService
     */
    private static $instance = NULL;

    /**
     * @var NodeEntity
     */
    protected $node;

    /**
     * @var mixed
     */
    protected $sessionType;

    /**
     * @var mixed
     */
    protected $session;

    /**
     * Session constructor.
     */
    private function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $this->session = $_SESSION;
    }

    /**
     * @return \Slim\Flash\Messages
     */
    public function flash()
    {
        return Container::get('flash');
    }

    /**
     * @return mixed|void
     */
    public static function boot() :SessionService
    {
        if(self::$instance == NULL) {
            self::$instance = new SessionService();
        }

        return self::$instance;
    }

    /**
     * @param string $key
     * @return mixed|string
     */
    public function get(string $key)
    {
        $value = Arr::get($_SESSION[$this->sessionType], $key);

        return $value;
    }

    /**
     * @param $key
     * @param $value
     * @return bool|void
     */
    public function set($key, $value)
    {
        if (empty($key)) {
            return false;
        }

        Arr::set($_SESSION[$this->sessionType], $key, $value);
    }

    /**
     * @param string $key
     */
    public function unset(string $key = '*')
    {
        if ($key == '*') {
            Arr::forget($_SESSION, $this->sessionType);
        }
        else {
            Arr::forget($_SESSION[$this->sessionType], $key);
        }
    }

    public function destroy()
    {
        session_destroy();
    }

    /**
     * @return mixed
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @return NodeEntity
     */
    public function getNode() :NodeEntity
    {
        if (empty($this->node)) {
            $this->node = NodeFactory::build();
        }

        return $this->node;
    }

    /**
     * @param string $type
     */
    public function setSessionType(string $type)
    {
        $this->sessionType = $type;
    }

    /**
     * @param string $url
     */
    public function setPreviousUrl(string $url)
    {
        $this->set('previousUrl', $url);
    }

    /**
     * @return mixed
     */
    public function getSessionType()
    {
        return $this->sessionType;
    }

    /**
     * @return string
     */
    public function getPreviousUrl()
    {
        return $this->get('previousUrl');
    }

    /**
     * @param NodeEntity $node
     */
    public function setNavigation(NodeEntity $node)
    {
        $this->node = $node;
        $this->set('limit', 20);
        $this->sessionType = $node->in_backend() == 1 ? env('ADMIN_NAME') : 'site';
        $this->setPreviousUrl(request()->isGet() ? request()->getFullUri() : request()->referrer());

        $queryParams = request()->getQueryParams();
        if (empty($this->get($this->node->name())) || empty($queryParams)) {
            $this->set('sort', '');
            $this->set('icon', '');
            $this->set('page', 1);
            $this->set('order', 'asc');
            $this->set('navigation', '');
            $this->set('icon_default', '<i class="fa fa-sort"></i>');
        }

        if (request()->has('reset_filters')) {
            unset($queryParams);
        }

        if(isset($queryParams['filters']) && count($queryParams['filters']) > 0) {
            foreach ($queryParams['filters'] as $key => $value) {
                App::setFilter('filters.'.$key, $value, is_int($value) ? 'INT' : 'STR');
            }
        }

        $this->set('limit', request()->has('l') ? request()->inputs('l') : $this->get('limit'));
        $this->set('page',  request()->has('p') ? request()->inputs('p') : $this->get('page'));
        $this->set('sort',  request()->has('s') ? request()->inputs('s') : $this->get('sort'));
        $this->set('order', request()->has('o') ? request()->inputs('o') : $this->get('order'));

        if (request()->has('o')) {
            $this->set('order', $this->get('order') == 'asc' ? 'desc' : 'asc');

            if ($this->get('order') == 'asc') {
                $this->set('icon', '<i class="fa fa-sort-up"></i>');
            }
            else {
                $this->set('icon', '<i class="fa fa-sort-desc"></i>');
            }
        }
    }
}