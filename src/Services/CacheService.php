<?php

namespace App\Services;

use Illuminate\Support\Arr;
use App\Entities\NodeEntity;
use App\Interfaces\Services;
use App\Models\Backend\Cache;
use App\Models\Frontend\Banners;
use App\Interfaces\Cache AS CacheInterface;

final class CacheService implements CacheInterface, Services
{
    /**
     * @var string
     */
    private $server = 'localhost';

    /**
     * @var string
     */
    private $port	= '11211';

    /**
     * @var null
     */
    static private $instance = NULL;

    /**
     * @var int
     */
    public $expire = 86400; //one day

    /**
     * @var \Memcached
     */
    private $cache;

     /**
     * @var string
     */
     private $appKey;

    /**
     * Cache constructor.
     */
    private function __construct()
    {
        $this->appKey = SITENAME.'-';
        $this->cache = new \Memcached();
        $this->cache->setOption(\Memcached::OPT_LIBKETAMA_COMPATIBLE, true);
        $this->cache->setOption(\Memcached::OPT_COMPRESSION, true);
        $this->cache->addServer($this->server, $this->port);
    }

    /**
     * @return CacheService|mixed|null
     */
    public static function boot()
    {
        if (self::$instance == NULL) {
            self::$instance = new CacheService();
        }

        return self::$instance;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key)
    {
        $key = $this->encodeCacheKey($key);

        $response = $this->cache->get($key);

        return unserialize($response);
    }

    /**
     * @param string $key
     * @param $data
     * @return bool
     */
    public function set(string $key, $data, $minutes = null) :bool
    {
        if ($minutes) {
            $this->setExpirationTime($minutes);
        }

        $encodedKey = $this->encodeCacheKey($key);

        if ($this->cache->set($encodedKey, serialize($data), $this->expire)) {
            return Cache::query()->updateOrInsert(
                ['encoded_key' => $encodedKey],
                ['decoded_key' => $key]
            );
        }

        return false;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function delete(string $key) :bool
    {
        $encodedKey = $this->encodeCacheKey($key);

        if ($this->cache->delete($encodedKey)) {
            Cache::query()->where(['encoded_key' => $encodedKey])->delete();

            return true;
        }

        return false;
    }

    /**
     * @return int
     */
    public function getResultCode()
    {
        return $this->cache->getResultCode();
    }

    /**
     * @return string
     */
    public function getResultMessage()
    {
        return $this->cache->getResultMessage();
    }

    /**
     * @param bool $matchWithDb
     * @return array
     */
    public function getKeys($matchWithDb = false) :array
    {
        $appKeys = [];
        $allKeys = $this->cache->getAllKeys();

        foreach ($allKeys as $key) {
            if (strpos($key, SITENAME) !== false) {
                $appKeys[] = $key;
            }
        }

        if ($matchWithDb == true) {
            $appKeys = Cache::query()
                ->whereIn('encoded_key', $appKeys)
                ->pluck('decoded_key')
                ->toArray();
        }

        return $appKeys;
    }

    /**
     * @return void
     */
    public function flush() :void
    {
        $keys = $this->getKeys();

        if ($this->cache->deleteMulti($keys)) {
            Cache::query()->truncate();
        }
    }

    /**
     * @param $time
     * @return mixed|void
     */
    public function setExpirationTime($time)
    {
        $this->expire = $time;
    }

    /**
     * @param $key
     * @param \Closure $callback
     * @return mixed
     */
    public function remember($key, \Closure $callback, $minutes = null)
    {
        if ($minutes) {
            $this->setExpirationTime($minutes);
        }

        if (!empty($value = $this->get($key))) {
            return $value;
        }

        $value = $callback();
        $this->set($key, $value, $minutes);

        return $value;
    }

    /**
     * @param NodeEntity $node
     * @param array $data
     * @return array
     */
    public function getCommonCachedData(NodeEntity $node, array $data) :array
    {
        $node->loadRelations(['parent']);
        $nodeToSearchBanners = $node->content_type_id() != 1 ? $node : $node->parent();
        $cachedNode = $this->get($nodeToSearchBanners->path(true));
        $data['banners'] = Arr::get($cachedNode, 'banners');

        if (empty($data['banners'])) {
            $banners = new Banners();
            $data['banners'] = $banners->getActiveBanners($nodeToSearchBanners->id(), $node->content_type_id());

            Arr::set($cachedNode, 'banners', $data['banners']);
            $this->set($nodeToSearchBanners->path(true), $cachedNode);
        }

        return $data;
    }

    /**
     * @param string $key
     * @return string
     */
    private function encodeCacheKey(string $key) :string
    {
        $encodedKey = md5(base64_encode($key));
        
        return $this->appKey.$encodedKey;
    }
}