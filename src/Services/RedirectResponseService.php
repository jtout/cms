<?php

namespace App\Services;

use Slim\Http\Response;
use App\Interfaces\Services;

class RedirectResponseService extends Response implements Services
{
    /**
     * @return RedirectResponseService
     */
    public static function boot() :RedirectResponseService
    {
        return new self();
    }

    /**
     * @param $url
     * @param null $status
     * @return RedirectResponseService
     */
    public function redirect($url, $status = null)
    {
        return $this->withRedirect($url, $status);
    }

    /**
     * @return bool|string|string[]
     */
    public function previous()
    {
        $node = session()->getNode();
        $referrer = request()->referrer();
        $queryParams = request()->getQueryParams();
        $url = $referrer ? $referrer : substr($node->path(), 0, strrpos($node->path(), '/'));

        if (isset($queryParams['action']) && $queryParams['action'] != '') {
            if (request()->isGet() && $node->module_action() != $queryParams['action']) {
                $url = $node->in_backend() == 1 ? strtok($node->path(true), '?') : $referrer;

                if (empty($url)) {
                    $url = env('DOMAIN');
                }
            }
        }

        if (is_null($url)) {
            $url = session()->getPreviousUrl();
        }

        return $url;
    }

    /**
     * @param int $status
     * @param array $headers
     * @return RedirectResponseService
     */
    public function back($status = 302, $headers = [])
    {
        $response = $this->withRedirect($this->previous(), $status);

        if (count($headers) > 0) {
            foreach ($headers as $key => $value) {
                $response = $response->withHeader($key, $value);
            }
        }

        return $response;
    }
}