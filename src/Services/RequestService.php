<?php

namespace App\Services;

use App\Traits\App;
use App\Facades\Container;
use Illuminate\Support\Arr;
use App\Interfaces\Request;
use App\Interfaces\Services;
use App\Requests\FormRequest;
use \Slim\Http\Request as SlimRequest;
use App\Facades\Request as RequestFacade;
use App\Exceptions\RequestNotValidException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class RequestService extends SlimRequest implements Services, Request
{
    /*
     * var Container $container
     */
    public static $container;

    /**
     * @return RequestService
     */
    public static function boot() :RequestService
    {
        $request = RequestService::createFromEnvironment(self::$container['environment']);

        $uploadedFiles = [];
        foreach ($request->getUploadedFiles() as $key => $uploadedFile) {
            $uploadedFiles[$key] = new UploadedFile(realpath($uploadedFile->file), $uploadedFile->getClientFilename(),
                $uploadedFile->getClientMediaType(), $uploadedFile->getError());
        }

        $request->uploadedFiles = $uploadedFiles;

        return $request;
    }

    /**
     * @return array
     */
    public function getInfo() :array
    {
        return [
            'attributes' => $this->getAttributes(),
            'queryParams' => $this->getQueryParams(),
            'parsedBody' => $this->getParsedBody(),
            'uploadedFiles' => $this->getUploadedFiles()
        ];
    }

    /**
     * @param array $data
     */
    public function set(array $data) :void
    {
        foreach ($data as $key => $value) {
            if ($this->isPost()) {
                Arr::set($this->bodyParsed, $key, $value);
            }
            else {
                Arr::set($this->queryParams, $key, $value);
            }
        }
    }

    /**
     * @param string $key
     * @param string $sanitize
     * @return array|int|mixed|object|null
     */
    public function inputs($key = '*', $sanitize = 'STRING')
    {
        if ($this->isPost()) {
            $data = $this->getParsedBody();
        }
        else {
            $data = $this->getQueryParams();
        }

        if ($key == '*') {
            return $data;
        }
        else {
            $value = Arr::get($data, $key);

            if (!empty($sanitize) && !is_array($value)) {
                if (is_int($value)) {
                    $sanitize = 'INT';
                }

                $value = App::sanitizeInput($value, $sanitize);

                if ($sanitize == 'INT' && empty($value)) {
                    return 0;
                }
            }

            return $value;
        }
    }

    /**
     * @return string
     */
    public function referrer() :string
    {
        return $this->getHeader('referer')[0] ?? '';
    }

    /**
     * @return string
     */
    public function getFullUri() :string
    {
        return (string)$this->getUri();
    }

    /**
     * @return string
     */
    public function getParsedUri() :string
    {
        return (string)env('DOMAIN').$this->getUri()->getPath();
    }

    /**
     * @return bool
     */
    public function isAjax() :bool
    {
        return $this->isXhr();
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has(string $key) :bool
    {
        return in_array($key, array_keys($this->inputs()));
    }

    /**
     * @param string $key
     * @return array|mixed
     */
    public function queryParams(string $key = '*')
    {
        if ($key == '*') {
            return $this->getQueryParams();
        }

        return Arr::get($this->getQueryParams(), $key);
    }

    /**
     * @param $controller
     * @return Request
     * @throws \ReflectionException
     */
    public function checkFormRequest($controller) :Request
    {
        $request = $this;
        $requestClass = get_class($this);
        $action = $this->queryParams('action');

        if ($action) {
            $reflection = new \ReflectionMethod($controller, $action);
            $params = $reflection->getParameters();

            if (!empty($params)) {
                $requestClass = $params[0]->getType()->getName();
            }
        }

        if ($this->isPost()) {
            $info = $this->getInfo();

            if (in_array($requestClass, $this->formRequests())) {
                $request = $requestClass::createFromEnvironment(Container::get('environment'))
                    ->withAttributes($info['attributes'])
                    ->withQueryParams($info['queryParams'])
                    ->withUploadedFiles($info['uploadedFiles']);

                if ($request instanceof FormRequest) {
                    $request->validate();
                }
            }
        }
        else if ($requestClass != get_class($request) && $requestClass != 'App\Interfaces\Request') {
            throw new RequestNotValidException('Invalid request type! Please try again!');
        }

        $request->sync();

        return $request;
    }

    /**
     * @return void
     */
    public function sync() :void
    {
        RequestFacade::swap($this);
    }

    /**
     * @return array
     */
    private function formRequests() :array
    {
        $formRequestsClasses = scandir(app_dir().'/src/Requests');

        return collect(array_slice($formRequestsClasses, 2))
            ->transform(function ($value){
                $value = replace_characters($value, '.php', '');
                return  'App\Requests\\'.$value;
            })->toArray();
    }
}