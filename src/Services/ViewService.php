<?php

namespace App\Services;

use App\Traits\Menu;
use App\Facades\Cache;
use App\Interfaces\Jsonable;
use App\Interfaces\Services;
use Illuminate\Support\Arr;
use \Slim\Views\PhpRenderer;
use App\Models\Backend\Nodes;
use App\Facades\AssetsCompression;
use \Psr\Http\Message\ResponseInterface as Response;

class ViewService implements Services, Jsonable
{
    use Menu;

    /**
     * @var null
     */
    protected $renderer = NULL;

    /**
     * @var string
     */
    protected $template;

    /**
     * @var string
     */
    protected $templatePath;

    /**
     * @var mixed
     */
    protected $attributes;

    /**
     * @var int
     */
    protected $status = 200;

    /**
     * @var string
     */
    protected $path = DIR."/src/Views/";

    /**
     * ViewService constructor.
     */
    private function __construct()
    {
        if ($this->renderer == NULL) {
            $this->renderer = new PhpRenderer($this->path);
        }

        return $this->renderer;
    }

    /**
     * @return ViewService
     */
    public static function boot() :ViewService
    {
        return new self();
    }

    /**
     * @param string $template
     * @param array $attributes
     * @param int $status
     * @return $this
     */
    public function make(string $template, array $attributes, int $status = 200)
    {
        $this->status = $status;
        $this->attributes = $attributes;
        $this->template = $template.'.php';
        $this->templatePath = $this->renderer->getTemplatePath().$this->template;

        return $this;
    }

    /**
     * @return Response
     */
    public function render() :Response
    {
        $this->composeCommonData();

        return $this->renderer->render(response(), $this->template, $this->attributes)
            ->withStatus($this->status)
            ->withHeader('Content-Type', 'text/html');
    }

    /**
     * @return Response
     */
    public function toJson() :Response
    {
        $data['html'] = $this->raw();
        $data['info'] = $this->attributes;

        return response()->withJson($data, $this->status);
    }

    /**
     * @return mixed
     */
    public function raw()
    {
        return $this->renderer->fetch($this->template, $this->attributes);
    }

    /**
     * @return PhpRenderer|null
     */
    public function getRenderer()
    {
        return $this->renderer;
    }

    /**
     * @return void
     */
    private function composeCommonData()
    {
        $nodes = new Nodes();
        $data = $this->attributes;
        $node = session()->getNode();
        $cachedData = $node->getCache();
        $backend = $node->in_backend() == 1 ? true : false;
        $menu = !empty($node->id()) ? $this->getMainMenu($node, $backend) : [];
        $breadcrumbs = Arr::get($cachedData, 'breadcrumbs');

        if (empty($breadcrumbs) && !empty($node->id())) {
            $breadcrumbs = $nodes->getBreadcrumbs(['id' => $node->id(), 'active' => 1])->toArray();
            Arr::set($cachedData, 'breadcrumbs', $breadcrumbs);
            $node->updateCache($cachedData);
        }

        if ($node->in_backend() == 0 && !empty($node->id())) {
            $data = Cache::getCommonCachedData($node, $data);
        }

        AssetsCompression::minifyAndCombineCommonAssets();

        $this->attributes = array(
            'node' => $node,
            'data' => $data,
            'breadcrumbs' => $breadcrumbs,
            'messages' =>  session()->flash()->getMessages(),
            'csrf_name' => request()->getAttribute('csrf_name'),
            'csrf_value' => request()->getAttribute('csrf_value'),
            'main_menu' => isset($menu['main_menu']) ? $menu['main_menu'] : '',
            'mobile_menu' => isset($menu['mobile_menu']) ? $menu['mobile_menu'] : '',
        );
    }
}