<?php

namespace App\Services;

use App\Interfaces\Services;

class JoomlaService implements Services
{
    /**
     * @var null
     */
    static private $__instance  = NULL;

    /**
     * @return \PDO
     */
    public static function boot() :\PDO
    {
        if(self::$__instance == NULL) {
            self::$__instance = self::__connect();
        }

        return self::$__instance;
    }

    /**
     * @return \PDO
     */
    private static function __connect() :\PDO
    {
        $pdo = new \PDO('mysql:host=' . env('JOOMLA_DB_HOST') . ';dbname=' . env('JOOMLA_DB_NAME') . ';charset=' . env('JOOMLA_DB_CHARSET') . ';connect_timeout=15', env('JOOMLA_DB_USER'), env('JOOMLA_DB_PASSWORD'));
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        $pdo->setAttribute(\PDO::MYSQL_ATTR_INIT_COMMAND, "SET sql_mode = NO_BACKSLASH_ESCAPES");
        $pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

        return $pdo;
    }
}