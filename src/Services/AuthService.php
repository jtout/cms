<?php

namespace App\Services;

use App\Traits\App;
use App\Facades\Cookies;
use App\Facades\Session;
use App\Entities\UserEntity;
use App\Interfaces\Services;
use App\Models\Backend\Roles;
use App\Models\Backend\Users;
use App\Factories\UserFactory;
use App\Factories\ModuleFactory;

class AuthService implements Services
{
    /**
     * @var array
     */
    protected $errors;

    /**
     * @return AuthService|mixed
     */
    public static function boot() :AuthService
    {
        return new self();
    }

    /**
     * @param string $type
     * @return void
     */
    public function initializeUserSession(string $type) :void
    {
        Session::setSessionType($type);

        if (!empty(user()->id())) {
            $user = Users::find(['id' => user()->id()]);

            if (!$user->is_enabled() || ($type == env('ADMIN_NAME') && !$user->can_access_backend())) {
                $this->logout();
            }
        }
        elseif (empty(Session::get('user.entity'))) {
            $user = $this->guest();
            Session::set('user.entity', serialize($user));
        }
    }

    /**
     * @param UserEntity $user
     * @param array $options
     * @return void
     */
    public function setUserSession(UserEntity $user, array $options = array()) :void
    {
        $user->setAccessLevel(App::authorizedAccessLevels($user->role()->access_level_id()));
        Session::set('user.entity', serialize($user));

        $hash = hash('sha512', SITENAME.user()->id());
        $hashedValue = user()->id().'#~#'.$hash;

        if (Session::getSessionType() == 'site') {
            Cookies::set(SITENAME.'_forum_login', $hashedValue, 0);
        }

        if (isset($options['remember_me']) && $options['remember_me'] == true) {
            Cookies::set(SITENAME.'_remember_me', $hashedValue, time() +2592000);
        }

        $user->setLastVisitDate(now());
        $user->setUpdatedBy(user()->id());
        $user->update();
    }

    /**
     * @return UserEntity
     */
    public function guest() :UserEntity
    {
        $roles = new Roles;
        $userPermissions = $roles->getRolesPermissions()->keyBy('action');
        $permissions = $roles->entity($userPermissions, ModuleFactory::class);

        $user = array(
            'id' => 0,
            'role_id' => 7,
            'is_enabled' => 0,
            'in_backend' => 0,
            'relations' => new \stdClass(),
            'access_level' => env('PUBLIC_ACCESS_LEVEL')
        );

        $user['relations']->permissions = $permissions;

        return UserFactory::build($user);
    }

    /**
     * @return UserEntity
     */
    public function user() :UserEntity
    {
        $userEntity = unserialize(Session::get('user.entity'));

        if (empty($userEntity)) {
            $userEntity = $this->guest();
        }

        return $userEntity;
    }

    /**
     * @param UserEntity $userToAuthenticate
     * @param array $options
     * @return bool
     */
    public function authenticateUser(UserEntity $userToAuthenticate, $options = array()) :bool
    {
        $filters = [
            'user_name' => $userToAuthenticate->username(),
        ];

        $userEntity = Users::find($filters, ['fields', 'permissions']);

        if (!$userEntity->id()) {
            $this->errors[] = 'Username does not exist!';
            return false;
        }

        if (!$userEntity->is_enabled()) {
            $this->errors[] = 'Account is not enabled!';
        }

        if (!$userEntity->is_activated()) {
            $this->errors[] = 'Account is not activated!';
        }

        if ($userEntity->is_enabled() && $userEntity->is_activated()) {
            if ($options['in_backend'] == 1 && !$userEntity->can_access_backend()) {
                $this->errors[] = 'Account do not have access to admin area!';
                return false;
            }

            if (password_verify($userToAuthenticate->password(), $userEntity->password())) {
                $this->setUserSession($userEntity, $options);
                return true;
            }
            else {
                $this->errors[] = 'Wrong password given!';
            }
        }

        return false;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function login(array $data) :bool
    {
        $username = isset($data['username']) ? App::sanitizeInput($data['username'], 'STRING') : '';
        $password = isset($data['password']) ? App::sanitizeInput($data['password'], 'STRING') : '';
        $inBackend = isset($data['in_backend']) ? App::sanitizeInput($data['in_backend'], 'INT') : '';

        $options = [
            'remember_me' => isset($data['remember_me']) ? true : false,
            'in_backend' => $inBackend
        ];

        $user = new UserEntity();
        $user->setUserName($username);
        $user->setPassword($password);

        $login = $this->authenticateUser($user, $options);

        if ($login === false) {
            flash_message('danger', implode('<br>', $this->errors));
        }

        return $login;
    }

    /**
     * @return bool
     */
    public function logout() :bool
    {
        Session::unset();

        if (Session::getSessionType() == 'site') {
            Cookies::delete(SITENAME.'_forum_login');
            Cookies::delete(SITENAME.'_remember_me');
            Cookies::delete(SITENAME.'_dark_mode');
        }

        return true;
    }
}