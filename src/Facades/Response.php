<?php

namespace App\Facades;

/**
 * @method static \Psr\Http\Message\ResponseInterface self()
 */

class Response extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'response';
    }
}