<?php

namespace App\Facades;

/**
 * @method static \App\Services\JoomlaService self()
 * @method static beginTransaction()
 * @method static commit()
 * @method static rollback()
 * @method static prepare($statement)
 */

class Joomla extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'joomla';
    }
}