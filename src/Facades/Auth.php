<?php

namespace App\Facades;

/**
 * @method static void logout
 * @method static bool login(array $data)
 * @method static \App\Entities\UserEntity user()
 * @method static \App\Services\AuthService self()
 * @method static \App\Entities\UserEntity guest()
 * @method static void initializeUserSession(string $type)
 * @method static void setUserSession(\App\Entities\UserEntity $user, array $options = array())
 * @method static bool authenticateUser(\App\Entities\UserEntity $userToAuthenticate, $options = array())
 */

class Auth extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'auth';
    }
}