<?php

namespace App\Facades;

/**
 * @method static \Elasticsearch\Client self()
 * @method static string getIndex()
 * @method static array setParams(string $method, \App\Entities\NodeEntity $node, string $query = '')
 */

class ElasticSearch extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'elasticSearch';
    }
}