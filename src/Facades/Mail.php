<?php

namespace App\Facades;

/**
 * @method static \App\Services\MailService self()
 * @method static bool send(\App\Entities\MailEntity $mailEntity)
 */

class Mail extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'mail';
    }
}