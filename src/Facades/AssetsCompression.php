<?php

namespace App\Facades;

/**
 * @method static minifyAndCombineCommonAssets()
 * @method static minifyAndCombineArticlesAssets()
 */

class AssetsCompression extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'assetsCompression';
    }
}