<?php

namespace App\Facades;

/**
 * @method static mixed get(string $key)
 * @method static bool delete(string $key)
 * @method static \App\Services\CookiesService self()
 * @method static bool set(string $key, $value, $expire = '')
 */

class Cookies extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'cookies';
    }
}