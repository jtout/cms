<?php

namespace App\Facades;

/**
 * @method static \Illuminate\Database\Query\Builder table(string $table, $connection = null)
 * @method static transaction(\Closure $callback, $attempts = 1)
 * @method static beginTransaction()
 * @method static commit()
 * @method static rollback()
 */

class DB extends Facade
{
    /**
     * @return \Illuminate\Database\Connection
     */
    public static function self() :\Illuminate\Database\Connection
    {
        return self::$app->getContainer()->get('db')->getConnection();
    }

    /**
     * @param $method
     * @param $args
     * @return mixed
     */
    public static function __callStatic($method, $args)
    {
        $instance = self::self();

        return $instance->$method(...$args);
    }
}