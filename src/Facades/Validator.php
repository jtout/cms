<?php

namespace App\Facades;

/**
 * @method static \App\Services\ValidatorService self()
 * @method static \Illuminate\Validation\Factory getFactory()
 * @method static \Illuminate\Contracts\Validation\Validator make(array $data, array $rules, array $messages = [], array $customAttributes = [])
 * @method static void extend(string $rule, \Closure|string $extension, string $message = null)
 * @method static void extendImplicit(string $rule, \Closure|string $extension, string $message = null)
 * @method static void replacer(string $rule, \Closure|string $replacer)
 *
 */
class Validator extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'validator';
    }
}