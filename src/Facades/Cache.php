<?php

namespace App\Facades;

/**
 * @method static array getKeys($matchWithDb = false)
 * @method static array flush()
 * @method static bool set($key, $value)
 * @method static bool delete($key)
 * @method static mixed get(string $key)
 * @method static mixed setExpirationTime(string $key)
 * @method static mixed remember($key, \Closure $callback, $minutes = null)
 * @method static \App\Services\CacheService self()
 * @method static array getCommonCachedData(\App\Entities\NodeEntity $node, array $data)
 */

class Cache extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'cache';
    }
}