<?php

namespace App\Facades;

/**
 * @method static bool detect()
 */

class DetectAttacks extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'detect_attacks';
    }
}