<?php

namespace App\Facades;

/**
 * @method static \App\Services\RedirectResponseService self()
 * @method static \App\Services\RedirectResponseService redirect($url, $status = null)
 * @method static \App\Services\RedirectResponseService back($status = 302, $headers = [])
 */

class Redirect extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'redirectResponse';
    }
}