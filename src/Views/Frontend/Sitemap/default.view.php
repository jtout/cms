<urlset
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    <?php foreach ($data['nodes'] as $node) :?>
        <?php if($node->is_visible()) : ?>
        <url>
            <loc><?= $node->path(true); ?></loc>
            <lastmod><?= gmdate('Y-m-d', strtotime($node->updated_on()))  ?></lastmod>
            <priority>1</priority>
            <?php if(!empty($node->translations())) :?>
                <xhtml:link rel="alternate" hreflang="<?= $node->language_iso_code() ?>" href="<?= $node->path(true) ?>"/>
                <?php foreach ($node->translations() as $translation)  :?>
                    <xhtml:link rel="alternate" hreflang="<?= $translation->language_iso_code() ?>" href="<?= $translation->path(true) ?>"/>
                <?php endforeach; ?>
            <?php endif; ?>
        </url>
        <?php endif; ?>
    <?php endforeach; ?>
</urlset>