<urlset
    xmlns:news="http://www.google.com/schemas/sitemap-news/0.9"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    <?php foreach ($data['nodes'] as $node) :?>
        <?php if($node->is_visible()) : ?>
        <url>
            <loc><?= $node->path(true) ?></loc>
            <news:news>
                <news:publication>
                    <news:name>Unboxholics.com</news:name>
                    <news:language><?= $node->language_iso_code() ?></news:language>
                </news:publication>
                <news:genres>Blog, Opinion</news:genres>
                <news:publication_date><?= gmdate('Y-m-d', strtotime($node->published_on())) ?></news:publication_date>
                <news:title><?= htmlspecialchars($node->title()) ?></news:title>
                <news:keywords>
                    <?php $tags = array(); ?>
                    <?php foreach ($node->tags() as $tag) :?>

                        <?php $tags[] = htmlspecialchars($tag->title()); ?>
                    <?php endforeach; ?>
                    <?= implode(', ', $tags) ?>
                </news:keywords>
            </news:news>
        </url>
        <?php endif; ?>
    <?php endforeach; ?>
</urlset>
