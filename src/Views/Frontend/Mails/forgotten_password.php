<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Notifications</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="<?= SITENAME ?> Notifications" name="description" />
    <meta content="Unboxholics" name="author" />
    <?php require_once DIR.'/src/Views/Backend/inc/global_css.php' ?>
</head>
<!-- END HEAD -->
<body>

<!-- BEGIN CONTAINER -->
<div class="page-container"  style="height: 100vh; background-color: #ffffff">
    <div class="page-content-wrapper" style="padding-top: 20px; background-color: #ffffff">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content" style="width: 100%; margin: 0 auto">
            <!-- BEGIN PAGE HEADER-->
            <h1 class="page-title"> Notifications</h1>
            <span>You requested to reset your password through our system.</span> <br>
            <span>If that was not you, please contact us at <?= $systemMail ?>.
            <strong>You can now login using the following password: <?= $password ?></strong></span>
        </div>
        <!-- END CONTENT BODY -->
    </div>
</div>
<!-- END CONTAINER -->

</body>

</html>