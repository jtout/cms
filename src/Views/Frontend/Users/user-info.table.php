<table class="table">
    <thead>
    <tr>
        <th scope="col">Username</th>
        <th scope="col">Email</th>
        <th scope="col">First Name</th>
        <th scope="col">Last Name</th>
        <th scope="col">Gender</th>
        <th scope="col">Privacy Policy</th>
        <th scope="col">Facebook</th>
        <th scope="col">Twitter</th>
        <th scope="col">Instagram</th>
        <th scope="col">PSN ID</th>
        <th scope="col">Xbox Live Gamertag</th>
        <th scope="col">Nintendo Id</th>
        <th scope="col">Steam Id</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?= $user->username() ?></td>
        <td><?= $user->email() ?></td>
        <td><?= $user->name() ?></td>
        <td><?= $user->last_name() ?></td>
        <td><?= \App\Traits\App::genders()[$user->gender()] ?></td>
        <td><?= $user->privacy_policy() == 1 ? 'Accepted' : 'Not Accepted'?></td>
        <td><?= $user->facebook() ?></td>
        <td><?= $user->twitter() ?></td>
        <td><?= $user->instagram() ?></td>
        <td><?= $user->psn_id() ?></td>
        <td><?= $user->xbox_live_gamertag() ?></td>
        <td><?= $user->nintendo_id() ?></td>
        <td><?= $user->steam_id() ?></td>
    </tr>
    </tbody>
</table>