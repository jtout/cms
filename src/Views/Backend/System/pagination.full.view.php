<?php
$paginate = '';
$node = session()->getNode()->name();

if (isset($paginator) && $paginator->lastPage() > 1) {
    $stages = 3;
    $page = $paginator->currentPage();
    if ($page == 0) {
        $page = 1;
    }

    $prev = $page - 1;
    $next = $page + 1;
    $lastpage = ceil($paginator->total() / $paginator->perPage());
    $LastPagem1 = $lastpage - 1;

    $meta_next = $next <= $lastpage ? '<link href="' . $paginator->url($next) . '" rel="next">' : '';
    $meta_prev = $page != 1 ? '<link href="' . $paginator->url($prev) . '" rel="prev">' : '';

    session()->set($node.'.next', $meta_next);
    session()->set($node.'.prev', $meta_next);

    if ($lastpage > 0) {
        $paginate .= '<ul class="pagination ">';
        if ($page > 1) {
            $paginate .= '<li><a href="' . $paginator->url(1) . '"> <i class="fa fa-angle-double-left"></i> </a></li>';
            $paginate .= '<li><a href="' . $paginator->url($prev) . '"> <i class="fa fa-angle-left"></i> </a></li>';
        }
        else {
            $paginate .= '<li class="disabled"><span> <i class="fa fa-angle-double-left"></i> </span></li>';
            $paginate .= '<li class="disabled"><span> <i class="fa fa-angle-left"></i> </span></li>';
        }
        if ($lastpage < 7 + ($stages * 2)) {
            for ($counter = 1; $counter <= $lastpage; $counter++) {
                if ($counter == $page) {
                    $paginate .= '<li class="active"><span>' . $counter . '</span></li>';
                }
                else {
                    $paginate .= '<li><a href="' . $paginator->url($counter) . '"><span>' . $counter . '</span></a></li>';
                }
            }
        }
        elseif ($lastpage > 5 + ($stages * 2)) {
            if ($page < 1 + ($stages * 2)) {
                for ($counter = 1; $counter < 4 + ($stages * 2); $counter++) {
                    if ($counter == $page) {
                        $paginate .= '<li class="active"><span>' . $counter . '</span></li>';
                    }
                    else {
                        $paginate .= '<li><a href="' . $paginator->url($counter) . '"><span>' . $counter . '</span></a></li>';
                    }
                }
                $paginate .= '<li class="more-links"><span>...</span></li>';
                $paginate .= '<li><a href="' . $paginator->url($LastPagem1) . '">' . $LastPagem1 . '</a></li>';
                $paginate .= '<li><a href="' . $paginator->url($lastpage) . '">' . $lastpage . '</a></li>';
            }
            elseif ($lastpage - ($stages * 2) > $page && $page > ($stages * 2)) {
                $paginate .= '<li><a href="' . $paginator->url(1) . '">1</a></li>';
                $paginate .= '<li><a href="' . $paginator->url(2) . '">2</a></li>';
                $paginate .= '<li class="more-links"><span>...</span></li>';
                for ($counter = $page - $stages; $counter <= $page + $stages; $counter++) {
                    if ($counter == $page) {
                        $paginate .= '<li class="active"><span>' . $counter . '</span></li>';
                    }
                    else {
                        $paginate .= '<li><a href="' . $paginator->url($counter) . '"><span>' . $counter . '</span></a></li>';
                    }
                }
                $paginate .= '<li class="more-links"><span>...</span></li>';
                $paginate .= '<li><a href="' . $paginator->url($LastPagem1) . '">' . $LastPagem1 . '</a></li>';
                $paginate .= '<li><a href="' . $paginator->url($lastpage) . '">' . $lastpage . '</a></li>';
            }
            else {
                $paginate .= '<li><a href="' . $paginator->url(1) . '">1</a></li>';
                $paginate .= '<li><a href="' . $paginator->url(2) . '">2</a></li>';
                $paginate .= '<li class="more-links"><span>...</span></li>';
                for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++) {
                    if ($counter == $page) {
                        $paginate .= '<li class="active"><span class="">' . $counter . '</span></li>';
                    }
                    else {
                        $paginate .= '<li><a href="' . $paginator->url($counter) . '"><span>' . $counter . '</span></a></li>';
                    }
                }
            }
        }
        if ($page < $counter - 1) {
            $paginate .= '<li><a href="' . $paginator->url($next) . '""> <i class="fa fa-angle-right"></i> </a></li>';
            $paginate .= '<li><a href="' . $paginator->url($lastpage) . '""> <i class="fa fa-angle-double-right"></i> </a></li>';
        }
        else {
            $paginate .= '<li class="disabled"><span> <i class="fa fa-angle-right"></i> </span></a></li>';
            $paginate .= '<li class="disabled"><span> <i class="fa fa-angle-double-right"></i> </span></li>';
        }
        $paginate .= '</ul>';
    }
}

echo $paginate;