<?php if (isset($paginator) && $paginator->lastPage() > 1) : ?>

<ul class="pagination">
    <?php
		$interval = isset($interval) ? abs(intval($interval)) : 3 ;
		$from = $paginator->currentPage() - $interval;
		if ($from < 1){
			$from = 1;
		}

		$to = $paginator->currentPage() + $interval;
		if ($to > $paginator->lastPage()){
			$to = $paginator->lastPage();
		}
    ?>

    <!-- first/previous -->
    <?php $class = $paginator->currentPage() > 1 ? '' : 'disabled'; ?>
    <li class="<?= $class ?>">
        <a href="<?= $class == 'disabled' ? '#' : $paginator->url(1) ?>" aria-label="First">
            <span aria-hidden="true">&laquo;</span>
        </a>
    </li>

    <li class="<?= $class ?>">
        <a href="<?= $class == 'disabled' ? '#' : $paginator->url($paginator->currentPage() - 1) ?>" aria-label="Previous">
            <span aria-hidden="true">&lsaquo;</span>
        </a>
    </li>

	<!-- links -->
	<?php if ($paginator->currentPage() > 3) : ?>
	<li class="hidden-xs"><a href="<?= $paginator->url(1) ?>">1</a></li>
    <?php endif; ?>

	<?php if ($paginator->currentPage() > 4) : ?>
	<li><span>...</span></li>
    <?php endif; ?>

	<?php foreach (range(1, $paginator->lastPage()) as $i) : ?>
        <?php if ($i >= $paginator->currentPage() - 2 && $i <= $paginator->currentPage() + 2) : ?>
            <?php if ($i == $paginator->currentPage()) : ?>
				<li class="active"><span><?= $i ?></span></li>
			<?php else : ?>
				<li><a href="<?= $paginator->url($i) ?>"><?= $i ?></a></li>
        	<?php endif; ?>
        <?php endif; ?>
	<?php endforeach; ?>

	<?php if ($paginator->currentPage() < $paginator->lastPage() - 3) : ?>
	<li><span>...</span></li>
	<?php endif; ?>

	<?php if ($paginator->currentPage() < $paginator->lastPage() - 2) : ?>
	<li class="hidden-xs"><a href="<?= $paginator->url($paginator->lastPage()) ?>"><?= $paginator->lastPage() ?></a></li>
	<?php endif; ?>

    <!-- next/last -->
    <?php $class = $paginator->currentPage() < $paginator->lastPage() ? '' : 'disabled'; ?>
	<li class="<?= $class ?>">
		<a href="<?= $class == 'disabled' ? '#' : $paginator->url($paginator->currentPage() + 1) ?>" aria-label="Next">
			<span aria-hidden="true">&rsaquo;</span>
		</a>
	</li>

    <li class="<?= $class ?>">
		<a href="<?= $class == 'disabled' ? '#' : $paginator->url($paginator->lastpage()) ?>" aria-label="Last">
			<span aria-hidden="true">&raquo;</span>
		</a>
	</li>
</ul>

<?php endif; ?>