<?php if (isset($paginator) && $paginator->lastPage() != $paginator->currentPage()) : ?>
    <a href="<?= request()->getUri()->getPath().'?p='.($paginator->currentPage() + 1) ?>" class="pagination-link btn btn-lg btn-dark"><span>Δειτε Περισσοτερα</span></a>
<?php endif; ?>