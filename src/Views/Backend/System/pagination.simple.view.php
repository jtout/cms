<?php if (isset($paginator) && $paginator->lastPage() > 1) : ?>
    <ul class="pagination">
        <li class="<?= $class ?>">
            <a href="<?= $class == 'disabled' ? '#' : $paginator->url($paginator->currentPage() - 1) ?>" aria-label="Previous">
                <span aria-hidden="true">&lsaquo;</span>
            </a>
        </li>

        <?php $class = $paginator->currentPage() < $paginator->lastPage() ? '' : 'disabled'; ?>
        <li class="<?= $class ?>">
            <a href="<?= $class == 'disabled' ? '#' : $paginator->url($paginator->currentPage() + 1) ?>" aria-label="Next">
                <span aria-hidden="true">&rsaquo;</span>
            </a>
        </li>
    </ul>
<?php endif; ?>