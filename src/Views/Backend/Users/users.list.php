<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title><?= SITENAME ?> | <?= $node->title() ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="<?= SITENAME ?> Dashboard" name="description" />
		<meta content="" name="author" />
		<?php require_once DIR.'/src/Views/Backend/inc/global_css.php' ?>
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

		<!-- END PAGE LEVEL PLUGINS -->
	</head>
	<!-- END HEAD -->

	<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-fixed page-md <?= session()->get('user.side_menu_open')  === true ? 'page-sidebar-closed' : ''?>">

		<!-- BEGIN HEADER -->
		<div class="page-header navbar navbar-fixed-top">
			<!-- BEGIN HEADER INNER -->
			<div class="page-header-inner ">
				<!-- BEGIN LOGO -->
				<?php require(DIR . '/src/Views/Backend/inc/logo.v.php'); ?>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN PAGE ACTIONS -->
				<!-- DOC: Remove "hide" class to enable the page header actions -->
				<?php require_once DIR. '/src/Views/Backend/inc/dropdown.v.php'?>
				<!-- END PAGE ACTIONS -->
				<!-- BEGIN PAGE TOP -->
				<div class="page-top">
					<!-- BEGIN TOP NAVIGATION MENU -->
					<?php require_once DIR. '/src/Views/Backend/inc/top_menu.v.php'?>
					<!-- END TOP NAVIGATION MENU -->
				</div>
				<!-- END PAGE TOP -->
			</div>
			<!-- END HEADER INNER -->
		</div>
		<!-- END HEADER -->
		<!-- BEGIN HEADER & CONTENT DIVIDER -->
		<div class="clearfix"> </div>
		<!-- END HEADER & CONTENT DIVIDER -->
		<!-- BEGIN CONTAINER -->
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/sidebar.v.php' ?>
			<!-- END SIDEBAR -->
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<!-- BEGIN PAGE HEADER-->
					<h1 class="page-title"> <?= $node->title() ?>
						<small></small>
					</h1>
					<!-- BEGIN BREADCRUMBS -->
					<?php require_once ( DIR . '/src/Views/Backend/inc/breadcrumbs.v.php' ); ?>
					<!-- END BREADCRUMBS -->
					<?php if(!empty($messages)) : ?>
						<?php foreach($messages as $type => $msgs) : ?>
							<?php foreach($msgs as $msg) : ?>
								<div class="alert alert-<?= $type ?>">
									<button class="close" data-close="alert"></button>
									<span><strong><?= $msg ?></strong></span>
								</div>
							<?php endforeach; ?>
						<?php endforeach; ?>
					<?php endif; ?>
					<!-- END PAGE HEADER-->

					<div class="row">
						<div class="col-md-12">
							<div class="table-scrollable">
								<div class="portlet light ">
									<div class="portlet-title">
										<div class="caption">
											<form id="search_list" name="search_list" class="form-inline" action='<?= $node->path() ?>?action=showRolesList' method="GET">
                                                <div class="form-group">
													<input type="text" class="form-control" name="filters[filter_title]" id="filter_title"
														   value="<?= session()->get($node->name().'.filters.filter_title') ?>" placeholder="Search Username/Email..." />
												</div>
												<div class="form-group">
                                                    <select name="filters[filter_role]" id="filter_role">
                                                        <option value="">Select Role</option>
                                                        <?php foreach ($data['roles'] as $role) : ?>
                                                            <option value="<?= $role->id() ?>" <?= $role->id() == session()->get($node->name().'.filters.filter_role') ? 'selected' : '' ?>>
                                                                <?= $role->title() ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
												</div>
                                                <div class="form-group">
                                                    <select id="filter_active" class="form-control" name="filters[filter_active]">
                                                        <option value="" >Select Active Status</option>
                                                        <option value="1" <?= session()->get($node->name().'.filters.filter_active') == 1 ? 'selected' : '' ?>>Active</option>
                                                        <option value="2" <?= session()->get($node->name().'.filters.filter_active') == 2 ? 'selected' : '' ?>>Not Active</option>
                                                    </select>
                                                </div>
												<div class="form-group">
													<select id="filter_activated" class="form-control" name="filters[filter_activated]">
														<option value="" >Select Activated Status</option>
														<option value="1" <?= session()->get($node->name().'.filters.filter_activated') == 1 ? 'selected' : '' ?>>Activated</option>
														<option value="2" <?= session()->get($node->name().'.filters.filter_activated') == 2 ? 'selected' : '' ?>>Not Activated</option>
													</select>
												</div>
												<div class="form-group">
													<select id="filter_backend" class="form-control" name="filters[filter_backend]">
														<option value="" >Select Backend Status</option>
														<option value="1" <?= session()->get($node->name().'.filters.filter_backend') == 1 ? 'selected' : '' ?>>In Backend</option>
														<option value="2" <?= session()->get($node->name().'.filters.filter_backend') == 2 ? 'selected' : '' ?>>Not in backend</option>
													</select>
												</div>
											</form>
										</div>
									</div>
									<form id="users_list_form" name="users_list_form" method="POST" action='<?= $node->path() ?>?action=deleteUser'>
										<div class="actions pull-right" style="padding-bottom:10px">
											<input type="hidden" name="csrf_name" value="<?php echo $csrf_name; ?>">
											<input type="hidden" name="csrf_value" value="<?php echo $csrf_value; ?>">
                                            <?php if(user()->has_permission_to('addUser')) : ?>
                                                <a class="btn btn-transparent green btn-outline  btn-sm active" href="<?= $node->path() ?>?action=addUser">Add new user</a>
                                            <?php endif; ?>
                                            <?php if(user()->has_permission_to('deleteUser')) : ?>
                                                <button id="delete_all" type="submit" class="btn btn-transparent red btn-outline  btn-sm active">Delete user</button>
                                            <?php endif; ?>
										</div>
										<div id="users_table" name="users_table" class="portlet-body table-responsive" style="width: 100%">
											<div><?= $data['rows']->fullPagination ?></div>
											<table class="table table-striped table-hover margin-top-20">
												<thead>
													<tr>
														<th>
															<div class="form-group form-md-checkboxes" style="margin: 0 auto;width: 20px;padding-top: 0;">
																<div class="md-checkbox-list" style="margin: 2px auto;">
																	<div class="md-checkbox">
																		<input id="select_all" type="checkbox" class="md-check item-checkbox">
																		<label for="select_all"><span></span><span class="check"></span><span class="box"></span></label>
																	</div>
																</div>
															</div>
														</th>
														<th>
															<a href="<?= navigation(true, false) ?>&s=users_id">ID
																<?= sorting() == 'users_id' ? sorting_icon() : sorting_icon_default() ?>
															</a>
														</th>
                                                        <th>
                                                            <a href="<?= navigation(true, false) ?>&s=users_username">Username
                                                                <?= sorting() == 'users_username' ? sorting_icon() : sorting_icon_default() ?>
                                                            </a>
                                                        </th>
                                                        <th>
                                                            <a href="<?= navigation(true, false) ?>&s=users_name">Name
                                                                <?= sorting() == 'users_name' ? sorting_icon() : sorting_icon_default() ?>
                                                            </a>
                                                        </th>
														<th>
															<a href="<?= navigation(true, false) ?>&s=users_role">Role
																<?= sorting() == 'users_role' ? sorting_icon() : sorting_icon_default() ?>
															</a>
														</th>
														<th>
															<a href="<?= navigation(true, false) ?>&s=users_active">Is Enabled
																<?= sorting() == 'users_active' ? sorting_icon() : sorting_icon_default() ?>
															</a>
														</th>
														<th>
															<a href="<?= navigation(true, false) ?>&s=users_active">Is Activated
                                                                <?= sorting() == 'users_activated' ? sorting_icon() : sorting_icon_default() ?>
															</a>
														</th>
														<th>
															<a href="<?= navigation(true, false) ?>&s=users_backend">In Backend
																<?= sorting() == 'users_backend' ? sorting_icon() : sorting_icon_default() ?>
															</a>
														</th>
                                                        <th>
                                                            <a href="<?= navigation(true, false) ?>&s=users_registration_date">Registration Date
                                                                <?= sorting() == 'users_registration_date' ? sorting_icon() : sorting_icon_default() ?>
                                                            </a>
                                                        </th>
													</tr>
												</thead>
												<tbody>
													<?php if(!empty($data['rows']->items())) : ?>
														<?php foreach ($data['rows']->items() as $user) : ?>
															<tr>
																<td>
																	<div class="form-group form-md-checkboxes" style="margin: 0 auto;width: 20px;padding-top: 0;">
																		<div class="md-checkbox-list" style="margin: 2px auto;">
																			<div class="md-checkbox">
																				<input name="user_checkbox[<?= $user->id() ?>]" id="<?= $user->id() ?>" value="<?= $user->role_id() ?>" type="checkbox" class="md-check item-checkbox">
																				<label for="<?= $user->id() ?>"><span></span><span class="check"></span><span class="box"></span></label>
																			</div>
																		</div>
																	</div>
																</td>
																<td><?= $user->id() ?></td>
                                                                <td>
                                                                    <?php if(user()->has_permission_to('updateUser')) : ?>
                                                                        <a href="<?= $node->path() ?>?action=editUser&id=<?= $user->id() ?>">
                                                                            <?= $user->username() ?>
                                                                        </a>
                                                                    <?php else : ?>
                                                                        <?= $user->username() ?>
                                                                    <?php endif; ?>
                                                                </td>
                                                                <td><?= $user->name().'&nbsp'.$user->last_name() ?></td>
																<td><?= $user->role()->title() ?></td>
																<td><i class="fa fa-<?= ($user->is_enabled() == 1) ? 'check' : 'times'; ?> font-<?= ($user->is_enabled() == 1) ? 'green-jungle' : 'red-thunderbird'; ?> font-lg"></i></td>
																<td><i class="fa fa-<?= ($user->is_activated() == 1) ? 'check' : 'times'; ?> font-<?= ($user->is_activated() == 1) ? 'green-jungle' : 'red-thunderbird'; ?> font-lg"></i></td>
																<td><i class="fa fa-<?= ($user->in_backend() == 1) ? 'check' : 'times'; ?> font-<?= ($user->in_backend() == 1) ? 'green-jungle' : 'red-thunderbird'; ?> font-lg"></i></td>
                                                                <td><?= $user->created_on() ?></td>
                                                            </tr>
														<?php endforeach; ?>
													<?php endif; ?>
												</tbody>
											</table>
											<div><?= $data['rows']->fullPagination ?></div>
											<div class="margin-top-20">Total rows: <?= $data['rows']->total() ?></div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->
			<!-- BEGIN QUICK SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/quick_sidebar.v.php' ?>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->

		<!-- BEGIN FOOTER -->
		<?php require_once DIR. '/src/Views/Backend/inc/footer.v.php' ?>
		<!-- END FOOTER -->

		<?php require_once DIR.'/src/Views/Backend/inc/global_js.php' ?>

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/pages/js/users.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->

	</body>

</html>