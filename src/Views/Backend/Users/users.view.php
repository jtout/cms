<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title><?= SITENAME ?> | <?= $node->title() ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="<?= SITENAME ?> Dashboard" name="description" />
		<meta content="" name="author" />
		<?php require_once DIR.'/src/Views/Backend/inc/global_css.php' ?>
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/icheck/skins/all.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css" />

		<!-- END PAGE LEVEL PLUGINS -->
	</head>
	<!-- END HEAD -->

	<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-fixed page-md <?= session()->get('user.side_menu_open')  === true ? 'page-sidebar-closed' : ''?>">

		<!-- BEGIN HEADER -->
		<div class="page-header navbar navbar-fixed-top">
			<!-- BEGIN HEADER INNER -->
			<div class="page-header-inner ">
				<!-- BEGIN LOGO -->
				<?php require(DIR . '/src/Views/Backend/inc/logo.v.php'); ?>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN PAGE ACTIONS -->
				<!-- DOC: Remove "hide" class to enable the page header actions -->
				<?php require_once DIR. '/src/Views/Backend/inc/dropdown.v.php'?>
				<!-- END PAGE ACTIONS -->
				<!-- BEGIN PAGE TOP -->
				<div class="page-top">
					<!-- BEGIN TOP NAVIGATION MENU -->
					<?php require_once DIR. '/src/Views/Backend/inc/top_menu.v.php'?>
					<!-- END TOP NAVIGATION MENU -->
				</div>
				<!-- END PAGE TOP -->
			</div>
			<!-- END HEADER INNER -->
		</div>
		<!-- END HEADER -->
		<!-- BEGIN HEADER & CONTENT DIVIDER -->
		<div class="clearfix"> </div>
		<!-- END HEADER & CONTENT DIVIDER -->
		<!-- BEGIN CONTAINER -->
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/sidebar.v.php' ?>
			<!-- END SIDEBAR -->
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<!-- BEGIN PAGE HEADER-->
					<h1 class="page-title"> <?= $node->title() ?>
						<small></small>
					</h1>
					<!-- BEGIN BREADCRUMBS -->
					<?php require_once ( DIR . '/src/Views/Backend/inc/breadcrumbs.v.php' ); ?>
					<!-- END BREADCRUMBS -->
					<?php if(!empty($messages)) : ?>
						<?php foreach($messages as $type => $msgs) : ?>
							<?php foreach($msgs as $msg) : ?>
								<div class="alert alert-<?= $type ?>">
									<button class="close" data-close="alert"></button>
									<span><strong><?= $msg ?></strong></span>
								</div>
							<?php endforeach; ?>
						<?php endforeach; ?>
					<?php endif; ?>
					<!-- END PAGE HEADER-->

					<div class="row">
						<div class="col-md-12">
							<div class="portlet light ">
								<div class="portlet-title">
									<div class="caption">
										<span class="caption-subject font-red-sunglo bold"><?= $data['user']->username() ?></span>
									</div>
								</div>
								<div class="portlet-body form">
									<form id="roles_form" role="form" action="<?= $node->path() ?>?action=updateUser" method="POST" class="tab-content" enctype="multipart/form-data">
										<input type="hidden" name="csrf_name" value="<?php echo $csrf_name; ?>">
										<input type="hidden" name="csrf_value" value="<?php echo $csrf_value; ?>">
										<input type="hidden" id="selected_permissions" name="selected_permissions" value="" />
                                        <input type="hidden" name="id" value="<?= $data['user']->id() ?>" />

										<div class="tabbable-line tabbable-full-width">
											<ul class="nav nav-tabs">
												<li class="active">
													<a href="#tab1" data-toggle="tab"> Main Info </a>
												</li>
												<li class="">
													<a href="#tab2" data-toggle="tab"> Permissions </a>
												</li>
												<li class="">
													<a href="#tab3" data-toggle="tab"> Additional Info </a>
												</li>
												<li class="">
													<a href="#tab4" data-toggle="tab"> Social Ids </a>
												</li>
												<li class="">
													<a href="#tab5" data-toggle="tab"> My Things </a>
												</li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane active" id="tab1">
													<div class="row">
														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="control-label">Username</label>
																<input type="text" name="user_name" value="<?= $data['user']->username() ?>" placeholder="Username" class="form-control" />
															</div>
														</div>

														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="control-label">Email</label>
																<input type="text" name="email" value="<?= $data['user']->email() ?>" placeholder="Email" class="form-control" />
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="control-label">Name</label>
																<input type="text" name="name" value="<?= $data['user']->name() ?>" placeholder="Name" class="form-control" />
															</div>
														</div>

														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="control-label">Last Name</label>
																<input type="text" name="last_name" value="<?= $data['user']->last_name() ?>" placeholder="Last Name" class="form-control" />
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="control-label">Password</label>
																<input type="password" name="password" value="" placeholder="Password" class="form-control" />
															</div>
														</div>

														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="control-label">Repeat Password</label>
																<input type="password" name="password_confirmation" value="" placeholder="Password" class="form-control" />
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="" for="in_backend">In Backend</label><p />
																<input id="in_backend" name="in_backend" type="checkbox" class="make-switch" data-size="normal" data-on-text="Yes" data-off-text="No" data-on-color="success" data-off-color="danger" <?= $data['user']->in_backend() == 1 ? 'checked' : '' ?>>
															</div>
														</div>

														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="" for="is_enabled">Enabled</label><p />
																<input id="is_enabled" name="is_enabled" type="checkbox" class="make-switch" data-size="normal" data-on-text="Yes" data-off-text="No" data-on-color="success" data-off-color="danger" <?= $data['user']->is_enabled() == 1 ? 'checked' : '' ?>>
															</div>
														</div>

                                                        <div class="col-xs-6 col-sm-3 col-lg-3">
                                                            <div class="form-group">
                                                                <label class="" for="is_enabled">Activated</label><p />
                                                                <input id="is_activated" name="is_activated" type="checkbox" class="make-switch" data-size="normal" data-on-text="Yes" data-off-text="No" data-on-color="success" data-off-color="danger" <?= $data['user']->is_activated() == 1 ? 'checked' : '' ?>>
                                                            </div>
                                                        </div>

														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="" for="privacy_policy">Privacy policy</label><p />
																<input id="privacy_policy" name="privacy_policy" type="checkbox" class="make-switch" data-size="normal" data-on-text="Yes" data-off-text="No" data-on-color="success" data-off-color="danger" <?= $data['user']->privacy_policy() == 1 ? 'checked' : '' ?>>
															</div>
														</div>
													</div>
												</div>
												<div class="tab-pane" id="tab2">
													<div class="row">
														<div class="col-xs-6 col-sm-3 col-lg-3">
															<label class="" for="role_id">Role</label>
															<select name="role_id" id="role_id">
															<?php foreach ($data['roles'] as $role) : ?>
																<option value="<?= $role->id() ?>" <?= $role->id() == $data['user']->role_id() ? 'selected' : '' ?>>
																	<?= $role->title() ?>
																</option>
															<?php endforeach; ?>
															</select>
														</div>
													</div>

													<div class="row" style="padding-top: 30px;">
														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<div id="roles_tree">
																	<?= $data['permissions'] ?>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="tab-pane" id="tab3">
													<div class="row">
														<div class="col-xs-6 col-sm-3 col-lg-3">
															<?php if(!empty($data['user']->avatar())) : ?>
																<div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">
																	<img src="<?= !empty($data['user']->avatar()) ? '/media/source/Profiles/'.$data['user']->id().'/images/originalImage/'.$data['user']->avatar() : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' ?>" alt="" />
                                                                </div>
																<label class="">Remove Avatar</label>
																<div class="form-group">
																	<input name="remove_avatar" type="checkbox" value="1" data-on-text="Yes" data-off-text="No" class="make-switch"  data-on-color="success" data-off-color="danger" <?= !empty($data['user']->avatar()) ? '' : 'checked' ?> >
																</div>
															<?php endif; ?>
                                                            <input name="avatar" type="hidden" value="<?= $data['user']->avatar() ?>">
														</div>
													</div>

													<div class="row">
														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="control-label">Gender</label>
																<select id="gender" class="form-control" name="gender">
																	<option value="0" <?= $data['user']->gender() == 0 ? 'selected' : '' ?>>Select Gender</option>
																	<option value="1" <?= $data['user']->gender() == 1 ? 'selected' : '' ?>>Male</option>
																	<option value="2" <?= $data['user']->gender() == 2 ? 'selected' : '' ?>>Female</option>
																</select>
															</div>
														</div>

														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="">Location</label>
																<input name="location" type="text" class="form-control" value="<?= $data['user']->location() ?>" />
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-xs-6 col-sm-3 col-lg-2">
															<div class="form-group">
																<label class="control-label" for="birthdate">Birthdate</label>
																<div class="input-group input-date">
																	<input id="birthdate" name="birthdate" type="text" class="form-control form-control-inline"
																		   data-provide="datepicker" data-date-format="dd-mm-yyyy" value="<?= $data['user']->birthdate(true) ?>">
                                                                    <span class="add-on"><i class="icon-remove"></i></span>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-xs-6 col-sm-3 col-lg-2">
															<div class="form-group">
																<label class="control-label" for="description">Description</label>
																<textarea id="description" name="description" class="form-control" rows="5" cols="20"><?= $data['user']->description() ?></textarea>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-6 col-sm-3 col-lg-2">
															<div class="form-group">
																<label class="control-label" for="signature">Forum Signature</label>
																<textarea id="signature" name="signature" class="form-control" rows="5" cols="20"><?= $data['user']->signature() ?></textarea>
															</div>
														</div>
													</div>
												</div>
												<div class="tab-pane" id="tab4">
													<div class="row">
														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="">Facebook</label>
																<input id="facebook" type="text" name="facebook" class="form-control" value="<?= $data['user']->facebook() ?>" />
															</div>
														</div>

														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="">Twitter</label>
																<input id="twitter" type="text" name="twitter" class="form-control" value="<?= $data['user']->twitter() ?>" />
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="">Instagram</label>
																<input id="instagram" type="text" name="instagram" class="form-control" value="<?= $data['user']->instagram() ?>" />
															</div>
														</div>

														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="">Twitch</label>
																<input id="twitch" type="text" name="twitch" class="form-control" value="<?= $data['user']->twitch() ?>" />
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="">PSN ID</label>
																<input id="psn_id" type="text" name="psn_id" class="form-control" value="<?= $data['user']->psn_id() ?>" />
															</div>
														</div>

														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="">Xbox Live Gamertag</label>
																<input id="xbox_live_gamertag" type="text" name="xbox_live_gamertag" class="form-control" value="<?= $data['user']->xbox_live_gamertag() ?>" />
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="">Nintendo ID</label>
																<input id="nintendo_id" type="text" name="nintendo_id" class="form-control" value="<?= $data['user']->nintendo_id() ?>" />
															</div>
														</div>

														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="">Steam ID</label>
																<input id="steam_id" type="text" name="steam_id" class="form-control" value="<?= $data['user']->steam_id() ?>" />
															</div>
														</div>
													</div>
												</div>
												<div class="tab-pane" id="tab5">
													<div class="row">
														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="">PC</label>
																<textarea name="pc" id="pc" class="form-control"><?= $data['user']->pc() ?></textarea>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="">Tablet</label>
																<input id="tablet" type="text" name="tablet" class="form-control" value="<?= $data['user']->tablet() ?>" />
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-xs-6 col-sm-3 col-lg-3">
															<div class="form-group">
																<label class="">Smartphone</label>
																<input id="smartphone" type="text" name="smartphone" class="form-control" value="<?= $data['user']->smartphone() ?>" />
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>


										<!-- FORM ACTIONS -->
										<div class="form-actions">
											<div class="row">
												<div class="col-md-4">
													<button type="submit" id="submit" class="btn green">Save</button>
													<a href="<?= env('DOMAIN').'/'.env('ADMIN_NAME').'/users' ?>" class="btn default">Cancel</a>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->
			<!-- BEGIN QUICK SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/quick_sidebar.v.php' ?>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->

		<!-- BEGIN FOOTER -->
		<?php require_once DIR. '/src/Views/Backend/inc/footer.v.php' ?>
		<!-- END FOOTER -->

		<?php require_once DIR.'/src/Views/Backend/inc/global_js.php' ?>

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/pages/js/users.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->

	</body>

</html>