<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title><?= SITENAME ?> | <?= $node->title() ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="<?= SITENAME ?> <?= $node->title() ?>" name="description" />
    <meta content="" name="author" />
    <meta name="robots" content="noindex, nofollow"/>
    <?php require_once 'inc/global_css.php' ?>
    <!-- BEGIN PAGE LEVEL PLUGINS -->

    <!-- END PAGE LEVEL PLUGINS -->
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-fixed page-md <?= session()->get('user.side_menu_open')  === true ? 'page-sidebar-closed' : ''?>">

<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <?php require_once(DIR . '/src/Views/Backend/inc/logo.v.php'); ?>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN PAGE ACTIONS -->
        <!-- DOC: Remove "hide" class to enable the page header actions -->
        <?php require_once DIR. '/src/Views/Backend/inc/dropdown.v.php'?>
        <!-- END PAGE ACTIONS -->
        <!-- BEGIN PAGE TOP -->
        <div class="page-top">
            <!-- BEGIN TOP NAVIGATION MENU -->
            <?php require_once DIR. '/src/Views/Backend/inc/top_menu.v.php'?>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php require_once DIR. '/src/Views/Backend/inc/sidebar.v.php' ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <h1 class="page-title"> Dashboard
                <small></small>
            </h1>
            <!-- BEGIN BREADCRUMBS -->
            <?php require_once ( DIR . '/src/Views/Backend/inc/breadcrumbs.v.php' ); ?>
            <!-- END BREADCRUMBS -->
            <?php if (!empty($messages)) : ?>
                <?php foreach($messages as $type => $msgs) : ?>
                    <?php foreach($msgs as $msg) : ?>
                        <div class="alert alert-<?= $type ?>">
                            <button class="close" data-close="alert"></button>
                            <span><strong><?= $msg ?></strong></span>
                        </div>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat2 ">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-green-sharp">
                                    <span data-counter="counterup" data-value="<?= $data['content_numbers']['users'] ?>">0</span>
                                    <small class="font-green-sharp"></small>
                                </h3>
                                <small>USERS</small>
                            </div>
                            <div class="icon">
                                <i class="icon-users"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat2 ">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-green-sharp">
                                    <span data-counter="counterup" data-value="<?= $data['content_numbers']['articles'] ?>">0</span>
                                    <small class="font-green-sharp"></small>
                                </h3>
                                <small>ARTICLES</small>
                            </div>
                            <div class="icon">
                                <i class="icon-notebook"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat2 ">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-red-haze">
                                    <span data-counter="counterup" data-value="<?= $data['content_numbers']['categories'] ?>">0</span>
                                </h3>
                                <small>Categories</small>
                            </div>
                            <div class="icon">
                                <i class="icon-drawer"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat2 ">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-purple-soft">
                                    <span data-counter="counterup" data-value="<?= $data['content_numbers']['tags'] ?>">0</span>
                                </h3>
                                <small>TAGS</small>
                            </div>
                            <div class="icon">
                                <i class="icon-tag"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->
    <?php require_once 'inc/quick_sidebar.v.php' ?>
    <!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> <?= date("Y"); ?> &copy; <?= SITENAME ?>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <!-- END FOOTER -->

    <?php require_once 'inc/global_js.php' ?>

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
    <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
</div>
</body>

</html>