<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title><?= SITENAME ?> | <?= $node->title() ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="<?= SITENAME ?> Dashboard" name="description" />
		<meta content="" name="author" />
		<?php require_once DIR.'/src/Views/Backend/inc/global_css.php' ?>
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

		<!-- END PAGE LEVEL PLUGINS -->
	</head>
	<!-- END HEAD -->

	<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-fixed page-md <?= session()->get('user.side_menu_open')  === true ? 'page-sidebar-closed' : ''?>">

		<!-- BEGIN HEADER -->
		<div class="page-header navbar navbar-fixed-top">
			<!-- BEGIN HEADER INNER -->
			<div class="page-header-inner ">
				<!-- BEGIN LOGO -->
				<?php require(DIR . '/src/Views/Backend/inc/logo.v.php'); ?>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN PAGE ACTIONS -->
				<!-- DOC: Remove "hide" class to enable the page header actions -->
				<?php require_once DIR. '/src/Views/Backend/inc/dropdown.v.php'?>
				<!-- END PAGE ACTIONS -->
				<!-- BEGIN PAGE TOP -->
				<div class="page-top">
					<!-- BEGIN TOP NAVIGATION MENU -->
					<?php require_once DIR. '/src/Views/Backend/inc/top_menu.v.php'?>
					<!-- END TOP NAVIGATION MENU -->
				</div>
				<!-- END PAGE TOP -->
			</div>
			<!-- END HEADER INNER -->
		</div>
		<!-- END HEADER -->
		<!-- BEGIN HEADER & CONTENT DIVIDER -->
		<div class="clearfix"> </div>
		<!-- END HEADER & CONTENT DIVIDER -->
		<!-- BEGIN CONTAINER -->
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/sidebar.v.php' ?>
			<!-- END SIDEBAR -->
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<!-- BEGIN PAGE HEADER-->
					<h1 class="page-title"> <?= $node->title() ?>
						<small></small>
					</h1>
					<!-- BEGIN BREADCRUMBS -->
					<?php require_once( DIR . '/src/Views/Backend/inc/breadcrumbs.v.php' ); ?>
					<!-- END BREADCRUMBS -->
					<?php if(!empty($messages)) : ?>
						<?php foreach($messages as $type => $msgs) : ?>
							<?php foreach($msgs as $msg) : ?>
								<div class="alert alert-<?= $type ?>">
									<button class="close" data-close="alert"></button>
									<span><strong><?= $msg ?></strong></span>
								</div>
							<?php endforeach; ?>
						<?php endforeach; ?>
					<?php endif; ?>
					<!-- END PAGE HEADER-->

					<div class="row">
						<div class="col-md-12">
							<div class="table-scrollable">
								<div class="portlet light ">
									<div class="portlet-title">
										<div class="caption">
											<form id="search_list" name="search_list" class="form-inline" action='<?= $node->path() ?>?action=showContactsList' method="GET">
                                                <div class="form-group">
													<input type="text" class="form-control" name="filters[filter_name]" id="filter_name"
														   value="<?= session()->get($node->name().'filters.filter_name') ?>" placeholder="Search Name..." />
												</div>
                                                <div class="form-group">
                                                    <select id="filter_active" class="form-control" name="filter_active">
                                                        <option value="" >Select Active Status</option>
                                                        <option value="1" <?= session()->get($node->name().'filters.filter_active') == 1 ? 'selected' : '' ?>>Active</option>
                                                        <option value="2" <?= session()->get($node->name().'filters.filter_active') == 2 ? 'selected' : '' ?>>Not Active</option>
                                                    </select>
                                                </div>
												<div class="form-group">
													<select id="filter_clients" class="form-control" name="filter_clients">
														<option value="" >Select Client</option>
														<?php foreach ($data['clients'] as $client) : ?>
															<option value="<?= $client->id() ?>" <?= session()->get($node->name().'.filters.filter_clients') == $client->id() ? 'selected' : '' ?>><?= $client->title() ?></option>
														<?php endforeach; ?>
													</select>
												</div>
											</form>
										</div>
									</div>
									<div class="actions pull-right" style="padding-bottom: 10px">
										<?php if(user()->has_permission_to('showClientsList')) : ?>
											<a class="btn btn-transparent default btn-outline  btn-sm active" href="<?= env('DOMAIN').'/'.env('ADMIN_NAME').'/banners/clients' ?>?action=showClientsList">Clients list</a>
										<?php endif; ?>
									</div>
									<div id="contacts_table" class="portlet-body table-responsive">
										<div><?= $data['rows']->fullPagination ?></div>
										<table class="table table-striped table-hover margin-top-20">
											<thead>
												<tr>
													<th>
														<a href="<?= navigation(true, false) ?>&s=contacts_id">ID
															<?= sorting() == 'contacts_id' ? sorting_icon() : sorting_icon_default() ?>
														</a>
													</th>
													<th>
														<a href="<?= navigation(true, false) ?>&s=contacts_name">Name
															<?= sorting() == 'contacts_name' ? sorting_icon() : sorting_icon_default() ?>
														</a>
													</th>
													<th>
														<a href="<?= navigation(true, false) ?>&s=contacts_client">Client
															<?= sorting() == 'contacts_client' ? sorting_icon() : sorting_icon_default() ?>
														</a>
													</th>
													<th>
														<a href="<?= navigation(true, false) ?>&s=contacts_active">Is Active
															<?= sorting() == 'contacts_active' ? sorting_icon() : sorting_icon_default() ?>
														</a>
													</th>
												</tr>
											</thead>
											<tbody>
												<?php if(!empty($data['rows']->items())) : ?>
													<?php foreach ($data['rows']->items() as $contact) : ?>
														<tr>
															<td><?= $contact->id() ?></td>
															<td>
																<?php if(user()->has_permission_to('updateContact')) : ?>
																	<a href="<?= $node->path() ?>?action=editContact&client_id=<?= $contact->client_id() ?>&contact_id=<?= $contact->id() ?>">
																		<?= $contact->name() ?>
																	</a>
																<?php else : ?>
																	<?= $contact->name() ?>
																<?php endif; ?>
															</td>
															<td><?= $contact->client_title() ?></td>
															<td><i class="fa fa-<?= ($contact->is_active() == 1) ? 'check' : 'times'; ?> font-<?= ($contact->is_active() == 1) ? 'green-jungle' : 'red-thunderbird'; ?> font-lg"></i></td>
														</tr>
													<?php endforeach; ?>
												<?php endif; ?>
											</tbody>
										</table>
										<div><?= $data['rows']->fullPagination ?></div>
										<div class="margin-top-20">Total rows: <?= $data['rows']->total() ?></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->
			<!-- BEGIN QUICK SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/quick_sidebar.v.php' ?>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->

		<!-- BEGIN FOOTER -->
		<?php require_once DIR. '/src/Views/Backend/inc/footer.v.php' ?>
		<!-- END FOOTER -->

		<?php require_once DIR.'/src/Views/Backend/inc/global_js.php' ?>

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/pages/js/contacts.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->

	</body>

</html>