<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title><?= SITENAME ?> | <?= $node->title() ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="<?= SITENAME ?> Dashboard" name="description" />
		<meta content="" name="author" />
		<?php require_once DIR.'/src/Views/Backend/inc/global_css.php' ?>
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<!-- END PAGE LEVEL PLUGINS -->
	</head>
	<!-- END HEAD -->

	<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-fixed page-md <?= session()->get('user.side_menu_open')  === true ? 'page-sidebar-closed' : ''?>">

		<!-- BEGIN HEADER -->
		<div class="page-header navbar navbar-fixed-top">
			<!-- BEGIN HEADER INNER -->
			<div class="page-header-inner ">
				<!-- BEGIN LOGO -->
				<?php require(DIR . '/src/Views/Backend/inc/logo.v.php'); ?>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN PAGE ACTIONS -->
				<!-- DOC: Remove "hide" class to enable the page header actions -->
				<?php require_once DIR. '/src/Views/Backend/inc/dropdown.v.php'?>
				<!-- END PAGE ACTIONS -->
				<!-- BEGIN PAGE TOP -->
				<div class="page-top">
					<!-- BEGIN TOP NAVIGATION MENU -->
					<?php require_once DIR. '/src/Views/Backend/inc/top_menu.v.php'?>
					<!-- END TOP NAVIGATION MENU -->
				</div>
				<!-- END PAGE TOP -->
			</div>
			<!-- END HEADER INNER -->
		</div>
		<!-- END HEADER -->
		<!-- BEGIN HEADER & CONTENT DIVIDER -->
		<div class="clearfix"> </div>
		<!-- END HEADER & CONTENT DIVIDER -->
		<!-- BEGIN CONTAINER -->
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/sidebar.v.php' ?>
			<!-- END SIDEBAR -->
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<!-- BEGIN PAGE HEADER-->
					<h1 class="page-title">  <?= $node->title().' ['.$data['client']->title().']' ?>
						<small></small>
					</h1>
					<!-- BEGIN BREADCRUMBS -->
					<?php require_once( DIR . '/src/Views/Backend/inc/breadcrumbs.v.php' ); ?>
					<!-- END BREADCRUMBS -->
					<?php if(!empty($messages)) : ?>
						<?php foreach($messages as $type => $msgs) : ?>
							<?php foreach($msgs as $msg) : ?>
								<div class="alert alert-<?= $type ?>">
									<button class="close" data-close="alert"></button>
									<span><strong><?= $msg ?></strong></span>
								</div>
							<?php endforeach; ?>
						<?php endforeach; ?>
					<?php endif; ?>
					<!-- END PAGE HEADER-->

					<div class="row">
						<div class="col-md-12">
							<div class="portlet light">
								<div class="portlet-title tabbable-line">
									<ul class="nav nav-tabs nav-justified">
										<li class="">
											<a href="<?= env('DOMAIN').'/'.env('ADMIN_NAME').'/banners/clients?action=editClient&id='.$data['client']->id() ?>">Client Info</a>
										</li>

										<li class="active">
											<a href="javascript:;">Contacts</a>
										</li>
									</ul>
								</div>

								<div class="portlet-body">
									<div class="tab-content">
										<div class="tab-pane active">
											<div class="row">
												<div class="col-md-3">
													<ul class="nav nav-pills nav-stacked">
														<li class="">
															<a href="<?= env('DOMAIN').'/'.env('ADMIN_NAME').'/banners/clients/contacts?action=addContact&client_id='.$data['client']->id() ?>"><i class="fa fa-plus-circle"></i> Add Contact</a>
														</li>
														<?php foreach ($data['contacts'] as $contact) : ?>
															<li class="<?= $data['contact']->id() == $contact->id() ? 'active' : '' ?>">
																<a href="<?= env('DOMAIN').'/'.env('ADMIN_NAME').'/banners/clients/contacts?action=editContact&client_id='.$data['client']->id().'&contact_id='.$contact->id() ?>">
																	<span class="badge badge-success pull-right"><?= $contact->is_active() == 1 ? '<i class="fa fa-check">' : '<i class="fa fa-times">' ?></i></span>
																	<?= $contact->name() ?>
																</a>
															</li>
														<?php endforeach; ?>
													</ul>
												</div>
												<div class="col-md-9">
													<div class="tab-content">
														<div class="tab-pane active">
															<form method="POST" action="<?= $node->path().'?action=updateContact' ?>" accept-charset="UTF-8" class="form"><input name="_method" type="hidden" value="PATCH"><input name="_token" type="hidden" value="zTtuzsAq1j1kmkn4jrODOxkkfeQDUBrpRhNqyzrN">
																<input type="hidden" name="csrf_name" value="<?php echo $csrf_name; ?>">
																<input type="hidden" name="csrf_value" value="<?php echo $csrf_value; ?>">
																<input type="hidden" name="client_id" value="<?= $data['client']->id() ?>">
																<input type="hidden" name="contact_id" value="<?= $data['contact']->id() ?>">
																<div class="form-body">
																	<div class="row">
																		<div class="col-md-12">
																			<div class="form-group form-md-line-input form-md-floating-label ">
																				<input class="form-control edited" name="name" type="text" value="<?= $data['contact']->name() ?>">
																				<label for="title">Name</label>
																			</div>
																		</div>
																	</div>

																	<div class="row">
																		<div class="col-md-12">
																			<div class="form-group form-md-line-input form-md-floating-label ">
																				<input class="form-control edited" name="email" type="text" value="<?= $data['contact']->email() ?>">
																				<label for="email" class="required">Email</label>
																			</div>
																		</div>
																	</div>

																	<div class="row">
																		<div class="col-md-12">
																			<div class="form-group form-md-line-input form-md-floating-label ">
																				<input class="form-control" name="phone" type="text" value="<?= $data['contact']->phone() ?>">
																				<label for="phone">Phone</label>
																			</div>
																		</div>
																	</div>

																	<div class="row">
																		<div class="col-md-12">
																			<div class="form-group">
																				<label class="" for="is_active">Active</label><p />
																				<input id="is_active" name="is_active" type="checkbox" class="make-switch" data-size="normal" data-on-text="Yes" data-off-text="No" data-on-color="success" data-off-color="danger" <?= $data['contact']->is_active() == 1 ? 'checked' : '' ?> >
																			</div>
																		</div>
																	</div>
																</div>

																<!-- FORM ACTIONS -->
																<div class="form-actions">
																	<div class="row">
																		<div class="col-sm-6 col-lg-4">
																			<button id="submit" type="submit" class="btn btn-primary btn-block">
																				<i class="fa fa-check"></i> Update Contact
																			</button>
																		</div>
																	</div>
																</div>
															</form>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->
			<!-- BEGIN QUICK SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/quick_sidebar.v.php' ?>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->

		<!-- BEGIN FOOTER -->
		<?php require_once DIR. '/src/Views/Backend/inc/footer.v.php' ?>
		<!-- END FOOTER -->

		<?php require_once DIR.'/src/Views/Backend/inc/global_js.php' ?>

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->

	</body>

</html>