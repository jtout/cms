<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title><?= SITENAME ?> | <?= $node->title() ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="<?= SITENAME ?> Dashboard" name="description" />
		<meta content="" name="author" />
		<?php require_once DIR.'/src/Views/Backend/inc/global_css.php' ?>
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

		<!-- END PAGE LEVEL PLUGINS -->
	</head>
	<!-- END HEAD -->

	<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-fixed page-md <?= session()->get('user.side_menu_open')  === true ? 'page-sidebar-closed' : ''?>">

		<!-- BEGIN HEADER -->
		<div class="page-header navbar navbar-fixed-top">
			<!-- BEGIN HEADER INNER -->
			<div class="page-header-inner ">
				<!-- BEGIN LOGO -->
				<?php require_once(DIR . '/src/Views/Backend/inc/logo.v.php'); ?>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN PAGE ACTIONS -->
				<!-- DOC: Remove "hide" class to enable the page header actions -->
				<?php require_once DIR. '/src/Views/Backend/inc/dropdown.v.php'?>
				<!-- END PAGE ACTIONS -->
				<!-- BEGIN PAGE TOP -->
				<div class="page-top">
					<!-- BEGIN TOP NAVIGATION MENU -->
					<?php require_once DIR. '/src/Views/Backend/inc/top_menu.v.php'?>
					<!-- END TOP NAVIGATION MENU -->
				</div>
				<!-- END PAGE TOP -->
			</div>
			<!-- END HEADER INNER -->
		</div>
		<!-- END HEADER -->
		<!-- BEGIN HEADER & CONTENT DIVIDER -->
		<div class="clearfix"> </div>
		<!-- END HEADER & CONTENT DIVIDER -->
		<!-- BEGIN CONTAINER -->
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/sidebar.v.php' ?>
			<!-- END SIDEBAR -->
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<!-- BEGIN PAGE HEADER-->
					<h1 class="page-title"> <?= $node->title() ?>
						<small></small>
					</h1>
					<!-- BEGIN BREADCRUMBS -->
					<?php require_once ( DIR . '/src/Views/Backend/inc/breadcrumbs.v.php' ); ?>
					<!-- END BREADCRUMBS -->
					<?php if(!empty($messages)) : ?>
						<?php foreach($messages as $type => $msgs) : ?>
							<?php foreach($msgs as $msg) : ?>
								<div class="alert alert-<?= $type ?>">
									<button class="close" data-close="alert"></button>
									<span><strong><?= $msg ?></strong></span>
								</div>
							<?php endforeach; ?>
						<?php endforeach; ?>
					<?php endif; ?>
					<!-- END PAGE HEADER-->

					<div class="row">
						<div class="col-md-12">
							<div class="portlet light ">
								<div class="portlet-title">
									<div class="caption">
										<span class="caption-subject font-red-sunglo bold">Node Info</span>
									</div>
								</div>
								<div class="portlet-body form">
									<form role="form" action="<?= $node->path() ?>?action=insertPanelNode" method="POST" class="tab-content" enctype="multipart/form-data">
										<input type="hidden" name="csrf_name" value="<?php echo $csrf_name; ?>">
										<input type="hidden" name="csrf_value" value="<?php echo $csrf_value; ?>">
										<input type="hidden" name="in_backend" value="1">

										<div class="form-group">
											<label for="parent_id" class="control-label">Parent</label>
											<select id="parent_id" class="form-control" name="parent_id">
												<?php foreach($data['parents'] as $parent) : ?>
													<option value="<?= $parent->id() ?>"
                                                        <?= isset(session()->get('insert_node')['parent_id']) && session()->get('insert_node')['parent_id'] ==  $parent->id() ? 'selected' : '' ?>>
														<?= str_repeat(' - ', $parent->depth()).$parent->title() ?>
													</option>
												<?php endforeach; ?>
											</select>
										</div>

										<div class="form-group">
											<label for="module_id" class="control-label">Module</label>
											<select id="module_id" class="form-control" name="module_id">
												<?php foreach($data['modules'] as $module) : ?>
													<option value="<?= $module->id() ?>"
                                                        <?= isset(session()->get('insert_node')['module_id']) && session()->get('insert_node')['module_id'] ==  $module->id() ? 'selected' : '' ?>>
														Title: <?= $module->title() ?> - Action: <?= $module->action() ?>
													</option>
												<?php endforeach; ?>
											</select>
										</div>

										<div class="form-group">
											<label for="access_level_id" class="control-label">Access Level</label>
											<select id="access_level_id" class="form-control" name="access_level_id">
												<?php foreach($data['access_levels'] as $accessLevel) : ?>
													<option value="<?= $accessLevel->id() ?>"
                                                        <?= isset(session()->get('insert_node')['access_level_id']) && session()->get('insert_node')['access_level_id'] ==  $accessLevel->id() ? 'selected' : '' ?>>
														<?= str_repeat(' - ', $accessLevel->depth()).$accessLevel->title() ?>
													</option>
												<?php endforeach; ?>
											</select>
										</div>

										<div class="form-group">
											<label class="control-label">Title</label>
											<input type="text" name="title" value="<?= isset(session()->get('insert_node')['title']) ? session()->get('insert_node')['title'] : '' ?>" placeholder="Title" class="form-control" />
										</div>

										<div class="form-group">
											<label class="control-label">Description</label>
											<input type="text" name="description" value="<?= isset(session()->get('insert_node')['description']) ? session()->get('insert_node')['description'] : '' ?>" placeholder="Description" class="form-control" />
										</div>

										<div class="form-group">
											<label class="control-label">Icon</label>
											<input type="text" name="icon" value="<?= isset(session()->get('insert_node')['icon']) ? session()->get('insert_node')['icon'] : '' ?>" placeholder="Icon" class="form-control" />
										</div>

										<div class="form-group">
											<label class="control-label">View</label>
											<input type="text" name="view" value="<?= isset(session()->get('insert_node')['view']) ? str_replace('Backend/','', session()->get('insert_node')['view']) : '' ?>" placeholder="Icon" class="form-control" />
										</div>

										<div class="form-group">
											<label class="control-label">Active</label>
											<select id="is_active" class="form-control" name="is_active">
                                                <option value="0" <?= isset(session()->get('insert_node')['is_active']) && session()->get('insert_node')['is_active'] == 0 ? 'selected' : '' ?>>No</option>
                                                <option value="1" <?= isset(session()->get('insert_node')['is_active']) && session()->get('insert_node')['is_active'] == 1 ? 'selected' : '' ?>>Yes</option>
											</select>
										</div>

										<div class="form-group">
											<label class="control-label">On Menu</label>
											<select id="on_menu" class="form-control" name="on_menu">
												<option value="0" <?= isset(session()->get('insert_node')['on_menu']) && session()->get('insert_node')['on_menu'] == 0 ? 'selected' : ''; ?>>No</option>
                                                <option value="1" <?= isset(session()->get('insert_node')['on_menu']) && session()->get('insert_node')['on_menu'] == 1 ? 'selected' : ''; ?>>Yes</option>
                                            </select>
										</div>

                                        <div class="form-group">
                                            <label class="control-label">Order</label>
                                            <input type="text" name="order" value="<?= isset(session()->get('insert_node')['order']) ? session()->get('insert_node')['order'] : '' ?>" placeholder="Order" class="form-control" />
                                        </div>

										<!-- FORM ACTIONS -->
										<div class="form-actions">
											<div class="row">
												<div class="col-md-4">
													<button type="submit" class="btn green">Save</button>
													<a href="<?= env('DOMAIN').'/'.env('ADMIN_NAME').'/system-settings/panel-pages' ?>" class="btn default">Cancel</a>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->
			<!-- BEGIN QUICK SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/quick_sidebar.v.php' ?>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->

		<!-- BEGIN FOOTER -->
		<?php require_once DIR. '/src/Views/Backend/inc/footer.v.php' ?>
		<!-- END FOOTER -->

		<?php require_once DIR.'/src/Views/Backend/inc/global_js.php' ?>

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/pages/js/nodes.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->

	</body>

</html>