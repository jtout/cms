<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Unboxholics Notifications</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="<?= SITENAME ?> Notifications" name="description" />
    <meta content="Unboxholics" name="author" />
    <?php require_once DIR.'/src/Views/Backend/inc/global_css.php' ?>
</head>
<!-- END HEAD -->
<body>

<!-- BEGIN CONTAINER -->
<div class="page-container" style="height: 100vh; background-color: #ffffff">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper" style="padding-top: 20px; background-color: #ffffff">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content" style="width: 100%; margin: 0 auto">
            <!-- BEGIN PAGE HEADER-->
            <h1 class="page-title"> Unboxholics Notifications</h1>
            <strong><?php echo $message ?></strong>

			<?php $info = json_decode($info); ?>

			<h2>Info</h2>
			IP: <?= $info->ip ?> <br />
			Type: <?= $info->type ?> <br />
			Continent Code: <?= $info->continent_code ?> <br />
			Continent Name: <?= $info->continent_name ?> <br />
			Country Code: <?= $info->country_code ?> <br />
			Country Name: <?= $info->country_name ?> <br />
			Region Name: <?= $info->region_name ?> <br />
			City: <?= $info->city ?> <br />
			Latitude: <?= $info->latitude ?> <br />
			Longitude: <?= $info->longitude ?> <br />
            Capital: <?= $info->location->capital ?> <br />
            Is EU: <?= $info->location->is_eu ?> <br />

			<p>Click <a href="http://api.ipstack.com/<?= $info->ip ?>?access_key=<?= env('IPSTACKKEY') ?>">here</a> for raw info.</p>
        </div>
        <!-- END CONTENT BODY -->
    </div>
</div>
<!-- END CONTAINER -->

</body>

</html>