<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title><?= SITENAME ?> | <?= $node->title() ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="<?= SITENAME ?> Dashboard" name="description" />
		<meta content="" name="author" />
		<?php require DIR.'/src/Views/Backend/inc/global_css.php' ?>
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/icheck/skins/all.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css" />

		<!-- END PAGE LEVEL PLUGINS -->
	</head>
	<!-- END HEAD -->

	<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-fixed page-md <?= session()->get('user.side_menu_open')  === true ? 'page-sidebar-closed' : ''?>">

		<!-- BEGIN HEADER -->
		<div class="page-header navbar navbar-fixed-top">
			<!-- BEGIN HEADER INNER -->
			<div class="page-header-inner ">
				<!-- BEGIN LOGO -->
				<?php require(DIR . '/src/Views/Backend/inc/logo.v.php'); ?>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN PAGE ACTIONS -->
				<!-- DOC: Remove "hide" class to enable the page header actions -->
				<?php require DIR . '/src/Views/Backend/inc/dropdown.v.php'?>
				<!-- END PAGE ACTIONS -->
				<!-- BEGIN PAGE TOP -->
				<div class="page-top">
					<!-- BEGIN TOP NAVIGATION MENU -->
					<?php require DIR . '/src/Views/Backend/inc/top_menu.v.php'?>
					<!-- END TOP NAVIGATION MENU -->
				</div>
				<!-- END PAGE TOP -->
			</div>
			<!-- END HEADER INNER -->
		</div>
		<!-- END HEADER -->
		<!-- BEGIN HEADER & CONTENT DIVIDER -->
		<div class="clearfix"> </div>
		<!-- END HEADER & CONTENT DIVIDER -->
		<!-- BEGIN CONTAINER -->
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<?php require DIR . '/src/Views/Backend/inc/sidebar.v.php' ?>
			<!-- END SIDEBAR -->
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<!-- BEGIN PAGE HEADER-->
					<h1 class="page-title"> <?= $node->title() ?>
						<small></small>
					</h1>
					<!-- BEGIN BREADCRUMBS -->
					<?php require ( DIR . '/src/Views/Backend/inc/breadcrumbs.v.php' ); ?>
					<!-- END BREADCRUMBS -->
					<?php if (!empty($messages)) : ?>
						<?php foreach($messages as $type => $msgs) : ?>
							<?php foreach($msgs as $msg) : ?>
								<div class="alert alert-<?= $type ?>">
									<button class="close" data-close="alert"></button>
									<span><strong><?= $msg ?></strong></span>
								</div>
							<?php endforeach; ?>
						<?php endforeach; ?>
					<?php endif; ?>
					<!-- END PAGE HEADER-->
					<div id="error_msg"></div>
					<div class="row">
						<div class="col-md-12">
							<div class="portlet light ">
								<div class="portlet-title">
									<div class="caption">
										<span class="caption-subject font-red-sunglo bold"><?= $data['extra_field']->title() ?></span>
									</div>
								</div>
								<div class="portlet-body form">
									<form id="roles_form" role="form" action="<?= $node->path() ?>?action=updateExtraField" method="POST" class="tab-content">
										<input type="hidden" name="csrf_name" value="<?php echo $csrf_name; ?>">
										<input type="hidden" name="csrf_value" value="<?php echo $csrf_value; ?>">
                                        <input type="hidden" id="id" name="id" value="<?= $data['extra_field']->id() ?>" />
                                        <input type="hidden" id="ordering" name="ordering" value="<?= $data['extra_field']->ordering() ?>" />
										<input type="hidden" id="view" name="view" value="edit" />

										<div class="row">
											<div class="col-xs-6 col-sm-3 col-lg-3">
												<div class="form-group">
													<label class="control-label">Title</label>
													<input type="text" name="title" value="<?= $data['extra_field']->title() ?>" placeholder="Title" class="form-control" />
												</div>
											</div>

                                            <div class="col-xs-6 col-sm-3 col-lg-3">
                                                <div class="form-group">
                                                    <label for="group_id" class="control-label">Group</label>
                                                    <select id="group_id" name="group_id">
                                                        <option value=""></option>
                                                        <?php foreach ($data['groups'] as $group) : ?>
                                                            <option value="<?= $group->id() ?>" <?= $data['extra_field']->group_id() == $group->id() ? 'selected' : '' ?>><?= $group->title() ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
										</div>

                                        <div class="row">
                                            <div class="col-xs-6 col-sm-3 col-lg-3">
                                                <div class="form-group">
                                                    <label for="type" class="control-label">Type</label>
                                                    <select id="type" name="type">
                                                        <option value=""></option>
                                                        <?php foreach ($data['types'] as $type => $name) : ?>
                                                            <option value="<?= $type ?>" <?= $data['extra_field']->type() == $type ? 'selected' : '' ?>><?= $name ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
											<div class="col-xs-6 col-sm-3 col-lg-3">
												<div class="form-group">
													<label class="" for="is_active">Active</label><p />
													<input id="is_active" name="is_active" type="checkbox" class="make-switch" data-size="normal" data-on-text="Yes" data-off-text="No" data-on-color="success" data-off-color="danger" <?= $data['extra_field']->is_active() == 1 ? 'checked' : '' ?>>
												</div>
											</div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-6 col-sm-3 col-lg-3">
                                                <div id="info" class="form-group <?= empty($data['extra_field']->info()) ? 'hide' : '' ?>">
                                                    <?php if (!empty($data['extra_field']->info())) : ?>
                                                        <?= $data['options'] ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>

										<!-- FORM ACTIONS -->
										<div class="form-actions">
											<div class="row">
												<div class="col-md-4">
													<button type="submit" id="submit" class="btn green">Save</button>
													<a href="<?= env('DOMAIN').'/'.env('ADMIN_NAME').'/extra-fields/extra-fields-list' ?>" class="btn default">Cancel</a>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->
			<!-- BEGIN QUICK SIDEBAR -->
			<?php require DIR . '/src/Views/Backend/inc/quick_sidebar.v.php' ?>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->

		<!-- BEGIN FOOTER -->
		<?php require DIR . '/src/Views/Backend/inc/footer.v.php' ?>
		<!-- END FOOTER -->

		<?php require DIR.'/src/Views/Backend/inc/global_js.php' ?>

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/jquery-repeater/jquery.repeater.min.js" type="text/javascript"></script>
        <script src="<?= env('DOMAIN') ?>/theme/admin/assets/pages/scripts/form-repeater.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/pages/js/extraFields.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->

	</body>

</html>