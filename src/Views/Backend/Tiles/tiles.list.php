<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title><?= SITENAME ?> | <?= $node->title() ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="<?= SITENAME ?> Dashboard" name="description" />
		<meta content="" name="author" />
		<?php require_once DIR.'/src/Views/Backend/inc/global_css.php' ?>
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
        <!-- END PAGE LEVEL PLUGINS -->
	</head>
	<!-- END HEAD -->

	<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-fixed page-md <?= session()->get('user.side_menu_open')  === true ? 'page-sidebar-closed' : ''?>">

		<!-- BEGIN HEADER -->
		<div class="page-header navbar navbar-fixed-top">
			<!-- BEGIN HEADER INNER -->
			<div class="page-header-inner ">
				<!-- BEGIN LOGO -->
				<?php require(DIR . '/src/Views/Backend/inc/logo.v.php'); ?>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN PAGE ACTIONS -->
				<!-- DOC: Remove "hide" class to enable the page header actions -->
				<?php require_once DIR. '/src/Views/Backend/inc/dropdown.v.php'?>
				<!-- END PAGE ACTIONS -->
				<!-- BEGIN PAGE TOP -->
				<div class="page-top">
					<!-- BEGIN TOP NAVIGATION MENU -->
					<?php require_once DIR. '/src/Views/Backend/inc/top_menu.v.php'?>
					<!-- END TOP NAVIGATION MENU -->
				</div>
				<!-- END PAGE TOP -->
			</div>
			<!-- END HEADER INNER -->
		</div>
		<!-- END HEADER -->
		<!-- BEGIN HEADER & CONTENT DIVIDER -->
		<div class="clearfix"> </div>
		<!-- END HEADER & CONTENT DIVIDER -->
		<!-- BEGIN CONTAINER -->
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/sidebar.v.php' ?>
			<!-- END SIDEBAR -->
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<!-- BEGIN PAGE HEADER-->
					<h1 class="page-title"> <?= $node->title() ?>
						<small></small>
					</h1>
					<!-- BEGIN BREADCRUMBS -->
					<?php require_once ( DIR . '/src/Views/Backend/inc/breadcrumbs.v.php' ); ?>
					<!-- END BREADCRUMBS -->
					<?php if (!empty($messages)) : ?>
						<?php foreach($messages as $type => $msgs) : ?>
							<?php foreach($msgs as $msg) : ?>
								<div class="alert alert-<?= $type ?>">
									<button class="close" data-close="alert"></button>
									<span><strong><?= $msg ?></strong></span>
								</div>
							<?php endforeach; ?>
						<?php endforeach; ?>
					<?php endif; ?>
                    <div id="response-msg"></div>
					<!-- END PAGE HEADER-->

					<div class="row">
                        <div class="col-md-6">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Tiles
                                    </div>
                                </div>
                                <div class="portlet" style="box-shadow:none">
                                    <div class="portlet-body">
										<div id="tree_tiles" class="tree-demo">
											<ul><?= $data['tiles']['tree'] ?></ul>
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Articles
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="portlet" style="box-shadow:none">
                                    <div class="portlet-body quick-edit">
                                        <form id="tiles_search_form" class="form" method="GET" action="<?= $node->path() ?>?action=showTilesList">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="filters[filter_title]" id="filter_title"
                                                       value="<?= session()->get($node->name().'.filters.filter_title') ?>" placeholder="Search by Title or ID" />
                                            </div>

                                            <div class="form-group">
                                                <label for="filter_on_tiles">Show articles only on tiles</label>
                                                <select id="filter_on_tiles" class="form-control" name="filters[filter_on_tiles]">
                                                    <option value="2" <?= session()->get($node->name().'.filters.filter_on_tiles') == 2 ? 'selected' : '' ?>>No</option>
                                                    <option value="1" <?= session()->get($node->name().'.filters.filter_on_tiles') == 1 ? 'selected' : '' ?>>Yes</option>
                                                </select>
                                            </div>
                                        </form>

                                        <form id="tiles_add_form" class="form" method="POST" action="<?= $node->path() ?>?action=addTiles">
                                            <input type="hidden" name="csrf_name" value="<?php echo $csrf_name; ?>">
                                            <input type="hidden" name="csrf_value" value="<?php echo $csrf_value; ?>">

											<div><?= $data['rows']->fullPagination ?></div>
											<div class="form-actions">
												<div class="row">
													<div class="col-md-4">
														<button type="submit" class="btn green">Add to Tiles</button>
													</div>
												</div>
											</div>
											<table class="table table-striped table-hover">
												<thead>
													<tr>
														<th>
															<div class="form-group form-md-checkboxes" style="margin: 0 auto;width: 20px;padding-top: 0;">
																<div class="md-checkbox-list" style="margin: 2px auto;">
																	<div class="md-checkbox">
																		<input id="select_all" type="checkbox" class="md-check item-checkbox">
																		<label for="select_all"><span></span><span class="check"></span><span class="box"></span></label>
																	</div>
																</div>
															</div>
														</th>
														<th>
															<a href="<?= navigation(true, false) ?>&s=nodes_id">ID
																<?= sorting() == 'nodes_id' ? sorting_icon() : sorting_icon_default() ?>
															</a>
														</th>
														<th>
															<a href="<?= navigation(true, false) ?>&s=nodes_title">Title
																<?= sorting() == 'nodes_title' ? sorting_icon() : sorting_icon_default() ?>
															</a>
														</th>
														<th>
															<a href="<?= navigation(true, false) ?>&s=nodes_active">Is Published
																<?= sorting() == 'nodes_active' ? sorting_icon() : sorting_icon_default() ?>
															</a>
														</th>
														<th>
															Remove from tiles
														</th>
													</tr>
												</thead>
												<tbody>
													<?php if (!empty($data['rows'])) : ?>
														<?php foreach ($data['rows']->items() as $nodeEntity) : ?>
															<tr>
																<td>
																	<?php if (!in_array($nodeEntity->id(), array_keys($data['tiles']['nodes']))) : ?>
																		<div class="form-group form-md-checkboxes" style="margin: 0 auto;width: 20px;padding-top: 0;">
																			<div class="md-checkbox-list" style="margin: 2px auto;">
																				<div class="md-checkbox">
																					<input id="<?= $nodeEntity->id() ?>" type="checkbox" name="node_checkbox[<?= $nodeEntity->id() ?>]" class="md-check item-checkbox">
																					<label for="<?= $nodeEntity->id() ?>"><span></span><span class="check"></span><span class="box"></span></label>
																				</div>
																			</div>
																		</div>
																	<?php endif; ?>
																</td>
																<td><?= $nodeEntity->id() ?></td>
																<td>
																	<a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/articles?action=editArticle&id=<?= $nodeEntity->id() ?>" target="_blank">
																		<?= $nodeEntity->title() ?>
																	</a>
																</td>
																<td>
																	<?php if ($nodeEntity->published_on() > date('Y-m-d H:i:s')) : ?>
																		<span class="label label-warning">Published but pending</span>
																	<?php else : ?>
																		<i class="fa fa-<?= ($nodeEntity->is_active() == 1) ? 'check' : 'times'; ?> font-<?= ($nodeEntity->is_active() == 1) ? 'green-jungle' : 'red-thunderbird'; ?> font-lg"></i>
																	<?php endif; ?>
																</td>
																<td>
																	<?php if (in_array($nodeEntity->id(), array_keys($data['tiles']['nodes']))) : ?>
																		<a class="btn red" href="<?= $node->path() ?>?action=removeTile&id=<?= $nodeEntity->id() ?>">Remove</a>
																	<?php else: ?>
																		<a href="#"><button disabled type="submit" class="btn red">Remove</button></a>
																	<?php endif; ?>
																</td>
															</tr>
														<?php endforeach; ?>
													<?php endif; ?>
												</tbody>
											</table>
											<div class="form-actions">
												<div class="row">
													<div class="col-md-4">
														<button type="submit" class="btn green">Add to Tiles</button>
													</div>
												</div>
											</div>
											<div><?= $data['rows']->fullPagination ?></div>
											<div class="margin-top-20">Total rows: <?= $data['rows']->total() ?></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
				<!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->
			<!-- BEGIN QUICK SIDEBAR -->
			<?php require DIR . '/src/Views/Backend/inc/quick_sidebar.v.php' ?>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->
		<!-- BEGIN FOOTER -->
		<div class="page-footer">
			<div class="page-footer-inner"> <?= date("Y"); ?> &copy; <?= SITENAME ?>
				<div class="scroll-to-top">
					<i class="icon-arrow-up"></i>
				</div>
			</div>
		</div>
		<!-- END FOOTER -->

		<?php require DIR.'/src/Views/Backend/inc/global_js.php' ?>

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
        <script async defer src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <script src="<?= env('DOMAIN') ?>/theme/admin/assets/pages/js/tiles.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->
	</body>
</html>