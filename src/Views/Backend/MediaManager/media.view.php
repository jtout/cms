<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title><?= SITENAME ?> | <?= $node->title() ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="<?= SITENAME ?> Dashboard" name="description" />
		<meta content="" name="author" />
		<?php require_once DIR.'/src/Views/Backend/inc/global_css.php' ?>
		<!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
		<!-- END PAGE LEVEL PLUGINS -->
	</head>
	<!-- END HEAD -->

	<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-fixed page-md <?= session()->get('user.side_menu_open')  === true ? 'page-sidebar-closed' : ''?>">

		<!-- BEGIN HEADER -->
		<div class="page-header navbar navbar-fixed-top">
			<!-- BEGIN HEADER INNER -->
			<div class="page-header-inner ">
				<!-- BEGIN LOGO -->
				<?php require(DIR . '/src/Views/Backend/inc/logo.v.php'); ?>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN PAGE ACTIONS -->
				<!-- DOC: Remove "hide" class to enable the page header actions -->
				<?php require_once DIR. '/src/Views/Backend/inc/dropdown.v.php'?>
				<!-- END PAGE ACTIONS -->
				<!-- BEGIN PAGE TOP -->
				<div class="page-top">
					<!-- BEGIN TOP NAVIGATION MENU -->
					<?php require_once DIR. '/src/Views/Backend/inc/top_menu.v.php'?>
					<!-- END TOP NAVIGATION MENU -->
				</div>
				<!-- END PAGE TOP -->
			</div>
			<!-- END HEADER INNER -->
		</div>
		<!-- END HEADER -->
		<!-- BEGIN HEADER & CONTENT DIVIDER -->
		<div class="clearfix"> </div>
		<!-- END HEADER & CONTENT DIVIDER -->
		<!-- BEGIN CONTAINER -->
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/sidebar.v.php' ?>
			<!-- END SIDEBAR -->
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<!-- BEGIN PAGE HEADER-->
					<h1 class="page-title"> <?= $node->title() ?>
						<small></small>
					</h1>
					<!-- BEGIN BREADCRUMBS -->
					<?php require_once ( DIR . '/src/Views/Backend/inc/breadcrumbs.v.php' ); ?>
					<!-- END BREADCRUMBS -->
					<?php if(!empty($messages)) : ?>
						<?php foreach($messages as $type => $msgs) : ?>
							<?php foreach($msgs as $msg) : ?>
								<div class="alert alert-<?= $type ?>">
									<button class="close" data-close="alert"></button>
									<span><strong><?= $msg ?></strong></span>
								</div>
							<?php endforeach; ?>
						<?php endforeach; ?>
					<?php endif; ?>
					<!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <form role="form" action="<?= $node->path() ?>?action=optimizeImage" method="POST" class="tab-content" enctype="multipart/form-data">
                                <input type="hidden" name="csrf_name" value="<?php echo $csrf_name; ?>">
                                <input type="hidden" name="csrf_value" value="<?php echo $csrf_value; ?>">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                                    <span class="fileinput-new "> Select Image</span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="hidden" value=""><input type="file" name="image">
                                                </span>
                                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn green optimize-button">Optimize</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <p/>
					<div class="row">
						<div class="col-md-12">
							<div class="portlet light ">
								<div class="row">
                                    <iframe width="100%"  style="min-height: 700px;" frameborder="0" src="<?= env('DOMAIN') ?>/media/filemanager/dialog.php?akey=<?= env('MEDIAKEY') ?>"></iframe>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->
			<!-- BEGIN QUICK SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/quick_sidebar.v.php' ?>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->

		<!-- BEGIN FOOTER -->
		<?php require_once DIR. '/src/Views/Backend/inc/footer.v.php' ?>
		<!-- END FOOTER -->

		<?php require_once DIR.'/src/Views/Backend/inc/global_js.php' ?>

		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
        <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL PLUGINS -->

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->

	</body>

</html>