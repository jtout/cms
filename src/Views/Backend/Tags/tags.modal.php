<form id="merge-tags-form" method="POST" action='<?= $node->path() ?>?action=mergeTags'>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Merge Tags</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <input type="hidden" name="csrf_name" value="<?php echo $csrf_name; ?>">
                <input type="hidden" name="csrf_value" value="<?php echo $csrf_value; ?>">
                <input type="hidden" name="merge_selected_tags" value="1">
                <?php $selectedTags = []; ?>
                <div class="form-group">
                    <h4>Merge selected tags into the following</h4>
                    <select id="merge_into" name="merge_into">
                    <?php foreach ($data['checkboxes'] as $checkbox) : ?>
                        <?php if ($checkbox['id'] != 'on') : ?>
                            <?php $selectedTags[] = $checkbox['id']; ?>
                            <option value="<?= $checkbox['id']?>"><?= $checkbox['title'] ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    </select>
                </div>

                <input type="hidden" name="selected_tags" value="<?= implode(',', $selectedTags) ?>">
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
        <button type="button" class="btn blue merge">Save</button>
    </div>
</form>
<script>
    $("#merge_into").select2({
        width: null,
        theme: 'bootstrap',
        placeholder: 'Select Tag'
    });
</script>