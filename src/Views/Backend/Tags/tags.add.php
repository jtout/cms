<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title><?= SITENAME ?> | <?= $node->title() ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="<?= SITENAME ?> Dashboard" name="description" />
		<meta content="" name="author" />
		<?php require_once DIR.'/src/Views/Backend/inc/global_css.php' ?>
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<!-- END PAGE LEVEL PLUGINS -->
	</head>
	<!-- END HEAD -->

	<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-fixed page-md <?= session()->get('user.side_menu_open')  === true ? 'page-sidebar-closed' : ''?>">

		<!-- BEGIN HEADER -->
		<div class="page-header navbar navbar-fixed-top">
			<!-- BEGIN HEADER INNER -->
			<div class="page-header-inner ">
				<!-- BEGIN LOGO -->
				<?php require_once(DIR . '/src/Views/Backend/inc/logo.v.php'); ?>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN PAGE ACTIONS -->
				<!-- DOC: Remove "hide" class to enable the page header actions -->
				<?php require_once DIR. '/src/Views/Backend/inc/dropdown.v.php'?>
				<!-- END PAGE ACTIONS -->
				<!-- BEGIN PAGE TOP -->
				<div class="page-top">
					<!-- BEGIN TOP NAVIGATION MENU -->
					<?php require_once DIR. '/src/Views/Backend/inc/top_menu.v.php'?>
					<!-- END TOP NAVIGATION MENU -->
				</div>
				<!-- END PAGE TOP -->
			</div>
			<!-- END HEADER INNER -->
		</div>
		<!-- END HEADER -->
		<!-- BEGIN HEADER & CONTENT DIVIDER -->
		<div class="clearfix"> </div>
		<!-- END HEADER & CONTENT DIVIDER -->
		<!-- BEGIN CONTAINER -->
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/sidebar.v.php' ?>
			<!-- END SIDEBAR -->
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<!-- BEGIN PAGE HEADER-->
					<h1 class="page-title"> <?= $node->title() ?>
						<small></small>
					</h1>
					<!-- BEGIN BREADCRUMBS -->
					<?php require_once ( DIR . '/src/Views/Backend/inc/breadcrumbs.v.php' ); ?>
					<!-- END BREADCRUMBS -->
					<?php if(!empty($messages)) : ?>
						<?php foreach($messages as $type => $msgs) : ?>
							<?php foreach($msgs as $msg) : ?>
								<div class="alert alert-<?= $type ?>">
									<button class="close" data-close="alert"></button>
									<span><strong><?= $msg ?></strong></span>
								</div>
							<?php endforeach; ?>
						<?php endforeach; ?>
					<?php endif; ?>
					<!-- END PAGE HEADER-->

					<div class="row">
						<div class="col-md-12">
							<div class="portlet light ">
								<div class="portlet-title">
									<div class="caption">
										<span class="caption-subject font-red-sunglo bold">Tag Info</span>
									</div>
								</div>
								<div class="portlet-body form">
                                    <div class="tabbable-line tabbable-full-width">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#main_info" data-toggle="tab"> Main Info </a>
                                            </li>
                                            <li class="">
                                                <a href="#main_image" data-toggle="tab"> Main Image</a>
                                            </li>
                                            <li class="">
                                                <a href="#meta_info" data-toggle="tab"> Publishing and Meta data </a>
                                            </li>
                                            <li class="">
                                                <a href="#settings" data-toggle="tab"> Settings </a>
                                            </li>
                                        </ul>
                                        <form role="form" action="<?= $node->path() ?>?action=insertTag" method="POST" class="tab-content" enctype="multipart/form-data">
                                            <input type="hidden" name="csrf_name" value="<?php echo $csrf_name; ?>">
                                            <input type="hidden" name="csrf_value" value="<?php echo $csrf_value; ?>">
											<?php if(!empty($data['translation'])) : ?>
												<input type="hidden" name="from_id" value="<?= $data['translation']['from_id'] ?>">
											<?php endif; ?>
                                            <!-- MAIN INFO -->
                                            <div class="tab-pane active" id="main_info">
                                                <div class="form-group">
                                                    <label class="control-label">Title</label>
                                                    <input type="text" name="title" value="<?= isset(session()->get('insert_tag')['title']) ? session()->get('insert_tag')['title'] : '' ?>" placeholder="Title" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Alias</label>
                                                    <input type="text" name="alias" value="<?= isset(session()->get('insert_tag')['name']) ? session()->get('insert_tag')['name'] : '' ?>" placeholder="Alias" class="form-control" />
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <div class="form-group">
                                                            <label for="is_active">Published</label><br />
                                                            <input id="is_active" value="" name="is_active" type="checkbox"  data-on-text="Yes" data-off-text="No" class="make-switch"  data-on-color="success" data-off-color="danger"
                                                                <?= isset(session()->get('insert_tag')['is_active']) && session()->get('insert_tag')['is_active'] == 1 ? 'checked' : '' ?>/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <div class="form-group">
                                                            <label for="is_featured">Featured</label><br />
                                                            <input id="is_featured" value="" name="is_featured" type="checkbox"  data-on-text="Yes" data-off-text="No" class="make-switch"  data-on-color="success" data-off-color="danger"
                                                                <?= isset(session()->get('insert_tag')['is_featured']) && session()->get('insert_tag')['is_featured'] == 1 ? 'checked' : '' ?>/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="language_id" class="">Language</label>
                                                            <select id="language_id" name="language_id">
                                                                <?php foreach ($data['languages'] as $language) :?>
                                                                    <option value="<?= $language->id() ?>"
																		<?php
																			if(isset(session()->get('insert_tag')['language_id']) && session()->get('insert_tag')['language_id'] == $language->id())
																				echo 'selected';
																			else
																				echo '';
																		?>>
                                                                        <?= $language->title() ?>
                                                                    </option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="description" class="control-label">Description</label>
                                                    <textarea class="form-control" id="description" name="description" rows="15"><?= isset(session()->get('insert_tag')['description']) ? session()->get('insert_tag')['description'] : '' ?></textarea>
                                                </div>
                                            </div>

                                            <!-- MAIN IMAGE -->
                                            <div class="tab-pane" id="main_image">
                                                <div class="form-group">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 400px; height: 250px;">
                                                            <img class="article-image" src="https://placeholdit.imgix.net/~text?txtsize=33&txt=no+image&w=400&h=250" alt=""> </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 400px; max-height: 250px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new select-main-image">Select Image</span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="image" value="" />
                                                            </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists remove-image" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix margin-top-10">
                                                        OR
                                                        <div class="input-append">
                                                            <input id="imageServer" name="imageServer" type="text" value="">
                                                            <a href="<?= env('DOMAIN') ?>/media/filemanager/dialog.php?akey=<?= env('MEDIAKEY') ?>&type=1&amp;field_id=imageServer'&amp;fldr=" class="btn default btn-file iframe-btn" type="button">Browse Server</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- META INFO -->
                                            <div class="tab-pane" id="meta_info">
                                                <div class="form-group">
                                                    <label class="control-label">Created On</label>
                                                    <input class="form-control form-control-inline input-medium date-picker" name="created_on" size="16" type="text"
														   value="<?= !isset(session()->get('insert_tag')['created_on']) ? date('Y-m-d H:i:s',time()) : session()->get('insert_tag')['created_on'] ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Published On</label>
                                                    <input class="form-control form-control-inline input-medium date-picker" name="published_on" size="16" type="text"
														   value="<?= !isset(session()->get('insert_tag')['published_on']) ? date('Y-m-d H:i:s',time()) : session()->get('insert_tag')['published_on'] ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Finish Publishing On</label>
                                                    <input class="form-control form-control-inline input-medium date-picker" name="finished_on" size="16" type="text"
														   value="<?= !isset(session()->get('insert_tag')['finished_on']) ? '' : session()->get('insert_tag')['finished_on'] ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Social Media Title</label>
                                                    <input type="text" name="social_media_title" value="" placeholder="Social Media title" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Meta Title</label>
                                                    <input type="text" name="meta_title" value="" placeholder="Meta title" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Meta Description</label>
                                                    <textarea  name="meta_description" class="form-control" rows="6"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Robots</label>
													<select name="robots" id="robots" class="form-control">
														<option value="INDEX, FOLLOW" <?= isset(session()->get('insert_tag')['robots']) && session()->get('insert_tag')['robots'] == 'INDEX, FOLLOW' ? 'selected' : '' ?>>Index, Follow</option>
														<option value="INDEX, NOFOLLOW" <?= isset(session()->get('insert_tag')['robots']) && session()->get('insert_tag')['robots'] == 'INDEX, NOFOLLOW' ? 'selected' : '' ?>>Index, No Follow</option>
														<option value="NOINDEX, NOFOLLOW" <?= isset(session()->get('insert_tag')['robots']) && session()->get('insert_tag')['robots'] == 'NOINDEX, NOFOLLOW' ? 'selected' : '' ?>>No Index, No Follow</option>
													</select>
                                                </div>
                                            </div>

                                            <!-- EXTRA SETTINGS -->
                                            <div class="tab-pane" id="settings">
                                                <div class="row">
                                                    <div class="form-group col-md-2">
                                                        <label for="view" class="">Template</label>
                                                        <select id="view" name="view" class="form-control">
                                                            <?php foreach($data['views'] as $view) : ?>
																<option value="<?= $view ?>.view"  <?= isset(session()->get('insert_tag')['view']) && session()->get('insert_tag')['view'] == 'Frontend/Tags/'.$view.'.view' ? 'selected' : '' ?>>
																	<?= ucfirst($view) ?>
																</option>
															<?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <!-- FORM ACTIONS -->
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <button type="submit" class="btn green">Save</button>
                                                        <a href="<?= env('DOMAIN').'/'.env('ADMIN_NAME').'/tags' ?>" class="btn default">Cancel</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end tab-pane-->
                                        </form>
                                    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->
			<!-- BEGIN QUICK SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/quick_sidebar.v.php' ?>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->

		<!-- BEGIN FOOTER -->
		<?php require_once DIR. '/src/Views/Backend/inc/footer.v.php' ?>
		<!-- END FOOTER -->

		<?php require_once DIR.'/src/Views/Backend/inc/global_js.php' ?>

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/fancybox/source/jquery.fancybox.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/pages/js/content.js" type="text/javascript"></script>
		<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=<?= env('TINYMCEKEY') ?>"></script>
        <?php require_once DIR.'/src/Views/Backend/inc/editor.php' ?>
		<!-- END PAGE LEVEL SCRIPTS -->
	</body>
</html>