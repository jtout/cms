<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title><?= SITENAME ?> | <?= $node->title() ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="<?= SITENAME ?> Dashboard" name="description" />
		<meta content="" name="author" />
		<?php require_once DIR.'/src/Views/Backend/inc/global_css.php' ?>
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
	</head>
	<!-- END HEAD -->

	<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-fixed page-md <?= session()->get('user.side_menu_open')  === true ? 'page-sidebar-closed' : ''?>">

		<!-- BEGIN HEADER -->
		<div class="page-header navbar navbar-fixed-top">
			<!-- BEGIN HEADER INNER -->
			<div class="page-header-inner ">
				<!-- BEGIN LOGO -->
				<?php require_once(DIR . '/src/Views/Backend/inc/logo.v.php'); ?>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN PAGE ACTIONS -->
				<!-- DOC: Remove "hide" class to enable the page header actions -->
				<?php require_once DIR. '/src/Views/Backend/inc/dropdown.v.php'?>
				<!-- END PAGE ACTIONS -->
				<!-- BEGIN PAGE TOP -->
				<div class="page-top">
					<!-- BEGIN TOP NAVIGATION MENU -->
					<?php require_once DIR. '/src/Views/Backend/inc/top_menu.v.php'?>
					<!-- END TOP NAVIGATION MENU -->
				</div>
				<!-- END PAGE TOP -->
			</div>
			<!-- END HEADER INNER -->
		</div>
		<!-- END HEADER -->
		<!-- BEGIN HEADER & CONTENT DIVIDER -->
		<div class="clearfix"> </div>
		<!-- END HEADER & CONTENT DIVIDER -->
		<!-- BEGIN CONTAINER -->
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/sidebar.v.php' ?>
			<!-- END SIDEBAR -->
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<!-- BEGIN PAGE HEADER-->
					<h1 class="page-title"> <?= $node->title() ?>
						<small></small>
					</h1>
					<!-- BEGIN BREADCRUMBS -->
					<?php require_once ( DIR . '/src/Views/Backend/inc/breadcrumbs.v.php' ); ?>
					<!-- END BREADCRUMBS -->
					<?php if(!empty($messages)) : ?>
						<?php foreach($messages as $type => $msgs) : ?>
							<?php foreach($msgs as $msg) : ?>
								<div class="alert alert-<?= $type ?>">
									<button class="close" data-close="alert"></button>
									<span><strong><?= $msg ?></strong></span>
								</div>
							<?php endforeach; ?>
						<?php endforeach; ?>
					<?php endif; ?>
					<!-- END PAGE HEADER-->

					<div class="row">
						<div class="col-md-12">
							<div class="table-scrollable">
								<div class="portlet light ">
									<div class="portlet-title">
										<div class="caption">
											<form id="search_list" name="search_list" class="form-inline" action='<?= $node->path() ?>?action=showNodesList' method="GET">
												<div class="form-group">
													<input type="text" class="form-control" name="filters[filter_title]" id="filter_title"
														   value="<?= session()->get($node->name().'.filters.filter_title') ?>" placeholder="Search Title..." />
												</div>
												<div class="form-group">
													<select id="filter_active" class="form-control" name="filters[filter_active]">
														<option value="" >Select Publish Status</option>
														<option value="1" <?= session()->get($node->name().'.filters.filter_active') == 1 ? 'selected' : '' ?>>Published</option>
														<option value="2" <?= session()->get($node->name().'.filters.filter_active') == 2 ? 'selected' : '' ?>>Not Published</option>
													</select>
												</div>
												<div class="form-group">
													<select id="filter_featured" class="form-control" name="filters[filter_featured]">
														<option value="" >Select Featured Status</option>
														<option value="1" <?= session()->get($node->name().'.filters.filter_featured') == 1 ? 'selected' : '' ?>>Featured</option>
														<option value="2" <?= session()->get($node->name().'.filters.filter_featured') == 2 ? 'selected' : '' ?>>Not Featured</option>
													</select>
												</div>
												<div class="form-group">
													<select id="filter_language" class="form-control" name="filters[filter_language]">
														<option value="" >Select Language</option>
                                                        <?php foreach ($data['languages'] as $language) : ?>
															<option value="<?= $language->id() ?>" <?= session()->get($node->name().'.filters.filter_language') == $language->id() ? 'selected' : '' ?>><?= $language->title() ?></option>
                                                        <?php endforeach; ?>
													</select>
												</div>
											</form>
										</div>
									</div>
									<form id="nodes_list_form" name="nodes_list_form" method="POST" action='<?= $node->path() ?>?action=deleteTag'>
										<div class="actions pull-right" style="padding-bottom: 10px">
											<input type="hidden" name="csrf_name" value="<?php echo $csrf_name; ?>">
											<input type="hidden" name="csrf_value" value="<?php echo $csrf_value; ?>">
                                            <?php if(user()->has_permission_to('mergeTags')) : ?>
                                                <a class="btn btn-transparent yellow btn-outline  btn-sm active" id="ajax-demo" data-url="<?= $node->path() ?>?action=mergeTags" data-toggle="modal">Merge Tags</a>
                                                <div id="ajax-modal" class="modal fade container" tabindex="-1"> </div>
                                            <?php endif; ?>
                                            <?php if(user()->has_permission_to('addTag')) : ?>
                                                <a class="btn btn-transparent green btn-outline  btn-sm active" href="<?= $node->path() ?>?action=addTag">Add new Tag</a>
                                            <?php endif; ?>
                                            <?php if(user()->has_permission_to('deleteTag')) : ?>
                                                <button id="delete_all" type="submit" class="btn btn-transparent red btn-outline  btn-sm active">Delete Tag</button>
                                            <?php endif; ?>
										</div>
										<div id="nodes_table" name="nodes_table" class="portlet-body table-responsive" style="width: 100%">
											<div><?= $data['rows']->fullPagination ?></div>
											<table class="table table-striped table-hover margin-top-20">
												<thead>
													<tr>
														<th>
															<div class="form-group form-md-checkboxes" style="margin: 0 auto;width: 20px;padding-top: 0;">
																<div class="md-checkbox-list" style="margin: 2px auto;">
																	<div class="md-checkbox">
																		<input id="select_all" type="checkbox" class="md-check item-checkbox">
																		<label for="select_all"><span></span><span class="check"></span><span class="box"></span></label>
																	</div>
																</div>
															</div>
														</th>
														<th>
															<a href="<?= navigation(true, false) ?>&s=nodes_id">ID
																<?= sorting() == 'nodes_id' ? sorting_icon() : sorting_icon_default() ?>
															</a>
														</th>
														<th>
															<a href="<?= navigation(true, false) ?>&s=nodes_title">Title
																<?= sorting() == 'nodes_title' ? sorting_icon() : sorting_icon_default() ?>
															</a>
														</th>
														<th>
															<a href="<?= navigation(true, false) ?>&s=nodes_featured">Is Featured
																<?= sorting() == 'nodes_featured' ? sorting_icon() : sorting_icon_default() ?>
															</a>
														</th>
														<th>
															<a href="<?= navigation(true, false) ?>&s=nodes_active">Is Published
																<?= sorting() == 'nodes_active' ? sorting_icon() : sorting_icon_default() ?>
															</a>
														</th>
                                                        <th>
                                                            <a href="<?= navigation(true, false) ?>&s=articles_count">Articles
                                                                <?= sorting() == 'articles_count' ? sorting_icon() : sorting_icon_default() ?>
                                                            </a>
                                                        </th>
														<th>
															<a href="<?= navigation(true, false) ?>&s=nodes_language">Language
																<?= sorting() == 'nodes_language' ? sorting_icon() : sorting_icon_default() ?>
															</a>
														</th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<?php if(!empty($data['rows']->items())) : ?>
														<?php foreach ($data['rows']->items() as $nodeEntity) : ?>
															<tr>
																<td>
																	<div class="form-group form-md-checkboxes" style="margin: 0 auto;width: 20px;padding-top: 0;">
																		<div class="md-checkbox-list" style="margin: 2px auto;">
																			<div class="md-checkbox">
																				<input id="<?= $nodeEntity->id() ?>" data-title="<?= $nodeEntity->title() ?>" type="checkbox" name="node_checkbox[<?= $nodeEntity->id() ?>]"
                                                                                       value="<?= $nodeEntity->id() ?>" class="md-check item-checkbox"
																					<?= !empty(session()->get('selected_tags') && in_array($nodeEntity->id(), array_keys(session()->get('selected_tags')))) ? 'checked' : ''?>>
																				<label for="<?= $nodeEntity->id() ?>"><span></span><span class="check"></span><span class="box"></span></label>
																			</div>
																		</div>
																	</div>
																</td>
																<td><?= $nodeEntity->id() ?></td>
																<td>
																	<?php if(!user()->has_permission_to('updateTag')) : ?>
																		<?= str_repeat(' - ', $nodeEntity->depth()) . $nodeEntity->title() ?>
																	<?php else: ?>
																		<a href="<?= $node->path() ?>?action=editTag&id=<?= $nodeEntity->id() ?>">
																			<?php if($nodeEntity->depth() > 2) : ?>
																				<?= str_repeat(' - ', $nodeEntity->depth() - 2) . $nodeEntity->title() ?>
																			<?php else : ?>
																				<?= $nodeEntity->title() ?>
																			<?php endif; ?>
																		</a>
																	<?php endif; ?>
																</td>
																<td><i class="fa fa-<?= ($nodeEntity->is_featured() == 1) ? 'check' : 'times'; ?> font-<?= ($nodeEntity->is_featured() == 1) ? 'green-jungle' : 'red-thunderbird'; ?> font-lg"></i></td>
																<td><i class="fa fa-<?= ($nodeEntity->is_active() == 1) ? 'check' : 'times'; ?> font-<?= ($nodeEntity->is_active() == 1) ? 'green-jungle' : 'red-thunderbird'; ?> font-lg"></i></td>
																<td><?= $nodeEntity->tags_articles_count() ?></td>
																<td><?= $nodeEntity->language()->title() ?></td>
																<td>
																	<a class="btn btn-transparent blue btn-outline btn-sm" href="<?= $nodeEntity->path(true) ?>" target="_blank">View Tag</a>
																</td>
															</tr>
														<?php endforeach; ?>
													<?php endif; ?>
												</tbody>
											</table>
											<div><?= $data['rows']->fullPagination ?></div>
											<div class="margin-top-20">Total rows: <?= $data['rows']->total() ?></div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->
			<!-- BEGIN QUICK SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/quick_sidebar.v.php' ?>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->

		<!-- BEGIN FOOTER -->
		<?php require_once DIR. '/src/Views/Backend/inc/footer.v.php' ?>
		<!-- END FOOTER -->

		<?php require_once DIR.'/src/Views/Backend/inc/global_js.php' ?>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/fancybox/source/jquery.fancybox.js" type="text/javascript"></script>
        <script async defer src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/pages/js/content.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/pages/js/tags.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->

	</body>

</html>