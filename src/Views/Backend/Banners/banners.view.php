<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title><?= SITENAME ?> | <?= $node->title() ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="<?= SITENAME ?> Dashboard" name="description" />
		<meta content="" name="author" />
		<?php require_once DIR.'/src/Views/Backend/inc/global_css.php' ?>
		<!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
	</head>
	<!-- END HEAD -->

	<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-fixed page-md <?= session()->get('user.side_menu_open')  === true ? 'page-sidebar-closed' : ''?>">

		<!-- BEGIN HEADER -->
		<div class="page-header navbar navbar-fixed-top">
			<!-- BEGIN HEADER INNER -->
			<div class="page-header-inner ">
				<!-- BEGIN LOGO -->
				<?php require(DIR . '/src/Views/Backend/inc/logo.v.php'); ?>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN PAGE ACTIONS -->
				<!-- DOC: Remove "hide" class to enable the page header actions -->
				<?php require_once DIR. '/src/Views/Backend/inc/dropdown.v.php'?>
				<!-- END PAGE ACTIONS -->
				<!-- BEGIN PAGE TOP -->
				<div class="page-top">
					<!-- BEGIN TOP NAVIGATION MENU -->
					<?php require_once DIR. '/src/Views/Backend/inc/top_menu.v.php'?>
					<!-- END TOP NAVIGATION MENU -->
				</div>
				<!-- END PAGE TOP -->
			</div>
			<!-- END HEADER INNER -->
		</div>
		<!-- END HEADER -->
		<!-- BEGIN HEADER & CONTENT DIVIDER -->
		<div class="clearfix"> </div>
		<!-- END HEADER & CONTENT DIVIDER -->
		<!-- BEGIN CONTAINER -->
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/sidebar.v.php' ?>
			<!-- END SIDEBAR -->
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<!-- BEGIN PAGE HEADER-->
					<h1 class="page-title"> <?= $node->title() ?>
						<small></small>
					</h1>
					<!-- BEGIN BREADCRUMBS -->
					<?php require_once( DIR . '/src/Views/Backend/inc/breadcrumbs.v.php' ); ?>
					<!-- END BREADCRUMBS -->
					<?php if(!empty($messages)) : ?>
						<?php foreach($messages as $type => $msgs) : ?>
							<?php foreach($msgs as $msg) : ?>
								<div class="alert alert-<?= $type ?>">
									<button class="close" data-close="alert"></button>
									<span><strong><?= $msg ?></strong></span>
								</div>
							<?php endforeach; ?>
						<?php endforeach; ?>
					<?php endif; ?>
					<!-- END PAGE HEADER-->

					<div class="row">
						<div class="col-md-12">
							<div class="portlet light ">
								<div class="portlet-title">
									<div class="caption">
										<span class="caption-subject font-red-sunglo bold"><?= $data['banner']->title() ?></span>
									</div>
								</div>
								<div class="portlet-body">
									<form id="roles_form" role="form" action="<?= $node->path() ?>?action=updateBanner" method="POST" class="tab-content">
										<input type="hidden" name="csrf_name" value="<?php echo $csrf_name; ?>">
										<input type="hidden" name="csrf_value" value="<?php echo $csrf_value; ?>">
										<input type="hidden" id="id" name="id" value="<?= $data['banner']->id() ?>">
                                        <input type="hidden" id="selected_nodes" name="selected_nodes" value="<?= $data['banner']->nodes() ?>">

                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#info" data-toggle="tab" aria-expanded="true">Main Info</a>
                                            </li>
                                            <li class="">
                                                <a href="#visibility" data-toggle="tab" aria-expanded="true">Visibility</a>
                                            </li>
                                            <li class="">
                                                <a href="#settings" data-toggle="tab" aria-expanded="true">Settings</a>
                                            </li>
                                        </ul>

                                        <div class="tab-content">
                                            <div class="tab-pane active" id="info">
                                                <div class="row">
													<div class="form-group col-md-6">
														<label class="control-label">Title</label>
														<input type="text" name="title" value="<?= $data['banner']->title() ?>" placeholder="Title" class="form-control" />
													</div>
												</div>

                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label for="type" class="control-label">Type</label>
                                                        <select id="type" class="form-control" name="type">
                                                            <option value="">Select Type</option>
                                                            <option value="image" <?= $data['banner']->type() == 'image' ? 'selected' : '' ?>>Image</option>
                                                            <option value="code" <?= $data['banner']->type() == 'code' ? 'selected' : '' ?>>Code</option>
                                                            <option value="articles-feed" <?= $data['banner']->type() == 'articles-feed' ? 'selected' : '' ?>>Articles Feed</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="click_url" class="control-label">Click URL</label>
                                                        <input type="text" id="click_url" name="click_url" value="<?= $data['banner']->click_url() ?>" placeholder="Url" class="form-control" />
                                                    </div>

                                                    <div id="field_id" class="form-group col-md-12">
                                                        <?php if(!empty($data['banner']->field())) : ?>
                                                            <?= $data['field'] ?>
                                                        <?php endif; ?>
                                                    </div>

                                                    <?php $selectedCategories = $data['banner']->settings() && isset($data['banner']->settings(true)['categories']) ? $data['banner']->settings(true)['categories'] : [] ?>
                                                    <div class="form-group col-md-6 hide">
                                                        <label for="categories" class="control-label">Categories</label>
                                                        <select id="categories" name="settings[categories][]" multiple>
                                                            <?php foreach ($data['categories'] as $category) :?>
                                                                <option value="<?= $category->id() ?>"
                                                                    <?= !empty($selectedCategories) && in_array($category->id(), $selectedCategories) ? 'selected' : '' ?>>
                                                                    <?php if($category->depth() > 2) : ?>
                                                                        <?= str_repeat(' - ', $category->depth() - 2) . $category->title() ?>
                                                                    <?php else : ?>
                                                                        <?= $category->title() ?>
                                                                    <?php endif; ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label for="client_id" class="control-label">Client</label>
                                                        <select id="client_id" class="form-control" name="client_id">
                                                            <option value="" >Select Client</option>
                                                            <?php foreach ($data['clients'] as $client) : ?>
                                                                <option value="<?= $client->id() ?>" <?= $data['banner']->client_id() == $client->id() ? 'selected' : '' ?>><?= $client->title() ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="position_id" class="control-label">Position</label>
                                                        <select id="position_id" class="form-control" name="position_id">
                                                            <option value="" >Select Position</option>
                                                            <?php foreach ($data['positions'] as $position) : ?>
                                                                <option value="<?= $position->id() ?>" <?= $data['banner']->position_id() == $position->id() ? 'selected' : '' ?>><?= $position->title() ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="is_active">Is Active</label><br />
                                                    <input id="is_active" value="" name="is_active" type="checkbox"  data-on-text="Yes" data-off-text="No" class="make-switch"  data-on-color="success" data-off-color="danger"
                                                        <?= $data['banner']->is_active() == 1 ? 'checked' : '' ?>/>
                                                </div>


                                                <div class="form-group">
                                                    <label class="control-label">Published On</label>
                                                    <input class="form-control form-control-inline input-medium date-picker" name="published_on" size="16" type="text"
                                                           value="<?= $data['banner']->published_on() ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Finish Publishing On</label>
                                                    <input class="form-control form-control-inline input-medium date-picker" name="finished_on" size="16" type="text"
                                                           value="<?= $data['banner']->finished_on() ?>">
                                                </div>

                                                <div class="form-group">
                                                    <label for="description" class="control-label">Description</label>
                                                    <textarea id="description" name="description" class="form-control"><?= $data['banner']->description() ?></textarea>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="visibility">
                                                <ul class="nav nav-tabs">
                                                    <?php foreach(LANGUAGES as $code => $language) : ?>
                                                        <?php if($code == LANGUAGE) : ?>
                                                            <li class="active">
                                                                <a href="#<?= $code ?>" data-toggle="tab" aria-expanded="true"><?= $language ?> (Default)</a>
                                                            </li>
                                                        <?php else: ?>
                                                            <li class="">
                                                                <a href="#<?= $code ?>" data-toggle="tab" aria-expanded="true"><?= $language ?></a>
                                                            </li>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </ul>

                                                <div class="tab-content">
                                                    <?php foreach(LANGUAGES as $code => $language) : ?>
                                                        <?php if($code == LANGUAGE) : ?>
                                                            <div class="tab-pane active" id="<?= $code ?>">
                                                                <div class="form-group">
                                                                    <label for="select_children">Select / Un-Select All</label><br />
                                                                    <input id="select_children" data-lang="<?= $code ?>" name="select_children" type="checkbox"  />
                                                                </div>

                                                                <div id="tree_<?= $code ?>" class="tree-demo">
                                                                    <ul><?= $data['menus']['menu'][$code] ?></ul>
                                                                </div>
                                                            </div>
                                                        <?php else: ?>
                                                            <div class="tab-pane" id="<?= $code ?>">
                                                                <div class="form-group">
                                                                    <label for="select_children">Select / Un-Select All</label><br />
                                                                    <input id="select_children" data-lang="<?= $code ?>" name="select_children" type="checkbox"  />
                                                                </div>

                                                                <div id="tree_<?= $code ?>" class="tree-demo">
                                                                    <ul><?= $data['menus']['menu'][$code] ?></ul>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="settings">
                                                <div class="row">
                                                    <div class="form-group col-md-1">
                                                        <label for="show_title">Show Title</label><br />
                                                        <input id="show_title"  name="settings[title]" type="checkbox"  data-on-text="Yes" data-off-text="No" class="make-switch"  data-on-color="success" data-off-color="danger"
                                                            <?= $data['banner']->show_title() ? 'checked' : '' ?>/>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-5">
                                                        <label class="control-label">Class</label>
                                                        <input type="text" name="settings[class]" value="<?= $data['banner']->class() ?>" placeholder="Class" class="form-control" />
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-5">
                                                        <label for="ordering">Order</label><br />
                                                        <input id="ordering" value="<?= $data['banner']->ordering() ?>" class="form-control" name="ordering" type="number" />
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-5">
                                                        <label for="show_in_greece">Show Only in Greece</label><br />
                                                        <input id="show_in_greece"  name="settings[show_in_greece]" type="checkbox"  data-on-text="Yes" data-off-text="No" class="make-switch"  data-on-color="success" data-off-color="danger"
                                                            <?= $data['banner']->show_only_in_greece() ? 'checked' : '' ?>/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <p/>

										<!-- FORM ACTIONS -->
										<div class="form-actions">
											<div class="row">
												<div class="col-md-4">
													<button type="submit" id="submit" class="btn green">Save</button>
													<a href="<?= env('DOMAIN').'/'.env('ADMIN_NAME').'/banners/banners-list' ?>" class="btn default">Cancel</a>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->
			<!-- BEGIN QUICK SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/quick_sidebar.v.php' ?>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->

		<!-- BEGIN FOOTER -->
		<?php require_once DIR. '/src/Views/Backend/inc/footer.v.php' ?>
		<!-- END FOOTER -->

		<?php require_once DIR.'/src/Views/Backend/inc/global_js.php' ?>

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/fancybox/source/jquery.fancybox.js" type="text/javascript"></script>
        <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
        <script src="<?= env('DOMAIN') ?>/theme/admin/assets/pages/js/banners.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->

	</body>

</html>