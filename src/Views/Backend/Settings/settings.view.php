<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title><?= SITENAME ?> | Settings </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="<?= SITENAME ?> settings" name="description" />
    <meta content="" name="author" />
    <?php require_once DIR.'/src/Views/Backend/inc/global_css.php' ?>
    <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
</head>
<!-- END HEAD -->
<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-fixed page-md <?= session()->get('user.side_menu_open')  === true ? 'page-sidebar-closed' : ''?>">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <?php require(DIR . '/src/Views/Backend/inc/logo.v.php'); ?>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN PAGE ACTIONS -->
        <!-- DOC: Remove "hide" class to enable the page header actions -->
        <?php require_once DIR. '/src/Views/Backend/inc/dropdown.v.php'?>
        <!-- END PAGE ACTIONS -->
        <!-- BEGIN PAGE TOP -->
        <div class="page-top">
            <!-- BEGIN TOP NAVIGATION MENU -->
            <?php require_once DIR. '/src/Views/Backend/inc/top_menu.v.php'?>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php require_once DIR. '/src/Views/Backend/inc/sidebar.v.php' ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <h1 class="page-title"> Settings
                <small></small>
            </h1>
            <!-- BEGIN BREADCRUMBS -->
            <?php require_once ( DIR . '/src/Views/Backend/inc/breadcrumbs.v.php' ); ?>
            <!-- END BREADCRUMBS -->
            <?php if (!empty($messages)) : ?>
                <?php foreach($messages as $type => $msgs) : ?>
                    <?php foreach($msgs as $msg) : ?>
                        <div class="alert alert-<?= $type ?>">
                            <button class="close" data-close="alert"></button>
                            <span><strong><?= $msg ?></strong></span>
                        </div>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light">
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form role="form" action="<?= $node->path() ?>?action=updateGeneralSettings" method="POST" class="form">
                                <input type="hidden" name="csrf_name" value="<?php echo $csrf_name; ?>">
                                <input type="hidden" name="csrf_value" value="<?php echo $csrf_value; ?>">

                                <div class="form-body">
                                    <div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<label class="control-label">Site Name</label>
												<input type="text" id="site_name" class="form-control" name="site_name" value="<?= $data['settings']->site_name() ?>" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<label class="control-label">System Administrator Email</label>
												<input type="text" id="system_administrator_email" class="form-control" name="system_administrator_email" value="<?= $data['settings']->system_administrator_email() ?>" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<label class="control-label">System Email</label>
												<input type="text" id="system_email" class="form-control" name="system_email" value="<?= $data['settings']->system_email() ?>" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<label class="control-label">Site Offline</label>
												<div class="margin-bottom-10">
													<input type="checkbox" id="site_offline" class="make-switch" name="site_offline"
                                                        <?= $data['settings']->site_offline() == 1 ? 'checked' : '' ?> />
												</div>
											</div>
										</div>
									</div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">Offline Message</label>
                                                <div class="margin-bottom-10">
                                                    <input type="text" id="offline_message" class="form-control" name="offline_message" value="<?= $data['settings']->offline_message() ?>" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
									<div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<label class="control-label">Allowed IPs (Separated by comma)</label>
												<textarea id="allowed_ips" class="form-control" name="allowed_ips"><?= $data['settings']->allowed_ips() ?></textarea>
											</div>
										</div>
									</div>
                                    <div class="form-group margin-top-30">
                                        <label class="control-label margin-bottom-30">Site Language</label>
                                        <?php foreach($data['languages'] as $language) : ?>
                                            <div class="margin-bottom-10">
                                                <label class="" style="width:60px" for="<?= $language->iso_code() ?>"><?= $language->title() ?></label>
                                                <input id="<?= $language->iso_code() ?>" type="radio" name="language" value="<?= $language->iso_code() ?>" <?= $data['settings']->language() == $language->iso_code() ? 'checked' : '' ?> class="make-switch switch-radio1">
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>

                                <!-- FORM ACTIONS -->
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <button type="submit" class="btn green">Save</button>
                                            <a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/system-settings" class="btn default">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->
    <?php require_once ( DIR . '/src/Views/Backend/inc/quick_sidebar.v.php' ); ?>
    <!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->

    <!-- BEGIN FOOTER -->
    <?php require_once DIR. '/src/Views/Backend/inc/footer.v.php' ?>
    <!-- END FOOTER -->

    <?php require_once DIR.'/src/Views/Backend/inc/global_js.php' ?>

    <!-- BEGIN CORE PLUGINS -->
    <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
</body>
</html>
