<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title><?= SITENAME ?> | <?= $node->title() ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="<?= SITENAME ?> Dashboard" name="description" />
		<meta content="" name="author" />
		<?php require_once DIR.'/src/Views/Backend/inc/global_css.php' ?>
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />

		<!-- END PAGE LEVEL PLUGINS -->
	</head>
	<!-- END HEAD -->

	<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-fixed page-md <?= session()->get('user.side_menu_open')  === true ? 'page-sidebar-closed' : ''?>">

		<!-- BEGIN HEADER -->
		<div class="page-header navbar navbar-fixed-top">
			<!-- BEGIN HEADER INNER -->
			<div class="page-header-inner ">
				<!-- BEGIN LOGO -->
				<?php require(DIR . '/src/Views/Backend/inc/logo.v.php'); ?>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN PAGE ACTIONS -->
				<!-- DOC: Remove "hide" class to enable the page header actions -->
				<?php require DIR . '/src/Views/Backend/inc/dropdown.v.php'?>
				<!-- END PAGE ACTIONS -->
				<!-- BEGIN PAGE TOP -->
				<div class="page-top">
					<!-- BEGIN TOP NAVIGATION MENU -->
					<?php require DIR . '/src/Views/Backend/inc/top_menu.v.php'?>
					<!-- END TOP NAVIGATION MENU -->
				</div>
				<!-- END PAGE TOP -->
			</div>
			<!-- END HEADER INNER -->
		</div>
		<!-- END HEADER -->
		<!-- BEGIN HEADER & CONTENT DIVIDER -->
		<div class="clearfix"> </div>
		<!-- END HEADER & CONTENT DIVIDER -->
		<!-- BEGIN CONTAINER -->
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<?php require DIR . '/src/Views/Backend/inc/sidebar.v.php' ?>
			<!-- END SIDEBAR -->
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<!-- BEGIN PAGE HEADER-->
					<h1 class="page-title"> <?= $node->title() ?>
						<small></small>
					</h1>
					<!-- BEGIN BREADCRUMBS -->
					<?php require ( DIR . '/src/Views/Backend/inc/breadcrumbs.v.php' ); ?>
					<!-- END BREADCRUMBS -->
					<?php if(!empty($messages)) : ?>
						<?php foreach($messages as $type => $msgs) : ?>
							<?php foreach($msgs as $msg) : ?>
								<div class="alert alert-<?= $type ?>">
									<button class="close" data-close="alert"></button>
									<span><strong><?= $msg ?></strong></span>
								</div>
							<?php endforeach; ?>
						<?php endforeach; ?>
					<?php endif; ?>
					<!-- END PAGE HEADER-->

					<div class="row">
						<div class="col-md-12">
							<div class="table-scrollable">
								<div class="portlet light ">
									<div class="portlet-title">
										<div class="caption">
											<form id="search_list" name="search_list" class="form-inline" action='<?= $node->path() ?>?action=showRolesList' method="GET">
                                                <div class="form-group">
                                                    <select id="filter_type" class="form-control" name="filters[filter_type]">
                                                        <option value="" >Select Type</option>
                                                        <?php foreach ($data['types'] as $type) : ?>
                                                            <option value="<?= $type ?>"
                                                                <?= session()->get($node->name().'.filters.filter_type') == $type ? 'selected' : '' ?>>
                                                                <?= $type ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <?php
                                                        if(session()->get($node->name().'.filters.filter_causer') == 3) {
                                                            $causer = 'guest';
                                                        }
                                                        else {
                                                            $causer = session()->get($node->name().'.filters.filter_causer');
                                                        }
                                                    ?>
                                                    <input type="text" class="form-control" name="filters[filter_causer]" id="filter_causer"
                                                           value="<?= $causer ?>" placeholder="Search Causer id/name..." />
                                                </div>
												<div class="form-group">
													<input type="text" class="form-control" name="filters[filter_info]" id="filter_info"
														   value="<?= session()->get($node->name().'.filters.filter_info') ?>" placeholder="Search Info..." />
												</div>
												<input style="visibility: hidden" type="submit" />
											</form>
										</div>
									</div>
									<form id="app_logs_list_form" name="app_logs_list_form" method="POST" action='<?= $node->path() ?>?action=deleteAppLog'>
										<div class="actions pull-right" style="padding-bottom: 10px">
											<input type="hidden" name="csrf_name" value="<?php echo $csrf_name; ?>">
											<input type="hidden" name="csrf_value" value="<?php echo $csrf_value; ?>">
                                            <?php if(user()->has_permission_to('deleteAppLog')) : ?>
                                                <button id="delete_all" type="submit" class="btn btn-transparent red btn-outline  btn-sm active">Delete app log</button>
                                            <?php endif; ?>
										</div>
										<div id="app_log_table" class="portlet-body table-responsive" style="width: 100%">
											<div><?= $data['rows']->fullPagination ?></div>
											<table class="table table-striped table-hover margin-top-20">
												<thead>
													<tr>
														<th>
															<div class="form-group form-md-checkboxes" style="margin: 0 auto;width: 20px;padding-top: 0;">
																<div class="md-checkbox-list" style="margin: 2px auto;">
																	<div class="md-checkbox">
																		<input id="select_all" type="checkbox" class="md-check item-checkbox">
																		<label for="select_all"><span></span><span class="check"></span><span class="box"></span></label>
																	</div>
																</div>
															</div>
														</th>
														<th>
															<a href="<?= navigation(true, false) ?>&s=app_logs_id">ID
																<?= sorting() == 'app_logs_id' ? sorting_icon() : sorting_icon_default() ?>
															</a>
														</th>
                                                        <th>
                                                            <a href="<?= navigation(true, false) ?>&s=app_logs_type">Type
                                                                <?= sorting() == 'app_logs_type' ? sorting_icon() : sorting_icon_default() ?>
                                                            </a>
                                                        </th>
                                                        <th>
                                                            <a href="<?= navigation(true, false) ?>&s=app_logs_model">Model
                                                                <?= sorting() == 'app_logs_model' ? sorting_icon() : sorting_icon_default() ?>
                                                            </a>
                                                        </th>
														<th>
															<a href="<?= navigation(true, false) ?>&s=app_logs_info">Info
																<?= sorting() == 'app_logs_info' ?  sorting_icon() : sorting_icon_default() ?>
															</a>
														</th>
                                                        <th>
                                                            <a href="<?= navigation(true, false) ?>&s=app_logs_causer">Causer
                                                                <?= sorting() == 'app_logs_causer' ?  sorting_icon() : sorting_icon_default() ?>
                                                            </a>
                                                        </th>
                                                        <th>
                                                            <a href="<?= navigation(true, false) ?>&s=app_logs_created_on">Created On
                                                                <?= sorting() == 'app_logs_created_on' ?  sorting_icon() : sorting_icon_default() ?>
                                                            </a>
                                                        </th>
													</tr>
												</thead>
												<tbody>
													<?php if(!empty($data['rows']->items())) : ?>
														<?php foreach ($data['rows']->items() as $appLog) : ?>
															<tr>
																<td>
																	<div class="form-group form-md-checkboxes" style="margin: 0 auto;width: 20px;padding-top: 0;">
																		<div class="md-checkbox-list" style="margin: 2px auto;">
																			<div class="md-checkbox">
																				<input name="app_log_checkbox[<?= $appLog->id() ?>]" id="<?= $appLog->id() ?>" type="checkbox" class="md-check item-checkbox">
																				<label for="<?= $appLog->id() ?>"><span></span><span class="check"></span><span class="box"></span></label>
																			</div>
																		</div>
																	</div>
																</td>
																<td><?= $appLog->id() ?></td>
                                                                <td><?= $appLog->type() ?></td>
                                                                <td><?= $appLog->model_instance() ?></td>
																<td>
																	<?php if(user()->has_permission_to('showAppLogInfo')) : ?>
                                                                        <a class="btn btn-transparent blue btn-outline btn-sm active ajax-demo" id="ajax-demo-<?= $appLog->id() ?>" data-id="<?= $appLog->id() ?>" data-url="<?= $node->path() ?>?action=showAppLogInfo" data-toggle="modal">View More Info</a>
                                                                        <div id="ajax-modal-<?= $appLog->id() ?>" class="app-log-modal modal fade container" tabindex="-1"> </div>
																	<?php else : ?>
                                                                        <span style="text-decoration: line-through;">View More Info</span>
																	<?php endif; ?>
																</td>
																<td><?= $appLog->causer()->username() ?></td>
																<td><?= $appLog->created_on(true) ?></td>
															</tr>
														<?php endforeach; ?>
													<?php endif; ?>
												</tbody>
											</table>
											<div><?= $data['rows']->fullPagination ?></div>
											<div class="margin-top-20">Total rows: <?= $data['rows']->total() ?></div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->
			<!-- BEGIN QUICK SIDEBAR -->
			<?php require DIR . '/src/Views/Backend/inc/quick_sidebar.v.php' ?>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->

		<!-- BEGIN FOOTER -->
		<?php require DIR . '/src/Views/Backend/inc/footer.v.php' ?>
		<!-- END FOOTER -->

		<?php require DIR.'/src/Views/Backend/inc/global_js.php' ?>

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
        <script src="<?= env('DOMAIN') ?>/theme/admin/assets/pages/js/appLogs.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
	</body>

</html>