<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Info</h4>
</div>
<div class="modal-body">
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Type</th>
                <th>Model</th>
                <th>Causer</th>
                <th>Date</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><?= $data['log']->id() ?></td>
                <td><?= $data['log']->type() ?></td>
                <td><?= $data['log']->model_instance() ?></td>
                <td><?= $data['log']->causer()->username() ?></td>
                <td><?= $data['log']->created_on(true) ?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="portlet light table-responsive">
        <div class="portlet-body form">
            <?php if (is_object($data['logInfo'])) : ?>
            <?php foreach ((array)$data['logInfo'] as $key => $value) : ?>
                <strong><?= $key ?></strong> => <span style="line-break: auto">
                        <?php if(is_object($value)) : ?>
                        <?php
                            print "<pre>";
                            print_r((array) $value);
                            print "</pre>";
                        ?>
                        <?php else: ?>
                            <?= $value ?>
                        <?php endif; ?>
                    </span> <br />
            <?php endforeach; ?>
            <?php else: ?>
                <?php
                    print "<pre>";
                    print_r($data['logInfo']);
                    print "</pre>";
                ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Close</button>
</div>