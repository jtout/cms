<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Info</h4>
</div>
<div class="modal-body">
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Type</th>
                <th>Model</th>
                <th>Causer</th>
                <th>Date</th>
                <th>IP</th>
                <th>Detected Value</th>
            </tr>
            </thead>
            <tbody>
            <?php if(isset($data['logInfo']) && !empty($data['logInfo'])) : ?>
                <tr>
                    <td><?= $data['log']->id() ?></td>
                    <td><?= $data['log']->type() ?></td>
                    <td><?= $data['log']->model_instance() ?></td>
                    <td><?= $data['log']->causer()->username() ?></td>
                    <td><?= $data['log']->created_on(true) ?></td>
                    <td><?= $data['logInfo']->ip ?></td>
                    <td><?= $data['logInfo']->value ?></td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
    <?php if(isset($data['logInfo']) && !empty($data['logInfo']->inputs)) : ?>
    <div class="portlet light table-responsive">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject font-red-sunglo bold">User Inputs</span>
            </div>
        </div>
        <div class="portlet-body form">
        <?php foreach ((array)$data['logInfo']->inputs as $key => $input) : ?>
            <?php if (is_array($input) || is_object($input)) : ?>
            <?php foreach ($input as $k => $v) : ?>
                <strong><?= $k ?></strong> => <span style="line-break: auto"><?= $v ?></span> <br />
            <?php endforeach; ?>
            <?php else: ?>
                <strong><?= $key ?></strong> => <span style="line-break: auto"><?= $input ?></span> <br />
            <?php endif; ?>
        <?php endforeach; ?>
        </div>
    </div>
    <?php endif;?>
    <div class="portlet light table-responsive">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject font-red-sunglo bold">Server Params</span>
            </div>
        </div>
        <div class="portlet-body form">
			<?php if(isset($data['logInfo']) && !empty($data['logInfo'])) : ?>
            <?php foreach ($data['logInfo']->server_params as $key => $server_param) : ?>
                <strong><?= $key ?></strong> => <span style="line-break: auto"><?= $server_param ?></span> <br />
            <?php endforeach; ?>
			<?php endif;?>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Close</button>
</div>