<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title><?= SITENAME ?> | <?= $node->title() ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="<?= SITENAME ?> Dashboard" name="description" />
		<meta content="" name="author" />
		<?php require_once DIR.'/src/Views/Backend/inc/global_css.php' ?>
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/icheck/skins/all.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css" />

		<!-- END PAGE LEVEL PLUGINS -->
	</head>
	<!-- END HEAD -->

	<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-fixed page-md <?= session()->get('user.side_menu_open')  === true ? 'page-sidebar-closed' : ''?>">

		<!-- BEGIN HEADER -->
		<div class="page-header navbar navbar-fixed-top">
			<!-- BEGIN HEADER INNER -->
			<div class="page-header-inner ">
				<!-- BEGIN LOGO -->
				<?php require(DIR . '/src/Views/Backend/inc/logo.v.php'); ?>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN PAGE ACTIONS -->
				<!-- DOC: Remove "hide" class to enable the page header actions -->
				<?php require_once DIR. '/src/Views/Backend/inc/dropdown.v.php'?>
				<!-- END PAGE ACTIONS -->
				<!-- BEGIN PAGE TOP -->
				<div class="page-top">
					<!-- BEGIN TOP NAVIGATION MENU -->
					<?php require_once DIR. '/src/Views/Backend/inc/top_menu.v.php'?>
					<!-- END TOP NAVIGATION MENU -->
				</div>
				<!-- END PAGE TOP -->
			</div>
			<!-- END HEADER INNER -->
		</div>
		<!-- END HEADER -->
		<!-- BEGIN HEADER & CONTENT DIVIDER -->
		<div class="clearfix"> </div>
		<!-- END HEADER & CONTENT DIVIDER -->
		<!-- BEGIN CONTAINER -->
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/sidebar.v.php' ?>
			<!-- END SIDEBAR -->
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<!-- BEGIN PAGE HEADER-->
					<h1 class="page-title"> <?= $node->title() ?>
						<small></small>
					</h1>
					<!-- BEGIN BREADCRUMBS -->
					<?php require_once ( DIR . '/src/Views/Backend/inc/breadcrumbs.v.php' ); ?>
					<!-- END BREADCRUMBS -->
					<?php if(!empty($messages)) : ?>
						<?php foreach($messages as $type => $msgs) : ?>
							<?php foreach($msgs as $msg) : ?>
								<div class="alert alert-<?= $type ?>">
									<button class="close" data-close="alert"></button>
									<span><strong><?= $msg ?></strong></span>
								</div>
							<?php endforeach; ?>
						<?php endforeach; ?>
					<?php endif; ?>
					<!-- END PAGE HEADER-->

					<div class="row">
						<div class="col-md-12">
							<div class="portlet light ">
								<div class="portlet-title">
									<div class="caption">
										<span class="caption-subject font-red-sunglo bold"><?= $data['role']->title() ?></span>
									</div>
								</div>
								<div class="portlet-body form">
									<form id="roles_form" role="form" action="<?= $node->path() ?>?action=updateRole" method="POST" class="tab-content" enctype="multipart/form-data">
										<input type="hidden" name="csrf_name" value="<?php echo $csrf_name; ?>">
										<input type="hidden" name="csrf_value" value="<?php echo $csrf_value; ?>">
										<input type="hidden" id="selected_permissions" name="selected_permissions" value="" />
                                        <input type="hidden" id="id" name="id" value="<?= $data['role']->id() ?>" />

										<?php if($data['role']->id() != 1) : ?>
										<div class="form-group">
											<label for="parent_id" class="control-label">Parent</label>
											<select id="parent_id" class="form-control" name="parent_id">
												<?php foreach($data['parents'] as $parent) : ?>
													<option value="<?= $parent->id() ?>" <?= $parent->id() == $data['role']->parent_id() ? 'selected' : '' ?>>
														<?= str_repeat(' - ', $parent->depth()).$parent->title() ?>
													</option>
												<?php endforeach; ?>
											</select>
										</div>
										<?php endif; ?>

										<div class="form-group">
											<label for="access_level_id" class="control-label">Access Level</label>
											<select id="access_level_id" class="form-control" name="access_level_id">
												<?php foreach($data['access_levels'] as $accessLevel) : ?>
													<option value="<?= $accessLevel->id() ?>" <?= $accessLevel->id() == $data['role']->access_level_id() ? 'selected' : '' ?>>
														<?= str_repeat(' - ', $accessLevel->depth()).$accessLevel->title() ?>
													</option>
												<?php endforeach; ?>
											</select>
										</div>

										<div class="form-group">
											<label class="control-label">Title</label>
											<input type="text" name="title" value="<?= $data['role']->title() ?>" placeholder="Title" class="form-control" />
										</div>

										<div class="form-group">
											<label class="control-label">In Backend</label>
											<select id="in_backend" class="form-control" name="in_backend">
												<option value="1" <?= $data['role']->in_backend() == 1 ? 'selected' : '' ?>>Yes</option>
												<option value="0" <?= $data['role']->in_backend() == 0 ? 'selected' : '' ?>>No</option>
											</select>
										</div>

                                        <div class="form-group">
                                            <label class="control-label">Permissions</label>
											<div id="roles_tree">
												<?= $data['permissions'] ?>
											</div>
                                        </div>

										<!-- FORM ACTIONS -->
										<div class="form-actions">
											<div class="row">
												<div class="col-md-4">
													<button type="submit" id="submit" class="btn green">Save</button>
													<a href="<?= env('DOMAIN').'/'.env('ADMIN_NAME').'/system-settings/roles' ?>" class="btn default">Cancel</a>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->
			<!-- BEGIN QUICK SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/quick_sidebar.v.php' ?>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->

		<!-- BEGIN FOOTER -->
		<?php require_once DIR. '/src/Views/Backend/inc/footer.v.php' ?>
		<!-- END FOOTER -->

		<?php require_once DIR.'/src/Views/Backend/inc/global_js.php' ?>

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/icheck/icheck.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/pages/js/roles.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->

	</body>

</html>