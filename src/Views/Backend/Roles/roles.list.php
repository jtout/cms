<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title><?= SITENAME ?> | <?= $node->title() ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="<?= SITENAME ?> Dashboard" name="description" />
		<meta content="" name="author" />
		<?php require_once DIR.'/src/Views/Backend/inc/global_css.php' ?>
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

		<!-- END PAGE LEVEL PLUGINS -->
	</head>
	<!-- END HEAD -->

	<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-fixed page-md <?= session()->get('user.side_menu_open')  === true ? 'page-sidebar-closed' : ''?>">

		<!-- BEGIN HEADER -->
		<div class="page-header navbar navbar-fixed-top">
			<!-- BEGIN HEADER INNER -->
			<div class="page-header-inner ">
				<!-- BEGIN LOGO -->
				<?php require(DIR . '/src/Views/Backend/inc/logo.v.php'); ?>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN PAGE ACTIONS -->
				<!-- DOC: Remove "hide" class to enable the page header actions -->
				<?php require_once DIR. '/src/Views/Backend/inc/dropdown.v.php'?>
				<!-- END PAGE ACTIONS -->
				<!-- BEGIN PAGE TOP -->
				<div class="page-top">
					<!-- BEGIN TOP NAVIGATION MENU -->
					<?php require_once DIR. '/src/Views/Backend/inc/top_menu.v.php'?>
					<!-- END TOP NAVIGATION MENU -->
				</div>
				<!-- END PAGE TOP -->
			</div>
			<!-- END HEADER INNER -->
		</div>
		<!-- END HEADER -->
		<!-- BEGIN HEADER & CONTENT DIVIDER -->
		<div class="clearfix"> </div>
		<!-- END HEADER & CONTENT DIVIDER -->
		<!-- BEGIN CONTAINER -->
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/sidebar.v.php' ?>
			<!-- END SIDEBAR -->
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<!-- BEGIN PAGE HEADER-->
					<h1 class="page-title"> <?= $node->title() ?>
						<small></small>
					</h1>
					<!-- BEGIN BREADCRUMBS -->
					<?php require_once ( DIR . '/src/Views/Backend/inc/breadcrumbs.v.php' ); ?>
					<!-- END BREADCRUMBS -->
					<?php if(!empty($messages)) : ?>
						<?php foreach($messages as $type => $msgs) : ?>
							<?php foreach($msgs as $msg) : ?>
								<div class="alert alert-<?= $type ?>">
									<button class="close" data-close="alert"></button>
									<span><strong><?= $msg ?></strong></span>
								</div>
							<?php endforeach; ?>
						<?php endforeach; ?>
					<?php endif; ?>
					<!-- END PAGE HEADER-->

					<div class="row">
						<div class="col-md-12">
							<div class="table-scrollable">
								<div class="portlet light ">
									<div class="portlet-title">
										<div class="caption">
											<form id="search_list" name="search_list" class="form-inline" action='<?= $node->path() ?>?action=showRolesList' method="GET">
                                                <div class="form-group">
													<input type="text" class="form-control" name="filters[filter_title]" id="filter_title"
														   value="<?= session()->get($node->name().'.filters.filter_title') ?>" placeholder="Search Title..." />
												</div>
												<div class="form-group">
                                                    <select id="filter_backend" class="form-control" name="filters[filter_backend]">
														<option value="" >Select Roles</option>
														<option value="1" <?= session()->get($node->name().'.filters.filter_backend') == 1 ? 'selected' : '' ?>>Backend</option>
														<option value="2" <?= session()->get($node->name().'.filters.filter_backend') == 2 ? 'selected' : '' ?>>Frontend</option>
													</select>
												</div>
                                                <div class="form-group">
                                                    <select id="filter_access_level" class="form-control" name="filters[filter_access_level]">
                                                        <option value="" >Select Access Level</option>
                                                        <?php foreach($data['access_levels'] as $accessLevel) : ?>
                                                            <option value="<?= $accessLevel->id() ?>"
                                                                <?= session()->get($node->name().'.filters.filter_access_level') == $accessLevel->id() ? 'selected' : '' ?>>
                                                                <?= $accessLevel->title() ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
											</form>
										</div>
									</div>
									<form id="modules_list_form" name="modules_list_form" method="POST" action='<?= $node->path() ?>?action=deleteRole'>
										<div class="actions pull-right" style="padding-bottom: 10px">
											<input type="hidden" name="csrf_name" value="<?php echo $csrf_name; ?>">
											<input type="hidden" name="csrf_value" value="<?php echo $csrf_value; ?>">
                                            <?php if(user()->has_permission_to('addRole')) : ?>
                                                <a class="btn btn-transparent green btn-outline  btn-sm active" href="<?= $node->path() ?>?action=addRole">Add new role</a>
                                            <?php endif; ?>
                                            <?php if(user()->has_permission_to('deleteRole')) : ?>
                                                <button id="delete_all" type="submit" class="btn btn-transparent red btn-outline  btn-sm active">Delete role</button>
                                            <?php endif; ?>
										</div>
										<div id="modules_table" name="modules_table" class="portlet-body table-responsive" style="width: 100%">
											<div><?= $data['rows']->fullPagination ?></div>
											<table class="table table-striped table-hover margin-top-20">
												<thead>
													<tr>
														<th>
															<div class="form-group form-md-checkboxes" style="margin: 0 auto;width: 20px;padding-top: 0;">
																<div class="md-checkbox-list" style="margin: 2px auto;">
																	<div class="md-checkbox">
																		<input id="select_all" type="checkbox" class="md-check item-checkbox">
																		<label for="select_all"><span></span><span class="check"></span><span class="box"></span></label>
																	</div>
																</div>
															</div>
														</th>
														<th>
															<a href="<?= navigation(true, false) ?>&s=roles_id">ID
																<?= sorting() == 'roles_id' ? sorting_icon() : sorting_icon_default() ?>
															</a>
														</th>
														<th>
															<a href="<?= navigation(true, false) ?>&s=roles_title">Title
																<?= sorting() == 'roles_title' ? sorting_icon() : sorting_icon_default() ?>
															</a>
														</th>
														<th>
															<a href="<?= navigation(true, false) ?>&s=roles_access_level">Access Level
																<?= sorting() == 'roles_access_level' ? sorting_icon() : sorting_icon_default() ?>
															</a>
														</th>
														<th>
															<a href="<?= navigation(true, false) ?>&s=roles_backend">In backend
																<?= sorting() == 'roles_backend' ? sorting_icon() : sorting_icon_default() ?>
															</a>
														</th>
													</tr>
												</thead>
												<tbody>
													<?php if(!empty($data['rows']->items())) : ?>
														<?php foreach ($data['rows']->items() as $role) : ?>
															<tr>
																<td>
																	<div class="form-group form-md-checkboxes" style="margin: 0 auto;width: 20px;padding-top: 0;">
																		<div class="md-checkbox-list" style="margin: 2px auto;">
																			<div class="md-checkbox">
																				<input name="role_checkbox[<?= $role->id() ?>]" id="<?= $role->id() ?>" value="<?= $role->id() ?>" type="checkbox" class="md-check item-checkbox">
																				<label for="<?= $role->id() ?>"><span></span><span class="check"></span><span class="box"></span></label>
																			</div>
																		</div>
																	</div>
																</td>
																<td><?= $role->id() ?></td>
																<td>
																	<?php if(user()->has_permission_to('updateRole')) : ?>
																		<a href="<?= $node->path() ?>?action=editRole&id=<?= $role->id() ?>">
																			<?= str_repeat(' - ', $role->depth()) . $role->title() ?>
																		</a>
																	<?php else : ?>
																		<?= str_repeat(' - ', $role->depth()) . $role->title() ?>
																	<?php endif; ?>
																</td>
																<td><span class="label <?= $role->access_level_label() ?>"><?= $role->access_level() ?></span> </td>
																<td><i class="fa fa-<?= ($role->in_backend() == 1) ? 'check' : 'times'; ?> font-<?= ($role->in_backend() == 1) ? 'green-jungle' : 'red-thunderbird'; ?> font-lg"></i></td>
															</tr>
														<?php endforeach; ?>
													<?php endif; ?>
												</tbody>
											</table>
											<div><?= $data['rows']->fullPagination ?></div>
											<div class="margin-top-20">Total rows: <?= $data['rows']->total() ?></div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->
			<!-- BEGIN QUICK SIDEBAR -->
			<?php require_once DIR. '/src/Views/Backend/inc/quick_sidebar.v.php' ?>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->

		<!-- BEGIN FOOTER -->
		<?php require_once DIR. '/src/Views/Backend/inc/footer.v.php' ?>
		<!-- END FOOTER -->

		<?php require_once DIR.'/src/Views/Backend/inc/global_js.php' ?>

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
		<script src="<?= env('DOMAIN') ?>/theme/admin/assets/pages/js/roles.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->

	</body>

</html>