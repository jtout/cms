<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title><?= SITENAME ?> | <?= $node->title() ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="<?= SITENAME ?> Dashboard" name="description" />
        <meta content="" name="author" />
        <?php require_once DIR.'/src/Views/Backend/inc/global_css.php' ?>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <link href="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-fixed page-md <?= session()->get('user.side_menu_open')  === true ? 'page-sidebar-closed' : ''?>">

    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <?php require(DIR . '/src/Views/Backend/inc/logo.v.php'); ?>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN PAGE ACTIONS -->
            <!-- DOC: Remove "hide" class to enable the page header actions -->
            <?php require_once DIR. '/src/Views/Backend/inc/dropdown.v.php'?>
            <!-- END PAGE ACTIONS -->
            <!-- BEGIN PAGE TOP -->
            <div class="page-top">
                <!-- BEGIN TOP NAVIGATION MENU -->
                <?php require_once DIR. '/src/Views/Backend/inc/top_menu.v.php'?>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END PAGE TOP -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php require_once DIR. '/src/Views/Backend/inc/sidebar.v.php' ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <h1 class="page-title"> <?= $node->title() ?>
                    <small></small>
                </h1>
                <!-- BEGIN BREADCRUMBS -->
                <?php require_once( DIR . '/src/Views/Backend/inc/breadcrumbs.v.php' ); ?>
                <!-- END BREADCRUMBS -->
                <?php if (!empty($messages)) : ?>
                    <?php foreach($messages as $type => $msgs) : ?>
                        <?php foreach($msgs as $msg) : ?>
                            <div class="alert alert-<?= $type ?>">
                                <button class="close" data-close="alert"></button>
                                <span><strong><?= $msg ?></strong></span>
                            </div>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
                <!-- END PAGE HEADER-->

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-red-sunglo bold"><?= $data['nodeEntity']->title() ?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="tabbable-line tabbable-full-width">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#main_info" data-toggle="tab"> Main Info </a>
                                        </li>
                                        <li class="">
                                            <a href="#main_image" data-toggle="tab"> Main Image & Gallery </a>
                                        </li>
                                        <li class="">
                                            <a href="#video_info" data-toggle="tab"> Video </a>
                                        </li>
                                        <li class="">
                                            <a href="#extra_fields" data-toggle="tab"> Extra fields </a>
                                        </li>
                                        <li class="">
                                            <a href="#meta_info" data-toggle="tab"> Publishing and Meta data </a>
                                        </li>
                                        <li class="">
                                            <a href="#translations" data-toggle="tab"> Translations </a>
                                        </li>
                                        <li class="">
                                            <a href="#settings" data-toggle="tab"> Settings </a>
                                        </li>
                                    </ul>
                                    <form role="form" action="<?= $node->path() ?>?action=updateArticle" method="POST" class="tab-content" enctype="multipart/form-data">
                                        <input type="hidden" name="csrf_name" value="<?php echo $csrf_name; ?>">
                                        <input type="hidden" name="csrf_value" value="<?php echo $csrf_value; ?>">
                                        <input type="hidden" name="id" id="id" value="<?= $data['nodeEntity']->id() ?>">
                                        <input type="hidden" name="old_gallery" value="<?= $data['nodeEntity']->gallery() ?>">
                                        <input type="hidden" name="old_image" value="<?= !empty($data['nodeEntity']->image()) ? $data['nodeEntity']->image() : '' ?>" />
                                        <input type="hidden" name="order" value="<?= $data['nodeEntity']->order() ?>" />

                                        <!-- MAIN INFO -->
                                        <div class="tab-pane active" id="main_info">
                                            <div class="form-group">
                                                <label class="control-label">Title</label>
                                                <input type="text" name="title" value="<?= $data['nodeEntity']->title() ?>" placeholder="Title" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Alias</label>
                                                <input type="text" name="alias" value="<?= $data['nodeEntity']->name() ?>" placeholder="Alias" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label for="parent_id" class="control-label">Category</label>
                                                <select id="parent_id" name="parent_id">
                                                    <?php foreach ($data['parents'] as $category) :?>
                                                        <option value="<?= $category->id() ?>"
                                                            <?= $data['nodeEntity']->parent_id()  == $category->id() ? 'selected' : '' ?>>
                                                            <?php if ($category->depth() > 2) : ?>
                                                                <?= str_repeat(' - ', $category->depth() - 2) . $category->title() ?>
                                                            <?php else : ?>
                                                                <?= $category->title() ?>
                                                            <?php endif; ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="tags" class="control-label">Tags</label>
                                                <select id="tags" name="tags[]" multiple>
                                                    <?php if ($data['nodeEntity']->tags()->count() > 0) : ?>
                                                        <?php foreach ($data['nodeEntity']->tags() as $tag) : ?>
                                                            <option value="<?= $tag->id() ?>" selected="selected"><?= $tag->title() ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                            <div class="row">
                                                <?php if (user()->has_permission_to('canPublishArticles')) : ?>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label for="is_active">Published</label><br />
                                                        <input id="is_active" value="" name="is_active" type="checkbox"  data-on-text="Yes" data-off-text="No" class="make-switch"  data-on-color="success" data-off-color="danger"
                                                            <?= $data['nodeEntity']->is_active() == 1 ? 'checked' : '' ?>/>
                                                    </div>
                                                </div>
                                                <?php endif; ?>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label for="is_featured">Featured</label><br />
                                                        <input id="is_featured" value="" name="is_featured" type="checkbox"  data-on-text="Yes" data-off-text="No" class="make-switch"  data-on-color="success" data-off-color="danger"
                                                            <?= $data['nodeEntity']->is_featured() == 1 ? 'checked' : '' ?>/>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="is_sticky">Sticky</label><br />
                                                        <input id="is_sticky" value="" name="is_sticky" type="checkbox"  data-on-text="Yes" data-off-text="No" class="make-switch"  data-on-color="success" data-off-color="danger"
                                                            <?= $data['nodeEntity']->is_sticky() == 1 ? 'checked' : '' ?>/>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="created_by" class="">Author</label>
                                                        <select id="created_by" name="created_by">
                                                            <?php foreach ($data['authors'] as $author) :?>
                                                                <option value="<?= $author->id() ?>"
                                                                    <?= $data['nodeEntity']->created_by() == $author->id() ? 'selected' : '' ?>>
                                                                    <?= $author->name().' '.$author->last_name() ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="access_level_id" class="">Access Level</label>
                                                        <select id="access_level_id" name="access_level_id">
                                                            <?php rsort($data['access_levels']); ?>
                                                            <?php foreach ($data['access_levels'] as $accessLevel) :?>
                                                                <option value="<?= $accessLevel->id() ?>"
                                                                    <?= $data['nodeEntity']->access_level_id() == $accessLevel->id() ? 'selected' : '' ?>>
                                                                    <?= $accessLevel->title() ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="language_id" class="">Language <?= count($data['translations']) > 0 ? '(<a href="#translations" data-toggle="tab">Translations</a>)' : '' ?></label>
                                                        <select id="language_id" name="language_id">
                                                            <option value="">Select Language</option>
                                                            <?php foreach ($data['languages'] as $language) :?>
                                                                <option value="<?= $language->id() ?>"
                                                                    <?php
                                                                    if ( $data['nodeEntity']->language_id() == $language->id())
                                                                        echo 'selected';
                                                                    else if (count($data['translations']) > 0 && $data['nodeEntity']->language_id() != $language->id())
                                                                        echo 'disabled';
                                                                    else
                                                                        echo '';
                                                                    ?>>
                                                                    <?= $language->title() ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label class="control-label">Intro Text</label>
                                                <input type="text" name="intro_text" value="<?= $data['nodeEntity']->intro_text(); ?>" placeholder="Intro text" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label for="description" class="control-label">Description</label>
                                                <textarea class="form-control" id="description" name="description" rows="15"><?= $data['nodeEntity']->description() ?></textarea>
                                            </div>
                                        </div>

                                        <!-- MAIN IMAGE & GALLERY -->
                                        <div class="tab-pane" id="main_image">
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 400px; height: 250px;">
                                                        <?php if (!empty($data['nodeEntity']->image())) : ?>
                                                            <img class="article-image" src="<?= $data['nodeEntity']->image(true, 'Articles', 'recentNewsImage') ?>" />
                                                        <?php else : ?>
                                                            <img class="article-image" src="https://via.placeholder.com/400x250.png" alt="">
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 400px; max-height: 250px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new select-main-image">Select Image</span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="image" value="" />
                                                        </span>
                                                        <a href="javascript:;" class="btn default fileinput-exists remove-image" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                                <div class="clearfix margin-top-10">
                                                    OR
                                                    <div class="input-append">
                                                        <input id="imageServer" name="imageServer" type="text" value="">
                                                        <a href="<?= env('DOMAIN') ?>/media/filemanager/dialog.php?akey=<?= env('MEDIAKEY') ?>&type=1&amp;field_id=imageServer'&amp;fldr=" class="btn default btn-file iframe-btn" type="button">Browse Server</a>
                                                    </div>
                                                </div>
                                                <div class="fileinput fileinput-new margin-top-30" data-provides="fileinput">
                                                    <div class="input-group input-large">
                                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                            <span class="fileinput-filename"></span>
                                                        </div>
                                                        <span class="input-group-addon btn default btn-file">
                                                                    <span class="fileinput-new "> Upload Gallery (.zip file) </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="hidden" value=""><input type="file" name="gallery"> </span>
                                                        <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                                <?php if (isset($data['nodeEntity']->gallery(true)['thumbs'])) : ?>
                                                    <div style="margin:20px 0;">
                                                        <?php foreach($data['nodeEntity']->gallery(true)['thumbs'] as $key => $photo) : ?>
                                                            <a class="grouped_elements" href="<?= $data['nodeEntity']->gallery(true)['source'][$key] ?>" rel="gal">
                                                                <img src="<?= $photo ?>" />
                                                            </a>
                                                        <?php endforeach;?>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="delete_gallery">Delete Existing gallery</label><br />
                                                        <input id="delete_gallery" value="" name="delete_gallery" type="checkbox"  data-on-text="Yes" data-off-text="No" class="make-switch"  data-on-color="success" data-off-color="danger"/>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>

                                        <!-- VIDEO -->
                                        <div class="tab-pane" id="video_info">
                                            <div class="form-group">
                                                <label for="source" class="">Source</label>
                                                <select id="source" name="source">
                                                    <?php foreach ($data['sources'] as $source) :?>
                                                        <option value="<?= $source ?>" <?= $source == $data['nodeEntity']->video_source() ? 'selected' : '' ?>>
                                                            <?= $source ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="video" class="control-label">Video ID for providers OR embed code for HTML</label>
                                                <textarea id="video" name="video" class="form-control" rows="6"><?= $data['nodeEntity']->video_stripped() ?></textarea>
                                            </div>

                                            <?php if (!empty($data['nodeEntity']->video())) : ?>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="embed-responsive embed-responsive-16by9">
                                                        <?= $data['nodeEntity']->video(true) ?>
                                                    </div>
                                                </div>
                                            </div> <br />
                                            <?php endif; ?>
                                        </div>

                                        <!-- EXTRA FIELDS -->
                                        <div class="tab-pane" id="extra_fields">
                                            <?php if (!empty($data['extra_fields'])) : ?>
                                                <?= $data['extra_fields'] ?>
                                            <?php endif; ?>
                                        </div>

                                        <!-- META INFO -->
                                        <div class="tab-pane" id="meta_info">
                                            <div class="form-group">
                                                <label class="control-label">Created On</label>
                                                <input class="form-control form-control-inline input-medium date-picker" name="created_on" size="16" type="text"
                                                       value="<?= $data['nodeEntity']->created_on()?>">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Published On</label>
                                                <input class="form-control form-control-inline input-medium date-picker" name="published_on" size="16" type="text"
                                                       value="<?= $data['nodeEntity']->published_on() ?>">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label hide">Finish Publishing On</label>
                                                <input type="hidden" class="form-control form-control-inline input-medium date-picker" name="finished_on" size="16" type="text"
                                                       value="<?= $data['nodeEntity']->finished_on() ?>">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Social Media Title</label>
                                                <input type="text" name="social_media_title" value="<?= $data['nodeEntity']->social_media_title() ?>" placeholder="Social Media title" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Meta Title</label>
                                                <input type="text" name="meta_title" value="<?= $data['nodeEntity']->meta_title() ?>" placeholder="Meta title" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Meta Description</label>
                                                <textarea  name="meta_description" class="form-control" rows="6"><?= $data['nodeEntity']->meta_description() ?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Robots</label>
                                                <select name="robots" id="robots" class="form-control">
                                                    <option value="INDEX, FOLLOW" <?= $data['nodeEntity']->robots() == 'INDEX, FOLLOW' ? 'selected' : '' ?>>Index, Follow</option>
                                                    <option value="INDEX, NOFOLLOW" <?= $data['nodeEntity']->robots() == 'INDEX, NOFOLLOW' ? 'selected' : '' ?>>Index, No Follow</option>
                                                    <option value="NOINDEX, NOFOLLOW" <?= $data['nodeEntity']->robots() == 'NOINDEX, NOFOLLOW' ? 'selected' : '' ?>>No Index, No Follow</option>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- TRANSLATIONS -->

                                        <div class="tab-pane" id="translations">
                                            <?php if (count($data['translations']) > 0) : ?>
                                                <?php foreach($data['translations'] as $translationCode => $translation) : ?>
                                                    <?php if ($data['nodeEntity']->language_iso_code() != $translationCode) : ?>
                                                        <?php if (is_object($translation)) : ?>
                                                            <?php foreach(LANGUAGES as $code => $title) : ?>
                                                                <?php if ($code == $translationCode) : ?>
                                                                    <div class="form-group">
                                                                        <a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/articles?action=editArticle&id=<?= $translation->translation_id ?>" class="btn default"><?= $title ?> Translation: <?=  $translation->title ?></a> &nbsp
                                                                        <a class="label label-danger" href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/articles?action=deleteArticleTranslation&node=<?= $data['nodeEntity']->id() ?>&translation=<?= $translation->translation_id ?>">Remove Translation</a>
                                                                    </div>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        <?php else : ?>
                                                            <div class="form-group">
                                                                <a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/articles?action=addArticle&from_id=<?= $data['nodeEntity']->id() ?>&iso_code=<?= $translationCode ?>" class="btn default">Add <?= $translation ?> Translation</a>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php else: ?>
                                                <?php foreach(LANGUAGES as $code => $title) : ?>
                                                    <?php if ($data['nodeEntity']->language_iso_code() != $code) : ?>
                                                        <div class="form-group">
                                                            <a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/articles?action=addArticle&from_id=<?= $data['nodeEntity']->id() ?>&iso_code=<?= $code ?>" class="btn default">Add <?= $title ?> Translation</a>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>

                                        <!-- EXTRA SETTINGS -->
                                        <div class="tab-pane" id="settings">
                                            <div class="row">
                                                <div class="form-group col-md-2">
                                                    <label for="view" class="">Template</label>
                                                    <select id="view" name="view" class="form-control">
                                                        <option value="from_category">Inherit from category</option>
                                                        <?php foreach($data['views'] as $view) : ?>
                                                            <option value="<?= $view ?>.view"  <?= $data['nodeEntity']->view() == 'Frontend/Articles/'.$view.'.view' ? 'selected' : '' ?>>
                                                                <?= ucfirst($view) ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-2">
                                                    <label for="sponsored_by_id" class="control-label">Sponsored By</label>
                                                    <select id="sponsored_by_id" name="sponsored_by_id">
                                                        <option value=""></option>
                                                        <?php foreach ($data['clients'] as $client) :?>
                                                            <option value="<?= $client->id() ?>"
                                                                <?= $data['nodeEntity']->sponsored_by_id() == $client->id() ? 'selected' : '' ?>>
                                                                <?= $client->title() ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-2">
                                                    <label for="comments_lock">Comments Lock</label><br />
                                                    <select id="comments_lock" name="comments_lock" class="form-control">
                                                        <option value="from_category">Inherit from category</option>
                                                        <option value="1" <?= $data['nodeEntity']->comments_lock() == 1 ? 'selected' : '' ?>>Yes</option>
                                                        <option value="0" <?= $data['nodeEntity']->comments_lock() == 0 ? 'selected' : '' ?>>No</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- FORM ACTIONS -->
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn green">Save</button>
                                                    <a href="<?= env('DOMAIN').'/'.env('ADMIN_NAME').'/articles' ?>" class="btn default">Cancel</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end tab-pane-->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->
        <?php require_once DIR. '/src/Views/Backend/inc/quick_sidebar.v.php' ?>
        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <?php require_once DIR. '/src/Views/Backend/inc/footer.v.php' ?>
    <!-- END FOOTER -->

    <?php require_once DIR.'/src/Views/Backend/inc/global_js.php' ?>

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/fancybox/source/jquery.fancybox.js" type="text/javascript"></script>
    <script src="<?= env('DOMAIN') ?>/theme/admin/assets/pages/js/content.js" type="text/javascript"></script>
    <script src="<?= env('DOMAIN') ?>/theme/admin/assets/pages/js/articles.js" type="text/javascript"></script>
	<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=<?= env('TINYMCEKEY') ?>"></script>
    <?php require_once DIR.'/src/Views/Backend/inc/editor.php' ?>
    <!-- END PAGE LEVEL SCRIPTS -->
    </body>
</html>