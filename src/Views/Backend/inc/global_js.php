<!--[if lt IE 9]>
<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/respond.min.js"></script>
<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/excanvas.min.js"></script>
<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?= env('DOMAIN') ?>/theme/admin/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?= env('DOMAIN') ?>/theme/admin/assets/layouts/layout2/scripts/layout.min.js" type="text/javascript"></script>
<script src="<?= env('DOMAIN') ?>/theme/admin/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="<?= env('DOMAIN') ?>/theme/admin/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->

<script>
	$(document).on('click', '.menu-toggler', function(){
		$.ajax({
			url:"?action=toggleSideMenuState",
			method: "GET",
			success:function(data) {}
		});
	});
</script>