<script>
    tinymce.init({
        selector: "textarea#description",theme: "modern",
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor code fullscreen"
        ],
        code_dialog_height: 600,
        code_dialog_width: 1200,
        toolbar1: "undo redo | blockquote | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
        toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code | fullscreen | code",
        image_advtab: true ,
        external_filemanager_path:"/media/filemanager/",
        filemanager_title:"Media Manager" ,
        external_plugins: { "filemanager" : "/media/filemanager/plugin.min.js"},
        filemanager_access_key:"<?= env('MEDIAKEY') ?>",
        width: '100%',
        height: 400,
        entity_encoding : "raw"
    });
</script>