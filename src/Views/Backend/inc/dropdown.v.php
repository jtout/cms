<div class="page-actions">
    <?php if(user()->has_permission_to('addArticle')) :  ?>
	<div class="btn-group">
		<button type="button" class="btn  btn-outline red dropdown-toggle" data-toggle="dropdown">
			<i class="fa fa-plus"></i>&nbsp;
			<span class="hidden-sm hidden-xs">New&nbsp;</span>&nbsp;
			<i class="fa fa-angle-down"></i>
		</button>
		<ul class="dropdown-menu" role="menu">
			<?php if(user()->has_permission_to('addArticle')) :  ?>
				<li>
					<a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/articles?action=addArticle">
						<i class="icon-notebook"></i> Article </a>
				</li>
			<?php endif; ?>
			<?php if(user()->has_permission_to('addTag')) :  ?>
				<li>
					<a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/tags?action=addTag">
						<i class="icon-tag"></i> Tag </a>
				</li>
			<?php endif; ?>
			<?php if(user()->has_permission_to('addCategory')) : ?>
				<li>
					<a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/categories?action=addCategory">
						<i class="icon-drawer"></i> Category </a>
				</li>
			<?php endif; ?>
			<?php if(user()->has_permission_to('addPage')) : ?>
				<li>
					<a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/pages?action=addPage">
						<i class="icon-book-open"></i> Page </a>
				</li>
			<?php endif; ?>
			<?php if(user()->has_permission_to('addModule')) : ?>
				<li>
					<a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/system-settings/modules?action=addModule">
						<i class="icon-puzzle"></i> Module </a>
				</li>
			<?php endif; ?>
			<?php if(user()->has_permission_to('addPanelNode')) : ?>
				<li>
					<a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/system-settings/<?= env('ADMIN_NAME') ?>-pages?action=addPanelNode">
						<i class="icon-map"></i> Panel Page </a>
				</li>
			<?php endif; ?>
		</ul>
	</div>
	<?php endif; ?>
	<?php if(user()->has_permission_to('showSystemSettings')) :  ?>
        <div class="btn-group">
            <button type="button" class="btn  btn-outline red dropdown-toggle" data-toggle="dropdown">
                <i class="icon-settings"></i>&nbsp;
                <span class="hidden-sm hidden-xs">System&nbsp;</span>&nbsp;
                <i class="fa fa-angle-down"></i>
            </button>
            <ul class="dropdown-menu" role="menu">
                <?php if(user()->has_permission_to('showAppLogsList')) : ?>
                    <li>
                        <a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/system-settings/app-logs">
                            <i class="icon-layers"></i> App Logs </a>
                    </li>
                <?php endif; ?>
                <?php if(user()->has_permission_to('showCacheList')) :  ?>
                    <li>
                        <a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/system-settings/cache">
                            <i class="glyphicon glyphicon-refresh"></i> Cache </a>
                    </li>
                <?php endif; ?>
                <?php if(user()->has_permission_to('editGeneralSettings')) :  ?>
                    <li>
                        <a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/system-settings/general-settings">
                            <i class="icon-wrench"></i> General Settings </a>
                    </li>
                <?php endif; ?>
                <?php if(user()->has_permission_to('showRolesList')) : ?>
                    <li>
                        <a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/system-settings/roles">
                            <i class="icon-key"></i> Roles </a>
                    </li>
                <?php endif; ?>
                <?php if(user()->has_permission_to('showAccessLevelsList')) : ?>
                    <li>
                        <a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/system-settings/access-levels">
                            <i class="icon-directions"></i> Access Levels </a>
                    </li>
                <?php endif; ?>
                <?php if(user()->has_permission_to('showLanguagesList')) : ?>
                    <li>
                        <a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/system-settings/languages">
                            <i class="icon-globe-alt"></i> Languages </a>
                    </li>
                <?php endif; ?>
                <?php if(user()->has_permission_to('showContentTypesList')) : ?>
                    <li>
                        <a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/system-settings/content-types">
                            <i class="icon-note"></i> Content Types </a>
                    </li>
                <?php endif; ?>
                <?php if(user()->has_permission_to('showModulesList')) : ?>
                    <li>
                        <a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/system-settings/modules">
                            <i class="icon-puzzle"></i> Modules </a>
                    </li>
                <?php endif; ?>
                <?php if(user()->has_permission_to('showPanelNodesList')) : ?>
                    <li>
                        <a href="<?= env('DOMAIN') ?>/<?= env('ADMIN_NAME') ?>/system-settings/panel-pages">
                            <i class="icon-map"></i> Panel Pages </a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
	<?php endif; ?>
</div>