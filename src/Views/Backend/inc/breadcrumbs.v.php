<!-- BEGIN PAGE BAR -->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<?php if(!empty($breadcrumbs)) : ?>
			<?php foreach ($breadcrumbs as $breadcrumb): ?>
				<li>
                    <i class="<?= $breadcrumb->icon() ?>"></i>
					<?php if ($breadcrumb != end($breadcrumbs)): ?>
                        <a href="<?= $breadcrumb->path() ?>"><span><?= $breadcrumb->title() ?></span></a>
                        <i class="fa fa-angle-right"></i>
					<?php else: ?>
                        <span><?= $breadcrumb->title() ?></span>
					<?php endif; ?>
				</li>
			<?php endforeach; ?>
		<?php endif; ?>
	</ul>
</div>
<!-- END PAGE BAR -->