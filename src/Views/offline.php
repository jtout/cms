<!DOCTYPE html>
<html lang="el">

<head>
    <title>Offline</title>
    <?php require_once DIR.'/src/Views/Frontend/Common/css.php' ?>
</head>

<body class="bg-light style-default style-rounded">

<main class="main oh" id="main">

    <div style="position: absolute;left: 50%;top: 50%;transform: translate(-50%, -50%);width: 600px;">
        <div class="main-container container pt-80 pb-80">
            <!-- post content -->
            <div class="blog__content mb-72">
                <div class="container text-center"">
                    <h1 class="page-404-number"><?= config()->get('offline_message') ?></h1>
                </div> <!-- end container -->
            </div> <!-- end post content -->
        </div> <!-- end main container -->
    </div>

</main> <!-- end main-wrapper -->

</body>
</html>