<?php

namespace App\Models;

use App\Traits\App;
use Illuminate\Database\Query\Builder;

class ModelQueryBuilder extends Builder
{
    /**
     * @var mixed
     */
    protected $model;

    /**
     * @var array
     */
    protected $relations = [];

    /**
     * @var bool
     */
    protected $toEntity;

    /**
     * @var string
     */
    protected $withFactory;

    /**
     * QueryBuilder constructor.
     * @param $model
     * @param array $relations
     */
    public function __construct($model = null, $relations = [])
    {
        parent::__construct(db());

        $this->model = $model ?? new Model;
        $this->relations = $relations;
    }

    /**
     * @param array $columns
     * @return \Illuminate\Support\Collection|mixed
     */
    public function get($columns = ['*'])
    {
        if (!empty($this->relations)) {

            $reflection = new \ReflectionClass(get_class($this->model));

            if ($reflection->hasMethod('loadQueryRelations')) {
                $this->model->loadQueryRelations($this, $this->relations);
            }

            $rows = parent::get($columns);

            if ($reflection->hasMethod('loadDataRelations')) {
                $relationsToSearch = $this->model->matchRelations($this->relations);
                $relationsData = $this->model->loadDataRelations($rows, array_keys($relationsToSearch));
                $rows = $this->model->mapRelationsDataToEntity($rows, $relationsToSearch, $relationsData);
            }
        }
        else {
            $rows = parent::get($columns);
        }

        return $this->toEntity ? $this->model->entity($rows, $this->withFactory) : $rows;
    }

    /**
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Model|Builder|mixed|object|null
     */
    public function first($columns = ['*'])
    {
        return $this->get($columns)->take(1)->first();
    }

    /**
     * @param int $perPage
     * @param array $columns
     * @param string $pageName
     * @param null $page
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Pagination\LengthAwarePaginator
     */
    public function paginate($perPage = 15, $columns = ['*'], $pageName = 'page', $page = null)
    {
        $rows = parent::paginate(session()->get('limit'), $columns, 'p', session()->get('page'))
            ->setPath(request()->getUri()->getPath())
            ->appends(request()->getQueryParams());

        $rows->getCollection()->transform(function ($value){
            $value = $this->model->entity($value);
            return $value;
        });

        $rows = App::renderPagination($rows);

        return $rows;
    }

    /**
     * @param array $relations
     * @return $this
     */
    public function withRelations(array $relations = [])
    {
        $this->relations = in_array('*', $relations) ? $this->model->getRelations() : $relations;

        return $this;
    }

    /**
     * @param null $factory
     * @return $this
     */
    public function withEntities($factory = null)
    {
        $this->withFactory = is_null($factory) ? $this->model->getFactory() : $factory;
        $this->toEntity = true;

        return $this;
    }
}