<?php

namespace App\Models\Backend;

use App\Models\Model;

class Panel extends Model
{
    /**
     * @return array
     */
    public function getContentNumbers() :array
    {
        $articlesCount = $this->table('nodes_content AS a')
            ->select('node_id')
            ->join('nodes AS b', 'b.id', '=', 'a.node_id')
            ->where([
                'a.content_type_id' => 1,
                'b.is_active' => 1
            ])->count();

        $categoriesCount = $this->table('nodes_content AS a')
            ->select('node_id')
            ->join('nodes AS b', 'b.id', '=', 'a.node_id')
            ->where([
                'a.content_type_id' => 2,
                'b.is_active' => 1
            ])->count();

        $tagsCount = $this->table('nodes_content AS a')
            ->select('node_id')
            ->join('nodes AS b', 'b.id', '=', 'a.node_id')
            ->where([
                'a.content_type_id' => 4,
                'b.is_active' => 1
            ])->count();

        $usersCount = $this->table('users')
            ->select('id')->count();

        $results['articles'] = $articlesCount;
        $results['categories'] = $categoriesCount;
        $results['tags'] = $tagsCount;
        $results['users'] = $usersCount;

        return $results;
    }
}