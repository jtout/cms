<?php

namespace App\Models\Backend;

use App\Traits\App;
use App\Models\Model;
use App\Factories\NodeFactory;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class Tiles extends Model
{
    /**
     * @var string
     */
    protected $table = 'tiles';

    /**
     * @var string
     */
    protected $factory = NodeFactory::class;

    /**
     * @var array
     */
    protected $relations = [
        'comments_count' => 'id'
    ];

    /**
     * @param Collection $records
     * @param array $relations
     * @return mixed
     */
    public function loadDataRelations(Collection $records, array $relations = [])
    {
        $relationsData = new Collection();

        if (in_array('comments_count', $relations)) {
            $relationsData->put('comments_count', $this->comments_count($records));
        }

        return $relationsData;
    }

    public function comments_count(Collection $records) :Collection
    {
        return (new Nodes)->comments_count($records);
    }

    /**
     * @param bool $withTree
     * @return mixed
     */
    public function getTiles($withTree = true)
    {
        $tiles['nodes']  = array();

        $query = $this->table('tiles AS a')->select([
            'a.node_id AS id',
            'a.ordering',
            'c.language_id',
            'c.title'
        ])
            ->join('nodes AS b', 'b.id', '=', 'a.node_id')
            ->join('nodes_content AS c', 'c.node_id', '=', 'b.id')
            ->orderBy('a.ordering');

        $rows = $query->get()->keyBy('id');
        $tiles['nodes'] = $this->entity($rows)->toArray();

        if ($withTree) {
            $tiles['tree'] = $this->createTilesTree($tiles['nodes']);
        }

        return $tiles;
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getArticles() :LengthAwarePaginator
    {
        $nodes = new Nodes();
        $articles = $nodes->list(false, 1);

        return $articles;
    }

    /**
     * @param int $id
     * @param int $order
     * @return array
     * @throws \Exception
     */
    public function addTile(int $id, int $order) :array
    {
        try
        {
            $response = array();

            $this->beginTransaction();
            
            $this->table()->insert(['node_id' => $id, 'ordering' => $order]);

            $this->commit();

            $response['success'] = true;
            $response['type'] = 'success';
            $response['message'] = 'Tile(s) added!';
        }
        catch (\Throwable $e)
        {
            $this->rollBack();

            $response['success'] = false;
            $response['type'] = 'danger';
            $response['message'] = $e->getMessage();
        }

        return $response;
    }

    /**
     * @return int
     */
    public function checkTilesOrdering()
    {
        $count = $this->table()->max('ordering');

        if (empty($count)) {
            $count = 0;
        }

        return $count;
    }

    /**
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function deleteTile(int $id) :array
    {
        try
        {
            $response = array();

            $this->beginTransaction();

            // Get ordering from deleting item
            $orderingFromDeletingNode = $this->table()->select(['*'])->where('node_id', $id)
                ->pluck('ordering')->first();

            // Select next items
            $results = $this->table()
                ->where('ordering', '>', $orderingFromDeletingNode)
                ->orderBy('ordering', 'asc')
                ->get('node_id');

            // Delete item
            $this->table()->where('node_id', $id)->delete();

            // Update Items ordering
            foreach ($results as $result) {
                $this->table()->where('node_id', $result->node_id)->update(['ordering' => (int)$orderingFromDeletingNode]);
                $orderingFromDeletingNode++;
            }

            $this->commit();

            $response['success'] = true;
            $response['type'] = 'success';
            $response['message'] = 'Tile removed!';
        }
        catch (\Throwable $e)
        {
            $this->rollBack();

            $response['success'] = false;
            $response['type'] = 'danger';
            $response['message'] = $e->getMessage();
        }

        return $response;
    }

    /**
     * @param array $ordering
     * @return array
     * @throws \Exception
     */
    public function updateOrder(array $ordering) :array
    {
        try
        {
            $response = array();

            $this->beginTransaction();

            foreach($ordering as $order => $id) {
                $order = App::sanitizeInput($order, 'INT');
                $id    = App::sanitizeInput($id, 'INT');

                $this->table()->where(['node_id' => $id])->update(['ordering' => $order]);
            }

            $this->commit();

            $response['success'] = true;
            $response['type'] = 'success';
            $response['message'] = 'Tiles Order updated!';
        }
        catch (\Throwable $e)
        {
            $this->rollBack();

            $response['success'] = false;
            $response['type'] = 'danger';
            $response['message'] = $e->getMessage();
        }

        return $response;
    }

    /**
     * @param array $tiles
     * @return string
     */
    private function createTilesTree(array $tiles) :string
    {
        $html = '';

        foreach ($tiles as $tile)
            $html .= '<li data-language="'.$tile->language_id().'" data-jstree=\'{"opened":false}\' id="'.$tile->id().'"><a href="#" >'.$tile->id().' - '.$tile->title().'</a></li>';

        return $html;
    }
}