<?php

namespace App\Models\Backend;

use App\Models\Model;
use App\Facades\Session;
use App\Entities\CacheEntity;
use App\Factories\CacheFactory;
use Illuminate\Pagination\LengthAwarePaginator;

class Cache extends Model
{
    /**
     * @var string
     */
    protected $table = 'cache';

    /**
     * @var string
     */
    protected $factory = CacheFactory::class;

    /**
     * @param array $filters
     * @param array $relations
     * @return CacheEntity
     */
    public static function find(array $filters, array $relations = []) :CacheEntity
    {
        return parent::find($filters, $relations);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function findById(int $id, array $relations = ['*']) :CacheEntity
    {
        return parent::findById($id, $relations);
    }

    /**
     * @param $filters
     * @param array $relations
     * @return CacheEntity
     */
    public static function findOrFail($filters, array $relations = []) :CacheEntity
    {
        return parent::findOrFail($filters, $relations);
    }

    /**
     * @param CacheEntity $cache
     * @return bool
     */
    public function insert(CacheEntity $cache) :bool
    {
        $this->table()->insert(
            [
                'encoded_key' => $cache->encoded_key(),
                'decoded_key' => $cache->decoded_key(),
                'created_at' => now()
            ]
        );

        return true;
    }

    /**
     * @param CacheEntity $cache
     * @return bool
     */
    public function delete(CacheEntity $cache) :bool
    {
        return $this->table()->where('id', $cache->id())->delete();
    }

    /**
     * @param array $filters
     * @return CacheEntity
     */
    public function getEntity(array $filters = []) :CacheEntity
    {
        $query = $this->table()->select(['*']);

        if (count($filters) > 0) {
            foreach ($filters as $key => $value) {
                $query->where($key, $value);
            }
        }

        return $query->withEntities()->first();
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getCacheList(): LengthAwarePaginator
    {
        $order = 'id' . " " . ((Session::get('order') == 'asc') ? 'ASC' : 'DESC');

        $query = $this->table()->select(['*'])
            ->orderByRaw($order);

        $query->when(Session::get(node()->name().'.filters.filter_decoded'), function ($q) {
            return $q->where('decoded_key', 'like', '%'.Session::get(node()->name().'.filters.filter_decoded').'%');
        });

        return $query->paginate();
    }
}