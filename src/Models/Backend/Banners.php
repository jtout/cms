<?php

namespace App\Models\Backend;

use App\Models\Model;
use App\Facades\Session;
use App\Entities\BannerEntity;
use App\Models\ModelQueryBuilder;
use App\Factories\BannerFactory;
use Illuminate\Pagination\LengthAwarePaginator;

class Banners extends Model
{
    /**
     * @var string
     */
    protected $table = 'banners';

    /**
     * @var string
     */
    protected $factory = BannerFactory::class;

    /**
     * @param array $filters
     * @param array $relations
     * @return BannerEntity
     */
    public static function find(array $filters, array $relations = []) :BannerEntity
    {
        return parent::find($filters, $relations);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function findById(int $id, array $relations = ['*']) :BannerEntity
    {
        return parent::findById($id, $relations);
    }

    /**
     * @param $filters
     * @param array $relations
     * @return BannerEntity
     */
    public static function findOrFail($filters, array $relations = []) :BannerEntity
    {
        return parent::findOrFail($filters, $relations);
    }

    /**
     * @param BannerEntity $banner
     * @return bool
     */
    public function insert(BannerEntity $banner) :bool
    {
        $id = $this->table()->insertGetId([
            'title' => $banner->title(),
            'alias' => $banner->alias(),
            'description' => $banner->description(),
            'position_id' => $banner->position_id(),
            'client_id' => $banner->client_id(),
            'click_url' => $banner->click_url(),
            'type' => $banner->type(),
            'field' => $banner->field(),
            'mobile_field' => $banner->mobile_field(),
            'settings' => $banner->settings(),
            'is_active' => $banner->is_active(),
            'ordering' => $banner->ordering(),
            'published_on' => $banner->published_on(),
            'finished_on' => $banner->finished_on(),
            'created_by' => $banner->created_by(),
            'created_on' => $banner->created_on(),
            'updated_by' => $banner->updated_by(),
            'updated_on' => $banner->updated_on(),
        ]);

        $banner->setId($id);

        $nodes = explode(',', $banner->nodes());
        foreach ($nodes as $node) {
            $this->table('banners_nodes')->insert([
                'banner_id' => $id,
                'node_id' => $node
            ]);
        }

        return true;
    }

    /**
     * @param BannerEntity $banner
     * @return bool
     */
    public function update(BannerEntity $banner) :bool
    {
        $this->table()->where('id', $banner->id())->update([
            'title' => $banner->title(),
            'alias' => $banner->alias(),
            'description' => $banner->description(),
            'position_id' => $banner->position_id(),
            'client_id' => $banner->client_id(),
            'click_url' => $banner->click_url(),
            'type' => $banner->type(),
            'field' => $banner->field(),
            'mobile_field' => $banner->mobile_field(),
            'settings' => $banner->settings(),
            'is_active' => $banner->is_active(),
            'ordering' => $banner->ordering(),
            'published_on' => $banner->published_on(),
            'finished_on' => $banner->finished_on(),
            'updated_by' => $banner->updated_by(),
            'updated_on' => $banner->updated_on(),
        ]);

        // Update banners_nodes table
        $this->table('banners_nodes')->where('banner_id', $banner->id())->delete();

        $nodes = explode(',', $banner->nodes());
        foreach ($nodes as $node) {
            $this->table('banners_nodes')->insert([
                'banner_id' => $banner->id(),
                'node_id' => $node
            ]);
        }

        return true;
    }

    /**
     * @param BannerEntity $banner
     * @return bool
     */
    public function delete(BannerEntity $banner) :bool
    {
        $this->table()->where('id', $banner->id())->delete();

        return true;
    }

    /**
     * @return ModelQueryBuilder
     */
    public static function getEntityQuery() :ModelQueryBuilder
    {
        return self::query('banners AS a')
            ->select(['a.*'])
            ->selectRaw('GROUP_CONCAT(b.node_id) AS nodes')
            ->join('banners_nodes AS b', 'b.banner_id', '=', 'a.id');
    }

    /**
     * @param array $filters
     * @return BannerEntity
     */
    public function getEntity(array $filters = []) :BannerEntity
    {
        $query = self::getEntityQuery();

        if(isset($filters['id'])) {
            $query->where('a.id', $filters['id']);
        }

        if(isset($filters['position_id'])) {
            $query->where('a.position_id', $filters['position_id']);
        }

        if(isset($filters['alias'])) {
            $query->where('a.alias', 'like', '%'.$filters['alias'].'%');
        }

        if(isset($filters['client_id'])) {
            $query->where('a.client_id', $filters['client_id']);
        }

        return $query->withEntities()->first();
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getBannersList() :LengthAwarePaginator
    {
        $order = 'a.id';

        $allowed_db = array(
            'banners_id' => 'a.id',
            'banners_title' => 'a.title',
            'banners_position' => 'c.title',
            'banners_clients' => 'b.title',
            'banners_active' => 'a.is_active',
            'banners_order' => 'a.ordering'
        );

        if (!empty(Session::get('sort'))) {
            $sort = 'a.id';

            if (isset($allowed_db[Session::get('sort')])) {
                $sort = $allowed_db[Session::get('sort')];
            }

            $order = $sort . " " . ((Session::get('order') == 'asc') ? 'ASC' : 'DESC');
        }

        $query = $this->table('banners AS a')
            ->select(['a.*', 'b.title AS client_title', 'c.title AS position_title'])
            ->join('clients AS b', 'b.id', '=', 'a.client_id')
            ->join('banners_positions AS c', 'c.id', '=', 'a.position_id')
            ->orderByRaw($order);

        $query->when(Session::get(node()->name().'.filters.filter_title'), function ($q) {
            return $q->where('a.title', 'like', '%'.Session::get(node()->name().'.filters.filter_title').'%');
        });

        $query->when(Session::get(node()->name().'.filters.filter_active'), function ($q) {
            return $q->where('a.is_active', Session::get(node()->name().'.filters.filter_active') == 2 ? 0 : 1);
        });

        $query->when(Session::get(node()->name().'.filters.filter_clients'), function ($q) {
            return $q->where('a.client_id', Session::get(node()->name().'.filters.filter_clients'));
        });

        $query->when(Session::get(node()->name().'.filters.filter_positions'), function ($q) {
            return $q->where('a.position_id', Session::get(node()->name().'.filters.filter_positions'));
        });

        return $query->paginate();
    }
}