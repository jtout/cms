<?php

namespace App\Models\Backend;

use App\Traits\App;
use App\Models\Model;
use App\Facades\Session;
use Illuminate\Support\Arr;
use App\Entities\NodeEntity;
use App\Factories\NodeFactory;
use App\Models\ModelQueryBuilder;
use App\Traits\ExtraFieldsHelper;
use Illuminate\Support\Collection;
use App\Traits\Nodes as NodesHelper;
use App\Observers\Cache as CacheObserver;
use Illuminate\Pagination\LengthAwarePaginator;

class Nodes extends Model
{
    use NodesHelper, ExtraFieldsHelper;

    /**
     * @var string
     */
    protected $table = 'nodes';

    /**
     * @var string
     */
    protected $factory = NodeFactory::class;

    /**
     * @var array
     */
    protected $relations = [
        'tags' => 'id',
        'translations' => 'id',
        'author' => 'created_by',
        'updated_by_author' => 'updated_by',
        'parent' => 'parent_id',
        'language' => 'language_id',
        'access_level' => 'access_level_id',
        'sponsored_by' => 'sponsored_by_id',
        'content_type' => 'content_type_id',
        'module' => 'module_id',
        'comments_count' => 'id'
    ];

    /**
     * @param array $filters
     * @param array $relations
     * @return NodeEntity
     */
    public static function find(array $filters, array $relations = []) :NodeEntity
    {
        return parent::find($filters, $relations);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function findById(int $id, array $relations = []) :NodeEntity
    {
        return parent::findById($id, $relations);
    }

    /**
     * @param $filters
     * @param array $relations
     * @return NodeEntity
     */
    public static function findOrFail($filters, array $relations = []) :NodeEntity
    {
        return parent::findOrFail($filters, $relations);
    }

    /**
     * @param Collection $records
     * @param array $relations
     * @return mixed
     */
    public function loadDataRelations(Collection $records, array $relations = [])
    {
        $relationsData = new Collection();

        if (in_array('translations', $relations)) {
            $relationsData->put('translations', $this->translations($records));
        }

        if (in_array('tags', $relations)) {
            $relationsData->put('tags', $this->tags($records));
        }

        if (in_array('author', $relations)) {
            $relationsData->put('author', $this->author($records));
        }

        if (in_array('updated_by_author', $relations)) {
            $relationsData->put('updated_by_author', $this->author($records, 'updated_by'));
        }

        if (in_array('parent', $relations)) {
            $relationsData->put('parent', $this->parent($records));
        }

        if (in_array('language', $relations)) {
            $relationsData->put('language', $this->language($records));
        }

        if (in_array('access_level', $relations)) {
            $relationsData->put('access_level', $this->access_level($records));
        }

        if (in_array('sponsored_by', $relations)) {
            $relationsData->put('sponsored_by', $this->sponsored_by($records));
        }

        if (in_array('content_type', $relations)) {
            $relationsData->put('content_type', $this->content_type($records));
        }

        if (in_array('module', $relations)) {
            $relationsData->put('module', $this->module($records));
        }

        if (in_array('comments_count', $relations)) {
            $relationsData->put('comments_count', $this->comments_count($records));
        }

        return $relationsData;
    }

    /**
     * @param Collection $records
     * @return Collection
     */
    public function comments_count(Collection $records) :Collection
    {
        $nodeIds = $records->pluck('id')->toArray();

        $count = Comments::query()
            ->select(['node_id AS id'])
            ->selectRaw('COUNT(id) AS count')
            ->whereIn('node_id', $nodeIds)
            ->where('is_active', 1)
            ->groupBy('node_id')
            ->get();

        foreach ($nodeIds as $nodeId) {
            if (!$count->where('id', $nodeId)->first()) {
                $count->put($nodeId, ['id' => $nodeId, 'count' => 0]);
            }
        }

        return $count->keyBy('id');
    }

    /**
     * @param Collection $records
     * @return Collection
     */
    public function sponsored_by(Collection $records) :Collection
    {
        $sponsoredBy = Clients::query()
            ->whereIn('id', $records->pluck('sponsored_by_id')->toArray())
            ->get()
            ->keyBy('id');

        return $sponsoredBy;
    }

    /**
     * @param Collection $records
     * @return Collection
     */
    public function content_type(Collection $records) :Collection
    {
        $contentTypes = ContentTypes::query()
            ->whereIn('id', $records->pluck('content_type_id')->toArray())
            ->get()
            ->keyBy('id');

        return $contentTypes;
    }

    /**
     * @param Collection $records
     * @return Collection
     */
    public function module(Collection $records) :Collection
    {
        $modules = Modules::query()
            ->whereIn('id', $records->pluck('module_id')->toArray())
            ->get()
            ->keyBy('id');

        return $modules;
    }

    /**
     * @param Collection $records
     * @return Collection
     */
    public function access_level(Collection $records) :Collection
    {
        $accessLevels = AccessLevels::query()
            ->whereIn('id', $records->pluck('access_level_id')->toArray())
            ->get()
            ->keyBy('id');

        return $accessLevels;
    }

    /**
     * @param Collection $records
     * @return Collection
     */
    public function language(Collection $records) :Collection
    {
        $languages = Languages::query()
            ->whereIn('id', $records->pluck('language_id')->toArray())
            ->get()
            ->keyBy('id');

        return $languages;
    }

    /**
     * @param Collection $records
     * @return Collection
     */
    public function parent(Collection $records) :Collection
    {
        $parents = Nodes::getEntityQuery()
            ->whereIn('a.id', $records->pluck('parent_id')->toArray())
            ->get()
            ->keyBy('id');

        return $parents;
    }

    /**
     * @param Collection $records
     * @param string $column
     * @return Collection
     */
    public function author(Collection $records, $column = 'created_by') :Collection
    {
        $authorsIds = $records->pluck($column)->toArray();
        $authors = Users::query()
            ->whereIn('id', $authorsIds)
            ->get()
            ->keyBy('id');

        return $authors;
    }

    /**
     * @param Collection $records
     * @return Collection
     */
    public function translations(Collection $records) :Collection
    {
        $dataToCollect = new Collection();
        $ids = $records->pluck('id')->toArray();
        $translations = $this->getTranslations($ids);

        if($translations->count() > 0) {
            foreach ($ids as $id) {
                $translationsEntities = $translations->where('node_id', $id)->transform(function ($translation){
                    return NodeFactory::build($translation);
                });

                $dataToCollect->put($id, $translationsEntities);
            }
        }

        return $dataToCollect;
    }

    /**
     * @param Collection $records
     * @return Collection
     */
    public function tags(Collection $records) :Collection
    {
        $dataToCollect = new Collection();
        $ids = $records->pluck('id')->toArray();
        $tags = $this->getTags($ids);

        if($tags->count() > 0) {
            foreach ($ids as $id) {
                $tagEntities = $tags->where('node_id', $id)->transform(function ($tag){
                    return NodeFactory::build($tag);
                });

                $dataToCollect->put($id, $tagEntities);
            }
        }

        return $dataToCollect;
    }

    /**
     * @return ModelQueryBuilder
     */
    public static function getEntityQuery() :ModelQueryBuilder
    {
        return self::query('nodes AS a')
            ->select([
                'a.*', 'b.depth', 'b.path', 'c.*', 'd.media_folder AS content_type_media_folder',
                'd.title AS content_type', 'e.title AS module_title', 'e.action AS module_action',
                'e.class AS module_class', 'e.request_type AS module_request_type', 'f.iso_code AS language_iso_code'
            ])
            ->join('nodes_closure AS b', 'b.descendant', '=', 'a.id')
            ->join('nodes_content AS c', 'c.node_id', '=', 'a.id')
            ->join('content_types AS d', 'd.id', '=', 'c.content_type_id')
            ->join('modules AS e', 'e.id', '=', 'a.module_id')
            ->join('languages AS f', 'f.id', '=', 'c.language_id')
            ->where([
                'b.ancestor' => 1,
                'e.is_function' => 1,
                'e.is_active' => 1
            ]);
    }

    /**
     * @param array $filters
     * @param array $relations
     * @return NodeEntity
     */
    public function getEntity(array $filters = [], array $relations = []) :NodeEntity
    {
        $query = self::getEntityQuery();

        if (isset($filters['id'])) {
            $query->whereIn('a.id', Arr::wrap($filters['id']));
        }

        if (isset($filters['name'])) {
            $query->where('a.name', $filters['name']);
        }

        if (isset($filters['type'])) {
            $query->where('c.content_type_id', $filters['type']);
        }

        if (isset($filters['path'])) {
            $query->where('b.path', App::convertEncoding($filters['path']));
        }

        if (isset($filters['is_active'])) {
            $query->where('a.is_active', $filters['is_active']);
        }

        if (isset($filters['language_id'])) {
            $query->where('c.language_id', $filters['language_id']);
        }

        return $query->withRelations($relations)->withEntities()->first() ?? new NodeEntity();
    }

    /**
     * @param string $action
     * @return NodeEntity
     */
    public function getNodeByAction(string $action) :NodeEntity
    {
        $query = $this->table('modules AS a')
            ->select([
                'b.id',
                'a.id AS module_id',
                'a.action AS module_action',
                'a.class AS module_class',
                'a.request_type AS module_request_type',
            ])
            ->leftJoin('nodes AS b', 'a.id', '=', 'b.module_id')
            ->where('a.action', $action);

        $row = $query->first();

        if(!empty($row->id)) {
            $node = $this->getEntity(['id' => $row->id, 'is_active' => 1]);
        }
        else if(!empty($row->module_id)) {
            $row->id = 0;
            $node = $this->entity($row);
        }
        else {
            $row->id = 0;
            $row->module_id = 0;
            $node = $this->entity($row);
        }

        return $node;
    }

    /**
     * @param bool $backend
     * @param int $type
     * @return array
     */
    public function getNodesList(bool $backend, int $type) :array
    {
        $nodes = $this->list($backend, $type);
        $languages = new Languages();
        $users = new Users();

        $data = array(
            'rows' => $nodes,
            'parents' => $this->getNodes($type = 2 , $backend = 0, $language = 0, $getHomepage = 0),
            'authors' => $users->getUsersByRole([1,2,3,4,5]),
            'max_level' => (int) $this->table('nodes_closure')->max('depth'),
            'languages' => $languages->getLanguages()
        );

        return $data;
    }

    /**
     * @param int $id
     * @param int $type
     * @param int $getHomepage
     * @return array
     */
    public function editNodeEntity(int $id, int $type, int $getHomepage) :array
    {
        $nodeEntity = self::findOrFail(['id' => $id, 'type' => $type], $this->relations);
        $data = array(
            'nodeEntity' => $nodeEntity,
            'parents' => $this->getNodes($type == 1 ? 2 : $nodeEntity->content_type_id(), $nodeEntity->in_backend(), $nodeEntity->language_id(), $getHomepage),
            'modules' => (new Modules)->getModules(['is_function' => 1, 'is_active' => 1]),
            'access_levels' => (new AccessLevels)->getAccessLevels()->toArray(),
            'authors' => (new Users)->getUsersByRole([1,2,3,4,5]),
            'languages' => (new Languages)->getLanguages(['is_active' => 1]),
            'views' => $this->getViews($nodeEntity, $nodeEntity->in_backend()),
            'extra_fields_groups' => (new ExtraFieldsGroups)->getExtraFieldsGroups(),
            'translations' => $this->getNodeTranslations($nodeEntity->id(), $nodeEntity->language_iso_code()),
            'extra_fields' => $this->renderExtraFields(
                (new ExtraFields)->getExtraFields([
                    'group_id' => $nodeEntity->extra_fields_group_id(),
                    'is_active' => 1
                ]),
                $nodeEntity->extra_fields_values()),
            'clients' => (new Clients)->getClients()
        );

        return $data;
    }

    /**
     * @param int $type
     * @param int $backend
     * @param NodeEntity $node
     * @param array $translation
     * @param int $getHomepage
     * @return array
     */
    public function addNodeEntity(int $type, int $backend, NodeEntity $node, array $translation = array(), $getHomepage = 0) :array
    {
        $languages = new Languages();
        $data = array(
            'modules' => (new Modules)->getModules(['is_function' => 1, 'is_active' => 1]),
            'access_levels' => (new AccessLevels)->getAccessLevels()->toArray(),
            'authors' => (new Users)->getUsersByRole([1,2,3,4,5]),
            'languages' => empty($translation['iso_code']) ? $languages->getLanguages(['is_active' => 1]) : $languages->getLanguages(['is_active' => 1, 'iso_code' => $translation['iso_code']]),
            'views' => $this->getViews($node, $backend),
            'extra_fields_groups' => (new ExtraFieldsGroups)->getExtraFieldsGroups(),
            'clients' => (new Clients)->getClients()
        );

        if(!empty($translation['iso_code'])) {
            $language = Languages::find(['iso_code' => $translation['iso_code']]);
            $data['parents'] = $this->getNodes($type, $backend, $language->id(), $getHomepage);
        }
        else {
            if($backend == 1) {
                $data['parents'] = $this->getNodes($type, $backend, $language = 1, $getHomepage);
            }
            else {
                $data['parents'] = $this->getNodes($type, $backend, $language = 0, $getHomepage);
            }
        }

        return $data;
    }

    /**
     * @param NodeEntity $node
     * @return bool
     */
    public function insert(NodeEntity $node) :bool
    {
        $id = $this->table()->insertGetId([
            'parent_id' => $node->parent_id(),
            'name' => $node->name(),
            'view' => $node->view(),
            'module_id' => $node->module_id(),
            'access_level_id' => $node->access_level_id(),
            'on_menu' => $node->on_menu(),
            'in_backend' => $node->in_backend(),
            'is_active' => $node->is_active(),
            'created_by' => $node->created_by(),
            'created_on' => $node->created_on(),
            'updated_by' => $node->updated_by(),
            'updated_on' => $node->updated_on()
        ]);

        $node->setId($id);

        $this->table('nodes_content')->insert([
            'node_id' => $node->id(),
            'title' => $node->title(),
            'intro_text' => $node->intro_text(),
            'description' => $node->description(),
            'image' => $node->image(),
            'gallery' => $node->gallery(),
            'video' => $node->video(),
            'icon' => $node->icon(),
            'language_id' => $node->language_id(),
            'content_type_id' => $node->content_type_id(),
            'is_featured' => $node->is_featured(),
            'is_sticky' => $node->is_sticky(),
            'social_media_title' => $node->social_media_title(),
            'meta_title' => $node->meta_title(),
            'meta_description' => $node->meta_description(),
            'robots' => $node->robots(),
            'published_on' => $node->published_on(),
            'finished_on' => $node->finished_on(),
            'homepage' => $node->homepage(),
            'extra_fields_group_id' => $node->extra_fields_group_id() == 0 ? null : $node->extra_fields_group_id(),
            'extra_fields_values' => $node->extra_fields_values(),
            'sponsored_by_id' => $node->sponsored_by_id() == 0 ? null : $node->sponsored_by_id(),
            'comments_lock' => $node->comments_lock()
        ]);

        // INSERT TRANSLATIONS
        if (!empty($node->from_id())) {
            $existingTranslations = $this->checkTranslations($node->from_id());

            if($existingTranslations['success'] === true) {
                $this->insertTranslations($node->from_id(), $node->id(), $existingTranslations['results']);
            }
        }

        //INSERT TAGS
        if (!empty($node->tags())) {
            $this->insertTags($node);
        }

        return true;
    }

    /**
     * @param NodeEntity $node
     * @return bool
     */
    public function update(NodeEntity $node) :bool
    {
        $this->table()->where('id', $node->id())->update([
            'parent_id' => $node->parent_id(),
            'name' => $node->name(),
            'view' => $node->view(),
            'module_id' => $node->module_id(),
            'access_level_id' => $node->access_level_id(),
            'on_menu' => $node->on_menu(),
            'in_backend' => $node->in_backend(),
            'is_active' => $node->is_active(),
            'order' => $node->order(),
            'created_by' => $node->created_by(),
            'created_on' => $node->created_on(),
            'updated_by' => $node->updated_by(),
            'updated_on' => $node->updated_on()
        ]);

        if ($node->homepage() == 1) {
            $this->table('nodes_content')
                ->where('language_id', $node->language_id())
                ->update(['homepage' => 0]);
        }

        $this->table('nodes_content')->where('node_id', $node->id())->update([
            'title' => $node->title(),
            'intro_text' => $node->intro_text(),
            'description' => $node->description(),
            'image' => $node->image(),
            'gallery' => $node->gallery(),
            'video' => $node->video(),
            'icon' => $node->icon(),
            'language_id' => $node->language_id(),
            'content_type_id' => $node->content_type_id(),
            'is_featured' => $node->is_featured(),
            'is_sticky' => $node->is_sticky(),
            'social_media_title' => $node->social_media_title(),
            'meta_title' => $node->meta_title(),
            'meta_description' => $node->meta_description(),
            'robots' => $node->robots(),
            'published_on' => $node->published_on(),
            'finished_on' => $node->finished_on(),
            'homepage' => $node->homepage(),
            'extra_fields_group_id' => $node->extra_fields_group_id() == 0 ? null : $node->extra_fields_group_id(),
            'extra_fields_values' => $node->extra_fields_values(),
            'sponsored_by_id' => $node->sponsored_by_id() == 0 ? null : $node->sponsored_by_id(),
            'comments_lock' => $node->comments_lock()
        ]);

        //INSERT TAGS
        if (!empty($node->tags())) {
            $this->insertTags($node);
        }

        return true;
    }

    /**
     * @param NodeEntity $node
     * @return bool
     */
    public function delete(NodeEntity $node) :bool
    {
        $this->table()->where('id', $node->id())->delete();

        return true;
    }

    /**
     * @param bool $backend
     * @param int $type
     * @return LengthAwarePaginator
     */
    public function list(bool $backend, int $type) :LengthAwarePaginator
    {
        $order = $type == 1 ? 'b.created_on DESC' : 'a.path';

        $allowed_db = array(
            'nodes_id' => 'b.id',
            'nodes_title' => 'c.title',
            'nodes_active' => 'b.is_active',
            'nodes_access_level' => 'd.title',
            'nodes_module' => 'e.title',
            'nodes_action' => 'e.action',
            'nodes_featured' => 'c.is_featured',
            'nodes_language' => 'f.title',
            'nodes_homepage' => 'c.homepage',
            'nodes_parent' => 'g.title',
            'nodes_modified_by' => 'k.name',
            'nodes_author' => 'j.name',
            'nodes_created' => 'b.created_on',
            'nodes_updated' => 'b.updated_on',
            'articles_count' => 'tags_articles_count'
        );

        if (!empty(Session::get('sort'))) {
            $sort = 'a.id';

            if (isset($allowed_db[Session::get('sort')])) {
                $sort = $allowed_db[Session::get('sort')];
            }

            $order = $sort . " " . ((Session::get('order') == 'asc') ? 'ASC' : 'DESC');
        }

        $query = $this->table('nodes_closure AS a')
            ->select([
                'b.id',
                'b.parent_id',
                'b.on_menu',
                'b.is_active',
                'b.access_level_id',
                'b.created_on',
                'b.updated_on',
                'b.created_by',
                'b.updated_by',
                'c.title',
                'c.is_featured',
                'c.language_id',
                'c.homepage',
                'c.published_on',
                'c.content_type_id',
                'a.path',
                'a.depth',
                'e.title AS module_title',
                'e.action AS module_action'
            ]);

        if($type == 4) {
            $query->selectRaw('COUNT(i.node_id) AS tags_articles_count');
        }

        $query->join('nodes AS b', 'b.id', '=', 'a.descendant')
            ->join('nodes_content AS c', 'c.node_id', '=', 'b.id')
            ->join('access_levels AS d', 'd.id', '=', 'b.access_level_id')
            ->join('modules AS e', 'e.id', '=', 'b.module_id')
            ->join('nodes_content AS g', 'g.node_id', '=', 'b.parent_id');


        if (Session::get('sort') == 'nodes_author' || Session::get('sort') == 'nodes_modified_by') {
            $query->join('users AS j', 'j.id', '=', 'b.created_by')
                ->join('users AS k', 'k.id', '=', 'b.updated_by');
        }

        if (Session::get(node()->name().'.filters.filter_on_tiles') == 1) {
            $query->join('tiles AS h', 'h.node_id', '=', 'b.id');
        }

        if($type == 4) {
            $query->leftJoin('nodes_tags AS i', 'i.tag_id', '=', 'b.id');
        }

        $query->where([
            'a.ancestor' => 1,
            'c.content_type_id' => $type,
            'b.in_backend' => (int)$backend
        ])
            ->groupBy('b.id')
            ->orderByRaw($order);

        $query->when(Session::get(node()->name().'.filters.filter_title'), function ($q) {
            return $q->where('c.title', 'like', '%'.Session::get(node()->name().'.filters.filter_title').'%')
                ->orWhere('b.id', 'like', '%'.Session::get(node()->name().'.filters.filter_title').'%');
        });

        $query->when(Session::get(node()->name().'.filters.filter_level'), function ($q) {
            return $q->where('a.depth', '<=', Session::get(node()->name().'.filters.filter_level'));
        });

        $query->when(Session::get(node()->name().'.filters.filter_language'), function ($q) {
            return $q->where('c.language_id', Session::get(node()->name().'.filters.filter_language'));
        });

        $query->when(Session::get(node()->name().'.filters.filter_parent'), function ($q) {
            return $q->where('g.node_id', Session::get(node()->name().'.filters.filter_parent'));
        });

        $query->when(Session::get(node()->name().'.filters.filter_author'), function ($q) {
            return $q->where('b.created_by', Session::get(node()->name().'.filters.filter_author'));
        });

        $query->when(Session::get(node()->name().'.filters.filter_featured'), function ($q) {
            $featured = Session::get(node()->name().'.filters.filter_featured');
            return $q->where('c.is_featured', $featured == 2 ? 0 : $featured);
        });

        $query->when(Session::get(node()->name().'.filters.filter_active'), function ($q) {
            $active = Session::get(node()->name().'.filters.filter_active');
            return $q->where('b.is_active', $active == 2 ? 0 : $active);
        });

        return $query->paginate();
    }

    /**
     * @param int $type
     * @param int $backend
     * @param int $languageId
     * @param int $getHomepage
     * @param string $term
     * @return Collection
     */
    public function getNodes($type, int $backend, $languageId = null, int $getHomepage = 0, string $term = '') :Collection
    {
        if (empty($languageId)) {
            $languageId = Languages::find(['iso_code' => LANGUAGE])->id();
        }

        $query = $this->table('nodes_closure AS a')
            ->select([
                'b.id',
                'a.depth',
                'c.homepage',
                'b.view'
            ])
            ->selectRaw("(CASE WHEN c.title = 'Home' THEN 'Parent' ELSE c.title END) AS title")
            ->join('nodes AS b', 'b.id', '=', 'a.descendant')
            ->join('nodes_content AS c', 'c.node_id', '=', 'b.id')
            ->where([
                'a.ancestor' => 1,
                'c.language_id' => $languageId,
                'b.is_active' => 1,
                'b.in_backend' => $backend
            ])
            ->whereIn('c.content_type_id', Arr::wrap($type));

        if ($getHomepage == 1) {
            $query->orWhereRaw('(b.in_backend = ? AND c.homepage = 1 AND c.language_id = ?)', [$backend, $languageId]);
        }

        if (!empty($term)) {
            $query->where('c.title', 'like', '%'.$term.'%');
        }

        $query->groupBy(['b.id'])->orderBy('a.path');

        return $query->withEntities()->get();
    }

    /**
     * @param NodeEntity $node
     */
    public function insertTags(NodeEntity $node)
    {
        try
        {
            $this->beginTransaction();

            // Delete node tags
            $this->table('nodes_tags')->where('node_id', $node->id())->delete();

            // Insert node tags
            foreach($node->tags() as $tag) {
                if(!is_numeric($tag) || empty($this->getEntity(['id' => $tag])->id())) {
                    $parent = $this->getLanguageParent($node->language_id());
                    $tag = strtolower($tag);

                    $tagEntity = NodeFactory::build([], 'Tag');
                    $tagEntity->setParentId($parent->id());
                    $tagEntity->setName(App::makeSefUrl(App::sanitizeInput($tag, 'STRING')));
                    $tagEntity->setTitle(App::sanitizeInput($tag, 'STRING'));
                    $tagEntity->setDescription(App::sanitizeInput($tag, 'STRING'));
                    $tagEntity->setIsActive(1);
                    $tagEntity->setCreatedOn(now());
                    $tagEntity->setUpdatedOn(now());
                    $tagEntity->setCreatedBy(user()->id());
                    $tagEntity->setUpdatedBy(user()->id());
                    $tagEntity->setPublishedOn(now());
                    $tagEntity->setSocialMediaTitle(App::sanitizeInput($tag, 'STRING'));
                    $tagEntity->setMetaTitle(App::sanitizeInput($tag, 'STRING'));
                    $tagEntity->setMetaDescription(App::sanitizeInput($tag, 'STRING'));
                    $tagEntity->setRobots('INDEX, FOLLOW');
                    $tagEntity->setView('Frontend/Tags/default.view');
                    $tagEntity->setLanguageId($node->language_id());

                    $tagEntity->insert();
                    $tag = $tagEntity->id();
                }

                $this->table('nodes_tags')->insert(['node_id' => $node->id(), 'tag_id' => $tag]);
            }

            $this->commit();
        }
        catch (\Throwable $e)
        {
            $this->rollBack();
            flash_message('danger', $e->getMessage());
        }
    }

    /**
     * @param int|array $id
     * @return Collection
     */
    public function getTranslations($id = null) :Collection
    {
        $query = $this->table('nodes_translations AS a')->select([
            'a.node_id',
            'e.iso_code AS language_iso_code',
            'b.title',
            'c.path'
        ])
            ->join('nodes_content AS b', 'b.node_id', '=', 'a.translation_id')
            ->join('nodes_closure AS c', 'c.descendant', '=', 'b.node_id')
            ->join('nodes AS d', 'd.id', '=', 'b.node_id')
            ->join('languages AS e', 'e.id', '=', 'b.language_id')
            ->where([
                'c.ancestor' => 1,
                'd.is_active' => 1
            ])
            ->whereIn('a.node_id', Arr::wrap($id))
            ->whereIn('d.access_level_id', explode(',', user()->access_level()));

        return $query->get();
    }

    /**
     * @param int $nodeId
     * @param int $translation
     * @return bool
     */
    public function deleteNodeTranslation(int $nodeId, int $translation) :bool
    {
        return $this->table('nodes_translations')
            ->where(['node_id' => $nodeId, 'translation_id' => $translation])
            ->orWhere(['node_id' => $translation, 'translation_id' => $nodeId])
            ->delete();
    }

    /**
     * @param $id
     * @return Collection
     */
    public function getTags($id = null) :Collection
    {
        return $this->table('nodes_tags AS a')
            ->select(['a.tag_id', 'a.node_id', 'b.id', 'b.access_level_id', 'd.title', 'd.published_on', 'c.path'])
            ->join('nodes AS b', 'b.id', '=', 'a.tag_id')
            ->join('nodes_closure AS c', 'c.descendant', '=', 'b.id')
            ->join('nodes_content AS d', 'd.node_id', '=', 'b.id')
            ->whereIn('a.node_id', Arr::wrap($id))
            ->where([
                'b.is_active' => 1,
                'c.ancestor' => 1,
            ])
            ->orderByRaw('a.id')
            ->groupBy(['a.tag_id'])
            ->get();
    }

    /**
     * @param int $parentId
     * @param bool $backend
     * @return array
     */
    public function getNodesOnMenu(int $parentId = 7, bool $backend = true) :array
    {
        $nodes = array();
        $results = array();

        $query = $this->table('nodes_closure')->select([
            'nodes.id',
            'nodes.in_backend',
            'modules.action AS module_action',
            'nodes_content.title',
            'nodes_content.icon',
        ]);

        $queryPath = $this->table('nodes_closure')
            ->select('path')
            ->where('ancestor', 1)
            ->whereColumn('descendant', 'nodes.id');

        $query->selectSub($queryPath, 'path')
            ->join('nodes', 'nodes.id', '=', 'nodes_closure.ancestor')
            ->join('nodes_content', 'nodes_content.node_id', '=', 'nodes.id')
            ->join('modules', 'modules.id', '=', 'nodes.module_id')
            ->where([
                'nodes.on_menu' => 1,
                'nodes.is_active' => 1,
                'nodes.in_backend' => (int)$backend,
                'nodes.parent_id' => $parentId,
            ])
            ->where('nodes.id', '<>', 1)
            ->whereIn('nodes.access_level_id', explode(',', user()->access_level()))
            ->groupBy('nodes.id')
            ->orderByRaw('nodes_closure.depth, nodes.order ASC');

        $rows = $query->get();

        foreach ($rows as $row) {
            $results[$row->id] = $row;
            $results[$row->id]->childs = $this->getNodesOnMenu($row->id, $row->in_backend);
            $nodes[] = $this->entity($results[$row->id]);
        }

        return $nodes;
    }

    /**
     * @param array $filters
     * @return Collection
     */
    public function getBreadcrumbs(array $filters) :Collection
    {
        $query = $this->table('nodes_closure')
            ->select(
                [
                    'nodes.id',
                    'nodes.parent_id',
                    'nodes_content.title',
                    'nodes_content.icon',
                    'nodes_content.content_type_id',
                    'nodes_content.homepage',
                    'nodes_closure.depth'
                ]);

        $subQuery = $this->table('nodes_closure')
            ->select(['path'])
            ->where('ancestor', 1)
            ->whereColumn('descendant', '=', 'nodes.id');

        $query->selectSub($subQuery, 'path')
            ->join('nodes', 'nodes.id', '=', 'nodes_closure.ancestor')
            ->join('nodes_content', 'nodes_content.node_id', '=', 'nodes.id')
            ->where([
                'nodes_closure.descendant' => Arr::get($filters, 'id'),
            ])
            ->where('nodes.id', '<>', 1)
            ->orderBy('nodes_closure.depth', 'desc');

        if (Arr::has($filters, ['active'])) {
            $query->where('nodes.is_active', Arr::get($filters, 'active'));
        }

        return $query->withEntities()->get();
    }

    /**
     * @param NodeEntity $node
     * @return array
     */
    public function getChilds(NodeEntity $node) :array
    {
        $backend = $node->in_backend() == 1 ? true : false;
        $data = array('nodes' => $this->getNodesOnMenu($node->id(), $backend));

        return $data;
    }

    /**
     * @param int $language
     * @return NodeEntity
     */
    public function getLanguageParent(int $language) :NodeEntity
    {
        $query = $this->table('nodes_content')->select(['node_id AS id'])
            ->where([
                'language_id' => $language,
                'homepage' => 1
            ]);

        return $query->withEntities()->first();
    }

    /**
     * @param int $tag
     * @return int
     */
    public function getTagsArticlesCount(int $tag) :int
    {
        $count = $this->table('nodes_tags')->selectRaw('COUNT(node_id) AS count')
            ->where('tag_id', $tag)
            ->groupBy(['tag_id'])
            ->pluck('count')->first();

        return is_null($count) ? 0 : $count;
    }

    /**
     * @param string $selectedTags
     * @param int $mergeInto
     * @return array
     */
    public function mergeTags(string $selectedTags, int $mergeInto) :array
    {
        try
        {
            $selectedTagsArray = explode(',', $selectedTags);

            $response = array(
                'success' => true,
                'message' => 'Tags merged!',
                'type' 	  => 'success'
            );

            $this->beginTransaction();

            // Get articles based on selected tags
            $query = $this->table('nodes_tags AS a')->select([
                    'a.node_id AS id',
                    'b.path',
                    'c.content_type_id',
                    'd.parent_id'
                ])
                ->join('nodes_closure AS b', 'b.descendant', '=', 'a.node_id')
                ->join('nodes_content AS c', 'c.node_id', '=', 'a.node_id')
                ->join('nodes AS d', 'd.id', '=', 'a.node_id')
                ->whereIn('a.tag_id', $selectedTagsArray)
                ->where('a.tag_id', '<>', $mergeInto)
                ->where('b.ancestor', 1);

            $rows = $query->get();
            $articles = $this->entity($rows);

            // Get selected tags nodes
            $query = $this->table('nodes AS a')->select([
                    'a.id',
                    'a.parent_id',
                    'b.path',
                    'c.content_type_id'
                ])
                ->join('nodes_closure AS b', 'b.descendant', '=', 'a.id')
                ->join('nodes_content AS c', 'c.node_id', '=', 'a.id')
                ->whereIn('a.id', $selectedTagsArray)
                ->where('b.ancestor', 1);

            $selectedTagsNodes = $query->withEntities()->get();

            // Delete cache and then delete selected tags nodes except the one we are going to merge
            foreach ($selectedTagsNodes as $selectedTagsNode) {
                // Notify Observer
                CacheObserver::observe($selectedTagsNode);
                $selectedTagsNode->fireObserverEvent('delete');
            }

            $this->table('nodes')->whereIn('id', $selectedTagsArray)
                ->where('id', '<>', $mergeInto)->delete();

            // Connect articles to the tag and delete cache
            foreach ($articles as $article) {
                $this->table('nodes_tags')->updateOrInsert([
                    'node_id' => $article->id(),
                    'tag_id' => $mergeInto
                ]);

                // Notify Observer
                CacheObserver::observe($article);
                $article->fireObserverEvent('delete');
            }

            $this->commit();
        }
        catch (\Throwable $e)
        {
            $this->rollBack();
            $response = array(
                'success' => false,
                'message' => $e->getMessage(),
                'type' 	  => 'danger'
            );
        }

        return $response;
    }

    /**
     * @param int $id
     * @param string $isoCode
     * @return array
     */
    private function getNodeTranslations(int $id, string $isoCode) :array
    {
        $results = $this->table('nodes_translations AS a')->select(['a.translation_id', 'c.iso_code', 'b.title'])
            ->join('nodes_content AS b', 'b.node_id', '=', 'a.translation_id')
            ->join('languages AS c', 'c.id', '=', 'b.language_id')
            ->where('a.node_id', $id)->get();

        $translations = array();
        if (!$results->isEmpty()) {
            $translations = LANGUAGES;
            foreach ($results as $result) {
                if($isoCode != $result->iso_code) {
                    $translations[$result->iso_code] = $result;
                }
            }
        }

        return $translations;
    }

    /**
     * @param int $fromId
     * @return array
     */
    private function checkTranslations(int $fromId) :array
    {
        $response['success'] = true;
        $response['results'] = $this->table('nodes_translations')
            ->where('node_id', $fromId)->get()->toArray();

        if ((count((array)$response['results']) >= (count(LANGUAGES) - 1))) {
            $response['success'] = false;
        }

        return $response;
    }

    /**
     * @param int $fromId
     * @param int $newTranslationId
     * @param array $translations
     */
    private function insertTranslations(int $fromId, int $newTranslationId, array $translations)
    {
        try
        {
            $this->beginTransaction();

            // Insert new node to node translations table
            $this->table('nodes_translations')->insert([
                'node_id' => $fromId, 'translation_id' => $newTranslationId
            ]);

            $this->table('nodes_translations')->insert([
                'node_id' => $newTranslationId, 'translation_id' => $fromId
            ]);

            // Insert the other translations for new node
            if (!empty($translations)) {
                foreach ($translations as $translation) {
                    $this->table('nodes_translations')->insert([
                        'node_id' => $newTranslationId, 'translation_id' => $translation->translation_id
                    ]);

                    $this->table('nodes_translations')->insert([
                        'node_id' => $translation->translation_id, 'translation_id' => $newTranslationId
                    ]);
                }
            }

            $this->commit();
        }
        catch (\Throwable $e)
        {
            $this->rollBack();
        }
    }
}