<?php

namespace App\Models\Backend;

use App\Models\Model;
use App\Facades\Session;
use App\Entities\ClientEntity;
use App\Factories\ClientFactory;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class Clients extends Model
{
    /**
     * @var string
     */
    protected $table = 'clients';

    /**
     * @var string
     */
    protected $factory = ClientFactory::class;

    /**
     * @param array $filters
     * @param array $relations
     * @return ClientEntity
     */
    public static function find(array $filters, array $relations = []) :ClientEntity
    {
        return parent::find($filters, $relations);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function findById(int $id, array $relations = ['*']) :ClientEntity
    {
        return parent::findById($id, $relations);
    }

    /**
     * @param $filters
     * @param array $relations
     * @return ClientEntity
     */
    public static function findOrFail($filters, array $relations = []) :ClientEntity
    {
        return parent::findOrFail($filters, $relations);
    }

    /**
     * @param array $filters
     * @return ClientEntity
     */
    public function getEntity(array $filters = []) :ClientEntity
    {
        $query = $this->table();

        if (isset($filters['id'])) {
            $query->where('id', $filters['id']);
        }

        return $query->withEntities()->first();
    }

    /**
     * @return Collection
     */
    public function getClients() :Collection
    {
        return $this->table()->select(['*'])->withEntities()->get();
    }

    /**
     * @param ClientEntity $client
     * @return bool
     */
    public function insert(ClientEntity $client) :bool
    {
        $id = $this->table()->insertGetId([
            'title' => $client->title(),
            'description' => $client->description(),
            'created_by' => $client->created_by(),
            'created_on' => $client->created_on(),
            'updated_by' => $client->updated_by(),
            'updated_on' => $client->updated_on(),
        ]);

        $client->setId($id);

        return true;
    }

    /**
     * @param ClientEntity $client
     * @return bool
     */
    public function update(ClientEntity $client) :bool
    {
        $this->table()->where('id', $client->id())->update([
            'title' => $client->title(),
            'description' => $client->description(),
            'updated_by' => $client->updated_by(),
            'updated_on' => $client->updated_on(),
        ]);

        return true;
    }

    /**
     * @param ClientEntity $client
     * @return bool
     */
    public function delete(ClientEntity $client) :bool
    {
        $this->table()->where('id', $client->id())->delete();

        return true;
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getClientsList() :LengthAwarePaginator
    {
        $order = 'a.id';

        $allowed_db = array(
            'clients_id' => 'a.id',
            'clients_title' => 'a.title',
            'clients_email' => 'a.email',
            'clients_phone' => 'a.phone'
        );

        if (!empty(Session::get('sort'))) {
            $sort = 'a.id';

            if (isset($allowed_db[Session::get('sort')])) {
                $sort = $allowed_db[Session::get('sort')];
            }

            $order = $sort . " " . ((Session::get('order') == 'asc') ? 'ASC' : 'DESC');
        }

        $query = $this->table('clients AS a')
            ->select(['a.*'])
            ->orderByRaw($order);

        $query->when(Session::get(node()->name().'.filters.filter_title'), function ($q) {
            return $q->where('a.title', 'like', '%'.Session::get(node()->name().'.filters.filter_title').'%');
        });

        return $query->paginate();
    }
}