<?php

namespace App\Models\Backend;

use App\Models\Model;
use App\Facades\Session;
use Illuminate\Support\Collection;
use App\Entities\AccessLevelEntity;
use App\Factories\AccessLevelFactory;
use Illuminate\Pagination\LengthAwarePaginator;

class AccessLevels extends Model
{
    /**
     * @var string
     */
    protected $table = 'access_levels';

    /**
     * @var string
     */
    protected $factory = AccessLevelFactory::class;

    /**
     * @param array $filters
     * @param array $relations
     * @return AccessLevelEntity
     */
    public static function find(array $filters, array $relations = []) :AccessLevelEntity
    {
        return parent::find($filters, $relations);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function findById(int $id, array $relations = ['*']) :AccessLevelEntity
    {
        return parent::findById($id, $relations);
    }

    /**
     * @param $filters
     * @param array $relations
     * @return AccessLevelEntity
     */
    public static function findOrFail($filters, array $relations = []) :AccessLevelEntity
    {
        return parent::findOrFail($filters, $relations);
    }

    /**
     * @param AccessLevelEntity $accessLevel
     * @return bool
     */
    public function update(AccessLevelEntity $accessLevel)
    {
        $this->table()->where('id', $accessLevel->id())->update([
            'parent_id' => $accessLevel->parent_id() == 0 ? null : $accessLevel->parent_id(),
            'title' => $accessLevel->title(),
            'label' => $accessLevel->label(),
            'updated_by' => $accessLevel->updated_by(),
            'updated_on' => $accessLevel->updated_on()
        ]);

        return true;
    }

    /**
     * @param AccessLevelEntity $accessLevel
     * @return bool
     */
    public function insert(AccessLevelEntity $accessLevel)
    {
        $id = $this->table()->insertGetId([
            'parent_id' => $accessLevel->parent_id(),
            'title' => $accessLevel->title(),
            'label' => $accessLevel->label(),
            'created_by' => $accessLevel->created_by(),
            'created_on' => $accessLevel->created_on(),
            'updated_by' => $accessLevel->updated_by(),
            'updated_on' => $accessLevel->updated_on(),
        ]);

        $accessLevel->setId($id);

        return true;
    }

    /**
     * @param array $filters
     * @return AccessLevelEntity
     */
    public function getEntity(array $filters = []) :AccessLevelEntity
    {
        $query = $this->table()->select(['*']);

        if(isset($filters['id'])) {
            $query->find($filters['id']);
        }

        return $query->withEntities()->first();
    }

    /**
     * @param AccessLevelEntity $accessLevel
     * @return bool
     */
    public function delete(AccessLevelEntity $accessLevel)
    {
        $this->table()->where('id',$accessLevel->id())
            ->where('id', '<>', 1)
            ->delete();

        return true;
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getAccessLevelsList() :LengthAwarePaginator
    {
        $order = 'b.path';

        $allowed_db = array(
            'level_id' => 'a.id',
            'level_title' => 'a.title'
        );

        if (!empty(Session::get('sort'))) {
            $sort = 'b.path';

            if (isset($allowed_db[Session::get('sort')])) {
                $sort = $allowed_db[Session::get('sort')];
            }

            $order = $sort . " " . ((Session::get('order') == 'asc') ? 'ASC' : 'DESC');
        }

        $query = $this->table('access_levels AS a')
            ->select(['a.*', 'b.depth'])
            ->join('access_levels_tree AS b', 'b.child_id', '=', 'a.id')
            ->where('b.parent_id', 1)
            ->orderByRaw($order);

        $query->when(Session::get(node()->name().'.filters.filter_title'), function ($q) {
            return $q->where('a.title', 'like', '%'.Session::get(node()->name().'.filters.filter_title').'%');
        });

        return $query->paginate();
    }

    /**
     * @return Collection
     */
    public function getAccessLevels() :Collection
    {
        $query = $this->table('access_levels AS a')
            ->select(['a.*', 'b.*'])
            ->join('access_levels_tree AS b', 'b.child_id', '=', 'a.id')
            ->where('b.parent_id', 1)
            ->orderBy('b.path', 'asc');

        return $query->withEntities()->get();
    }

    /**
     * @param $accessLevel
     * @return string
     */
    public function getAuthorizedAccessLevels($accessLevel) :string
    {
        $query = $this->table('access_levels_tree AS a')
            ->selectRaw('GROUP_CONCAT(b.id) AS access_levels')
            ->join('access_levels AS b', 'a.child_id', '=', 'b.id')
            ->where('a.parent_id', $accessLevel);

        $accessLevels = $query->first()->access_levels;

        if(is_null($accessLevels)) {
            $accessLevels = '';
        }

        return $accessLevels;
    }
}