<?php

namespace App\Models\Backend;

use App\Models\Model;
use App\Facades\Session;
use Illuminate\Support\Arr;
use App\Entities\LanguageEntity;
use App\Factories\LanguageFactory;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class Languages extends Model
{
    /**
     * @var string
     */
    protected $table = 'languages';

    /**
     * @var string
     */
    protected $factory = LanguageFactory::class;

    /**
     * @param array $filters
     * @param array $relations
     * @return LanguageEntity
     */
    public static function find(array $filters, array $relations = []) :LanguageEntity
    {
        return parent::find($filters, $relations);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function findById(int $id, array $relations = ['*']) :LanguageEntity
    {
        return parent::findById($id, $relations);
    }

    /**
     * @param $filters
     * @param array $relations
     * @return LanguageEntity
     */
    public static function findOrFail($filters, array $relations = []) :LanguageEntity
    {
        return parent::findOrFail($filters, $relations);
    }

    /**
     * @param LanguageEntity $language
     * @return bool
     */
    public function update(LanguageEntity $language) :bool
    {
        $this->table()->where('id', $language->id())->update([
            'title' => $language->title(),
            'iso_code' => $language->iso_code(),
            'is_active' => $language->active(),
            'updated_by' => $language->updated_by(),
            'updated_on' => $language->updated_on()
        ]);

        return true;
    }

    /**
     * @param LanguageEntity $language
     * @return bool
     */
    public function insert(LanguageEntity $language) :bool
    {
        $id = $this->table()->insertGetId([
            'title' => $language->title(),
            'iso_code' => $language->iso_code(),
            'is_active' => $language->active(),
            'created_by' => $language->created_by(),
            'created_on' => $language->created_on(),
            'updated_by' => $language->updated_by(),
            'updated_on' => $language->updated_on(),
        ]);

        $language->setId($id);

        return true;
    }

    /**
     * @param LanguageEntity $language
     * @return bool
     */
    public function delete(LanguageEntity $language) :bool
    {
        $this->table()->where('id', $language->id())->delete();

        return true;
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getLanguagesList() :LengthAwarePaginator
    {
        $order = 'a.id';
        $allowed_db = array(
            'language_id' => 'a.id',
            'language_title' => 'a.title',
            'language_published' => 'a.is_active'
        );

        if (!empty(Session::get('sort'))) {
            $sort = 'a.id';

            if (isset($allowed_db[Session::get('sort')])) {
                $sort = $allowed_db[Session::get('sort')];
            }

            $order = $sort . " " . ((Session::get('order') == 'asc') ? 'ASC' : 'DESC');
        }


        $query = $this->table('languages AS a')->select(['a.*'])->orderByRaw($order);

        $query->when(Session::get(node()->name().'.filters.filter_title'), function ($q) {
            return $q->where('a.title', 'like', '%'.Session::get(node()->name().'.filters.filter_title').'%');
        });

        $query->when(Session::get(node()->name().'.filters.filter_active'), function ($q) {
            $active = Session::get(node()->name().'.filters.filter_active');
            return $q->where('a.is_active', $active == 2 ? 0 : $active);
        });

        return $query->paginate();
    }

    /**
     * @param array $filters
     * @return Collection
     */
    public function getLanguages($filters = array()) :Collection
    {
        $query = $this->table()
            ->select(['id', 'title', 'iso_code', 'is_active']);

        $query->when(Arr::has($filters, 'is_active'), function($q) use ($filters) {
            $q->where('is_active', $filters['is_active']);
        });

        $query->when(Arr::has($filters, 'iso_code'), function($q) use ($filters) {
            $q->where('iso_code', $filters['iso_code']);
        });

        $query->orderByRaw('iso_code = "'.LANGUAGE.'" DESC', 'id ASC');

        return $query->withEntities()->get();
    }

    /**
     * @param array $filters
     * @return LanguageEntity
     */
    public function getEntity(array $filters = []) :LanguageEntity
    {
        $query = $this->table()
            ->select([
                'id',
                'title',
                'iso_code',
                'is_active'
            ]);

        $query->when(Arr::has($filters, 'id'), function($q) use ($filters) {
            $q->where('id', $filters['id']);
        });

        $query->when(Arr::has($filters, 'iso_code'), function($q) use ($filters) {
            $q->where('iso_code', $filters['iso_code']);
        });

        return $query->withEntities()->first();
    }
}