<?php

namespace App\Models\Backend;

use App\Models\Model;
use App\Facades\Session;
use App\Entities\ContentTypeEntity;
use App\Factories\ContentTypeFactory;
use Illuminate\Pagination\LengthAwarePaginator;

class ContentTypes extends Model
{
    /**
     * @var string
     */
    protected $table = 'content_types';

    /**
     * @var string
     */
    protected $factory = ContentTypeFactory::class;

    /**
     * @param array $filters
     * @param array $relations
     * @return ContentTypeEntity
     */
    public static function find(array $filters, array $relations = []) :ContentTypeEntity
    {
        return parent::find($filters, $relations);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function findById(int $id, array $relations = ['*']) :ContentTypeEntity
    {
        return parent::findById($id, $relations);
    }

    /**
     * @param $filters
     * @param array $relations
     * @return ContentTypeEntity
     */
    public static function findOrFail($filters, array $relations = []) :ContentTypeEntity
    {
        return parent::findOrFail($filters, $relations);
    }

    /**
     * @param ContentTypeEntity $contentType
     * @return bool
     */
    public function update(ContentTypeEntity $contentType) :bool
    {
       $this->table()->where('id', $contentType->id())->update([
            'title' => $contentType->title(),
            'updated_by' => $contentType->updated_by(),
            'updated_on' => $contentType->updated_on()
        ]);

       return true;
    }

    /**
     * @param ContentTypeEntity $contentType
     * @return bool
     */
    public function insert(ContentTypeEntity $contentType) :bool
    {
        $id = $this->table()->insertGetId([
            'title' => $contentType->title(),
            'created_by' => $contentType->created_by(),
            'updated_by' => $contentType->updated_by(),
            'created_on' => $contentType->created_on(),
            'updated_on' => $contentType->updated_on()
        ]);

        $contentType->setId($id);

        return true;
    }

    /**
     * @param ContentTypeEntity $contentType
     * @return bool
     */
    public function delete(ContentTypeEntity $contentType) :bool
    {
        $this->table()->where('id', $contentType->id())->delete();

        return true;
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getContentTypesList() :LengthAwarePaginator
    {
        $order = 'a.id';

        $allowed_db = array(
            'content_id' => 'a.id',
            'content_title' => 'a.title'
        );

        if (!empty(Session::get('sort'))) {
            $sort = 'a.id';

            if (isset($allowed_db[Session::get('sort')])) {
                $sort = $allowed_db[Session::get('sort')];
            }

            $order = $sort . " " . ((Session::get('order') == 'asc') ? 'ASC' : 'DESC');
        }

        $query = $this->table('content_types AS a')->select(['*'])->orderByRaw($order);

        $query->when(Session::get(node()->name().'.filters.filter_title'), function ($q) {
            return $q->where('title', 'like', Session::get(node()->name().'.filters.filter_title'));
        });

        return $query->paginate();
    }

    /**
     * @param array $filters
     * @return ContentTypeEntity
     */
    public function getEntity(array $filters = []) :ContentTypeEntity
    {
        $query = $this->table();

        if (isset($filters['id'])) {
            $query->where('id', $filters['id']);
        }

        return $query->withEntities()->first();
    }
}