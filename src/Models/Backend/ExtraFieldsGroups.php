<?php

namespace App\Models\Backend;

use App\Models\Model;
use App\Facades\Session;
use Illuminate\Support\Collection;
use App\Entities\ExtraFieldGroupEntity;
use App\Factories\ExtraFieldGroupFactory;
use Illuminate\Pagination\LengthAwarePaginator;

class ExtraFieldsGroups extends Model
{
    /**
     * @var string
     */
    protected $table = 'extra_fields_groups';

    /**
     * @var string
     */
    protected $factory = ExtraFieldGroupFactory::class;

    /**
     * @param array $filters
     * @param array $relations
     * @return ExtraFieldGroupEntity
     */
    public static function find(array $filters, array $relations = []) :ExtraFieldGroupEntity
    {
        return parent::find($filters, $relations);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function findById(int $id, array $relations = ['*']) :ExtraFieldGroupEntity
    {
        return parent::findById($id, $relations);
    }

    /**
     * @param $filters
     * @param array $relations
     * @return ExtraFieldGroupEntity
     */
    public static function findOrFail($filters, array $relations = []) :ExtraFieldGroupEntity
    {
        return parent::findOrFail($filters, $relations);
    }

    /**
     * @param ExtraFieldGroupEntity $extraFieldsGroup
     * @return bool
     */
    public function update(ExtraFieldGroupEntity $extraFieldsGroup) :bool
    {
        $this->table()->where('id', $extraFieldsGroup->id())->update([
            'title' => $extraFieldsGroup->title(),
            'updated_by' => $extraFieldsGroup->updated_by(),
            'updated_on' => $extraFieldsGroup->updated_on()
        ]);

        return true;
    }

    /**
     * @param ExtraFieldGroupEntity $extraFieldsGroup
     * @return bool
     */
    public function insert(ExtraFieldGroupEntity $extraFieldsGroup) :bool
    {
        $id = $this->table()->insertGetId([
            'title' => $extraFieldsGroup->title(),
            'created_by' => $extraFieldsGroup->created_by(),
            'created_on' => $extraFieldsGroup->created_on(),
            'updated_by' => $extraFieldsGroup->updated_by(),
            'updated_on' => $extraFieldsGroup->updated_on()
        ]);

        $extraFieldsGroup->setId($id);

        return true;
    }

    /**
     * @param ExtraFieldGroupEntity $extraFieldGroup
     * @return bool
     */
    public function delete(ExtraFieldGroupEntity $extraFieldGroup) :bool
    {
        $this->table()->where('id', $extraFieldGroup->id())->delete();

        return true;
    }

    /**
     * @param array $filters
     * @return ExtraFieldGroupEntity
     */
    public function getEntity(array $filters = []) :ExtraFieldGroupEntity
    {
        $query = $this->table()->select(['*']);

        if(isset($filters['id'])) {
            $query->where('id', $filters['id']);
        }

        return $query->withEntities()->first();
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getExtraFieldsGroupsList() :LengthAwarePaginator
    {
        $order = 'a.id';
        $allowed_db = array(
            'group_id' => 'a.id',
            'group_title' => 'a.title'
        );

        if (!empty(Session::get('sort'))) {
            $sort = 'a.id';

            if (isset($allowed_db[Session::get('sort')])) {
                $sort = $allowed_db[Session::get('sort')];
            }

            $order = $sort . " " . ((Session::get('order') == 'asc') ? 'ASC' : 'DESC');
        }

        $query = $this->table('extra_fields_groups AS a')->select(['a.*'])->orderByRaw($order);

        $query->when(Session::get(node()->name().'.filters.filter_title'), function ($q) {
            return $q->where('a.title', 'like', '%'.Session::get(node()->name().'.filters.filter_title').'%');
        });

        return $query->paginate();
    }

    /**
     * @return Collection
     */
    public function getExtraFieldsGroups() :Collection
    {
        return $this->table()->select(['*'])->withEntities()->get();
    }
}