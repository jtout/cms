<?php

namespace App\Models\Backend;

use App\Models\Model;
use App\Facades\Session;
use App\Entities\AppLogEntity;
use App\Factories\AppLogFactory;
use Illuminate\Pagination\LengthAwarePaginator;

class AppLogs extends Model
{
    /**
     * @var string
     */
    protected $table = 'app_logs';

    /**
     * @var string
     */
    protected $factory = AppLogFactory::class;

    /**
     * @param array $filters
     * @param array $relations
     * @return AppLogEntity
     */
    public static function find(array $filters, array $relations = []) :AppLogEntity
    {
        return parent::find($filters, $relations);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function findById(int $id, array $relations = ['*']) :AppLogEntity
    {
        return parent::findById($id, $relations);
    }

    /**
     * @param $filters
     * @param array $relations
     * @return AppLogEntity
     */
    public static function findOrFail($filters, array $relations = []) :AppLogEntity
    {
        return parent::findOrFail($filters, $relations);
    }

    /**
     * @param array $filters
     * @return AppLogEntity
     */
    public function getEntity(array $filters = []) :AppLogEntity
    {
        $query = $this->table()->select(['*']);

        if(isset($filters['id'])) {
            $query->where('id', $filters['id']);
        }

        return $this->entity($query->first());
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getAppLogsList() :LengthAwarePaginator
    {
        $order = 'a.id DESC';
        $causerColumn = 'b.id';

        $allowed_db = array(
            'app_logs_id' => 'a.id',
            'app_logs_type' => 'a.type',
            'app_logs_model' => 'a.model_instance',
            'app_logs_causer' => 'b.user_name',
            'app_logs_created_on' => 'a.created_on'

        );

        if(request()->inputs('filters.filter_causer') == 'guest') {
            request()->set(['filters.filter_causer' => 3]);
            $causerColumn = 'a.created_by';
        }

        if (!empty(Session::get('sort'))) {
            $sort = 'a.id';

            if (isset($allowed_db[Session::get('sort')])) {
                $sort = $allowed_db[Session::get('sort')];
            }

            $order = $sort . " " . ((Session::get('order') == 'asc') ? 'ASC' : 'DESC');
        }

        $query = $this->table('app_logs AS a')
            ->select(['a.*'])
            ->leftJoin('users AS b', 'b.id', '=', 'a.created_by')
            ->orderByRaw($order);;

        $query->when(Session::get(node()->name().'.filters.filter_type'), function ($q) {
            return $q->where('a.type', 'like', '%'.Session::get(node()->name().'.filters.filter_type').'%');
        });

        if (!empty(Session::get(node()->name().'.filters.filter_causer'))) {
            if(session()->get(node()->name().'.filters.filter_causer') == 3) {
                $causer = 'guest';
            }
            else {
                $causer = session()->get(node()->name().'.filters.filter_causer');
            }

            $query->where('b.user_name', 'like', '%'.$causer.'%')
                ->orWhere($causerColumn, $causer);
        }

        $query->when(Session::get(node()->name().'.filters.filter_info'), function ($q) {
            return $q->where('a.info', 'like', '%'.Session::get(node()->name().'.filters.filter_info').'%');
        });

        return $query->paginate();
    }

    /**
     * @param AppLogEntity $appLog
     * @return bool
     */
    public function insert(AppLogEntity $appLog) :bool
    {
        $this->table()->insert([
            'type' => $appLog->type(),
            'model_instance' => $appLog->model_instance(),
            'info' => $appLog->info(),
            'created_by' => $appLog->created_by(),
            'created_on' => now(),
        ]);

        return true;
    }

    /**
     * @param AppLogEntity $appLog
     * @return bool
     */
    public function delete(AppLogEntity $appLog) :bool
    {
        $this->table()->where('id', $appLog->id())->delete();

        return true;
    }
}