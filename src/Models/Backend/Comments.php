<?php

namespace App\Models\Backend;

use App\Models\Model;
use App\Facades\Session;
use App\Entities\CommentEntity;
use App\Factories\CommentFactory;
use Illuminate\Pagination\LengthAwarePaginator;

class Comments extends Model
{
    /**
     * @var string
     */
    protected $table = 'comments';

    /**
     * @var string
     */
    protected $factory = CommentFactory::class;

    /**
     * @param array $filters
     * @param array $relations
     * @return CommentEntity
     */
    public static function find(array $filters, array $relations = []) :CommentEntity
    {
        return parent::find($filters, $relations);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function findById(int $id, array $relations = ['*']) :CommentEntity
    {
        return parent::findById($id, $relations);
    }

    /**
     * @param $filters
     * @param array $relations
     * @return CommentEntity
     */
    public static function findOrFail($filters, array $relations = []) :CommentEntity
    {
        return parent::findOrFail($filters, $relations);
    }

    /**
     * @param CommentEntity $comment
     * @return bool
     */
    public function insert(CommentEntity $comment) :bool
    {
        $id = $this->table()->insertGetId([
            'parent_id' => $comment->parent_id(),
            'text' => $comment->text(),
            'node_id' => $comment->node_id(),
            'is_active' => $comment->is_active(),
            'created_by' => $comment->created_by(),
            'created_on' => $comment->created_on(),
            'updated_by' => $comment->updated_by(),
            'updated_on' => $comment->updated_on(),
        ]);

        $comment->setId($id);

        return true;
    }

    /**
     * @param CommentEntity $comment
     * @return bool
     */
    public function update(CommentEntity $comment) :bool
    {
        $this->table()->where('id', $comment->id())->update([
            'text' => $comment->text(),
            'is_active' => $comment->is_active(),
            'updated_by' => $comment->updated_by(),
            'updated_on' => $comment->updated_on(),
        ]);

        return true;
    }

    /**
     * @param CommentEntity $comment
     * @return bool
     */
    public function delete(CommentEntity $comment) :bool
    {
        $this->table()->where('id', $comment->id())->delete();

        return true;
    }

    /**
     * @param array $filters
     * @return CommentEntity
     */
    public function getEntity(array $filters = array()) :CommentEntity
    {
        $query = $this->table('comments AS a')->select([
            'a.*' ,
            'b.title',
            'c.user_name AS author',
        ]);

        $likesCountQuery = $this->table('comments_likes')->selectRaw('COUNT(comment_id)')
            ->whereColumn('comment_id', '=', 'a.id');

        $query->selectSub($likesCountQuery,'likes_count')
            ->join('nodes_content AS b', 'b.node_id', '=', 'a.node_id')
            ->join('users AS c', 'c.id', '=', 'a.created_by');

        if (isset($filters['id'])) {
            $query->where('a.id', $filters['id']);
        }

        return $query->withEntities()->first();
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getCommentsList() :LengthAwarePaginator
    {
        $order = 'a.created_on DESC';

        $allowed_db = array(
            'comments_id' => 'a.id',
            'comments_active' => 'a.is_active',
            'comments_author' => 'b.user_name',
            'comments_title' => 'c.title'
        );

        if (!empty(Session::get('sort'))) {
            $sort = 'a.id';

            if (isset($allowed_db[Session::get('sort')])) {
                $sort = $allowed_db[Session::get('sort')];
            }

            $order = $sort . " " . ((Session::get('order') == 'asc') ? 'ASC' : 'DESC');
        }

        $query = $this->table('comments AS a')->select(['a.*', 'b.user_name AS author', 'c.title'])
            ->join('users AS b', 'b.id', '=', 'a.created_by')
            ->join('nodes_content AS c', 'c.node_id', '=', 'a.node_id')
            ->orderByRaw($order);

        $query->when(Session::get(node()->name().'.filters.filter_author'), function ($q) {
            return $q->where('b.user_name', 'like', '%'.Session::get(node()->name().'.filters.filter_author').'%');
        });

        $query->when(Session::get(node()->name().'.filters.filter_title'), function ($q) {
            return $q->where('c.title', 'like', '%'.Session::get(node()->name().'.filters.filter_title').'%');
        });

        $query->when(Session::get(node()->name().'.filters.filter_active'), function ($q) {
            $active = Session::get(node()->name().'.filters.filter_active');
            return $q->where('a.is_active', $active == 2 ? 0 : $active);
        });

        return $query->paginate();
    }
}