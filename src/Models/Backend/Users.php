<?php

namespace App\Models\Backend;

use App\Models\Model;
use App\Facades\Session;
use App\Entities\UserEntity;
use Illuminate\Support\Arr;
use App\Factories\UserFactory;
use App\Factories\ModuleFactory;
use App\Models\ModelQueryBuilder;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class Users extends Model
{
    /**
     * @var string
     */
    protected $table = 'users';

    /**
     * @var array
     */
    protected $relations = ['fields', 'permissions' => 'id'];

    /**
     * @var string
     */
    protected $factory = UserFactory::class;

    /**
     * @param array $filters
     * @param array $relations
     * @return UserEntity
     */
    public static function find(array $filters, array $relations = []) :UserEntity
    {
        return parent::find($filters, $relations);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function findById(int $id, array $relations = []) :UserEntity
    {
        return parent::findById($id, $relations);
    }

    /**
     * @param $filters
     * @param array $relations
     * @return UserEntity
     */
    public static function findOrFail($filters, array $relations = []) :UserEntity
    {
        return parent::findOrFail($filters, $relations);
    }

    /**
     * @param UserEntity $user
     * @return bool
     */
    public function insert(UserEntity $user) :bool
    {
        $usersValues = [
            'user_name' => $user->username(),
            'name' => $user->name(),
            'last_name' => $user->last_name(),
            'password' => hash_string($user->password()),
            'role_id' => $user->role_id(),
            'email' => $user->email(),
            'last_visit_date' => $user->last_visit_date(),
            'in_backend' => $user->in_backend(),
            'is_enabled' => $user->is_enabled(),
            'is_activated' => $user->is_activated(),
            'activation_token' => $user->activation_token(),
            'privacy_policy' => $user->privacy_policy(),
            'created_by' => $user->created_by(),
            'created_on' => $user->created_on(),
            'updated_by' => $user->updated_by(),
            'updated_on' => $user->updated_on(),
            'activated_on' => $user->activated_on()
        ];

        $id = $this->table()->insertGetId($usersValues);
        $user->setId($id);

        $usersFieldsValues = [
            'user_id' => $user->id(),
            'avatar' => $user->avatar(),
            'gender' => $user->gender(),
            'description' => $user->description(),
            'signature' => $user->signature(),
            'birthdate' => $user->birthdate(),
            'location' => $user->location(),
            'twitch' => $user->twitch(),
            'instagram' => $user->instagram(),
            'twitter' => $user->twitter(),
            'facebook' => $user->facebook(),
            'psn_id' => $user->psn_id(),
            'xbox_live_gamertag' => $user->xbox_live_gamertag(),
            'nintendo_id' => $user->nintendo_id(),
            'steam_id' => $user->steam_id(),
            'pc' => $user->pc(),
            'tablet' => $user->tablet(),
            'smartphone' => $user->smartphone(),
        ];

        $this->table('users_fields')->insert($usersFieldsValues);

        // Insert into users_modules table
        $userPermissions = $user->getRelations()->permissions;
        $permissions = explode(',', $userPermissions);

        foreach ($permissions as $permission) {
            $this->table('users_modules')->insert([
                'user_id' => $user->id(),
                'module_id' => $permission
            ]);
        }

        return true;
    }

    /**
     * @param UserEntity $user
     * @return bool
     */
    public function update(UserEntity $user) :bool
    {
        $usersValues = [
            'name' => $user->name(),
            'last_name' => $user->last_name(),
            'user_name' => $user->username(),
            'role_id' => $user->role_id(),
            'email' => $user->email(),
            'last_visit_date' => $user->last_visit_date(),
            'in_backend' => $user->in_backend(),
            'is_enabled' => $user->is_enabled(),
            'is_activated' => $user->is_activated(),
            'privacy_policy' => $user->privacy_policy(),
            'updated_by' => $user->updated_by(),
            'updated_on' => now(),
        ];

        if(!empty($user->password()) && $user->isDirty('password')) {
            $usersValues['password'] = hash_string($user->password());
        }

        $this->table()->where('id', $user->id())->update($usersValues);

        $usersFieldsValues = [
            'avatar' => $user->avatar(),
            'gender' => $user->gender(),
            'description' => $user->description(),
            'signature' => $user->signature(),
            'birthdate' => $user->birthdate(),
            'location' => $user->location(),
            'twitch' => $user->twitch(),
            'instagram' => $user->instagram(),
            'twitter' => $user->twitter(),
            'facebook' => $user->facebook(),
            'psn_id' => $user->psn_id(),
            'xbox_live_gamertag' => $user->xbox_live_gamertag(),
            'nintendo_id' => $user->nintendo_id(),
            'steam_id' => $user->steam_id(),
            'pc' => $user->pc(),
            'tablet' => $user->tablet(),
            'smartphone' => $user->smartphone(),
        ];

        $this->table('users_fields')->where('user_id', $user->id())->update($usersFieldsValues);

        $userPermissions = $user->getRelations()->permissions;
        if(is_string($userPermissions) && !empty($userPermissions)) {
            $this->table('users_modules')->where('user_id', $user->id())->delete();

            $permissions = explode(',', $userPermissions);
            foreach ($permissions as $permission) {
                $this->table('users_modules')->insert([
                    'user_id' => $user->id(),
                    'module_id' => $permission
                ]);
            }
        }

        return true;
    }

    /**
     * @param UserEntity $user
     * @return bool
     */
    public function delete(UserEntity $user) :bool
    {
        $this->table()
            ->where('id', $user->id())
            ->whereNotIn('role_id', [1,2])
            ->delete();

        return true;
    }

    /**
     * @param array $filters
     * @param array $relations
     * @return UserEntity
     */
    public function getEntity(array $filters, array $relations = []) :UserEntity
    {
        $query = $this->table()->select(['*']);

        foreach ($filters as $column => $value) {
            $query->where($column, $value);
        }

        $user = $query->withRelations($relations)->withEntities()->first();

        return $user ?? new UserEntity();
    }

    /**
     * @param Collection $records
     * @param array $relations
     * @return mixed
     */
    public function loadDataRelations(Collection $records, array $relations = [])
    {
        $relationsData = new Collection();

        if (in_array('permissions', $relations)) {
            $ids = $records->pluck('id')->toArray();
            $permissions = $this->getUserPermissions($ids);
            $userPermissions = collect();

            foreach ($ids as $id) {
                $dataToCollect = $permissions->where('user_id', $id)
                    ->keyBy('action')->transform(function ($permission){
                        return ModuleFactory::build($permission);
                    });

                $userPermissions->put($id, $dataToCollect);
            }

            $relationsData->put('permissions', $userPermissions);
        }

        return $relationsData;
    }

    /**
     * @param ModelQueryBuilder $query
     * @param array $relations
     * @return ModelQueryBuilder
     */
    public function loadQueryRelations(ModelQueryBuilder $query, array $relations = []) :ModelQueryBuilder
    {
        if (in_array('fields', $relations)) {
            $query->join('users_fields', 'user_id', '=', 'id');
        }

        return $query;
    }

    /**
     * @param null $userId
     * @return Collection
     */
    public function getUserPermissions($userId = null) :Collection
    {
        $query = $this->table('users_modules AS a')
            ->select(['a.module_id AS id', 'b.action', 'a.user_id'])
            ->join('modules AS b', 'b.id', '=', 'a.module_id')
            ->where('b.is_active', 1)
            ->whereIn('a.user_id', Arr::wrap($userId))
            ->where('b.action', '<>', '');

        return $query->get();
    }

    /**
     * @param array $roles
     * @return Collection
     */
    public function getUsersByRole(array $roles) :Collection
    {
        $query = $this->table()->select(['id', 'name', 'last_name', 'email'])
            ->whereIn('role_id', $roles)
            ->where('id', '<>', 1);

        return $query->withEntities()->get();
    }

    /**
     * @param string $action
     * @param string $email
     * @return bool|string
     */
    public function resetAction(string $action, string $email)
    {
        switch($action)
        {
            case 'username':
                $user = $this->getEntity(['email' => $email]);

                return $user->username();
                break;

            case 'password':
                $password = uniqid();
                $user = $this->getEntity(['email' => $email], ['fields']);
                $user->setPassword($password);
                $user->update();

                return $password;
                break;

            default:
                return false;
                break;
        }
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getUsersList(): LengthAwarePaginator
    {
        $order = 'a.id';

        $allowed_db = array(
            'users_id' => 'a.id',
            'users_name' => 'a.name',
            'users_users_username' => 'a.user_name',
            'users_role' => 'b.title',
            'users_active' => 'a.is_enabled',
            'users_activated' => 'a.is_activated',
            'users_backend' => 'a.in_backend',
            'users_registration_date' => 'a.created_on'
        );

        if (!empty(Session::get('sort'))) {
            $sort = 'a.id';

            if (isset($allowed_db[Session::get('sort')])) {
                $sort = $allowed_db[Session::get('sort')];
            }

            $order = $sort . " " . ((Session::get('order') == 'asc') ? 'ASC' : 'DESC');
        }

        $query = $this->table('users AS a')->select(['a.*'])
            ->join('roles AS b', 'b.id', '=', 'a.role_id')
            ->orderByRaw($order)
            ->groupBy(['a.id']);

        $query->when(Session::get(node()->name().'.filters.filter_title'), function ($q) {
            $title = Session::get(node()->name().'.filters.filter_title');

            return $q->where('a.name','like', '%'.$title.'%')
                ->orWhere('a.user_name', 'like', '%'.$title.'%')
                ->orWhere('a.last_name', 'like', '%'.$title.'%')
                ->orWhere('a.email', 'like', '%'.$title.'%');
        });

        $query->when(Session::get(node()->name().'.filters.filter_role'), function ($q) {
            return $q->where('a.role_id', Session::get(node()->name().'.filters.filter_role'));
        });

        $query->when(Session::get(node()->name().'.filters.filter_active'), function ($q) {
            $active = Session::get(node()->name().'.filters.filter_active');
            return $q->where('a.is_enabled', $active == 2 ? 0 : $active);
        });

        $query->when(Session::get(node()->name().'.filters.filter_activated'), function ($q) {
            $active = Session::get(node()->name().'.filters.filter_activated');
            return $q->where('a.is_activated', $active == 2 ? 0 : $active);
        });

        $query->when(Session::get(node()->name().'.filters.filter_backend'), function ($q) {
            $backend = Session::get(node()->name().'.filters.filter_backend');
            return $q->where('a.in_backend', $backend == 2 ? 0 : $backend);
        });

        return $query->paginate();
    }
}
