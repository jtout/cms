<?php

namespace App\Models\Backend;

use App\Models\Model;
use App\Facades\Session;
use App\Entities\RoleEntity;
use App\Factories\RoleFactory;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class Roles extends Model
{
    /**
     * @var string
     */
    protected $table = 'roles';

    /**
     * @var string
     */
    protected $factory = RoleFactory::class;

    /**
     * @param array $filters
     * @param array $relations
     * @return RoleEntity
     */
    public static function find(array $filters, array $relations = []) :RoleEntity
    {
        return parent::find($filters, $relations);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function findById(int $id, array $relations = ['*']) :RoleEntity
    {
        return parent::findById($id, $relations);
    }

    /**
     * @param $filters
     * @param array $relations
     * @return RoleEntity
     */
    public static function findOrFail($filters, array $relations = []) :RoleEntity
    {
        return parent::findOrFail($filters, $relations);
    }

    /**
     * @param RoleEntity $role
     * @return bool
     */
    public function insert(RoleEntity $role) :bool
    {
        $id = $query = $this->table()->insertGetId([
            'parent_id' => $role->parent_id(),
            'title' => $role->title(),
            'in_backend' => $role->in_backend(),
            'created_by' => $role->created_by(),
            'created_on' => $role->created_on(),
            'updated_by' => $role->updated_by(),
            'updated_on' => $role->updated_on()
        ]);

        $role->setId($id);

        // Insert into roles_access_levels table
        $this->table('roles_access_levels')->insert(['role_id' => $id, 'access_level_id' => $role->access_level_id()]);

        // Insert into roles_modules table
        $permissions = explode(',', $role->permissions());
        foreach ($permissions as $permission) {
            $this->table('roles_modules')->insert(['role_id' => $id, 'module_id' => $permission]);
        }

        return true;
    }

    /**
     * @param RoleEntity $role
     * @return bool
     */
    public function update(RoleEntity $role) :bool
    {
        $this->table()->where('id', $role->id())->update([
            'parent_id' => $role->parent_id(),
            'title' => $role->title(),
            'in_backend' => $role->in_backend(),
            'updated_by' => $role->updated_by(),
            'updated_on' => $role->updated_on()
        ]);

        // Update roles_access_levels table
        $this->table('roles_access_levels')->where('role_id', $role->id())->delete();
        $this->table('roles_access_levels')->insert(['role_id' => $role->id(), 'access_level_id' => $role->access_level_id()]);

        // Update roles_modules table
        $this->table('roles_modules')->where('role_id', $role->id())->delete();

        $permissions = explode(',', $role->permissions());
        foreach ($permissions as $permission) {
            $this->table('roles_modules')->insert(['role_id' => $role->id(), 'module_id' => $permission]);
        }

        return true;
    }

    /**
     * @param RoleEntity $role
     * @return bool
     */
    public function delete(RoleEntity $role) :bool
    {
        $this->table()->where('id', $role->id())->where('id', '<>', 1)->delete();

        return true;
    }

    /**
     * @param array $filters
     * @return Collection
     */
    public function getRoles($filters = array()) :Collection
    {
        $query = $this->table('roles AS a')->select([
            'a.id',
            'a.parent_id',
            'a.title',
            'a.in_backend',
            'b.depth',
        ])
            ->selectRaw('GROUP_CONCAT(c.module_id) AS permissions')
            ->join('roles_tree AS b', 'b.child_id', '=', 'a.id')
            ->leftJoin('roles_modules AS c', 'c.role_id', '=', 'a.id')
            ->where('b.parent_id', 1)
            ->groupBy(['a.id'])
            ->orderBy('b.path');

        if (isset($filters['in_backend'])) {
            $query->where('a.in_backend', $filters['in_backend']);
        }

        if (isset($filters['id'])) {
            $query->where('a.id', $filters['id']);
        }

        return $query->withEntities()->get();
    }

    /**
     * @param array $filters
     * @return RoleEntity
     */
    public function getEntity(array $filters = []) :RoleEntity
    {
        $query = $this->table('roles AS a')
            ->select([
                'a.id',
                'a.parent_id',
                'a.title',
                'a.in_backend',
                'c.access_level_id',
                'd.title AS access_level',
                'd.label AS access_level_label'
            ])
            ->selectRaw('GROUP_CONCAT(e.module_id) AS permissions')
            ->join('roles_access_levels AS c', 'c.role_id', '=', 'a.id')
            ->join('access_levels AS d', 'd.id', '=', 'c.access_level_id')
            ->leftJoin('roles_modules AS e', 'e.role_id', '=', 'a.id');

        if (isset($filters['id'])) {
            $query->where('a.id', $filters['id']);
        }

        return $query->withEntities()->first();
    }

    /**
     * @param int $roleId
     * @return Collection
     */
    public function getRolesPermissions($roleId = 7) :Collection
    {
        $query = $this->table('roles_modules AS a')
            ->select(['a.module_id AS id', 'b.action'])
            ->join('modules AS b', 'b.id', '=', 'a.module_id')
            ->where('b.is_active', 1)
            ->whereIn('a.role_id', Arr::wrap($roleId))
            ->where('b.action', '<>', '');

        return $query->get();
    }

    /**
     * @return LengthAwarePaginator
     */
    public function rolesList(): LengthAwarePaginator
    {
        $order = 'a.parent_id ';
        $allowed_db = array(
            'roles_id' => 'a.id',
            'roles_title' => 'a.title',
            'roles_access_level' => 'd.title',
            'roles_backend' => 'a.in_backend'
        );

        if (!empty(Session::get('sort'))) {
            $sort = 'a.parent_id';

            if (isset($allowed_db[Session::get('sort')])) {
                $sort = $allowed_db[Session::get('sort')];
            }

            $order = $sort . " " . ((Session::get('order') == 'asc') ? 'ASC' : 'DESC');
        }

        $query = $this->table('roles AS a')->select([
            'a.*',
            'b.depth',
            'd.title AS access_level',
            'd.label AS access_level_label'
        ])
            ->join('roles_tree AS b', 'b.child_id', '=', 'a.id')
            ->join('roles_access_levels AS c', 'c.role_id', '=', 'a.id')
            ->join('access_levels AS d', 'd.id', '=', 'c.access_level_id')
            ->where('b.parent_id', 1)
            ->orderByRaw($order);

        $query->when(Session::get(node()->name().'.filters.filter_title'), function ($q) {
            return $q->where('a.title', 'like', '%'.Session::get(node()->name().'.filters.filter_title').'%');
        });

        $query->when(Session::get(node()->name().'.filters.filter_access_level'), function ($q) {
            return $q->where('d.id', '=', Session::get(node()->name().'.filters.filter_access_level'));
        });

        $query->when(Session::get(node()->name().'.filters.filter_backend'), function ($q) {
            $backend = Session::get(node()->name().'.filters.filter_backend');
            return $q->where('a.in_backend', $backend == 2 ? 0 : $backend);
        });

        return $query->paginate();
    }

    /**
     * @param int $role
     * @return string
     */
    public function getAuthorizedRoles(int $role) :string
    {
        $authorizedRoles = '';

        $row = $this->table('roles AS a')->select(['b.path'])
            ->join('roles_tree AS b', 'b.child_id', '=', 'a.id')
            ->where(['b.parent_id' => 1, 'a.id' => $role])
            ->orderBy('a.parent_id')->first();

        if ($row) {
            $authorizedRoles = $row->path;
        }

        return $authorizedRoles;
    }
}