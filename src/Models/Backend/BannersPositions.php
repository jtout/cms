<?php

namespace App\Models\Backend;

use App\Models\Model;
use App\Facades\Session;
use Illuminate\Support\Collection;
use App\Entities\BannersPositionEntity;
use App\Factories\BannersPositionFactory;
use Illuminate\Pagination\LengthAwarePaginator;

class BannersPositions extends Model
{
    /**
     * @var string
     */
    protected $table = 'banners_positions';

    /**
     * @var string
     */
    protected $factory = BannersPositionFactory::class;

    /**
     * @param array $filters
     * @param array $relations
     * @return BannersPositionEntity
     */
    public static function find(array $filters, array $relations = []) :BannersPositionEntity
    {
        return parent::find($filters, $relations);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function findById(int $id, array $relations = ['*']) :BannersPositionEntity
    {
        return parent::findById($id, $relations);
    }

    /**
     * @param $filters
     * @param array $relations
     * @return BannersPositionEntity
     */
    public static function findOrFail($filters, array $relations = []) :BannersPositionEntity
    {
        return parent::findOrFail($filters, $relations);
    }

    /**
     * @param array $filters
     * @return BannersPositionEntity
     */
    public function getEntity(array $filters = []) :BannersPositionEntity
    {
        $query = $this->table();

        if (isset($filters['id'])) {
            $query->where('id', $filters['id']);
        }

        return $query->withEntities()->first();
    }

    /**
     * @param BannersPositionEntity $position
     * @return bool
     */
    public function insert(BannersPositionEntity $position) :bool
    {
        $id = $this->table()->insertGetId([
            'title' => $position->title(),
            'alias' => $position->alias(),
            'created_by' => $position->created_by(),
            'created_on' => $position->created_on(),
            'updated_by' => $position->updated_by(),
            'updated_on' => $position->updated_on(),
        ]);

        $position->setId($id);

        return true;
    }

    /**
     * @param BannersPositionEntity $position
     * @return bool
     */
    public function update(BannersPositionEntity $position) :bool
    {
        $this->table()->where('id', $position->id())
            ->update([
                'title' => $position->title(),
                'alias' => $position->alias(),
                'updated_by' => $position->updated_by(),
                'updated_on' => $position->updated_on(),
            ]);

        return true;
    }

    /**
     * @param BannersPositionEntity $position
     * @return bool
     */
    public function delete(BannersPositionEntity $position) :bool
    {
        $this->table()->where('id', $position->id())->delete();

        return true;
    }

    /**
     * @return Collection
     */
    public function getPositions() :Collection
    {
        return $this->table()->select(['*'])->withEntities()->get();
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getBannersPositionsList() :LengthAwarePaginator
    {
        $order = 'a.id';

        $allowed_db = array(
            'field_id' => 'a.id',
            'field_title' => 'a.title'
        );

        if (!empty(Session::get('sort'))) {
            $sort = 'a.id';

            if (isset($allowed_db[Session::get('sort')])) {
                $sort = $allowed_db[Session::get('sort')];
            }

            $order = $sort . " " . ((Session::get('order') == 'asc') ? 'ASC' : 'DESC');
        }

        $query = $this->table('banners_positions AS a')
            ->select(['a.*'])
            ->orderByRaw($order);

        $query->when(Session::get(node()->name().'.filters.filter_title'), function ($q) {
            return $q->where('a.title', 'like', '%'.Session::get(node()->name().'.filters.filter_title').'%');
        });

        return $query->paginate();
    }
}