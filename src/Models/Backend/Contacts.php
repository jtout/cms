<?php

namespace App\Models\Backend;

use App\Models\Model;
use App\Facades\Session;
use App\Entities\ContactEntity;
use App\Factories\ContactFactory;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class Contacts extends Model
{
    /**
     * @var string
     */
    protected $table = 'clients_contacts';

    /**
     * @var string
     */
    protected $factory = ContactFactory::class;


    /**
     * @param array $filters
     * @param array $relations
     * @return ContactEntity
     */
    public static function find(array $filters, array $relations = []) :ContactEntity
    {
        return parent::find($filters, $relations);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function findById(int $id, array $relations = ['*']) :ContactEntity
    {
        return parent::findById($id, $relations);
    }

    /**
     * @param $filters
     * @param array $relations
     * @return ContactEntity
     */
    public static function findOrFail($filters, array $relations = []) :ContactEntity
    {
        return parent::findOrFail($filters, $relations);
    }

    /**
     * @param int $clientId
     * @return Collection
     */
    public function getClientContactsList(int $clientId) :Collection
    {
        return $this->table()
            ->select(['*'])
            ->where('client_id', $clientId)
            ->withEntities()
            ->get();
    }

    /**
     * @param array $filters
     * @return ContactEntity
     */
    public function getEntity(array $filters = []) :ContactEntity
    {
        $query = $this->table();

        if(isset($filters['id'])) {
            $query->where('id', $filters['id']);
        }

        return $query->withEntities()->first();
    }

    /**
     * @param ContactEntity $contact
     * @return bool
     */
    public function insert(ContactEntity $contact) :bool
    {
        $id = $this->table()->insertGetId([
            'client_id' => $contact->client_id(),
            'name' => $contact->name(),
            'email' => $contact->email(),
            'phone' => $contact->phone(),
            'is_active' => $contact->is_active(),
            'created_by' => $contact->created_by(),
            'created_on' => $contact->created_on(),
            'updated_by' => $contact->updated_by(),
            'updated_on' => $contact->updated_on(),
        ]);

        $contact->setId($id);

        return true;
    }

    /**
     * @param ContactEntity $contact
     * @return bool
     */
    public function update(ContactEntity $contact) :bool
    {
        $this->table()->where('id', $contact->id())->update([
            'name' => $contact->name(),
            'email' => $contact->email(),
            'phone' => $contact->phone(),
            'is_active' => $contact->is_active(),
            'updated_by' => $contact->updated_by(),
            'updated_on' => $contact->updated_on(),
        ]);

        return true;
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getContactsList() :LengthAwarePaginator
    {
        $order = 'a.id';
        $allowed_db = array(
            'contacts_id' => 'a.id',
            'contacts_name' => 'a.name',
            'contacts_client' => 'b.title',
            'contacts_active' => 'a.is_active'
        );

        if (!empty(Session::get('sort'))) {
            $sort = 'a.id';

            if (isset($allowed_db[Session::get('sort')])) {
                $sort = $allowed_db[Session::get('sort')];
            }

            $order = $sort . " " . ((Session::get('order') == 'asc') ? 'ASC' : 'DESC');
        }

        $query = $this->table('clients_contacts AS a')->select(['a.*', 'b.title AS client_title'])
            ->join('clients AS b', 'b.id', '=', 'a.client_id')
            ->orderByRaw($order);

        $query->when(Session::get(node()->name().'.filters.filter_name'), function ($q) {
            return $q->where('a.name', 'like', '%'.Session::get('filter_name').'%');
        });

        $query->when(Session::get(node()->name().'.filters.filter_clients'), function ($q) {
            return $q->where('a.client_id', Session::get(node().'.filters.filter_clients'));
        });

        $query->when(Session::get(node()->name().'.filters.filter_active'), function ($q) {
            $active = Session::get(node()->name().'.filters.filter_active');
            return $q->where('a.is_active', $active == 2 ? 0 : $active);
        });

        return $query->paginate();
    }
}