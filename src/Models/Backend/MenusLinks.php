<?php

namespace App\Models\Backend;

use App\Models\Model;
use App\Entities\MenuLinkEntity;
use App\Factories\MenuLinkFactory;

class MenusLinks extends Model
{
    /**
     * @var string
     */
    protected $table = 'menus_links';

    /**
     * @var string
     */
    protected $factory = MenuLinkFactory::class;

    /**
     * @param array $filters
     * @param array $relations
     * @return MenuLinkEntity
     */
    public static function find(array $filters, array $relations = []) :MenuLinkEntity
    {
        return parent::find($filters, $relations);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function findById(int $id, array $relations = ['*']) :MenuLinkEntity
    {
        return parent::findById($id, $relations);
    }

    /**
     * @param $filters
     * @param array $relations
     * @return MenuLinkEntity
     */
    public static function findOrFail($filters, array $relations = []) :MenuLinkEntity
    {
        return parent::findOrFail($filters, $relations);
    }

    /**
     * @param int $id
     * @return MenuLinkEntity
     */
    public function getEntity(int $id) :MenuLinkEntity
    {
        return $this->table()->select(['*'])
            ->where('id', $id)
            ->withEntities();
    }
}