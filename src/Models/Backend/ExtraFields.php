<?php

namespace App\Models\Backend;

use App\Models\Model;
use App\Facades\Session;
use App\Entities\ExtraFieldEntity;
use Illuminate\Support\Collection;
use App\Factories\ExtraFieldFactory;
use Illuminate\Pagination\LengthAwarePaginator;

class ExtraFields extends Model
{
    /**
     * @var string
     */
    protected $table = 'extra_fields';

    /**
     * @var string
     */
    protected $factory = ExtraFieldFactory::class;

    /**
     * @param array $filters
     * @param array $relations
     * @return ExtraFieldEntity
     */
    public static function find(array $filters, array $relations = []) :ExtraFieldEntity
    {
        return parent::find($filters, $relations);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function findById(int $id, array $relations = ['*']) :ExtraFieldEntity
    {
        return parent::findById($id, $relations);
    }

    /**
     * @param $filters
     * @param array $relations
     * @return ExtraFieldEntity
     */
    public static function findOrFail($filters, array $relations = []) :ExtraFieldEntity
    {
        return parent::findOrFail($filters, $relations);
    }

    /**
     * @param ExtraFieldEntity $extraField
     * @return bool
     */
    public function update(ExtraFieldEntity $extraField) :bool
    {
        $this->table()->where('id', $extraField->id())->update([
            'title' => $extraField->title(),
            'group_id' => $extraField->group_id(),
            'type' => $extraField->type(),
            'info' => $extraField->info(),
            'is_active' => $extraField->is_active(),
            'ordering' => $extraField->ordering(),
            'updated_by' => $extraField->updated_by(),
            'updated_on' => $extraField->updated_on(),
        ]);

        return true;
    }

    /**
     * @param ExtraFieldEntity $extraField
     * @return bool
     */
    public function insert(ExtraFieldEntity $extraField) :bool
    {
        $id = $this->table()->insertGetId([
            'title' => $extraField->title(),
            'group_id' => $extraField->group_id(),
            'type' => $extraField->type(),
            'info' => $extraField->info(),
            'is_active' => $extraField->is_active(),
            'created_by' => $extraField->created_by(),
            'created_on' => $extraField->created_on(),
            'updated_by' => $extraField->updated_by(),
            'updated_on' => $extraField->updated_on(),
        ]);

        $extraField->setId($id);

        return true;
    }

    /**
     * @param ExtraFieldEntity $extraField
     * @return bool
     */
    public function delete(ExtraFieldEntity $extraField) :bool
    {
        $this->table()->where('id', $extraField->id())->delete();

        return true;
    }

    /**
     * @param array $filters
     * @return ExtraFieldEntity
     */
    public function getEntity(array $filters = []) :ExtraFieldEntity
    {
        $query = $this->table('extra_fields AS a')->select(['a.*'])
            ->join('extra_fields_groups AS b', 'b.id', '=', 'a.group_id');

        if (isset($filters['id'])) {
            $query->where('a.id', $filters['id']);
        }

        return $query->withEntities()->first();
    }

    /**
     * @return array
     */
    public function getExtraFieldsList() :LengthAwarePaginator
    {
        $order = 'a.id';
        $allowed_db = array(
            'field_id' => 'a.id',
            'field_title' => 'a.title',
            'field_group' => 'b.title',
            'field_active' => 'a.is_active',
            'field_order' => 'a.ordering'
        );

        if (!empty(Session::get('sort'))) {
            $sort = 'a.id';

            if (isset($allowed_db[Session::get('sort')])) {
                $sort = $allowed_db[Session::get('sort')];
            }

            $order = $sort . " " . ((Session::get('order') == 'asc') ? 'ASC' : 'DESC');
        }

        $query = $this->table('extra_fields AS a')->select(['a.*', 'b.title AS group_title'])
            ->join('extra_fields_groups AS b', 'b.id', '=', 'a.group_id')
            ->orderByRaw($order);

        $query->when(Session::get(node()->name().'.filters.filter_title'), function ($q) {
            return $q->where('a.title', 'like', '%'.Session::get(node()->name().'.filters.filter_title').'%');
        });

        $query->when(Session::get(node()->name().'.filters.filter_group'), function ($q) {
            return $q->where('a.group_id', Session::get(node()->name().'.filters.filter_group'));
        });

        return $query->paginate();
    }

    /**
     * @param array $filters
     * @return Collection
     */
    public function getExtraFields($filters = array()) :Collection
    {
        $query = $this->table()->select(['*']);

        if (isset($filters['id'])) {
            $query->where('id', $filters['id']);
        }

        if (isset($filters['is_active'])) {
            $query->where('is_active', $filters['is_active']);
        }

        if (isset($filters['group_id'])) {
            $query->where('group_id', $filters['group_id']);
        }

        $query->orderBy('ordering');

        return $query->withEntities()->get();
    }
}