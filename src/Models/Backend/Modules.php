<?php

namespace App\Models\Backend;

use App\Models\Model;
use App\Facades\Session;
use App\Entities\ModuleEntity;
use App\Factories\ModuleFactory;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class Modules extends Model
{
    /**
     * @var string
     */
    protected $table = 'modules';

    /**
     * @var string
     */
    protected $factory = ModuleFactory::class;

    /**
     * @param array $filters
     * @param array $relations
     * @return ModuleEntity
     */
    public static function find(array $filters, array $relations = []) :ModuleEntity
    {
        return parent::find($filters, $relations);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function findById(int $id, array $relations = ['*']) :ModuleEntity
    {
        return parent::findById($id, $relations);
    }

    /**
     * @param $filters
     * @param array $relations
     * @return ModuleEntity
     */
    public static function findOrFail($filters, array $relations = []) :ModuleEntity
    {
        return parent::findOrFail($filters, $relations);
    }

    /**
     * @param array $filters
     * @return Collection
     */
    public function getModules($filters = array()) :Collection
    {
        $query = $this->table('modules AS a')->select([
            'a.id',
            'a.parent_id',
            'a.title',
            'a.action',
            'b.depth',
            'a.is_function'
        ])
            ->join('modules_tree AS b', 'b.child_id', '=', 'a.id')
            ->where('b.parent_id', 1)
            ->orderBy('b.path');

        if (isset($filters['is_function'])) {
            $query->where('a.is_function', $filters['is_function']);
        }

        if (isset($filters['is_active'])) {
            $query->where('a.is_active', $filters['is_active']);
        }

        return $query->withEntities()->get();
    }

    /**
     * @param ModuleEntity $module
     * @return bool
     */
    public function update(ModuleEntity $module) :bool
    {
        $this->table()->where('id', $module->id())->update([
            'parent_id' => $module->parent_id(),
            'is_function' => $module->is_function(),
            'title' => $module->title(),
            'class' => $module->class(),
            'action' => $module->action(),
            'description' => $module->description(),
            'is_active' => $module->is_active(),
            'updated_by' => $module->updated_by(),
            'updated_on' => $module->updated_on(),
        ]);

        return true;
    }

    /**
     * @param ModuleEntity $module
     * @return bool
     */
    public function insert(ModuleEntity $module) :bool
    {
        $id = $this->table()->insertGetId([
            'parent_id' => $module->parent_id(),
            'is_function' => $module->is_function(),
            'title' => $module->title(),
            'class' => $module->class(),
            'action' => $module->action(),
            'description' => $module->description(),
            'is_active' => $module->is_active(),
            'created_by' => $module->created_by(),
            'created_on' => $module->created_on(),
            'updated_by' => $module->updated_by(),
            'updated_on' => $module->updated_on()
        ]);

        $module->setId($id);

        return true;
    }

    /**
     * @param ModuleEntity $module
     * @return bool
     */
    public function delete(ModuleEntity $module) :bool
    {
        $this->table()->where('id', $module->id())->delete();

        return true;
    }

    /**
     * @param array $filters
     * @return ModuleEntity
     */
    public function getEntity(array $filters = []) :ModuleEntity
    {
        $query = $this->table()->select(['*']);

        if (isset($filters['id'])) {
            $query->where('id', $filters['id']);
        }

        return $query->withEntities()->first();
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getModulesList() :LengthAwarePaginator
    {
        $order = 'b.path';
        $path = '';

        $allowed_db = array(
            'modules_id' => 'a.id',
            'modules_title' => 'a.title',
            'modules_description' => 'a.description',
            'modules_active' => 'a.is_active',
            'modules_function' => 'a.is_function',
            'modules_action' => 'a.action'
        );

        if (isset(request()->inputs('filters')['filter_modules'])) {
            if (request()->inputs('filters.filter_modules') == 1) {
                $path = 'System/Frontend/';
            }
            else if (request()->inputs('filters.filter_modules') == 2) {
                $path = 'System/Backend/';
            }
        }

        if (!empty(Session::get('sort'))) {
            $sort = 'b.path';

            if (isset($allowed_db[Session::get('sort')])) {
                $sort = $allowed_db[Session::get('sort')];
            }

            $order = $sort . " " . ((Session::get('order') == 'asc') ? 'ASC' : 'DESC');
        }

        $query = $this->table('modules AS a')->select(['a.*', 'b.depth'])
            ->join('modules_tree AS b', 'b.child_id', '=', 'a.id')
            ->where('b.parent_id', 1)
            ->orderByRaw($order);

        $query->when(Session::get(node()->name().'.filters.filter_modules'), function ($q) use($path) {
            return $q->where('b.path', 'like', $path.'%');
        });

        $query->when(Session::get(node()->name().'.filters.filter_title'), function ($q) {
            return $q->where('a.title', 'like', '%'.Session::get(node()->name().'.filters.filter_title').'%');
        });

        $query->when(Session::get(node()->name().'.filters.filter_level'), function ($q) {
            return $q->where('b.depth', Session::get(node()->name().'.filters.filter_level'));
        });

        $query->when(Session::get(node()->name().'.filters.filter_active'), function ($q) {
            $active = Session::get(node()->name().'.filters.filter_active');
            return $q->where('a.is_active', $active == 2 ? 0 : $active);
        });

        $query->when(Session::get(node()->name().'.filters.filter_function'), function ($q) {
            $function = Session::get(node()->name().'.filters.filter_function');
            return $q->where('a.is_function', $function == 2 ? 0 : $function);
        });

        return $query->paginate();
    }
}