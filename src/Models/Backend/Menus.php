<?php

namespace App\Models\Backend;

use App\Traits\App;
use App\Models\Model;
use App\Entities\MenuEntity;
use App\Factories\MenuFactory;
use App\Factories\NodeFactory;
use Illuminate\Support\Collection;

class Menus extends Model
{
    use \App\Traits\Nodes;

    /**
     * @var string
     */
    protected $table = 'menus';

    /**
     * @var string
     */
    protected $factory = MenuFactory::class;

    /**
     * @param array $filters
     * @param array $relations
     * @return MenuEntity
     */
    public static function find(array $filters, array $relations = []) :MenuEntity
    {
        return parent::find($filters, $relations);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function findById(int $id, array $relations = ['*']) :MenuEntity
    {
        return parent::findById($id, $relations);
    }

    /**
     * @param $filters
     * @param array $relations
     * @return MenuEntity
     */
    public static function findOrFail($filters, array $relations = []) :MenuEntity
    {
        return parent::findOrFail($filters, $relations);
    }

    /**
     * @param array $filters
     * @return MenuEntity
     */
    public function getEntity(array $filters = []) :MenuEntity
    {
        $query = $this->table()->select(['*']);

        foreach ($filters as $column => $filter) {
            $query->where($column, $filter);
        }

        return $query->withEntities()->first();
    }

    /**
     * @param int $backend
     * @param array $selectedNodes
     * @param bool $homepage
     * @return array
     */
    public function getMenus($backend = 0, $selectedNodes = array(), $homepage = false)
    {
        $data = array();
        $accessLevels = new AccessLevels();
        $data['access_levels'] = $accessLevels->getAccessLevels();

        foreach (LANGUAGES as $code => $title) {
            $language  = Languages::find(['iso_code' => $code]);
            $nodes  = $this->getNodesWithChildren($language->id(), $backend);
            $data['menu'][$code] = $this->createMenuTree($nodes->toArray(), $selectedNodes);
        }

        return $data;
    }


    /**
     * @param int $id
     * @return \array
     */
    public function fetchMenuInfo(int $id) :array
    {
        $query = $this->table('nodes_closure AS a')
            ->select(['b.name','b.is_active','b.on_menu','b.access_level_id','a.path','c.title','c.language_id'])
            ->join('nodes AS b', 'b.id', '=', 'a.descendant')
            ->join('nodes_content AS c', 'c.node_id', '=', 'b.id')
            ->where(['a.ancestor' => 1, 'b.id' => $id]);

        $row = (array)$query->first();

        return $row;
    }

    /**
     * @param int $id
     * @param int $parent
     * @param array $ordering
     * @return array
     */
    public function updateOrder(int $id, int $parent, array $ordering) :array
    {
        try {
            $response = array();

            $this->beginTransaction();

            $node = Nodes::findById($id);
            $node->setParentId($parent);
            $node->update();

            foreach($ordering as $order => $id) {
                $order = App::sanitizeInput($order, 'INT');
                $id    = App::sanitizeInput($id, 'INT');

                $node = Nodes::findById($id);
                $node->setOrder($order);
                $node->setUpdatedBy(user()->id());
                $node->setUpdatedOn(now());
                $node->update();
            }

            $this->commit();

            $response['success'] = true;
            $response['type'] = 'success';
            $response['message'] = 'Menu Order updated';
        } catch (\Throwable $e) {
            $this->rollBack();

            $response['success'] = false;
            $response['type'] = 'danger';
            $response['message'] = $e->getMessage();
        }

        return $response;
    }

    /**
     * @param int $language
     * @param int $parentId
     * @param int $backend
     * @return Collection
     */
    private function getNodesWithChildren(int $language, int $backend, $parentId = null) :Collection
    {
        $languageParent = (new Nodes)->getLanguageParent($language)->id();

        $query = $this->table('nodes_closure')
            ->select([
                'nodes.id',
                'nodes.parent_id',
                'nodes_content.title as title',
                'nodes_content.language_id',
                'nodes.in_backend',
                'nodes_closure.depth',
                'nodes_content.homepage'
            ])
            ->join('nodes', 'nodes.id', '=', 'nodes_closure.descendant')
            ->join('nodes_content', 'nodes_content.node_id', '=', 'nodes.id')
            ->where([
                'nodes_content.language_id' => $language,
                'nodes_closure.ancestor' => 1,
                'nodes.parent_id' => empty($parentId) ? $languageParent : $parentId,
                'nodes.in_backend' => (int)$backend
            ])
            ->whereNotIn('nodes_content.content_type_id', [1,4])
            ->groupBy(['nodes.id'])
            ->orderBy('nodes_content.homepage', 'desc')
            ->orderBy('nodes.order', 'asc');

            if (empty($parentId)) {
                $query->orWhere('nodes.id', $languageParent);
            }

        $nodes = $query->withEntities(NodeFactory::class)->get();

        $nodes->transform(function ($node){
            if (!$node->homepage()) {
                $children = $this->getNodesWithChildren($node->language_id(), $node->in_backend(), $node->id());
                $node->setChilds($children);
            }

            return $node;
        });

        return $nodes;
    }
}