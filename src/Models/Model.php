<?php

namespace App\Models;

use Closure;
use App\Facades\DB;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use App\Exceptions\EntityNotFoundException;

class Model
{
    /**
     * @var string
     */
    protected $table;

    /**
     * @var string
     */
    protected $factory;

    /**
     * @var array
     */
    protected $relations = [];


    /**
     * @param int $id
     * @return mixed
     */
    public static function findById(int $id, array $relations = [])
    {
        return (new static)->getEntity(['id' => $id], $relations);
    }

    /**
     * @param array $filters
     * @param array $relations
     * @return mixed
     */
    public static function find(array $filters, array $relations = [])
    {
        return (new static)->getEntity($filters, $relations);
    }

    /**
     * @param $filters
     * @param array $relations
     * @return mixed
     */
    public static function findOrFail($filters, array $relations = [])
    {
        $result = null;

        if (is_array($filters)) {
            $result = static::find($filters, $relations);
        }
        else if (is_numeric($filters)) {
            $result = static::findById($filters, $relations);
        }

        if (!$result || empty($result->id())) {
            throw new EntityNotFoundException($result, Arr::wrap($filters));
        }

        return $result;
    }

    /**
     * @param null $table
     * @return ModelQueryBuilder
     */
    public function table($table = null) :ModelQueryBuilder
    {
        if ($table) {
            $this->table = $table;
        }

        $builder = new ModelQueryBuilder($this);

        return $builder->from($this->table);
    }

    /**
     * @param null $table
     * @return ModelQueryBuilder
     */
    public static function query($table = null) :ModelQueryBuilder
    {
        $model = new static;
        $builder = new ModelQueryBuilder($model);

        return $table ? $builder->from($table) : $builder->from($model->getTable());
    }

    /**
     * @return string
     */
    public function getFactory()
    {
        return $this->factory;
    }

    /**
     * @return array
     */
    public function getRelations()
    {
        return $this->relations;
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param Closure $callback
     * @param int $attempts
     */
    public function transaction(Closure $callback, $attempts = 1)
    {
        DB::transaction($callback, $attempts);
    }

    public function beginTransaction()
    {
        DB::beginTransaction();
    }

    public function commit()
    {
        DB::commit();
    }

    public function rollBack()
    {
        DB::rollBack();
    }

    /**
     * @param array $relations
     * @return array
     */
    public function matchRelations(array $relations) :array
    {
        $modelRelations = $this->getRelations();
        $relationsToSearch = $modelRelations;

        if ($relations !== $modelRelations) {
            $relationsToSearch = [];
            foreach ($relations as $relation) {
                $relationsToSearch[$relation] = $modelRelations[$relation];
            }
        }

        return $relationsToSearch;
    }

    /**
     * @param Collection $collection
     * @param array $relationsToSearch
     * @param Collection $relationsData
     * @return Collection
     */
    public function mapRelationsDataToEntity(Collection $collection, array $relationsToSearch, Collection $relationsData)
    {
        $collection->map(function ($row) use ($relationsToSearch, $relationsData) {
            $row->relations = new \stdClass();

            foreach ($relationsToSearch as $relation => $column) {
                $data = $relationsData->get($relation);

                if (is_null($column)) {
                    $row->relations->{$relation} = $data;
                }
                else {
                    if (!is_null($data) && !is_numeric($relation)) {
                        $row->relations->{$relation} = $data->get($row->{$column});
                    }
                }
            }
        });

        return $collection;
    }

    /**
     * @param mixed
     * @param $factory
     * @return mixed | Collection
     */
    public function entity($rows, string $factory = '')
    {
        $entityFactory = empty($factory) ? $this->factory : $factory;

        if ($rows instanceof Collection && !empty($rows)) {
            $results = collect();
            foreach ($rows as $key => $value) {
                $results->put($key, $entityFactory::build($value));
            }
        }
        else {
            $results = $entityFactory::build($rows);
        }

        return $results;
    }
}