<?php

namespace App\Models\Frontend;

use App\Factories\MenuLinkFactory;
use App\Models\Backend\Menus as BackendMenus;

class Menus extends BackendMenus
{
    /**
     * @param int $menuId
     * @param null $parentId
     * @return array
     */
    public function getMenuLinks(int $menuId, $parentId = null) :array
    {
        $menuLinks = array();

        $query = $this->table('menus_links')
            ->select(['*'])->where(
                [
                    'menu_id' => $menuId,
                    'is_active' => 1
                ])
        ;

        if (is_null($parentId)) {
            $query->whereNull('parent_id');
        }
        else {
            $query->where('parent_id', $parentId);
        }

        $query->orderBy('ordering', 'asc');
        $rows = $query->get();

        foreach ($rows as $row) {
            $results[$row->id] = $row;
            $results[$row->id]->childs = $this->getMenuLinks($menuId, $row->id);
            $menuLinks[] = MenuLinkFactory::build($results[$row->id]);
        }

        return $menuLinks;
    }
}