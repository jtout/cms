<?php

namespace App\Models\Frontend;

use App\Models\Backend\Nodes;

class Sitemap extends Nodes
{
    /**
     * @param null $type
     * @return array
     */
    public function getAllNodes($type = null)
    {
        $relations = array('translations');

        $query = $this->table('nodes AS a')->select([
            'a.id',
            'a.access_level_id',
            'b.path',
            'c.title',
            'c.content_type_id',
            'c.published_on',
            'a.updated_on',
            'd.iso_code AS language_iso_code'
        ])
            ->join('nodes_closure AS b', 'b.descendant', '=', 'a.id')
            ->join('nodes_content AS c', 'c.node_id', '=', 'a.id')
            ->join('languages AS d', 'd.id', '=', 'c.language_id')
            ->where(['b.ancestor' => 1, 'a.is_active' => 1])
            ->groupBy(['a.id'])
            ->orderBy('c.published_on', 'desc')
            ->limit(20000);

        if ($type == 1) {
            $relations[] = 'tags';
        }

        $query->withRelations($relations);

        if (!is_null($type)) {
            $query->where('c.content_type_id', $type);
        }

        return $query->withEntities()->get();
    }
}