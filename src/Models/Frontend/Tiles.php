<?php

namespace App\Models\Frontend;

use App\Models\Backend\Tiles AS BackendTiles;

class Tiles extends BackendTiles
{
    /**
     * @param bool $list
     * @return array
     */
    public function getFrontEndTiles($list = false) :array
    {
        $results = array();

        $rows = $this->table('tiles AS a')->select([
            'a.node_id AS id',
            'a.ordering AS tiles_order',
            'b.parent_id',
            'b.access_level_id',
            'c.video',
            'c.title',
            'c.image',
            'c.published_on',
            'f.path'
        ])
            ->join('nodes_closure AS f', 'f.descendant', '=', 'a.node_id')
            ->join('nodes AS b', 'b.id', '=', 'a.node_id')
            ->join('nodes_content AS c', 'c.node_id', '=', 'b.id')
            ->where('f.ancestor', 1)
            ->groupBy(['a.node_id'])
            ->orderBy('a.ordering')
            ->withRelations(['comments_count'])
            ->get();

        foreach ($rows as $row) {
            if (!$list) {
                if ($row->tiles_order >= 0 && $row->tiles_order <= 2) {
                    $results['small'][] = $this->entity($row);
                }
                else if ($row->tiles_order == 3) {
                    $results['big'][] = $this->entity($row);
                }
                else if ($row->tiles_order == 4) {
                    $results['small_1'][] = $this->entity($row);
                }
                else if ($row->tiles_order == 5) {
                    $results['small_2'][] = $this->entity($row);
                }
            }
            else {
                $results['list'][] = $this->entity($row);
            }
        }

        return $results;
    }
}