<?php

namespace App\Models\Frontend;

use App\Traits\App;
use App\Facades\Session;
use Illuminate\Support\Arr;
use App\Entities\NodeEntity;
use Illuminate\Support\Collection;
use App\Models\Backend\Nodes as BackendNodes;
use Illuminate\Pagination\LengthAwarePaginator;

class Nodes extends BackendNodes
{
    /**
     * @param NodeEntity $node
     * @param int $type
     * @return LengthAwarePaginator
     */
    public function getList(NodeEntity $node, int $type) :LengthAwarePaginator
    {
        $query = $this->table('nodes_closure AS a')->select([
            'b.id',
            'b.parent_id',
            'b.created_by',
            'b.access_level_id',
            'c.title',
            'c.image',
            'c.sponsored_by_id',
            'c.published_on',
            'c.intro_text',
            'c.description',
            'c.is_sticky',
            'c.is_featured',
            'c.content_type_id',
            'c.video',
            'd.title AS access_level',
            'd.label AS access_level_label',
            'a.path',
            'f.media_folder AS content_type_media_folder'
        ])
            ->join('nodes AS b', 'b.id', '=', 'a.descendant')
            ->join('nodes_content AS c', 'c.node_id', '=', 'b.id')
            ->join('access_levels AS d', 'd.id', '=', 'b.access_level_id')
            ->join('content_types AS f', 'f.id', '=', 'c.content_type_id')
            ->where([
                'a.ancestor' => 1,
                'c.content_type_id' => $type,
                'b.in_backend' => 0,
                'b.is_active' => 1,
                'c.language_id' => $node->language_id()
            ])
            ->groupBy(['b.id'])
            ->orderBy('c.is_sticky', 'desc')
            ->orderBy('c.published_on', 'desc');

        if ($node->content_type_id() == 2) {
            $query->where('a.path', 'like', $node->path(false).'%');
        }
        else if ($node->content_type_id() == 4) {
            $query->join('nodes_tags AS i', 'i.node_id', '=', 'b.id');
            $query->where('i.tag_id', $node->id());
        }

        if ($node->name() == 'search') {
            $query->where('c.title', 'like', '%'.Session::get('search.q').'%');
        }

        return $query
            ->withRelations(['author', 'parent', 'comments_count', 'sponsored_by'])
            ->paginate();
    }

    /**
     * @param int $language
     * @param int $type
     * @param array $parents
     * @return LengthAwarePaginator
     */
    public function getRecentNodes(int $language, int $type, bool $onTiles = false, array $parents = []) :LengthAwarePaginator
    {
        $query = $this->table('nodes AS a')->select([
            'b.path',
            'a.id',
            'a.parent_id',
            'a.access_level_id',
            'c.video',
            'c.title',
            'c.intro_text',
            'c.image',
            'c.is_featured',
            'c.is_sticky',
            'c.published_on',
            'c.finished_on',
            'c.intro_text',
            'c.sponsored_by_id',
            'c.content_type_id',
            'a.created_by'
        ])
            ->join('nodes_closure AS b', 'b.descendant', '=', 'a.id')
            ->join('nodes_content AS c', 'c.node_id', '=', 'a.id')
            ->where([
                'b.ancestor' => 1,
                'a.is_active' => 1,
                'c.content_type_id' => $type,
                'c.language_id' => $language
            ])

            ->groupBy(['a.id'])
            ->orderByRaw('c.is_sticky DESC, c.published_on DESC');

        if ($onTiles) {
            $tiles = $this->table('tiles')->pluck('node_id')->toArray();
            $query->whereNotIn('a.id', $tiles);
        }

        if (count($parents) > 0) {
            $query->whereIn('a.parent_id', $parents);
        }

        return $query->withRelations(['parent', 'comments_count', 'access_level'])->paginate();
    }

    /**
     * @param int $tagId
     * @param int $language
     * @param int $limit
     * @param int $nodeId
     * @return Collection
     */
    public function getRelatedNodes(int $tagId, int $language, int $limit, int $nodeId = 0) :Collection
    {
        $query = $this->table('nodes_tags AS a')->select([
            'b.id',
            'b.access_level_id',
            'c.path',
            'd.title',
            'd.image',
            'd.language_id',
            'd.published_on'
        ])
            ->join('nodes AS b', 'b.id', '=', 'a.node_id')
            ->join('nodes_closure AS c', 'c.descendant', '=', 'b.id')
            ->join('nodes_content AS d', 'd.node_id', '=', 'a.node_id')
            ->where([
                'c.ancestor' => 1,
                'b.is_active' => 1,
                'a.tag_id' => $tagId,
                'd.language_id' => $language
            ])
            ->groupBy(['b.id'])
            ->orderBy('b.created_on', 'desc')
            ->limit($limit);

        if (!empty($nodeId)) {
            $query->where('b.id', '<>', $nodeId);
        }

        return $query->withEntities()->get();
    }

    /**
     * @param int $language
     * @param array $categories
     * @param int $limit
     * @return Collection
     */
    public function getFeaturedNodes(int $language, array $categories, int $limit) :Collection
    {
        $query = $this->table('nodes AS a')->select([
            'a.id',
            'a.access_level_id',
            'b.path',
            'c.title',
            'c.content_type_id',
            'c.image',
            'c.intro_text',
            'c.published_on'
        ])
            ->join('nodes_closure AS b', 'b.descendant', '=', 'a.id')
            ->join('nodes_content AS c', 'c.node_id', '=', 'a.id')
            ->leftJoin('tiles AS d', 'd.node_id', '=', 'a.id')
            ->whereNull('d.node_id')
            ->whereIn('a.parent_id', Arr::flatten($categories))
            ->where([
                'b.ancestor' => 1,
                'a.is_active' => 1,
                'c.language_id' => $language,
            ])
            ->orderBy('c.published_on', 'desc')
            ->limit($limit);

        return $this->entity($query->get());
    }

    /**
     * @param $position
     * @param $id
     * @param $parentId
     * @return NodeEntity
     */
    public function getCloserArticle($position, $id, $parentId)
    {
        if ($position == 'next') {
            $order = 'a.id ASC';
            $operator = '>';
        }
        else {
            $order = 'a.id DESC';
            $operator = '<';
        }

        $accessLevels = App::sanitizeInput(user()->access_level(), 'STRING');
        $node = self::getEntityQuery()
            ->where([
                'c.content_type_id' => 1,
                'a.parent_id' => $parentId,
            ])
            ->whereIn('a.access_level_id', explode(',', $accessLevels))
            ->where('c.published_on', '<=', now())
            ->where('a.id', $operator, $id)
            ->orderByRaw($order)
            ->limit(1)
            ->withEntities()
            ->withRelations(['author', 'tags'])
            ->first();

        if (is_null($node)) {
            return new NodeEntity();
        }

        return $node;
    }
}