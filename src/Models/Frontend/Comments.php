<?php

namespace App\Models\Frontend;

use App\Entities\NodeEntity;
use App\Models\Backend\Users;
use App\Entities\CommentEntity;
use App\Models\Backend\Comments as BackendCommentsModel;

class Comments extends BackendCommentsModel
{
    /**
     * @param NodeEntity $article
     * @param string $order
     * @return array
     */
    public function loadComments(NodeEntity $article, string $order) :array
    {
        $comments = $this->getComments($article, $order, false);

        if (isset($comments['list']) && !empty($comments['list'])) {
            $comments['html'] 	= '';
            $comments['html']  .= '<ul id="comments-'.$article->id().'" class="comments-list">';

            foreach($comments['list'] as $comment) {
                $comments['html'] .= $this->createCommentsTree($article, $comment, $order);
            }

            $comments['html'] .= '</ul>';
        }

        $comments['count'] = $this->getCommentsCount($article->id());

        return $comments;
    }

    /**
     * @param CommentEntity $comment
     * @return string
     */
    public function likeUnlikeComment(CommentEntity $comment) :string
    {
        if ($comment->is_liked()) {
            $this->table('comments_likes')->where([
                'comment_id' => $comment->id(),
                'user_id' => user()->id(),
                'node_id' => $comment->node_id()
            ])->delete();
        }
        else {
            $this->table('comments_likes')->insert([
                'comment_id' => $comment->id(),
                'user_id' => user()->id(),
                'node_id' => $comment->node_id()
            ]);
        }

        $comment->setLikesCount($this->getLikesCount($comment->id()));

        return $this->createCommentDiv($comment);
    }

    /**
     * @param int $id
     * @param bool $pluckIds
     * @return array
     */
    public function getLikers(int $id, bool $pluckIds = false) :array
    {
        $results = array();
        $rows = $this->table('comments_likes')
            ->select(['*'])->where('comment_id', $id)->get();

        foreach ($rows as $row) {
            if ($pluckIds) {
                $results[$row->user_id] = $row->user_id;
            }
            else {
                $results[$row->user_id] = Users::find(['id' => $row->user_id], ['fields']);
            }
        }

        return $results;
    }

    /**
     * @param null $nodeId
     * @param null $parentId
     * @return int
     */
    public function getCommentsCount($nodeId = null, $parentId = null) :int
    {
        $query = $this->table('comments')
            ->where('is_active', 1)
            ->groupBy(['node_id']);

        if (!is_null($nodeId)) {
            $query->where('node_id', $nodeId);
        }

        if (!is_null($parentId)) {
            $query->where('parent_id', $parentId);
        }

        return $query->count('id');
    }

    /**
     * @param int $commentId
     * @return int
     */
    public function getLikesCount(int $commentId) :int
    {
        return $this->table('comments_likes')
            ->where('comment_id', $commentId)->count('comment_id');
    }

    /**
     * @param NodeEntity $article
     * @param CommentEntity $comment
     * @param string $order
     * @return string
     */
    private function createCommentsTree(NodeEntity $article, CommentEntity $comment, string $order) :string
    {
        $childs = $this->getComments($article, 'a.created_on ASC', $comment->id());
        $html   = '';

        if (empty($childs['list'])) {
            $html .= '<li class="comment">'.$this->createCommentDiv($comment).'</li>';
        }
        else {
            $html .= '<li class="comment">'.$this->createCommentDiv($comment).'';

            $html .= '		<ul id="children-'.($comment->parent_id() == null ? $comment->id() : $comment->parent_id()).'" class="children">';

            foreach($childs['list'] as $child)
                $html .= $this->createCommentsTree($article, $child, $order);

            $html .= '		</ul>';
            $html .= '</li>';
        }

        return $html;
    }

    /**
     * @param CommentEntity $comment
     * @return string
     */
    private function createCommentDiv(CommentEntity $comment) :string
    {
        $info['comment'] = $comment;
        $data = view('Frontend/Comments/comments.view', $info)->raw();

        return $data;
    }

    /**
     * @param NodeEntity $article
     * @param string $order
     * @param bool $parentId
     * @return array
     */
    private function getComments(NodeEntity $article, string $order, $parentId = false) :array
    {
        $results = array();

        $query = $this->table('comments AS a')->select(['a.*', 'b.title']);
        $countSubQuery = $this->table('comments_likes')
            ->selectRaw('COUNT(comment_id)')
            ->whereColumn('comment_id', 'a.id');

        $query->selectSub($countSubQuery, 'likes_count')
            ->join('nodes_content AS b', 'b.node_id', '=', 'a.node_id')
            ->join('users AS c', 'c.id', '=', 'a.created_by')
            ->where([
                'a.is_active' => 1,
                'a.node_id' => $article->id(),
                'c.is_enabled' => 1
            ])
            ->orderByRaw($order);

        if ($parentId === false) {
            $query->whereNull('a.parent_id');
        }
        else {
            $query->where('a.parent_id', $parentId);
        }

        $rows = $query->paginate();

        $results['list'] = $rows->items();
        $results['paginate'] = $rows->onlyNextPagination;

        return $results;
    }
}