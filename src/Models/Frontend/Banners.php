<?php

namespace App\Models\Frontend;

use App\Models\Backend\Banners as BackendBanners;

class Banners extends BackendBanners
{
    /**
     * @param int $nodeToSearch
     * @param int $nodeContentType
     * @return array
     */
    public function getActiveBanners(int $nodeToSearch, int $nodeContentType) :array
    {
        $results = array();

        $query = $this->table('banners AS a')->select([
            'a.id',
            'a.title',
            'a.alias',
            'a.click_url',
            'a.type',
            'a.field',
            'a.mobile_field',
            'a.settings',
            'a.position_id',
            'a.ordering',
            'a.published_on',
            'a.finished_on',
            'b.title AS position_title',
            'b.alias AS position_alias'
        ]);

        $nodesSubQuery = $this->table('banners_nodes')
            ->selectRaw('GROUP_CONCAT(node_id)')
            ->whereColumn('banner_id','=', 'a.id');

        $query->selectSub($nodesSubQuery, 'nodes')
            ->join('banners_positions AS b', 'b.id', '=', 'a.position_id')
            ->join('banners_nodes AS c', 'c.banner_id', '=', 'a.id')
            ->where('a.is_active', 1)
            ->groupBy(['a.id'])
            ->orderBy('ordering', 'asc');

        $rows = $query->get();

        foreach ($rows as $row) {
            if ($nodeContentType == 4) {
                $results[$row->position_alias][] = $this->entity($row);
            }
            else {
                if (in_array($nodeToSearch, explode(',', $row->nodes))) {
                    $results[$row->position_alias][] = $this->entity($row);
                }
            }
        }

        return $results;
   }
}