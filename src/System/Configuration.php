<?php

namespace App\System;

use Illuminate\Support\Arr;
use App\Interfaces\Singleton;
use App\Entities\SettingsEntity;
use App\Factories\SettingsFactory;

final class Configuration implements Singleton
{
    /**
     * @var SettingsEntity
     */
    private $settings;

    /**
     * @var null|Configuration
     */
    private static $instance = NULL;

    /**
     * Configuration constructor.
     */
    private function __construct()
    {
        $this->settings = $this->settings();

        ini_set('session.cookie_httponly', true);
        ini_set('session.use_cookies', true);
        ini_set('session.use_only_cookies', true);
        ini_set('session.use_strict_mode', true);

        date_default_timezone_set('Europe/Athens');
        setlocale(LC_MONETARY, 'el_GR.UTF-8');
        mb_internal_encoding('UTF-8');
        mb_regex_encoding('UTF-8');
        mb_http_output('UTF-8');

        defined('DIR')              || define('DIR',            app_dir());
        defined('SITENAME')         || define('SITENAME',       $this->get('site_name'));
        defined('LANGUAGE')         || define('LANGUAGE',       $this->get('language'));
        defined('LANGUAGES')        || define('LANGUAGES',      $this->get('languages'));
        defined('SITEOFFLINE')      || define('SITEOFFLINE',    $this->get('site_offline'));
        defined('LOGO_WHITE')       || define('LOGO_WHITE',     $this->get('logo_white'));
        defined('LOGO_BLACK')       || define('LOGO_BLACK',     $this->get('logo_black'));
    }

    /**
     * @return Configuration
     */
    public static function initialize() :Configuration
    {
        if (self::$instance == NULL) {
            self::$instance = new Configuration();
        }

        return self::$instance;
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public function get($name)
    {
        return Arr::get($this->settings->toArray(), $name);
    }

    /**
     * @return SettingsEntity
     */
    public function settings() :SettingsEntity
    {
        $config = file_get_contents(app_dir().'/config.json');
        $settings = json_decode($config, 1);

        return SettingsFactory::build($settings);
    }
}