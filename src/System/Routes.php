<?php

namespace App\System;

use App\Facades\App;

final class Routes
{
    public static function register()
    {
        App::get('/[{optional}[/{path:.*}]]', '\App\Controllers\Controller:processRequest');
        App::post('/[{optional}[/{path:.*}]]', '\App\Controllers\Controller:processRequest');
    }
}