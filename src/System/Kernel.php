<?php

namespace App\System;

use Slim\App;
use Dotenv\Dotenv;
use App\Facades\Facade;
use App\Facades\Container;
use App\Middleware\Middleware;
use Slim\Container as SlimContainer;
use Psr\Http\Message\ResponseInterface as Response;

final class Kernel
{
    /**
     * @return Response
     * @throws \Slim\Exception\NotFoundException
     * @throws \Slim\Exception\MethodNotAllowedException
     */
    public static function boot() :Response
    {
        $container = new SlimContainer([
            'settings' => [
                'displayErrorDetails' => true,
                'determineRouteBeforeAppMiddleware' => true
            ]
        ]);

        $app = new App($container);
        Facade::setFacadeApplication($app);
        Dotenv::create(app_dir())->load();
        Configuration::initialize();

        Kernel::registerServiceProviders();
        Routes::register();
        Middleware::boot();

        return $app->run();
    }

    public static function registerServiceProviders()
    {
        $serviceProviders = [
            // Core app service providers.
            \App\Providers\DatabaseServiceProvider::class,
            \App\Providers\SessionServiceProvider::class,
            \App\Providers\CookiesServiceProvider::class,
            \App\Providers\CacheServiceProvider::class,
            \App\Providers\CsrfServiceProvider::class,
            \App\Providers\FlashServiceProvider::class,
            \App\Providers\ViewServiceProvider::class,
            \App\Providers\ErrorHandlerServiceProvider::class,
            \App\Providers\MailServiceProvider::class,
            \App\Providers\AuthServiceProvider::class,
            \App\Providers\RequestServiceProvider::class,
            \App\Providers\RedirectResponseServiceProvider::class,
            \App\Providers\ValidatorServiceProvider::class,

            // Custom service providers
            \App\Providers\AssetsCompressionServiceProvider::class,
            \App\Providers\DetectAttacksServiceProvider::class,
            \App\Providers\JoomlaServiceProvider::class
        ];

        foreach ($serviceProviders as $serviceProvider) {
            Container::register(new $serviceProvider());
        }
    }
}