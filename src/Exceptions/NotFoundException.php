<?php

namespace App\Exceptions;

use Throwable;

class NotFoundException extends AppException
{
    /**
     * @var string
     */
    protected $view = '404';

    /**
     * @var string
     */
    protected $message = 'Page Not Found';

    /**
     * NotFoundException constructor.
     * @param string $message
     * @param Throwable|null $previous
     */
    public function __construct($message = "", Throwable $previous = null)
    {
        parent::__construct(404, $message, $previous);

        $this->data->put('csrf', csrf()->generateToken());
    }
}