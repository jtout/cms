<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Support\Arr;
use App\Interfaces\Exception;
use Illuminate\Support\Collection;

class AppException extends \RuntimeException implements Exception
{
    /**
     * @var string
     */
    protected $view ;

    /**
     * @var Collection
     */
    protected $data;

    /**
     * @var string
     */
    protected $message = SITENAME.' Error! Please get in contact with an administrator!';

    /**
     * AppException constructor.
     * @param int $code
     * @param string $message
     * @param Throwable|null $previous
     */
    public function __construct($code = 0, $message = "", Throwable $previous = null)
    {
        $message = !empty($message) ? $message : $this->message;

        parent::__construct($message, $code, $previous);

        if (empty($this->data)) {
            $this->data = new Collection();
        }

        $this->data->put('errors', Arr::wrap($message));
        $this->data->put('success', false);
    }

    /**
     * @return string
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data->toArray();
    }
}