<?php

namespace App\Exceptions;

use Throwable;

class MethodNotFoundException extends AppException
{
    /**
     * MethodNotFoundException constructor.
     * @param $module
     * @param $action
     */
    public function __construct($module, $action, Throwable $previous = null)
    {
        if (user()->in_backend() == 1) {
            $this->message = $module .'::'.$action.' class method is not available!';
        }

        parent::__construct(404, $this->message, $previous);
    }
}