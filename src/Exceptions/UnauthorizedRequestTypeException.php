<?php

namespace App\Exceptions;

use Throwable;

class UnauthorizedRequestTypeException extends AppException
{
    /**
     * @var string
     */
    protected $view = '401';

    /**
     * @var string
     */
    protected $message = 'Unauthorized Request Type!';

    /**
     * UnauthorizedAccessException constructor.
     * @param string $message
     * @param Throwable|null $previous
     */
    public function __construct($message = "", Throwable $previous = null)
    {
        parent::__construct(401, $message, $previous);
    }
}