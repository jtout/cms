<?php

namespace App\Exceptions;

use Illuminate\Support\Arr;

class RequestNotValidException extends AppException
{
    /**
     * RequestNotValidException constructor.
     * @param null $messages
     */
    public function __construct($messages = null)
    {
        parent::__construct(422, 'Invalid data!');

        if (!is_null($messages)) {
            $this->data->put('errors', Arr::wrap($messages));
        }
    }
}