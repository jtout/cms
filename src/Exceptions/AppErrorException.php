<?php

namespace App\Exceptions;

use Throwable;

class AppErrorException extends AppException
{
    /**
     * @var string
     */
    protected $view = '500';

    /**
     * AppErrorException constructor.
     * @param string $message
     * @param Throwable|null $previous
     */
    public function __construct($message = "", Throwable $previous = null)
    {
        parent::__construct(500, $message, $previous);
    }
}