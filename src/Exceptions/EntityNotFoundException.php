<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Support\Arr;

class EntityNotFoundException extends AppException
{
    /**
     * @var string
     */
    protected $message = 'Entity Not Found';

    /**
     * EntityNotFoundException constructor.
     * @param null $object
     * @param array $filters
     * @param Throwable|null $previous
     */
    public function __construct($object = null, array $filters = [], Throwable $previous = null)
    {
        if (is_null($object)) {
            $this->message .= ' - Requested: null';
        }
        else {
            $id = is_array($filters) ? Arr::get($filters, 'id') : $filters;
            $this->message .= ' - Requested: '.get_class($object).' ['.$id.']';
        }

        parent::__construct(404, $this->message, $previous);
    }
}