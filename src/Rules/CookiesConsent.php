<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CookiesConsent implements Rule
{
    /**
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (empty($value)) {
            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function message()
    {
        return 'You have to accept our cookies policy in order to proceed!';
    }
}