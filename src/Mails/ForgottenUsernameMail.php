<?php

namespace App\Mails;

use App\Entities\MailEntity;

class ForgottenUsernameMail extends MailEntity
{
    /**
     * ForgottenUsernameMail constructor.
     * @param string $email
     * @param string $username
     */
    public function __construct(string $email, string $username)
    {
        $data['username'] = $username;

        $this->setTo($email);
        $this->setSubject(SITENAME.' - Forgotten username');
        $this->setMailData($data);
        $this->setTemplate('Frontend/Mails/forgotten_username');
    }
}