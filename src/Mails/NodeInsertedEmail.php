<?php

namespace App\Mails;

use App\Entities\MailEntity;

class NodeInsertedEmail extends MailEntity
{
    /**
     * NodeInsertedEmail constructor.
     * @param int $id
     * @param string $name
     * @param int $contentTypeId
     */
    public function __construct(int $id, string $name, int $contentTypeId)
    {
        $data['message'] = 'New Node inserted with id: '.$id.', name: '.$name.', content type id: '.$contentTypeId;

        $this->setTo(config()->get('system_administrator_email'));
        $this->setSubject(SITENAME.' Node Inserted Notifications');
        $this->setMailData($data);
        $this->setTemplate('Backend/Mails/node_inserted');
    }
}