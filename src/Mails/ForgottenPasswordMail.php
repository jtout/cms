<?php

namespace App\Mails;

use App\Entities\MailEntity;

class ForgottenPasswordMail extends MailEntity
{
    /**
     * ForgottenPasswordMail constructor.
     * @param string $email
     * @param string $password
     */
    public function __construct(string $email, string $password)
    {
        $data['password'] = $password;

        $this->setTo($email);
        $this->setSubject(SITENAME.' - Forgotten password');
        $this->setMailData($data);
        $this->setTemplate('Frontend/Mails/forgotten_password');
    }
}