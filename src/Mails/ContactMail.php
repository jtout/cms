<?php

namespace App\Mails;

use App\Entities\MailEntity;

class ContactMail extends MailEntity
{
    /**
     * ContactMail constructor.
     * @param array $mailData
     */
    public function __construct(array $mailData)
    {
        $this->setTo('contact@cms.com');
        $this->setSubject($mailData['subject']);
        $this->setMailData($mailData);
        $this->setTemplate('Frontend/Mails/contact');
    }
}