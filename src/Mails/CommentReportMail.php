<?php

namespace App\Mails;

use App\Entities\MailEntity;
use App\Entities\UserEntity;

class CommentReportMail extends MailEntity
{
    /**
     * CommentReportMail constructor.
     * @param UserEntity $user
     * @param array $data
     */
    public function __construct(UserEntity $user, array $data)
    {
        $data['user']  = user()->username();

        $this->setTo($user->email());
        $this->setSubject(SITENAME.' - Comment Report');
        $this->setMailData($data);
        $this->setTemplate('Frontend/Mails/comment_report');
    }
}