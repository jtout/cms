<?php

namespace App\Mails;

use App\Entities\MailEntity;
use App\Entities\UserEntity;

class RegistrationMail extends MailEntity
{
    /**
     * RegistrationMail constructor.
     * @param UserEntity $userEntity
     */
    public function __construct(UserEntity $userEntity)
    {
        $data['user']  = $userEntity;

        $this->setTo($userEntity->email());
        $this->setSubject(SITENAME.' - Registration');
        $this->setMailData($data);
        $this->setTemplate('Frontend/Mails/registration');
    }
}