<?php

namespace App\Mails;

use App\Entities\MailEntity;

class LoginMail extends MailEntity
{
    /**
     * LoginMail constructor.
     * @param string $message
     */
    public function __construct(string $message)
    {
        $data['message'] = $message;
        $data['info'] = file_get_contents('http://api.ipstack.com/'.get_user_info_by_ip()->ip.'?access_key='.env('IPSTACKKEY'));

        $this->setTo(config()->get('system_administrator_email'));
        $this->setSubject(SITENAME.' Login Notifications');
        $this->setMailData($data);
        $this->setTemplate('Backend/Mails/login_notification');
    }
}