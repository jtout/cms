<?php

namespace App\Controllers\Frontend;

use App\Traits\App;
use App\Facades\Auth;
use App\Facades\Mail;
use App\Facades\Cache;
use App\Mails\LoginMail;
use App\Facades\Cookies;
use App\Facades\Session;
use Illuminate\Support\Arr;
use App\Interfaces\Request;
use App\Entities\UserEntity;
use App\Models\Backend\Users;
use App\Rules\CookiesConsent;
use App\Rules\GoogleRecaptcha;
use App\Models\Frontend\Nodes;
use App\Controllers\Controller;
use App\Requests\UserRegistration;
use App\Mails\ForgottenUsernameMail;
use App\Mails\ForgottenPasswordMail;
use App\Models\Frontend\Site as SiteModel;
use App\Requests\UserFacebookRegistration;
use \Psr\Http\Message\ResponseInterface as Response;

class Site extends Controller
{

    /**
     * @var SiteModel
     */
    private $model;

    /**
     * Site constructor.
     */
    public function __construct()
    {
        $this->model = new SiteModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function login(Request $request) :Response
    {
        $request->set(['cookies_consent' => Cookies::get(SITENAME.'_cookies_consent')]);

        $this->validate($request->inputs(),
            [
                'cookies_consent' => new CookiesConsent,
                'token' => ['required', new GoogleRecaptcha],
                'username' => 'required',
                'password' => 'required'
            ],
            [
                'token.required' => 'Please try again, Google Recaptcha token is missing!',
                'username.required' => 'Username is required!',
                'password.required' => 'Password is required'
            ]
        );

        $data = $request->inputs();
        $data['in_backend'] = 0;

        $login = Auth::login($data);

        if ($login === true) {
            if (user()->can_access_backend()) {
                $mailEntity = new LoginMail(user()->username(). ' logged in to frontend!');
                Mail::send($mailEntity);
            }

            return redirect(env('DOMAIN'));
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function logout(Request $request) :Response
    {
        Auth::logout();

        return redirect(env('DOMAIN'));
    }

    /**
     * @param UserRegistration $request
     * @return Response
     */
    public function register(UserRegistration $request) :Response
    {
        $userEntity = new UserEntity();
        $userEntity->process($request->getValidatedData());

        try {
            $userEntity->insert();
        } catch (\Exception $e) {
            flash_message('danger', 'There was a problem with your registration. Please contact
                    us through the <a href="'.env('DOMAIN').'/contact-us">contact form</a>.');
        }

        return redirect(env('DOMAIN'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function activateUser(Request $request)
    {
        $token = $request->inputs('token');

        if (empty($token)) {
            flash_message('danger', 'Activation token does not exist');
            return redirect(env('DOMAIN'));
        }

        $user = Users::find(['activation_token' => $token], ['*']);

        if (empty($user->id())) {
            flash_message('danger', 'Token does not match a registered user!');
            return redirect(env('DOMAIN'));
        }

        if ($user->is_activated()) {
            flash_message('warning', 'User already activated. Please login with your credentials.');
            return redirect(env('DOMAIN'));
        }

        $user->setIsActivated(1);
        $user->setIsEnabled(1);
        $user->setActivatedOn(now());
        $user->setUpdatedBy(1);
        $user->update();
        Auth::setUserSession($user);

        flash_message('success', 'Your account has been activated and you were automatically logged in!');

        return redirect(env('DOMAIN'));
    }

    /**
     * @param UserFacebookRegistration $request
     * @return Response
     */
    public function registerFacebookUser(UserFacebookRegistration $request) :Response
    {
        $data = $request->getValidatedData();
        $userExists = Users::find(['email' => $data['email']], ['*']);

        if (empty($userExists->id())) {
            $userEntity = new UserEntity();
            $userEntity->process($data);

            try {
                $userEntity->insert();
                $userEntity = $userEntity->reload();

                Auth::setUserSession($userEntity);
            } catch (\Exception $e) {
                flash_message('danger', 'There was a problem with your registration. Please contact
                    us through the <a href="'.env('DOMAIN').'/contact-us">contact form</a>.');
            }
        }
        else {
            $userExists->setName($data['name']);
            $userExists->setLastName($data['last_name']);
            $userExists->setAvatar($data['avatar']);
            $userExists->update();

            Auth::setUserSession($userExists);
        }

        return redirect(env('DOMAIN'));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function managePrivacyPolicy(Request $request) :Response
    {
        $data = $request->inputs();

        $this->validate($data,
            [
                'token' => ['required', new GoogleRecaptcha],
                'privacy_policy' => 'required|not_in:0'
            ],
            ['privacy_policy.required' => 'You did not accept our privacy policy!']
        );

        $id = App::sanitizeInput(user()->id(), 'INT');
        $userEntity = Users::findById($id, ['*']);
        $userEntity->setPrivacyPolicy((int) 1);
        $userEntity->setUpdatedBy($id);
        $userEntity->update();

        Auth::setUserSession($userEntity);
        flash_message('success', 'You accepted our privacy policy!');

        return redirect(env('DOMAIN'));
    }

    public function manageDarkMode(Request $request)
    {
        $darkMode = $request->inputs('dark_mode');
        Session::set('user.dark_mode', $darkMode);

        if (!empty(Cookies::get(SITENAME.'_cookies_consent'))) {
            Cookies::set(SITENAME.'_dark_mode', $darkMode);
        }

        $responseData = [
            'success' => true,
            'dark-mode' => $darkMode,
            'logo' => $darkMode == 1 ? LOGO_WHITE : LOGO_BLACK
        ];

        return response()->withJson($responseData, 200);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function acceptCookies(Request $request) :Response
    {
        Cookies::set(SITENAME.'_cookies_consent', true, time()+2592000);
        flash_message('info', 'You accepted our cookies policy');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function declineCookies(Request $request) :Response
    {
        Cookies::delete(SITENAME.'_cookies_consent');
        flash_message('info', 'You declined our cookies policy');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function forgottenAction(Request $request) :Response
    {
        $email = $request->inputs('email', 'EMAIL');
        $forgotten = $request->inputs('forgotten');

        if ($email == false) {
            flash_message('warning', 'Email is required!');
            return redirect()->back();
        }

        $user = Users::find(['email' => $email]);

        if (!empty($user->id())) {
            $users = new Users();

            switch ($forgotten) {
                case 'username':
                    $username = $users->resetAction('username', $email);
                    $mailEntity = new ForgottenUsernameMail($email, $username);
                    Mail::send($mailEntity);

                    flash_message('success', 'We sent you an email with your username!');
                    break;

                case 'password':
                    $password = $users->resetAction('password', $email);
                    $mailEntity = new ForgottenPasswordMail($email, $password);
                    Mail::send($mailEntity);

                    flash_message('success', 'Password reset successful! We sent you an email 
                            with instructions so you can log in.');
                    break;

                default:
                    flash_message('danger', 'Invalid action. Please try again!');
                    return redirect()->back();
                    break;
            }
        }
        else {
            flash_message('warning', 'This email does not exist in our database!');
            return redirect($this->node()->path());
        }

        return redirect(env('DOMAIN'));
    }
}