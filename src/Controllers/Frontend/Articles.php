<?php

namespace App\Controllers\Frontend;

use App\Interfaces\Request;
use App\Interfaces\Jsonable;
use App\Facades\AssetsCompression;
use \Psr\Http\Message\ResponseInterface as Response;

class Articles extends Nodes implements Jsonable
{
    /**
     * @param Request $request
     * @return Response
     */
    public function viewArticle(Request $request) :Response
    {
        if ($request->inputs('format') == 'json') {
            return $this->toJson();
        }

        AssetsCompression::minifyAndCombineArticlesAssets();

        return view($this->node()->view())->render();
    }

    /**
     * @return Response
     */
    public function toJson() :Response
    {
        $node = $this->node();
        $current['node'] = $node;
        $result['current']['html'] = view($node->view(), $current)->raw();

        if ($node->next_node()->id()) {
            $result['next']['title'] = $node->next_node()->title(true);
            $result['next']['path'] = $node->next_node()->path(true, true);
        }

        if ($node->previous_node()->id()) {
            $prev['node'] = $node->previous_node();
            $result['prev']['html'] = view('Frontend/Articles/next-article.view', $prev)->raw();
            $result['prev']['title'] = $node->previous_node()->title(true);
            $result['prev']['path'] = $node->previous_node()->path(true, true);
        }

        return response()->withJson($result, 200);
    }
}