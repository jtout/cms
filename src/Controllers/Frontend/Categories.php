<?php

namespace App\Controllers\Frontend;

use App\Traits\App;
use App\Facades\Cache;
use Illuminate\Support\Arr;
use App\Interfaces\Request;
use App\Interfaces\Jsonable;
use App\Interfaces\RssFormat;
use App\Models\Frontend\Tiles;
use \Psr\Http\Message\ResponseInterface as Response;

class Categories extends Nodes implements Jsonable, RssFormat
{
    /**
     * var array
     */
    protected $articles;

    /**
     * @param Request $request
     * @return Response
     */
    public function viewCategory(Request $request) :Response
    {
        $page = session()->get('page');
        $key = $request->getParsedUri();
        $cachedData = Cache::get($key);

        $articles = Arr::get($cachedData, 'articles.raw.'.$page);

        if (empty($articles)) {
            $articles = $this->model->getList($cachedData['node'], $type = 1);

            Arr::set($cachedData, 'articles.raw.'.$page, $articles);
            Cache::set($key, $cachedData);
        }

        $data['articles'] = $articles;

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @return Response
     */
    public function toJson() :Response
    {
        $results = array();

        foreach ($this->articles as $article) {
            if ($article->is_visible()) {
                $results[$article->id()]['articleId'] = $article->id();
                $results[$article->id()]['categoryId'] = $article->parent_id();
                $results[$article->id()]['categoryName'] = $article->parent()->title();
                $results[$article->id()]['articleTitle'] = $article->title();
                $results[$article->id()]['articleIntro'] = strip_tags($article->intro_text());
                $results[$article->id()]['articleImage'] = $article->image(true, 'Articles', 'recentNewsImage');
                $results[$article->id()]['path'] = $article->path(true);
                $results[$article->id()]['commentCount'] = $article->comments_count();
            }
        }

        return response()->withJson($results, 200);
    }

    /**
     * @return Response
     */
    public function toRss() :Response
    {
        $xml = new \SimpleXMLElement('<rss/>');
        $xml->addAttribute("version", "2.0");

        $channel = $xml->addChild("channel");
        $channel->addChild("title", SITENAME.' '.$this->node()->title(true, true));
        $channel->addChild("link", $this->node()->path(true));
        $channel->addChild("description", $this->node()->meta_description(true));
        $channel->addChild("language", $this->node()->language_iso_code());

        foreach ($this->articles as $article) {
            if($article->is_visible()) {
                $item = $channel->addChild("item");

                $item->addChild("title", $article->title(true, true));
                $item->addChild("link", $article->path(true));
                $item->addChild("description", strip_tags($article->intro_text(true)));
                $item->addChild("pubDate", date(DATE_RSS, strtotime($article->published_on())));
                $item->addChild("guid", $article->path(true));
            }
        }

        return response()->write($xml->asXML())
            ->withHeader('Content-Type', 'application/rss+xml; charset=UTF-8');
    }
}