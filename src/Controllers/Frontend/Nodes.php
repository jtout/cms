<?php

namespace App\Controllers\FrontEnd;

use App\Controllers\Controller;
use App\Models\Frontend\Nodes as NodesModel;

class Nodes extends Controller
{
    /**
     * @var NodesModel
     */
    protected $model;

    /**
     * Nodes constructor.
     */
    public function __construct()
    {
        $this->model = new NodesModel();
    }
}