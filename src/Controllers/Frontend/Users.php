<?php

namespace App\Controllers\Frontend;

use App\Traits\App;
use App\Traits\Media;
use App\Facades\Auth;
use App\Interfaces\Request;
use App\Requests\UpdateSiteProfile;
use App\Controllers\Backend\Profile;
use \App\Models\Backend\Users as UsersModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Users extends Profile
{
    use Media;

    /**
     * @param Request $request
     * @return Response
     */
    public function viewUser(Request $request) :Response
    {
        $node = $this->node();
        $data['node'] = $node;

        if ($node->name() == 'users' && empty($request->inputs('profile'))) {
            return redirect(env('DOMAIN'));
        }

        if ($request->inputs('profile')) {
            $username = $request->inputs('profile');
            $data['user'] = UsersModel::findOrFail(['user_name' => $username], ['*']);
        }
        else if ($request->inputs('id', 'INT')) {
            $id = $request->inputs('id', 'INT');
            if (user()->has_permission_to('editAllSiteUsers') || (user()->id() == $id)) {
                $data['user'] = UsersModel::findOrFail($request->inputs('id', 'INT'), ['*']);
            }
            else {
                abort(401);
            }
        }
        else {
            return redirect()->back();
        }

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param UpdateSiteProfile $request
     * @return Response
     * @throws \ImagickException
     */
    public function updateSiteUser(UpdateSiteProfile $request): Response
    {
        $id = $request->inputs('id', 'INT');

        if (user()->has_permission_to('editAllSiteUsers') || (user()->id() == $id)) {
            $user = UsersModel::findOrFail($id, ['*']);

            $data = $request->getValidatedData();
            $user->process($data);
            $user->update();

            if ($request->getImageUploaded()) {
                $this->uploadUserImage($request->getUploadedFiles()['new_avatar'], $user);
            }

            if (user()->id() === $user->id() && !$request->has('delete_profile')) {
                Auth::setUserSession($user);
            }
            elseif ($request->has('delete_profile')) {
                Auth::logout();
                return redirect(env('DOMAIN'));
            }
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function downloadInfo(Request $request) :Response
    {
        $userId = App::sanitizeInput(user()->id(), 'INT');
        $info['user'] = UsersModel::findOrFail(['id' => $userId], ['fields']);
        $table = view('Frontend/Users/user-info.table', $info)->raw();

        return response()->write($table)->withHeader('Content-type', 'application/vnd.ms-excel')
            ->withHeader('Content-Disposition', 'attachment; filename=info.xls');
    }
}