<?php

namespace App\Controllers\Frontend;

use App\Facades\Cache;
use Illuminate\Support\Arr;
use App\Interfaces\Request;
use \Psr\Http\Message\ResponseInterface as Response;

class Tags extends Nodes
{
    /**
     * @param Request $request
     * @return Response
     */
    public function viewTag(Request $request) :Response
    {
        $page = session()->get('page');
        $key = $request->getParsedUri();
        $cachedData = Cache::get($key);
        $articles = Arr::get($cachedData, 'articles.'.$page);

        if (empty($articles)) {
            $articles = $this->model->getList($cachedData['node'], $type = 1);
            Arr::set($cachedData, 'articles.'.$page, $articles);
            Cache::set($key, $cachedData);
        }

        $data['articles'] = $articles;

        return view($this->node()->view(), $data)->render();
    }
}