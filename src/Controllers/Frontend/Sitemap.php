<?php

namespace App\Controllers\Frontend;

use App\Interfaces\Request;
use App\Controllers\Controller;
use App\Models\Frontend\Sitemap as SitemapModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Sitemap extends Controller
{
    /**
     * @var SitemapModel
     */
    private $model;

    /**
     * Sitemap constructor.
     */
    public function __construct()
    {
        $this->model = new SitemapModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function viewSitemap(Request $request) :Response
    {
        $sitemap = new SitemapModel();
        $data['nodes'] = $sitemap->getAllNodes();
        $data['forum_nodes'] = array();

        return view($this->node()->view(), $data)->render()
            ->withHeader('Content-type',
            'application/xml; charset=utf-8');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function viewGoogleNewsSitemap(Request $request) :Response
    {
        $sitemap = new SitemapModel();
        $data['nodes'] = $sitemap->getAllNodes(1);

        return view($this->node()->view(), $data)->render()
            ->withHeader('Content-type',
            'application/xml; charset=utf-8');
    }
}