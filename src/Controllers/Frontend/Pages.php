<?php

namespace App\Controllers\Frontend;

use App\Traits\App;
use App\Facades\Mail;
use App\Facades\Cache;
use App\Mails\ContactMail;
use Illuminate\Support\Arr;
use App\Interfaces\Request;
use App\Requests\ContactForm;
use App\Models\Frontend\Tiles;
use \Psr\Http\Message\ResponseInterface as Response;

class Pages extends Nodes
{
    /**
     * @param Request $request
     * @return Response
     */
    public function viewPage(Request $request) :Response
    {
        $node = $this->node();

        if(user()->id() && ($node->name() == 'login' || $node->name() == 'register')) {
            flash_message('info', 'You are already registered and logged in');
            return redirect()->back();
        }

        if($node->name() == 'user' && $request->inputs('username')) {
            return redirect(env('DOMAIN'));
        }

        if($node->name() == 'search' && empty($request->inputs('q'))) {
            return redirect()->back();
        }

        $data['node'] = $node;

        if ($request->has('q')) {
            if($request->inputs('q') != session()->get('search.q')) {
                session()->set('page', 1);
            }

            App::setFilter('q', $request->inputs('q'), 'STR');
            $data['search_results'] = $this->model->getList($node, $type = 1);
        }

        return view($this->node()->view(), $data)->render();
    }
}