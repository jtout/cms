<?php

namespace App\Controllers;

use App\Traits\App;
use App\Facades\Cache;
use App\Interfaces\Request;
use Illuminate\Support\Arr;
use App\Entities\NodeEntity;
use App\Models\Backend\Nodes;
use App\Traits\AuthorizeRequests;
use App\Traits\ValidatesRequests;
use App\Exceptions\NotFoundException;
use App\Exceptions\MethodNotFoundException;
use Psr\Http\Message\ResponseInterface as Response;

class Controller
{
    use AuthorizeRequests, ValidatesRequests;

    /**
     * @var NodeEntity
     */
    protected static $node;

    /**
     * @return NodeEntity
     */
    public function node() :NodeEntity
    {
        return static::$node;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function processRequest(Request $request) :Response
    {
        // Check if node exists in Cache
        $nodes = new Nodes();
        $cache = Cache::get($request->getParsedUri());
        $node  = empty(Arr::get($cache, 'node'))
            ? Nodes::find(
                ['path' => App::buildPath($request), 'is_active' => 1],
                ['translations']
            )
            : $cache['node'];

        if (!empty($node->id()) && $node->is_visible()) {
            $queryParams = $request->getQueryParams();
            $action  	 = $node->module_action();
            $module 	 = $node->module_class();
            $requestType = $node->module_request_type();

            if (isset($queryParams['action']) && $queryParams['action'] != '') {
                if ($request->isPost() && $node->in_backend() == 1) {
                    $requestType = 'POST';
                    $action = App::sanitizeInput($queryParams['action'], 'STRING');
                }
                else if ($node->module_action() != $queryParams['action']) {
                    $action = App::sanitizeInput($queryParams['action'], 'STRING');
                    $nodeByAction = $nodes->getNodeByAction($action);
                    $requestType = $nodeByAction->module_request_type();

                    if (!empty($nodeByAction->id())) {
                        $node = $nodeByAction;
                    }
                    else if (empty($nodeByAction->id()) && !empty($nodeByAction->module_id())) {
                        $module = $nodeByAction->module_class();
                    }
                }
            }

            $this->validateRequestType($request, $requestType);

            $controller = new $module();
            session()->setNavigation($node);

            if (!method_exists($controller, $action)) {
                throw new MethodNotFoundException($module, $action);
            }

            $this->authorize($action);

            static::$node = $node;
            $request = $request->checkFormRequest($controller);

            if (empty(Arr::get($cache, 'node')) && !$node->in_backend()) {
                Cache::set($request->getParsedUri(), array('node' => $node));
            }

            return $controller->$action($request);
        }

        throw new NotFoundException();
    }
}