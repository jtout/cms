<?php

namespace App\Controllers\Backend;

use App\Facades\Auth;
use App\Facades\Mail;
use App\Facades\Session;
use App\Mails\LoginMail;
use App\Interfaces\Request;
use App\Rules\GoogleRecaptcha;
use App\Controllers\Controller;
use App\Models\Backend\Panel as PanelModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Panel extends Controller
{
    /**
     * @var PanelModel
     */
    private $model;

    /**
     * Panel constructor.
     */
    public function __construct()
    {
        $this->model = new PanelModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showPanel(Request $request) :Response
    {
        if(!empty(user()->id())) {
            return redirect(env('DOMAIN').'/'.env('ADMIN_NAME').'/dashboard');
        }
        else {
            return view('Backend/login')->render();
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function dashboard(Request $request) :Response
    {
        if(!empty(user()->id())) {
            return $this->dashboardDetails($request);
        }
        else {
            return redirect(env('DOMAIN').'/'.env('ADMIN_NAME'));
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function panelLogin(Request $request) :Response
    {
        $this->validate($request->inputs(),
            [
                'token' => ['required', new GoogleRecaptcha],
                'username' => 'required',
                'password' => 'required'
            ],
            [
                'token.required' => 'Please try again, Google Recaptcha token is missing!',
                'username.required' => 'Username is required!',
                'password.required' => 'Password is required'
            ]
        );

        $data = (array)$request->getParsedBody();
        $data['in_backend'] = 1;
        Auth::login($data);

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function panelLogout(Request $request) :Response
    {
        Auth::logout();

        return redirect(env('DOMAIN').'/'.env('ADMIN_NAME'));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function toggleSideMenuState(Request $request) :Response
    {
        $menuStatus = Session::get('user.side_menu_open') === false ? true : false;
        Session::set('user.side_menu_open', $menuStatus);

        return response()->withJson($menuStatus, 200);
    }

    /**
     * @param Request $request
     * @return Response
     */
    private function dashboardDetails(Request $request) :Response
    {
        $data['content_numbers'] = $this->model->getContentNumbers();

        return view($this->node()->view(), $data)->render();
    }
}