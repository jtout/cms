<?php

namespace App\Controllers\Backend;

use App\Traits\App;
use App\Interfaces\Request;
use App\Controllers\Controller;
use App\Entities\ExtraFieldGroupEntity;
use App\Requests\InsertExtraFieldGroup;
use App\Requests\UpdateExtraFieldGroup;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\Backend\ExtraFieldsGroups as ExtraFieldsGroupsModel;

class ExtraFieldsGroups extends Controller
{
    /**
     * @var ExtraFieldsGroupsModel
     */
    private $model;

    /**
     * ExtraFieldsGroups constructor.
     */
    public function __construct()
    {
        $this->model = new ExtraFieldsGroupsModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showExtraFieldsGroupsList(Request $request) :Response
    {
        $data['rows'] = $this->model->getExtraFieldsGroupsList();

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editExtraFieldsGroup(Request $request) :Response
    {
        $id = $request->inputs('id');
        $extraFieldsGroup = ExtraFieldsGroupsModel::findOrFail($id);
        $data['extra_fields_group'] = $extraFieldsGroup;

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addExtraFieldsGroup(Request $request) :Response
    {
        return view($this->node()->view())->render();
    }

    /**
     * @param UpdateExtraFieldGroup $request
     * @return Response
     */
    public function updateExtraFieldsGroup(UpdateExtraFieldGroup $request) :Response
    {
        $data = $request->getValidatedData();
        $extraFieldsGroup = ExtraFieldsGroupsModel::findById($data['id']);
        $extraFieldsGroup->process($data);
        $extraFieldsGroup->update();

        return redirect()->back();
    }

    /**
     * @param InsertExtraFieldGroup $request
     * @return Response
     */
    public function insertExtraFieldsGroup(InsertExtraFieldGroup $request) :Response
    {
        $extraFieldsGroup = new ExtraFieldGroupEntity();
        $extraFieldsGroup->process($request->getValidatedData());
        $extraFieldsGroup->insert();

        return redirect('?action=editExtraFieldsGroup&id='.$extraFieldsGroup->id());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deleteExtraFieldsGroup(Request $request) :Response
    {
        $this->validate($request->inputs(),
            ['extra_fields_groups_checkbox' => 'required'],
            ['extra_fields_groups_checkbox.required' => 'Please select extra field groups to delete.']
        );

        foreach ($request->inputs('extra_fields_groups_checkbox') as $id => $value) {
            $extraFieldsGroup = ExtraFieldsGroupsModel::findById(App::sanitizeInput($id, 'INT'));
            $extraFieldsGroup->delete();
        }

        flash_message('success', 'Extra Fields Group(s) Deleted');

        return redirect()->back();
    }
}