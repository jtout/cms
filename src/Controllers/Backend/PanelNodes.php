<?php

namespace App\Controllers\Backend;

use App\Interfaces\Request;
use App\Entities\NodeEntity;
use App\Requests\InsertPanelPage;
use App\Requests\UpdatePanelPage;
use App\Models\Backend\Nodes as NodesModel;
use \Psr\Http\Message\ResponseInterface as Response;

class PanelNodes extends Nodes
{
    /**
     * @param Request $request
     * @return Response
     */
    public function showPanelNodesList(Request $request) :Response
    {
        $data = $this->model->getNodesList($backend = true, $type = 3);

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addPanelNode(Request $request) :Response
    {
        $data = $this->model->addNodeEntity($type = 3, $backend = 1, $this->node());

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editPanelNode(Request $request) :Response
    {
        $id = $request->inputs('id', 'INT');
        $data = $this->model->editNodeEntity($id, $type = 3, $getHomepage = 0);

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param UpdatePanelPage $request
     * @return Response
     */
    public function updatePanelNode(UpdatePanelPage $request) :Response
    {
        $data = $request->getValidatedData();
        $nodeEntity = NodesModel::findOrFail($data['id']);
        $nodeEntity->process($data);
        $nodeEntity->update();

        return redirect()->back();
    }

    /**
     * @param InsertPanelPage $request
     * @return Response
     */
    public function insertPanelNode(InsertPanelPage $request) :Response
    {
        $nodeEntity = new NodeEntity();
        $nodeEntity->process($request->getValidatedData(), 'PanelPage');
        $nodeEntity->insert();

        return redirect('?action=editPanelNode&id='.$nodeEntity->id());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deletePanelNode(Request $request) :Response
    {
        return $this->deleteNode($request);
    }
}