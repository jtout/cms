<?php

namespace App\Controllers\Backend;

use App\Traits\App;
use App\Traits\Media;
use App\Interfaces\Request;
use App\Controllers\Controller;
use \Psr\Http\Message\ResponseInterface as Response;

final class MediaManager extends Controller
{
    use Media;

    /**
     * @param Request $request
     * @return Response
     */
    public function showMediaManager(Request $request) :Response
    {
        return view($this->node()->view())->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function optimizeImage(Request $request) :Response
    {
        $uploadedFiles = $request->getUploadedFiles();

        if($uploadedFiles['image']->getError() === UPLOAD_ERR_OK) {
            $directoryName = DIR.'/public_html/media/source/Optimized';

            if (!file_exists($directoryName)) {
                mkdir($directoryName, 0755, true);
            }

            $image =  $uploadedFiles['image'];
            $imageName  = str_replace(' ','-', trim(App::sanitizeInput($image->getClientOriginalName(), 'STRING')));

            try {
                $image->move(realpath($directoryName), $imageName);
                App::optimizeImage($directoryName .'/'. $imageName);

                $file = $directoryName .'/'. $imageName;
                $fh = fopen($file, 'rb');

                $stream = new \Slim\Http\Stream($fh); // create a stream instance for the response body

                App::removeDirectory($directoryName);

                return response()->withHeader('Content-Type', 'application/force-download')
                    ->withHeader('Content-Type', 'application/octet-stream')
                    ->withHeader('Content-Type', 'application/download')
                    ->withHeader('Content-Description', 'File Transfer')
                    ->withHeader('Content-Transfer-Encoding', 'binary')
                    ->withHeader('Content-Disposition', 'attachment; filename="' . basename($file) . '"')
                    ->withHeader('Expires', '0')
                    ->withHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                    ->withHeader('Pragma', 'public')
                    ->withBody($stream); // all stream contents will be sent to the response
            }
            catch (\Exception $e) {
                flash_message('danger', $e->getMessage());
            }
        }
        else {
            flash_message('danger', 'Upload Error');
        }

        return redirect()->back();
    }
}