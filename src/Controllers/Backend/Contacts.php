<?php

namespace App\Controllers\Backend;

use App\Interfaces\Request;
use App\Entities\ContactEntity;
use App\Controllers\Controller;
use App\Models\Backend\Clients;
use App\Requests\InsertContact;
use App\Requests\UpdateContact;
use Illuminate\Validation\Rule;
use App\Models\Backend\Contacts as ContactsModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Contacts extends Controller
{
    /**
     * @var ContactsModel
     */
    private $model;

    /**
     * Contacts constructor.
     */
    public function __construct()
    {
        $this->model = new ContactsModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showContactsList(Request $request) :Response
    {
        $clients = new Clients();

        $data = array(
            'rows' => $this->model->getContactsList(),
            'clients' => $clients->getClients()
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showClientContacts(Request $request) :Response
    {
        $clientId = $request->inputs('client_id', 'INT');
        $clientEntity = Clients::findById($clientId);

        if(empty($clientEntity->id())) {
            return redirect(env('DOMAIN').'/'.env('ADMIN_NAME').'/clients');
        }

        $data = array(
            'client' 	=> $clientEntity,
            'contacts'  => $this->model->getClientContactsList($clientId)
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addContact(Request $request) :Response
    {
        $clientId = $request->inputs('client_id', 'INT');
        $clientEntity = Clients::findOrFail($clientId);
        $data = array(
            'client' => $clientEntity,
            'contacts'  => $this->model->getClientContactsList($clientId)
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editContact(Request $request) :Response
    {
        $clientId 	= $request->inputs('client_id', 'INT');
        $contactId  = $request->inputs('contact_id', 'INT');
        $data['client'] = Clients::findOrFail($clientId);
        $data['contact'] = ContactsModel::findOrFail($contactId);
        $data['contacts'] = $this->model->getClientContactsList($clientId);

        $clientContacts = [];
        foreach ($data['contacts'] as $contact) {
            $clientContacts[] = $contact->id();
        }

        $this->validate(
            $request->inputs(),
            ['contact_id' => Rule::in($clientContacts)],
            ['contact_id.*' => 'Contact not found in this client!']
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param InsertContact $request
     * @return Response
     */
    public function insertContact(InsertContact $request) :Response
    {
        $contactEntity = new ContactEntity();
        $contactEntity->process($request->getValidatedData());
        $contactEntity->insert();

        return redirect('?action=editContact&client_id='.$contactEntity->client_id().'&contact_id='.$contactEntity->id());
    }

    /**
     * @param UpdateContact $request
     * @return Response
     */
    public function updateContact(UpdateContact $request) :Response
    {
        $data = $request->getValidatedData();
        $contactEntity = ContactsModel::findById($data['id']);
        $contactEntity->process($data);
        $contactEntity->update();

        return redirect()->back();
    }
}