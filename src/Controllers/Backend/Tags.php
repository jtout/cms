<?php

namespace App\Controllers\Backend;

use App\Traits\App;
use App\Facades\Session;
use App\Interfaces\Request;
use App\Requests\UpdateTag;
use App\Requests\InsertTag;
use App\Entities\NodeEntity;
use App\Models\Backend\Nodes as NodesModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Tags extends Nodes
{
    /**
     * @param Request $request
     * @return Response
     */
    public function showTagsList(Request $request) :Response
    {
        if ($request->isAjax() && $request->has('tags')) {
            $tags = $request->inputs('tags');
            $action = $request->inputs('action');
            $selectedTags = empty(Session::get('selected_tags')) ? [] : Session::get('selected_tags');

            foreach ($tags as $tag) {
                if ($action == 0) {
                    $key = in_array($tag['id'], array_keys($selectedTags)) ? $tag['id'] : null;

                    if ($key == 0 || !empty($key)) {
                        unset($selectedTags[$key]);
                    }
                }
                else {
                    $selectedTags[$tag['id']] = $tag;
                }
            }

            Session::set('selected_tags', $selectedTags);

            return response()->withJson(['success' => true], 200);
        }

        $data = $this->model->getNodesList($backend = false, $type = 4);

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addTag(Request $request) :Response
    {
        $translation['from_id'] = $request->inputs('from_id');
        $translation['iso_code'] = $request->inputs('iso_code');

        $data = $this->model->addNodeEntity($type = 2, $backend = 0, $this->node(), $translation, $getHomepage = 0);

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editTag(Request $request) :Response
    {
        $id = $request->inputs('id', 'INT');
        $data = $this->model->editNodeEntity($id, $type = 4, $getHomepage = 0);

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function mergeTags(Request $request) :Response
    {
        $data = $request->inputs();
        $data['view'] = 'Backend/Tags/tags.modal';

        if (isset($data['selected_tags'])) {
            $checkboxes = explode(',', $data['selected_tags']);
        }
        else {
            $data['checkboxes'] = Session::get('selected_tags');
            $checkboxes = !empty($data['checkboxes']) ? $data['checkboxes'] : [];
        }

        if(count($checkboxes) < 2) {
            $result['type'] = 'warning';
            $result['message']= 'Please select at least two tags!';

            if($request->isAjax()) {
                return response()->withJson($result, 200);
            }
            else {
                flash_message($result['type'], $result['message']);
                return redirect()->back();
            }
        }

        if (isset($data['merge_selected_tags']) && $data['merge_selected_tags'] == 1) {
            $selectedTags = App::sanitizeInput($data['selected_tags'], 'STRING');
            $mergeInto = App::sanitizeInput($data['merge_into'], 'STRING');

            $mergeTags = $this->model->mergeTags($selectedTags, (int)$mergeInto);
            flash_message($mergeTags['type'], $mergeTags['message']);

            if ($mergeTags['success'] == true) {
                Session::unset('selected_tags');
            }

            return redirect()->back();
        }

        return view($data['view'], $data)->render();
    }

    /**
     * @param UpdateTag $request
     * @return Response
     * @throws \ImagickException
     */
    public function updateTag(UpdateTag $request) :Response
    {
        // Update page
        $nodeEntity = NodesModel::findOrFail($request->inputs('id', 'INT'));
        $nodeEntity->process($request->getValidatedData());
        $nodeEntity->update();

        // Upload Files
        $this->uploadFiles($request, $nodeEntity);

        return redirect()->back();
    }

    /**
     * @param InsertTag $request
     * @return Response
     * @throws \ImagickException
     */
    public function insertTag(InsertTag $request) :Response
    {
        // Insert new article
        $nodeEntity = new NodeEntity();
        $nodeEntity->process($request->getValidatedData(), 'Tag');
        $nodeEntity->insert();

        // Upload Files
        $this->uploadFiles($request, $nodeEntity);

        return redirect('?action=editTag&id='.$nodeEntity->id());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deleteTag(Request $request) :Response
    {
        return $this->deleteNode($request);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deleteTagTranslation(Request $request) :Response
    {
        $this->deleteNodeTranslation($request);

        return redirect()->back();
    }
}