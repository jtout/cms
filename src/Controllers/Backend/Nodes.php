<?php

namespace App\Controllers\Backend;

use App\Traits\App;
use App\Traits\Media;
use App\Interfaces\Request;
use App\Controllers\Controller;
use App\Models\Backend\Nodes as NodesModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Nodes extends Controller
{
    use Media;

    /**
     * @var NodesModel
     */
    protected $model;

    /**
     * Nodes constructor.
     */
    public function __construct()
    {
        $this->model = new NodesModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getNodes(Request $request): Response
    {
        $data = array();
        $term = empty($request->inputs('q')) ? '' : $request->inputs('q');
        $type = $request->inputs('type');
        $backend = $request->inputs('backend');
        $language = $request->inputs('language');
        $getHomepage = empty($request->inputs('getHomepage')) ? 0 : $request->inputs('getHomepage');
        $nodes = $this->model->getNodes($type, $backend, $language, $getHomepage, $term);

        $i = 0;
        foreach ($nodes as $node) {
            if($getHomepage == 1) {
                if($node->depth() > 1) {
                    $data[$node->id()]['text'] = str_repeat(' - ', $node->depth() - 1) . $node->title();
                }
                else {
                    $data[$node->id()]['text'] = $node->title();
                }

                $data[$node->id()]['id'] = $node->id();
            }
            else if($type == 4) {
                $data[$term]['children'][$i]['id'] = $node->id();
                $data[$term]['children'][$i]['text'] = $node->title();

                if(count($data[$term]['children']) > 0) {
                    $data[$term]['text'] = 'Suggestions';
                }
            }
            else {
                if($node->depth() > 2) {
                    $data[$node->id()]['text'] = str_repeat(' - ', $node->depth() - 2) . $node->title();
                }
                else {
                    $data[$node->id()]['text'] = $node->title();
                }

                $data[$node->id()]['id'] = $node->id();
            }

            $i++;
        }

        return response()->withJson($data, 200);
    }

    /**
     * @param Request $request
     * @return Response
     */
    protected function deleteNode(Request $request) :Response
    {
        $contentType = lcfirst($this->node()->title());

        $this->validate($request->inputs(),
            ['node_checkbox' => 'required'],
            ['node_checkbox.required' => 'Please select '.$contentType.' to delete.']
        );

        foreach ($request->inputs('node_checkbox') as $nodeId => $value) {
            $nodeEntity = NodesModel::findOrFail(App::sanitizeInput($nodeId, 'INT'));
            $nodeEntity->delete();
        }

        flash_message('success', ucfirst(rtrim($contentType, 's')).'(s) Deleted');

        return redirect()->back();
    }

    /**
     * @param Request $request
     */
    protected function deleteNodeTranslation(Request $request) :void
    {
        $nodeId = $request->inputs('node', 'INT');
        $translation = $request->inputs('translation');

        $this->model->deleteNodeTranslation($nodeId, $translation);
        flash_message('success', 'Translation Deleted');
    }
}