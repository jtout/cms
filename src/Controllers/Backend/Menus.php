<?php

namespace App\Controllers\Backend;

use App\Traits\App;
use App\Interfaces\Request;
use App\Models\Backend\Nodes;
use App\Controllers\Controller;
use App\Models\Backend\Menus as MenusModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Menus extends Controller
{
    /**
     * @var MenusModel
     */
    private $model;

    /**
     * Menus constructor.
     */
    public function __construct()
    {
        $this->model = new MenusModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showMenus(Request $request) :Response
    {
        $nodes = new Nodes();
        $childs = $nodes->getChilds($this->node());

        return view($this->node()->view(), $childs)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showFrontEndMenu(Request $request) :Response
    {
        $data = $this->model->getMenus(0);

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showBackEndMenu(Request $request) :Response
    {
        $data = $this->model->getMenus(1);

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getMenuInfo(Request $request) :Response
    {
        $id = $request->inputs('id', 'INT');
        $data = $this->model->fetchMenuInfo($id);

        return response()->withJson($data, 200);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function updateMenuInfo(Request $request) :Response
    {
        $data = $request->inputs();

        $nodeEntity = Nodes::findById($request->inputs('id', 'INT'));
        $nodeEntity->setTitle($request->inputs('title'));
        $nodeEntity->setName(App::makeSefUrl($request->inputs('title')));
        $nodeEntity->setIsActive(isset($data['is_active']) ? 1 : 0);
        $nodeEntity->setOnMenu(isset($data['on_menu']) ? 1 : 0);
        $nodeEntity->setAccessLevelId($request->inputs('access_level'));
        $nodeEntity->setUpdatedBy(user()->id());

        $nodeEntity->update();
        flash_message('success', 'Menu updated!');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function updateMenuOrder(Request $request) :Response
    {
        $params = $request->inputs();
        $nodes = new Nodes();

        $data['id'] = $request->inputs('id', 'INT');
        $data['parent'] = $params['parent'] === '#' ? $nodes->getLanguageParent($params['language'])->id() : $params['parent'];
        $data['ordering'] = (array) $params['ordering'];

        $update = $this->model->updateOrder($data['id'], $data['parent'], $data['ordering']);

        return response()->withJson($update, 201);
    }
}