<?php

namespace App\Controllers\Backend;

use App\Traits\App;
use App\Facades\Cache;
use App\Interfaces\Request;
use App\Controllers\Controller;
use \App\Models\Backend\Nodes as Nodes;
use App\Observers\Cache as CacheObserver;
use App\Models\Backend\Tiles as TilesModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Tiles extends Controller
{
    /**
     * @var TilesModel
     */
    private $model;

    /**
     * Tiles constructor.
     */
    public function __construct()
    {
        $this->model = new TilesModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showTilesList(Request $request) :Response
    {
        $data['rows'] = $this->model->getArticles();
        $data['tiles'] = $this->model->getTiles();

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function addTiles(Request $request) :Response
    {
        $data = $request->inputs();

        if(isset($data['node_checkbox'])) {
            $order = $this->model->checkTilesOrdering();
            $order++;

            foreach ($data['node_checkbox'] as $id => $val) {
                $id = App::sanitizeInput($id, 'INT');
                $insert = $this->model->addTile($id, $order);
                $order++;

                $node = Nodes::findById($id);
                CacheObserver::observe($node);
                $node->fireObserverEvent('delete');
            }

            Cache::delete('tiles');
            flash_message($insert['type'], $insert['message']);
        }
        else {
            flash_message('info', 'No articles selected');
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function removeTile(Request $request) :Response
    {
        $id = $request->inputs('id', 'INT');
        $delete = $this->model->deleteTile($id);

        flash_message($delete['type'], $delete['message']);
        Cache::delete('tiles');

        $node = Nodes::findOrFail($id);
        CacheObserver::observe($node);
        $node->fireObserverEvent('delete');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function updateTilesOrder(Request $request) :Response
    {
        $params = (array) $request->inputs();
        $data['ordering'] = (array) $params['ordering'];

        $update = $this->model->updateOrder($data['ordering']);
        Cache::delete('tiles');

        return response()->withJson($update, 200);
    }
}