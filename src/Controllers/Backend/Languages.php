<?php

namespace App\Controllers\Backend;

use App\Traits\App;
use App\Interfaces\Request;
use App\Controllers\Controller;
use App\Requests\InsertLanguage;
use App\Requests\UpdateLanguage;
use App\Entities\LanguageEntity;
use App\Models\Backend\Languages as LanguagesModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Languages extends Controller
{
    /**
     * @var LanguagesModel
     */
    private $model;

    /**
     * Languages constructor.
     */
    public function __construct()
    {
        $this->model = new LanguagesModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showLanguagesList(Request $request) :Response
    {
        $data['rows'] = $this->model->getLanguagesList();

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editLanguage(Request $request) :Response
    {
        $id = $request->inputs('id');
        $data['language'] = LanguagesModel::findOrFail($id);

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addLanguage(Request $request) :Response
    {
        return view($this->node()->view())->render();
    }

    /**
     * @param InsertLanguage $request
     * @return Response
     */
    public function updateLanguage(UpdateLanguage $request) :Response
    {
        $data = $request->getValidatedData();
        $language = LanguagesModel::findOrFail($data['id']);
        $language->process($data);
        $language->update();

        return redirect()->back();
    }

    /**
     * @param UpdateLanguage $request
     * @return Response
     */
    public function insertLanguage(InsertLanguage $request) :Response
    {
        $language = new LanguageEntity();
        $language->process($request->getValidatedData());
        $language->insert();

        return redirect('?action=editLanguage&id='.$language->id());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deleteLanguage(Request $request) :Response
    {
        $this->validate($request->inputs(),
            ['language_checkbox' => 'required'],
            ['language_checkbox.required' => 'Please select languages to delete.']
        );

        foreach ($request->inputs('language_checkbox') as $id => $value) {
            $language = LanguagesModel::findById(App::sanitizeInput($id, 'INT'));
            $language->delete();
        }

        flash_message('success', 'Language(s) Deleted');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getLanguages(Request $request) :Response
    {
        return response()->withJson(LANGUAGES, 200);
    }
}