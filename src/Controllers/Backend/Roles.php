<?php

namespace App\Controllers\Backend;

use App\Traits\App;
use App\Facades\Session;
use App\Interfaces\Request;
use App\Entities\RoleEntity;
use App\Requests\UpdateRole;
use App\Requests\InsertRole;
use App\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\Backend\AccessLevels;
use App\Models\Backend\Roles as RolesModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Roles extends Controller
{
    /**
     * @var RolesModel
     */
    private $model;

    /**
     * Roles constructor.
     */
    public function __construct()
    {
        $this->model = new RolesModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showRolesList(Request $request) :Response
    {
        $accessLevels = new AccessLevels();

        $data = array(
            'rows' =>$this->model->rolesList(),
            'access_levels' => $accessLevels->getAccessLevels()
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editRole(Request $request) :Response
    {
        $id = $request->inputs('id', 'INT');
        $role = RolesModel::findOrFail($id);
        $accessLevels = new AccessLevels();
        $data = array(
            'role' => $role,
            'access_levels' => $accessLevels->getAccessLevels(),
            'permissions' => App::createPermissionsHtmlList(explode(',', $role->permissions())),
            'parents' => $this->model->getRoles()
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addRole(Request $request) :Response
    {
        $accessLevels = new AccessLevels();
        $permissions = !empty(Session::get('insert_role')['permissions']) ? explode(',', Session::get('insert_role')['permissions']) : array();
        $data = array(
            'access_levels' => $accessLevels->getAccessLevels(),
            'permissions' => App::createPermissionsHtmlList($permissions),
            'parents' => $this->model->getRoles()
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param InsertRole $request
     * @return Response
     */
    public function insertRole(InsertRole $request) :Response
    {
        $roleEntity = new RoleEntity();
        $roleEntity = new RoleEntity();
        $roleEntity->process($request->getValidatedData());
        $roleEntity->insert();

        return redirect('?action=editRole&id='.$roleEntity->id());
    }

    /**
     * @param UpdateRole $request
     * @return Response
     */
    public function updateRole(UpdateRole $request) :Response
    {
        $data = $request->getValidatedData();
        $roleEntity = RolesModel::findOrFail($data['id']);
        $roleEntity->process($data);
        $roleEntity->update();

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deleteRole(Request $request) :Response
    {
        $this->validate(
            $request->inputs(),
            [
                'role_checkbox' => 'required',
                'role_checkbox.*' => Rule::notIn([1])
            ],
            [
                'role_checkbox.required' => 'Please select role to delete.',
                'role_checkbox.*' => 'You cannot delete Developer Role!'
            ]
        );

        foreach ($request->inputs('role_checkbox') as $role => $value) {
            $role = RolesModel::findById(App::sanitizeInput($role, 'INT'));
            $role->delete();
        }

        flash_message('success', 'Role(s) Deleted');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getRoles(Request $request) :Response
    {
        $role = RolesModel::findOrFail($request->inputs('role_id'));
        $actions = explode(',',$role->permissions());

        $data = array(
            'success' => true,
            'message' => $actions
        );

        return response()->withJson($data, 201);
    }
}