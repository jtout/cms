<?php

namespace App\Controllers\Backend;

use App\Traits\App;
use App\Interfaces\Request;
use App\Entities\NodeEntity;
use Illuminate\Validation\Rule;
use App\Controllers\Controller;
use App\Models\Backend\AppLogs as AppLogsModel;
use \Psr\Http\Message\ResponseInterface as Response;

class AppLogs extends Controller
{
    /**
     * @var AppLogsModel
     */
    private $model;

    /**
     * AppLogs constructor.
     */
    public function __construct()
    {
        $this->model = new AppLogsModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showAppLogsList(Request $request) :Response
    {
        $data = array(
            'rows' => $this->model->getAppLogsList(),
            'types' => App::appLogsTypes()
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showAppLogInfo(Request $request) :Response
    {
        $id = $request->inputs('id', 'INT');
        $data['log'] = AppLogsModel::findById($id);
        $data['logInfo'] = json_decode($data['log']->info());

        if (json_last_error()!== JSON_ERROR_NONE) {
            $data['logInfo'] = $data['log']->info();
        }

        if ($data['log']->type() == 'Changes') {
            $data['view'] = 'Backend/AppLogs/app_logs_changes.modal';
        }
        else {
            $data['view'] = 'Backend/AppLogs/app_logs.modal';
        }

        return view($data['view'] , $data)->render();
    }

    /**
     * @param Request $request
     * @param NodeEntity $node
     * @return Response
     */
    public function deleteAppLog(Request $request) :Response
    {
        $this->validate($request->inputs(),
            ['app_log_checkbox' => 'required'],
            ['app_log_checkbox.required' => 'Please select app log to delete.']
        );

        foreach ($request->inputs('app_log_checkbox') as $appLogId => $value) {
            $appLogEntity = AppLogsModel::findOrFail(App::sanitizeInput($appLogId, 'INT'));
            $appLogEntity->delete();
        }

        flash_message('success', 'App Log(s) Deleted');

        return redirect()->back();
    }
}