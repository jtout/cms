<?php

namespace App\Controllers\Backend;

use App\Traits\App;
use App\Interfaces\Request;
use App\Requests\UpdateClient;
use App\Entities\ClientEntity;
use App\Requests\InsertClient;
use App\Controllers\Controller;
use App\Models\Backend\Clients as ClientsModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Clients extends Controller
{
    /**
     * @var ClientsModel
     */
    private $model;

    /**
     * Clients constructor.
     */
    public function __construct()
    {
        $this->model = new ClientsModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showClientsList(Request $request) :Response
    {
        $data['rows'] = $this->model->getClientsList();

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addClient(Request $request) :Response
    {
        return view($this->node()->view())->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editClient(Request $request) :Response
    {
        $id = $request->inputs('id', 'INT');
        $client = ClientsModel::findOrFail($id);
        $data = array(
            'client' => $client
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param InsertClient $request
     * @return Response
     */
    public function insertClient(InsertClient $request) :Response
    {
        $clientEntity = new ClientEntity();
        $clientEntity->process($request->getValidatedData());
        $clientEntity->insert();

        return redirect('?action=editClient&id='.$clientEntity->id());
    }

    /**
     * @param UpdateClient $request
     * @return Response
     */
    public function updateClient(UpdateClient $request) :Response
    {
        $data = $request->getValidatedData();
        $clientEntity = ClientsModel::findById($data['id']);
        $clientEntity->process($data);
        $clientEntity->update();

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deleteClients(Request $request) :Response
    {
        $this->validate($request->inputs(),
            ['banners_positions_checkbox' => 'required'],
            ['banners_positions_checkbox.required' => 'Please select clients to delete.']
        );

        foreach ($request->inputs('clients_checkbox') as $id => $value) {
            $client = ClientsModel::findOrFail(App::sanitizeInput($id, 'INT'));
            $client->delete();
        }

        flash_message('success', 'Client(s) deleted!');

        return redirect()->back();
    }
}