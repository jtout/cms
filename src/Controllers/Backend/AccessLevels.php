<?php

namespace App\Controllers\Backend;

use App\Traits\App;
use App\Interfaces\Request;
use Illuminate\Validation\Rule;
use App\Controllers\Controller;
use App\Entities\AccessLevelEntity;
use App\Requests\InsertAccessLevel;
use App\Requests\UpdateAccessLevel;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\Backend\AccessLevels AS AccessLevelsModel;

class AccessLevels extends Controller
{
    /**
     * @var AccessLevelsModel
     */
    private $model;

    /**
     * AccessLevels constructor.
     */
    public function __construct()
    {
        $this->model = new AccessLevelsModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showAccessLevelsList(Request $request) :Response
    {
        $data['rows'] = $this->model->getAccessLevelsList();

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addAccessLevel(Request $request) :Response
    {
        $data = array(
            'parents' => $this->model->getAccessLevels()
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editAccessLevel(Request $request) :Response
    {
        $id = $request->inputs('id', 'INT');
        $accessLevel = AccessLevelsModel::findOrFail($id);

        $data = array(
            'accessLevel' => $accessLevel,
            'parents' => $this->model->getAccessLevels()
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param InsertAccessLevel $request
     * @return Response
     */
    public function updateAccessLevel(UpdateAccessLevel $request) :Response
    {
        $data = $request->getValidatedData();
        $accessLevel = AccessLevelsModel::findOrFail($data['id']);
        $accessLevel->process($data);
        $accessLevel->update();

        return redirect()->back();
    }

    /**
     * @param UpdateAccessLevel $request
     * @return Response
     */
    public function insertAccessLevel(InsertAccessLevel $request) :Response
    {
        $accessLevel = new AccessLevelEntity();
        $accessLevel->process($request->getValidatedData());
        $accessLevel->insert();

        return redirect('?action=editAccessLevel&id='.$accessLevel->id());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deleteAccessLevel(Request $request) :Response
    {
        $this->validate(
            $request->inputs(),
            [
                'access_level_checkbox' => 'required',
                'access_level_checkbox.*' => Rule::notIn([1])
            ],
            [
                'access_level_checkbox.required' => 'Please select access levels to delete.',
                'access_level_checkbox.*' => 'You cannot delete Developer access level!'
            ]
        );

        foreach ($request->inputs('access_level_checkbox') as $accessLevel => $value) {
            $accessLevelEntity = AccessLevelsModel::findById(App::sanitizeInput($accessLevel, 'INT'));
            $accessLevelEntity->delete();
        }

        flash_message('success', 'Access Level(s) Deleted');

        return redirect()->back();
    }
}