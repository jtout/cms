<?php

namespace App\Controllers\Backend;

use App\Traits\App;
use App\Interfaces\Request;
use App\Controllers\Controller;
use App\Requests\UpdateComment;
use App\Models\Backend\Comments as CommentsModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Comments extends Controller
{
    /**
     * @var CommentsModel
     */
    private $model;

    /**
     * Comments constructor.
     */
    public function __construct()
    {
        $this->model = new CommentsModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showCommentsList(Request $request) :Response
    {
        $data['rows'] = $this->model->getCommentsList();

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editComment(Request $request) :Response
    {
        $data['comment'] = CommentsModel::findOrFail($request->inputs('id', 'INT'));

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param UpdateComment $request
     * @return Response
     */
    public function updateComment(UpdateComment $request) :Response
    {
        $data = $request->getValidatedData();
        $comment = CommentsModel::findById($data['id']);
        $comment->process($data);
        $comment->update();

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deleteComment(Request $request) :Response
    {
        $this->validate($request->inputs(),
            ['comment_checkbox' => 'required'],
            ['comment_checkbox.required' => 'Please select comments to delete.']
        );

        foreach ($request->inputs('comment_checkbox') as $comment => $value) {
            $commentEntity = CommentsModel::findOrFail(App::sanitizeInput($comment, 'INT'));
            $commentEntity->delete();
        }

        flash_message('success', 'Comment(s) Deleted');

        return redirect()->back();
    }
}