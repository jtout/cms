<?php

namespace App\Controllers\Backend;

use App\Traits\App;
use App\Interfaces\Request;
use App\Controllers\Controller;
use App\Requests\UpdateBannerPosition;
use App\Requests\InsertBannerPosition;
use App\Entities\BannersPositionEntity;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\Backend\BannersPositions as BannersPositionsModel;

class BannersPositions extends Controller
{
    /**
     * @var BannersPositionsModel
     */
    private $model;

    /**
     * BannersPositions constructor.
     */
    public function __construct()
    {
        $this->model = new BannersPositionsModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showBannersPositionsList(Request $request) :Response
    {
        $data['rows'] = $this->model->getBannersPositionsList();

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addBannersPosition(Request $request) :Response
    {
        return view($this->node()->view())->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editBannersPosition(Request $request) :Response
    {
        $id = $request->inputs('id', 'INT');
        $position = BannersPositionsModel::findOrFail($id);

        $data = array(
            'position' => $position
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param InsertBannerPosition $request
     * @return Response
     */
    public function updateBannersPosition(UpdateBannerPosition $request) :Response
    {
        $data = $request->getValidatedData();
        $position = BannersPositionsModel::findOrFail($data['id']);
        $position->process($data);
        $position->update();

        return redirect()->back();
    }

    /**
     * @param UpdateBannerPosition $request
     * @return Response
     */
    public function insertBannersPosition(InsertBannerPosition $request) :Response
    {
        $position = new BannersPositionEntity();
        $position->process($request->getValidatedData());
        $position->insert();

        return redirect('?action=editBannersPosition&id='.$position->id());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deleteBannersPositions(Request $request) :Response
    {
        $this->validate($request->inputs(),
            ['banners_positions_checkbox' => 'required'],
            ['banners_positions_checkbox.required' => 'Please select position to delete.']
        );

        foreach ($request->inputs('banners_positions_checkbox') as $positionId => $value) {
            $position = BannersPositionsModel::findOrFail(App::sanitizeInput($positionId, 'INT'));
            $position->delete();
        }

        flash_message('success', 'Banners Position(s) Deleted');

        return redirect()->back();
    }
}