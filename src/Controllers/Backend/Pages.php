<?php

namespace App\Controllers\Backend;

use App\Interfaces\Request;
use App\Entities\NodeEntity;
use App\Requests\InsertPage;
use App\Requests\UpdatePage;
use App\Models\Backend\Nodes as NodesModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Pages extends Nodes
{
    /**
     * @param Request $request
     * @return Response
     */
    public function showPagesList(Request $request) :Response
    {
        $data = $this->model->getNodesList($backend = false, $type = 3);

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addPage(Request $request) :Response
    {
        $translation['from_id'] = $request->inputs('from_id', 'INT');
        $translation['iso_code'] = $request->inputs('iso_code');

        $data = $this->model->addNodeEntity($type = 3, $backend = 0, $this->node(), $translation, $getHomepage = 1);

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editPage(Request $request) :Response
    {
        $id = $request->inputs('id', 'INT');
        $data = $this->model->editNodeEntity($id, $type = 3, $getHomepage = 1);

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param UpdatePage $request
     * @return Response
     * @throws \ImagickException
     */
    public function updatePage(UpdatePage $request) :Response
    {
        // Update page
        $nodeEntity = NodesModel::findOrFail($request->inputs('id', 'INT'));
        $nodeEntity->process($request->getValidatedData());
        $nodeEntity->update();

        // Upload Files
        $this->uploadFiles($request, $nodeEntity);

        return redirect()->back();
    }

    /**
     * @param InsertPage $request
     * @return Response
     * @throws \ImagickException
     */
    public function insertPage(InsertPage $request) :Response
    {
        // Insert new article
        $nodeEntity = new NodeEntity();
        $nodeEntity->process($request->getValidatedData(), 'Page');
        $nodeEntity->insert();

        // Upload Files
        $this->uploadFiles($request, $nodeEntity);

        return redirect('?action=editPage&id='.$nodeEntity->id());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deletePage(Request $request) :Response
    {
        return $this->deleteNode($request);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deletePageTranslation(Request $request) :Response
    {
        $this->deleteNodeTranslation($request);

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getPages(Request $request): Response
    {
        return $this->getNodes($request);
    }
}