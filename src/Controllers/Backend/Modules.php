<?php

namespace App\Controllers\Backend;

use App\Traits\App;
use App\Interfaces\Request;
use App\Entities\ModuleEntity;
use App\Requests\InsertModule;
use App\Requests\UpdateModule;
use App\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\Backend\Modules as ModulesModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Modules extends Controller
{
    /**
     * @var ModulesModel
     */
    private $model;

    /**
     * Modules constructor.
     */
    public function __construct()
    {
        $this->model = new ModulesModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showModulesList(Request $request) :Response
    {
        $data = array(
            'rows' => $this->model->getModulesList(),
            'max_level' => (int) db()->table('modules_tree')->max('depth'),
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editModule(Request $request) :Response
    {
        $id = $request->inputs('id', 'INT');
        $module = ModulesModel::findOrFail($id);
        $data = array(
            'module' => $module,
            'modules' => $this->model->getModules(['is_function' => 0, 'is_active' => 1])
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addModule(Request $request) :Response
    {
        $data = array(
            'modules' => $this->model->getModules(['is_function' => 0, 'is_active' => 1])
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param InsertModule $request
     * @return Response
     */
    public function updateModule(UpdateModule $request) :Response
    {
        $data = $request->getValidatedData();
        $module = ModulesModel::findById($data['id']);
        $module->process($data);
        $module->update();

        return redirect()->back();
    }

    /**
     * @param InsertModule $request
     * @return Response
     */
    public function insertModule(InsertModule $request) :Response
    {
        $module = new ModuleEntity();
        $module->process($request->getValidatedData());
        $module->insert();

        return redirect('?action=editModule&id='.$module->id());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deleteModule(Request $request) :Response
    {
        $this->validate($request->inputs(),
            [
                'module_checkbox' => 'required',
                'module_checkbox.*' => Rule::notIn([1, 2, 3])
            ],
            [
                'module_checkbox.required' => 'Please select modules to delete.',
                'module_checkbox.*' => 'You cannot delete System, Backend and Frontend!'
            ]
        );

        foreach ($request->inputs('module_checkbox') as $module => $value) {
            $module = ModulesModel::findById(App::sanitizeInput($module, 'INT'));
            $module->delete();
        }

        flash_message('success', 'Module(s) Deleted!');

        return redirect()->back();
    }
}