<?php

namespace App\Controllers\Backend;

use App\Interfaces\Request;
use App\System\Configuration;
use App\Controllers\Controller;
use \Psr\Http\Message\ResponseInterface as Response;

final class Settings extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function editGeneralSettings(Request $request) :Response
    {
        $languages = new \App\Models\Backend\Languages();
        $settings = Configuration::initialize()->settings();

        $data = array(
            'settings' => $settings,
            'languages' => $languages->getLanguages()
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function updateGeneralSettings(Request $request) :Response
    {
        $this->validate(
            $request->inputs(),
            ['site_name' => 'required'],
            ['site_name.required' => 'Site Name cannot be empty!']
        );

        $siteName = $request->inputs('site_name');

        $sanitizedData = array(
            'site_name' => $siteName,
            'language' => $request->inputs('language'),
            'site_offline' => $request->has('site_offline') ? (int) 1 : (int) 0,
            'offline_message' => $request->inputs('offline_message') ?? '',
            'system_email_from_name' => $siteName,
            'system_administrator_email' => $request->inputs('system_administrator_email', 'EMAIL'),
            'system_email' => $request->inputs('system_email', 'EMAIL'),
            'allowed_ips' => $request->inputs('allowed_ips'),
        );

        $languages = new \App\Models\Backend\Languages();
        $activeLanguages = $languages->getLanguages(['is_active' => 1]);

        foreach($activeLanguages as $activeLanguage) {
            $sanitizedData['languages'][$activeLanguage->iso_code()] = $activeLanguage->title();
        }

        $file = fopen(DIR.'/config.json', 'w');
        fwrite($file, json_encode($sanitizedData, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
        fclose($file);

        flash_message('success', 'Settings updated!');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showSystemSettings(Request $request) :Response
    {
        $nodes = new \App\Models\Backend\Nodes();
        $childs = $nodes->getChilds($this->node());

        return view($this->node()->view(), $childs)->render();
    }

    /**
     * @param array $data
     * @return array
     */
    private function validateData(array $data): array
    {
        $response = array(
            'success' => true
        );

        if (empty($data['site_name'])) {
            $response['success'] = false;
            flash_message('danger', 'Site Name cannot be empty!');
        }

        return $response;
    }
}