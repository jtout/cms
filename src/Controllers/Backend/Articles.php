<?php

namespace App\Controllers\Backend;

use App\Traits\Media;
use App\Facades\Session;
use App\Interfaces\Request;
use App\Entities\NodeEntity;
use App\Requests\InsertArticle;
use App\Requests\UpdateArticle;
use App\Traits\ExtraFieldsHelper;
use App\Models\Backend\ExtraFields;
use App\Models\Backend\Nodes as NodesModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Articles extends Nodes
{
    use ExtraFieldsHelper;

    /**
     * @param Request $request
     * @return Response
     */
    public function showArticlesList(Request $request) :Response
    {
        $data = $this->model->getNodesList($backend = false, $type = 1);

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deleteArticle(Request $request) :Response
    {
        return $this->deleteNode($request);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deleteArticleTranslation(Request $request) :Response
    {
        $this->deleteNodeTranslation($request);

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getCategories(Request $request) :Response
    {
        return $this->getNodes($request);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchTags(Request $request) :Response
    {
        return $this->getNodes($request);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getExtraFields(Request $request) :Response
    {
        $id = $request->inputs('id', 'INT');
        $parentId = $request->inputs('parent_id');

        $nodeEntity = NodesModel::findById($id);
        $parentNodeEntity = NodesModel::findById($parentId);

        $extraFieldsModel = new ExtraFields();
        $extraFields = $extraFieldsModel->getExtraFields(['group_id' => $parentNodeEntity->extra_fields_group_id(), 'is_active' => 1]);
        $fields =  $this->renderExtraFields($extraFields, $nodeEntity->extra_fields_values());

        if (empty($fields)) {
            $data['message'] = '<div id="error_msg" class="alert alert-info">This category has no extra fields!</div>';
            $data['success'] = false;
        }
        else {
            $data['message'] = $fields;
            $data['success'] = true;
        }

        return response()->withJson($data, 200);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addArticle(Request $request) :Response
    {
        $translation['from_id']  = $request->inputs('from_id', 'INT');
        $translation['iso_code'] = $request->inputs('iso_code');

        $data = $this->model->addNodeEntity($type = 2, $backend = 0, $this->node(), $translation, $getHomepage = 0);
        $data['sources'] = array_keys(Media::videoSources());

        if (!empty(Session::get('insert_article')['parent_id'])) {
            $extraFields = new ExtraFields();
            $parentNodeEntity = $this->model->getEntity(['id' => Session::get('insert_article')['parent_id']]);
            $data['extra_fields'] = $this->renderExtraFields($extraFields->getExtraFields(['group_id' => $parentNodeEntity->extra_fields_group_id()]),
                Session::get('insert_article')['extra_fields_values']);
        }

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editArticle(Request $request) :Response
    {
        $id = $request->inputs('id', 'INT');
        $data = $this->model->editNodeEntity($id, $type = 1, $getHomepage = 0);
        $data['sources'] = array_keys(Media::videoSources());

        if (user()->has_permission_to('editOwnArticle') && $data['nodeEntity']->created_by() != user()->id()) {
            abort(401);
        }

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param InsertArticle $request
     * @return Response
     * @throws \ImagickException
     */
    public function insertArticle(InsertArticle $request) :Response
    {
        $nodeEntity = new NodeEntity();
        $nodeEntity->process($request->getValidatedData(), 'Article');
        $nodeEntity->insert();

        $this->uploadFiles($request, $nodeEntity);

        return redirect('?action=editArticle&id='.$nodeEntity->id());
    }

    /**
     * @param UpdateArticle $request
     * @return Response
     * @throws \ImagickException
     */
    public function updateArticle(UpdateArticle $request) :Response
    {
        $data = $request->getValidatedData();
        $nodeEntity = NodesModel::findOrFail($data['id']);
        $nodeEntity->process($data);
        $nodeEntity->update();

        $this->uploadFiles($request, $nodeEntity);

        return redirect()->back();
    }
}