<?php

namespace App\Controllers\Backend;

use App\Traits\App;
use App\Interfaces\Request;
use App\Controllers\Controller;
use App\Traits\ExtraFieldsHelper;
use App\Entities\ExtraFieldEntity;
use App\Requests\InsertExtraField;
use App\Requests\UpdateExtraField;
use App\Models\Backend\ExtraFieldsGroups;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\Backend\ExtraFields as ExtraFieldsModel;

class ExtraFields extends Controller
{
    use ExtraFieldsHelper;

    /**
     * @var ExtraFieldsModel
     */
    private $model;

    /**
     * ExtraFields constructor.
     */
    public function __construct()
    {
        $this->model = new ExtraFieldsModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showExtraFields(Request $request) :Response
    {
        $nodes = new \App\Models\Backend\Nodes();
        $childs = $nodes->getChilds($this->node());

        return view($this->node()->view(), $childs)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showExtraFieldsList(Request $request) :Response
    {
        $extraFieldsGroups = new ExtraFieldsGroups();

        $data = array(
            'rows' => $this->model->getExtraFieldsList(),
            'groups' => $extraFieldsGroups->getExtraFieldsGroups()
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editExtraField(Request $request) :Response
    {
        $id = $request->inputs('id', 'INT');
        $data['extra_field'] = ExtraFieldsModel::findOrFail($id);

        $extraFieldsGroups = new ExtraFieldsGroups();
        $data['groups'] = $extraFieldsGroups->getExtraFieldsGroups();
        $data['types'] = $this->extraFieldsTypes();
        $data['options'] = $this->renderExtraFieldsTypes($data['extra_field']->type(), $data['extra_field']->info());

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addExtraField(Request $request) :Response
    {
        $extraFieldsGroups = new ExtraFieldsGroups();
        $data['groups'] = $extraFieldsGroups->getExtraFieldsGroups();
        $data['types'] = $this->extraFieldsTypes();

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param UpdateExtraField $request
     * @return Response
     */
    public function updateExtraField(UpdateExtraField $request) :Response
    {
        $data = $request->getValidatedData();
        $extraField = ExtraFieldsModel::findOrFail($data['id']);
        $extraField->process($data);
        $extraField->update();

        return redirect()->back();
    }

    /**
     * @param InsertExtraField $request
     * @return Response
     */
    public function insertExtraField(InsertExtraField $request) :Response
    {
        $extraField = new ExtraFieldEntity();
        $extraField->process($request->getValidatedData());
        $extraField->insert();

        return redirect('?action=editExtraField&id='.$extraField->id());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deleteExtraField(Request $request) :Response
    {
        $this->validate($request->inputs(),
            ['extra_field_checkbox' => 'required'],
            ['extra_field_checkbox.required' => 'Please select extra fields to delete.']
        );

        foreach ($request->inputs('extra_field_checkbox') as $id => $value) {
            $extraField = ExtraFieldsModel::findOrFail(App::sanitizeInput($id, 'INT'));
            $extraField->delete();
        }

        flash_message('success', 'Extra Field(s) Deleted');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function renderExtraFieldType(Request $request) :Response
    {
        $values = '';
        $type = $request->inputs('type');
        $id = $request->inputs('id', 'INT');
        $group = $request->inputs('group_id', 'INT');
        $view = $request->inputs('view');

        if(!empty($group)) {
            if($view == 'edit') {
                $extraField = $this->model->getExtraFields(['id' => $id, 'group_id' => $group]);
            }

            if(!empty($extraField)) {
                $values = $extraField[0]->info();
            }

            $data['message']  = $this->renderExtraFieldsTypes($type, $values);
            $data['success']  = true;
        }
        else {
            $data = array(
                'success' => false,
                'message' => '<div class="alert alert-warning">
                                    <button class="close" data-close="alert"></button>
                                    <span><strong>Please select a group first!</strong></span>
                                   </div>'
            );
        }

        return response()->withJson($data, 200);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function updateExtraFieldsOrder(Request $request) :Response
    {
        try
        {
            $data = [];
            $extraFields  = empty($request->inputs('extraFields')) ? [] : $request->inputs('extraFields');

            if(count($extraFields) > 0) {
                $data = array(
                    'success' => true,
                    'message' => 'Order Updated!',
                    'type' => 'success',
                    'extraFields' => $extraFields
                );

                foreach ($extraFields as $id => $order) {
                    $extraField = ExtraFieldsModel::findById($id);
                    $extraField->setOrdering($order);
                    $extraField->update();
                }
            }
        }
        catch (\Throwable $e)
        {
            $data = array(
                'success' => false,
                'message' => $e->getMessage(),
                'type' => 'danger'
            );
        }

        return response()->withJson($data, 200);
    }
}