<?php

namespace App\Controllers\Backend;

use App\Interfaces\Request;
use App\Controllers\Controller;
use App\Facades\Cache as SystemCache;
use App\Models\Backend\Cache as CacheModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Cache extends Controller
{
    /**
     * @var CacheModel
     */
    private $model;

    /**
     * Clients constructor.
     */
    public function __construct()
    {
        $this->model = new CacheModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showCacheList(Request $request) :Response
    {
        $data['rows'] = $this->model->getCacheList();

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deleteCache(Request $request) :Response
    {
        $data = (array) $request->getParsedBody();

        if(isset($data['delete_selected'])) {
            $this->deleteSelectedCache($data['cachedObjects']);
        }
        else if(isset($data['delete_all'])) {
            SystemCache::flush();
            flash_message('success', 'Cache cleared!');
        }

        return redirect()->back();
    }

    /**
     * @param $cachedObjects
     */
    private function deleteSelectedCache($cachedObjects)
    {
        foreach ($cachedObjects as $cachedObject => $value) {
            if (SystemCache::delete($cachedObject)) {
                flash_message('success', '"'.$cachedObject.'"'. ' successfully deleted');
            }
            else {
                flash_message('danger', '"'.$cachedObject.'"'. ' could not be deleted');
            }
        }
    }
}