<?php

namespace App\Controllers\Backend;

use App\Traits\Media;
use App\Facades\Auth;
use App\Interfaces\Request;
use App\Models\Backend\Roles;
use App\Requests\UpdateProfile;
use App\Models\Backend\Users as UsersModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Profile extends Users
{
    use Media;

    /**
     * @param Request $request
     * @return Response
     */
    public function showProfile(Request $request) :Response
    {
        $user = UsersModel::findOrFail(['id' => user()->id()], ['permissions', 'fields']);
        $roles = new Roles();

        $data = array(
            'user' => $user,
            'roles' => $roles->getRoles(),
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param UpdateProfile $request
     * @return Response
     * @throws \ImagickException
     */
    public function updateProfile(UpdateProfile $request) :Response
    {
        $user = UsersModel::findOrFail(user()->id());

        $data = $request->getValidatedData();
        $user->process($data);
        $user->update();

        if ($request->getImageUploaded()) {
            $this->uploadUserImage($request->getUploadedFiles()['new_avatar'], $user);
        }

        if(user()->id() === $user->id()) {
            Auth::setUserSession($user);
        }

        return redirect()->back();
    }
}