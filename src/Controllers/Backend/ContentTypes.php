<?php

namespace App\Controllers\Backend;

use App\Traits\App;
use App\Interfaces\Request;
use App\Controllers\Controller;
use App\Entities\ContentTypeEntity;
use App\Requests\UpdateContentType;
use App\Requests\InsertContentType;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\Backend\ContentTypes as ContentTypesModel;

class ContentTypes extends Controller
{
    /**
     * @var ContentTypesModel
     */
    private $model;

    /**
     * ContentTypes constructor.
     */
    public function __construct()
    {
        $this->model = new ContentTypesModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showContentTypesList(Request $request) :Response
    {
        $data['rows'] = $this->model->getContentTypesList();

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editContentType(Request $request) :Response
    {
        $id = $request->inputs('id', 'INT');
        $data['content_type'] = ContentTypesModel::findOrFail($id);

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addContentType(Request $request) :Response
    {
        return view($this->node()->view())->render();
    }

    /**
     * @param UpdateContentType $request
     * @return Response
     */
    public function updateContentType(UpdateContentType $request) :Response
    {
        $data = $request->getValidatedData();
        $contentType = ContentTypesModel::findOrFail($data['id']);
        $contentType->process($data);
        $contentType->update();

        return redirect()->back();
    }

    /**
     * @param InsertContentType $request
     * @return Response
     */
    public function insertContentType(InsertContentType $request) :Response
    {
        $contentType = new ContentTypeEntity();
        $contentType->process($request->getValidatedData());
        $contentType->insert();

        return redirect('?action=editContentType&id='.$contentType->id());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deleteContentType(Request $request) :Response
    {
        $this->validate($request->inputs(),
            ['content_checkbox' => 'required'],
            ['content_checkbox.required' => 'Please select content type to delete.']
        );

        foreach ($request->inputs('content_checkbox') as $id => $value) {
            $contentType = ContentTypesModel::findOrFail(App::sanitizeInput($id, 'INT'));
            $contentType->delete();
        }

        flash_message('success', 'Content Type(s) deleted!');

        return redirect()->back();
    }
}