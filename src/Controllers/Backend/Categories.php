<?php

namespace App\Controllers\Backend;

use App\Interfaces\Request;
use App\Entities\NodeEntity;
use App\Requests\InsertCategory;
use App\Requests\UpdateCategory;
use App\Models\Backend\Nodes as NodesModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Categories extends Nodes
{
    /**
     * @param Request $request
     * @return Response
     */
    public function showCategoriesList(Request $request) :Response
    {
        $data = $this->model->getNodesList($backend = false, $type = 2);

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addCategory(Request $request) :Response
    {
        $translation['from_id'] = $request->inputs('from_id', 'INT');
        $translation['iso_code'] = $request->inputs('iso_code');

        $data = $this->model->addNodeEntity($type = 2, $backend = 0, $this->node(), $translation, $getHomepage = 1);

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editCategory(Request $request) :Response
    {
        $id = $request->inputs('id', 'INT');
        $data = $this->model->editNodeEntity($id, $type = 2, $getHomepage = 1);

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param UpdateCategory $request
     * @return Response
     * @throws \ImagickException
     */
    public function updateCategory(UpdateCategory $request) :Response
    {
        // Update category
        $nodeEntity = NodesModel::findById($request->inputs('id', 'INT'));
        $nodeEntity->process($request->getValidatedData());
        $nodeEntity->update();

        // Upload Files
        $this->uploadFiles($request, $nodeEntity);

        return redirect()->back();
    }

    /**
     * @param InsertCategory $request
     * @return Response
     * @throws \ImagickException
     */
    public function insertCategory(InsertCategory $request) :Response
    {
        // Insert new article
        $nodeEntity = new NodeEntity();
        $nodeEntity->process($request->getValidatedData(), 'Category');
        $nodeEntity->insert();

        // Upload Files
        $this->uploadFiles($request, $nodeEntity);

        return redirect('?action=editCategory&id='.$nodeEntity->id());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deleteCategory(Request $request) :Response
    {
        return $this->deleteNode($request);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deleteCategoryTranslation(Request $request) :Response
    {
        $this->deleteNodeTranslation($request);

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getCategories(Request $request): Response
    {
        return $this->getNodes($request);
    }
}