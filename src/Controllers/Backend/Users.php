<?php

namespace App\Controllers\Backend;

use App\Traits\App;
use App\Facades\Session;
use App\Interfaces\Request;
use App\Entities\UserEntity;
use App\Requests\UpdateUser;
use App\Requests\InsertUser;
use App\Models\Backend\Roles;
use App\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\Backend\Users as UsersModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Users extends Controller
{
    /**
     * @var UsersModel
     */
    private $model;

    /**
     * Users constructor.
     */
    public function __construct()
    {
        $this->model = new UsersModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showUsersList(Request $request) :Response
    {
        $roles = new Roles();

        $data = array(
            'rows' => $this->model->getUsersList(),
            'roles' => $roles->getRoles()
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editUser(Request $request) :Response
    {
        $id = $request->inputs('id', 'INT');
        $user = UsersModel::findOrFail($id, ['*']);
        $roles = new Roles();
        $permissions = $user->permissions()->transform(function ($value){
            return $value->id();
        })->toArray();

        $data = array(
            'user' => $user,
            'permissions' => App::createPermissionsHtmlList($permissions),
            'roles' => $roles->getRoles(),
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addUser(Request $request) :Response
    {
        $roles = new Roles();
        $permissions = !empty(Session::get('insert_user')['permissions']) ? explode(',', Session::get('insert_user')['permissions']) : array();

        $data = array(
            'permissions' => App::createPermissionsHtmlList($permissions),
            'roles' => $roles->getRoles(),
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param UpdateUser $request
     * @return Response
     */
    public function updateUser(UpdateUser $request) :Response
    {
        $data = $request->getValidatedData();
        $user = UsersModel::findOrFail($data['id'], ['*']);
        $user->process($data);
        $user->update();

        return redirect()->back();
    }

    /**
     * @param InsertUser $request
     * @return Response
     */
    public function insertUser(InsertUser $request) :Response
    {
        $userEntity = new UserEntity();
        $userEntity->process($request->getValidatedData());
        $userEntity->insert();

        return redirect('?action=editUser&id='.$userEntity->id());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deleteUser(Request $request) :Response
    {
        $this->validate($request->inputs(),
            [
                'user_checkbox' => 'required',
                'user_checkbox.*' => Rule::notIn([1, 2])
            ],
            [
                'user_checkbox.required' => 'Please select users to delete.',
                'user_checkbox.*' => 'You cannot delete Administrator and Super Administrator users!'
            ]
        );

        foreach ($request->inputs('user_checkbox') as $user => $role) {
            $userEntity = UsersModel::findOrFail($user, ['*']);
            $userEntity->delete();
        }

        flash_message('success', 'User(s) Deleted');

        return redirect()->back();
    }
}