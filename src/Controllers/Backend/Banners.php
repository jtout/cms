<?php

namespace App\Controllers\Backend;

use App\Traits\App;
use App\Facades\Session;
use App\Interfaces\Request;
use App\Models\Backend\Menus;
use App\Models\Backend\Nodes;
use App\Traits\BannersHelper;
use App\Entities\BannerEntity;
use App\Requests\UpdateBanner;
use App\Requests\InsertBanner;
use App\Controllers\Controller;
use App\Models\Backend\Clients;
use App\Models\Backend\BannersPositions;
use App\Models\Backend\Banners as BannersModel;
use \Psr\Http\Message\ResponseInterface as Response;

class Banners extends Controller
{
    use BannersHelper;

    /**
     * @var BannersModel
     */
    private $model;

    /**
     * Banners constructor.
     */
    public function __construct()
    {
        $this->model = new BannersModel();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showBanners(Request $request) :Response
    {
        $nodes = new Nodes();
        $childs = $nodes->getChilds($this->node());

        return view($this->node()->view(), $childs)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showBannersList(Request $request) :Response
    {
        $clients = new Clients();
        $positions = new BannersPositions();

        $data = array(
            'rows' => $this->model->getBannersList(),
            'clients' => $clients->getClients(),
            'positions' => $positions->getPositions()
        );

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editBanner(Request $request) :Response
    {
        $id = $request->inputs('id', 'INT');
        $banner = BannersModel::findOrFail($id);

        $clients = new Clients();
        $positions = new BannersPositions();
        $menus = new Menus();
        $nodes = new Nodes();

        $data = array(
            'clients' => $clients->getClients(),
            'positions' => $positions->getPositions(),
            'banner' => $banner,
            'menus' => $menus->getMenus(0, explode(',', $banner->nodes()), true),
            'categories' => $nodes->getNodes($type = 2, $backend = 0, $languageId = 0, $getHomepage = 0)
        );

        $values['field'] = $banner->field();
        $values['mobile_field'] = $banner->mobile_field();
        $data['field'] = $this->renderBannerType($data['banner']->type(), $values);

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addBanner(Request $request) :Response
    {
        $clients = new Clients();
        $positions = new BannersPositions();
        $menus = new Menus();
        $nodes = new Nodes();

        $data = array(
            'clients' => $clients->getClients(),
            'positions' => $positions->getPositions(),
            'menus' => $menus->getMenus(0, array(), true),
            'categories' => $nodes->getNodes($type = 2, $backend = 0, $languageId = 0, $getHomepage = 0)
        );

        if (isset(Session::get('insert_banner')['type']) && isset(Session::get('insert_banner')['field'])) {
            $type = Session::get('insert_banner')['type'];
            $values['field'] = Session::get('insert_banner')['field'];
            $values['mobile_field'] = Session::get('insert_banner')['mobile_field'];
            $data['field'] = $this->renderBannerType($type, $values);
        }

        return view($this->node()->view(), $data)->render();
    }

    /**
     * @param InsertBanner $request
     * @return Response
     */
    public function insertBanner(InsertBanner $request) :Response
    {
        $banner = new BannerEntity();
        $banner->process($request->getValidatedData());
        $banner->insert();

        return redirect('?action=editBanner&id='.$banner->id());
    }

    /**
     * @param UpdateBanner $request
     * @return Response
     */
    public function updateBanner(UpdateBanner $request) :Response
    {
        $data = $request->getValidatedData();
        $banner = BannersModel::findOrFail($data['id']);
        $banner->process($data);
        $banner->update();

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function deleteBanner(Request $request) :Response
    {
        $this->validate($request->inputs(),
            ['banners_checkbox' => 'required'],
            ['banners_checkbox.required' => 'Please select banners to delete.']
        );

        foreach ($request->inputs('banners_checkbox') as $id => $value) {
            $banner = BannersModel::findById(App::sanitizeInput($id, 'INT'));
            $banner->delete();
        }

        flash_message('success', 'Banner(s) Deleted');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getBannerType(Request $request) :Response
    {
        $id = $request->inputs('id', 'INT');
        $type = $request->inputs('type');

        $banner = BannersModel::findById($id);
        $values['field'] = $banner->field();
        $values['mobile_field'] = $banner->mobile_field();

        $data = $this->renderBannerType($type, $values);

        return response()->withJson($data, 200);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function updateBannersOrder(Request $request) :Response
    {
        $data = [];
        $banners  = empty($request->inputs('banners')) ? []  : $request->inputs('banners');

        if(count($banners) > 0) {
            $data = array(
                'success' => true,
                'message' => 'Order Updated!',
                'type' => 'success',
                'banners' => $banners
            );

            foreach ($banners as $banner => $order) {
                $bannerEntity = BannersModel::findOrFail(App::sanitizeInput($banner, 'INT'));
                $bannerEntity->setOrdering($order);
                $bannerEntity->update();
            }
        }

        return response()->withJson($data, 200);
    }
}