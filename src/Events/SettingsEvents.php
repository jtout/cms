<?php

namespace App\Events;

use App\Entities\SettingsEntity;

class SettingsEvents
{
    /**
     * @param SettingsEntity $settings
     */
    public function inserted(SettingsEntity $settings)
    {
        
    }

    /**
     * @param SettingsEntity $settings
     */
    public function updated(SettingsEntity $settings)
    {
        
    }

    /**
     * @param SettingsEntity $settings
     */
    public function deleted(SettingsEntity $settings)
    {
        
    }

    /**
     * @param SettingsEntity $settings
     */
    public function inserting(SettingsEntity $settings)
    {

    }

    /**
     * @param SettingsEntity $settings
     */
    public function updating(SettingsEntity $settings)
    {

    }

    /**
     * @param SettingsEntity $settings
     */
    public function deleting(SettingsEntity $settings)
    {

    }
}