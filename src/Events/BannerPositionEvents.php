<?php

namespace App\Events;

use App\Entities\BannersPositionEntity;

class BannerPositionEvents
{
    /**
     * @param BannersPositionEntity $bannersPosition
     */
    public function inserted(BannersPositionEntity $bannersPosition)
    {

    }

    /**
     * @param BannersPositionEntity $bannersPosition
     */
    public function updated(BannersPositionEntity $bannersPosition)
    {

    }

    /**
     * @param BannersPositionEntity $bannersPosition
     */
    public function deleted(BannersPositionEntity $bannersPosition)
    {

    }

    /**
     * @param BannersPositionEntity $bannersPosition
     */
    public function inserting(BannersPositionEntity $bannersPosition)
    {

    }

    /**
     * @param BannersPositionEntity $bannersPosition
     */
    public function updating(BannersPositionEntity $bannersPosition)
    {

    }

    /**
     * @param BannersPositionEntity $bannersPosition
     */
    public function deleting(BannersPositionEntity $bannersPosition)
    {

    }
}