<?php

namespace App\Events;

use App\Entities\RoleEntity;

class RoleEvents
{
    /**
     * @param RoleEntity $role
     */
    public function inserted(RoleEntity $role)
    {

    }

    /**
     * @param RoleEntity $role
     */
    public function updated(RoleEntity $role)
    {

    }

    /**
     * @param RoleEntity $role
     */
    public function deleted(RoleEntity $role)
    {

    }

    /**
     * @param RoleEntity $role
     */
    public function inserting(RoleEntity $role)
    {

    }

    /**
     * @param RoleEntity $role
     */
    public function updating(RoleEntity $role)
    {

    }

    /**
     * @param RoleEntity $role
     */
    public function deleting(RoleEntity $role)
    {

    }
}