<?php

namespace App\Events;

use App\Entities\AccessLevelEntity;

class AccessLevelEvents
{
    /**
     * @param AccessLevelEntity $accessLevel
     */
    public function inserted(AccessLevelEntity $accessLevel)
    {

    }

    /**
     * @param AccessLevelEntity $accessLevel
     */
    public function updated(AccessLevelEntity $accessLevel)
    {

    }

    /**
     * @param AccessLevelEntity $accessLevel
     */
    public function deleted(AccessLevelEntity $accessLevel)
    {

    }

    /**
     * @param AccessLevelEntity $accessLevel
     */
    public function inserting(AccessLevelEntity $accessLevel)
    {

    }

    /**
     * @param AccessLevelEntity $accessLevel
     */
    public function updating(AccessLevelEntity $accessLevel)
    {

    }

    /**
     * @param AccessLevelEntity $accessLevel
     */
    public function deleting(AccessLevelEntity $accessLevel)
    {

    }
}