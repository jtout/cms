<?php

namespace App\Events;

use App\Entities\ModuleEntity;

class ModuleEvents
{
    /**
     * @param ModuleEntity $module
     */
    public function inserted(ModuleEntity $module)
    {

    }

    /**
     * @param ModuleEntity $module
     */
    public function updated(ModuleEntity $module)
    {

    }

    /**
     * @param ModuleEntity $module
     */
    public function deleted(ModuleEntity $module)
    {
        
    }

    /**
     * @param ModuleEntity $module
     */
    public function inserting(ModuleEntity $module)
    {

    }

    /**
     * @param ModuleEntity $module
     */
    public function updating(ModuleEntity $module)
    {

    }

    /**
     * @param ModuleEntity $module
     */
    public function deleting(ModuleEntity $module)
    {

    }
}