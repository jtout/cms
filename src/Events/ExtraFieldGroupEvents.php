<?php

namespace App\Events;

use App\Entities\ExtraFieldGroupEntity;

class ExtraFieldGroupEvents
{
    /**
     * @param ExtraFieldGroupEntity $extraFieldGroupGroup
     */
    public function inserted(ExtraFieldGroupEntity $extraFieldGroupGroup)
    {

    }

    /**
     * @param ExtraFieldGroupEntity $extraFieldGroup
     */
    public function updated(ExtraFieldGroupEntity $extraFieldGroup)
    {

    }

    /**
     * @param ExtraFieldGroupEntity $extraFieldGroup
     */
    public function deleted(ExtraFieldGroupEntity $extraFieldGroup)
    {

    }

    /**
     * @param ExtraFieldGroupEntity $extraFieldGroup
     */
    public function inserting(ExtraFieldGroupEntity $extraFieldGroup)
    {

    }

    /**
     * @param ExtraFieldGroupEntity $extraFieldGroup
     */
    public function updating(ExtraFieldGroupEntity $extraFieldGroup)
    {

    }

    /**
     * @param ExtraFieldGroupEntity $extraFieldGroup
     */
    public function deleting(ExtraFieldGroupEntity $extraFieldGroup)
    {

    }
}