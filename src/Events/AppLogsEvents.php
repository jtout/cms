<?php

namespace App\Events;

use App\Entities\AppLogEntity;

class AppLogsEvents
{
    /**
     * @param AppLogEntity $appLog
     */
    public function inserted(AppLogEntity $appLog)
    {

    }

    /**
     * @param AppLogEntity $appLog
     */
    public function updated(AppLogEntity $appLog)
    {

    }

    /**
     * @param AppLogEntity $appLog
     */
    public function deleted(AppLogEntity $appLog)
    {

    }

    /**
     * @param AppLogEntity $appLog
     */
    public function inserting(AppLogEntity $appLog)
    {

    }

    /**
     * @param AppLogEntity $appLog
     */
    public function updating(AppLogEntity $appLog)
    {

    }

    /**
     * @param AppLogEntity $appLog
     */
    public function deleting(AppLogEntity $appLog)
    {

    }
}