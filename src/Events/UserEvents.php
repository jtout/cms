<?php

namespace App\Events;

use App\Facades\Mail;
use App\Observers\Joomla;
use App\Entities\UserEntity;
use App\Mails\NotificationMail;
use App\Mails\RegistrationMail;

class UserEvents
{
    /**
     * @param UserEntity $user
     */
    public function inserted(UserEntity $user)
    {
        if ($user->activation_token() != 'from-facebook') {
            $mailEntity = new RegistrationMail($user);
            Mail::send($mailEntity);
        }
    }

    /**
     * @param UserEntity $user
     */
    public function updated(UserEntity $user)
    {
        if ($user->isDirty('in_backend') && $user->in_backend() == 1) {
            $mailEntity = new NotificationMail('Caution! '.$user->username() .' can now access backend!');
            Mail::send($mailEntity);
        }
    }

    /**
     * @param UserEntity $user
     */
    public function deleted(UserEntity $user)
    {

    }

    /**
     * @param UserEntity $user
     */
    public function inserting(UserEntity $user)
    {

    }

    /**
     * @param UserEntity $user
     */
    public function updating(UserEntity $user)
    {

    }

    /**
     * @param UserEntity $user
     */
    public function deleting(UserEntity $user)
    {

    }
}