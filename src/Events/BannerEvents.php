<?php

namespace App\Events;

use App\Observers\Cache;
use App\Entities\BannerEntity;

class BannerEvents
{
    /**
     * @param BannerEntity $banner
     */
    public function inserted(BannerEntity $banner)
    {

    }

    /**
     * @param BannerEntity $banner
     */
    public function updated(BannerEntity $banner)
    {

    }

    /**
     * @param BannerEntity $banner
     */
    public function deleted(BannerEntity $banner)
    {

    }

    /**
     * @param BannerEntity $banner
     */
    public function inserting(BannerEntity $banner)
    {

    }

    /**
     * @param BannerEntity $banner
     */
    public function updating(BannerEntity $banner)
    {

    }

    /**
     * @param BannerEntity $banner
     */
    public function deleting(BannerEntity $banner)
    {

    }
}