<?php

namespace App\Events;

use App\Entities\ContentTypeEntity;

class ContentTypeEvents
{
    /**
     * @param ContentTypeEntity $contentType
     */
    public function inserted(ContentTypeEntity $contentType)
    {

    }

    /**
     * @param ContentTypeEntity $contentType
     */
    public function updated(ContentTypeEntity $contentType)
    {

    }

    /**
     * @param ContentTypeEntity $contentType
     */
    public function deleted(ContentTypeEntity $contentType)
    {

    }

    /**
     * @param ContentTypeEntity $contentType
     */
    public function inserting(ContentTypeEntity $contentType)
    {

    }

    /**
     * @param ContentTypeEntity $contentType
     */
    public function updating(ContentTypeEntity $contentType)
    {

    }

    /**
     * @param ContentTypeEntity $contentType
     */
    public function deleting(ContentTypeEntity $contentType)
    {

    }
}