<?php

namespace App\Events;

use App\Entities\ExtraFieldEntity;

class ExtraFieldEvents
{
    /**
     * @param ExtraFieldEntity $extraField
     */
    public function inserted(ExtraFieldEntity $extraField)
    {

    }

    /**
     * @param ExtraFieldEntity $extraField
     */
    public function updated(ExtraFieldEntity $extraField)
    {

    }

    /**
     * @param ExtraFieldEntity $extraField
     */
    public function deleted(ExtraFieldEntity $extraField)
    {

    }

    /**
     * @param ExtraFieldEntity $extraField
     */
    public function inserting(ExtraFieldEntity $extraField)
    {

    }

    /**
     * @param ExtraFieldEntity $extraField
     */
    public function updating(ExtraFieldEntity $extraField)
    {

    }

    /**
     * @param ExtraFieldEntity $extraField
     */
    public function deleting(ExtraFieldEntity $extraField)
    {

    }
}