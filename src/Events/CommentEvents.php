<?php

namespace App\Events;

use App\Observers\Cache;
use App\Entities\CommentEntity;

class CommentEvents
{
    /**
     * @param CommentEntity $comment
     */
    public function inserted(CommentEntity $comment)
    {
        $comment->fireObserverEvent('delete', Cache::class);
    }

    /**
     * @param CommentEntity $comment
     */
    public function updated(CommentEntity $comment)
    {
        $comment->fireObserverEvent('delete', Cache::class);
    }

    /**
     * @param CommentEntity $comment
     */
    public function deleted(CommentEntity $comment)
    {
        $comment->fireObserverEvent('delete', Cache::class);
    }

    /**
     * @param CommentEntity $comment
     */
    public function inserting(CommentEntity $comment)
    {

    }

    /**
     * @param CommentEntity $comment
     */
    public function updating(CommentEntity $comment)
    {

    }

    /**
     * @param CommentEntity $comment
     */
    public function deleting(CommentEntity $comment)
    {

    }
}