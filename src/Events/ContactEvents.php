<?php

namespace App\Events;

use App\Entities\ContactEntity;

class ContactEvents
{
    /**
     * @param ContactEntity $contact
     */
    public function inserted(ContactEntity $contact)
    {

    }

    /**
     * @param ContactEntity $contact
     */
    public function updated(ContactEntity $contact)
    {

    }

    /**
     * @param ContactEntity $contact
     */
    public function deleted(ContactEntity $contact)
    {

    }

    /**
     * @param ContactEntity $contact
     */
    public function inserting(ContactEntity $contact)
    {

    }

    /**
     * @param ContactEntity $contact
     */
    public function updating(ContactEntity $contact)
    {

    }

    /**
     * @param ContactEntity $contact
     */
    public function deleting(ContactEntity $contact)
    {

    }
}