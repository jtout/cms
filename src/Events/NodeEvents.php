<?php

namespace App\Events;

use App\Traits\App;
use App\Observers\Cache;
use App\Entities\NodeEntity;

class NodeEvents
{
    /**
     * @param NodeEntity $node
     */
    public function inserted(NodeEntity $node)
    {
        $node->fireObserverEvent('delete', Cache::class);
    }

    /**
     * @param NodeEntity $node
     */
    public function updated(NodeEntity $node)
    {
        $node->fireObserverEvent('delete', Cache::class);
    }

    /**
     * @param NodeEntity $node
     */
    public function deleted(NodeEntity $node)
    {
        $node->fireObserverEvent('delete', Cache::class);

        $sourceDir  = env('MEDIA_DIRECTORY').'/source/'.$node->content_type_media_folder().'/'.$node->id();
        $thumbsDir  = env('MEDIA_DIRECTORY').'/thumbs/'.$node->content_type_media_folder().'/'.$node->id();
        App::removeDirectory($sourceDir);
        App::removeDirectory($thumbsDir);
    }

    /**
     * @param NodeEntity $node
     */
    public function inserting(NodeEntity $node)
    {

    }

    /**
     * @param NodeEntity $node
     */
    public function updating(NodeEntity $node)
    {

    }

    /**
     * @param NodeEntity $node
     */
    public function deleting(NodeEntity $node)
    {

    }
}