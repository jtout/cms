<?php

namespace App\Events;

use App\Entities\LanguageEntity;

class LanguageEvents
{
    /**
     * @param LanguageEntity $language
     */
    public function inserted(LanguageEntity $language)
    {

    }

    /**
     * @param LanguageEntity $language
     */
    public function updated(LanguageEntity $language)
    {

    }

    /**
     * @param LanguageEntity $language
     */
    public function deleted(LanguageEntity $language)
    {

    }

    /**
     * @param LanguageEntity $language
     */
    public function inserting(LanguageEntity $language)
    {

    }

    /**
     * @param LanguageEntity $language
     */
    public function updating(LanguageEntity $language)
    {

    }

    /**
     * @param LanguageEntity $language
     */
    public function deleting(LanguageEntity $language)
    {

    }
}