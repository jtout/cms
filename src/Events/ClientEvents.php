<?php

namespace App\Events;

use App\Entities\ClientEntity;

class ClientEvents
{
    /**
     * @param ClientEntity $client
     */
    public function inserted(ClientEntity $client)
    {

    }

    /**
     * @param ClientEntity $client
     */
    public function updated(ClientEntity $client)
    {

    }

    /**
     * @param ClientEntity $client
     */
    public function deleted(ClientEntity $client)
    {

    }

    /**
     * @param ClientEntity $client
     */
    public function inserting(ClientEntity $client)
    {

    }

    /**
     * @param ClientEntity $client
     */
    public function updating(ClientEntity $client)
    {

    }

    /**
     * @param ClientEntity $client
     */
    public function deleting(ClientEntity $client)
    {

    }
}