<?php

if (!function_exists('device_is_mobile'))
{
    /**
     * @return bool
     */
    function device_is_mobile()
    {
        $device = new Mobile_Detect();

        return $device->isMobile();
    }
}

if (!function_exists('device_is_tablet'))
{
    /**
     * @return bool
     */
    function device_is_tablet()
    {
        $device = new Mobile_Detect();

        return $device->isTablet();
    }
}

if (!function_exists('cookies'))
{
    /**
     * @return \App\Services\CookiesService
     */
    function cookies()
    {
        return app('cookies');
    }
}

if (!function_exists('csrf'))
{
    /**
     * @return \App\Services\CsrfService
     */
    function csrf()
    {
        return app('csrf');
    }
}

if (!function_exists('db'))
{
    /**
     * @return \Illuminate\Database\Connection
     */
    function db()
    {
        return app('db')->getConnection();
    }
}

if (!function_exists('view'))
{
    /**
     * @param string $template
     * @param array $attributes
     * @param int $status
     * @return \App\Services\ViewService
     */
    function view(string $template, array $attributes = [], $status = 200)
    {
        $view = app('view');

        if (func_num_args() === 0) {
            return $view;
        }
        else {
            return $view->make($template, $attributes, $status);
        }
    }
}

if (!function_exists('config'))
{
    function config()
    {
       $configuration = \App\System\Configuration::initialize();

       return $configuration;
    }
}

if (!function_exists('request'))
{
    /**
     * @return \App\Interfaces\Request
     */
    function request()
    {
        return app('request');
    }
}

if (!function_exists('response'))
{
    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    function response()
    {
        return app('response');
    }
}

if (!function_exists('redirect'))
{
    /**
     * @return \App\Services\RedirectResponseService
     */
    function redirect($url = null, $status = null)
    {
        $response = app('redirectResponse');

        if (func_num_args() === 0 || is_null($url)) {
            return $response;
        }
        else {
            return $response->withRedirect($url, $status);
        }
    }
}

if (!function_exists('app'))
{
    /**
     * @param null $service
     * @return mixed|\Slim\Container
     */
    function app($service = null)
    {
        $container =  \App\Facades\Container::self();

        if (is_null($service)) {
            return $container;
        }
        else {
            return $container->get($service);
        }
    }
}

if (!function_exists('flash_message'))
{
    /**
     * @param string $type
     * @param string $message
     */
    function flash_message(string $type, string $message)
    {
        session()->flash()->addMessage($type, $message);
    }
}

if (!function_exists('abort'))
{
    /**
     * @param int $code
     * @param string $message
     * @param null $exception
     * @return mixed
     */
    function abort(int $code, $message = "", $exception = null)
    {
        return app('errorHandler')->generateManualException($code, $message, $exception);
    }
}

if (!function_exists('user'))
{
    /**
     * @return \App\Entities\UserEntity
     */
    function user()
    {
        return auth()->user();
    }
}

if (!function_exists('auth'))
{
    /**
     * @return \App\Services\AuthService
     */
    function auth()
    {
        return app('auth');
    }
}

if (!function_exists('now'))
{
    /**
     * @return false|string
     */
    function now()
    {
        return date("Y-m-d H:i:s");
    }
}

if (!function_exists('timestamp'))
{
    /**
     * @return int
     */
    function timestamp()
    {
        $now = new \DateTime();
        return $now->getTimestamp();
    }
}

if (!function_exists('email'))
{
    /**
     * @return \App\Services\MailService
     */
    function email()
    {
        return app('mail');
    }
}

if (!function_exists('csrf'))
{
    /**
     * @return \App\Services\CsrfService
     */
    function csrf()
    {
        return app('csrf');
    }
}

if (!function_exists('session'))
{
    /**
     * @return \App\Services\SessionService
     */
    function session()
    {
        return app('session');
    }
}

if (!function_exists('cache'))
{
    /**
     * @return \App\Services\CacheService
     */
    function cache()
    {
        return app('cache');
    }
}

if (!function_exists('cache'))
{
    function cache()
    {
        $session = \App\Facades\Cache::self();

        return $session;
    }
}

if (!function_exists('navigation'))
{
    /**
     * @param bool $withOrder
     * @param bool $withSort
     * @return string
     */
    function navigation($withOrder = true, $withSort = true)
    {
        $nav = request()->getUri()->getPath();

        if ($withOrder) {
            $nav .= '?o='.session()->get('order');
        }

        if ($withSort) {
            $nav .= '?s='.session()->get('sort');

            if ($withOrder) {
                replace_characters($nav, '?o=', '&o=');
            }
        }

        return $nav;
    }
}

if (!function_exists('node'))
{
    /**
     * @return \App\Entities\NodeEntity
     */
    function node()
    {
        return session()->getNode();
    }
}

if (!function_exists('sorting'))
{
    /**
     * @return mixed|string
     */
    function sorting()
    {
        return session()->get('sort');
    }
}

if (!function_exists('sorting_icon'))
{
    /**
     * @return mixed|string
     */
    function sorting_icon()
    {
        return session()->get('icon');
    }
}

if (!function_exists('sorting_icon_default'))
{
    /**
     * @return mixed|string
     */
    function sorting_icon_default()
    {
        return session()->get('icon_default');
    }
}

if (!function_exists('hash_string'))
{
    /**
     * @param string $string
     * @param int $algorithm
     * @return bool|string
     */
    function hash_string(string $string, $algorithm = PASSWORD_BCRYPT)
    {
        return password_hash($string, $algorithm);
    }
}

if (!function_exists('remove_spaces'))
{
    /**
     * @param string $string
     * @return string|string[]|null
     */
    function remove_spaces(string $string)
    {
        $string = replace_characters($string, '%20', '');

        return preg_replace("/\s+/u", '', $string);
    }
}

if (!function_exists('replace_characters'))
{
    /**
     * @param string $string
     * @param string $search
     * @param string $replaceWith
     * @return mixed|string
     */
    function replace_characters(string $string, string $search, string $replaceWith = '<br />')
    {
        if (Illuminate\Support\Str::contains($string, $search)) {
            $string = str_replace($search, $replaceWith, $string);
        }

        return $string;
    }
}

if (!function_exists('env'))
{
    /**
     * @param $key
     * @param null $default
     * @return array|false|string|null
     */
    function env($key, $default = null)
    {
        $value = getenv($key);
        if ($value === false) {
            return $default;
        }
        
        return $value;
    }
}

if (!function_exists('app_dir'))
{
    /**
     * @return string
     */
    function app_dir()
    {
        return dirname(__DIR__);
    }
}

if (!function_exists('dark_mode'))
{
    /**
     * @return bool
     */
    function dark_mode()
    {
        if (cookies()->get(SITENAME.'_dark_mode') == 1 || session()->get('user.dark_mode') == 1) {
            return true;
        }

        return false;
    }
}

if (!function_exists('get_user_info_by_ip'))
{
    /**
     * @return stdClass
     */
    function get_user_info_by_ip()
    {
        $remote  = !isset($_SERVER["HTTP_CF_CONNECTING_IP"]) ? $_SERVER['REMOTE_ADDR'] : $_SERVER["HTTP_CF_CONNECTING_IP"];
        $client  = isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : $_SERVER['REMOTE_ADDR'];
        $forward = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
        $result  = new stdClass();

        if (filter_var($remote, FILTER_VALIDATE_IP)) {
            $ip = $remote;
        }
        else if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        }
        else {
            $ip = $forward;
        }

        $result->iso_code = geoip_country_code_by_name($ip);
        $result->country = geoip_country_name_by_name($ip);
        $result->ip = $ip;

        return $result;
    }
}