<?php

namespace App\Providers;

use Pimple\Container;
use App\Services\DetectAttacksService;

class DetectAttacksServiceProvider extends ServiceProvider
{
    /**
     * @param Container $container
     */
    public function register(Container $container)
    {
        $container['detect_attacks'] = function () {
            return DetectAttacksService::boot();
        };
    }
}