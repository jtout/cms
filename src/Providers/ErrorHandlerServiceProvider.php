<?php

namespace App\Providers;

use Pimple\Container;
use App\Services\ErrorHandlerService;

class ErrorHandlerServiceProvider extends ServiceProvider
{
    /**
     * @param Container $container
     */
    public function register(Container $container)
    {
        $container['phpErrorHandler'] = $container['errorHandler'] = function () {
            return ErrorHandlerService::boot();
        };
    }
}