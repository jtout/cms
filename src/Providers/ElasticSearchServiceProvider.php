<?php

namespace App\Providers;

use Pimple\Container;
use App\Services\ElasticSearchService;

class ElasticSearchServiceProvider extends ServiceProvider
{
    /**
     * @param Container $container
     * @return mixed|void
     */
    public function register(Container $container)
    {
        $container['elasticSearch'] = function () {
           return ElasticSearchService::boot();
        };
    }
}