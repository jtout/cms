<?php

namespace App\Providers;

use Pimple\Container;
use App\Services\RedirectResponseService;

class RedirectResponseServiceProvider extends ServiceProvider
{
    /**
     * @param Container $container
     * @return mixed|void
     */
    public function register(Container $container)
    {
        $container['redirectResponse'] = function () {
            return RedirectResponseService::boot();
        };
    }
}