<?php

namespace App\Providers;

use Pimple\Container;
use App\Services\CookiesService;

class CookiesServiceProvider extends ServiceProvider
{
    /**
     * @param Container $container
     * @return mixed|void
     */
    public function register(Container $container)
    {
        $container['cookies'] = function () {
           return CookiesService::boot();
        };
    }
}