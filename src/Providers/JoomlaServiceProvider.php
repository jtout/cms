<?php

namespace App\Providers;

use Pimple\Container;
use App\Services\JoomlaService;

class JoomlaServiceProvider extends ServiceProvider
{
    /**
     * @param Container $container
     * @return mixed|void
     */
    public function register(Container $container)
    {
        $container['joomla'] = function () {
           return JoomlaService::boot();
        };
    }
}