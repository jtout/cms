<?php

namespace App\Providers;

use Pimple\Container;
use App\Services\AssetsCompressionService;

class AssetsCompressionServiceProvider extends ServiceProvider
{
    /**
     * @param Container $container
     * @return mixed|void
     */
    public function register(Container $container)
    {
        $container['assetsCompression'] = function () {
           return AssetsCompressionService::boot();
        };
    }
}