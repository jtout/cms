<?php

namespace App\Providers;

use Pimple\Container;
use App\Services\ValidatorService;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * @param Container $container
     * @return mixed|void
     */
    public function register(Container $container)
    {
        $container['validator'] = function () {
            return ValidatorService::boot();
        };
    }
}