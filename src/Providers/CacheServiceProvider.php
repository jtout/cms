<?php

namespace App\Providers;

use Pimple\Container;
use App\Services\CacheService;

class CacheServiceProvider extends ServiceProvider
{
    /**
     * @param Container $container
     * @return mixed|void
     */
    public function register(Container $container)
    {
        $container['cache'] = function () {
           return CacheService::boot();
        };
    }
}