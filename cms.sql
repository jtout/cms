-- ----------------------------
-- Table structure for access_levels
-- ----------------------------
DROP TABLE IF EXISTS `access_levels`;
CREATE TABLE `access_levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(128) CHARACTER SET utf8 NOT NULL,
  `label` varchar(128) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `access_levels_ibfk_1` (`parent_id`) USING BTREE,
  KEY `created_by` (`created_by`) USING BTREE,
  KEY `updated_by` (`updated_by`) USING BTREE,
  CONSTRAINT `access_levels_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `access_levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of access_levels
-- ----------------------------
BEGIN;
INSERT INTO `access_levels` VALUES (1, NULL, 'Developer', 'label-danger', 1, '2018-01-15 15:22:51', 3, '2018-02-09 13:16:05');
INSERT INTO `access_levels` VALUES (2, 1, 'Admin', 'label-success', 1, '2018-01-15 15:22:51', 1, '2018-01-15 15:22:51');
INSERT INTO `access_levels` VALUES (3, 2, 'Special', 'label-info', 1, '2018-01-15 15:22:51', 1, '2018-01-21 00:05:09');
INSERT INTO `access_levels` VALUES (4, 3, 'User', 'label-default', 1, '2018-01-15 15:22:51', 1, '2018-01-21 00:05:17');
INSERT INTO `access_levels` VALUES (5, 4, 'Public', 'label-primary', 3, '2018-02-07 09:07:35', 3, '2019-10-06 00:45:26');
COMMIT;

-- ----------------------------
-- Table structure for access_levels_tree
-- ----------------------------
DROP TABLE IF EXISTS `access_levels_tree`;
CREATE TABLE `access_levels_tree` (
  `parent_id` int(11) NOT NULL,
  `child_id` int(11) NOT NULL,
  `depth` int(11) DEFAULT NULL,
  `path` mediumtext CHARACTER SET utf8,
  KEY `access_levels_tree_ibfk_1` (`parent_id`) USING BTREE,
  KEY `access_levels_tree_ibfk_2` (`child_id`) USING BTREE,
  CONSTRAINT `access_levels_tree_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `access_levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `access_levels_tree_ibfk_2` FOREIGN KEY (`child_id`) REFERENCES `access_levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of access_levels_tree
-- ----------------------------
BEGIN;
INSERT INTO `access_levels_tree` VALUES (1, 1, 0, '1/');
INSERT INTO `access_levels_tree` VALUES (2, 2, 0, '2/');
INSERT INTO `access_levels_tree` VALUES (1, 2, 1, '1/2/');
INSERT INTO `access_levels_tree` VALUES (3, 3, 0, '3/');
INSERT INTO `access_levels_tree` VALUES (2, 3, 1, '2/3/');
INSERT INTO `access_levels_tree` VALUES (1, 3, 2, '1/2/3/');
INSERT INTO `access_levels_tree` VALUES (4, 4, 0, '4/');
INSERT INTO `access_levels_tree` VALUES (3, 4, 1, '3/4/');
INSERT INTO `access_levels_tree` VALUES (2, 4, 2, '2/3/4/');
INSERT INTO `access_levels_tree` VALUES (1, 4, 3, '1/2/3/4/');
INSERT INTO `access_levels_tree` VALUES (5, 5, 0, '5/');
INSERT INTO `access_levels_tree` VALUES (4, 5, 1, '4/5/');
INSERT INTO `access_levels_tree` VALUES (3, 5, 2, '3/4/5/');
INSERT INTO `access_levels_tree` VALUES (2, 5, 3, '2/3/4/5/');
INSERT INTO `access_levels_tree` VALUES (1, 5, 4, '1/2/3/4/5/');
COMMIT;

-- ----------------------------
-- Table structure for app_logs
-- ----------------------------
DROP TABLE IF EXISTS `app_logs`;
CREATE TABLE `app_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `model_instance` varchar(255) NOT NULL,
  `info` longtext NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10643 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for banners
-- ----------------------------
DROP TABLE IF EXISTS `banners`;
CREATE TABLE `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `position_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `click_url` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `field` longtext,
  `mobile_field` longtext,
  `settings` longtext,
  `is_active` int(11) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_on` datetime DEFAULT NULL,
  `finished_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `banners_ibfk_1` (`position_id`) USING BTREE,
  KEY `banners_ibfk_2` (`client_id`) USING BTREE,
  CONSTRAINT `banners_ibfk_1` FOREIGN KEY (`position_id`) REFERENCES `positions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `banners_ibfk_2` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for banners_nodes
-- ----------------------------
DROP TABLE IF EXISTS `banners_nodes`;
CREATE TABLE `banners_nodes` (
  `banner_id` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  KEY `banner_id` (`banner_id`) USING BTREE,
  KEY `node_id` (`node_id`) USING BTREE,
  CONSTRAINT `banners_nodes_ibfk_1` FOREIGN KEY (`banner_id`) REFERENCES `banners` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `banners_nodes_ibfk_2` FOREIGN KEY (`node_id`) REFERENCES `nodes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for cache
-- ----------------------------
DROP TABLE IF EXISTS `cache`;
CREATE TABLE `cache` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `encoded_key` varchar(255) NOT NULL,
  `decoded_key` longtext CHARACTER SET utf8mb4 NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for clients
-- ----------------------------
DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `text` longtext NOT NULL,
  `node_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`,`node_id`,`updated_by`) USING BTREE,
  KEY `created_by` (`created_by`) USING BTREE,
  KEY `node_id` (`node_id`) USING BTREE,
  KEY `id` (`id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`node_id`) REFERENCES `nodes_content` (`node_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `comments_ibfk_3` FOREIGN KEY (`parent_id`) REFERENCES `comments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=255384 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for comments_likes
-- ----------------------------
DROP TABLE IF EXISTS `comments_likes`;
CREATE TABLE `comments_likes` (
  `comment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  KEY `node_id` (`node_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `comment_id` (`comment_id`) USING BTREE,
  CONSTRAINT `comments_likes_ibfk_1` FOREIGN KEY (`node_id`) REFERENCES `nodes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `comments_likes_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `comments_likes_ibfk_3` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `is_active` int(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `client_id` (`client_id`) USING BTREE,
  CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for content_types
-- ----------------------------
DROP TABLE IF EXISTS `content_types`;
CREATE TABLE `content_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `media_folder` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of content_types
-- ----------------------------
BEGIN;
INSERT INTO `content_types` VALUES (1, 'Article', 'Articles', 1, '2018-02-14 13:45:21', 1, '2018-02-14 13:45:21');
INSERT INTO `content_types` VALUES (2, 'Category', 'Categories', 1, '2018-02-14 13:45:21', 1, '2018-02-14 13:45:21');
INSERT INTO `content_types` VALUES (3, 'Page', 'Pages', 1, '2018-02-14 13:45:21', 1, '2018-02-14 13:45:21');
INSERT INTO `content_types` VALUES (4, 'Tag', 'Tags', 1, '2018-02-14 13:45:21', 1, '2018-02-14 13:45:21');
INSERT INTO `content_types` VALUES (5, 'Sitemap', NULL, 1, '2018-11-13 19:56:46', 1, '2018-11-13 19:56:46');
INSERT INTO `content_types` VALUES (6, 'User Page', NULL, 1, '2018-11-19 20:38:41', 1, '2018-11-19 20:38:41');
COMMIT;

-- ----------------------------
-- Table structure for extra_fields
-- ----------------------------
DROP TABLE IF EXISTS `extra_fields`;
CREATE TABLE `extra_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `info` longtext,
  `is_active` int(1) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `group_id` (`group_id`) USING BTREE,
  CONSTRAINT `extra_fields_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `extra_fields_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for extra_fields_groups
-- ----------------------------
DROP TABLE IF EXISTS `extra_fields_groups`;
CREATE TABLE `extra_fields_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `iso_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of languages
-- ----------------------------
BEGIN;
INSERT INTO `languages` VALUES (1, 'English', 'en', 1, 1, '2018-01-21 17:40:10', 1, '2018-01-21 17:40:19');
INSERT INTO `languages` VALUES (2, 'Greek', 'el', 1, 1, '2018-01-21 17:40:12', 1, '2018-01-21 17:40:21');
INSERT INTO `languages` VALUES (3, 'Italian', 'it', 0, 1, '2018-01-21 17:40:12', 1, '2018-12-08 13:23:04');
COMMIT;

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 NOT NULL,
  `language_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE,
  CONSTRAINT `menus_ibfk_1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for menus_links
-- ----------------------------
DROP TABLE IF EXISTS `menus_links`;
CREATE TABLE `menus_links` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) unsigned NOT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 NOT NULL,
  `node_id` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `class` varchar(25) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `ordering` int(11) DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `node_id` (`node_id`) USING BTREE,
  KEY `menu_id` (`menu_id`) USING BTREE,
  CONSTRAINT `menus_links_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `menus_links` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `menus_links_ibfk_2` FOREIGN KEY (`node_id`) REFERENCES `nodes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `menus_links_ibfk_3` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for modules
-- ----------------------------
DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `is_function` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `class` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `request_type` varchar(10) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `created_by` (`created_by`) USING BTREE,
  KEY `updated_by` (`updated_by`) USING BTREE,
  CONSTRAINT `modules_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=228 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of modules
-- ----------------------------
BEGIN;
INSERT INTO `modules` VALUES (1, 1, 0, 'System', '', '', '', 'System Modules', 1, 1, '2018-01-11 11:22:13', 1, '2018-01-21 00:44:47');
INSERT INTO `modules` VALUES (2, 1, 0, 'Backend', '', '', '', 'Backend Modules', 1, 1, '2018-01-11 11:22:13', 1, '2018-01-21 00:44:50');
INSERT INTO `modules` VALUES (3, 1, 0, 'Frontend', '', NULL, '', 'Frontend Modules', 1, 1, '2018-01-11 11:22:13', 1, '2019-10-01 11:12:47');
INSERT INTO `modules` VALUES (4, 2, 0, 'Panel', '', NULL, '', 'Panel Module', 1, 1, '2018-01-11 11:22:13', 1, '2019-10-01 11:12:47');
INSERT INTO `modules` VALUES (5, 4, 1, 'Dashboard', '\\App\\Http\\Controllers\\Backend\\Panel', 'dashboard', 'GET', 'Show Dashboard', 1, 1, '2018-01-11 11:22:13', 1, '2019-10-01 11:12:47');
INSERT INTO `modules` VALUES (6, 4, 1, 'Panel Login', '\\App\\Http\\Controllers\\Backend\\Panel', 'login', 'POST', 'Panel Login action', 1, 1, '2018-01-11 11:22:13', 3, '2019-10-02 10:51:53');
INSERT INTO `modules` VALUES (7, 4, 1, 'Panel Logout', '\\App\\Http\\Controllers\\Backend\\Panel', 'logout', 'GET', 'Panel Logout action', 1, 1, '2018-01-11 11:22:13', 3, '2019-10-02 10:51:53');
INSERT INTO `modules` VALUES (8, 3, 1, 'Frontpage', '\\App\\Http\\Controllers\\Frontend\\Pages', 'frontpage', 'GET', 'Frontpage action', 0, 1, '2018-01-11 11:22:13', 3, '2019-10-01 11:12:47');
INSERT INTO `modules` VALUES (9, 4, 0, 'System Settings', '', '', '', 'System Settings module', 1, 1, '2018-01-11 11:22:13', 1, '2018-01-22 08:50:54');
INSERT INTO `modules` VALUES (10, 12, 1, 'Update Module', '\\App\\Http\\Controllers\\Backend\\Modules', 'updateModule', 'POST', 'Update Module Action', 1, 1, '2018-01-11 11:22:13', 1, '2019-10-01 11:12:48');
INSERT INTO `modules` VALUES (11, 17, 1, 'List Panel Nodes', '\\App\\Http\\Controllers\\Backend\\PanelNodes', 'showPanelNodesList', 'GET', 'Action to show panel nodes', 1, 1, '2018-01-11 11:22:13', 1, '2019-10-01 11:12:48');
INSERT INTO `modules` VALUES (12, 9, 0, 'Modules', '', '', '', '', 1, 1, '2018-01-11 11:22:13', 1, '2019-10-01 11:12:48');
INSERT INTO `modules` VALUES (13, 12, 1, 'List Modules', '\\App\\Http\\Controllers\\Backend\\Modules', 'showModulesList', 'GET', 'Show all modules action', 1, 1, '2018-01-11 11:22:13', 1, '2019-10-01 11:12:48');
INSERT INTO `modules` VALUES (14, 4, 1, 'Show Panel', '\\App\\Http\\Controllers\\Backend\\Panel', 'showPanel', 'GET', 'Show Panel action', 1, 1, '2018-01-11 11:22:13', 1, '2019-10-01 11:12:48');
INSERT INTO `modules` VALUES (15, 9, 1, 'Show System Settings', '\\App\\Http\\Controllers\\Backend\\Settings', 'showSystemSettings', 'GET', 'Show System Settings', 1, 1, '2018-01-11 11:22:13', 1, '2019-10-01 11:12:48');
INSERT INTO `modules` VALUES (16, 12, 1, 'Edit Module', '\\App\\Http\\Controllers\\Backend\\Modules', 'editModule', 'GET', 'Edit Module Action', 1, 1, '2018-01-11 11:22:13', 1, '2019-10-01 11:12:48');
INSERT INTO `modules` VALUES (17, 9, 0, 'Panel Nodes', '', '', '', '', 1, 1, '2018-01-11 11:22:13', 1, '2019-10-01 11:12:48');
INSERT INTO `modules` VALUES (18, 17, 1, 'Edit Panel Node', '\\App\\Http\\Controllers\\Backend\\PanelNodes', 'editPanelNode', 'GET', 'Edit Node Action', 1, 1, '2018-01-12 17:36:35', 1, '2019-10-01 11:12:49');
INSERT INTO `modules` VALUES (19, 17, 1, 'Update Panel Node', '\\App\\Http\\Controllers\\Backend\\PanelNodes', 'updatePanelNode', 'POST', 'Update Node Action', 1, 1, '2018-01-12 17:37:23', 1, '2019-10-01 11:12:49');
INSERT INTO `modules` VALUES (20, 17, 1, 'Add Panel Node', '\\App\\Http\\Controllers\\Backend\\PanelNodes', 'addPanelNode', 'GET', 'Add Node Action', 1, 1, '2018-01-12 17:37:50', 1, '2019-10-01 11:12:49');
INSERT INTO `modules` VALUES (21, 17, 1, 'Insert Panel Node', '\\App\\Http\\Controllers\\Backend\\PanelNodes', 'insertPanelNode', 'POST', 'Insert Node Action', 1, 1, '2018-01-13 22:12:23', 1, '2019-10-01 11:12:49');
INSERT INTO `modules` VALUES (22, 17, 1, 'Delete Panel Node', '\\App\\Http\\Controllers\\Backend\\PanelNodes', 'deletePanelNode', 'POST', 'Delete Node Action', 1, 1, '2018-01-14 00:11:18', 3, '2019-10-01 11:12:49');
INSERT INTO `modules` VALUES (23, 12, 1, 'Add Module', '\\App\\Http\\Controllers\\Backend\\Modules', 'addModule', 'GET', 'Add Module Action', 1, 1, '2018-01-14 17:45:32', 1, '2019-10-01 11:12:49');
INSERT INTO `modules` VALUES (24, 12, 1, 'Insert Module', '\\App\\Http\\Controllers\\Backend\\Modules', 'insertModule', 'POST', 'Insert Module Action', 1, 1, '2018-01-14 18:12:17', 1, '2019-10-01 11:12:49');
INSERT INTO `modules` VALUES (25, 9, 0, 'Roles', '', '', '', '', 1, 1, '2018-01-14 17:23:48', 1, '2019-10-01 11:12:49');
INSERT INTO `modules` VALUES (26, 25, 1, 'List Roles', '\\App\\Http\\Controllers\\Backend\\Roles', 'showRolesList', 'GET', 'List roles action', 1, 1, '2018-01-14 17:27:58', 1, '2019-10-01 11:12:50');
INSERT INTO `modules` VALUES (27, 25, 1, 'Add Role', '\\App\\Http\\Controllers\\Backend\\Roles', 'addRole', 'GET', 'Add Role action', 1, 1, '2018-01-14 17:39:16', 1, '2019-10-01 11:12:50');
INSERT INTO `modules` VALUES (28, 25, 1, 'Edit Role', '\\App\\Http\\Controllers\\Backend\\Roles', 'editRole', 'GET', 'Edit Role action', 1, 1, '2018-01-14 19:09:50', 1, '2019-10-01 11:12:50');
INSERT INTO `modules` VALUES (29, 25, 1, 'Update Role', '\\App\\Http\\Controllers\\Backend\\Roles', 'updateRole', 'POST', 'Update Role Action', 1, 1, '2018-01-14 19:11:58', 1, '2019-10-01 11:12:50');
INSERT INTO `modules` VALUES (30, 25, 1, 'Insert Role', '\\App\\Http\\Controllers\\Backend\\Roles', 'insertRole', 'POST', 'Insert Role action', 1, 1, '2018-01-14 19:12:35', 1, '2019-10-01 11:12:50');
INSERT INTO `modules` VALUES (31, 12, 1, 'Delete Module', '\\App\\Http\\Controllers\\Backend\\Modules', 'deleteModule', 'POST', 'Delete Module Action', 1, 1, '2018-01-15 08:29:03', 1, '2019-10-01 11:12:50');
INSERT INTO `modules` VALUES (32, 25, 1, 'Delete Role', '\\App\\Http\\Controllers\\Backend\\Roles', 'deleteRole', 'POST', 'Delete Role Action', 1, 1, '2018-01-15 11:48:46', 1, '2019-10-01 11:12:50');
INSERT INTO `modules` VALUES (33, 9, 0, 'Access Levels', '', '', '', 'Access Levels module', 1, 1, '2018-01-15 12:16:26', 1, '2019-10-01 11:12:50');
INSERT INTO `modules` VALUES (34, 33, 1, 'List Access Levels', '\\App\\Http\\Controllers\\Backend\\AccessLevels', 'showAccessLevelsList', 'GET', 'List access levels action', 1, 1, '2018-01-15 12:17:49', 1, '2019-10-01 11:12:51');
INSERT INTO `modules` VALUES (35, 33, 1, 'Edit Access Level', '\\App\\Http\\Controllers\\Backend\\AccessLevels', 'editAccessLevel', 'GET', 'Edit Access Level Action', 1, 1, '2018-01-15 15:31:58', 1, '2019-10-01 11:12:51');
INSERT INTO `modules` VALUES (36, 33, 1, 'Update Access Level', '\\App\\Http\\Controllers\\Backend\\AccessLevels', 'updateAccessLevel', 'POST', 'Update Access Level action', 1, 1, '2018-01-15 15:32:25', 1, '2019-10-01 11:12:51');
INSERT INTO `modules` VALUES (37, 33, 1, 'Add Access Level', '\\App\\Http\\Controllers\\Backend\\AccessLevels', 'addAccessLevel', 'GET', 'Add Access Level action', 1, 1, '2018-01-15 16:01:09', 1, '2019-10-01 11:12:51');
INSERT INTO `modules` VALUES (38, 33, 1, 'Insert Access Level', '\\App\\Http\\Controllers\\Backend\\AccessLevels', 'insertAccessLevel', 'POST', 'Insert Access Level action', 1, 1, '2018-01-15 16:01:32', 1, '2019-10-01 11:12:51');
INSERT INTO `modules` VALUES (39, 33, 1, 'Delete Access Level', '\\App\\Http\\Controllers\\Backend\\AccessLevels', 'deleteAccessLevel', 'POST', 'Delete Access Level Action', 1, 1, '2018-01-15 16:24:13', 1, '2019-10-01 11:12:51');
INSERT INTO `modules` VALUES (40, 9, 0, 'General Settings', '', '', '', 'General Settings Module', 1, 1, '2018-01-15 17:46:15', 1, '2019-10-01 11:12:51');
INSERT INTO `modules` VALUES (41, 40, 1, 'Edit General Settings', '\\App\\Http\\Controllers\\Backend\\Settings', 'editGeneralSettings', 'GET', 'Edit General Settings Action', 1, 1, '2018-01-15 17:47:41', 1, '2019-10-01 11:12:51');
INSERT INTO `modules` VALUES (42, 40, 1, 'Update General Settings', '\\App\\Http\\Controllers\\Backend\\Settings', 'updateGeneralSettings', 'POST', 'Update General Settings Action', 1, 1, '2018-01-15 19:07:00', 1, '2019-10-01 11:12:52');
INSERT INTO `modules` VALUES (43, 4, 0, 'Users', '', '', '', 'Users Module', 1, 1, '2018-01-17 19:37:01', 1, '2019-10-01 11:12:52');
INSERT INTO `modules` VALUES (44, 43, 1, 'Show Users List', '\\App\\Http\\Controllers\\Backend\\Users', 'showUsersList', 'GET', 'Show Users List Action', 1, 1, '2018-01-17 19:41:02', 1, '2019-10-01 11:12:52');
INSERT INTO `modules` VALUES (45, 43, 1, 'Add User', '\\App\\Http\\Controllers\\Backend\\Users', 'addUser', 'GET', 'Add User Action', 1, 1, '2018-01-17 22:41:56', 1, '2019-10-01 11:12:52');
INSERT INTO `modules` VALUES (46, 43, 1, 'Delete User', '\\App\\Http\\Controllers\\Backend\\Users', 'deleteUser', 'POST', 'Delete User Action', 1, 1, '2018-01-17 22:42:17', 1, '2019-10-01 11:12:52');
INSERT INTO `modules` VALUES (47, 43, 1, 'Insert User', '\\App\\Http\\Controllers\\Backend\\Users', 'insertUser', 'POST', 'Insert User Action', 1, 1, '2018-01-17 22:42:47', 1, '2019-10-01 11:12:52');
INSERT INTO `modules` VALUES (48, 43, 1, 'Update User', '\\App\\Http\\Controllers\\Backend\\Users', 'updateUser', 'POST', 'Update User Action', 1, 1, '2018-01-17 22:43:10', 1, '2019-10-01 11:12:52');
INSERT INTO `modules` VALUES (49, 43, 1, 'Edit User', '\\App\\Http\\Controllers\\Backend\\Users', 'editUser', 'GET', 'Edit User Action', 1, 1, '2018-01-17 22:43:39', 1, '2019-10-01 11:12:52');
INSERT INTO `modules` VALUES (50, 25, 1, 'Get Roles', '\\App\\Http\\Controllers\\Backend\\Roles', 'getRoles', 'GET', 'Get Roles Ajax action', 1, 1, '2018-01-18 15:53:12', 1, '2019-10-01 11:12:53');
INSERT INTO `modules` VALUES (51, 4, 0, 'Articles', '\\App\\Http\\Controllers\\Backend\\Articles', '', '', 'Articles Module', 1, 1, '2018-01-20 23:37:13', 1, '2019-10-01 11:12:53');
INSERT INTO `modules` VALUES (52, 51, 1, 'Add Article', '\\App\\Http\\Controllers\\Backend\\Articles', 'addArticle', 'GET', 'Add Article action', 1, 1, '2018-01-20 23:37:36', 1, '2019-10-01 11:12:53');
INSERT INTO `modules` VALUES (53, 51, 1, 'Show Articles List', '\\App\\Http\\Controllers\\Backend\\Articles', 'showArticlesList', 'GET', 'Show Articles List action', 1, 1, '2018-01-20 23:38:11', 1, '2019-10-01 11:12:53');
INSERT INTO `modules` VALUES (54, 51, 1, 'Edit Article', '\\App\\Http\\Controllers\\Backend\\Articles', 'editArticle', 'GET', 'Edit Article action', 1, 1, '2018-01-20 23:47:41', 1, '2019-10-01 11:12:53');
INSERT INTO `modules` VALUES (55, 4, 1, 'Media Manager', '\\App\\Http\\Controllers\\Backend\\MediaManager', 'showMediaManager', 'GET', 'Media Manager Module', 1, 1, '2018-01-22 08:50:01', 1, '2019-10-01 11:12:53');
INSERT INTO `modules` VALUES (56, 4, 0, 'Categories', '\\App\\Http\\Controllers\\Backend\\Categories', '', '', 'Categories Module', 1, 1, '2018-01-22 09:35:49', 1, '2019-10-01 11:12:53');
INSERT INTO `modules` VALUES (57, 56, 1, 'Show Categories List', '\\App\\Http\\Controllers\\Backend\\Categories', 'showCategoriesList', 'GET', 'Show Categories list action', 1, 1, '2018-01-22 09:36:25', 1, '2019-10-01 11:12:53');
INSERT INTO `modules` VALUES (58, 56, 1, 'Add Category', '\\App\\Http\\Controllers\\Backend\\Categories', 'addCategory', 'GET', 'Add Category action', 1, 1, '2018-01-22 09:40:59', 1, '2019-10-01 11:12:54');
INSERT INTO `modules` VALUES (59, 3, 0, 'Frontend Categories', '\\App\\Http\\Controllers\\Frontend\\Categories', '', '', 'Categories Frontend Module', 1, 1, '2018-01-22 10:21:54', 3, '2019-10-01 11:12:54');
INSERT INTO `modules` VALUES (60, 59, 1, 'View Frontend Category', '\\App\\Http\\Controllers\\Frontend\\Categories', 'viewCategory', 'GET', 'View Frontend Category action', 1, 1, '2018-01-22 10:22:45', 3, '2019-10-01 11:12:54');
INSERT INTO `modules` VALUES (61, 56, 1, 'Edit Category', '\\App\\Http\\Controllers\\Backend\\Categories', 'editCategory', 'GET', 'Edit Category action', 1, 1, '2018-01-22 10:57:32', 1, '2019-10-01 11:12:54');
INSERT INTO `modules` VALUES (62, 56, 1, 'Insert Category', '\\App\\Http\\Controllers\\Backend\\Categories', 'insertCategory', 'POST', 'Insert Category Action', 1, 1, '2018-01-28 18:57:17', 1, '2019-10-01 11:12:54');
INSERT INTO `modules` VALUES (63, 56, 1, 'Update Category', '\\App\\Http\\Controllers\\Backend\\Categories', 'updateCategory', 'POST', 'Update Category action', 1, 1, '2018-01-28 18:57:45', 1, '2019-10-01 11:12:54');
INSERT INTO `modules` VALUES (64, 56, 1, 'Delete Category', '\\App\\Http\\Controllers\\Backend\\Categories', 'deleteCategory', 'POST', 'Delete Category action', 1, 3, '2018-01-29 16:10:09', 3, '2019-10-01 11:12:54');
INSERT INTO `modules` VALUES (65, 56, 1, 'Get Categories', '\\App\\Http\\Controllers\\Backend\\Categories', 'getCategories', 'GET', 'Get Categories ajax action', 1, 3, '2018-01-30 10:47:37', 3, '2019-10-01 11:12:54');
INSERT INTO `modules` VALUES (66, 4, 0, 'Pages', '\\App\\Http\\Controllers\\Backend\\Pages', '', '', 'Pages Module', 1, 3, '2018-01-30 13:35:18', 3, '2019-10-01 11:12:55');
INSERT INTO `modules` VALUES (67, 66, 1, 'Show Pages List', '\\App\\Http\\Controllers\\Backend\\Pages', 'showPagesList', 'GET', 'Show Pages List action', 1, 3, '2018-01-30 13:36:35', 3, '2019-10-01 11:12:55');
INSERT INTO `modules` VALUES (68, 66, 1, 'Add Page', '\\App\\Http\\Controllers\\Backend\\Pages', 'addPage', 'GET', 'Add page action', 1, 3, '2018-01-30 13:55:02', 3, '2019-10-01 11:12:55');
INSERT INTO `modules` VALUES (69, 66, 1, 'Edit Page', '\\App\\Http\\Controllers\\Backend\\Pages', 'editPage', 'GET', 'Edit Page action', 1, 3, '2018-01-30 13:55:28', 3, '2019-10-01 11:12:55');
INSERT INTO `modules` VALUES (70, 4, 1, 'Toggle Side Menu State', '\\App\\Http\\Controllers\\Backend\\Panel', 'toggleSideMenuState', 'GET', 'Toggle Side Menu State action', 1, 3, '2018-01-30 14:10:12', 3, '2019-10-01 11:12:55');
INSERT INTO `modules` VALUES (71, 56, 1, 'Delete Category Translation', '\\App\\Http\\Controllers\\Backend\\Categories', 'deleteCategoryTranslation', 'POST', 'Delete Category Translation action', 1, 3, '2018-02-07 12:54:26', 3, '2019-10-01 11:12:55');
INSERT INTO `modules` VALUES (72, 66, 1, 'Update Page', '\\App\\Http\\Controllers\\Backend\\Pages', 'updatePage', 'POST', 'Update Page action', 1, 3, '2018-02-08 13:16:05', 3, '2019-10-01 11:12:55');
INSERT INTO `modules` VALUES (73, 66, 1, 'Delete Page', '\\App\\Http\\Controllers\\Backend\\Pages', 'deletePage', 'POST', 'Delete Page action', 1, 3, '2018-02-08 13:34:25', 3, '2019-10-01 11:12:55');
INSERT INTO `modules` VALUES (74, 3, 0, 'Frontend Pages', '\\App\\Http\\Controllers\\Frontend\\Pages', '', '', 'Pages Frontend Module', 1, 3, '2018-02-08 13:44:16', 3, '2019-10-01 11:12:56');
INSERT INTO `modules` VALUES (75, 74, 1, 'View Frontend page', '\\App\\Http\\Controllers\\Frontend\\Pages', 'viewPage', 'GET', 'View Frontend page action', 1, 3, '2018-02-08 13:45:20', 3, '2019-10-01 11:12:56');
INSERT INTO `modules` VALUES (76, 66, 1, 'Insert Page', '\\App\\Http\\Controllers\\Backend\\Pages', 'insertPage', 'POST', 'Insert Page action', 1, 3, '2018-02-09 12:00:17', 3, '2019-10-01 11:12:56');
INSERT INTO `modules` VALUES (77, 66, 1, 'Get Pages', '\\App\\Http\\Controllers\\Backend\\Pages', 'getPages', 'GET', 'Get Pages action', 1, 3, '2018-02-09 12:14:24', 3, '2019-10-01 11:12:56');
INSERT INTO `modules` VALUES (78, 3, 0, 'Frontend Articles', '', '', '', 'Frontend Articles module', 1, 3, '2018-02-09 15:18:42', 3, '2018-02-09 15:18:42');
INSERT INTO `modules` VALUES (79, 78, 1, 'View Frontend Article', '\\App\\Http\\Controllers\\Frontend\\Articles', 'viewArticle', 'GET', 'View Frontend Article action', 1, 3, '2018-02-09 15:19:19', 3, '2019-10-01 11:12:56');
INSERT INTO `modules` VALUES (80, 51, 1, 'Delete Article', '\\App\\Http\\Controllers\\Backend\\Articles', 'deleteArticle', 'POST', 'Delete Article action', 1, 3, '2018-02-09 15:24:48', 3, '2019-10-01 11:12:56');
INSERT INTO `modules` VALUES (1, 51, 1, 'Update Article', '\\App\\Http\\Controllers\\Backend\\Articles', 'updateArticle', 'POST', 'Update Article action', 1, 3, '2018-02-09 15:25:19', 3, '2019-10-01 11:12:56');
INSERT INTO `modules` VALUES (82, 51, 1, 'Insert Article', '\\App\\Http\\Controllers\\Backend\\Articles', 'insertArticle', 'POST', 'Insert Article action', 1, 3, '2018-02-09 15:25:40', 3, '2019-10-01 11:12:57');
INSERT INTO `modules` VALUES (83, 51, 1, 'Delete Article translation', '\\App\\Http\\Controllers\\Backend\\Articles', 'deleteArticleTranslation', 'POST', 'Delete Article translation action', 1, 3, '2018-02-09 15:26:44', 3, '2019-10-01 11:12:57');
INSERT INTO `modules` VALUES (84, 51, 1, 'Get Categories', '\\App\\Http\\Controllers\\Backend\\Articles', 'getCategories', 'GET', 'Get Categories ajax action', 1, 3, '2018-02-09 15:27:15', 3, '2019-10-01 11:12:57');
INSERT INTO `modules` VALUES (85, 51, 1, 'Edit own article', '\\App\\Http\\Controllers\\Backend\\Articles', 'editOwnArticle', 'GET', 'Edit own article action', 1, 3, '2018-02-09 15:29:58', 3, '2019-10-01 11:12:57');
INSERT INTO `modules` VALUES (86, 51, 1, 'Search tags', '\\App\\Http\\Controllers\\Backend\\Articles', 'searchTags', 'GET', 'Search tags ajax action', 1, 3, '2018-02-12 09:34:26', 3, '2019-10-01 11:12:57');
INSERT INTO `modules` VALUES (87, 3, 0, 'Frontend Tags', '\\App\\Http\\Controllers\\Frontend\\Tags', '', '', 'Frontend tags module', 1, 3, '2018-02-12 11:26:52', 3, '2019-10-01 11:12:57');
INSERT INTO `modules` VALUES (88, 87, 1, 'View Frontend tag', '\\App\\Http\\Controllers\\Frontend\\Tags', 'viewTag', 'GET', 'View Frontend tag action', 1, 3, '2018-02-12 11:28:18', 3, '2019-10-01 11:12:57');
INSERT INTO `modules` VALUES (89, 4, 0, 'Tags', '\\App\\Http\\Controllers\\Backend\\Tags', '', '', 'Tags Module', 1, 3, '2018-02-12 14:44:38', 3, '2019-10-01 11:12:57');
INSERT INTO `modules` VALUES (90, 89, 1, 'Add Tag', '\\App\\Http\\Controllers\\Backend\\Tags', 'addTag', 'GET', 'Add Tag action', 1, 3, '2018-02-12 14:44:59', 3, '2019-10-01 11:12:58');
INSERT INTO `modules` VALUES (91, 89, 1, 'Edit Tag', '\\App\\Http\\Controllers\\Backend\\Tags', 'editTag', 'GET', 'Edit tag action', 1, 3, '2018-02-12 14:45:24', 3, '2019-10-01 11:12:58');
INSERT INTO `modules` VALUES (92, 89, 1, 'Delete tag', '\\App\\Http\\Controllers\\Backend\\Tags', 'deleteTag', 'POST', 'Delete tag action', 1, 3, '2018-02-12 14:45:43', 3, '2019-10-01 11:12:58');
INSERT INTO `modules` VALUES (93, 89, 1, 'Insert Tag', '\\App\\Http\\Controllers\\Backend\\Tags', 'insertTag', 'POST', 'Insert Tag action', 1, 3, '2018-02-12 14:46:04', 3, '2019-10-01 11:12:58');
INSERT INTO `modules` VALUES (94, 89, 1, 'Update tag', '\\App\\Http\\Controllers\\Backend\\Tags', 'updateTag', 'POST', 'Update tag action', 1, 3, '2018-02-12 14:46:31', 3, '2019-10-01 11:12:58');
INSERT INTO `modules` VALUES (95, 89, 1, 'Show Tags list', '\\App\\Http\\Controllers\\Backend\\Tags', 'showTagsList', 'GET', 'Show Tags list action', 1, 3, '2018-02-12 14:48:03', 3, '2019-10-01 11:12:58');
INSERT INTO `modules` VALUES (96, 51, 1, 'Get Extra Fields', '\\App\\Http\\Controllers\\Backend\\Articles', 'getExtraFields', 'GET', 'Get Extra Fields ajax action', 1, 3, '2018-02-13 10:14:57', 3, '2019-10-01 11:12:58');
INSERT INTO `modules` VALUES (97, 4, 0, 'Menus', '\\App\\Http\\Controllers\\Backend\\Menus', '', '', 'Menus module', 1, 3, '2018-02-13 14:06:07', 3, '2019-10-01 11:12:58');
INSERT INTO `modules` VALUES (98, 97, 1, 'Show Menus', '\\App\\Http\\Controllers\\Backend\\Menus', 'showMenus', 'GET', 'Show Menus action', 1, 3, '2018-02-13 14:07:21', 3, '2019-10-01 11:12:59');
INSERT INTO `modules` VALUES (99, 4, 1, 'Get Languages', '\\App\\Http\\Controllers\\Backend\\Languages', 'getLanguages', 'GET', 'Get Languages ajax action', 1, 3, '2018-02-13 16:15:09', 3, '2019-10-01 11:12:59');
INSERT INTO `modules` VALUES (100, 97, 1, 'Update Menu Order', '\\App\\Http\\Controllers\\Backend\\Menus', 'updateMenuOrder', 'POST', 'Update Menu Order action', 1, 3, '2018-02-12 07:57:40', 3, '2019-10-01 11:12:59');
INSERT INTO `modules` VALUES (101, 97, 1, 'Get Menu Info', '\\App\\Http\\Controllers\\Backend\\Menus', 'getMenuInfo', 'GET', 'Get Menu Info action', 1, 3, '2018-02-12 07:58:09', 3, '2019-10-01 11:12:59');
INSERT INTO `modules` VALUES (102, 97, 1, 'Update menu info', '\\App\\Http\\Controllers\\Backend\\Menus', 'updateMenuInfo', 'POST', 'Update menu info action', 1, 3, '2018-02-12 07:58:48', 3, '2019-10-01 11:12:59');
INSERT INTO `modules` VALUES (103, 9, 0, 'Languages', '\\App\\Http\\Controllers\\Backend\\Languages', '', '', 'Languages Module', 1, 3, '2018-02-14 11:39:26', 3, '2019-10-01 11:12:59');
INSERT INTO `modules` VALUES (104, 103, 1, 'Show Languages', '\\App\\Http\\Controllers\\Backend\\Languages', 'showLanguagesList', 'GET', 'Show Languages action', 1, 3, '2018-02-14 11:40:11', 3, '2019-10-01 11:12:59');
INSERT INTO `modules` VALUES (105, 103, 1, 'Add Language', '\\App\\Http\\Controllers\\Backend\\Languages', 'addLanguage', 'GET', 'Add Language action', 1, 3, '2018-02-14 11:40:36', 3, '2019-10-01 11:12:59');
INSERT INTO `modules` VALUES (106, 103, 1, 'Edit Language', '\\App\\Http\\Controllers\\Backend\\Languages', 'editLanguage', 'GET', 'Edit Language action', 1, 3, '2018-02-14 11:41:02', 3, '2019-10-01 11:13:00');
INSERT INTO `modules` VALUES (107, 103, 1, 'Insert Language', '\\App\\Http\\Controllers\\Backend\\Languages', 'insertLanguage', 'POST', 'Insert Language action', 1, 3, '2018-02-14 11:41:27', 3, '2019-10-01 11:13:00');
INSERT INTO `modules` VALUES (108, 103, 1, 'Update Language', '\\App\\Http\\Controllers\\Backend\\Languages', 'updateLanguage', 'POST', 'Update Language action', 1, 3, '2018-02-14 11:41:51', 3, '2019-10-01 11:13:00');
INSERT INTO `modules` VALUES (109, 103, 1, 'Delete Language', '\\App\\Http\\Controllers\\Backend\\Languages', 'deleteLanguage', 'POST', 'Delete Language action', 1, 3, '2018-02-14 11:42:18', 3, '2019-10-01 11:13:00');
INSERT INTO `modules` VALUES (110, 9, 0, 'Content Types', '\\App\\Http\\Controllers\\Backend\\ContentTypes', '', '', 'Content Types module', 1, 3, '2018-02-14 13:00:37', 3, '2019-10-01 11:13:00');
INSERT INTO `modules` VALUES (111, 110, 1, 'Show Content Types', '\\App\\Http\\Controllers\\Backend\\ContentTypes', 'showContentTypesList', 'GET', 'Show Content Types action', 1, 3, '2018-02-14 13:01:10', 3, '2019-10-01 11:13:00');
INSERT INTO `modules` VALUES (112, 110, 1, 'Add Content Type', '\\App\\Http\\Controllers\\Backend\\ContentTypes', 'addContentType', 'GET', 'Add Content Type action', 1, 3, '2018-02-14 13:01:38', 3, '2019-10-01 11:13:00');
INSERT INTO `modules` VALUES (113, 110, 1, 'Edit Content Type', '\\App\\Http\\Controllers\\Backend\\ContentTypes', 'editContentType', 'GET', 'Edit Content Type action', 1, 3, '2018-02-14 13:02:05', 3, '2019-10-01 11:13:00');
INSERT INTO `modules` VALUES (114, 110, 1, 'Update Content Type', '\\App\\Http\\Controllers\\Backend\\ContentTypes', 'updateContentType', 'POST', 'Update Content Type action', 1, 3, '2018-02-14 13:02:33', 3, '2019-10-01 11:13:01');
INSERT INTO `modules` VALUES (115, 110, 1, 'Insert Content Type', '\\App\\Http\\Controllers\\Backend\\ContentTypes', 'insertContentType', 'POST', 'Insert Content Type action', 1, 3, '2018-02-14 13:03:19', 3, '2019-10-01 11:13:01');
INSERT INTO `modules` VALUES (116, 110, 1, 'Delete Content Type', '\\App\\Http\\Controllers\\Backend\\ContentTypes', 'deleteContentType', 'POST', 'Delete Content Type action', 1, 3, '2018-02-14 13:03:48', 3, '2019-10-01 11:13:01');
INSERT INTO `modules` VALUES (117, 4, 0, 'Extra Fields', '\\App\\Http\\Controllers\\Backend\\ExtraFields', '', '', 'Extra Fields module', 1, 3, '2018-02-14 14:15:19', 3, '2019-10-01 11:13:01');
INSERT INTO `modules` VALUES (118, 133, 1, 'Show Extra Fields List', '\\App\\Http\\Controllers\\Backend\\ExtraFields', 'showExtraFieldslist', 'GET', 'Show Extra Fields List action', 1, 3, '2018-02-14 14:15:52', 3, '2019-10-01 11:13:01');
INSERT INTO `modules` VALUES (119, 133, 1, 'Add Extra Field', '\\App\\Http\\Controllers\\Backend\\ExtraFields', 'addExtraField', 'GET', 'Add Extra Field action', 1, 3, '2018-02-14 14:16:19', 3, '2019-10-01 11:13:01');
INSERT INTO `modules` VALUES (120, 133, 1, 'Edit Extra Field', '\\App\\Http\\Controllers\\Backend\\ExtraFields', 'editExtraField', 'GET', 'Edit Extra Field action', 1, 3, '2018-02-14 14:16:46', 3, '2019-10-01 11:13:01');
INSERT INTO `modules` VALUES (121, 133, 1, 'Delete Extra Field', '\\App\\Http\\Controllers\\Backend\\ExtraFields', 'deleteExtraField', 'POST', 'Delete Extra Field action', 1, 3, '2018-02-14 14:17:29', 3, '2019-10-01 11:13:01');
INSERT INTO `modules` VALUES (122, 133, 1, 'Update Extra Fields', '\\App\\Http\\Controllers\\Backend\\ExtraFields', 'updateExtraField', 'POST', 'Update Extra Fields action', 1, 3, '2018-02-14 14:17:50', 3, '2019-10-01 11:13:02');
INSERT INTO `modules` VALUES (123, 133, 1, 'Insert Extra Field', '\\App\\Http\\Controllers\\Backend\\ExtraFields', 'insertExtraField', 'POST', 'Insert Extra Field action', 1, 3, '2018-02-14 14:18:12', 3, '2019-10-01 11:13:02');
INSERT INTO `modules` VALUES (124, 117, 0, 'Extra Fields Groups', '\\App\\Http\\Controllers\\Backend\\ExtraFieldsGroups', '', '', 'Extra Fields Groups module', 1, 3, '2018-02-14 14:21:32', 3, '2019-10-01 11:13:02');
INSERT INTO `modules` VALUES (125, 124, 1, 'Show Extra Fields Groups list', '\\App\\Http\\Controllers\\Backend\\ExtraFieldsGroups', 'showExtraFieldsGroupsList', 'GET', 'Show Extra Fields Groups list action', 1, 3, '2018-02-14 14:23:30', 3, '2019-10-01 11:13:02');
INSERT INTO `modules` VALUES (126, 124, 1, 'Add Extra Fields Group', '\\App\\Http\\Controllers\\Backend\\ExtraFieldsGroups', 'addExtraFieldsGroup', 'GET', 'Add Extra Fields Group action', 1, 3, '2018-02-14 14:23:54', 3, '2019-10-01 11:13:02');
INSERT INTO `modules` VALUES (127, 124, 1, 'Edit Extra Fields Group', '\\App\\Http\\Controllers\\Backend\\ExtraFieldsGroups', 'editExtraFieldsGroup', 'GET', 'Edit Extra Fields Group action', 1, 3, '2018-02-14 14:24:17', 3, '2019-10-01 11:13:02');
INSERT INTO `modules` VALUES (128, 124, 1, 'Delete Extra Fields Group', '\\App\\Http\\Controllers\\Backend\\ExtraFieldsGroups', 'deleteExtraFieldsGroup', 'POST', 'Delete Extra Fields Group action', 1, 3, '2018-02-14 14:24:41', 3, '2019-10-01 11:13:02');
INSERT INTO `modules` VALUES (129, 124, 1, 'Update Extra Fields Group', '\\App\\Http\\Controllers\\Backend\\ExtraFieldsGroups', 'updateExtraFieldsGroup', 'POST', 'Update Extra Fields Group action', 1, 3, '2018-02-14 14:25:04', 3, '2019-10-01 11:13:03');
INSERT INTO `modules` VALUES (130, 124, 1, 'Insert Extra Fields Group', '\\App\\Http\\Controllers\\Backend\\ExtraFieldsGroups', 'insertExtraFieldsGroup', 'POST', 'Insert Extra Fields Group action', 1, 3, '2018-02-14 14:25:28', 3, '2019-10-01 11:13:03');
INSERT INTO `modules` VALUES (131, 117, 1, 'Show Extra Fields', '\\App\\Http\\Controllers\\Backend\\ExtraFields', 'showExtraFields', 'GET', 'Show Extra Fields action', 1, 3, '2018-02-14 15:28:17', 3, '2019-10-01 11:13:03');
INSERT INTO `modules` VALUES (132, 133, 1, 'Render Extra Field Type', '\\App\\Http\\Controllers\\Backend\\ExtraFields', 'renderExtraFieldType', 'GET', 'Render Extra Field Type ajax action', 1, 3, '2018-02-14 02:48:19', 3, '2019-10-01 11:13:03');
INSERT INTO `modules` VALUES (133, 117, 0, 'Extra Fields List', '\\App\\Http\\Controllers\\Backend\\ExtraFields', '', '', 'Extra Fields List module', 1, 3, '2018-02-14 07:22:43', 3, '2019-10-01 11:13:03');
INSERT INTO `modules` VALUES (134, 4, 0, 'Profile', '\\App\\Http\\Controllers\\Backend\\Profile', '', '', 'Profile module', 1, 3, '2018-02-15 11:47:21', 3, '2019-10-01 11:13:03');
INSERT INTO `modules` VALUES (135, 134, 1, 'Show Profile', '\\App\\Http\\Controllers\\Backend\\Profile', 'showProfile', 'GET', 'Show Profile action', 1, 3, '2018-02-15 11:47:45', 3, '2019-10-01 11:13:03');
INSERT INTO `modules` VALUES (136, 134, 1, 'Update Profile', '\\App\\Http\\Controllers\\Backend\\Profile', 'updateProfile', 'POST', 'Update Profile action', 1, 3, '2018-02-15 11:48:09', 3, '2019-10-01 11:13:03');
INSERT INTO `modules` VALUES (137, 4, 0, 'Banners', '\\App\\Http\\Controllers\\Backend\\Banners', '', '', 'Banners Module', 1, 3, '2018-02-22 12:19:43', 3, '2019-10-01 11:13:04');
INSERT INTO `modules` VALUES (138, 137, 1, 'Show Banners', '\\App\\Http\\Controllers\\Backend\\Banners', 'showBanners', 'GET', 'Show Banners action', 1, 3, '2018-02-22 12:20:09', 3, '2019-10-01 11:13:04');
INSERT INTO `modules` VALUES (139, 137, 0, 'Banners Positions', '\\App\\Http\\Controllers\\Backend\\BannersPositions', '', '', 'Banners Positions module', 1, 3, '2018-02-22 12:44:06', 3, '2019-10-01 11:13:04');
INSERT INTO `modules` VALUES (140, 139, 1, 'Show Banners Positions List', '\\App\\Http\\Controllers\\Backend\\BannersPositions', 'showBannersPositionsList', 'GET', 'Show Banners Positions List action', 1, 3, '2018-02-22 12:44:48', 3, '2019-10-01 11:13:04');
INSERT INTO `modules` VALUES (141, 139, 1, 'Add Banners Postion', '\\App\\Http\\Controllers\\Backend\\BannersPositions', 'addBannersPosition', 'GET', 'Add Banners Postion action', 1, 3, '2018-02-22 13:28:43', 3, '2019-10-01 11:13:04');
INSERT INTO `modules` VALUES (142, 139, 1, 'Insert Banners Position', '\\App\\Http\\Controllers\\Backend\\BannersPositions', 'insertBannersPosition', 'POST', 'Insert Banners Position action', 1, 3, '2018-02-22 14:02:53', 3, '2019-10-01 11:13:04');
INSERT INTO `modules` VALUES (143, 139, 1, 'Update Banners Position', '\\App\\Http\\Controllers\\Backend\\BannersPositions', 'updateBannersPosition', 'POST', 'Update Banners Position action', 1, 3, '2018-02-22 14:03:20', 3, '2019-10-01 11:13:04');
INSERT INTO `modules` VALUES (144, 139, 1, 'Edit Banners Position', '\\App\\Http\\Controllers\\Backend\\BannersPositions', 'editBannersPosition', 'GET', 'Edit Banners Position action', 1, 3, '2018-02-22 14:04:50', 3, '2019-10-01 11:13:04');
INSERT INTO `modules` VALUES (145, 139, 1, 'Delete Banners Position', '\\App\\Http\\Controllers\\Backend\\BannersPositions', 'deleteBannersPositions', 'POST', 'Delete Banners Position action', 1, 3, '2018-02-22 14:44:58', 3, '2019-10-01 11:13:05');
INSERT INTO `modules` VALUES (146, 4, 0, 'Clients', '\\App\\Http\\Controllers\\Backend\\Clients', '', '', 'Clients module', 1, 3, '2018-02-22 15:06:29', 3, '2019-10-01 11:13:05');
INSERT INTO `modules` VALUES (147, 146, 1, 'Clients List', '\\App\\Http\\Controllers\\Backend\\Clients', 'showClientsList', 'GET', 'Clients List action', 1, 3, '2018-02-22 15:07:16', 3, '2019-10-01 11:13:05');
INSERT INTO `modules` VALUES (148, 146, 1, 'Add Client', '\\App\\Http\\Controllers\\Backend\\Clients', 'addClient', 'GET', 'Add Client action', 1, 3, '2018-02-27 13:38:07', 3, '2019-10-01 11:13:05');
INSERT INTO `modules` VALUES (149, 146, 1, 'Edit Client', '\\App\\Http\\Controllers\\Backend\\Clients', 'editClient', 'GET', 'Edit Client action', 1, 3, '2018-02-27 13:38:31', 3, '2019-10-01 11:13:05');
INSERT INTO `modules` VALUES (150, 146, 1, 'Insert Client', '\\App\\Http\\Controllers\\Backend\\Clients', 'insertClient', 'POST', 'Insert Client action', 1, 3, '2018-02-27 13:38:47', 3, '2019-10-01 11:13:05');
INSERT INTO `modules` VALUES (151, 146, 1, 'Update Client', '\\App\\Http\\Controllers\\Backend\\Clients', 'updateClient', 'POST', 'Update Client action', 1, 3, '2018-02-27 13:39:17', 3, '2019-10-01 11:13:05');
INSERT INTO `modules` VALUES (152, 146, 1, 'Delete Clients', '\\App\\Http\\Controllers\\Backend\\Clients', 'deleteClients', 'POST', 'Delete Clients action', 1, 3, '2018-02-27 13:39:41', 3, '2019-10-01 11:13:05');
INSERT INTO `modules` VALUES (153, 4, 0, 'Contacts', '\\App\\Http\\Controllers\\Backend\\Contacts', '', '', 'Contacts Module', 1, 3, '2018-03-02 10:26:11', 3, '2019-10-01 11:13:06');
INSERT INTO `modules` VALUES (154, 153, 1, 'Show Client Contacts', '\\App\\Http\\Controllers\\Backend\\Contacts', 'showClientContacts', 'GET', 'Show Client Contacts List', 1, 3, '2018-03-02 10:27:24', 3, '2019-10-01 11:13:06');
INSERT INTO `modules` VALUES (155, 153, 1, 'Add new contact', '\\App\\Http\\Controllers\\Backend\\Contacts', 'addContact', 'GET', 'Add new contact action', 1, 3, '2018-03-02 11:10:02', 3, '2019-10-01 11:13:06');
INSERT INTO `modules` VALUES (156, 153, 1, 'Insert Contact', '\\App\\Http\\Controllers\\Backend\\Contacts', 'insertContact', 'POST', 'Insert Contact action', 1, 3, '2018-03-02 11:58:53', 3, '2019-10-01 11:13:06');
INSERT INTO `modules` VALUES (157, 153, 1, 'Edit Contact', '\\App\\Http\\Controllers\\Backend\\Contacts', 'editContact', 'GET', 'Edit Contact action', 1, 3, '2018-03-02 14:49:20', 3, '2019-10-01 11:13:06');
INSERT INTO `modules` VALUES (158, 153, 1, 'Update Contact', '\\App\\Http\\Controllers\\Backend\\Contacts', 'updateContact', 'POST', 'Update Contact action', 1, 3, '2018-03-02 14:49:45', 3, '2019-10-01 11:13:06');
INSERT INTO `modules` VALUES (159, 153, 1, 'Show Contacts List', '\\App\\Http\\Controllers\\Backend\\Contacts', 'showContactsList', 'GET', 'Show Contacts List action', 1, 3, '2018-03-05 09:32:29', 3, '2019-10-01 11:13:06');
INSERT INTO `modules` VALUES (160, 97, 1, 'Show Frontend menu', '\\App\\Http\\Controllers\\Backend\\Menus', 'showFrontEndMenu', 'GET', 'Show Frontend menu action', 1, 3, '2018-03-05 11:02:05', 3, '2019-10-01 11:13:06');
INSERT INTO `modules` VALUES (161, 97, 1, 'Show Backend Menu', '\\App\\Http\\Controllers\\Backend\\Menus', 'showBackEndMenu', 'GET', 'Show Backend Menu action', 1, 3, '2018-03-05 11:02:29', 3, '2019-10-01 11:13:07');
INSERT INTO `modules` VALUES (162, 137, 0, 'Banners List', '\\App\\Http\\Controllers\\Backend\\Banners', '', '', 'Banners List module', 1, 3, '2018-03-05 11:24:07', 3, '2019-10-01 11:13:07');
INSERT INTO `modules` VALUES (163, 162, 1, 'Show Banners List', '\\App\\Http\\Controllers\\Backend\\Banners', 'showBannersList', 'GET', 'Show Banners List action', 1, 3, '2018-03-05 11:24:55', 3, '2019-10-01 11:13:07');
INSERT INTO `modules` VALUES (164, 162, 1, 'Add Banner', '\\App\\Http\\Controllers\\Backend\\Banners', 'addBanner', 'GET', 'Add Banner action', 1, 3, '2018-03-05 12:18:00', 3, '2019-10-01 11:13:07');
INSERT INTO `modules` VALUES (165, 162, 1, 'Edit Banner', '\\App\\Http\\Controllers\\Backend\\Banners', 'editBanner', 'GET', 'Edit Banner action', 1, 3, '2018-03-05 12:18:27', 3, '2019-10-01 11:13:07');
INSERT INTO `modules` VALUES (166, 162, 1, 'Update Banner', '\\App\\Http\\Controllers\\Backend\\Banners', 'updateBanner', 'POST', 'Update Banner action', 1, 3, '2018-03-05 12:18:53', 3, '2019-10-01 11:13:07');
INSERT INTO `modules` VALUES (167, 162, 1, 'Insert Banner', '\\App\\Http\\Controllers\\Backend\\Banners', 'insertBanner', 'POST', 'Insert Banner action', 1, 3, '2018-03-05 12:19:29', 3, '2019-10-01 11:13:07');
INSERT INTO `modules` VALUES (168, 162, 1, 'Delete Banner', '\\App\\Http\\Controllers\\Backend\\Banners', 'deleteBanner', 'POST', 'Delete Banner action', 1, 3, '2018-03-05 12:19:56', 3, '2019-10-01 11:13:07');
INSERT INTO `modules` VALUES (169, 162, 1, 'Get Banner type', '\\App\\Http\\Controllers\\Backend\\Banners', 'getBannerType', 'GET', 'Get Banner type ajax action', 1, 3, '2018-03-05 12:53:26', 3, '2019-10-01 11:13:08');
INSERT INTO `modules` VALUES (170, 9, 0, 'Cache', '', '', '', 'Module to manipulate cache', 1, 3, '2018-05-26 11:17:48', 3, '2018-05-31 12:23:23');
INSERT INTO `modules` VALUES (171, 170, 1, 'Show Cache List', '\\App\\Http\\Controllers\\Backend\\Cache', 'showCacheList', 'GET', 'Show Cache List', 1, 3, '2018-05-31 12:24:55', 3, '2019-10-01 11:13:08');
INSERT INTO `modules` VALUES (172, 170, 1, 'Delete Cache', '\\App\\Http\\Controllers\\Backend\\Cache', 'deleteCache', 'POST', 'Action to delete cache', 1, 3, '2018-05-31 12:29:52', 3, '2019-10-01 11:13:08');
INSERT INTO `modules` VALUES (173, 4, 0, 'Comments', '\\App\\Http\\Controllers\\Backend\\Comments', '', '', 'Module to manage comments', 1, 3, '2018-05-31 14:52:16', 3, '2019-10-01 11:13:08');
INSERT INTO `modules` VALUES (174, 173, 1, 'Show Comments', '\\App\\Http\\Controllers\\Backend\\Comments', 'showCommentsList', 'GET', 'Action to show comments list', 1, 3, '2018-05-31 14:52:56', 3, '2019-10-01 11:13:08');
INSERT INTO `modules` VALUES (175, 173, 1, 'Edit Comment', '\\App\\Http\\Controllers\\Backend\\Comments', 'editComment', 'GET', 'Action to EditComment', 1, 3, '2018-05-31 16:02:52', 3, '2019-10-01 11:13:08');
INSERT INTO `modules` VALUES (176, 173, 1, 'Delete Comment', '\\App\\Http\\Controllers\\Backend\\Comments', 'deleteComment', 'POST', 'Action to Delete Comment', 1, 3, '2018-05-31 16:12:19', 3, '2019-10-01 11:13:08');
INSERT INTO `modules` VALUES (177, 3, 0, 'Frontend Comments', '\\App\\Http\\Controllers\\Frontend\\Comments', '', '', 'Module for frontend comments', 1, 3, '2018-06-01 12:19:25', 3, '2019-10-01 11:13:08');
INSERT INTO `modules` VALUES (178, 177, 1, 'Add Comment', '\\App\\Http\\Controllers\\Frontend\\Comments', 'addComment', 'POST', 'Action to add comments from frontend', 1, 3, '2018-06-01 12:19:51', 3, '2019-10-01 11:13:09');
INSERT INTO `modules` VALUES (179, 181, 1, 'Login', '\\App\\Http\\Controllers\\Frontend\\Site', 'login', 'POST', 'Login action', 1, 3, '2018-06-01 14:57:44', 3, '2019-10-01 11:13:09');
INSERT INTO `modules` VALUES (180, 181, 1, 'Logout', '\\App\\Http\\Controllers\\Frontend\\Site', 'logout', 'GET', 'Logout action', 1, 3, '2018-06-01 15:00:10', 3, '2019-10-01 11:13:09');
INSERT INTO `modules` VALUES (181, 3, 0, 'Site', '', '', '', 'Site General Module', 1, 3, '2018-06-01 01:10:07', 3, '2018-06-01 01:10:07');
INSERT INTO `modules` VALUES (182, 181, 1, 'Accept Cookies', '\\App\\Http\\Controllers\\Frontend\\Site', 'acceptCookies', 'GET', 'Accept Cookies action', 1, 3, '2018-06-01 01:11:00', 3, '2019-10-01 11:13:09');
INSERT INTO `modules` VALUES (183, 181, 1, 'Decline Cookies', '\\App\\Http\\Controllers\\Frontend\\Site', 'declineCookies', 'GET', 'Decline Cookies action', 1, 3, '2018-06-01 01:12:10', 3, '2019-10-01 11:13:09');
INSERT INTO `modules` VALUES (184, 173, 1, 'Update Comment', '\\App\\Http\\Controllers\\Backend\\Comments', 'updateComment', 'POST', 'Update Comment action', 1, 3, '2018-06-01 07:14:16', 3, '2019-10-01 11:13:09');
INSERT INTO `modules` VALUES (185, 4, 0, 'Migrator', '\\App\\Http\\Controllers\\Backend\\Panel', '', '', 'Migrator Module', 1, 3, '2018-06-05 01:32:10', 3, '2019-10-01 11:13:09');
INSERT INTO `modules` VALUES (186, 185, 1, 'Migration Options', '\\App\\Http\\Controllers\\Backend\\Panel', 'migrationOptions', 'GET', 'Migration Options action', 1, 3, '2018-06-05 01:32:40', 3, '2019-10-01 11:13:10');
INSERT INTO `modules` VALUES (187, 185, 1, 'Migrate Actions', '\\App\\Http\\Controllers\\Backend\\Panel', 'migrateActions', 'POST', 'Migrate Actions', 1, 3, '2018-06-05 01:33:06', 3, '2019-10-01 11:13:10');
INSERT INTO `modules` VALUES (188, 181, 1, 'Register', '\\App\\Http\\Controllers\\Frontend\\Site', 'register', 'POST', 'Register action', 1, 3, '2018-06-08 11:47:36', 3, '2019-10-01 11:13:10');
INSERT INTO `modules` VALUES (189, 181, 1, 'Login With Facebook', '\\App\\Http\\Controllers\\Frontend\\Site', 'registerFacebookUser', 'POST', 'Login With Facebook action', 1, 1, '2018-06-08 02:05:17', 1, '2019-10-01 11:13:10');
INSERT INTO `modules` VALUES (190, 181, 1, 'Activate User', '\\App\\Http\\Controllers\\Frontend\\Site', 'activateUser', 'GET', 'Activate User action', 1, 1, '2018-06-09 16:37:55', 1, '2019-10-01 11:13:10');
INSERT INTO `modules` VALUES (191, 181, 1, 'Forgotten User actions', '\\App\\Http\\Controllers\\Frontend\\Site', 'forgottenAction', 'POST', 'Forgotten user actions', 1, 1, '2018-06-09 17:51:56', 1, '2019-10-01 11:13:10');
INSERT INTO `modules` VALUES (192, 181, 1, 'Sitemap', '\\App\\Http\\Controllers\\Frontend\\Sitemap', 'viewSitemap', 'GET', 'Web app sitemap', 1, 1, '2018-11-13 19:52:46', 1, '2019-10-01 11:13:10');
INSERT INTO `modules` VALUES (193, 181, 1, 'Google News Sitemap', '\\App\\Http\\Controllers\\Frontend\\Sitemap', 'viewGoogleNewsSitemap', 'GET', 'Web app google news sitemap', 1, 1, '2018-11-14 20:02:41', 1, '2019-10-01 11:13:10');
INSERT INTO `modules` VALUES (194, 4, 0, 'Tiles', '\\App\\Http\\Controllers\\Backend\\Tiles', '', '', 'Tiles module', 1, 1, '2018-11-16 11:05:58', 1, '2019-10-01 11:13:11');
INSERT INTO `modules` VALUES (195, 194, 1, 'Tiles List', '\\App\\Http\\Controllers\\Backend\\Tiles', 'showTilesList', 'GET', 'Tiles List', 1, 1, '2018-11-16 11:06:20', 1, '2019-10-01 11:13:11');
INSERT INTO `modules` VALUES (196, 194, 1, 'Add Tiles', '\\App\\Http\\Controllers\\Backend\\Tiles', 'addTiles', 'POST', 'Add Tiles module', 1, 1, '2018-11-16 12:30:11', 1, '2019-10-01 11:13:11');
INSERT INTO `modules` VALUES (197, 194, 1, 'Remove Tiles', '\\App\\Http\\Controllers\\Backend\\Tiles', 'removeTile', 'POST', 'Remove tiles module', 1, 1, '2018-11-16 12:50:42', 1, '2019-10-01 11:13:11');
INSERT INTO `modules` VALUES (198, 194, 1, 'Update Tiles Order', '\\App\\Http\\Controllers\\Backend\\Tiles', 'updateTilesOrder', 'POST', 'Update Tiles Order action', 1, 1, '2018-11-16 12:59:55', 1, '2019-10-01 11:13:11');
INSERT INTO `modules` VALUES (199, 181, 1, 'Get News', '\\App\\Http\\Controllers\\Frontend\\Site', 'getNews', 'GET', 'Get News ajax action', 1, 1, '2018-11-17 17:36:11', 1, '2019-10-01 11:13:11');
INSERT INTO `modules` VALUES (200, 181, 1, 'Manage Privacy Policy', '\\App\\Http\\Controllers\\Frontend\\Site', 'managePrivacyPolicy', 'POST', 'Manage Privacy Policy action', 1, 1, '2018-11-19 11:28:10', 1, '2019-10-01 11:13:11');
INSERT INTO `modules` VALUES (201, 3, 0, 'Frontend Users', '\\App\\Http\\Controllers\\Frontend\\User', '', '', 'Frontend Users module', 1, 1, '2018-11-19 14:48:31', 1, '2019-10-01 11:13:11');
INSERT INTO `modules` VALUES (203, 201, 1, 'Edit All Site Users', '\\App\\Http\\Controllers\\Frontend\\Users', 'editAllSiteUsers', 'GET', 'Edit Site User action', 1, 1, '2018-11-19 14:58:19', 1, '2019-10-01 11:13:12');
INSERT INTO `modules` VALUES (204, 201, 1, 'Update Site User', '\\App\\Http\\Controllers\\Frontend\\Users', 'updateSiteUser', 'POST', 'Update Site User action', 1, 1, '2018-11-19 16:16:21', 1, '2019-10-01 11:13:12');
INSERT INTO `modules` VALUES (205, 201, 1, 'View User', '\\App\\Http\\Controllers\\Frontend\\Users', 'viewUser', 'GET', 'View User Action', 1, 1, '2018-11-19 20:44:13', 1, '2019-10-01 11:13:12');
INSERT INTO `modules` VALUES (206, 177, 1, 'Get Comments', '\\App\\Http\\Controllers\\Frontend\\Comments', 'getComments', 'GET', 'Get Comments Action', 1, 1, '2018-11-20 20:23:08', 1, '2019-10-01 11:13:12');
INSERT INTO `modules` VALUES (207, 177, 1, 'Like Comment', '\\App\\Http\\Controllers\\Frontend\\Comments', 'likeComments', 'POST', 'Like Comment Action', 1, 1, '2018-11-22 14:45:17', 1, '2019-10-01 11:13:12');
INSERT INTO `modules` VALUES (208, 177, 1, 'Show Likes', '\\App\\Http\\Controllers\\Frontend\\Comments', 'showLikes', 'GET', 'Show Likes action', 1, 1, '2018-11-22 14:53:21', 1, '2019-10-01 11:13:12');
INSERT INTO `modules` VALUES (209, 177, 1, 'Edit Frontend Comments', '\\App\\Http\\Controllers\\Frontend\\Comments', 'editFrontendComments', 'GET', 'Edit Frontend Comments action', 1, 1, '2018-11-22 22:41:48', 1, '2019-10-01 11:13:12');
INSERT INTO `modules` VALUES (210, 177, 1, 'Delete Frontend Comments', '\\App\\Http\\Controllers\\Frontend\\Comments', 'deleteFrontendComments', 'POST', 'Delete Frontend Comments actions', 1, 1, '2018-11-22 22:42:17', 1, '2019-10-01 11:13:12');
INSERT INTO `modules` VALUES (211, 177, 1, 'Show Comment Form', '\\App\\Http\\Controllers\\Frontend\\Comments', 'showCommentForm', 'GET', 'Show Comment Form action', 1, 1, '2018-11-23 00:17:03', 1, '2019-10-01 11:13:13');
INSERT INTO `modules` VALUES (212, 177, 1, 'Update Frontend Comments', '\\App\\Http\\Controllers\\Frontend\\Comments', 'updateFrontendComments', 'POST', 'Update Frontend Comments action', 1, 1, '2018-11-23 00:17:52', 1, '2019-10-01 11:13:13');
INSERT INTO `modules` VALUES (213, 177, 1, 'Report Comments', '\\App\\Http\\Controllers\\Frontend\\Comments', 'reportComments', 'POST', 'Report Comments action', 1, 1, '2018-11-25 09:56:42', 1, '2019-10-01 11:13:13');
INSERT INTO `modules` VALUES (214, 201, 1, 'Download Info', '\\App\\Http\\Controllers\\Frontend\\Users', 'downloadInfo', 'GET', 'Download Info action', 1, 1, '2018-11-25 18:04:15', 1, '2019-10-01 11:13:13');
INSERT INTO `modules` VALUES (215, 74, 1, 'Search', '\\App\\Http\\Controllers\\Frontend\\Pages', 'search', 'GET', 'Search action', 1, 1, '2018-11-25 20:42:00', 1, '2019-10-01 11:13:13');
INSERT INTO `modules` VALUES (216, 74, 1, 'Contact Us', '\\App\\Http\\Controllers\\Frontend\\Pages', 'contact', 'POST', 'Contact Us action', 1, 1, '2018-11-30 14:58:30', 1, '2019-10-01 11:13:13');
INSERT INTO `modules` VALUES (217, 177, 1, 'Edit Own Frontend Comments', '\\App\\Http\\Controllers\\Frontend\\Comments', 'editOwnComments', 'GET', 'Edit Own Frontend Comments action', 1, 1, '2018-12-02 19:51:15', 1, '2019-10-01 11:13:13');
INSERT INTO `modules` VALUES (218, 51, 1, 'Publish Articles', '\\App\\Http\\Controllers\\Backend\\Articles', 'canPublishArticles', '', 'Publish Articles action', 1, 1, '2018-12-10 21:10:35', 1, '2019-10-01 11:13:13');
INSERT INTO `modules` VALUES (219, 162, 1, 'Update Banners Order', '\\App\\Http\\Controllers\\Backend\\Banners', 'updateBannersOrder', 'POST', 'Update Banners Order ajax action', 1, 81, '2019-02-03 22:04:12', 81, '2019-10-01 11:13:14');
INSERT INTO `modules` VALUES (220, 4, 1, 'Optimize Image', '\\App\\Http\\Controllers\\Backend\\Media Manager', 'optimizeImage', 'POST', 'Optimize Image Module', 1, 81, '2019-02-09 17:57:59', 81, '2019-10-01 11:13:14');
INSERT INTO `modules` VALUES (221, 133, 1, 'Update Extra Fields Order', '\\App\\Http\\Controllers\\Backend\\ExtraFields', 'updateExtraFieldsOrder', 'POST', 'Update Extra Fields Order ajax action', 1, 81, '2019-03-12 16:30:33', 81, '2019-10-01 11:13:14');
INSERT INTO `modules` VALUES (222, 89, 1, 'Merge Tags', '\\App\\Http\\Controllers\\Backend\\Tags', 'mergeTags', 'POST', 'Merge Tags action', 1, 81, '2019-03-16 12:34:39', 81, '2019-10-01 11:13:14');
INSERT INTO `modules` VALUES (223, 9, 0, 'App Logs', '\\App\\Http\\Controllers\\Backend\\App\\HttpLogs', '', '', 'App Logs module', 1, 81, '2019-05-04 18:32:57', 81, '2019-10-01 11:13:14');
INSERT INTO `modules` VALUES (224, 223, 1, 'Show App Logs List', '\\App\\Http\\Controllers\\Backend\\App\\HttpLogs', 'showAppLogsList', 'GET', 'Show App Logs action', 1, 81, '2019-05-04 18:44:41', 81, '2019-10-01 11:13:14');
INSERT INTO `modules` VALUES (225, 223, 1, 'Show App Log Info', '\\App\\Http\\Controllers\\Backend\\App\\HttpLogs', 'showAppLogInfo', 'GET', 'Show App Log Info action', 1, 81, '2019-05-05 01:08:15', 81, '2019-10-01 11:13:14');
INSERT INTO `modules` VALUES (226, 223, 1, 'Delete App Log', '\\App\\Http\\Controllers\\Backend\\App\\HttpLogs', 'deleteAppLog', 'POST', 'Delete App Log action', 1, 81, '2019-05-05 03:15:15', 81, '2019-10-01 11:13:15');
INSERT INTO `modules` VALUES (227, 181, 1, 'Manage Dark Mode', '\\App\\Http\\Controllers\\Frontend\\Site', 'manageDarkMode', 'POST', 'Manage Dark Mode ajax action', 1, 81, '2019-05-23 14:25:24', 81, '2019-10-01 11:13:15');
COMMIT;

-- ----------------------------
-- Table structure for modules_tree
-- ----------------------------
DROP TABLE IF EXISTS `modules_tree`;
CREATE TABLE `modules_tree` (
  `parent_id` int(11) NOT NULL,
  `child_id` int(11) NOT NULL,
  `depth` int(11) NOT NULL,
  `path` text NOT NULL,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `child_id` (`child_id`) USING BTREE,
  CONSTRAINT `modules_tree_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `modules_tree_ibfk_2` FOREIGN KEY (`child_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of modules_tree
-- ----------------------------
BEGIN;
INSERT INTO `modules_tree` VALUES (1, 1, 0, 'System/');
INSERT INTO `modules_tree` VALUES (2, 2, 0, 'Backend/');
INSERT INTO `modules_tree` VALUES (1, 2, 1, 'System/Backend/');
INSERT INTO `modules_tree` VALUES (3, 3, 0, 'Frontend/');
INSERT INTO `modules_tree` VALUES (1, 3, 1, 'System/Frontend/');
INSERT INTO `modules_tree` VALUES (4, 4, 0, 'Panel/');
INSERT INTO `modules_tree` VALUES (2, 4, 1, 'Backend/Panel/');
INSERT INTO `modules_tree` VALUES (1, 4, 2, 'System/Backend/Panel/');
INSERT INTO `modules_tree` VALUES (5, 5, 0, 'Dashboard/');
INSERT INTO `modules_tree` VALUES (4, 5, 1, 'Panel/Dashboard/');
INSERT INTO `modules_tree` VALUES (2, 5, 2, 'Backend/Panel/Dashboard/');
INSERT INTO `modules_tree` VALUES (1, 5, 3, 'System/Backend/Panel/Dashboard/');
INSERT INTO `modules_tree` VALUES (6, 6, 0, 'Panel Login/');
INSERT INTO `modules_tree` VALUES (4, 6, 1, 'Panel/Panel Login/');
INSERT INTO `modules_tree` VALUES (2, 6, 2, 'Backend/Panel/Panel Login/');
INSERT INTO `modules_tree` VALUES (1, 6, 3, 'System/Backend/Panel/Panel Login/');
INSERT INTO `modules_tree` VALUES (7, 7, 0, 'Panel Logout/');
INSERT INTO `modules_tree` VALUES (4, 7, 1, 'Panel/Panel Logout/');
INSERT INTO `modules_tree` VALUES (2, 7, 2, 'Backend/Panel/Panel Logout/');
INSERT INTO `modules_tree` VALUES (1, 7, 3, 'System/Backend/Panel/Panel Logout/');
INSERT INTO `modules_tree` VALUES (8, 8, 0, 'Frontpage/');
INSERT INTO `modules_tree` VALUES (3, 8, 1, 'Frontend/Frontpage/');
INSERT INTO `modules_tree` VALUES (1, 8, 2, 'System/Frontend/Frontpage/');
INSERT INTO `modules_tree` VALUES (9, 9, 0, 'System Settings/');
INSERT INTO `modules_tree` VALUES (11, 11, 0, 'List Panel Nodes/');
INSERT INTO `modules_tree` VALUES (13, 13, 0, 'List Modules/');
INSERT INTO `modules_tree` VALUES (14, 14, 0, 'Show Panel/');
INSERT INTO `modules_tree` VALUES (4, 14, 1, 'Panel/Show Panel/');
INSERT INTO `modules_tree` VALUES (2, 14, 2, 'Backend/Panel/Show Panel/');
INSERT INTO `modules_tree` VALUES (1, 14, 3, 'System/Backend/Panel/Show Panel/');
INSERT INTO `modules_tree` VALUES (10, 10, 0, 'Update Module/');
INSERT INTO `modules_tree` VALUES (12, 12, 0, 'Modules/');
INSERT INTO `modules_tree` VALUES (12, 13, 1, 'Modules/List Modules/');
INSERT INTO `modules_tree` VALUES (15, 15, 0, 'Show System Settings/');
INSERT INTO `modules_tree` VALUES (16, 16, 0, 'Edit Module/');
INSERT INTO `modules_tree` VALUES (12, 16, 1, 'Modules/Edit Module/');
INSERT INTO `modules_tree` VALUES (17, 17, 0, 'Panel Nodes/');
INSERT INTO `modules_tree` VALUES (12, 10, 1, 'Modules/Update Module/');
INSERT INTO `modules_tree` VALUES (17, 11, 1, 'Panel Nodes/List Panel Nodes/');
INSERT INTO `modules_tree` VALUES (9, 12, 1, 'System Settings/Modules/');
INSERT INTO `modules_tree` VALUES (9, 13, 2, 'System Settings/Modules/List Modules/');
INSERT INTO `modules_tree` VALUES (9, 16, 2, 'System Settings/Modules/Edit Module/');
INSERT INTO `modules_tree` VALUES (9, 10, 2, 'System Settings/Modules/Update Module/');
INSERT INTO `modules_tree` VALUES (9, 17, 1, 'System Settings/Panel Nodes/');
INSERT INTO `modules_tree` VALUES (9, 11, 2, 'System Settings/Panel Nodes/List Panel Nodes/');
INSERT INTO `modules_tree` VALUES (18, 18, 0, 'Edit Panel Node/');
INSERT INTO `modules_tree` VALUES (17, 18, 1, 'Panel Nodes/Edit Panel Node/');
INSERT INTO `modules_tree` VALUES (9, 18, 2, 'System Settings/Panel Nodes/Edit Panel Node/');
INSERT INTO `modules_tree` VALUES (19, 19, 0, 'Update Panel Node/');
INSERT INTO `modules_tree` VALUES (17, 19, 1, 'Panel Nodes/Update Panel Node/');
INSERT INTO `modules_tree` VALUES (9, 19, 2, 'System Settings/Panel Nodes/Update Panel Node/');
INSERT INTO `modules_tree` VALUES (20, 20, 0, 'Add Panel Node/');
INSERT INTO `modules_tree` VALUES (17, 20, 1, 'Panel Nodes/Add Panel Node/');
INSERT INTO `modules_tree` VALUES (9, 20, 2, 'System Settings/Panel Nodes/Add Panel Node/');
INSERT INTO `modules_tree` VALUES (21, 21, 0, 'Insert Panel Node/');
INSERT INTO `modules_tree` VALUES (17, 21, 1, 'Panel Nodes/Insert Panel Node/');
INSERT INTO `modules_tree` VALUES (9, 21, 2, 'System Settings/Panel Nodes/Insert Panel Node/');
INSERT INTO `modules_tree` VALUES (22, 22, 0, 'Delete Panel Node/');
INSERT INTO `modules_tree` VALUES (17, 22, 1, 'Panel Nodes/Delete Panel Node/');
INSERT INTO `modules_tree` VALUES (9, 22, 2, 'System Settings/Panel Nodes/Delete Panel Node/');
INSERT INTO `modules_tree` VALUES (23, 23, 0, 'Add Module/');
INSERT INTO `modules_tree` VALUES (12, 23, 1, 'Modules/Add Module/');
INSERT INTO `modules_tree` VALUES (9, 23, 2, 'System Settings/Modules/Add Module/');
INSERT INTO `modules_tree` VALUES (24, 24, 0, 'Insert Module/');
INSERT INTO `modules_tree` VALUES (12, 24, 1, 'Modules/Insert Module/');
INSERT INTO `modules_tree` VALUES (9, 24, 2, 'System Settings/Modules/Insert Module/');
INSERT INTO `modules_tree` VALUES (25, 25, 0, 'Roles/');
INSERT INTO `modules_tree` VALUES (9, 25, 1, 'System Settings/Roles/');
INSERT INTO `modules_tree` VALUES (26, 26, 0, 'List Roles/');
INSERT INTO `modules_tree` VALUES (25, 26, 1, 'Roles/List Roles/');
INSERT INTO `modules_tree` VALUES (9, 26, 2, 'System Settings/Roles/List Roles/');
INSERT INTO `modules_tree` VALUES (27, 27, 0, 'Add Role/');
INSERT INTO `modules_tree` VALUES (25, 27, 1, 'Roles/Add Role/');
INSERT INTO `modules_tree` VALUES (9, 27, 2, 'System Settings/Roles/Add Role/');
INSERT INTO `modules_tree` VALUES (28, 28, 0, 'Edit Role/');
INSERT INTO `modules_tree` VALUES (25, 28, 1, 'Roles/Edit Role/');
INSERT INTO `modules_tree` VALUES (9, 28, 2, 'System Settings/Roles/Edit Role/');
INSERT INTO `modules_tree` VALUES (29, 29, 0, 'Update Role/');
INSERT INTO `modules_tree` VALUES (25, 29, 1, 'Roles/Update Role/');
INSERT INTO `modules_tree` VALUES (9, 29, 2, 'System Settings/Roles/Update Role/');
INSERT INTO `modules_tree` VALUES (30, 30, 0, 'Insert Role/');
INSERT INTO `modules_tree` VALUES (25, 30, 1, 'Roles/Insert Role/');
INSERT INTO `modules_tree` VALUES (9, 30, 2, 'System Settings/Roles/Insert Role/');
INSERT INTO `modules_tree` VALUES (31, 31, 0, 'Delete Module/');
INSERT INTO `modules_tree` VALUES (12, 31, 1, 'Modules/Delete Module/');
INSERT INTO `modules_tree` VALUES (9, 31, 2, 'System Settings/Modules/Delete Module/');
INSERT INTO `modules_tree` VALUES (32, 32, 0, 'Delete Role/');
INSERT INTO `modules_tree` VALUES (25, 32, 1, 'Roles/Delete Role/');
INSERT INTO `modules_tree` VALUES (9, 32, 2, 'System Settings/Roles/Delete Role/');
INSERT INTO `modules_tree` VALUES (33, 33, 0, 'Access Levels/');
INSERT INTO `modules_tree` VALUES (9, 33, 1, 'System Settings/Access Levels/');
INSERT INTO `modules_tree` VALUES (34, 34, 0, 'List Access Levels/');
INSERT INTO `modules_tree` VALUES (33, 34, 1, 'Access Levels/List Access Levels/');
INSERT INTO `modules_tree` VALUES (9, 34, 2, 'System Settings/Access Levels/List Access Levels/');
INSERT INTO `modules_tree` VALUES (35, 35, 0, 'Edit Access Level/');
INSERT INTO `modules_tree` VALUES (33, 35, 1, 'Access Levels/Edit Access Level/');
INSERT INTO `modules_tree` VALUES (9, 35, 2, 'System Settings/Access Levels/Edit Access Level/');
INSERT INTO `modules_tree` VALUES (36, 36, 0, 'update Access Level/');
INSERT INTO `modules_tree` VALUES (33, 36, 1, 'Access Levels/update Access Level/');
INSERT INTO `modules_tree` VALUES (9, 36, 2, 'System Settings/Access Levels/update Access Level/');
INSERT INTO `modules_tree` VALUES (37, 37, 0, 'Add Access Level/');
INSERT INTO `modules_tree` VALUES (33, 37, 1, 'Access Levels/Add Access Level/');
INSERT INTO `modules_tree` VALUES (9, 37, 2, 'System Settings/Access Levels/Add Access Level/');
INSERT INTO `modules_tree` VALUES (38, 38, 0, 'Insert Access Level/');
INSERT INTO `modules_tree` VALUES (33, 38, 1, 'Access Levels/Insert Access Level/');
INSERT INTO `modules_tree` VALUES (9, 38, 2, 'System Settings/Access Levels/Insert Access Level/');
INSERT INTO `modules_tree` VALUES (39, 39, 0, 'Delete Access Level/');
INSERT INTO `modules_tree` VALUES (33, 39, 1, 'Access Levels/Delete Access Level/');
INSERT INTO `modules_tree` VALUES (9, 39, 2, 'System Settings/Access Levels/Delete Access Level/');
INSERT INTO `modules_tree` VALUES (40, 40, 0, 'General Settings/');
INSERT INTO `modules_tree` VALUES (9, 40, 1, 'System Settings/General Settings/');
INSERT INTO `modules_tree` VALUES (41, 41, 0, 'Edit General Settings/');
INSERT INTO `modules_tree` VALUES (40, 41, 1, 'General Settings/Edit General Settings/');
INSERT INTO `modules_tree` VALUES (9, 41, 2, 'System Settings/General Settings/Edit General Settings/');
INSERT INTO `modules_tree` VALUES (42, 42, 0, 'Update General Settings/');
INSERT INTO `modules_tree` VALUES (40, 42, 1, 'General Settings/Update General Settings/');
INSERT INTO `modules_tree` VALUES (9, 42, 2, 'System Settings/General Settings/Update General Settings/');
INSERT INTO `modules_tree` VALUES (43, 43, 0, 'Users/');
INSERT INTO `modules_tree` VALUES (4, 43, 1, 'Panel/Users/');
INSERT INTO `modules_tree` VALUES (2, 43, 2, 'Backend/Panel/Users/');
INSERT INTO `modules_tree` VALUES (1, 43, 3, 'System/Backend/Panel/Users/');
INSERT INTO `modules_tree` VALUES (44, 44, 0, 'Show Users List/');
INSERT INTO `modules_tree` VALUES (43, 44, 1, 'Users/Show Users List/');
INSERT INTO `modules_tree` VALUES (4, 44, 2, 'Panel/Users/Show Users List/');
INSERT INTO `modules_tree` VALUES (2, 44, 3, 'Backend/Panel/Users/Show Users List/');
INSERT INTO `modules_tree` VALUES (1, 44, 4, 'System/Backend/Panel/Users/Show Users List/');
INSERT INTO `modules_tree` VALUES (45, 45, 0, 'Add User/');
INSERT INTO `modules_tree` VALUES (43, 45, 1, 'Users/Add User/');
INSERT INTO `modules_tree` VALUES (4, 45, 2, 'Panel/Users/Add User/');
INSERT INTO `modules_tree` VALUES (2, 45, 3, 'Backend/Panel/Users/Add User/');
INSERT INTO `modules_tree` VALUES (1, 45, 4, 'System/Backend/Panel/Users/Add User/');
INSERT INTO `modules_tree` VALUES (46, 46, 0, 'Delete User/');
INSERT INTO `modules_tree` VALUES (43, 46, 1, 'Users/Delete User/');
INSERT INTO `modules_tree` VALUES (4, 46, 2, 'Panel/Users/Delete User/');
INSERT INTO `modules_tree` VALUES (2, 46, 3, 'Backend/Panel/Users/Delete User/');
INSERT INTO `modules_tree` VALUES (1, 46, 4, 'System/Backend/Panel/Users/Delete User/');
INSERT INTO `modules_tree` VALUES (47, 47, 0, 'Insert User/');
INSERT INTO `modules_tree` VALUES (43, 47, 1, 'Users/Insert User/');
INSERT INTO `modules_tree` VALUES (4, 47, 2, 'Panel/Users/Insert User/');
INSERT INTO `modules_tree` VALUES (2, 47, 3, 'Backend/Panel/Users/Insert User/');
INSERT INTO `modules_tree` VALUES (1, 47, 4, 'System/Backend/Panel/Users/Insert User/');
INSERT INTO `modules_tree` VALUES (48, 48, 0, 'Update User/');
INSERT INTO `modules_tree` VALUES (43, 48, 1, 'Users/Update User/');
INSERT INTO `modules_tree` VALUES (4, 48, 2, 'Panel/Users/Update User/');
INSERT INTO `modules_tree` VALUES (2, 48, 3, 'Backend/Panel/Users/Update User/');
INSERT INTO `modules_tree` VALUES (1, 48, 4, 'System/Backend/Panel/Users/Update User/');
INSERT INTO `modules_tree` VALUES (49, 49, 0, 'Edit User/');
INSERT INTO `modules_tree` VALUES (43, 49, 1, 'Users/Edit User/');
INSERT INTO `modules_tree` VALUES (4, 49, 2, 'Panel/Users/Edit User/');
INSERT INTO `modules_tree` VALUES (2, 49, 3, 'Backend/Panel/Users/Edit User/');
INSERT INTO `modules_tree` VALUES (1, 49, 4, 'System/Backend/Panel/Users/Edit User/');
INSERT INTO `modules_tree` VALUES (50, 50, 0, 'Get Roles/');
INSERT INTO `modules_tree` VALUES (25, 50, 1, 'Roles/Get Roles/');
INSERT INTO `modules_tree` VALUES (9, 50, 2, 'System Settings/Roles/Get Roles/');
INSERT INTO `modules_tree` VALUES (51, 51, 0, 'Articles/');
INSERT INTO `modules_tree` VALUES (4, 51, 1, 'Panel/Articles/');
INSERT INTO `modules_tree` VALUES (2, 51, 2, 'Backend/Panel/Articles/');
INSERT INTO `modules_tree` VALUES (1, 51, 3, 'System/Backend/Panel/Articles/');
INSERT INTO `modules_tree` VALUES (52, 52, 0, 'Add Article/');
INSERT INTO `modules_tree` VALUES (51, 52, 1, 'Articles/Add Article/');
INSERT INTO `modules_tree` VALUES (4, 52, 2, 'Panel/Articles/Add Article/');
INSERT INTO `modules_tree` VALUES (2, 52, 3, 'Backend/Panel/Articles/Add Article/');
INSERT INTO `modules_tree` VALUES (1, 52, 4, 'System/Backend/Panel/Articles/Add Article/');
INSERT INTO `modules_tree` VALUES (53, 53, 0, 'Show Articles List/');
INSERT INTO `modules_tree` VALUES (51, 53, 1, 'Articles/Show Articles List/');
INSERT INTO `modules_tree` VALUES (4, 53, 2, 'Panel/Articles/Show Articles List/');
INSERT INTO `modules_tree` VALUES (2, 53, 3, 'Backend/Panel/Articles/Show Articles List/');
INSERT INTO `modules_tree` VALUES (1, 53, 4, 'System/Backend/Panel/Articles/Show Articles List/');
INSERT INTO `modules_tree` VALUES (54, 54, 0, 'Edit Article/');
INSERT INTO `modules_tree` VALUES (51, 54, 1, 'Articles/Edit Article/');
INSERT INTO `modules_tree` VALUES (4, 54, 2, 'Panel/Articles/Edit Article/');
INSERT INTO `modules_tree` VALUES (2, 54, 3, 'Backend/Panel/Articles/Edit Article/');
INSERT INTO `modules_tree` VALUES (1, 54, 4, 'System/Backend/Panel/Articles/Edit Article/');
INSERT INTO `modules_tree` VALUES (55, 55, 0, 'Media Manager/');
INSERT INTO `modules_tree` VALUES (4, 55, 1, 'Panel/Media Manager/');
INSERT INTO `modules_tree` VALUES (2, 55, 2, 'Backend/Panel/Media Manager/');
INSERT INTO `modules_tree` VALUES (1, 55, 3, 'System/Backend/Panel/Media Manager/');
INSERT INTO `modules_tree` VALUES (4, 9, 1, 'Panel/System Settings/');
INSERT INTO `modules_tree` VALUES (4, 12, 2, 'Panel/System Settings/Modules/');
INSERT INTO `modules_tree` VALUES (4, 13, 3, 'Panel/System Settings/Modules/List Modules/');
INSERT INTO `modules_tree` VALUES (4, 16, 3, 'Panel/System Settings/Modules/Edit Module/');
INSERT INTO `modules_tree` VALUES (4, 10, 3, 'Panel/System Settings/Modules/Update Module/');
INSERT INTO `modules_tree` VALUES (4, 17, 2, 'Panel/System Settings/Panel Nodes/');
INSERT INTO `modules_tree` VALUES (4, 11, 3, 'Panel/System Settings/Panel Nodes/List Panel Nodes/');
INSERT INTO `modules_tree` VALUES (4, 18, 3, 'Panel/System Settings/Panel Nodes/Edit Panel Node/');
INSERT INTO `modules_tree` VALUES (4, 19, 3, 'Panel/System Settings/Panel Nodes/Update Panel Node/');
INSERT INTO `modules_tree` VALUES (4, 20, 3, 'Panel/System Settings/Panel Nodes/Add Panel Node/');
INSERT INTO `modules_tree` VALUES (4, 21, 3, 'Panel/System Settings/Panel Nodes/Insert Panel Node/');
INSERT INTO `modules_tree` VALUES (4, 22, 3, 'Panel/System Settings/Panel Nodes/Delete Panel Node/');
INSERT INTO `modules_tree` VALUES (4, 23, 3, 'Panel/System Settings/Modules/Add Module/');
INSERT INTO `modules_tree` VALUES (4, 24, 3, 'Panel/System Settings/Modules/Insert Module/');
INSERT INTO `modules_tree` VALUES (4, 25, 2, 'Panel/System Settings/Roles/');
INSERT INTO `modules_tree` VALUES (4, 26, 3, 'Panel/System Settings/Roles/List Roles/');
INSERT INTO `modules_tree` VALUES (4, 27, 3, 'Panel/System Settings/Roles/Add Role/');
INSERT INTO `modules_tree` VALUES (4, 28, 3, 'Panel/System Settings/Roles/Edit Role/');
INSERT INTO `modules_tree` VALUES (4, 29, 3, 'Panel/System Settings/Roles/Update Role/');
INSERT INTO `modules_tree` VALUES (4, 30, 3, 'Panel/System Settings/Roles/Insert Role/');
INSERT INTO `modules_tree` VALUES (4, 31, 3, 'Panel/System Settings/Modules/Delete Module/');
INSERT INTO `modules_tree` VALUES (4, 32, 3, 'Panel/System Settings/Roles/Delete Role/');
INSERT INTO `modules_tree` VALUES (4, 33, 2, 'Panel/System Settings/Access Levels/');
INSERT INTO `modules_tree` VALUES (4, 34, 3, 'Panel/System Settings/Access Levels/List Access Levels/');
INSERT INTO `modules_tree` VALUES (4, 35, 3, 'Panel/System Settings/Access Levels/Edit Access Level/');
INSERT INTO `modules_tree` VALUES (4, 36, 3, 'Panel/System Settings/Access Levels/update Access Level/');
INSERT INTO `modules_tree` VALUES (4, 37, 3, 'Panel/System Settings/Access Levels/Add Access Level/');
INSERT INTO `modules_tree` VALUES (4, 38, 3, 'Panel/System Settings/Access Levels/Insert Access Level/');
INSERT INTO `modules_tree` VALUES (4, 39, 3, 'Panel/System Settings/Access Levels/Delete Access Level/');
INSERT INTO `modules_tree` VALUES (4, 40, 2, 'Panel/System Settings/General Settings/');
INSERT INTO `modules_tree` VALUES (4, 41, 3, 'Panel/System Settings/General Settings/Edit General Settings/');
INSERT INTO `modules_tree` VALUES (4, 42, 3, 'Panel/System Settings/General Settings/Update General Settings/');
INSERT INTO `modules_tree` VALUES (4, 50, 3, 'Panel/System Settings/Roles/Get Roles/');
INSERT INTO `modules_tree` VALUES (2, 9, 2, 'Backend/Panel/System Settings/');
INSERT INTO `modules_tree` VALUES (2, 12, 3, 'Backend/Panel/System Settings/Modules/');
INSERT INTO `modules_tree` VALUES (2, 13, 4, 'Backend/Panel/System Settings/Modules/List Modules/');
INSERT INTO `modules_tree` VALUES (2, 16, 4, 'Backend/Panel/System Settings/Modules/Edit Module/');
INSERT INTO `modules_tree` VALUES (2, 10, 4, 'Backend/Panel/System Settings/Modules/Update Module/');
INSERT INTO `modules_tree` VALUES (2, 17, 3, 'Backend/Panel/System Settings/Panel Nodes/');
INSERT INTO `modules_tree` VALUES (2, 11, 4, 'Backend/Panel/System Settings/Panel Nodes/List Panel Nodes/');
INSERT INTO `modules_tree` VALUES (2, 18, 4, 'Backend/Panel/System Settings/Panel Nodes/Edit Panel Node/');
INSERT INTO `modules_tree` VALUES (2, 19, 4, 'Backend/Panel/System Settings/Panel Nodes/Update Panel Node/');
INSERT INTO `modules_tree` VALUES (2, 20, 4, 'Backend/Panel/System Settings/Panel Nodes/Add Panel Node/');
INSERT INTO `modules_tree` VALUES (2, 21, 4, 'Backend/Panel/System Settings/Panel Nodes/Insert Panel Node/');
INSERT INTO `modules_tree` VALUES (2, 22, 4, 'Backend/Panel/System Settings/Panel Nodes/Delete Panel Node/');
INSERT INTO `modules_tree` VALUES (2, 23, 4, 'Backend/Panel/System Settings/Modules/Add Module/');
INSERT INTO `modules_tree` VALUES (2, 24, 4, 'Backend/Panel/System Settings/Modules/Insert Module/');
INSERT INTO `modules_tree` VALUES (2, 25, 3, 'Backend/Panel/System Settings/Roles/');
INSERT INTO `modules_tree` VALUES (2, 26, 4, 'Backend/Panel/System Settings/Roles/List Roles/');
INSERT INTO `modules_tree` VALUES (2, 27, 4, 'Backend/Panel/System Settings/Roles/Add Role/');
INSERT INTO `modules_tree` VALUES (2, 28, 4, 'Backend/Panel/System Settings/Roles/Edit Role/');
INSERT INTO `modules_tree` VALUES (2, 29, 4, 'Backend/Panel/System Settings/Roles/Update Role/');
INSERT INTO `modules_tree` VALUES (2, 30, 4, 'Backend/Panel/System Settings/Roles/Insert Role/');
INSERT INTO `modules_tree` VALUES (2, 31, 4, 'Backend/Panel/System Settings/Modules/Delete Module/');
INSERT INTO `modules_tree` VALUES (2, 32, 4, 'Backend/Panel/System Settings/Roles/Delete Role/');
INSERT INTO `modules_tree` VALUES (2, 33, 3, 'Backend/Panel/System Settings/Access Levels/');
INSERT INTO `modules_tree` VALUES (2, 34, 4, 'Backend/Panel/System Settings/Access Levels/List Access Levels/');
INSERT INTO `modules_tree` VALUES (2, 35, 4, 'Backend/Panel/System Settings/Access Levels/Edit Access Level/');
INSERT INTO `modules_tree` VALUES (2, 36, 4, 'Backend/Panel/System Settings/Access Levels/update Access Level/');
INSERT INTO `modules_tree` VALUES (2, 37, 4, 'Backend/Panel/System Settings/Access Levels/Add Access Level/');
INSERT INTO `modules_tree` VALUES (2, 38, 4, 'Backend/Panel/System Settings/Access Levels/Insert Access Level/');
INSERT INTO `modules_tree` VALUES (2, 39, 4, 'Backend/Panel/System Settings/Access Levels/Delete Access Level/');
INSERT INTO `modules_tree` VALUES (2, 40, 3, 'Backend/Panel/System Settings/General Settings/');
INSERT INTO `modules_tree` VALUES (2, 41, 4, 'Backend/Panel/System Settings/General Settings/Edit General Settings/');
INSERT INTO `modules_tree` VALUES (2, 42, 4, 'Backend/Panel/System Settings/General Settings/Update General Settings/');
INSERT INTO `modules_tree` VALUES (2, 50, 4, 'Backend/Panel/System Settings/Roles/Get Roles/');
INSERT INTO `modules_tree` VALUES (1, 9, 3, 'System/Backend/Panel/System Settings/');
INSERT INTO `modules_tree` VALUES (1, 12, 4, 'System/Backend/Panel/System Settings/Modules/');
INSERT INTO `modules_tree` VALUES (1, 13, 5, 'System/Backend/Panel/System Settings/Modules/List Modules/');
INSERT INTO `modules_tree` VALUES (1, 16, 5, 'System/Backend/Panel/System Settings/Modules/Edit Module/');
INSERT INTO `modules_tree` VALUES (1, 10, 5, 'System/Backend/Panel/System Settings/Modules/Update Module/');
INSERT INTO `modules_tree` VALUES (1, 17, 4, 'System/Backend/Panel/System Settings/Panel Nodes/');
INSERT INTO `modules_tree` VALUES (1, 11, 5, 'System/Backend/Panel/System Settings/Panel Nodes/List Panel Nodes/');
INSERT INTO `modules_tree` VALUES (1, 18, 5, 'System/Backend/Panel/System Settings/Panel Nodes/Edit Panel Node/');
INSERT INTO `modules_tree` VALUES (1, 19, 5, 'System/Backend/Panel/System Settings/Panel Nodes/Update Panel Node/');
INSERT INTO `modules_tree` VALUES (1, 20, 5, 'System/Backend/Panel/System Settings/Panel Nodes/Add Panel Node/');
INSERT INTO `modules_tree` VALUES (1, 21, 5, 'System/Backend/Panel/System Settings/Panel Nodes/Insert Panel Node/');
INSERT INTO `modules_tree` VALUES (1, 22, 5, 'System/Backend/Panel/System Settings/Panel Nodes/Delete Panel Node/');
INSERT INTO `modules_tree` VALUES (1, 23, 5, 'System/Backend/Panel/System Settings/Modules/Add Module/');
INSERT INTO `modules_tree` VALUES (1, 24, 5, 'System/Backend/Panel/System Settings/Modules/Insert Module/');
INSERT INTO `modules_tree` VALUES (1, 25, 4, 'System/Backend/Panel/System Settings/Roles/');
INSERT INTO `modules_tree` VALUES (1, 26, 5, 'System/Backend/Panel/System Settings/Roles/List Roles/');
INSERT INTO `modules_tree` VALUES (1, 27, 5, 'System/Backend/Panel/System Settings/Roles/Add Role/');
INSERT INTO `modules_tree` VALUES (1, 28, 5, 'System/Backend/Panel/System Settings/Roles/Edit Role/');
INSERT INTO `modules_tree` VALUES (1, 29, 5, 'System/Backend/Panel/System Settings/Roles/Update Role/');
INSERT INTO `modules_tree` VALUES (1, 30, 5, 'System/Backend/Panel/System Settings/Roles/Insert Role/');
INSERT INTO `modules_tree` VALUES (1, 31, 5, 'System/Backend/Panel/System Settings/Modules/Delete Module/');
INSERT INTO `modules_tree` VALUES (1, 32, 5, 'System/Backend/Panel/System Settings/Roles/Delete Role/');
INSERT INTO `modules_tree` VALUES (1, 33, 4, 'System/Backend/Panel/System Settings/Access Levels/');
INSERT INTO `modules_tree` VALUES (1, 34, 5, 'System/Backend/Panel/System Settings/Access Levels/List Access Levels/');
INSERT INTO `modules_tree` VALUES (1, 35, 5, 'System/Backend/Panel/System Settings/Access Levels/Edit Access Level/');
INSERT INTO `modules_tree` VALUES (1, 36, 5, 'System/Backend/Panel/System Settings/Access Levels/update Access Level/');
INSERT INTO `modules_tree` VALUES (1, 37, 5, 'System/Backend/Panel/System Settings/Access Levels/Add Access Level/');
INSERT INTO `modules_tree` VALUES (1, 38, 5, 'System/Backend/Panel/System Settings/Access Levels/Insert Access Level/');
INSERT INTO `modules_tree` VALUES (1, 39, 5, 'System/Backend/Panel/System Settings/Access Levels/Delete Access Level/');
INSERT INTO `modules_tree` VALUES (1, 40, 4, 'System/Backend/Panel/System Settings/General Settings/');
INSERT INTO `modules_tree` VALUES (1, 41, 5, 'System/Backend/Panel/System Settings/General Settings/Edit General Settings/');
INSERT INTO `modules_tree` VALUES (1, 42, 5, 'System/Backend/Panel/System Settings/General Settings/Update General Settings/');
INSERT INTO `modules_tree` VALUES (1, 50, 5, 'System/Backend/Panel/System Settings/Roles/Get Roles/');
INSERT INTO `modules_tree` VALUES (56, 56, 0, 'Categories/');
INSERT INTO `modules_tree` VALUES (4, 56, 1, 'Panel/Categories/');
INSERT INTO `modules_tree` VALUES (2, 56, 2, 'Backend/Panel/Categories/');
INSERT INTO `modules_tree` VALUES (1, 56, 3, 'System/Backend/Panel/Categories/');
INSERT INTO `modules_tree` VALUES (57, 57, 0, 'Show Categories List/');
INSERT INTO `modules_tree` VALUES (56, 57, 1, 'Categories/Show Categories List/');
INSERT INTO `modules_tree` VALUES (4, 57, 2, 'Panel/Categories/Show Categories List/');
INSERT INTO `modules_tree` VALUES (2, 57, 3, 'Backend/Panel/Categories/Show Categories List/');
INSERT INTO `modules_tree` VALUES (1, 57, 4, 'System/Backend/Panel/Categories/Show Categories List/');
INSERT INTO `modules_tree` VALUES (58, 58, 0, 'Add Category/');
INSERT INTO `modules_tree` VALUES (56, 58, 1, 'Categories/Add Category/');
INSERT INTO `modules_tree` VALUES (4, 58, 2, 'Panel/Categories/Add Category/');
INSERT INTO `modules_tree` VALUES (2, 58, 3, 'Backend/Panel/Categories/Add Category/');
INSERT INTO `modules_tree` VALUES (1, 58, 4, 'System/Backend/Panel/Categories/Add Category/');
INSERT INTO `modules_tree` VALUES (59, 59, 0, 'Frontend Categories/');
INSERT INTO `modules_tree` VALUES (3, 59, 1, 'Frontend/Frontend Categories/');
INSERT INTO `modules_tree` VALUES (1, 59, 2, 'System/Frontend/Frontend Categories/');
INSERT INTO `modules_tree` VALUES (60, 60, 0, 'View Frontend Category/');
INSERT INTO `modules_tree` VALUES (59, 60, 1, 'Frontend Categories/View Frontend Category/');
INSERT INTO `modules_tree` VALUES (3, 60, 2, 'Frontend/Frontend Categories/View Frontend Category/');
INSERT INTO `modules_tree` VALUES (1, 60, 3, 'System/Frontend/Frontend Categories/View Frontend Category/');
INSERT INTO `modules_tree` VALUES (61, 61, 0, 'Edit Category/');
INSERT INTO `modules_tree` VALUES (56, 61, 1, 'Categories/Edit Category/');
INSERT INTO `modules_tree` VALUES (4, 61, 2, 'Panel/Categories/Edit Category/');
INSERT INTO `modules_tree` VALUES (2, 61, 3, 'Backend/Panel/Categories/Edit Category/');
INSERT INTO `modules_tree` VALUES (1, 61, 4, 'System/Backend/Panel/Categories/Edit Category/');
INSERT INTO `modules_tree` VALUES (9, 15, 1, 'System Settings/Show System Settings/');
INSERT INTO `modules_tree` VALUES (4, 15, 2, 'Panel/System Settings/Show System Settings/');
INSERT INTO `modules_tree` VALUES (2, 15, 3, 'Backend/Panel/System Settings/Show System Settings/');
INSERT INTO `modules_tree` VALUES (1, 15, 4, 'System/Backend/Panel/System Settings/Show System Settings/');
INSERT INTO `modules_tree` VALUES (62, 62, 0, 'Insert Category/');
INSERT INTO `modules_tree` VALUES (56, 62, 1, 'Categories/Insert Category/');
INSERT INTO `modules_tree` VALUES (4, 62, 2, 'Panel/Categories/Insert Category/');
INSERT INTO `modules_tree` VALUES (2, 62, 3, 'Backend/Panel/Categories/Insert Category/');
INSERT INTO `modules_tree` VALUES (1, 62, 4, 'System/Backend/Panel/Categories/Insert Category/');
INSERT INTO `modules_tree` VALUES (63, 63, 0, 'Update Category/');
INSERT INTO `modules_tree` VALUES (56, 63, 1, 'Categories/Update Category/');
INSERT INTO `modules_tree` VALUES (4, 63, 2, 'Panel/Categories/Update Category/');
INSERT INTO `modules_tree` VALUES (2, 63, 3, 'Backend/Panel/Categories/Update Category/');
INSERT INTO `modules_tree` VALUES (1, 63, 4, 'System/Backend/Panel/Categories/Update Category/');
INSERT INTO `modules_tree` VALUES (64, 64, 0, 'Delete Category/');
INSERT INTO `modules_tree` VALUES (56, 64, 1, 'Categories/Delete Category/');
INSERT INTO `modules_tree` VALUES (4, 64, 2, 'Panel/Categories/Delete Category/');
INSERT INTO `modules_tree` VALUES (2, 64, 3, 'Backend/Panel/Categories/Delete Category/');
INSERT INTO `modules_tree` VALUES (1, 64, 4, 'System/Backend/Panel/Categories/Delete Category/');
INSERT INTO `modules_tree` VALUES (65, 65, 0, 'Get Categories/');
INSERT INTO `modules_tree` VALUES (56, 65, 1, 'Categories/Get Categories/');
INSERT INTO `modules_tree` VALUES (4, 65, 2, 'Panel/Categories/Get Categories/');
INSERT INTO `modules_tree` VALUES (2, 65, 3, 'Backend/Panel/Categories/Get Categories/');
INSERT INTO `modules_tree` VALUES (1, 65, 4, 'System/Backend/Panel/Categories/Get Categories/');
INSERT INTO `modules_tree` VALUES (66, 66, 0, 'Pages/');
INSERT INTO `modules_tree` VALUES (4, 66, 1, 'Panel/Pages/');
INSERT INTO `modules_tree` VALUES (2, 66, 2, 'Backend/Panel/Pages/');
INSERT INTO `modules_tree` VALUES (1, 66, 3, 'System/Backend/Panel/Pages/');
INSERT INTO `modules_tree` VALUES (67, 67, 0, 'Show Pages List/');
INSERT INTO `modules_tree` VALUES (66, 67, 1, 'Pages/Show Pages List/');
INSERT INTO `modules_tree` VALUES (4, 67, 2, 'Panel/Pages/Show Pages List/');
INSERT INTO `modules_tree` VALUES (2, 67, 3, 'Backend/Panel/Pages/Show Pages List/');
INSERT INTO `modules_tree` VALUES (1, 67, 4, 'System/Backend/Panel/Pages/Show Pages List/');
INSERT INTO `modules_tree` VALUES (68, 68, 0, 'Add Page/');
INSERT INTO `modules_tree` VALUES (66, 68, 1, 'Pages/Add Page/');
INSERT INTO `modules_tree` VALUES (4, 68, 2, 'Panel/Pages/Add Page/');
INSERT INTO `modules_tree` VALUES (2, 68, 3, 'Backend/Panel/Pages/Add Page/');
INSERT INTO `modules_tree` VALUES (1, 68, 4, 'System/Backend/Panel/Pages/Add Page/');
INSERT INTO `modules_tree` VALUES (69, 69, 0, 'Edit Page/');
INSERT INTO `modules_tree` VALUES (66, 69, 1, 'Pages/Edit Page/');
INSERT INTO `modules_tree` VALUES (4, 69, 2, 'Panel/Pages/Edit Page/');
INSERT INTO `modules_tree` VALUES (2, 69, 3, 'Backend/Panel/Pages/Edit Page/');
INSERT INTO `modules_tree` VALUES (1, 69, 4, 'System/Backend/Panel/Pages/Edit Page/');
INSERT INTO `modules_tree` VALUES (70, 70, 0, 'Toggle Side Menu State/');
INSERT INTO `modules_tree` VALUES (4, 70, 1, 'Panel/Toggle Side Menu State/');
INSERT INTO `modules_tree` VALUES (2, 70, 2, 'Backend/Panel/Toggle Side Menu State/');
INSERT INTO `modules_tree` VALUES (1, 70, 3, 'System/Backend/Panel/Toggle Side Menu State/');
INSERT INTO `modules_tree` VALUES (71, 71, 0, 'Delete Category Translation/');
INSERT INTO `modules_tree` VALUES (56, 71, 1, 'Categories/Delete Category Translation/');
INSERT INTO `modules_tree` VALUES (4, 71, 2, 'Panel/Categories/Delete Category Translation/');
INSERT INTO `modules_tree` VALUES (2, 71, 3, 'Backend/Panel/Categories/Delete Category Translation/');
INSERT INTO `modules_tree` VALUES (1, 71, 4, 'System/Backend/Panel/Categories/Delete Category Translation/');
INSERT INTO `modules_tree` VALUES (72, 72, 0, 'Update Page/');
INSERT INTO `modules_tree` VALUES (66, 72, 1, 'Pages/Update Page/');
INSERT INTO `modules_tree` VALUES (4, 72, 2, 'Panel/Pages/Update Page/');
INSERT INTO `modules_tree` VALUES (2, 72, 3, 'Backend/Panel/Pages/Update Page/');
INSERT INTO `modules_tree` VALUES (1, 72, 4, 'System/Backend/Panel/Pages/Update Page/');
INSERT INTO `modules_tree` VALUES (73, 73, 0, 'Delete Page/');
INSERT INTO `modules_tree` VALUES (66, 73, 1, 'Pages/Delete Page/');
INSERT INTO `modules_tree` VALUES (4, 73, 2, 'Panel/Pages/Delete Page/');
INSERT INTO `modules_tree` VALUES (2, 73, 3, 'Backend/Panel/Pages/Delete Page/');
INSERT INTO `modules_tree` VALUES (1, 73, 4, 'System/Backend/Panel/Pages/Delete Page/');
INSERT INTO `modules_tree` VALUES (74, 74, 0, 'Frontend Pages/');
INSERT INTO `modules_tree` VALUES (3, 74, 1, 'Frontend/Frontend Pages/');
INSERT INTO `modules_tree` VALUES (1, 74, 2, 'System/Frontend/Frontend Pages/');
INSERT INTO `modules_tree` VALUES (75, 75, 0, 'View Frontend page/');
INSERT INTO `modules_tree` VALUES (74, 75, 1, 'Frontend Pages/View Frontend page/');
INSERT INTO `modules_tree` VALUES (3, 75, 2, 'Frontend/Frontend Pages/View Frontend page/');
INSERT INTO `modules_tree` VALUES (1, 75, 3, 'System/Frontend/Frontend Pages/View Frontend page/');
INSERT INTO `modules_tree` VALUES (76, 76, 0, 'Insert Page/');
INSERT INTO `modules_tree` VALUES (66, 76, 1, 'Pages/Insert Page/');
INSERT INTO `modules_tree` VALUES (4, 76, 2, 'Panel/Pages/Insert Page/');
INSERT INTO `modules_tree` VALUES (2, 76, 3, 'Backend/Panel/Pages/Insert Page/');
INSERT INTO `modules_tree` VALUES (1, 76, 4, 'System/Backend/Panel/Pages/Insert Page/');
INSERT INTO `modules_tree` VALUES (77, 77, 0, 'Get Pages/');
INSERT INTO `modules_tree` VALUES (66, 77, 1, 'Pages/Get Pages/');
INSERT INTO `modules_tree` VALUES (4, 77, 2, 'Panel/Pages/Get Pages/');
INSERT INTO `modules_tree` VALUES (2, 77, 3, 'Backend/Panel/Pages/Get Pages/');
INSERT INTO `modules_tree` VALUES (1, 77, 4, 'System/Backend/Panel/Pages/Get Pages/');
INSERT INTO `modules_tree` VALUES (78, 78, 0, 'Frontend Articles/');
INSERT INTO `modules_tree` VALUES (3, 78, 1, 'Frontend/Frontend Articles/');
INSERT INTO `modules_tree` VALUES (1, 78, 2, 'System/Frontend/Frontend Articles/');
INSERT INTO `modules_tree` VALUES (79, 79, 0, 'View Frontend Article/');
INSERT INTO `modules_tree` VALUES (78, 79, 1, 'Frontend Articles/View Frontend Article/');
INSERT INTO `modules_tree` VALUES (3, 79, 2, 'Frontend/Frontend Articles/View Frontend Article/');
INSERT INTO `modules_tree` VALUES (1, 79, 3, 'System/Frontend/Frontend Articles/View Frontend Article/');
INSERT INTO `modules_tree` VALUES (80, 80, 0, 'Delete Article/');
INSERT INTO `modules_tree` VALUES (51, 80, 1, 'Articles/Delete Article/');
INSERT INTO `modules_tree` VALUES (4, 80, 2, 'Panel/Articles/Delete Article/');
INSERT INTO `modules_tree` VALUES (2, 80, 3, 'Backend/Panel/Articles/Delete Article/');
INSERT INTO `modules_tree` VALUES (1, 80, 4, 'System/Backend/Panel/Articles/Delete Article/');
INSERT INTO `modules_tree` VALUES (1, 81, 0, 'Update Article/');
INSERT INTO `modules_tree` VALUES (51, 81, 1, 'Articles/Update Article/');
INSERT INTO `modules_tree` VALUES (4, 81, 2, 'Panel/Articles/Update Article/');
INSERT INTO `modules_tree` VALUES (2, 81, 3, 'Backend/Panel/Articles/Update Article/');
INSERT INTO `modules_tree` VALUES (1, 81, 4, 'System/Backend/Panel/Articles/Update Article/');
INSERT INTO `modules_tree` VALUES (82, 82, 0, 'Insert Article/');
INSERT INTO `modules_tree` VALUES (51, 82, 1, 'Articles/Insert Article/');
INSERT INTO `modules_tree` VALUES (4, 82, 2, 'Panel/Articles/Insert Article/');
INSERT INTO `modules_tree` VALUES (2, 82, 3, 'Backend/Panel/Articles/Insert Article/');
INSERT INTO `modules_tree` VALUES (1, 82, 4, 'System/Backend/Panel/Articles/Insert Article/');
INSERT INTO `modules_tree` VALUES (83, 83, 0, 'Delete Article translation/');
INSERT INTO `modules_tree` VALUES (51, 83, 1, 'Articles/Delete Article translation/');
INSERT INTO `modules_tree` VALUES (4, 83, 2, 'Panel/Articles/Delete Article translation/');
INSERT INTO `modules_tree` VALUES (2, 83, 3, 'Backend/Panel/Articles/Delete Article translation/');
INSERT INTO `modules_tree` VALUES (1, 83, 4, 'System/Backend/Panel/Articles/Delete Article translation/');
INSERT INTO `modules_tree` VALUES (84, 84, 0, 'Get Categories/');
INSERT INTO `modules_tree` VALUES (51, 84, 1, 'Articles/Get Categories/');
INSERT INTO `modules_tree` VALUES (4, 84, 2, 'Panel/Articles/Get Categories/');
INSERT INTO `modules_tree` VALUES (2, 84, 3, 'Backend/Panel/Articles/Get Categories/');
INSERT INTO `modules_tree` VALUES (1, 84, 4, 'System/Backend/Panel/Articles/Get Categories/');
INSERT INTO `modules_tree` VALUES (85, 85, 0, 'Edit own article/');
INSERT INTO `modules_tree` VALUES (51, 85, 1, 'Articles/Edit own article/');
INSERT INTO `modules_tree` VALUES (4, 85, 2, 'Panel/Articles/Edit own article/');
INSERT INTO `modules_tree` VALUES (2, 85, 3, 'Backend/Panel/Articles/Edit own article/');
INSERT INTO `modules_tree` VALUES (1, 85, 4, 'System/Backend/Panel/Articles/Edit own article/');
INSERT INTO `modules_tree` VALUES (86, 86, 0, 'Search tags/');
INSERT INTO `modules_tree` VALUES (51, 86, 1, 'Articles/Search tags/');
INSERT INTO `modules_tree` VALUES (4, 86, 2, 'Panel/Articles/Search tags/');
INSERT INTO `modules_tree` VALUES (2, 86, 3, 'Backend/Panel/Articles/Search tags/');
INSERT INTO `modules_tree` VALUES (1, 86, 4, 'System/Backend/Panel/Articles/Search tags/');
INSERT INTO `modules_tree` VALUES (87, 87, 0, 'Frontend tags/');
INSERT INTO `modules_tree` VALUES (3, 87, 1, 'Frontend/Frontend tags/');
INSERT INTO `modules_tree` VALUES (1, 87, 2, 'System/Frontend/Frontend tags/');
INSERT INTO `modules_tree` VALUES (88, 88, 0, 'View Frontend tag/');
INSERT INTO `modules_tree` VALUES (87, 88, 1, 'Frontend tags/View Frontend tag/');
INSERT INTO `modules_tree` VALUES (3, 88, 2, 'Frontend/Frontend tags/View Frontend tag/');
INSERT INTO `modules_tree` VALUES (1, 88, 3, 'System/Frontend/Frontend tags/View Frontend tag/');
INSERT INTO `modules_tree` VALUES (89, 89, 0, 'Tags/');
INSERT INTO `modules_tree` VALUES (4, 89, 1, 'Panel/Tags/');
INSERT INTO `modules_tree` VALUES (2, 89, 2, 'Backend/Panel/Tags/');
INSERT INTO `modules_tree` VALUES (1, 89, 3, 'System/Backend/Panel/Tags/');
INSERT INTO `modules_tree` VALUES (90, 90, 0, 'Add Tag/');
INSERT INTO `modules_tree` VALUES (89, 90, 1, 'Tags/Add Tag/');
INSERT INTO `modules_tree` VALUES (4, 90, 2, 'Panel/Tags/Add Tag/');
INSERT INTO `modules_tree` VALUES (2, 90, 3, 'Backend/Panel/Tags/Add Tag/');
INSERT INTO `modules_tree` VALUES (1, 90, 4, 'System/Backend/Panel/Tags/Add Tag/');
INSERT INTO `modules_tree` VALUES (91, 91, 0, 'Edit Tag/');
INSERT INTO `modules_tree` VALUES (89, 91, 1, 'Tags/Edit Tag/');
INSERT INTO `modules_tree` VALUES (4, 91, 2, 'Panel/Tags/Edit Tag/');
INSERT INTO `modules_tree` VALUES (2, 91, 3, 'Backend/Panel/Tags/Edit Tag/');
INSERT INTO `modules_tree` VALUES (1, 91, 4, 'System/Backend/Panel/Tags/Edit Tag/');
INSERT INTO `modules_tree` VALUES (92, 92, 0, 'Delete tag/');
INSERT INTO `modules_tree` VALUES (89, 92, 1, 'Tags/Delete tag/');
INSERT INTO `modules_tree` VALUES (4, 92, 2, 'Panel/Tags/Delete tag/');
INSERT INTO `modules_tree` VALUES (2, 92, 3, 'Backend/Panel/Tags/Delete tag/');
INSERT INTO `modules_tree` VALUES (1, 92, 4, 'System/Backend/Panel/Tags/Delete tag/');
INSERT INTO `modules_tree` VALUES (93, 93, 0, 'Insert Tag/');
INSERT INTO `modules_tree` VALUES (89, 93, 1, 'Tags/Insert Tag/');
INSERT INTO `modules_tree` VALUES (4, 93, 2, 'Panel/Tags/Insert Tag/');
INSERT INTO `modules_tree` VALUES (2, 93, 3, 'Backend/Panel/Tags/Insert Tag/');
INSERT INTO `modules_tree` VALUES (1, 93, 4, 'System/Backend/Panel/Tags/Insert Tag/');
INSERT INTO `modules_tree` VALUES (94, 94, 0, 'Update tag/');
INSERT INTO `modules_tree` VALUES (89, 94, 1, 'Tags/Update tag/');
INSERT INTO `modules_tree` VALUES (4, 94, 2, 'Panel/Tags/Update tag/');
INSERT INTO `modules_tree` VALUES (2, 94, 3, 'Backend/Panel/Tags/Update tag/');
INSERT INTO `modules_tree` VALUES (1, 94, 4, 'System/Backend/Panel/Tags/Update tag/');
INSERT INTO `modules_tree` VALUES (95, 95, 0, 'Show Tags list/');
INSERT INTO `modules_tree` VALUES (89, 95, 1, 'Tags/Show Tags list/');
INSERT INTO `modules_tree` VALUES (4, 95, 2, 'Panel/Tags/Show Tags list/');
INSERT INTO `modules_tree` VALUES (2, 95, 3, 'Backend/Panel/Tags/Show Tags list/');
INSERT INTO `modules_tree` VALUES (1, 95, 4, 'System/Backend/Panel/Tags/Show Tags list/');
INSERT INTO `modules_tree` VALUES (96, 96, 0, 'Get Extra Fields/');
INSERT INTO `modules_tree` VALUES (51, 96, 1, 'Articles/Get Extra Fields/');
INSERT INTO `modules_tree` VALUES (4, 96, 2, 'Panel/Articles/Get Extra Fields/');
INSERT INTO `modules_tree` VALUES (2, 96, 3, 'Backend/Panel/Articles/Get Extra Fields/');
INSERT INTO `modules_tree` VALUES (1, 96, 4, 'System/Backend/Panel/Articles/Get Extra Fields/');
INSERT INTO `modules_tree` VALUES (97, 97, 0, 'Menus/');
INSERT INTO `modules_tree` VALUES (4, 97, 1, 'Panel/Menus/');
INSERT INTO `modules_tree` VALUES (2, 97, 2, 'Backend/Panel/Menus/');
INSERT INTO `modules_tree` VALUES (1, 97, 3, 'System/Backend/Panel/Menus/');
INSERT INTO `modules_tree` VALUES (98, 98, 0, 'Show Menus/');
INSERT INTO `modules_tree` VALUES (97, 98, 1, 'Menus/Show Menus/');
INSERT INTO `modules_tree` VALUES (4, 98, 2, 'Panel/Menus/Show Menus/');
INSERT INTO `modules_tree` VALUES (2, 98, 3, 'Backend/Panel/Menus/Show Menus/');
INSERT INTO `modules_tree` VALUES (1, 98, 4, 'System/Backend/Panel/Menus/Show Menus/');
INSERT INTO `modules_tree` VALUES (99, 99, 0, 'Get Languages/');
INSERT INTO `modules_tree` VALUES (4, 99, 1, 'Panel/Get Languages/');
INSERT INTO `modules_tree` VALUES (2, 99, 2, 'Backend/Panel/Get Languages/');
INSERT INTO `modules_tree` VALUES (1, 99, 3, 'System/Backend/Panel/Get Languages/');
INSERT INTO `modules_tree` VALUES (100, 100, 0, 'Update Menu Order/');
INSERT INTO `modules_tree` VALUES (97, 100, 1, 'Menus/Update Menu Order/');
INSERT INTO `modules_tree` VALUES (4, 100, 2, 'Panel/Menus/Update Menu Order/');
INSERT INTO `modules_tree` VALUES (2, 100, 3, 'Backend/Panel/Menus/Update Menu Order/');
INSERT INTO `modules_tree` VALUES (1, 100, 4, 'System/Backend/Panel/Menus/Update Menu Order/');
INSERT INTO `modules_tree` VALUES (101, 101, 0, 'Get Menu Info/');
INSERT INTO `modules_tree` VALUES (97, 101, 1, 'Menus/Get Menu Info/');
INSERT INTO `modules_tree` VALUES (4, 101, 2, 'Panel/Menus/Get Menu Info/');
INSERT INTO `modules_tree` VALUES (2, 101, 3, 'Backend/Panel/Menus/Get Menu Info/');
INSERT INTO `modules_tree` VALUES (1, 101, 4, 'System/Backend/Panel/Menus/Get Menu Info/');
INSERT INTO `modules_tree` VALUES (102, 102, 0, 'Update menu info/');
INSERT INTO `modules_tree` VALUES (97, 102, 1, 'Menus/Update menu info/');
INSERT INTO `modules_tree` VALUES (4, 102, 2, 'Panel/Menus/Update menu info/');
INSERT INTO `modules_tree` VALUES (2, 102, 3, 'Backend/Panel/Menus/Update menu info/');
INSERT INTO `modules_tree` VALUES (1, 102, 4, 'System/Backend/Panel/Menus/Update menu info/');
INSERT INTO `modules_tree` VALUES (103, 103, 0, 'Languages/');
INSERT INTO `modules_tree` VALUES (9, 103, 1, 'System Settings/Languages/');
INSERT INTO `modules_tree` VALUES (4, 103, 2, 'Panel/System Settings/Languages/');
INSERT INTO `modules_tree` VALUES (2, 103, 3, 'Backend/Panel/System Settings/Languages/');
INSERT INTO `modules_tree` VALUES (1, 103, 4, 'System/Backend/Panel/System Settings/Languages/');
INSERT INTO `modules_tree` VALUES (104, 104, 0, 'Show Languages/');
INSERT INTO `modules_tree` VALUES (103, 104, 1, 'Languages/Show Languages/');
INSERT INTO `modules_tree` VALUES (9, 104, 2, 'System Settings/Languages/Show Languages/');
INSERT INTO `modules_tree` VALUES (4, 104, 3, 'Panel/System Settings/Languages/Show Languages/');
INSERT INTO `modules_tree` VALUES (2, 104, 4, 'Backend/Panel/System Settings/Languages/Show Languages/');
INSERT INTO `modules_tree` VALUES (1, 104, 5, 'System/Backend/Panel/System Settings/Languages/Show Languages/');
INSERT INTO `modules_tree` VALUES (105, 105, 0, 'Add Language/');
INSERT INTO `modules_tree` VALUES (103, 105, 1, 'Languages/Add Language/');
INSERT INTO `modules_tree` VALUES (9, 105, 2, 'System Settings/Languages/Add Language/');
INSERT INTO `modules_tree` VALUES (4, 105, 3, 'Panel/System Settings/Languages/Add Language/');
INSERT INTO `modules_tree` VALUES (2, 105, 4, 'Backend/Panel/System Settings/Languages/Add Language/');
INSERT INTO `modules_tree` VALUES (1, 105, 5, 'System/Backend/Panel/System Settings/Languages/Add Language/');
INSERT INTO `modules_tree` VALUES (106, 106, 0, 'Edit Language/');
INSERT INTO `modules_tree` VALUES (103, 106, 1, 'Languages/Edit Language/');
INSERT INTO `modules_tree` VALUES (9, 106, 2, 'System Settings/Languages/Edit Language/');
INSERT INTO `modules_tree` VALUES (4, 106, 3, 'Panel/System Settings/Languages/Edit Language/');
INSERT INTO `modules_tree` VALUES (2, 106, 4, 'Backend/Panel/System Settings/Languages/Edit Language/');
INSERT INTO `modules_tree` VALUES (1, 106, 5, 'System/Backend/Panel/System Settings/Languages/Edit Language/');
INSERT INTO `modules_tree` VALUES (107, 107, 0, 'Insert Language/');
INSERT INTO `modules_tree` VALUES (103, 107, 1, 'Languages/Insert Language/');
INSERT INTO `modules_tree` VALUES (9, 107, 2, 'System Settings/Languages/Insert Language/');
INSERT INTO `modules_tree` VALUES (4, 107, 3, 'Panel/System Settings/Languages/Insert Language/');
INSERT INTO `modules_tree` VALUES (2, 107, 4, 'Backend/Panel/System Settings/Languages/Insert Language/');
INSERT INTO `modules_tree` VALUES (1, 107, 5, 'System/Backend/Panel/System Settings/Languages/Insert Language/');
INSERT INTO `modules_tree` VALUES (108, 108, 0, 'Update Language/');
INSERT INTO `modules_tree` VALUES (103, 108, 1, 'Languages/Update Language/');
INSERT INTO `modules_tree` VALUES (9, 108, 2, 'System Settings/Languages/Update Language/');
INSERT INTO `modules_tree` VALUES (4, 108, 3, 'Panel/System Settings/Languages/Update Language/');
INSERT INTO `modules_tree` VALUES (2, 108, 4, 'Backend/Panel/System Settings/Languages/Update Language/');
INSERT INTO `modules_tree` VALUES (1, 108, 5, 'System/Backend/Panel/System Settings/Languages/Update Language/');
INSERT INTO `modules_tree` VALUES (109, 109, 0, 'Delete Language/');
INSERT INTO `modules_tree` VALUES (103, 109, 1, 'Languages/Delete Language/');
INSERT INTO `modules_tree` VALUES (9, 109, 2, 'System Settings/Languages/Delete Language/');
INSERT INTO `modules_tree` VALUES (4, 109, 3, 'Panel/System Settings/Languages/Delete Language/');
INSERT INTO `modules_tree` VALUES (2, 109, 4, 'Backend/Panel/System Settings/Languages/Delete Language/');
INSERT INTO `modules_tree` VALUES (1, 109, 5, 'System/Backend/Panel/System Settings/Languages/Delete Language/');
INSERT INTO `modules_tree` VALUES (110, 110, 0, 'Content Types/');
INSERT INTO `modules_tree` VALUES (9, 110, 1, 'System Settings/Content Types/');
INSERT INTO `modules_tree` VALUES (4, 110, 2, 'Panel/System Settings/Content Types/');
INSERT INTO `modules_tree` VALUES (2, 110, 3, 'Backend/Panel/System Settings/Content Types/');
INSERT INTO `modules_tree` VALUES (1, 110, 4, 'System/Backend/Panel/System Settings/Content Types/');
INSERT INTO `modules_tree` VALUES (111, 111, 0, 'Show Content Types/');
INSERT INTO `modules_tree` VALUES (110, 111, 1, 'Content Types/Show Content Types/');
INSERT INTO `modules_tree` VALUES (9, 111, 2, 'System Settings/Content Types/Show Content Types/');
INSERT INTO `modules_tree` VALUES (4, 111, 3, 'Panel/System Settings/Content Types/Show Content Types/');
INSERT INTO `modules_tree` VALUES (2, 111, 4, 'Backend/Panel/System Settings/Content Types/Show Content Types/');
INSERT INTO `modules_tree` VALUES (1, 111, 5, 'System/Backend/Panel/System Settings/Content Types/Show Content Types/');
INSERT INTO `modules_tree` VALUES (112, 112, 0, 'Add Content Type/');
INSERT INTO `modules_tree` VALUES (110, 112, 1, 'Content Types/Add Content Type/');
INSERT INTO `modules_tree` VALUES (9, 112, 2, 'System Settings/Content Types/Add Content Type/');
INSERT INTO `modules_tree` VALUES (4, 112, 3, 'Panel/System Settings/Content Types/Add Content Type/');
INSERT INTO `modules_tree` VALUES (2, 112, 4, 'Backend/Panel/System Settings/Content Types/Add Content Type/');
INSERT INTO `modules_tree` VALUES (1, 112, 5, 'System/Backend/Panel/System Settings/Content Types/Add Content Type/');
INSERT INTO `modules_tree` VALUES (113, 113, 0, 'Edit Content Type/');
INSERT INTO `modules_tree` VALUES (110, 113, 1, 'Content Types/Edit Content Type/');
INSERT INTO `modules_tree` VALUES (9, 113, 2, 'System Settings/Content Types/Edit Content Type/');
INSERT INTO `modules_tree` VALUES (4, 113, 3, 'Panel/System Settings/Content Types/Edit Content Type/');
INSERT INTO `modules_tree` VALUES (2, 113, 4, 'Backend/Panel/System Settings/Content Types/Edit Content Type/');
INSERT INTO `modules_tree` VALUES (1, 113, 5, 'System/Backend/Panel/System Settings/Content Types/Edit Content Type/');
INSERT INTO `modules_tree` VALUES (114, 114, 0, 'Update Content Type/');
INSERT INTO `modules_tree` VALUES (110, 114, 1, 'Content Types/Update Content Type/');
INSERT INTO `modules_tree` VALUES (9, 114, 2, 'System Settings/Content Types/Update Content Type/');
INSERT INTO `modules_tree` VALUES (4, 114, 3, 'Panel/System Settings/Content Types/Update Content Type/');
INSERT INTO `modules_tree` VALUES (2, 114, 4, 'Backend/Panel/System Settings/Content Types/Update Content Type/');
INSERT INTO `modules_tree` VALUES (1, 114, 5, 'System/Backend/Panel/System Settings/Content Types/Update Content Type/');
INSERT INTO `modules_tree` VALUES (115, 115, 0, 'Insert Content Type/');
INSERT INTO `modules_tree` VALUES (110, 115, 1, 'Content Types/Insert Content Type/');
INSERT INTO `modules_tree` VALUES (9, 115, 2, 'System Settings/Content Types/Insert Content Type/');
INSERT INTO `modules_tree` VALUES (4, 115, 3, 'Panel/System Settings/Content Types/Insert Content Type/');
INSERT INTO `modules_tree` VALUES (2, 115, 4, 'Backend/Panel/System Settings/Content Types/Insert Content Type/');
INSERT INTO `modules_tree` VALUES (1, 115, 5, 'System/Backend/Panel/System Settings/Content Types/Insert Content Type/');
INSERT INTO `modules_tree` VALUES (116, 116, 0, 'Delete Content Type/');
INSERT INTO `modules_tree` VALUES (110, 116, 1, 'Content Types/Delete Content Type/');
INSERT INTO `modules_tree` VALUES (9, 116, 2, 'System Settings/Content Types/Delete Content Type/');
INSERT INTO `modules_tree` VALUES (4, 116, 3, 'Panel/System Settings/Content Types/Delete Content Type/');
INSERT INTO `modules_tree` VALUES (2, 116, 4, 'Backend/Panel/System Settings/Content Types/Delete Content Type/');
INSERT INTO `modules_tree` VALUES (1, 116, 5, 'System/Backend/Panel/System Settings/Content Types/Delete Content Type/');
INSERT INTO `modules_tree` VALUES (117, 117, 0, 'Extra Fields/');
INSERT INTO `modules_tree` VALUES (4, 117, 1, 'Panel/Extra Fields/');
INSERT INTO `modules_tree` VALUES (2, 117, 2, 'Backend/Panel/Extra Fields/');
INSERT INTO `modules_tree` VALUES (1, 117, 3, 'System/Backend/Panel/Extra Fields/');
INSERT INTO `modules_tree` VALUES (118, 118, 0, 'Show Extra Fields List/');
INSERT INTO `modules_tree` VALUES (119, 119, 0, 'Add Extra Field/');
INSERT INTO `modules_tree` VALUES (120, 120, 0, 'Edit Extra Field/');
INSERT INTO `modules_tree` VALUES (121, 121, 0, 'Delete Extra Field/');
INSERT INTO `modules_tree` VALUES (122, 122, 0, 'Update Extra Fields/');
INSERT INTO `modules_tree` VALUES (123, 123, 0, 'Insert Extra Field/');
INSERT INTO `modules_tree` VALUES (124, 124, 0, 'Extra Fields Groups/');
INSERT INTO `modules_tree` VALUES (125, 125, 0, 'Show Extra Fields Groups list/');
INSERT INTO `modules_tree` VALUES (124, 125, 1, 'Extra Fields Groups/Show Extra Fields Groups list/');
INSERT INTO `modules_tree` VALUES (126, 126, 0, 'Add Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (124, 126, 1, 'Extra Fields Groups/Add Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (127, 127, 0, 'Edit Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (124, 127, 1, 'Extra Fields Groups/Edit Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (128, 128, 0, 'Delete Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (124, 128, 1, 'Extra Fields Groups/Delete Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (129, 129, 0, 'Update Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (124, 129, 1, 'Extra Fields Groups/Update Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (130, 130, 0, 'Insert Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (124, 130, 1, 'Extra Fields Groups/Insert Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (131, 131, 0, 'Show Extra Fields/');
INSERT INTO `modules_tree` VALUES (132, 132, 0, 'Render Extra Field Type/');
INSERT INTO `modules_tree` VALUES (133, 133, 0, 'Extra Fields List/');
INSERT INTO `modules_tree` VALUES (117, 133, 1, 'Extra Fields/Extra Fields List/');
INSERT INTO `modules_tree` VALUES (4, 133, 2, 'Panel/Extra Fields/Extra Fields List/');
INSERT INTO `modules_tree` VALUES (2, 133, 3, 'Backend/Panel/Extra Fields/Extra Fields List/');
INSERT INTO `modules_tree` VALUES (1, 133, 4, 'System/Backend/Panel/Extra Fields/Extra Fields List/');
INSERT INTO `modules_tree` VALUES (133, 132, 1, 'Extra Fields List/Render Extra Field Type/');
INSERT INTO `modules_tree` VALUES (117, 132, 2, 'Extra Fields/Extra Fields List/Render Extra Field Type/');
INSERT INTO `modules_tree` VALUES (4, 132, 3, 'Panel/Extra Fields/Extra Fields List/Render Extra Field Type/');
INSERT INTO `modules_tree` VALUES (2, 132, 4, 'Backend/Panel/Extra Fields/Extra Fields List/Render Extra Field Type/');
INSERT INTO `modules_tree` VALUES (1, 132, 5, 'System/Backend/Panel/Extra Fields/Extra Fields List/Render Extra Field Type/');
INSERT INTO `modules_tree` VALUES (133, 123, 1, 'Extra Fields List/Insert Extra Field/');
INSERT INTO `modules_tree` VALUES (117, 123, 2, 'Extra Fields/Extra Fields List/Insert Extra Field/');
INSERT INTO `modules_tree` VALUES (4, 123, 3, 'Panel/Extra Fields/Extra Fields List/Insert Extra Field/');
INSERT INTO `modules_tree` VALUES (2, 123, 4, 'Backend/Panel/Extra Fields/Extra Fields List/Insert Extra Field/');
INSERT INTO `modules_tree` VALUES (1, 123, 5, 'System/Backend/Panel/Extra Fields/Extra Fields List/Insert Extra Field/');
INSERT INTO `modules_tree` VALUES (133, 122, 1, 'Extra Fields List/Update Extra Fields/');
INSERT INTO `modules_tree` VALUES (117, 122, 2, 'Extra Fields/Extra Fields List/Update Extra Fields/');
INSERT INTO `modules_tree` VALUES (4, 122, 3, 'Panel/Extra Fields/Extra Fields List/Update Extra Fields/');
INSERT INTO `modules_tree` VALUES (2, 122, 4, 'Backend/Panel/Extra Fields/Extra Fields List/Update Extra Fields/');
INSERT INTO `modules_tree` VALUES (1, 122, 5, 'System/Backend/Panel/Extra Fields/Extra Fields List/Update Extra Fields/');
INSERT INTO `modules_tree` VALUES (133, 121, 1, 'Extra Fields List/Delete Extra Field/');
INSERT INTO `modules_tree` VALUES (117, 121, 2, 'Extra Fields/Extra Fields List/Delete Extra Field/');
INSERT INTO `modules_tree` VALUES (4, 121, 3, 'Panel/Extra Fields/Extra Fields List/Delete Extra Field/');
INSERT INTO `modules_tree` VALUES (2, 121, 4, 'Backend/Panel/Extra Fields/Extra Fields List/Delete Extra Field/');
INSERT INTO `modules_tree` VALUES (1, 121, 5, 'System/Backend/Panel/Extra Fields/Extra Fields List/Delete Extra Field/');
INSERT INTO `modules_tree` VALUES (133, 120, 1, 'Extra Fields List/Edit Extra Field/');
INSERT INTO `modules_tree` VALUES (117, 120, 2, 'Extra Fields/Extra Fields List/Edit Extra Field/');
INSERT INTO `modules_tree` VALUES (4, 120, 3, 'Panel/Extra Fields/Extra Fields List/Edit Extra Field/');
INSERT INTO `modules_tree` VALUES (2, 120, 4, 'Backend/Panel/Extra Fields/Extra Fields List/Edit Extra Field/');
INSERT INTO `modules_tree` VALUES (1, 120, 5, 'System/Backend/Panel/Extra Fields/Extra Fields List/Edit Extra Field/');
INSERT INTO `modules_tree` VALUES (133, 119, 1, 'Extra Fields List/Add Extra Field/');
INSERT INTO `modules_tree` VALUES (117, 119, 2, 'Extra Fields/Extra Fields List/Add Extra Field/');
INSERT INTO `modules_tree` VALUES (4, 119, 3, 'Panel/Extra Fields/Extra Fields List/Add Extra Field/');
INSERT INTO `modules_tree` VALUES (2, 119, 4, 'Backend/Panel/Extra Fields/Extra Fields List/Add Extra Field/');
INSERT INTO `modules_tree` VALUES (1, 119, 5, 'System/Backend/Panel/Extra Fields/Extra Fields List/Add Extra Field/');
INSERT INTO `modules_tree` VALUES (133, 118, 1, 'Extra Fields List/Show Extra Fields List/');
INSERT INTO `modules_tree` VALUES (117, 118, 2, 'Extra Fields/Extra Fields List/Show Extra Fields List/');
INSERT INTO `modules_tree` VALUES (4, 118, 3, 'Panel/Extra Fields/Extra Fields List/Show Extra Fields List/');
INSERT INTO `modules_tree` VALUES (2, 118, 4, 'Backend/Panel/Extra Fields/Extra Fields List/Show Extra Fields List/');
INSERT INTO `modules_tree` VALUES (1, 118, 5, 'System/Backend/Panel/Extra Fields/Extra Fields List/Show Extra Fields List/');
INSERT INTO `modules_tree` VALUES (117, 124, 1, 'Extra Fields/Extra Fields Groups/');
INSERT INTO `modules_tree` VALUES (117, 125, 2, 'Extra Fields/Extra Fields Groups/Show Extra Fields Groups list/');
INSERT INTO `modules_tree` VALUES (117, 126, 2, 'Extra Fields/Extra Fields Groups/Add Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (117, 127, 2, 'Extra Fields/Extra Fields Groups/Edit Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (117, 128, 2, 'Extra Fields/Extra Fields Groups/Delete Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (117, 129, 2, 'Extra Fields/Extra Fields Groups/Update Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (117, 130, 2, 'Extra Fields/Extra Fields Groups/Insert Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (4, 124, 2, 'Panel/Extra Fields/Extra Fields Groups/');
INSERT INTO `modules_tree` VALUES (4, 125, 3, 'Panel/Extra Fields/Extra Fields Groups/Show Extra Fields Groups list/');
INSERT INTO `modules_tree` VALUES (4, 126, 3, 'Panel/Extra Fields/Extra Fields Groups/Add Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (4, 127, 3, 'Panel/Extra Fields/Extra Fields Groups/Edit Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (4, 128, 3, 'Panel/Extra Fields/Extra Fields Groups/Delete Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (4, 129, 3, 'Panel/Extra Fields/Extra Fields Groups/Update Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (4, 130, 3, 'Panel/Extra Fields/Extra Fields Groups/Insert Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (2, 124, 3, 'Backend/Panel/Extra Fields/Extra Fields Groups/');
INSERT INTO `modules_tree` VALUES (2, 125, 4, 'Backend/Panel/Extra Fields/Extra Fields Groups/Show Extra Fields Groups list/');
INSERT INTO `modules_tree` VALUES (2, 126, 4, 'Backend/Panel/Extra Fields/Extra Fields Groups/Add Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (2, 127, 4, 'Backend/Panel/Extra Fields/Extra Fields Groups/Edit Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (2, 128, 4, 'Backend/Panel/Extra Fields/Extra Fields Groups/Delete Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (2, 129, 4, 'Backend/Panel/Extra Fields/Extra Fields Groups/Update Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (2, 130, 4, 'Backend/Panel/Extra Fields/Extra Fields Groups/Insert Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (1, 124, 4, 'System/Backend/Panel/Extra Fields/Extra Fields Groups/');
INSERT INTO `modules_tree` VALUES (1, 125, 5, 'System/Backend/Panel/Extra Fields/Extra Fields Groups/Show Extra Fields Groups list/');
INSERT INTO `modules_tree` VALUES (1, 126, 5, 'System/Backend/Panel/Extra Fields/Extra Fields Groups/Add Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (1, 127, 5, 'System/Backend/Panel/Extra Fields/Extra Fields Groups/Edit Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (1, 128, 5, 'System/Backend/Panel/Extra Fields/Extra Fields Groups/Delete Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (1, 129, 5, 'System/Backend/Panel/Extra Fields/Extra Fields Groups/Update Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (1, 130, 5, 'System/Backend/Panel/Extra Fields/Extra Fields Groups/Insert Extra Fields Group/');
INSERT INTO `modules_tree` VALUES (134, 134, 0, 'Profile/');
INSERT INTO `modules_tree` VALUES (4, 134, 1, 'Panel/Profile/');
INSERT INTO `modules_tree` VALUES (2, 134, 2, 'Backend/Panel/Profile/');
INSERT INTO `modules_tree` VALUES (1, 134, 3, 'System/Backend/Panel/Profile/');
INSERT INTO `modules_tree` VALUES (135, 135, 0, 'Show Profile/');
INSERT INTO `modules_tree` VALUES (134, 135, 1, 'Profile/Show Profile/');
INSERT INTO `modules_tree` VALUES (4, 135, 2, 'Panel/Profile/Show Profile/');
INSERT INTO `modules_tree` VALUES (2, 135, 3, 'Backend/Panel/Profile/Show Profile/');
INSERT INTO `modules_tree` VALUES (1, 135, 4, 'System/Backend/Panel/Profile/Show Profile/');
INSERT INTO `modules_tree` VALUES (136, 136, 0, 'Update Profile/');
INSERT INTO `modules_tree` VALUES (134, 136, 1, 'Profile/Update Profile/');
INSERT INTO `modules_tree` VALUES (4, 136, 2, 'Panel/Profile/Update Profile/');
INSERT INTO `modules_tree` VALUES (2, 136, 3, 'Backend/Panel/Profile/Update Profile/');
INSERT INTO `modules_tree` VALUES (1, 136, 4, 'System/Backend/Panel/Profile/Update Profile/');
INSERT INTO `modules_tree` VALUES (137, 137, 0, 'Banners/');
INSERT INTO `modules_tree` VALUES (4, 137, 1, 'Panel/Banners/');
INSERT INTO `modules_tree` VALUES (2, 137, 2, 'Backend/Panel/Banners/');
INSERT INTO `modules_tree` VALUES (1, 137, 3, 'System/Backend/Panel/Banners/');
INSERT INTO `modules_tree` VALUES (138, 138, 0, 'Show Banners/');
INSERT INTO `modules_tree` VALUES (137, 138, 1, 'Banners/Show Banners/');
INSERT INTO `modules_tree` VALUES (4, 138, 2, 'Panel/Banners/Show Banners/');
INSERT INTO `modules_tree` VALUES (2, 138, 3, 'Backend/Panel/Banners/Show Banners/');
INSERT INTO `modules_tree` VALUES (1, 138, 4, 'System/Backend/Panel/Banners/Show Banners/');
INSERT INTO `modules_tree` VALUES (139, 139, 0, 'Banners Positions/');
INSERT INTO `modules_tree` VALUES (137, 139, 1, 'Banners/Banners Positions/');
INSERT INTO `modules_tree` VALUES (4, 139, 2, 'Panel/Banners/Banners Positions/');
INSERT INTO `modules_tree` VALUES (2, 139, 3, 'Backend/Panel/Banners/Banners Positions/');
INSERT INTO `modules_tree` VALUES (1, 139, 4, 'System/Backend/Panel/Banners/Banners Positions/');
INSERT INTO `modules_tree` VALUES (140, 140, 0, 'Show Banners Positions List/');
INSERT INTO `modules_tree` VALUES (139, 140, 1, 'Banners Positions/Show Banners Positions List/');
INSERT INTO `modules_tree` VALUES (137, 140, 2, 'Banners/Banners Positions/Show Banners Positions List/');
INSERT INTO `modules_tree` VALUES (4, 140, 3, 'Panel/Banners/Banners Positions/Show Banners Positions List/');
INSERT INTO `modules_tree` VALUES (2, 140, 4, 'Backend/Panel/Banners/Banners Positions/Show Banners Positions List/');
INSERT INTO `modules_tree` VALUES (1, 140, 5, 'System/Backend/Panel/Banners/Banners Positions/Show Banners Positions List/');
INSERT INTO `modules_tree` VALUES (141, 141, 0, 'Add Banners Postion/');
INSERT INTO `modules_tree` VALUES (139, 141, 1, 'Banners Positions/Add Banners Postion/');
INSERT INTO `modules_tree` VALUES (137, 141, 2, 'Banners/Banners Positions/Add Banners Postion/');
INSERT INTO `modules_tree` VALUES (4, 141, 3, 'Panel/Banners/Banners Positions/Add Banners Postion/');
INSERT INTO `modules_tree` VALUES (2, 141, 4, 'Backend/Panel/Banners/Banners Positions/Add Banners Postion/');
INSERT INTO `modules_tree` VALUES (1, 141, 5, 'System/Backend/Panel/Banners/Banners Positions/Add Banners Postion/');
INSERT INTO `modules_tree` VALUES (142, 142, 0, 'Insert Banners Position/');
INSERT INTO `modules_tree` VALUES (139, 142, 1, 'Banners Positions/Insert Banners Position/');
INSERT INTO `modules_tree` VALUES (137, 142, 2, 'Banners/Banners Positions/Insert Banners Position/');
INSERT INTO `modules_tree` VALUES (4, 142, 3, 'Panel/Banners/Banners Positions/Insert Banners Position/');
INSERT INTO `modules_tree` VALUES (2, 142, 4, 'Backend/Panel/Banners/Banners Positions/Insert Banners Position/');
INSERT INTO `modules_tree` VALUES (1, 142, 5, 'System/Backend/Panel/Banners/Banners Positions/Insert Banners Position/');
INSERT INTO `modules_tree` VALUES (143, 143, 0, 'Update Banners Position/');
INSERT INTO `modules_tree` VALUES (139, 143, 1, 'Banners Positions/Update Banners Position/');
INSERT INTO `modules_tree` VALUES (137, 143, 2, 'Banners/Banners Positions/Update Banners Position/');
INSERT INTO `modules_tree` VALUES (4, 143, 3, 'Panel/Banners/Banners Positions/Update Banners Position/');
INSERT INTO `modules_tree` VALUES (2, 143, 4, 'Backend/Panel/Banners/Banners Positions/Update Banners Position/');
INSERT INTO `modules_tree` VALUES (1, 143, 5, 'System/Backend/Panel/Banners/Banners Positions/Update Banners Position/');
INSERT INTO `modules_tree` VALUES (144, 144, 0, 'Edit Banners Position/');
INSERT INTO `modules_tree` VALUES (139, 144, 1, 'Banners Positions/Edit Banners Position/');
INSERT INTO `modules_tree` VALUES (137, 144, 2, 'Banners/Banners Positions/Edit Banners Position/');
INSERT INTO `modules_tree` VALUES (4, 144, 3, 'Panel/Banners/Banners Positions/Edit Banners Position/');
INSERT INTO `modules_tree` VALUES (2, 144, 4, 'Backend/Panel/Banners/Banners Positions/Edit Banners Position/');
INSERT INTO `modules_tree` VALUES (1, 144, 5, 'System/Backend/Panel/Banners/Banners Positions/Edit Banners Position/');
INSERT INTO `modules_tree` VALUES (145, 145, 0, 'Delete Banners Position/');
INSERT INTO `modules_tree` VALUES (139, 145, 1, 'Banners Positions/Delete Banners Position/');
INSERT INTO `modules_tree` VALUES (137, 145, 2, 'Banners/Banners Positions/Delete Banners Position/');
INSERT INTO `modules_tree` VALUES (4, 145, 3, 'Panel/Banners/Banners Positions/Delete Banners Position/');
INSERT INTO `modules_tree` VALUES (2, 145, 4, 'Backend/Panel/Banners/Banners Positions/Delete Banners Position/');
INSERT INTO `modules_tree` VALUES (1, 145, 5, 'System/Backend/Panel/Banners/Banners Positions/Delete Banners Position/');
INSERT INTO `modules_tree` VALUES (146, 146, 0, 'Clients/');
INSERT INTO `modules_tree` VALUES (147, 147, 0, 'Clients List/');
INSERT INTO `modules_tree` VALUES (146, 147, 1, 'Clients/Clients List/');
INSERT INTO `modules_tree` VALUES (4, 146, 1, 'Panel/Clients/');
INSERT INTO `modules_tree` VALUES (2, 146, 2, 'Backend/Panel/Clients/');
INSERT INTO `modules_tree` VALUES (1, 146, 3, 'System/Backend/Panel/Clients/');
INSERT INTO `modules_tree` VALUES (4, 147, 2, 'Panel/Clients/Clients List/');
INSERT INTO `modules_tree` VALUES (2, 147, 3, 'Backend/Panel/Clients/Clients List/');
INSERT INTO `modules_tree` VALUES (1, 147, 4, 'System/Backend/Panel/Clients/Clients List/');
INSERT INTO `modules_tree` VALUES (148, 148, 0, 'Add Client/');
INSERT INTO `modules_tree` VALUES (146, 148, 1, 'Clients/Add Client/');
INSERT INTO `modules_tree` VALUES (4, 148, 2, 'Panel/Clients/Add Client/');
INSERT INTO `modules_tree` VALUES (2, 148, 3, 'Backend/Panel/Clients/Add Client/');
INSERT INTO `modules_tree` VALUES (1, 148, 4, 'System/Backend/Panel/Clients/Add Client/');
INSERT INTO `modules_tree` VALUES (149, 149, 0, 'Edit Client/');
INSERT INTO `modules_tree` VALUES (146, 149, 1, 'Clients/Edit Client/');
INSERT INTO `modules_tree` VALUES (4, 149, 2, 'Panel/Clients/Edit Client/');
INSERT INTO `modules_tree` VALUES (2, 149, 3, 'Backend/Panel/Clients/Edit Client/');
INSERT INTO `modules_tree` VALUES (1, 149, 4, 'System/Backend/Panel/Clients/Edit Client/');
INSERT INTO `modules_tree` VALUES (150, 150, 0, 'Insert Client/');
INSERT INTO `modules_tree` VALUES (146, 150, 1, 'Clients/Insert Client/');
INSERT INTO `modules_tree` VALUES (4, 150, 2, 'Panel/Clients/Insert Client/');
INSERT INTO `modules_tree` VALUES (2, 150, 3, 'Backend/Panel/Clients/Insert Client/');
INSERT INTO `modules_tree` VALUES (1, 150, 4, 'System/Backend/Panel/Clients/Insert Client/');
INSERT INTO `modules_tree` VALUES (151, 151, 0, 'Update Client/');
INSERT INTO `modules_tree` VALUES (146, 151, 1, 'Clients/Update Client/');
INSERT INTO `modules_tree` VALUES (4, 151, 2, 'Panel/Clients/Update Client/');
INSERT INTO `modules_tree` VALUES (2, 151, 3, 'Backend/Panel/Clients/Update Client/');
INSERT INTO `modules_tree` VALUES (1, 151, 4, 'System/Backend/Panel/Clients/Update Client/');
INSERT INTO `modules_tree` VALUES (152, 152, 0, 'Delete Clients/');
INSERT INTO `modules_tree` VALUES (146, 152, 1, 'Clients/Delete Clients/');
INSERT INTO `modules_tree` VALUES (4, 152, 2, 'Panel/Clients/Delete Clients/');
INSERT INTO `modules_tree` VALUES (2, 152, 3, 'Backend/Panel/Clients/Delete Clients/');
INSERT INTO `modules_tree` VALUES (1, 152, 4, 'System/Backend/Panel/Clients/Delete Clients/');
INSERT INTO `modules_tree` VALUES (153, 153, 0, 'Contacts/');
INSERT INTO `modules_tree` VALUES (4, 153, 1, 'Panel/Contacts/');
INSERT INTO `modules_tree` VALUES (2, 153, 2, 'Backend/Panel/Contacts/');
INSERT INTO `modules_tree` VALUES (1, 153, 3, 'System/Backend/Panel/Contacts/');
INSERT INTO `modules_tree` VALUES (154, 154, 0, 'Show Client Contacts/');
INSERT INTO `modules_tree` VALUES (153, 154, 1, 'Contacts/Show Client Contacts/');
INSERT INTO `modules_tree` VALUES (4, 154, 2, 'Panel/Contacts/Show Client Contacts/');
INSERT INTO `modules_tree` VALUES (2, 154, 3, 'Backend/Panel/Contacts/Show Client Contacts/');
INSERT INTO `modules_tree` VALUES (1, 154, 4, 'System/Backend/Panel/Contacts/Show Client Contacts/');
INSERT INTO `modules_tree` VALUES (155, 155, 0, 'Add new contact/');
INSERT INTO `modules_tree` VALUES (153, 155, 1, 'Contacts/Add new contact/');
INSERT INTO `modules_tree` VALUES (4, 155, 2, 'Panel/Contacts/Add new contact/');
INSERT INTO `modules_tree` VALUES (2, 155, 3, 'Backend/Panel/Contacts/Add new contact/');
INSERT INTO `modules_tree` VALUES (1, 155, 4, 'System/Backend/Panel/Contacts/Add new contact/');
INSERT INTO `modules_tree` VALUES (156, 156, 0, 'Insert Contact/');
INSERT INTO `modules_tree` VALUES (153, 156, 1, 'Contacts/Insert Contact/');
INSERT INTO `modules_tree` VALUES (4, 156, 2, 'Panel/Contacts/Insert Contact/');
INSERT INTO `modules_tree` VALUES (2, 156, 3, 'Backend/Panel/Contacts/Insert Contact/');
INSERT INTO `modules_tree` VALUES (1, 156, 4, 'System/Backend/Panel/Contacts/Insert Contact/');
INSERT INTO `modules_tree` VALUES (157, 157, 0, 'Edit Contact/');
INSERT INTO `modules_tree` VALUES (153, 157, 1, 'Contacts/Edit Contact/');
INSERT INTO `modules_tree` VALUES (4, 157, 2, 'Panel/Contacts/Edit Contact/');
INSERT INTO `modules_tree` VALUES (2, 157, 3, 'Backend/Panel/Contacts/Edit Contact/');
INSERT INTO `modules_tree` VALUES (1, 157, 4, 'System/Backend/Panel/Contacts/Edit Contact/');
INSERT INTO `modules_tree` VALUES (158, 158, 0, 'Update Contact/');
INSERT INTO `modules_tree` VALUES (153, 158, 1, 'Contacts/Update Contact/');
INSERT INTO `modules_tree` VALUES (4, 158, 2, 'Panel/Contacts/Update Contact/');
INSERT INTO `modules_tree` VALUES (2, 158, 3, 'Backend/Panel/Contacts/Update Contact/');
INSERT INTO `modules_tree` VALUES (1, 158, 4, 'System/Backend/Panel/Contacts/Update Contact/');
INSERT INTO `modules_tree` VALUES (159, 159, 0, 'Show Contacts List/');
INSERT INTO `modules_tree` VALUES (153, 159, 1, 'Contacts/Show Contacts List/');
INSERT INTO `modules_tree` VALUES (4, 159, 2, 'Panel/Contacts/Show Contacts List/');
INSERT INTO `modules_tree` VALUES (2, 159, 3, 'Backend/Panel/Contacts/Show Contacts List/');
INSERT INTO `modules_tree` VALUES (1, 159, 4, 'System/Backend/Panel/Contacts/Show Contacts List/');
INSERT INTO `modules_tree` VALUES (117, 131, 1, 'Extra Fields/Show Extra Fields/');
INSERT INTO `modules_tree` VALUES (4, 131, 2, 'Panel/Extra Fields/Show Extra Fields/');
INSERT INTO `modules_tree` VALUES (2, 131, 3, 'Backend/Panel/Extra Fields/Show Extra Fields/');
INSERT INTO `modules_tree` VALUES (1, 131, 4, 'System/Backend/Panel/Extra Fields/Show Extra Fields/');
INSERT INTO `modules_tree` VALUES (160, 160, 0, 'Show Frontend menu/');
INSERT INTO `modules_tree` VALUES (97, 160, 1, 'Menus/Show Frontend menu/');
INSERT INTO `modules_tree` VALUES (4, 160, 2, 'Panel/Menus/Show Frontend menu/');
INSERT INTO `modules_tree` VALUES (2, 160, 3, 'Backend/Panel/Menus/Show Frontend menu/');
INSERT INTO `modules_tree` VALUES (1, 160, 4, 'System/Backend/Panel/Menus/Show Frontend menu/');
INSERT INTO `modules_tree` VALUES (161, 161, 0, 'Show Backend Menu/');
INSERT INTO `modules_tree` VALUES (97, 161, 1, 'Menus/Show Backend Menu/');
INSERT INTO `modules_tree` VALUES (4, 161, 2, 'Panel/Menus/Show Backend Menu/');
INSERT INTO `modules_tree` VALUES (2, 161, 3, 'Backend/Panel/Menus/Show Backend Menu/');
INSERT INTO `modules_tree` VALUES (1, 161, 4, 'System/Backend/Panel/Menus/Show Backend Menu/');
INSERT INTO `modules_tree` VALUES (162, 162, 0, 'Banners List/');
INSERT INTO `modules_tree` VALUES (137, 162, 1, 'Banners/Banners List/');
INSERT INTO `modules_tree` VALUES (4, 162, 2, 'Panel/Banners/Banners List/');
INSERT INTO `modules_tree` VALUES (2, 162, 3, 'Backend/Panel/Banners/Banners List/');
INSERT INTO `modules_tree` VALUES (1, 162, 4, 'System/Backend/Panel/Banners/Banners List/');
INSERT INTO `modules_tree` VALUES (163, 163, 0, 'Show Banners List/');
INSERT INTO `modules_tree` VALUES (162, 163, 1, 'Banners List/Show Banners List/');
INSERT INTO `modules_tree` VALUES (137, 163, 2, 'Banners/Banners List/Show Banners List/');
INSERT INTO `modules_tree` VALUES (4, 163, 3, 'Panel/Banners/Banners List/Show Banners List/');
INSERT INTO `modules_tree` VALUES (2, 163, 4, 'Backend/Panel/Banners/Banners List/Show Banners List/');
INSERT INTO `modules_tree` VALUES (1, 163, 5, 'System/Backend/Panel/Banners/Banners List/Show Banners List/');
INSERT INTO `modules_tree` VALUES (164, 164, 0, 'Add Banner/');
INSERT INTO `modules_tree` VALUES (162, 164, 1, 'Banners List/Add Banner/');
INSERT INTO `modules_tree` VALUES (137, 164, 2, 'Banners/Banners List/Add Banner/');
INSERT INTO `modules_tree` VALUES (4, 164, 3, 'Panel/Banners/Banners List/Add Banner/');
INSERT INTO `modules_tree` VALUES (2, 164, 4, 'Backend/Panel/Banners/Banners List/Add Banner/');
INSERT INTO `modules_tree` VALUES (1, 164, 5, 'System/Backend/Panel/Banners/Banners List/Add Banner/');
INSERT INTO `modules_tree` VALUES (165, 165, 0, 'Edit Banner/');
INSERT INTO `modules_tree` VALUES (162, 165, 1, 'Banners List/Edit Banner/');
INSERT INTO `modules_tree` VALUES (137, 165, 2, 'Banners/Banners List/Edit Banner/');
INSERT INTO `modules_tree` VALUES (4, 165, 3, 'Panel/Banners/Banners List/Edit Banner/');
INSERT INTO `modules_tree` VALUES (2, 165, 4, 'Backend/Panel/Banners/Banners List/Edit Banner/');
INSERT INTO `modules_tree` VALUES (1, 165, 5, 'System/Backend/Panel/Banners/Banners List/Edit Banner/');
INSERT INTO `modules_tree` VALUES (166, 166, 0, 'Update Banner/');
INSERT INTO `modules_tree` VALUES (162, 166, 1, 'Banners List/Update Banner/');
INSERT INTO `modules_tree` VALUES (137, 166, 2, 'Banners/Banners List/Update Banner/');
INSERT INTO `modules_tree` VALUES (4, 166, 3, 'Panel/Banners/Banners List/Update Banner/');
INSERT INTO `modules_tree` VALUES (2, 166, 4, 'Backend/Panel/Banners/Banners List/Update Banner/');
INSERT INTO `modules_tree` VALUES (1, 166, 5, 'System/Backend/Panel/Banners/Banners List/Update Banner/');
INSERT INTO `modules_tree` VALUES (167, 167, 0, 'Insert Banner/');
INSERT INTO `modules_tree` VALUES (162, 167, 1, 'Banners List/Insert Banner/');
INSERT INTO `modules_tree` VALUES (137, 167, 2, 'Banners/Banners List/Insert Banner/');
INSERT INTO `modules_tree` VALUES (4, 167, 3, 'Panel/Banners/Banners List/Insert Banner/');
INSERT INTO `modules_tree` VALUES (2, 167, 4, 'Backend/Panel/Banners/Banners List/Insert Banner/');
INSERT INTO `modules_tree` VALUES (1, 167, 5, 'System/Backend/Panel/Banners/Banners List/Insert Banner/');
INSERT INTO `modules_tree` VALUES (168, 168, 0, 'Delete Banner/');
INSERT INTO `modules_tree` VALUES (162, 168, 1, 'Banners List/Delete Banner/');
INSERT INTO `modules_tree` VALUES (137, 168, 2, 'Banners/Banners List/Delete Banner/');
INSERT INTO `modules_tree` VALUES (4, 168, 3, 'Panel/Banners/Banners List/Delete Banner/');
INSERT INTO `modules_tree` VALUES (2, 168, 4, 'Backend/Panel/Banners/Banners List/Delete Banner/');
INSERT INTO `modules_tree` VALUES (1, 168, 5, 'System/Backend/Panel/Banners/Banners List/Delete Banner/');
INSERT INTO `modules_tree` VALUES (169, 169, 0, 'Get Banner type/');
INSERT INTO `modules_tree` VALUES (162, 169, 1, 'Banners List/Get Banner type/');
INSERT INTO `modules_tree` VALUES (137, 169, 2, 'Banners/Banners List/Get Banner type/');
INSERT INTO `modules_tree` VALUES (4, 169, 3, 'Panel/Banners/Banners List/Get Banner type/');
INSERT INTO `modules_tree` VALUES (2, 169, 4, 'Backend/Panel/Banners/Banners List/Get Banner type/');
INSERT INTO `modules_tree` VALUES (1, 169, 5, 'System/Backend/Panel/Banners/Banners List/Get Banner type/');
INSERT INTO `modules_tree` VALUES (170, 170, 0, 'Cache/');
INSERT INTO `modules_tree` VALUES (9, 170, 1, 'System Settings/Cache/');
INSERT INTO `modules_tree` VALUES (4, 170, 2, 'Panel/System Settings/Cache/');
INSERT INTO `modules_tree` VALUES (2, 170, 3, 'Backend/Panel/System Settings/Cache/');
INSERT INTO `modules_tree` VALUES (1, 170, 4, 'System/Backend/Panel/System Settings/Cache/');
INSERT INTO `modules_tree` VALUES (171, 171, 0, 'Show Cache List/');
INSERT INTO `modules_tree` VALUES (170, 171, 1, 'Cache/Show Cache List/');
INSERT INTO `modules_tree` VALUES (9, 171, 2, 'System Settings/Cache/Show Cache List/');
INSERT INTO `modules_tree` VALUES (4, 171, 3, 'Panel/System Settings/Cache/Show Cache List/');
INSERT INTO `modules_tree` VALUES (2, 171, 4, 'Backend/Panel/System Settings/Cache/Show Cache List/');
INSERT INTO `modules_tree` VALUES (1, 171, 5, 'System/Backend/Panel/System Settings/Cache/Show Cache List/');
INSERT INTO `modules_tree` VALUES (172, 172, 0, 'Delete Cache/');
INSERT INTO `modules_tree` VALUES (170, 172, 1, 'Cache/Delete Cache/');
INSERT INTO `modules_tree` VALUES (9, 172, 2, 'System Settings/Cache/Delete Cache/');
INSERT INTO `modules_tree` VALUES (4, 172, 3, 'Panel/System Settings/Cache/Delete Cache/');
INSERT INTO `modules_tree` VALUES (2, 172, 4, 'Backend/Panel/System Settings/Cache/Delete Cache/');
INSERT INTO `modules_tree` VALUES (1, 172, 5, 'System/Backend/Panel/System Settings/Cache/Delete Cache/');
INSERT INTO `modules_tree` VALUES (173, 173, 0, 'Comments/');
INSERT INTO `modules_tree` VALUES (4, 173, 1, 'Panel/Comments/');
INSERT INTO `modules_tree` VALUES (2, 173, 2, 'Backend/Panel/Comments/');
INSERT INTO `modules_tree` VALUES (1, 173, 3, 'System/Backend/Panel/Comments/');
INSERT INTO `modules_tree` VALUES (174, 174, 0, 'Show Comments/');
INSERT INTO `modules_tree` VALUES (173, 174, 1, 'Comments/Show Comments/');
INSERT INTO `modules_tree` VALUES (4, 174, 2, 'Panel/Comments/Show Comments/');
INSERT INTO `modules_tree` VALUES (2, 174, 3, 'Backend/Panel/Comments/Show Comments/');
INSERT INTO `modules_tree` VALUES (1, 174, 4, 'System/Backend/Panel/Comments/Show Comments/');
INSERT INTO `modules_tree` VALUES (175, 175, 0, 'Edit Comment/');
INSERT INTO `modules_tree` VALUES (173, 175, 1, 'Comments/Edit Comment/');
INSERT INTO `modules_tree` VALUES (4, 175, 2, 'Panel/Comments/Edit Comment/');
INSERT INTO `modules_tree` VALUES (2, 175, 3, 'Backend/Panel/Comments/Edit Comment/');
INSERT INTO `modules_tree` VALUES (1, 175, 4, 'System/Backend/Panel/Comments/Edit Comment/');
INSERT INTO `modules_tree` VALUES (176, 176, 0, 'Delete Comment/');
INSERT INTO `modules_tree` VALUES (173, 176, 1, 'Comments/Delete Comment/');
INSERT INTO `modules_tree` VALUES (4, 176, 2, 'Panel/Comments/Delete Comment/');
INSERT INTO `modules_tree` VALUES (2, 176, 3, 'Backend/Panel/Comments/Delete Comment/');
INSERT INTO `modules_tree` VALUES (1, 176, 4, 'System/Backend/Panel/Comments/Delete Comment/');
INSERT INTO `modules_tree` VALUES (177, 177, 0, 'Frontend Comments/');
INSERT INTO `modules_tree` VALUES (3, 177, 1, 'Frontend/Frontend Comments/');
INSERT INTO `modules_tree` VALUES (1, 177, 2, 'System/Frontend/Frontend Comments/');
INSERT INTO `modules_tree` VALUES (178, 178, 0, 'Add Comment/');
INSERT INTO `modules_tree` VALUES (177, 178, 1, 'Frontend Comments/Add Comment/');
INSERT INTO `modules_tree` VALUES (3, 178, 2, 'Frontend/Frontend Comments/Add Comment/');
INSERT INTO `modules_tree` VALUES (1, 178, 3, 'System/Frontend/Frontend Comments/Add Comment/');
INSERT INTO `modules_tree` VALUES (179, 179, 0, 'Login/');
INSERT INTO `modules_tree` VALUES (180, 180, 0, 'Logout/');
INSERT INTO `modules_tree` VALUES (181, 181, 0, 'Site/');
INSERT INTO `modules_tree` VALUES (3, 181, 1, 'Frontend/Site/');
INSERT INTO `modules_tree` VALUES (1, 181, 2, 'System/Frontend/Site/');
INSERT INTO `modules_tree` VALUES (181, 179, 1, 'Site/Login/');
INSERT INTO `modules_tree` VALUES (3, 179, 2, 'Frontend/Site/Login/');
INSERT INTO `modules_tree` VALUES (1, 179, 3, 'System/Frontend/Site/Login/');
INSERT INTO `modules_tree` VALUES (181, 180, 1, 'Site/Logout/');
INSERT INTO `modules_tree` VALUES (3, 180, 2, 'Frontend/Site/Logout/');
INSERT INTO `modules_tree` VALUES (1, 180, 3, 'System/Frontend/Site/Logout/');
INSERT INTO `modules_tree` VALUES (182, 182, 0, 'Accept Cookies/');
INSERT INTO `modules_tree` VALUES (181, 182, 1, 'Site/Accept Cookies/');
INSERT INTO `modules_tree` VALUES (3, 182, 2, 'Frontend/Site/Accept Cookies/');
INSERT INTO `modules_tree` VALUES (1, 182, 3, 'System/Frontend/Site/Accept Cookies/');
INSERT INTO `modules_tree` VALUES (183, 183, 0, 'Decline Cookies/');
INSERT INTO `modules_tree` VALUES (181, 183, 1, 'Site/Decline Cookies/');
INSERT INTO `modules_tree` VALUES (3, 183, 2, 'Frontend/Site/Decline Cookies/');
INSERT INTO `modules_tree` VALUES (1, 183, 3, 'System/Frontend/Site/Decline Cookies/');
INSERT INTO `modules_tree` VALUES (184, 184, 0, 'Update Comment/');
INSERT INTO `modules_tree` VALUES (173, 184, 1, 'Comments/Update Comment/');
INSERT INTO `modules_tree` VALUES (4, 184, 2, 'Panel/Comments/Update Comment/');
INSERT INTO `modules_tree` VALUES (2, 184, 3, 'Backend/Panel/Comments/Update Comment/');
INSERT INTO `modules_tree` VALUES (1, 184, 4, 'System/Backend/Panel/Comments/Update Comment/');
INSERT INTO `modules_tree` VALUES (185, 185, 0, 'Migrator/');
INSERT INTO `modules_tree` VALUES (4, 185, 1, 'Panel/Migrator/');
INSERT INTO `modules_tree` VALUES (2, 185, 2, 'Backend/Panel/Migrator/');
INSERT INTO `modules_tree` VALUES (1, 185, 3, 'System/Backend/Panel/Migrator/');
INSERT INTO `modules_tree` VALUES (186, 186, 0, 'Migration Options/');
INSERT INTO `modules_tree` VALUES (185, 186, 1, 'Migrator/Migration Options/');
INSERT INTO `modules_tree` VALUES (4, 186, 2, 'Panel/Migrator/Migration Options/');
INSERT INTO `modules_tree` VALUES (2, 186, 3, 'Backend/Panel/Migrator/Migration Options/');
INSERT INTO `modules_tree` VALUES (1, 186, 4, 'System/Backend/Panel/Migrator/Migration Options/');
INSERT INTO `modules_tree` VALUES (187, 187, 0, 'Migrate Actions/');
INSERT INTO `modules_tree` VALUES (185, 187, 1, 'Migrator/Migrate Actions/');
INSERT INTO `modules_tree` VALUES (4, 187, 2, 'Panel/Migrator/Migrate Actions/');
INSERT INTO `modules_tree` VALUES (2, 187, 3, 'Backend/Panel/Migrator/Migrate Actions/');
INSERT INTO `modules_tree` VALUES (1, 187, 4, 'System/Backend/Panel/Migrator/Migrate Actions/');
INSERT INTO `modules_tree` VALUES (188, 188, 0, 'Register/');
INSERT INTO `modules_tree` VALUES (181, 188, 1, 'Site/Register/');
INSERT INTO `modules_tree` VALUES (3, 188, 2, 'Frontend/Site/Register/');
INSERT INTO `modules_tree` VALUES (1, 188, 3, 'System/Frontend/Site/Register/');
INSERT INTO `modules_tree` VALUES (189, 189, 0, 'Login With Facebook/');
INSERT INTO `modules_tree` VALUES (181, 189, 1, 'Site/Login With Facebook/');
INSERT INTO `modules_tree` VALUES (3, 189, 2, 'Frontend/Site/Login With Facebook/');
INSERT INTO `modules_tree` VALUES (1, 189, 3, 'System/Frontend/Site/Login With Facebook/');
INSERT INTO `modules_tree` VALUES (190, 190, 0, 'Activate User/');
INSERT INTO `modules_tree` VALUES (181, 190, 1, 'Site/Activate User/');
INSERT INTO `modules_tree` VALUES (3, 190, 2, 'Frontend/Site/Activate User/');
INSERT INTO `modules_tree` VALUES (1, 190, 3, 'System/Frontend/Site/Activate User/');
INSERT INTO `modules_tree` VALUES (191, 191, 0, 'Forgotten User actions/');
INSERT INTO `modules_tree` VALUES (181, 191, 1, 'Site/Forgotten User actions/');
INSERT INTO `modules_tree` VALUES (3, 191, 2, 'Frontend/Site/Forgotten User actions/');
INSERT INTO `modules_tree` VALUES (1, 191, 3, 'System/Frontend/Site/Forgotten User actions/');
INSERT INTO `modules_tree` VALUES (192, 192, 0, 'Sitemap/');
INSERT INTO `modules_tree` VALUES (181, 192, 1, 'Site/Sitemap/');
INSERT INTO `modules_tree` VALUES (3, 192, 2, 'Frontend/Site/Sitemap/');
INSERT INTO `modules_tree` VALUES (1, 192, 3, 'System/Frontend/Site/Sitemap/');
INSERT INTO `modules_tree` VALUES (193, 193, 0, 'Google News Sitemap/');
INSERT INTO `modules_tree` VALUES (181, 193, 1, 'Site/Google News Sitemap/');
INSERT INTO `modules_tree` VALUES (3, 193, 2, 'Frontend/Site/Google News Sitemap/');
INSERT INTO `modules_tree` VALUES (1, 193, 3, 'System/Frontend/Site/Google News Sitemap/');
INSERT INTO `modules_tree` VALUES (194, 194, 0, 'Tiles/');
INSERT INTO `modules_tree` VALUES (4, 194, 1, 'Panel/Tiles/');
INSERT INTO `modules_tree` VALUES (2, 194, 2, 'Backend/Panel/Tiles/');
INSERT INTO `modules_tree` VALUES (1, 194, 3, 'System/Backend/Panel/Tiles/');
INSERT INTO `modules_tree` VALUES (195, 195, 0, 'Tiles List/');
INSERT INTO `modules_tree` VALUES (194, 195, 1, 'Tiles/Tiles List/');
INSERT INTO `modules_tree` VALUES (4, 195, 2, 'Panel/Tiles/Tiles List/');
INSERT INTO `modules_tree` VALUES (2, 195, 3, 'Backend/Panel/Tiles/Tiles List/');
INSERT INTO `modules_tree` VALUES (1, 195, 4, 'System/Backend/Panel/Tiles/Tiles List/');
INSERT INTO `modules_tree` VALUES (196, 196, 0, 'Add Tiles/');
INSERT INTO `modules_tree` VALUES (194, 196, 1, 'Tiles/Add Tiles/');
INSERT INTO `modules_tree` VALUES (4, 196, 2, 'Panel/Tiles/Add Tiles/');
INSERT INTO `modules_tree` VALUES (2, 196, 3, 'Backend/Panel/Tiles/Add Tiles/');
INSERT INTO `modules_tree` VALUES (1, 196, 4, 'System/Backend/Panel/Tiles/Add Tiles/');
INSERT INTO `modules_tree` VALUES (197, 197, 0, 'Remove Tiles/');
INSERT INTO `modules_tree` VALUES (194, 197, 1, 'Tiles/Remove Tiles/');
INSERT INTO `modules_tree` VALUES (4, 197, 2, 'Panel/Tiles/Remove Tiles/');
INSERT INTO `modules_tree` VALUES (2, 197, 3, 'Backend/Panel/Tiles/Remove Tiles/');
INSERT INTO `modules_tree` VALUES (1, 197, 4, 'System/Backend/Panel/Tiles/Remove Tiles/');
INSERT INTO `modules_tree` VALUES (198, 198, 0, 'Update Tiles Order/');
INSERT INTO `modules_tree` VALUES (194, 198, 1, 'Tiles/Update Tiles Order/');
INSERT INTO `modules_tree` VALUES (4, 198, 2, 'Panel/Tiles/Update Tiles Order/');
INSERT INTO `modules_tree` VALUES (2, 198, 3, 'Backend/Panel/Tiles/Update Tiles Order/');
INSERT INTO `modules_tree` VALUES (1, 198, 4, 'System/Backend/Panel/Tiles/Update Tiles Order/');
INSERT INTO `modules_tree` VALUES (199, 199, 0, 'Get News/');
INSERT INTO `modules_tree` VALUES (181, 199, 1, 'Site/Get News/');
INSERT INTO `modules_tree` VALUES (3, 199, 2, 'Frontend/Site/Get News/');
INSERT INTO `modules_tree` VALUES (1, 199, 3, 'System/Frontend/Site/Get News/');
INSERT INTO `modules_tree` VALUES (200, 200, 0, 'Manage Privacy Policy/');
INSERT INTO `modules_tree` VALUES (181, 200, 1, 'Site/Manage Privacy Policy/');
INSERT INTO `modules_tree` VALUES (3, 200, 2, 'Frontend/Site/Manage Privacy Policy/');
INSERT INTO `modules_tree` VALUES (1, 200, 3, 'System/Frontend/Site/Manage Privacy Policy/');
INSERT INTO `modules_tree` VALUES (201, 201, 0, 'Frontend Users/');
INSERT INTO `modules_tree` VALUES (3, 201, 1, 'Frontend/Frontend Users/');
INSERT INTO `modules_tree` VALUES (1, 201, 2, 'System/Frontend/Frontend Users/');
INSERT INTO `modules_tree` VALUES (203, 203, 0, 'Edit All Site Users/');
INSERT INTO `modules_tree` VALUES (201, 203, 1, 'Frontend Users/Edit All Site Users/');
INSERT INTO `modules_tree` VALUES (3, 203, 2, 'Frontend/Frontend Users/Edit All Site Users/');
INSERT INTO `modules_tree` VALUES (1, 203, 3, 'System/Frontend/Frontend Users/Edit All Site Users/');
INSERT INTO `modules_tree` VALUES (204, 204, 0, 'Update Site User/');
INSERT INTO `modules_tree` VALUES (201, 204, 1, 'Frontend Users/Update Site User/');
INSERT INTO `modules_tree` VALUES (3, 204, 2, 'Frontend/Frontend Users/Update Site User/');
INSERT INTO `modules_tree` VALUES (1, 204, 3, 'System/Frontend/Frontend Users/Update Site User/');
INSERT INTO `modules_tree` VALUES (205, 205, 0, 'View User/');
INSERT INTO `modules_tree` VALUES (201, 205, 1, 'Frontend Users/View User/');
INSERT INTO `modules_tree` VALUES (3, 205, 2, 'Frontend/Frontend Users/View User/');
INSERT INTO `modules_tree` VALUES (1, 205, 3, 'System/Frontend/Frontend Users/View User/');
INSERT INTO `modules_tree` VALUES (206, 206, 0, 'Get Comments/');
INSERT INTO `modules_tree` VALUES (177, 206, 1, 'Frontend Comments/Get Comments/');
INSERT INTO `modules_tree` VALUES (3, 206, 2, 'Frontend/Frontend Comments/Get Comments/');
INSERT INTO `modules_tree` VALUES (1, 206, 3, 'System/Frontend/Frontend Comments/Get Comments/');
INSERT INTO `modules_tree` VALUES (207, 207, 0, 'Like Comment/');
INSERT INTO `modules_tree` VALUES (177, 207, 1, 'Frontend Comments/Like Comment/');
INSERT INTO `modules_tree` VALUES (3, 207, 2, 'Frontend/Frontend Comments/Like Comment/');
INSERT INTO `modules_tree` VALUES (1, 207, 3, 'System/Frontend/Frontend Comments/Like Comment/');
INSERT INTO `modules_tree` VALUES (208, 208, 0, 'Show Likes/');
INSERT INTO `modules_tree` VALUES (177, 208, 1, 'Frontend Comments/Show Likes/');
INSERT INTO `modules_tree` VALUES (3, 208, 2, 'Frontend/Frontend Comments/Show Likes/');
INSERT INTO `modules_tree` VALUES (1, 208, 3, 'System/Frontend/Frontend Comments/Show Likes/');
INSERT INTO `modules_tree` VALUES (209, 209, 0, 'Edit Frontend Comments/');
INSERT INTO `modules_tree` VALUES (177, 209, 1, 'Frontend Comments/Edit Frontend Comments/');
INSERT INTO `modules_tree` VALUES (3, 209, 2, 'Frontend/Frontend Comments/Edit Frontend Comments/');
INSERT INTO `modules_tree` VALUES (1, 209, 3, 'System/Frontend/Frontend Comments/Edit Frontend Comments/');
INSERT INTO `modules_tree` VALUES (210, 210, 0, 'Delete Frontend Comments/');
INSERT INTO `modules_tree` VALUES (177, 210, 1, 'Frontend Comments/Delete Frontend Comments/');
INSERT INTO `modules_tree` VALUES (3, 210, 2, 'Frontend/Frontend Comments/Delete Frontend Comments/');
INSERT INTO `modules_tree` VALUES (1, 210, 3, 'System/Frontend/Frontend Comments/Delete Frontend Comments/');
INSERT INTO `modules_tree` VALUES (211, 211, 0, 'Show Comment Form/');
INSERT INTO `modules_tree` VALUES (177, 211, 1, 'Frontend Comments/Show Comment Form/');
INSERT INTO `modules_tree` VALUES (3, 211, 2, 'Frontend/Frontend Comments/Show Comment Form/');
INSERT INTO `modules_tree` VALUES (1, 211, 3, 'System/Frontend/Frontend Comments/Show Comment Form/');
INSERT INTO `modules_tree` VALUES (212, 212, 0, 'Update Frontend Comments/');
INSERT INTO `modules_tree` VALUES (177, 212, 1, 'Frontend Comments/Update Frontend Comments/');
INSERT INTO `modules_tree` VALUES (3, 212, 2, 'Frontend/Frontend Comments/Update Frontend Comments/');
INSERT INTO `modules_tree` VALUES (1, 212, 3, 'System/Frontend/Frontend Comments/Update Frontend Comments/');
INSERT INTO `modules_tree` VALUES (213, 213, 0, 'Report Comments/');
INSERT INTO `modules_tree` VALUES (177, 213, 1, 'Frontend Comments/Report Comments/');
INSERT INTO `modules_tree` VALUES (3, 213, 2, 'Frontend/Frontend Comments/Report Comments/');
INSERT INTO `modules_tree` VALUES (1, 213, 3, 'System/Frontend/Frontend Comments/Report Comments/');
INSERT INTO `modules_tree` VALUES (214, 214, 0, 'Download Info/');
INSERT INTO `modules_tree` VALUES (201, 214, 1, 'Frontend Users/Download Info/');
INSERT INTO `modules_tree` VALUES (3, 214, 2, 'Frontend/Frontend Users/Download Info/');
INSERT INTO `modules_tree` VALUES (1, 214, 3, 'System/Frontend/Frontend Users/Download Info/');
INSERT INTO `modules_tree` VALUES (215, 215, 0, 'Search/');
INSERT INTO `modules_tree` VALUES (74, 215, 1, 'Frontend Pages/Search/');
INSERT INTO `modules_tree` VALUES (3, 215, 2, 'Frontend/Frontend Pages/Search/');
INSERT INTO `modules_tree` VALUES (1, 215, 3, 'System/Frontend/Frontend Pages/Search/');
INSERT INTO `modules_tree` VALUES (216, 216, 0, 'Contact Us/');
INSERT INTO `modules_tree` VALUES (74, 216, 1, 'Frontend Pages/Contact Us/');
INSERT INTO `modules_tree` VALUES (3, 216, 2, 'Frontend/Frontend Pages/Contact Us/');
INSERT INTO `modules_tree` VALUES (1, 216, 3, 'System/Frontend/Frontend Pages/Contact Us/');
INSERT INTO `modules_tree` VALUES (217, 217, 0, 'Edit Own Frontend Comments/');
INSERT INTO `modules_tree` VALUES (177, 217, 1, 'Frontend Comments/Edit Own Frontend Comments/');
INSERT INTO `modules_tree` VALUES (3, 217, 2, 'Frontend/Frontend Comments/Edit Own Frontend Comments/');
INSERT INTO `modules_tree` VALUES (1, 217, 3, 'System/Frontend/Frontend Comments/Edit Own Frontend Comments/');
INSERT INTO `modules_tree` VALUES (218, 218, 0, 'Publish Articles/');
INSERT INTO `modules_tree` VALUES (51, 218, 1, 'Articles/Publish Articles/');
INSERT INTO `modules_tree` VALUES (4, 218, 2, 'Panel/Articles/Publish Articles/');
INSERT INTO `modules_tree` VALUES (2, 218, 3, 'Backend/Panel/Articles/Publish Articles/');
INSERT INTO `modules_tree` VALUES (1, 218, 4, 'System/Backend/Panel/Articles/Publish Articles/');
INSERT INTO `modules_tree` VALUES (219, 219, 0, 'Update Banners Order/');
INSERT INTO `modules_tree` VALUES (162, 219, 1, 'Banners List/Update Banners Order/');
INSERT INTO `modules_tree` VALUES (137, 219, 2, 'Banners/Banners List/Update Banners Order/');
INSERT INTO `modules_tree` VALUES (4, 219, 3, 'Panel/Banners/Banners List/Update Banners Order/');
INSERT INTO `modules_tree` VALUES (2, 219, 4, 'Backend/Panel/Banners/Banners List/Update Banners Order/');
INSERT INTO `modules_tree` VALUES (1, 219, 5, 'System/Backend/Panel/Banners/Banners List/Update Banners Order/');
INSERT INTO `modules_tree` VALUES (220, 220, 0, 'Optimize Image/');
INSERT INTO `modules_tree` VALUES (4, 220, 1, 'Panel/Optimize Image/');
INSERT INTO `modules_tree` VALUES (2, 220, 2, 'Backend/Panel/Optimize Image/');
INSERT INTO `modules_tree` VALUES (1, 220, 3, 'System/Backend/Panel/Optimize Image/');
INSERT INTO `modules_tree` VALUES (221, 221, 0, 'Update Extra Fields Order/');
INSERT INTO `modules_tree` VALUES (133, 221, 1, 'Extra Fields List/Update Extra Fields Order/');
INSERT INTO `modules_tree` VALUES (117, 221, 2, 'Extra Fields/Extra Fields List/Update Extra Fields Order/');
INSERT INTO `modules_tree` VALUES (4, 221, 3, 'Panel/Extra Fields/Extra Fields List/Update Extra Fields Order/');
INSERT INTO `modules_tree` VALUES (2, 221, 4, 'Backend/Panel/Extra Fields/Extra Fields List/Update Extra Fields Order/');
INSERT INTO `modules_tree` VALUES (1, 221, 5, 'System/Backend/Panel/Extra Fields/Extra Fields List/Update Extra Fields Order/');
INSERT INTO `modules_tree` VALUES (222, 222, 0, 'Merge Tags/');
INSERT INTO `modules_tree` VALUES (89, 222, 1, 'Tags/Merge Tags/');
INSERT INTO `modules_tree` VALUES (4, 222, 2, 'Panel/Tags/Merge Tags/');
INSERT INTO `modules_tree` VALUES (2, 222, 3, 'Backend/Panel/Tags/Merge Tags/');
INSERT INTO `modules_tree` VALUES (1, 222, 4, 'System/Backend/Panel/Tags/Merge Tags/');
INSERT INTO `modules_tree` VALUES (223, 223, 0, 'App Logs/');
INSERT INTO `modules_tree` VALUES (224, 224, 0, 'Show App Logs List/');
INSERT INTO `modules_tree` VALUES (223, 224, 1, 'App Logs/Show App Logs List/');
INSERT INTO `modules_tree` VALUES (225, 225, 0, 'Show App Log Info/');
INSERT INTO `modules_tree` VALUES (223, 225, 1, 'App Logs/Show App Log Info/');
INSERT INTO `modules_tree` VALUES (226, 226, 0, 'Delete App Log/');
INSERT INTO `modules_tree` VALUES (223, 226, 1, 'App Logs/Delete App Log/');
INSERT INTO `modules_tree` VALUES (9, 223, 1, 'System Settings/App Logs/');
INSERT INTO `modules_tree` VALUES (9, 224, 2, 'System Settings/App Logs/Show App Logs List/');
INSERT INTO `modules_tree` VALUES (9, 225, 2, 'System Settings/App Logs/Show App Log Info/');
INSERT INTO `modules_tree` VALUES (9, 226, 2, 'System Settings/App Logs/Delete App Log/');
INSERT INTO `modules_tree` VALUES (4, 223, 2, 'Panel/System Settings/App Logs/');
INSERT INTO `modules_tree` VALUES (4, 224, 3, 'Panel/System Settings/App Logs/Show App Logs List/');
INSERT INTO `modules_tree` VALUES (4, 225, 3, 'Panel/System Settings/App Logs/Show App Log Info/');
INSERT INTO `modules_tree` VALUES (4, 226, 3, 'Panel/System Settings/App Logs/Delete App Log/');
INSERT INTO `modules_tree` VALUES (2, 223, 3, 'Backend/Panel/System Settings/App Logs/');
INSERT INTO `modules_tree` VALUES (2, 224, 4, 'Backend/Panel/System Settings/App Logs/Show App Logs List/');
INSERT INTO `modules_tree` VALUES (2, 225, 4, 'Backend/Panel/System Settings/App Logs/Show App Log Info/');
INSERT INTO `modules_tree` VALUES (2, 226, 4, 'Backend/Panel/System Settings/App Logs/Delete App Log/');
INSERT INTO `modules_tree` VALUES (1, 223, 4, 'System/Backend/Panel/System Settings/App Logs/');
INSERT INTO `modules_tree` VALUES (1, 224, 5, 'System/Backend/Panel/System Settings/App Logs/Show App Logs List/');
INSERT INTO `modules_tree` VALUES (1, 225, 5, 'System/Backend/Panel/System Settings/App Logs/Show App Log Info/');
INSERT INTO `modules_tree` VALUES (1, 226, 5, 'System/Backend/Panel/System Settings/App Logs/Delete App Log/');
INSERT INTO `modules_tree` VALUES (227, 227, 0, 'Manage Dark Mode/');
INSERT INTO `modules_tree` VALUES (181, 227, 1, 'Site/Manage Dark Mode/');
INSERT INTO `modules_tree` VALUES (3, 227, 2, 'Frontend/Site/Manage Dark Mode/');
INSERT INTO `modules_tree` VALUES (1, 227, 3, 'System/Frontend/Site/Manage Dark Mode/');
COMMIT;

-- ----------------------------
-- Table structure for positions
-- ----------------------------
DROP TABLE IF EXISTS `positions`;
CREATE TABLE `positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of positions
-- ----------------------------
BEGIN;
INSERT INTO `positions` VALUES (1, 'Top Banner', 'top-banner', 1, '2018-11-09 11:18:03', 1, '2018-11-09 11:18:03');
INSERT INTO `positions` VALUES (2, 'Above Menu Banner', 'above-menu-banner', 1, '2018-11-15 20:01:57', 1, '2018-11-15 20:01:57');
INSERT INTO `positions` VALUES (3, 'After Tiles Banner', 'after-tiles-banner', 1, '2018-11-16 21:29:20', 1, '2018-11-16 21:29:20');
INSERT INTO `positions` VALUES (4, 'Skin', 'skin', 1, '2018-11-17 13:13:03', 1, '2018-11-17 13:13:03');
INSERT INTO `positions` VALUES (5, 'Sidebar banner', 'sidebar-banner', 1, '2018-11-17 15:54:59', 1, '2018-11-17 15:54:59');
INSERT INTO `positions` VALUES (6, 'End of Articles', 'end-of-articles', 1, '2018-11-25 00:02:35', 1, '2018-11-25 00:02:35');
INSERT INTO `positions` VALUES (7, 'Inside Articles', 'inside-articles', 1, '2018-11-25 00:02:47', 1, '2018-11-25 00:02:47');
INSERT INTO `positions` VALUES (8, 'After Tiles Banner Left', 'after-tiles-banner-left', 1, '2018-11-25 00:22:51', 1, '2018-11-25 00:22:51');
INSERT INTO `positions` VALUES (9, 'After Tiles Banner Right', 'after-tiles-banner-right', 1, '2018-11-25 00:23:03', 1, '2018-11-25 00:23:03');
COMMIT;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(128) CHARACTER SET utf8 NOT NULL,
  `in_backend` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `roles_ibfk_1` (`parent_id`) USING BTREE,
  KEY `updated_by` (`updated_by`) USING BTREE,
  KEY `created_by` (`created_by`) USING BTREE,
  CONSTRAINT `roles_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `roles` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of roles
-- ----------------------------
BEGIN;
INSERT INTO `roles` VALUES (1, NULL, 'Developer', 1, 1, '2018-01-15 10:20:08', 81, '2019-05-05 03:15:24');
INSERT INTO `roles` VALUES (2, 1, 'Administrator', 1, 1, '2018-01-15 10:20:08', 81, '2019-03-16 12:35:43');
INSERT INTO `roles` VALUES (3, 2, 'Publisher', 1, 1, '2018-01-15 10:20:08', 81, '2019-03-16 12:35:50');
INSERT INTO `roles` VALUES (4, 3, 'Editor', 1, 1, '2018-01-15 10:20:08', 81, '2019-02-09 18:00:33');
INSERT INTO `roles` VALUES (5, 4, 'Author', 1, 1, '2018-01-15 10:20:08', 81, '2019-02-09 18:00:42');
INSERT INTO `roles` VALUES (6, 5, 'User', 0, 1, '2018-01-15 10:20:08', 1, '2018-12-02 19:51:39');
INSERT INTO `roles` VALUES (7, 6, 'Guest', 0, 1, '2018-01-15 10:20:08', 1, '2019-10-05 16:48:01');
INSERT INTO `roles` VALUES (8, 2, 'Moderator', 1, 1, '2018-11-25 10:45:38', 81, '2019-05-21 08:46:16');
COMMIT;

-- ----------------------------
-- Table structure for roles_access_levels
-- ----------------------------
DROP TABLE IF EXISTS `roles_access_levels`;
CREATE TABLE `roles_access_levels` (
  `role_id` int(11) NOT NULL,
  `access_level_id` int(11) NOT NULL,
  KEY `access_level_id` (`access_level_id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  CONSTRAINT `roles_access_levels_ibfk_1` FOREIGN KEY (`access_level_id`) REFERENCES `access_levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `roles_access_levels_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of roles_access_levels
-- ----------------------------
BEGIN;
INSERT INTO `roles_access_levels` VALUES (7, 5);
INSERT INTO `roles_access_levels` VALUES (6, 4);
INSERT INTO `roles_access_levels` VALUES (4, 3);
INSERT INTO `roles_access_levels` VALUES (5, 3);
INSERT INTO `roles_access_levels` VALUES (2, 2);
INSERT INTO `roles_access_levels` VALUES (3, 3);
INSERT INTO `roles_access_levels` VALUES (1, 1);
INSERT INTO `roles_access_levels` VALUES (8, 3);
COMMIT;

-- ----------------------------
-- Table structure for roles_modules
-- ----------------------------
DROP TABLE IF EXISTS `roles_modules`;
CREATE TABLE `roles_modules` (
  `role_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`module_id`) USING BTREE,
  KEY `fk_RolesFunctions_fldFunction` (`module_id`) USING BTREE,
  CONSTRAINT `roles_modules_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `roles_modules_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of roles_modules
-- ----------------------------
BEGIN;
INSERT INTO `roles_modules` VALUES (1, 5);
INSERT INTO `roles_modules` VALUES (1, 6);
INSERT INTO `roles_modules` VALUES (1, 7);
INSERT INTO `roles_modules` VALUES (1, 8);
INSERT INTO `roles_modules` VALUES (1, 9);
INSERT INTO `roles_modules` VALUES (1, 10);
INSERT INTO `roles_modules` VALUES (1, 11);
INSERT INTO `roles_modules` VALUES (1, 12);
INSERT INTO `roles_modules` VALUES (1, 13);
INSERT INTO `roles_modules` VALUES (1, 14);
INSERT INTO `roles_modules` VALUES (1, 15);
INSERT INTO `roles_modules` VALUES (1, 16);
INSERT INTO `roles_modules` VALUES (1, 17);
INSERT INTO `roles_modules` VALUES (1, 18);
INSERT INTO `roles_modules` VALUES (1, 19);
INSERT INTO `roles_modules` VALUES (1, 20);
INSERT INTO `roles_modules` VALUES (1, 21);
INSERT INTO `roles_modules` VALUES (1, 22);
INSERT INTO `roles_modules` VALUES (1, 23);
INSERT INTO `roles_modules` VALUES (1, 24);
INSERT INTO `roles_modules` VALUES (1, 25);
INSERT INTO `roles_modules` VALUES (1, 26);
INSERT INTO `roles_modules` VALUES (1, 27);
INSERT INTO `roles_modules` VALUES (1, 28);
INSERT INTO `roles_modules` VALUES (1, 29);
INSERT INTO `roles_modules` VALUES (1, 30);
INSERT INTO `roles_modules` VALUES (1, 31);
INSERT INTO `roles_modules` VALUES (1, 32);
INSERT INTO `roles_modules` VALUES (1, 33);
INSERT INTO `roles_modules` VALUES (1, 34);
INSERT INTO `roles_modules` VALUES (1, 35);
INSERT INTO `roles_modules` VALUES (1, 36);
INSERT INTO `roles_modules` VALUES (1, 37);
INSERT INTO `roles_modules` VALUES (1, 38);
INSERT INTO `roles_modules` VALUES (1, 39);
INSERT INTO `roles_modules` VALUES (1, 40);
INSERT INTO `roles_modules` VALUES (1, 41);
INSERT INTO `roles_modules` VALUES (1, 42);
INSERT INTO `roles_modules` VALUES (1, 43);
INSERT INTO `roles_modules` VALUES (1, 44);
INSERT INTO `roles_modules` VALUES (1, 45);
INSERT INTO `roles_modules` VALUES (1, 46);
INSERT INTO `roles_modules` VALUES (1, 47);
INSERT INTO `roles_modules` VALUES (1, 48);
INSERT INTO `roles_modules` VALUES (1, 49);
INSERT INTO `roles_modules` VALUES (1, 50);
INSERT INTO `roles_modules` VALUES (1, 52);
INSERT INTO `roles_modules` VALUES (1, 53);
INSERT INTO `roles_modules` VALUES (1, 54);
INSERT INTO `roles_modules` VALUES (1, 55);
INSERT INTO `roles_modules` VALUES (1, 56);
INSERT INTO `roles_modules` VALUES (1, 57);
INSERT INTO `roles_modules` VALUES (1, 58);
INSERT INTO `roles_modules` VALUES (1, 59);
INSERT INTO `roles_modules` VALUES (1, 60);
INSERT INTO `roles_modules` VALUES (1, 61);
INSERT INTO `roles_modules` VALUES (1, 62);
INSERT INTO `roles_modules` VALUES (1, 63);
INSERT INTO `roles_modules` VALUES (1, 64);
INSERT INTO `roles_modules` VALUES (1, 65);
INSERT INTO `roles_modules` VALUES (1, 66);
INSERT INTO `roles_modules` VALUES (1, 67);
INSERT INTO `roles_modules` VALUES (1, 68);
INSERT INTO `roles_modules` VALUES (1, 69);
INSERT INTO `roles_modules` VALUES (1, 70);
INSERT INTO `roles_modules` VALUES (1, 71);
INSERT INTO `roles_modules` VALUES (1, 72);
INSERT INTO `roles_modules` VALUES (1, 73);
INSERT INTO `roles_modules` VALUES (1, 74);
INSERT INTO `roles_modules` VALUES (1, 75);
INSERT INTO `roles_modules` VALUES (1, 76);
INSERT INTO `roles_modules` VALUES (1, 77);
INSERT INTO `roles_modules` VALUES (1, 78);
INSERT INTO `roles_modules` VALUES (1, 79);
INSERT INTO `roles_modules` VALUES (1, 80);
INSERT INTO `roles_modules` VALUES (1, 81);
INSERT INTO `roles_modules` VALUES (1, 82);
INSERT INTO `roles_modules` VALUES (1, 83);
INSERT INTO `roles_modules` VALUES (1, 84);
INSERT INTO `roles_modules` VALUES (1, 86);
INSERT INTO `roles_modules` VALUES (1, 87);
INSERT INTO `roles_modules` VALUES (1, 88);
INSERT INTO `roles_modules` VALUES (1, 89);
INSERT INTO `roles_modules` VALUES (1, 90);
INSERT INTO `roles_modules` VALUES (1, 91);
INSERT INTO `roles_modules` VALUES (1, 92);
INSERT INTO `roles_modules` VALUES (1, 93);
INSERT INTO `roles_modules` VALUES (1, 94);
INSERT INTO `roles_modules` VALUES (1, 95);
INSERT INTO `roles_modules` VALUES (1, 96);
INSERT INTO `roles_modules` VALUES (1, 97);
INSERT INTO `roles_modules` VALUES (1, 98);
INSERT INTO `roles_modules` VALUES (1, 99);
INSERT INTO `roles_modules` VALUES (1, 100);
INSERT INTO `roles_modules` VALUES (1, 101);
INSERT INTO `roles_modules` VALUES (1, 102);
INSERT INTO `roles_modules` VALUES (1, 103);
INSERT INTO `roles_modules` VALUES (1, 104);
INSERT INTO `roles_modules` VALUES (1, 105);
INSERT INTO `roles_modules` VALUES (1, 106);
INSERT INTO `roles_modules` VALUES (1, 107);
INSERT INTO `roles_modules` VALUES (1, 108);
INSERT INTO `roles_modules` VALUES (1, 109);
INSERT INTO `roles_modules` VALUES (1, 110);
INSERT INTO `roles_modules` VALUES (1, 111);
INSERT INTO `roles_modules` VALUES (1, 112);
INSERT INTO `roles_modules` VALUES (1, 113);
INSERT INTO `roles_modules` VALUES (1, 114);
INSERT INTO `roles_modules` VALUES (1, 115);
INSERT INTO `roles_modules` VALUES (1, 116);
INSERT INTO `roles_modules` VALUES (1, 117);
INSERT INTO `roles_modules` VALUES (1, 118);
INSERT INTO `roles_modules` VALUES (1, 119);
INSERT INTO `roles_modules` VALUES (1, 120);
INSERT INTO `roles_modules` VALUES (1, 121);
INSERT INTO `roles_modules` VALUES (1, 122);
INSERT INTO `roles_modules` VALUES (1, 123);
INSERT INTO `roles_modules` VALUES (1, 124);
INSERT INTO `roles_modules` VALUES (1, 125);
INSERT INTO `roles_modules` VALUES (1, 126);
INSERT INTO `roles_modules` VALUES (1, 127);
INSERT INTO `roles_modules` VALUES (1, 128);
INSERT INTO `roles_modules` VALUES (1, 129);
INSERT INTO `roles_modules` VALUES (1, 130);
INSERT INTO `roles_modules` VALUES (1, 131);
INSERT INTO `roles_modules` VALUES (1, 132);
INSERT INTO `roles_modules` VALUES (1, 133);
INSERT INTO `roles_modules` VALUES (1, 134);
INSERT INTO `roles_modules` VALUES (1, 135);
INSERT INTO `roles_modules` VALUES (1, 136);
INSERT INTO `roles_modules` VALUES (1, 137);
INSERT INTO `roles_modules` VALUES (1, 138);
INSERT INTO `roles_modules` VALUES (1, 139);
INSERT INTO `roles_modules` VALUES (1, 140);
INSERT INTO `roles_modules` VALUES (1, 141);
INSERT INTO `roles_modules` VALUES (1, 142);
INSERT INTO `roles_modules` VALUES (1, 143);
INSERT INTO `roles_modules` VALUES (1, 144);
INSERT INTO `roles_modules` VALUES (1, 145);
INSERT INTO `roles_modules` VALUES (1, 146);
INSERT INTO `roles_modules` VALUES (1, 147);
INSERT INTO `roles_modules` VALUES (1, 148);
INSERT INTO `roles_modules` VALUES (1, 149);
INSERT INTO `roles_modules` VALUES (1, 150);
INSERT INTO `roles_modules` VALUES (1, 151);
INSERT INTO `roles_modules` VALUES (1, 152);
INSERT INTO `roles_modules` VALUES (1, 153);
INSERT INTO `roles_modules` VALUES (1, 154);
INSERT INTO `roles_modules` VALUES (1, 155);
INSERT INTO `roles_modules` VALUES (1, 156);
INSERT INTO `roles_modules` VALUES (1, 157);
INSERT INTO `roles_modules` VALUES (1, 158);
INSERT INTO `roles_modules` VALUES (1, 159);
INSERT INTO `roles_modules` VALUES (1, 160);
INSERT INTO `roles_modules` VALUES (1, 161);
INSERT INTO `roles_modules` VALUES (1, 162);
INSERT INTO `roles_modules` VALUES (1, 163);
INSERT INTO `roles_modules` VALUES (1, 164);
INSERT INTO `roles_modules` VALUES (1, 165);
INSERT INTO `roles_modules` VALUES (1, 166);
INSERT INTO `roles_modules` VALUES (1, 167);
INSERT INTO `roles_modules` VALUES (1, 168);
INSERT INTO `roles_modules` VALUES (1, 169);
INSERT INTO `roles_modules` VALUES (1, 170);
INSERT INTO `roles_modules` VALUES (1, 171);
INSERT INTO `roles_modules` VALUES (1, 172);
INSERT INTO `roles_modules` VALUES (1, 173);
INSERT INTO `roles_modules` VALUES (1, 174);
INSERT INTO `roles_modules` VALUES (1, 175);
INSERT INTO `roles_modules` VALUES (1, 176);
INSERT INTO `roles_modules` VALUES (1, 177);
INSERT INTO `roles_modules` VALUES (1, 178);
INSERT INTO `roles_modules` VALUES (1, 180);
INSERT INTO `roles_modules` VALUES (1, 182);
INSERT INTO `roles_modules` VALUES (1, 183);
INSERT INTO `roles_modules` VALUES (1, 184);
INSERT INTO `roles_modules` VALUES (1, 185);
INSERT INTO `roles_modules` VALUES (1, 186);
INSERT INTO `roles_modules` VALUES (1, 187);
INSERT INTO `roles_modules` VALUES (1, 192);
INSERT INTO `roles_modules` VALUES (1, 193);
INSERT INTO `roles_modules` VALUES (1, 194);
INSERT INTO `roles_modules` VALUES (1, 195);
INSERT INTO `roles_modules` VALUES (1, 196);
INSERT INTO `roles_modules` VALUES (1, 197);
INSERT INTO `roles_modules` VALUES (1, 198);
INSERT INTO `roles_modules` VALUES (1, 199);
INSERT INTO `roles_modules` VALUES (1, 200);
INSERT INTO `roles_modules` VALUES (1, 201);
INSERT INTO `roles_modules` VALUES (1, 203);
INSERT INTO `roles_modules` VALUES (1, 204);
INSERT INTO `roles_modules` VALUES (1, 205);
INSERT INTO `roles_modules` VALUES (1, 206);
INSERT INTO `roles_modules` VALUES (1, 207);
INSERT INTO `roles_modules` VALUES (1, 208);
INSERT INTO `roles_modules` VALUES (1, 209);
INSERT INTO `roles_modules` VALUES (1, 210);
INSERT INTO `roles_modules` VALUES (1, 211);
INSERT INTO `roles_modules` VALUES (1, 212);
INSERT INTO `roles_modules` VALUES (1, 213);
INSERT INTO `roles_modules` VALUES (1, 214);
INSERT INTO `roles_modules` VALUES (1, 215);
INSERT INTO `roles_modules` VALUES (1, 216);
INSERT INTO `roles_modules` VALUES (1, 217);
INSERT INTO `roles_modules` VALUES (1, 218);
INSERT INTO `roles_modules` VALUES (1, 219);
INSERT INTO `roles_modules` VALUES (1, 220);
INSERT INTO `roles_modules` VALUES (1, 221);
INSERT INTO `roles_modules` VALUES (1, 222);
INSERT INTO `roles_modules` VALUES (1, 223);
INSERT INTO `roles_modules` VALUES (1, 224);
INSERT INTO `roles_modules` VALUES (1, 225);
INSERT INTO `roles_modules` VALUES (1, 226);
INSERT INTO `roles_modules` VALUES (1, 227);
INSERT INTO `roles_modules` VALUES (2, 5);
INSERT INTO `roles_modules` VALUES (2, 6);
INSERT INTO `roles_modules` VALUES (2, 7);
INSERT INTO `roles_modules` VALUES (2, 8);
INSERT INTO `roles_modules` VALUES (2, 14);
INSERT INTO `roles_modules` VALUES (2, 43);
INSERT INTO `roles_modules` VALUES (2, 44);
INSERT INTO `roles_modules` VALUES (2, 45);
INSERT INTO `roles_modules` VALUES (2, 46);
INSERT INTO `roles_modules` VALUES (2, 47);
INSERT INTO `roles_modules` VALUES (2, 48);
INSERT INTO `roles_modules` VALUES (2, 49);
INSERT INTO `roles_modules` VALUES (2, 50);
INSERT INTO `roles_modules` VALUES (2, 52);
INSERT INTO `roles_modules` VALUES (2, 53);
INSERT INTO `roles_modules` VALUES (2, 54);
INSERT INTO `roles_modules` VALUES (2, 55);
INSERT INTO `roles_modules` VALUES (2, 56);
INSERT INTO `roles_modules` VALUES (2, 57);
INSERT INTO `roles_modules` VALUES (2, 58);
INSERT INTO `roles_modules` VALUES (2, 59);
INSERT INTO `roles_modules` VALUES (2, 60);
INSERT INTO `roles_modules` VALUES (2, 61);
INSERT INTO `roles_modules` VALUES (2, 62);
INSERT INTO `roles_modules` VALUES (2, 63);
INSERT INTO `roles_modules` VALUES (2, 64);
INSERT INTO `roles_modules` VALUES (2, 65);
INSERT INTO `roles_modules` VALUES (2, 66);
INSERT INTO `roles_modules` VALUES (2, 67);
INSERT INTO `roles_modules` VALUES (2, 68);
INSERT INTO `roles_modules` VALUES (2, 69);
INSERT INTO `roles_modules` VALUES (2, 70);
INSERT INTO `roles_modules` VALUES (2, 71);
INSERT INTO `roles_modules` VALUES (2, 72);
INSERT INTO `roles_modules` VALUES (2, 73);
INSERT INTO `roles_modules` VALUES (2, 74);
INSERT INTO `roles_modules` VALUES (2, 75);
INSERT INTO `roles_modules` VALUES (2, 76);
INSERT INTO `roles_modules` VALUES (2, 77);
INSERT INTO `roles_modules` VALUES (2, 78);
INSERT INTO `roles_modules` VALUES (2, 79);
INSERT INTO `roles_modules` VALUES (2, 80);
INSERT INTO `roles_modules` VALUES (2, 81);
INSERT INTO `roles_modules` VALUES (2, 82);
INSERT INTO `roles_modules` VALUES (2, 83);
INSERT INTO `roles_modules` VALUES (2, 84);
INSERT INTO `roles_modules` VALUES (2, 86);
INSERT INTO `roles_modules` VALUES (2, 87);
INSERT INTO `roles_modules` VALUES (2, 88);
INSERT INTO `roles_modules` VALUES (2, 89);
INSERT INTO `roles_modules` VALUES (2, 90);
INSERT INTO `roles_modules` VALUES (2, 91);
INSERT INTO `roles_modules` VALUES (2, 92);
INSERT INTO `roles_modules` VALUES (2, 93);
INSERT INTO `roles_modules` VALUES (2, 94);
INSERT INTO `roles_modules` VALUES (2, 95);
INSERT INTO `roles_modules` VALUES (2, 96);
INSERT INTO `roles_modules` VALUES (2, 98);
INSERT INTO `roles_modules` VALUES (2, 99);
INSERT INTO `roles_modules` VALUES (2, 100);
INSERT INTO `roles_modules` VALUES (2, 101);
INSERT INTO `roles_modules` VALUES (2, 102);
INSERT INTO `roles_modules` VALUES (2, 117);
INSERT INTO `roles_modules` VALUES (2, 118);
INSERT INTO `roles_modules` VALUES (2, 119);
INSERT INTO `roles_modules` VALUES (2, 120);
INSERT INTO `roles_modules` VALUES (2, 121);
INSERT INTO `roles_modules` VALUES (2, 122);
INSERT INTO `roles_modules` VALUES (2, 123);
INSERT INTO `roles_modules` VALUES (2, 124);
INSERT INTO `roles_modules` VALUES (2, 125);
INSERT INTO `roles_modules` VALUES (2, 126);
INSERT INTO `roles_modules` VALUES (2, 127);
INSERT INTO `roles_modules` VALUES (2, 128);
INSERT INTO `roles_modules` VALUES (2, 129);
INSERT INTO `roles_modules` VALUES (2, 130);
INSERT INTO `roles_modules` VALUES (2, 131);
INSERT INTO `roles_modules` VALUES (2, 132);
INSERT INTO `roles_modules` VALUES (2, 133);
INSERT INTO `roles_modules` VALUES (2, 134);
INSERT INTO `roles_modules` VALUES (2, 135);
INSERT INTO `roles_modules` VALUES (2, 136);
INSERT INTO `roles_modules` VALUES (2, 137);
INSERT INTO `roles_modules` VALUES (2, 138);
INSERT INTO `roles_modules` VALUES (2, 139);
INSERT INTO `roles_modules` VALUES (2, 140);
INSERT INTO `roles_modules` VALUES (2, 141);
INSERT INTO `roles_modules` VALUES (2, 142);
INSERT INTO `roles_modules` VALUES (2, 143);
INSERT INTO `roles_modules` VALUES (2, 144);
INSERT INTO `roles_modules` VALUES (2, 145);
INSERT INTO `roles_modules` VALUES (2, 146);
INSERT INTO `roles_modules` VALUES (2, 147);
INSERT INTO `roles_modules` VALUES (2, 148);
INSERT INTO `roles_modules` VALUES (2, 149);
INSERT INTO `roles_modules` VALUES (2, 150);
INSERT INTO `roles_modules` VALUES (2, 151);
INSERT INTO `roles_modules` VALUES (2, 152);
INSERT INTO `roles_modules` VALUES (2, 153);
INSERT INTO `roles_modules` VALUES (2, 154);
INSERT INTO `roles_modules` VALUES (2, 155);
INSERT INTO `roles_modules` VALUES (2, 156);
INSERT INTO `roles_modules` VALUES (2, 157);
INSERT INTO `roles_modules` VALUES (2, 158);
INSERT INTO `roles_modules` VALUES (2, 159);
INSERT INTO `roles_modules` VALUES (2, 160);
INSERT INTO `roles_modules` VALUES (2, 162);
INSERT INTO `roles_modules` VALUES (2, 163);
INSERT INTO `roles_modules` VALUES (2, 164);
INSERT INTO `roles_modules` VALUES (2, 165);
INSERT INTO `roles_modules` VALUES (2, 166);
INSERT INTO `roles_modules` VALUES (2, 167);
INSERT INTO `roles_modules` VALUES (2, 168);
INSERT INTO `roles_modules` VALUES (2, 169);
INSERT INTO `roles_modules` VALUES (2, 173);
INSERT INTO `roles_modules` VALUES (2, 174);
INSERT INTO `roles_modules` VALUES (2, 175);
INSERT INTO `roles_modules` VALUES (2, 176);
INSERT INTO `roles_modules` VALUES (2, 177);
INSERT INTO `roles_modules` VALUES (2, 178);
INSERT INTO `roles_modules` VALUES (2, 180);
INSERT INTO `roles_modules` VALUES (2, 182);
INSERT INTO `roles_modules` VALUES (2, 183);
INSERT INTO `roles_modules` VALUES (2, 184);
INSERT INTO `roles_modules` VALUES (2, 192);
INSERT INTO `roles_modules` VALUES (2, 193);
INSERT INTO `roles_modules` VALUES (2, 194);
INSERT INTO `roles_modules` VALUES (2, 195);
INSERT INTO `roles_modules` VALUES (2, 196);
INSERT INTO `roles_modules` VALUES (2, 197);
INSERT INTO `roles_modules` VALUES (2, 198);
INSERT INTO `roles_modules` VALUES (2, 199);
INSERT INTO `roles_modules` VALUES (2, 200);
INSERT INTO `roles_modules` VALUES (2, 201);
INSERT INTO `roles_modules` VALUES (2, 203);
INSERT INTO `roles_modules` VALUES (2, 204);
INSERT INTO `roles_modules` VALUES (2, 205);
INSERT INTO `roles_modules` VALUES (2, 206);
INSERT INTO `roles_modules` VALUES (2, 207);
INSERT INTO `roles_modules` VALUES (2, 208);
INSERT INTO `roles_modules` VALUES (2, 209);
INSERT INTO `roles_modules` VALUES (2, 210);
INSERT INTO `roles_modules` VALUES (2, 211);
INSERT INTO `roles_modules` VALUES (2, 212);
INSERT INTO `roles_modules` VALUES (2, 213);
INSERT INTO `roles_modules` VALUES (2, 214);
INSERT INTO `roles_modules` VALUES (2, 215);
INSERT INTO `roles_modules` VALUES (2, 216);
INSERT INTO `roles_modules` VALUES (2, 217);
INSERT INTO `roles_modules` VALUES (2, 218);
INSERT INTO `roles_modules` VALUES (2, 219);
INSERT INTO `roles_modules` VALUES (2, 220);
INSERT INTO `roles_modules` VALUES (2, 221);
INSERT INTO `roles_modules` VALUES (2, 222);
INSERT INTO `roles_modules` VALUES (2, 227);
INSERT INTO `roles_modules` VALUES (3, 5);
INSERT INTO `roles_modules` VALUES (3, 6);
INSERT INTO `roles_modules` VALUES (3, 7);
INSERT INTO `roles_modules` VALUES (3, 8);
INSERT INTO `roles_modules` VALUES (3, 14);
INSERT INTO `roles_modules` VALUES (3, 52);
INSERT INTO `roles_modules` VALUES (3, 53);
INSERT INTO `roles_modules` VALUES (3, 54);
INSERT INTO `roles_modules` VALUES (3, 55);
INSERT INTO `roles_modules` VALUES (3, 56);
INSERT INTO `roles_modules` VALUES (3, 57);
INSERT INTO `roles_modules` VALUES (3, 58);
INSERT INTO `roles_modules` VALUES (3, 59);
INSERT INTO `roles_modules` VALUES (3, 60);
INSERT INTO `roles_modules` VALUES (3, 61);
INSERT INTO `roles_modules` VALUES (3, 62);
INSERT INTO `roles_modules` VALUES (3, 63);
INSERT INTO `roles_modules` VALUES (3, 64);
INSERT INTO `roles_modules` VALUES (3, 65);
INSERT INTO `roles_modules` VALUES (3, 70);
INSERT INTO `roles_modules` VALUES (3, 71);
INSERT INTO `roles_modules` VALUES (3, 74);
INSERT INTO `roles_modules` VALUES (3, 75);
INSERT INTO `roles_modules` VALUES (3, 78);
INSERT INTO `roles_modules` VALUES (3, 79);
INSERT INTO `roles_modules` VALUES (3, 80);
INSERT INTO `roles_modules` VALUES (3, 81);
INSERT INTO `roles_modules` VALUES (3, 82);
INSERT INTO `roles_modules` VALUES (3, 83);
INSERT INTO `roles_modules` VALUES (3, 84);
INSERT INTO `roles_modules` VALUES (3, 86);
INSERT INTO `roles_modules` VALUES (3, 87);
INSERT INTO `roles_modules` VALUES (3, 88);
INSERT INTO `roles_modules` VALUES (3, 89);
INSERT INTO `roles_modules` VALUES (3, 90);
INSERT INTO `roles_modules` VALUES (3, 91);
INSERT INTO `roles_modules` VALUES (3, 92);
INSERT INTO `roles_modules` VALUES (3, 93);
INSERT INTO `roles_modules` VALUES (3, 94);
INSERT INTO `roles_modules` VALUES (3, 95);
INSERT INTO `roles_modules` VALUES (3, 96);
INSERT INTO `roles_modules` VALUES (3, 99);
INSERT INTO `roles_modules` VALUES (3, 117);
INSERT INTO `roles_modules` VALUES (3, 118);
INSERT INTO `roles_modules` VALUES (3, 119);
INSERT INTO `roles_modules` VALUES (3, 120);
INSERT INTO `roles_modules` VALUES (3, 121);
INSERT INTO `roles_modules` VALUES (3, 122);
INSERT INTO `roles_modules` VALUES (3, 123);
INSERT INTO `roles_modules` VALUES (3, 124);
INSERT INTO `roles_modules` VALUES (3, 125);
INSERT INTO `roles_modules` VALUES (3, 126);
INSERT INTO `roles_modules` VALUES (3, 127);
INSERT INTO `roles_modules` VALUES (3, 128);
INSERT INTO `roles_modules` VALUES (3, 129);
INSERT INTO `roles_modules` VALUES (3, 130);
INSERT INTO `roles_modules` VALUES (3, 131);
INSERT INTO `roles_modules` VALUES (3, 132);
INSERT INTO `roles_modules` VALUES (3, 133);
INSERT INTO `roles_modules` VALUES (3, 134);
INSERT INTO `roles_modules` VALUES (3, 135);
INSERT INTO `roles_modules` VALUES (3, 136);
INSERT INTO `roles_modules` VALUES (3, 178);
INSERT INTO `roles_modules` VALUES (3, 180);
INSERT INTO `roles_modules` VALUES (3, 182);
INSERT INTO `roles_modules` VALUES (3, 183);
INSERT INTO `roles_modules` VALUES (3, 192);
INSERT INTO `roles_modules` VALUES (3, 193);
INSERT INTO `roles_modules` VALUES (3, 199);
INSERT INTO `roles_modules` VALUES (3, 200);
INSERT INTO `roles_modules` VALUES (3, 204);
INSERT INTO `roles_modules` VALUES (3, 205);
INSERT INTO `roles_modules` VALUES (3, 206);
INSERT INTO `roles_modules` VALUES (3, 207);
INSERT INTO `roles_modules` VALUES (3, 208);
INSERT INTO `roles_modules` VALUES (3, 211);
INSERT INTO `roles_modules` VALUES (3, 212);
INSERT INTO `roles_modules` VALUES (3, 213);
INSERT INTO `roles_modules` VALUES (3, 214);
INSERT INTO `roles_modules` VALUES (3, 215);
INSERT INTO `roles_modules` VALUES (3, 216);
INSERT INTO `roles_modules` VALUES (3, 217);
INSERT INTO `roles_modules` VALUES (3, 218);
INSERT INTO `roles_modules` VALUES (3, 220);
INSERT INTO `roles_modules` VALUES (3, 221);
INSERT INTO `roles_modules` VALUES (3, 222);
INSERT INTO `roles_modules` VALUES (3, 227);
INSERT INTO `roles_modules` VALUES (4, 5);
INSERT INTO `roles_modules` VALUES (4, 6);
INSERT INTO `roles_modules` VALUES (4, 7);
INSERT INTO `roles_modules` VALUES (4, 8);
INSERT INTO `roles_modules` VALUES (4, 14);
INSERT INTO `roles_modules` VALUES (4, 52);
INSERT INTO `roles_modules` VALUES (4, 53);
INSERT INTO `roles_modules` VALUES (4, 54);
INSERT INTO `roles_modules` VALUES (4, 55);
INSERT INTO `roles_modules` VALUES (4, 57);
INSERT INTO `roles_modules` VALUES (4, 59);
INSERT INTO `roles_modules` VALUES (4, 60);
INSERT INTO `roles_modules` VALUES (4, 61);
INSERT INTO `roles_modules` VALUES (4, 63);
INSERT INTO `roles_modules` VALUES (4, 65);
INSERT INTO `roles_modules` VALUES (4, 70);
INSERT INTO `roles_modules` VALUES (4, 74);
INSERT INTO `roles_modules` VALUES (4, 75);
INSERT INTO `roles_modules` VALUES (4, 78);
INSERT INTO `roles_modules` VALUES (4, 79);
INSERT INTO `roles_modules` VALUES (4, 81);
INSERT INTO `roles_modules` VALUES (4, 82);
INSERT INTO `roles_modules` VALUES (4, 84);
INSERT INTO `roles_modules` VALUES (4, 86);
INSERT INTO `roles_modules` VALUES (4, 87);
INSERT INTO `roles_modules` VALUES (4, 88);
INSERT INTO `roles_modules` VALUES (4, 91);
INSERT INTO `roles_modules` VALUES (4, 94);
INSERT INTO `roles_modules` VALUES (4, 95);
INSERT INTO `roles_modules` VALUES (4, 96);
INSERT INTO `roles_modules` VALUES (4, 99);
INSERT INTO `roles_modules` VALUES (4, 134);
INSERT INTO `roles_modules` VALUES (4, 135);
INSERT INTO `roles_modules` VALUES (4, 136);
INSERT INTO `roles_modules` VALUES (4, 178);
INSERT INTO `roles_modules` VALUES (4, 180);
INSERT INTO `roles_modules` VALUES (4, 182);
INSERT INTO `roles_modules` VALUES (4, 183);
INSERT INTO `roles_modules` VALUES (4, 192);
INSERT INTO `roles_modules` VALUES (4, 193);
INSERT INTO `roles_modules` VALUES (4, 199);
INSERT INTO `roles_modules` VALUES (4, 200);
INSERT INTO `roles_modules` VALUES (4, 204);
INSERT INTO `roles_modules` VALUES (4, 205);
INSERT INTO `roles_modules` VALUES (4, 206);
INSERT INTO `roles_modules` VALUES (4, 207);
INSERT INTO `roles_modules` VALUES (4, 208);
INSERT INTO `roles_modules` VALUES (4, 211);
INSERT INTO `roles_modules` VALUES (4, 212);
INSERT INTO `roles_modules` VALUES (4, 213);
INSERT INTO `roles_modules` VALUES (4, 214);
INSERT INTO `roles_modules` VALUES (4, 215);
INSERT INTO `roles_modules` VALUES (4, 216);
INSERT INTO `roles_modules` VALUES (4, 217);
INSERT INTO `roles_modules` VALUES (4, 220);
INSERT INTO `roles_modules` VALUES (4, 227);
INSERT INTO `roles_modules` VALUES (5, 5);
INSERT INTO `roles_modules` VALUES (5, 6);
INSERT INTO `roles_modules` VALUES (5, 7);
INSERT INTO `roles_modules` VALUES (5, 8);
INSERT INTO `roles_modules` VALUES (5, 14);
INSERT INTO `roles_modules` VALUES (5, 52);
INSERT INTO `roles_modules` VALUES (5, 53);
INSERT INTO `roles_modules` VALUES (5, 54);
INSERT INTO `roles_modules` VALUES (5, 55);
INSERT INTO `roles_modules` VALUES (5, 57);
INSERT INTO `roles_modules` VALUES (5, 59);
INSERT INTO `roles_modules` VALUES (5, 60);
INSERT INTO `roles_modules` VALUES (5, 70);
INSERT INTO `roles_modules` VALUES (5, 74);
INSERT INTO `roles_modules` VALUES (5, 75);
INSERT INTO `roles_modules` VALUES (5, 78);
INSERT INTO `roles_modules` VALUES (5, 79);
INSERT INTO `roles_modules` VALUES (5, 81);
INSERT INTO `roles_modules` VALUES (5, 82);
INSERT INTO `roles_modules` VALUES (5, 84);
INSERT INTO `roles_modules` VALUES (5, 85);
INSERT INTO `roles_modules` VALUES (5, 86);
INSERT INTO `roles_modules` VALUES (5, 87);
INSERT INTO `roles_modules` VALUES (5, 88);
INSERT INTO `roles_modules` VALUES (5, 95);
INSERT INTO `roles_modules` VALUES (5, 96);
INSERT INTO `roles_modules` VALUES (5, 99);
INSERT INTO `roles_modules` VALUES (5, 134);
INSERT INTO `roles_modules` VALUES (5, 135);
INSERT INTO `roles_modules` VALUES (5, 136);
INSERT INTO `roles_modules` VALUES (5, 178);
INSERT INTO `roles_modules` VALUES (5, 180);
INSERT INTO `roles_modules` VALUES (5, 182);
INSERT INTO `roles_modules` VALUES (5, 183);
INSERT INTO `roles_modules` VALUES (5, 192);
INSERT INTO `roles_modules` VALUES (5, 193);
INSERT INTO `roles_modules` VALUES (5, 199);
INSERT INTO `roles_modules` VALUES (5, 200);
INSERT INTO `roles_modules` VALUES (5, 204);
INSERT INTO `roles_modules` VALUES (5, 205);
INSERT INTO `roles_modules` VALUES (5, 206);
INSERT INTO `roles_modules` VALUES (5, 207);
INSERT INTO `roles_modules` VALUES (5, 208);
INSERT INTO `roles_modules` VALUES (5, 211);
INSERT INTO `roles_modules` VALUES (5, 212);
INSERT INTO `roles_modules` VALUES (5, 213);
INSERT INTO `roles_modules` VALUES (5, 214);
INSERT INTO `roles_modules` VALUES (5, 215);
INSERT INTO `roles_modules` VALUES (5, 216);
INSERT INTO `roles_modules` VALUES (5, 217);
INSERT INTO `roles_modules` VALUES (5, 220);
INSERT INTO `roles_modules` VALUES (5, 227);
INSERT INTO `roles_modules` VALUES (6, 5);
INSERT INTO `roles_modules` VALUES (6, 6);
INSERT INTO `roles_modules` VALUES (6, 7);
INSERT INTO `roles_modules` VALUES (6, 8);
INSERT INTO `roles_modules` VALUES (6, 14);
INSERT INTO `roles_modules` VALUES (6, 59);
INSERT INTO `roles_modules` VALUES (6, 60);
INSERT INTO `roles_modules` VALUES (6, 74);
INSERT INTO `roles_modules` VALUES (6, 75);
INSERT INTO `roles_modules` VALUES (6, 78);
INSERT INTO `roles_modules` VALUES (6, 79);
INSERT INTO `roles_modules` VALUES (6, 87);
INSERT INTO `roles_modules` VALUES (6, 88);
INSERT INTO `roles_modules` VALUES (6, 178);
INSERT INTO `roles_modules` VALUES (6, 180);
INSERT INTO `roles_modules` VALUES (6, 182);
INSERT INTO `roles_modules` VALUES (6, 183);
INSERT INTO `roles_modules` VALUES (6, 192);
INSERT INTO `roles_modules` VALUES (6, 193);
INSERT INTO `roles_modules` VALUES (6, 199);
INSERT INTO `roles_modules` VALUES (6, 200);
INSERT INTO `roles_modules` VALUES (6, 204);
INSERT INTO `roles_modules` VALUES (6, 205);
INSERT INTO `roles_modules` VALUES (6, 206);
INSERT INTO `roles_modules` VALUES (6, 207);
INSERT INTO `roles_modules` VALUES (6, 208);
INSERT INTO `roles_modules` VALUES (6, 211);
INSERT INTO `roles_modules` VALUES (6, 212);
INSERT INTO `roles_modules` VALUES (6, 213);
INSERT INTO `roles_modules` VALUES (6, 214);
INSERT INTO `roles_modules` VALUES (6, 215);
INSERT INTO `roles_modules` VALUES (6, 216);
INSERT INTO `roles_modules` VALUES (6, 217);
INSERT INTO `roles_modules` VALUES (6, 227);
INSERT INTO `roles_modules` VALUES (7, 5);
INSERT INTO `roles_modules` VALUES (7, 6);
INSERT INTO `roles_modules` VALUES (7, 7);
INSERT INTO `roles_modules` VALUES (7, 8);
INSERT INTO `roles_modules` VALUES (7, 14);
INSERT INTO `roles_modules` VALUES (7, 59);
INSERT INTO `roles_modules` VALUES (7, 60);
INSERT INTO `roles_modules` VALUES (7, 74);
INSERT INTO `roles_modules` VALUES (7, 75);
INSERT INTO `roles_modules` VALUES (7, 78);
INSERT INTO `roles_modules` VALUES (7, 79);
INSERT INTO `roles_modules` VALUES (7, 87);
INSERT INTO `roles_modules` VALUES (7, 88);
INSERT INTO `roles_modules` VALUES (7, 179);
INSERT INTO `roles_modules` VALUES (7, 182);
INSERT INTO `roles_modules` VALUES (7, 183);
INSERT INTO `roles_modules` VALUES (7, 188);
INSERT INTO `roles_modules` VALUES (7, 189);
INSERT INTO `roles_modules` VALUES (7, 190);
INSERT INTO `roles_modules` VALUES (7, 191);
INSERT INTO `roles_modules` VALUES (7, 192);
INSERT INTO `roles_modules` VALUES (7, 193);
INSERT INTO `roles_modules` VALUES (7, 199);
INSERT INTO `roles_modules` VALUES (7, 205);
INSERT INTO `roles_modules` VALUES (7, 206);
INSERT INTO `roles_modules` VALUES (7, 208);
INSERT INTO `roles_modules` VALUES (7, 215);
INSERT INTO `roles_modules` VALUES (7, 216);
INSERT INTO `roles_modules` VALUES (7, 227);
INSERT INTO `roles_modules` VALUES (8, 5);
INSERT INTO `roles_modules` VALUES (8, 6);
INSERT INTO `roles_modules` VALUES (8, 7);
INSERT INTO `roles_modules` VALUES (8, 8);
INSERT INTO `roles_modules` VALUES (8, 14);
INSERT INTO `roles_modules` VALUES (8, 43);
INSERT INTO `roles_modules` VALUES (8, 44);
INSERT INTO `roles_modules` VALUES (8, 45);
INSERT INTO `roles_modules` VALUES (8, 46);
INSERT INTO `roles_modules` VALUES (8, 47);
INSERT INTO `roles_modules` VALUES (8, 48);
INSERT INTO `roles_modules` VALUES (8, 49);
INSERT INTO `roles_modules` VALUES (8, 59);
INSERT INTO `roles_modules` VALUES (8, 60);
INSERT INTO `roles_modules` VALUES (8, 74);
INSERT INTO `roles_modules` VALUES (8, 75);
INSERT INTO `roles_modules` VALUES (8, 78);
INSERT INTO `roles_modules` VALUES (8, 79);
INSERT INTO `roles_modules` VALUES (8, 87);
INSERT INTO `roles_modules` VALUES (8, 88);
INSERT INTO `roles_modules` VALUES (8, 99);
INSERT INTO `roles_modules` VALUES (8, 134);
INSERT INTO `roles_modules` VALUES (8, 135);
INSERT INTO `roles_modules` VALUES (8, 136);
INSERT INTO `roles_modules` VALUES (8, 173);
INSERT INTO `roles_modules` VALUES (8, 174);
INSERT INTO `roles_modules` VALUES (8, 175);
INSERT INTO `roles_modules` VALUES (8, 176);
INSERT INTO `roles_modules` VALUES (8, 177);
INSERT INTO `roles_modules` VALUES (8, 178);
INSERT INTO `roles_modules` VALUES (8, 180);
INSERT INTO `roles_modules` VALUES (8, 182);
INSERT INTO `roles_modules` VALUES (8, 183);
INSERT INTO `roles_modules` VALUES (8, 184);
INSERT INTO `roles_modules` VALUES (8, 192);
INSERT INTO `roles_modules` VALUES (8, 193);
INSERT INTO `roles_modules` VALUES (8, 199);
INSERT INTO `roles_modules` VALUES (8, 200);
INSERT INTO `roles_modules` VALUES (8, 201);
INSERT INTO `roles_modules` VALUES (8, 203);
INSERT INTO `roles_modules` VALUES (8, 204);
INSERT INTO `roles_modules` VALUES (8, 205);
INSERT INTO `roles_modules` VALUES (8, 206);
INSERT INTO `roles_modules` VALUES (8, 207);
INSERT INTO `roles_modules` VALUES (8, 208);
INSERT INTO `roles_modules` VALUES (8, 209);
INSERT INTO `roles_modules` VALUES (8, 210);
INSERT INTO `roles_modules` VALUES (8, 211);
INSERT INTO `roles_modules` VALUES (8, 212);
INSERT INTO `roles_modules` VALUES (8, 213);
INSERT INTO `roles_modules` VALUES (8, 214);
INSERT INTO `roles_modules` VALUES (8, 215);
INSERT INTO `roles_modules` VALUES (8, 216);
INSERT INTO `roles_modules` VALUES (8, 217);
INSERT INTO `roles_modules` VALUES (8, 227);
COMMIT;

-- ----------------------------
-- Table structure for roles_tree
-- ----------------------------
DROP TABLE IF EXISTS `roles_tree`;
CREATE TABLE `roles_tree` (
  `parent_id` int(11) NOT NULL,
  `child_id` int(11) NOT NULL,
  `depth` int(11) DEFAULT NULL,
  `path` mediumtext CHARACTER SET utf8,
  KEY `roles_tree_ibfk_1` (`parent_id`) USING BTREE,
  KEY `roles_tree_ibfk_2` (`child_id`) USING BTREE,
  CONSTRAINT `roles_tree_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `roles_tree_ibfk_2` FOREIGN KEY (`child_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of roles_tree
-- ----------------------------
BEGIN;
INSERT INTO `roles_tree` VALUES (1, 1, 0, '1/');
INSERT INTO `roles_tree` VALUES (2, 2, 0, '2/');
INSERT INTO `roles_tree` VALUES (1, 2, 1, '1/2/');
INSERT INTO `roles_tree` VALUES (3, 3, 0, '3/');
INSERT INTO `roles_tree` VALUES (2, 3, 1, '2/3/');
INSERT INTO `roles_tree` VALUES (1, 3, 2, '1/2/3/');
INSERT INTO `roles_tree` VALUES (4, 4, 0, '4/');
INSERT INTO `roles_tree` VALUES (3, 4, 1, '3/4/');
INSERT INTO `roles_tree` VALUES (2, 4, 2, '2/3/4/');
INSERT INTO `roles_tree` VALUES (1, 4, 3, '1/2/3/4/');
INSERT INTO `roles_tree` VALUES (5, 5, 0, '5/');
INSERT INTO `roles_tree` VALUES (4, 5, 1, '4/5/');
INSERT INTO `roles_tree` VALUES (3, 5, 2, '3/4/5/');
INSERT INTO `roles_tree` VALUES (2, 5, 3, '2/3/4/5/');
INSERT INTO `roles_tree` VALUES (1, 5, 4, '1/2/3/4/5/');
INSERT INTO `roles_tree` VALUES (6, 6, 0, '6/');
INSERT INTO `roles_tree` VALUES (5, 6, 1, '5/6/');
INSERT INTO `roles_tree` VALUES (4, 6, 2, '4/5/6/');
INSERT INTO `roles_tree` VALUES (3, 6, 3, '3/4/5/6/');
INSERT INTO `roles_tree` VALUES (2, 6, 4, '2/3/4/5/6/');
INSERT INTO `roles_tree` VALUES (1, 6, 5, '1/2/3/4/5/6/');
INSERT INTO `roles_tree` VALUES (7, 7, 0, '7/');
INSERT INTO `roles_tree` VALUES (6, 7, 1, '6/7/');
INSERT INTO `roles_tree` VALUES (5, 7, 2, '5/6/7/');
INSERT INTO `roles_tree` VALUES (4, 7, 3, '4/5/6/7/');
INSERT INTO `roles_tree` VALUES (3, 7, 4, '3/4/5/6/7/');
INSERT INTO `roles_tree` VALUES (2, 7, 5, '2/3/4/5/6/7/');
INSERT INTO `roles_tree` VALUES (1, 7, 6, '1/2/3/4/5/6/7/');
INSERT INTO `roles_tree` VALUES (8, 8, 0, '8/');
INSERT INTO `roles_tree` VALUES (2, 8, 1, '2/8/');
INSERT INTO `roles_tree` VALUES (1, 8, 2, '1/2/8/');
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `last_name` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `user_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `role_id` int(11) NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `last_visit_date` datetime NOT NULL,
  `is_enabled` int(1) NOT NULL DEFAULT '0',
  `in_backend` int(1) NOT NULL DEFAULT '0',
  `is_activated` int(1) DEFAULT NULL,
  `privacy_policy` int(11) NOT NULL DEFAULT '0',
  `activation_token` varchar(255) DEFAULT NULL,
  `activated_on` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `enabled` (`is_enabled`) USING BTREE,
  KEY `access_backend` (`in_backend`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `updated_by` (`updated_by`) USING BTREE,
  KEY `created_by` (`created_by`) USING BTREE,
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=70106 DEFAULT CHARSET=utf8mb4;

-- Records of users --
INSERT INTO `users`(`id`, `name`, `last_name`, `user_name`, `password`, `role_id`, `email`, `remember_token`, `email_verified_at`, `last_visit_date`, `is_enabled`, `in_backend`, `is_activated`, `privacy_policy`, `activation_token`, `activated_on`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (1, 'Demouser', '', 'demouser', '$2y$10$.zPJrLlJ.z9bf8ZNKyUwtOp9UiWz.vgi8y6vpvYyRDbpMZiVyVPji', 1, 'info@cms.com', NULL, NULL, '2019-09-26 21:30:14', 1, 1, 1, 1, NULL, NULL, 1, '2019-09-26 21:02:33', 1, '2019-09-26 21:48:54');

-- Records of users fields --
INSERT INTO `users_fields`(`user_id`, `avatar`, `gender`, `description`, `signature`, `birthdate`, `location`, `twitch`, `instagram`, `twitter`, `facebook`, `psn_id`, `xbox_live_gamertag`, `nintendo_id`, `steam_id`, `pc`, `tablet`, `smartphone`, `consoles`) VALUES (1, '', 1, 'tests', 'test', '1990-12-29', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'tst', 'test', '1,3,5,6');

-- Records of users modules --
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 52);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 83);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 80);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 54);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 96);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 82);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 218);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 86);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 53);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 81);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 164);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 168);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 165);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 169);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 167);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 163);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 166);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 219);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 141);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 145);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 144);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 142);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 140);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 143);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 138);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 148);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 147);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 152);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 149);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 150);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 151);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 176);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 175);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 174);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 184);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 155);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 157);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 156);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 154);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 159);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 158);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 5);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 126);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 128);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 127);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 130);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 125);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 129);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 119);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 121);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 120);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 123);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 132);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 118);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 221);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 122);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 131);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 99);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 55);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 101);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 161);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 160);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 98);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 102);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 100);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 187);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 186);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 220);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 68);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 73);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 69);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 77);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 76);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 67);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 72);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 6);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 7);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 135);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 136);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 14);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 37);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 39);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 35);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 38);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 34);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 36);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 226);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 225);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 224);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 172);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 171);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 112);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 116);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 113);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 115);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 111);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 114);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 41);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 42);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 105);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 109);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 106);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 107);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 104);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 108);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 23);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 31);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 16);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 24);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 13);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 10);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 20);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 22);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 18);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 21);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 11);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 19);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 27);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 32);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 28);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 50);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 30);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 26);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 29);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 15);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 90);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 92);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 91);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 93);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 222);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 95);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 94);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 196);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 197);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 195);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 198);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 70);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 45);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 46);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 49);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 47);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 44);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 48);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 79);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 60);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 178);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 210);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 209);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 217);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 206);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 207);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 213);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 211);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 208);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 212);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 216);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 215);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 75);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 88);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 214);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 203);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 204);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 205);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 182);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 183);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 199);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 193);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 180);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 200);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 192);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 162);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 139);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 137);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 146);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 173);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 153);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 124);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 133);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 117);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 97);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 185);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 66);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 134);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 33);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 223);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 170);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 110);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 40);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 103);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 12);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 17);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 25);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 9);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 89);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 194);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 43);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 78);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 59);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 177);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 74);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 87);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 201);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 56);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 58);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 71);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 64);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 61);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 65);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 62);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 57);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 63);
INSERT INTO `users_modules`(`user_id`, `module_id`) VALUES (1, 227);

-- ----------------------------
-- Table structure for users_fields
-- ----------------------------
DROP TABLE IF EXISTS `users_fields`;
CREATE TABLE `users_fields` (
  `user_id` int(11) DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `gender` int(1) DEFAULT '0',
  `description` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `signature` varchar(255) NOT NULL,
  `birthdate` date DEFAULT NULL,
  `location` varchar(50) CHARACTER SET utf8 DEFAULT '',
  `twitch` varchar(50) CHARACTER SET utf8 DEFAULT '',
  `instagram` varchar(50) CHARACTER SET utf8 DEFAULT '',
  `twitter` varchar(50) CHARACTER SET utf8 DEFAULT '',
  `facebook` varchar(50) CHARACTER SET utf8 DEFAULT '',
  `psn_id` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `xbox_live_gamertag` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `nintendo_id` varchar(255) DEFAULT '',
  `steam_id` varchar(255) DEFAULT '',
  `pc` longtext,
  `tablet` varchar(255) DEFAULT '',
  `smartphone` varchar(255) DEFAULT '',
  `consoles` varchar(255) DEFAULT '',
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `users_fields_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for users_modules
-- ----------------------------
DROP TABLE IF EXISTS `users_modules`;
CREATE TABLE `users_modules` (
  `user_id` int(11) DEFAULT NULL,
  `module_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `module_id` (`module_id`) USING BTREE,
  CONSTRAINT `users_modules_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_modules_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for nodes
-- ----------------------------
DROP TABLE IF EXISTS `nodes`;
CREATE TABLE `nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `old_tag_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `view` varchar(255) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `access_level_id` int(11) DEFAULT NULL,
  `on_menu` int(1) NOT NULL DEFAULT '0',
  `in_backend` int(1) NOT NULL DEFAULT '0',
  `is_active` int(1) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `nodes_ibfk_2` (`access_level_id`) USING BTREE,
  KEY `module_id` (`module_id`) USING BTREE,
  CONSTRAINT `nodes_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `nodes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nodes_ibfk_2` FOREIGN KEY (`access_level_id`) REFERENCES `access_levels` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `nodes_ibfk_3` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=68437 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for nodes_closure
-- ----------------------------
DROP TABLE IF EXISTS `nodes_closure`;
CREATE TABLE `nodes_closure` (
  `ancestor` int(11) NOT NULL,
  `descendant` int(11) NOT NULL,
  `depth` int(11) NOT NULL,
  `path` text CHARACTER SET latin1 NOT NULL,
  KEY `ancestor` (`ancestor`) USING BTREE,
  KEY `descendant` (`descendant`) USING BTREE,
  CONSTRAINT `nodes_closure_ibfk_1` FOREIGN KEY (`ancestor`) REFERENCES `nodes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nodes_closure_ibfk_2` FOREIGN KEY (`descendant`) REFERENCES `nodes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for nodes_content
-- ----------------------------
DROP TABLE IF EXISTS `nodes_content`;
CREATE TABLE `nodes_content` (
  `node_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `icon` varchar(255) NOT NULL DEFAULT '',
  `intro_text` varchar(255) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '',
  `gallery` int(1) NOT NULL DEFAULT '0',
  `video` longtext,
  `language_id` int(11) DEFAULT '1',
  `content_type_id` int(11) NOT NULL DEFAULT '1',
  `is_featured` int(1) NOT NULL DEFAULT '0',
  `is_sticky` int(1) NOT NULL DEFAULT '0',
  `homepage` int(1) NOT NULL DEFAULT '0',
  `sponsored_by_id` int(11) DEFAULT NULL,
  `comments_lock` int(1) DEFAULT '0',
  `extra_fields_group_id` int(11) DEFAULT NULL,
  `extra_fields_values` longtext,
  `social_media_title` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL DEFAULT '',
  `meta_description` varchar(255) NOT NULL DEFAULT '',
  `robots` varchar(50) NOT NULL DEFAULT '',
  `published_on` datetime NOT NULL,
  `finished_on` datetime NOT NULL,
  PRIMARY KEY (`node_id`) USING BTREE,
  UNIQUE KEY `node_id` (`node_id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE,
  KEY `type_id` (`content_type_id`) USING BTREE,
  KEY `extra_field_group_id` (`extra_fields_group_id`) USING BTREE,
  KEY `sponsored_by_id` (`sponsored_by_id`) USING BTREE,
  CONSTRAINT `nodes_content_ibfk_1` FOREIGN KEY (`node_id`) REFERENCES `nodes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nodes_content_ibfk_3` FOREIGN KEY (`content_type_id`) REFERENCES `content_types` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `nodes_content_ibfk_5` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nodes_content_ibfk_6` FOREIGN KEY (`extra_fields_group_id`) REFERENCES `extra_fields_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nodes_content_ibfk_7` FOREIGN KEY (`sponsored_by_id`) REFERENCES `clients` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=68437 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for nodes_tags
-- ----------------------------
DROP TABLE IF EXISTS `nodes_tags`;
CREATE TABLE `nodes_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `node_id_tag_id` (`node_id`,`tag_id`) USING BTREE,
  KEY `node_id` (`node_id`) USING BTREE,
  KEY `tag_id` (`tag_id`) USING BTREE,
  CONSTRAINT `nodes_tags_ibfk_1` FOREIGN KEY (`tag_id`) REFERENCES `nodes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nodes_tags_ibfk_2` FOREIGN KEY (`node_id`) REFERENCES `nodes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=183657 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for nodes_translations
-- ----------------------------
DROP TABLE IF EXISTS `nodes_translations`;
CREATE TABLE `nodes_translations` (
  `node_id` int(11) NOT NULL,
  `translation_id` int(11) NOT NULL,
  KEY `node_id` (`node_id`) USING BTREE,
  KEY `translation_id` (`translation_id`) USING BTREE,
  CONSTRAINT `nodes_translations_ibfk_1` FOREIGN KEY (`node_id`) REFERENCES `nodes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nodes_translations_ibfk_2` FOREIGN KEY (`translation_id`) REFERENCES `nodes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Records of nodes --
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (1, NULL, NULL, '/', '', NULL, NULL, 0, 0, 1, 0, 1, '2018-01-28 19:57:06', 1, '2018-01-28 19:57:06');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (2, 7, NULL, 'dashboard', 'backend/dashboard', 5, 3, 1, 1, 1, 1, 1, '2018-01-28 19:57:06', 81, '2019-10-01 12:34:01');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (4, 6, NULL, 'modules', 'backend/modules/modules.list', 13, 1, 1, 1, 1, 6, 1, '2018-01-28 19:57:06', 1, '2019-10-01 12:34:01');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (5, 6, NULL, 'panel-pages', 'backend/nodes/nodes.list', 11, 1, 1, 1, 1, 7, 1, '2018-01-28 19:57:06', 1, '2019-10-01 12:34:01');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (6, 7, NULL, 'system-settings', 'backend/default.page', 15, 1, 0, 1, 1, 13, 1, '2018-01-28 19:57:06', 81, '2019-10-01 12:34:01');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (7, 3, NULL, 'panel', 'backend/login', 14, 5, 0, 1, 1, 0, 1, '2018-01-28 19:57:06', 1, '2019-10-01 12:34:02');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (9, 5, NULL, 'edit-panel-page', 'backend/nodes/nodes.view', 18, 1, 0, 1, 1, 0, 1, '2018-01-28 19:57:06', 1, '2019-10-01 12:34:02');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (10, 5, NULL, 'add-panel-page', 'backend/nodes/nodes.add', 20, 1, 0, 1, 1, 0, 1, '2018-01-28 19:57:06', 1, '2019-10-01 12:34:02');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (11, 6, NULL, 'roles', 'backend/roles/roles.list', 26, 1, 1, 1, 1, 2, 1, '2018-01-28 19:57:06', 1, '2019-10-01 12:34:02');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (12, 11, NULL, 'add-role', 'backend/roles/roles.add', 27, 1, 0, 1, 1, 0, 1, '2018-01-28 19:57:06', 1, '2019-10-01 12:34:02');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (13, 4, NULL, 'add-module', 'backend/modules/modules.add', 23, 1, 0, 1, 1, 0, 1, '2018-01-28 19:57:06', 1, '2019-10-01 12:34:02');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (14, 11, NULL, 'edit-role', 'backend/roles/roles.view', 28, 1, 0, 1, 1, 0, 1, '2018-01-28 19:57:06', 1, '2019-10-01 12:34:02');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (15, 6, NULL, 'access-levels', 'backend/access_levels/access_levels.list', 34, 1, 1, 1, 1, 3, 1, '2018-01-28 19:57:06', 1, '2019-10-01 12:34:02');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (16, 15, NULL, 'edit-access-level', 'backend/access_levels/access_levels.view', 35, 1, 0, 1, 1, 0, 1, '2018-01-28 19:57:06', 1, '2019-10-01 12:34:03');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (17, 15, NULL, 'add-access-level', 'backend/access_levels/access_levels.add', 37, 1, 0, 1, 1, 0, 1, '2018-01-28 19:57:06', 1, '2019-10-01 12:34:03');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (18, 6, NULL, 'general-settings', 'backend/settings/settings.view', 41, 1, 1, 1, 1, 0, 1, '2018-01-28 19:57:06', 1, '2019-10-01 12:34:03');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (19, 7, NULL, 'users', 'backend/users/users.list', 44, 3, 1, 1, 1, 2, 1, '2018-01-28 19:57:06', 81, '2019-10-01 12:34:03');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (20, 4, NULL, 'edit-module', 'backend/modules/modules.view', 16, 1, 0, 1, 1, 0, 1, '2018-01-28 19:57:06', 1, '2019-10-01 12:34:03');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (21, 19, NULL, 'add-user', 'backend/users/users.add', 45, 3, 0, 1, 1, 0, 1, '2018-01-28 19:57:06', 81, '2019-10-01 12:34:03');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (22, 19, NULL, 'edit-user', 'backend/users/users.view', 49, 3, 0, 1, 1, 0, 1, '2018-01-28 19:57:06', 81, '2019-10-01 12:34:03');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (23, 7, NULL, 'articles', 'backend/articles/articles.list', 53, 3, 1, 1, 1, 4, 1, '2018-01-28 19:57:06', 81, '2019-10-01 12:34:03');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (24, 23, NULL, 'add-article', 'backend/articles/articles.add', 52, 3, 0, 1, 1, 3, 1, '2018-01-28 19:57:06', 1, '2019-10-01 12:34:04');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (25, 23, NULL, 'edit-article', 'backend/articles/articles.view', 54, 3, 0, 1, 1, 4, 1, '2018-01-28 19:57:06', 1, '2019-10-01 12:34:04');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (26, 7, NULL, 'media-manager', 'backend/media_manager/media.view', 55, 3, 1, 1, 1, 9, 1, '2018-01-28 19:57:06', 81, '2019-10-01 12:34:04');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (27, 7, NULL, 'categories', 'backend/categories/categories.list', 57, 3, 1, 1, 1, 6, 1, '2018-01-28 19:57:06', 81, '2019-10-01 12:34:04');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (28, 27, NULL, 'add-category', 'backend/categories/categories.add', 58, 3, 0, 1, 1, 0, 1, '2018-01-28 19:57:06', 1, '2019-10-01 12:34:04');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (29, 27, NULL, 'edit-category', 'backend/categories/categories.view', 61, 3, 0, 1, 1, 0, 1, '2018-01-28 19:57:06', 1, '2019-10-01 12:34:04');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (30, 7, NULL, 'pages', 'backend/pages/pages.list', 67, 3, 1, 1, 1, 7, 1, '2018-01-30 13:54:08', 81, '2019-10-01 12:34:04');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (34, 30, NULL, 'add-page', 'backend/pages/pages.add', 68, 3, 0, 1, 1, 0, 1, '2018-02-08 13:37:47', 1, '2019-10-01 12:34:04');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (35, 30, NULL, 'edit-page', 'backend/pages/pages.view', 69, 3, 0, 1, 1, 0, 1, '2018-02-08 13:39:46', 1, '2019-10-01 12:34:05');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (36, 7, NULL, 'tags', 'backend/tags/tags.list', 95, 3, 1, 1, 1, 5, 1, '2018-02-12 14:55:16', 81, '2019-10-01 12:34:05');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (37, 36, NULL, 'add-tag', 'backend/tags/tags.add', 90, 3, 0, 1, 1, 0, 1, '2018-02-12 14:58:23', 1, '2019-10-01 12:34:05');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (38, 36, NULL, 'edit-tag', 'backend/tags/tags.view', 91, 3, 0, 1, 1, 0, 1, '2018-02-12 14:59:29', 1, '2019-10-01 12:34:05');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (39, 7, NULL, 'menus', 'backend/default.page', 98, 2, 1, 1, 1, 12, 1, '2018-02-13 14:08:19', 81, '2019-10-01 12:34:05');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (40, 6, NULL, 'languages', 'backend/languages/languages.list', 104, 1, 1, 1, 1, 4, 1, '2018-02-14 11:44:17', 1, '2019-10-01 12:34:05');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (41, 40, NULL, 'edit-language', 'backend/languages/languages.view', 106, 1, 0, 1, 1, 0, 1, '2018-02-14 12:13:42', 1, '2019-10-01 12:34:05');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (42, 40, NULL, 'add-language', 'backend/languages/languages.add', 105, 1, 0, 1, 1, 0, 1, '2018-02-14 12:14:19', 1, '2019-10-01 12:34:05');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (43, 6, NULL, 'content-types', 'backend/content_types/content_types.list', 111, 1, 1, 1, 1, 5, 1, '2018-02-14 13:05:04', 1, '2019-10-01 12:34:06');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (44, 43, NULL, 'add-content-type', 'backend/content_types/content_types.add', 112, 1, 0, 1, 1, 0, 1, '2018-02-14 13:07:00', 1, '2019-10-01 12:34:06');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (45, 43, NULL, 'edit-content-type', 'backend/content_types/content_types.view', 113, 1, 0, 1, 1, 0, 1, '2018-02-14 13:07:32', 1, '2019-10-01 12:34:06');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (46, 7, NULL, 'extra-fields', 'backend/default.page', 131, 1, 1, 1, 1, 11, 1, '2018-02-14 14:20:14', 81, '2019-10-01 12:34:06');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (47, 46, NULL, 'extra-fields-groups', 'backend/extrafieldsgroups/extra_fields_groups.list', 125, 1, 1, 1, 1, 8, 1, '2018-02-14 14:27:12', 1, '2019-10-01 12:34:06');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (48, 47, NULL, 'add-extra-fields-group', 'backend/extrafieldsgroups/extra_fields_groups.add', 126, 1, 0, 1, 1, 0, 1, '2018-02-14 15:08:25', 1, '2019-10-01 12:34:06');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (49, 47, NULL, 'edit-extra-fields-group', 'backend/extrafieldsgroups/extra_fields_groups.view', 127, 1, 0, 1, 1, 0, 1, '2018-02-14 15:08:51', 1, '2019-10-01 12:34:06');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (50, 46, NULL, 'extra-fields-list', 'backend/extrafields/extra_fields.list', 118, 1, 1, 1, 1, 0, 1, '2018-02-14 15:31:07', 1, '2019-10-01 12:34:06');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (51, 50, NULL, 'add-extra-field', 'backend/extrafields/extra_fields.add', 119, 1, 0, 1, 1, 0, 1, '2018-02-14 15:37:32', 1, '2019-10-01 12:34:07');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (52, 50, NULL, 'edit-extra-field', 'backend/extrafields/extra_fields.view', 120, 1, 0, 1, 1, 0, 1, '2018-02-14 15:38:02', 1, '2019-10-01 12:34:07');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (53, 7, NULL, 'my-profile', 'backend/profile/profile.view', 135, 3, 0, 1, 1, 0, 1, '2018-02-15 11:52:30', 81, '2019-10-01 12:34:07');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (54, 7, NULL, 'banners', 'backend/default.page', 138, 3, 1, 1, 1, 10, 1, '2018-02-22 12:20:51', 81, '2019-10-01 12:34:07');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (55, 54, NULL, 'positions', 'backend/banners/positions/positions.list', 140, 1, 1, 1, 1, 1, 1, '2018-02-22 12:46:20', 1, '2019-10-01 12:34:07');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (56, 55, NULL, 'add-banners-position', 'backend/banners/positions/positions.add', 141, 1, 0, 1, 1, 0, 1, '2018-02-22 13:29:41', 1, '2019-10-01 12:34:07');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (57, 55, NULL, 'edit-banners-position', 'backend/banners/positions/positions.edit', 144, 1, 0, 1, 1, 0, 1, '2018-02-22 14:22:08', 1, '2019-10-01 12:34:07');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (58, 54, NULL, 'clients', 'backend/clients/clients.list', 147, 3, 1, 1, 1, 2, 1, '2018-02-22 15:11:06', 1, '2019-10-01 12:34:08');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (59, 58, NULL, 'add-client', 'backend/clients/clients.add', 148, 3, 0, 1, 1, 0, 1, '2018-02-22 16:29:24', 1, '2019-10-01 12:34:08');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (60, 58, NULL, 'edit-client', 'backend/clients/clients.view', 149, 3, 0, 1, 1, 0, 1, '2018-02-27 13:42:47', 1, '2019-10-01 12:34:08');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (61, 64, NULL, 'client-contacts', 'backend/contacts/contacts.inner_list', 154, 3, 0, 1, 1, 0, 1, '2018-03-02 10:29:11', 1, '2019-10-01 12:34:08');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (62, 64, NULL, 'add-contact', 'backend/contacts/contacts.add', 155, 3, 0, 1, 1, 0, 1, '2018-03-02 11:12:29', 1, '2019-10-01 12:34:08');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (63, 64, NULL, 'edit-contact', 'backend/contacts/contacts.view', 157, 3, 0, 1, 1, 0, 1, '2018-03-02 14:50:19', 1, '2019-10-01 12:34:08');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (64, 58, NULL, 'contacts', 'backend/contacts/contacts.list', 159, 3, 0, 1, 1, 0, 1, '2018-03-05 09:37:29', 1, '2019-10-01 12:34:08');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (65, 39, NULL, 'frontend-menu', 'backend/menus/menus.list', 160, 2, 1, 1, 1, 0, 1, '2018-03-05 11:03:33', 1, '2019-10-01 12:34:08');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (66, 39, NULL, 'backend-menu', 'backend/menus/menus.list', 161, 1, 1, 1, 1, 0, 1, '2018-03-05 11:04:17', 1, '2019-10-01 12:34:09');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (67, 54, NULL, 'banners-list', 'backend/banners/banners.list', 163, 3, 1, 1, 1, 0, 1, '2018-03-05 11:41:08', 1, '2019-10-01 12:34:09');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (68, 67, NULL, 'add-banner', 'backend/banners/banners.add', 164, 3, 0, 1, 1, 0, 1, '2018-03-05 12:30:00', 1, '2019-10-01 12:34:09');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (69, 67, NULL, 'edit-banner', 'backend/banners/banners.view', 165, 3, 0, 1, 1, 0, 1, '2018-03-05 12:30:39', 1, '2019-10-01 12:34:09');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (70, 6, NULL, 'cache', 'backend/cache/cache.list', 171, 1, 1, 1, 1, 1, 1, '2018-05-26 12:04:10', 1, '2019-10-01 12:34:09');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (71, 7, NULL, 'comments', 'backend/comments/comments.list', 174, 3, 1, 1, 1, 8, 1, '2018-05-31 14:54:39', 81, '2019-10-01 12:34:09');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (72, 1, NULL, 'el', 'frontend/pages/home.view', 75, 5, 1, 0, 1, 0, 1, '2018-06-01 14:25:30', 81, '2019-10-01 12:34:09');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (74, 71, NULL, 'edit-comment', 'backend/comments/comments.view', 175, 3, 0, 1, 1, 0, 1, '2018-06-01 07:56:24', 1, '2019-10-01 12:34:10');
INSERT INTO `nodes`(`id`, `parent_id`, `old_tag_id`, `name`, `view`, `module_id`, `access_level_id`, `on_menu`, `in_backend`, `is_active`, `order`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (75, 6, NULL, 'app-logs', 'backend/applogs/app_logs.list', 224, 1, 1, 1, 1, 13, 81, '2019-05-04 18:54:44', 81, '2019-10-01 15:56:05');

-- ----------------------------
-- Records of nodes_closure
-- ----------------------------
BEGIN;
INSERT INTO `nodes_closure` VALUES (1, 1, 0, '/');
INSERT INTO `nodes_closure` VALUES (1, 3, 1, '/en/');
INSERT INTO `nodes_closure` VALUES (3, 3, 0, 'en/');
INSERT INTO `nodes_closure` VALUES (1, 7, 2, '/en/panel/');
INSERT INTO `nodes_closure` VALUES (7, 7, 0, 'panel/');
INSERT INTO `nodes_closure` VALUES (1, 2, 3, '/en/panel/dashboard/');
INSERT INTO `nodes_closure` VALUES (3, 2, 2, 'en/panel/dashboard/');
INSERT INTO `nodes_closure` VALUES (2, 2, 0, 'dashboard/');
INSERT INTO `nodes_closure` VALUES (6, 6, 0, 'system-settings/');
INSERT INTO `nodes_closure` VALUES (4, 4, 0, 'modules/');
INSERT INTO `nodes_closure` VALUES (6, 5, 1, 'system-settings/panel-pages/');
INSERT INTO `nodes_closure` VALUES (5, 5, 0, 'panel-pages/');
INSERT INTO `nodes_closure` VALUES (6, 9, 2, 'system-settings/panel-pages/edit-panel-page/');
INSERT INTO `nodes_closure` VALUES (5, 9, 1, 'panel-pages/edit-panel-page/');
INSERT INTO `nodes_closure` VALUES (9, 9, 0, 'edit-panel-page/');
INSERT INTO `nodes_closure` VALUES (6, 10, 2, 'system-settings/panel-pages/add-panel-page/');
INSERT INTO `nodes_closure` VALUES (5, 10, 1, 'panel-pages/add-panel-page/');
INSERT INTO `nodes_closure` VALUES (10, 10, 0, 'add-panel-page/');
INSERT INTO `nodes_closure` VALUES (11, 11, 0, 'roles/');
INSERT INTO `nodes_closure` VALUES (11, 12, 1, 'roles/add-role/');
INSERT INTO `nodes_closure` VALUES (12, 12, 0, 'add-role/');
INSERT INTO `nodes_closure` VALUES (4, 13, 1, 'modules/add-module/');
INSERT INTO `nodes_closure` VALUES (13, 13, 0, 'add-module/');
INSERT INTO `nodes_closure` VALUES (11, 14, 1, 'roles/edit-role/');
INSERT INTO `nodes_closure` VALUES (14, 14, 0, 'edit-role/');
INSERT INTO `nodes_closure` VALUES (6, 15, 1, 'system-settings/access-levels/');
INSERT INTO `nodes_closure` VALUES (15, 15, 0, 'access-levels/');
INSERT INTO `nodes_closure` VALUES (6, 16, 2, 'system-settings/access-levels/edit-access-level/');
INSERT INTO `nodes_closure` VALUES (15, 16, 1, 'access-levels/edit-access-level/');
INSERT INTO `nodes_closure` VALUES (16, 16, 0, 'edit-access-level/');
INSERT INTO `nodes_closure` VALUES (6, 17, 2, 'system-settings/access-levels/add-access-level/');
INSERT INTO `nodes_closure` VALUES (15, 17, 1, 'access-levels/add-access-level/');
INSERT INTO `nodes_closure` VALUES (17, 17, 0, 'add-access-level/');
INSERT INTO `nodes_closure` VALUES (18, 18, 0, 'general-settings/');
INSERT INTO `nodes_closure` VALUES (6, 18, 1, 'system-settings/general-settings/');
INSERT INTO `nodes_closure` VALUES (1, 19, 3, '/en/panel/users/');
INSERT INTO `nodes_closure` VALUES (7, 19, 1, 'panel/users/');
INSERT INTO `nodes_closure` VALUES (19, 19, 0, 'users/');
INSERT INTO `nodes_closure` VALUES (4, 20, 1, 'modules/edit-module/');
INSERT INTO `nodes_closure` VALUES (20, 20, 0, 'edit-module/');
INSERT INTO `nodes_closure` VALUES (21, 21, 0, 'add-user/');
INSERT INTO `nodes_closure` VALUES (1, 21, 4, '/en/panel/users/add-user/');
INSERT INTO `nodes_closure` VALUES (7, 21, 2, 'panel/users/add-user/');
INSERT INTO `nodes_closure` VALUES (19, 21, 1, 'users/add-user/');
INSERT INTO `nodes_closure` VALUES (1, 22, 4, '/en/panel/users/edit-user/');
INSERT INTO `nodes_closure` VALUES (7, 22, 2, 'panel/users/edit-user/');
INSERT INTO `nodes_closure` VALUES (19, 22, 1, 'users/edit-user/');
INSERT INTO `nodes_closure` VALUES (22, 22, 0, 'edit-user/');
INSERT INTO `nodes_closure` VALUES (6, 11, 1, 'system-settings/roles/');
INSERT INTO `nodes_closure` VALUES (6, 12, 2, 'system-settings/roles/add-role/');
INSERT INTO `nodes_closure` VALUES (6, 14, 2, 'system-settings/roles/edit-role/');
INSERT INTO `nodes_closure` VALUES (23, 23, 0, 'articles/');
INSERT INTO `nodes_closure` VALUES (23, 24, 1, 'articles/add-article/');
INSERT INTO `nodes_closure` VALUES (24, 24, 0, 'add-article/');
INSERT INTO `nodes_closure` VALUES (23, 25, 1, 'articles/edit-article/');
INSERT INTO `nodes_closure` VALUES (25, 25, 0, 'edit-article/');
INSERT INTO `nodes_closure` VALUES (6, 4, 1, 'system-settings/modules/');
INSERT INTO `nodes_closure` VALUES (6, 13, 2, 'system-settings/modules/add-module/');
INSERT INTO `nodes_closure` VALUES (6, 20, 2, 'system-settings/modules/edit-module/');
INSERT INTO `nodes_closure` VALUES (1, 23, 3, '/en/panel/articles/');
INSERT INTO `nodes_closure` VALUES (1, 24, 4, '/en/panel/articles/add-article/');
INSERT INTO `nodes_closure` VALUES (1, 25, 4, '/en/panel/articles/edit-article/');
INSERT INTO `nodes_closure` VALUES (7, 23, 1, 'panel/articles/');
INSERT INTO `nodes_closure` VALUES (7, 24, 2, 'panel/articles/add-article/');
INSERT INTO `nodes_closure` VALUES (7, 25, 2, 'panel/articles/edit-article/');
INSERT INTO `nodes_closure` VALUES (26, 26, 0, 'media-manager/');
INSERT INTO `nodes_closure` VALUES (27, 27, 0, 'categories/');
INSERT INTO `nodes_closure` VALUES (27, 28, 1, 'categories/add-category/');
INSERT INTO `nodes_closure` VALUES (28, 28, 0, 'add-category/');
INSERT INTO `nodes_closure` VALUES (27, 29, 1, 'categories/edit-category/');
INSERT INTO `nodes_closure` VALUES (29, 29, 0, 'edit-category/');
INSERT INTO `nodes_closure` VALUES (1, 27, 3, '/en/panel/categories/');
INSERT INTO `nodes_closure` VALUES (1, 28, 4, '/en/panel/categories/add-category/');
INSERT INTO `nodes_closure` VALUES (1, 29, 4, '/en/panel/categories/edit-category/');
INSERT INTO `nodes_closure` VALUES (7, 27, 1, 'panel/categories/');
INSERT INTO `nodes_closure` VALUES (7, 28, 2, 'panel/categories/add-category/');
INSERT INTO `nodes_closure` VALUES (7, 29, 2, 'panel/categories/edit-category/');
INSERT INTO `nodes_closure` VALUES (1, 26, 3, '/en/panel/media-manager/');
INSERT INTO `nodes_closure` VALUES (7, 26, 1, 'panel/media-manager/');
INSERT INTO `nodes_closure` VALUES (1, 30, 3, '/en/panel/pages/');
INSERT INTO `nodes_closure` VALUES (7, 30, 1, 'panel/pages/');
INSERT INTO `nodes_closure` VALUES (30, 30, 0, 'pages/');
INSERT INTO `nodes_closure` VALUES (1, 34, 4, '/en/panel/pages/add-page/');
INSERT INTO `nodes_closure` VALUES (7, 34, 2, 'panel/pages/add-page/');
INSERT INTO `nodes_closure` VALUES (30, 34, 1, 'pages/add-page/');
INSERT INTO `nodes_closure` VALUES (34, 34, 0, 'add-page/');
INSERT INTO `nodes_closure` VALUES (1, 35, 4, '/en/panel/pages/edit-page/');
INSERT INTO `nodes_closure` VALUES (7, 35, 2, 'panel/pages/edit-page/');
INSERT INTO `nodes_closure` VALUES (30, 35, 1, 'pages/edit-page/');
INSERT INTO `nodes_closure` VALUES (35, 35, 0, 'edit-page/');
INSERT INTO `nodes_closure` VALUES (1, 36, 3, '/en/panel/tags/');
INSERT INTO `nodes_closure` VALUES (7, 36, 1, 'panel/tags/');
INSERT INTO `nodes_closure` VALUES (36, 36, 0, 'tags/');
INSERT INTO `nodes_closure` VALUES (1, 37, 4, '/en/panel/tags/add-tag/');
INSERT INTO `nodes_closure` VALUES (7, 37, 2, 'panel/tags/add-tag/');
INSERT INTO `nodes_closure` VALUES (36, 37, 1, 'tags/add-tag/');
INSERT INTO `nodes_closure` VALUES (37, 37, 0, 'add-tag/');
INSERT INTO `nodes_closure` VALUES (1, 38, 4, '/en/panel/tags/edit-tag/');
INSERT INTO `nodes_closure` VALUES (7, 38, 2, 'panel/tags/edit-tag/');
INSERT INTO `nodes_closure` VALUES (36, 38, 1, 'tags/edit-tag/');
INSERT INTO `nodes_closure` VALUES (38, 38, 0, 'edit-tag/');
INSERT INTO `nodes_closure` VALUES (1, 39, 3, '/en/panel/menus/');
INSERT INTO `nodes_closure` VALUES (7, 39, 1, 'panel/menus/');
INSERT INTO `nodes_closure` VALUES (39, 39, 0, 'menus/');
INSERT INTO `nodes_closure` VALUES (6, 40, 1, 'system-settings/languages/');
INSERT INTO `nodes_closure` VALUES (40, 40, 0, 'languages/');
INSERT INTO `nodes_closure` VALUES (6, 41, 2, 'system-settings/languages/edit-language/');
INSERT INTO `nodes_closure` VALUES (40, 41, 1, 'languages/edit-language/');
INSERT INTO `nodes_closure` VALUES (41, 41, 0, 'edit-language/');
INSERT INTO `nodes_closure` VALUES (6, 42, 2, 'system-settings/languages/add-language/');
INSERT INTO `nodes_closure` VALUES (40, 42, 1, 'languages/add-language/');
INSERT INTO `nodes_closure` VALUES (42, 42, 0, 'add-language/');
INSERT INTO `nodes_closure` VALUES (6, 43, 1, 'system-settings/content-types/');
INSERT INTO `nodes_closure` VALUES (43, 43, 0, 'content-types/');
INSERT INTO `nodes_closure` VALUES (6, 44, 2, 'system-settings/content-types/add-content-type/');
INSERT INTO `nodes_closure` VALUES (43, 44, 1, 'content-types/add-content-type/');
INSERT INTO `nodes_closure` VALUES (44, 44, 0, 'add-content-type/');
INSERT INTO `nodes_closure` VALUES (6, 45, 2, 'system-settings/content-types/edit-content-type/');
INSERT INTO `nodes_closure` VALUES (43, 45, 1, 'content-types/edit-content-type/');
INSERT INTO `nodes_closure` VALUES (45, 45, 0, 'edit-content-type/');
INSERT INTO `nodes_closure` VALUES (46, 46, 0, 'extra-fields/');
INSERT INTO `nodes_closure` VALUES (47, 47, 0, 'extra-fields-groups/');
INSERT INTO `nodes_closure` VALUES (46, 47, 1, 'extra-fields/extra-fields-groups/');
INSERT INTO `nodes_closure` VALUES (47, 48, 1, 'extra-fields-groups/add-extra-fields-group/');
INSERT INTO `nodes_closure` VALUES (46, 48, 2, 'extra-fields/extra-fields-groups/add-extra-fields-group/');
INSERT INTO `nodes_closure` VALUES (48, 48, 0, 'add-extra-fields-group/');
INSERT INTO `nodes_closure` VALUES (47, 49, 1, 'extra-fields-groups/edit-extra-fields-group/');
INSERT INTO `nodes_closure` VALUES (46, 49, 2, 'extra-fields/extra-fields-groups/edit-extra-fields-group/');
INSERT INTO `nodes_closure` VALUES (49, 49, 0, 'edit-extra-fields-group/');
INSERT INTO `nodes_closure` VALUES (46, 50, 1, 'extra-fields/extra-fields-list/');
INSERT INTO `nodes_closure` VALUES (50, 50, 0, 'extra-fields-list/');
INSERT INTO `nodes_closure` VALUES (51, 51, 0, 'add-extra-field/');
INSERT INTO `nodes_closure` VALUES (52, 52, 0, 'edit-extra-field/');
INSERT INTO `nodes_closure` VALUES (46, 51, 2, 'extra-fields/extra-fields-list/add-extra-field/');
INSERT INTO `nodes_closure` VALUES (50, 51, 1, 'extra-fields-list/add-extra-field/');
INSERT INTO `nodes_closure` VALUES (46, 52, 2, 'extra-fields/extra-fields-list/edit-extra-field/');
INSERT INTO `nodes_closure` VALUES (50, 52, 1, 'extra-fields-list/edit-extra-field/');
INSERT INTO `nodes_closure` VALUES (1, 53, 3, '/en/panel/my-profile/');
INSERT INTO `nodes_closure` VALUES (7, 53, 1, 'panel/my-profile/');
INSERT INTO `nodes_closure` VALUES (53, 53, 0, 'my-profile/');
INSERT INTO `nodes_closure` VALUES (1, 54, 3, '/en/panel/banners/');
INSERT INTO `nodes_closure` VALUES (7, 54, 1, 'panel/banners/');
INSERT INTO `nodes_closure` VALUES (54, 54, 0, 'banners/');
INSERT INTO `nodes_closure` VALUES (1, 55, 4, '/en/panel/banners/positions/');
INSERT INTO `nodes_closure` VALUES (7, 55, 2, 'panel/banners/positions/');
INSERT INTO `nodes_closure` VALUES (54, 55, 1, 'banners/positions/');
INSERT INTO `nodes_closure` VALUES (55, 55, 0, 'positions/');
INSERT INTO `nodes_closure` VALUES (1, 56, 5, '/en/panel/banners/positions/add-banners-position/');
INSERT INTO `nodes_closure` VALUES (7, 56, 3, 'panel/banners/positions/add-banners-position/');
INSERT INTO `nodes_closure` VALUES (54, 56, 2, 'banners/positions/add-banners-position/');
INSERT INTO `nodes_closure` VALUES (55, 56, 1, 'positions/add-banners-position/');
INSERT INTO `nodes_closure` VALUES (56, 56, 0, 'add-banners-position/');
INSERT INTO `nodes_closure` VALUES (57, 57, 0, 'edit-banners-position/');
INSERT INTO `nodes_closure` VALUES (1, 57, 5, '/en/panel/banners/positions/edit-banners-position/');
INSERT INTO `nodes_closure` VALUES (7, 57, 3, 'panel/banners/positions/edit-banners-position/');
INSERT INTO `nodes_closure` VALUES (54, 57, 2, 'banners/positions/edit-banners-position/');
INSERT INTO `nodes_closure` VALUES (55, 57, 1, 'positions/edit-banners-position/');
INSERT INTO `nodes_closure` VALUES (58, 58, 0, 'clients/');
INSERT INTO `nodes_closure` VALUES (58, 59, 1, 'clients/add-client/');
INSERT INTO `nodes_closure` VALUES (59, 59, 0, 'add-client/');
INSERT INTO `nodes_closure` VALUES (58, 60, 1, 'clients/edit-client/');
INSERT INTO `nodes_closure` VALUES (60, 60, 0, 'edit-client/');
INSERT INTO `nodes_closure` VALUES (61, 61, 0, 'client-contacts/');
INSERT INTO `nodes_closure` VALUES (62, 62, 0, 'add-contact/');
INSERT INTO `nodes_closure` VALUES (63, 63, 0, 'edit-contact/');
INSERT INTO `nodes_closure` VALUES (64, 64, 0, 'contacts/');
INSERT INTO `nodes_closure` VALUES (64, 61, 1, 'contacts/client-contacts/');
INSERT INTO `nodes_closure` VALUES (64, 62, 1, 'contacts/add-contact/');
INSERT INTO `nodes_closure` VALUES (64, 63, 1, 'contacts/edit-contact/');
INSERT INTO `nodes_closure` VALUES (58, 64, 1, 'clients/contacts/');
INSERT INTO `nodes_closure` VALUES (58, 61, 2, 'clients/contacts/client-contacts/');
INSERT INTO `nodes_closure` VALUES (58, 62, 2, 'clients/contacts/add-contact/');
INSERT INTO `nodes_closure` VALUES (58, 63, 2, 'clients/contacts/edit-contact/');
INSERT INTO `nodes_closure` VALUES (1, 65, 4, '/en/panel/menus/frontend-menu/');
INSERT INTO `nodes_closure` VALUES (7, 65, 2, 'panel/menus/frontend-menu/');
INSERT INTO `nodes_closure` VALUES (39, 65, 1, 'menus/frontend-menu/');
INSERT INTO `nodes_closure` VALUES (65, 65, 0, 'frontend-menu/');
INSERT INTO `nodes_closure` VALUES (1, 66, 4, '/en/panel/menus/backend-menu/');
INSERT INTO `nodes_closure` VALUES (7, 66, 2, 'panel/menus/backend-menu/');
INSERT INTO `nodes_closure` VALUES (39, 66, 1, 'menus/backend-menu/');
INSERT INTO `nodes_closure` VALUES (66, 66, 0, 'backend-menu/');
INSERT INTO `nodes_closure` VALUES (1, 67, 4, '/en/panel/banners/banners-list/');
INSERT INTO `nodes_closure` VALUES (7, 67, 2, 'panel/banners/banners-list/');
INSERT INTO `nodes_closure` VALUES (54, 67, 1, 'banners/banners-list/');
INSERT INTO `nodes_closure` VALUES (67, 67, 0, 'banners-list/');
INSERT INTO `nodes_closure` VALUES (1, 68, 5, '/en/panel/banners/banners-list/add-banner/');
INSERT INTO `nodes_closure` VALUES (7, 68, 3, 'panel/banners/banners-list/add-banner/');
INSERT INTO `nodes_closure` VALUES (54, 68, 2, 'banners/banners-list/add-banner/');
INSERT INTO `nodes_closure` VALUES (67, 68, 1, 'banners-list/add-banner/');
INSERT INTO `nodes_closure` VALUES (68, 68, 0, 'add-banner/');
INSERT INTO `nodes_closure` VALUES (1, 69, 5, '/en/panel/banners/banners-list/edit-banner/');
INSERT INTO `nodes_closure` VALUES (7, 69, 3, 'panel/banners/banners-list/edit-banner/');
INSERT INTO `nodes_closure` VALUES (54, 69, 2, 'banners/banners-list/edit-banner/');
INSERT INTO `nodes_closure` VALUES (67, 69, 1, 'banners-list/edit-banner/');
INSERT INTO `nodes_closure` VALUES (69, 69, 0, 'edit-banner/');
INSERT INTO `nodes_closure` VALUES (6, 70, 1, 'system-settings/cache/');
INSERT INTO `nodes_closure` VALUES (70, 70, 0, 'cache/');
INSERT INTO `nodes_closure` VALUES (71, 71, 0, 'comments/');
INSERT INTO `nodes_closure` VALUES (1, 72, 1, '/el/');
INSERT INTO `nodes_closure` VALUES (72, 72, 0, 'el/');
INSERT INTO `nodes_closure` VALUES (71, 74, 1, 'comments/edit-comment/');
INSERT INTO `nodes_closure` VALUES (74, 74, 0, 'edit-comment/');
INSERT INTO `nodes_closure` VALUES (75, 75, 0, 'app-logs/');
INSERT INTO `nodes_closure` VALUES (6, 75, 1, 'system-settings/app-logs/');
INSERT INTO `nodes_closure` VALUES (1, 75, 4, '/en/panel/system-settings/app-logs/');
INSERT INTO `nodes_closure` VALUES (7, 75, 2, 'panel/system-settings/app-logs/');
INSERT INTO `nodes_closure` VALUES (3, 75, 3, 'en/panel/system-settings/app-logs/');

-- Records nodes_content --

INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (1, 'System', '', '', '', 'DEFAULT_IMAGE', 0, NULL, NULL, 3, 0, 0, 0, NULL, 0, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (2, 'Dashboard', ' icon-speedometer', '', '', 'DEFAULT_IMAGE', 0, '', 1, 3, 0, 0, 0, NULL, 0, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (3, 'Home', '', '', '', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 1, NULL, 0, NULL, '', '', 'Home', '', 'NOINDEX, NOFOLLOW', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (4, 'Modules', 'icon-puzzle', '', '', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (5, 'Panel Pages', 'icon-map', '', '', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (6, 'System Settings', 'icon-settings', '', '', 'DEFAULT_IMAGE', 0, '', 1, 3, 0, 0, 0, NULL, 0, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (7, 'Panel', 'icon-home', '', '', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', 'NOINDEX, NOFOLLOW', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (9, 'Edit Panel Page', 'icon-pencil', '', '', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (10, 'Add Panel Page', 'icon-pencil', '', '', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (11, 'Roles', 'icon-key', '', 'Roles Panel page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (12, 'Add Role', 'icon-pencil', '', 'Add Role page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (13, 'Add Module', 'icon-pencil', '', 'Add Module Page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (14, 'Edit Role', 'icon-pencil', '', 'Edit Role Page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (15, 'Access Levels', 'icon-directions', '', 'Access Levels page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (16, 'Edit Access Level', 'icon-pencil', '', 'Edit Access Level Page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (17, 'Add Access Level', 'icon-pencil', '', 'Add Access Level Page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (18, 'General Settings', 'icon-wrench', '', 'General Settings Page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (19, 'Users', 'icon-users', '', 'Users list Page', 'DEFAULT_IMAGE', 0, '', 1, 3, 0, 0, 0, NULL, 0, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (20, 'Edit Module', 'icon-pencil', '', 'Edit Module page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (21, 'Add User', 'icon-pencil', '', 'Add User Page', 'DEFAULT_IMAGE', 0, '', 1, 3, 0, 0, 0, NULL, 0, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (22, 'Edit User', 'icon-pencil', '', 'Edit User Page', 'DEFAULT_IMAGE', 0, '', 1, 3, 0, 0, 0, NULL, 0, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (23, 'Articles', 'icon-notebook', '', 'Articles List Page', 'DEFAULT_IMAGE', 0, '', 1, 3, 0, 0, 0, NULL, 0, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (24, 'Add Article', 'icon-pencil', '', 'Add Article Page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (25, 'Edit Article', 'icon-pencil', '', 'Edit Article page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (26, 'Media Manager', 'icon-picture', '', 'Media Manager Page', 'DEFAULT_IMAGE', 0, '', 1, 3, 0, 0, 0, NULL, 0, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (27, 'Categories', 'icon-drawer', '', 'Categories Page', 'DEFAULT_IMAGE', 0, '', 1, 3, 0, 0, 0, NULL, 0, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (28, 'Add Category', 'icon-pencil', '', 'Add Category Page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (29, 'Edit category', 'icon-pencil', '', 'Edit category Page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (30, 'Pages', 'icon-book-open', '', 'Pages modules page', 'DEFAULT_IMAGE', 0, '', 1, 3, 0, 0, 0, NULL, 0, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (34, 'Add Page', 'icon-pencil', '', 'Add Frontend page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (35, 'Edit Page', 'icon-pencil', '', 'Edit Frontend page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (36, 'Tags', 'icon-tag', '', 'Tags Module', 'DEFAULT_IMAGE', 0, '', 1, 3, 0, 0, 0, NULL, 0, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (37, 'Add Tag', 'icon-pencil', '', 'Add tag page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (38, 'Edit Tag', 'icon-pencil', '', 'Edit Tag Page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (39, 'Menus', 'icon-list', '', 'Menus Page', 'DEFAULT_IMAGE', 0, '', 1, 3, 0, 0, 0, NULL, 0, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (40, 'Languages', 'icon-globe-alt', '', 'Languages Page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (41, 'Edit Language', 'icon-pencil', '', 'Edit Language Page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (42, 'Add Language', 'icon-pencil', '', 'Edit Language page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (43, 'Content Types', ' icon-note', '', 'Content Types page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (44, 'Add Content Type', 'icon-pencil', '', 'Add Content Type page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (45, 'Edit Content Type', 'icon-pencil', '', 'Add Content Type page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (46, 'Extra Fields', 'glyphicon glyphicon-th', '', '', 'DEFAULT_IMAGE', 0, '', 1, 3, 0, 0, 0, NULL, 0, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (47, 'Extra Fields Groups', 'glyphicon glyphicon-list-alt', '', 'Extra Fields Groups page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (48, 'Add Extra Fields Group', 'icon-pencil', '', 'Add Extra Fields Group page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (49, 'Edit Extra Fields Group', 'icon-pencil', '', 'Edit Extra Fields Group page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (50, 'Extra Fields List', 'glyphicon glyphicon-list', '', 'Extra Fields list page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (51, 'Add Extra Field', 'icon-pencil', '', 'Add Extra Field page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (52, 'Edit Extra Field', 'icon-pencil', '', 'Edit Extra Field page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (53, 'My Profile', 'icon-user', '', 'Profile page', 'DEFAULT_IMAGE', 0, '', 1, 3, 0, 0, 0, NULL, 0, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (54, 'Banners', 'glyphicon glyphicon-bookmark', '', 'Banners List', 'DEFAULT_IMAGE', 0, '', 1, 3, 0, 0, 0, NULL, 0, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (55, 'Positions', 'icon-list', '', 'Banners Positions List', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (56, 'Add Banners Position', 'icon-pencil', '', 'Add Banners Position page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (57, 'Edit Banners Position', 'icon-pencil', '', 'Edit Banners Position page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (58, 'Clients', 'fa fa-briefcase', '', 'Clients page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (59, 'Add Client', 'icon-pencil', '', 'Add Client page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (60, 'Edit Client', 'icon-pencil', '', 'Edit Client page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (61, 'Client Contacts', 'icon-pencil', '', 'Client Contacts List page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (62, 'Add Contact', 'icon-pencil', '', 'Add Contact page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (63, 'Edit Contact', 'icon-pencil', '', 'Edit Contact page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (64, 'Contacts', 'icon-pencil', '', 'Contacts List page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (65, 'Frontend Menu', 'icon-list', '', 'Frontend Menu page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (66, 'Backend Menu', 'icon-list', '', 'Backend Menu page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (67, 'Banners List', 'icon-list', '', 'Banners List page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (68, 'Add Banner', 'icon-pencil', '', 'Add Banner page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (69, 'Edit Banner', 'icon-pencil', '', 'Edit Banner page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (70, 'Cache', 'glyphicon glyphicon-refresh', '', 'Cache panel page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, 0, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (71, 'Comments', 'fa fa-comments-o', '', 'Comments panel page', 'DEFAULT_IMAGE', 0, '', 1, 3, 0, 0, 0, NULL, 0, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (72, 'Home', '', '', '', 'DEFAULT_IMAGE', 0, NULL, 2, 3, 0, 0, 1, NULL, 0, NULL, '', '', '', 'INDEX, FOLLOW', '2018-06-01 14:25:01', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (74, 'Edit Comment', 'icon-pencil', '', 'Edit Comment panel page', 'DEFAULT_IMAGE', 0, NULL, 1, 3, 0, 0, 0, NULL, 0, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `nodes_content`(`node_id`, `title`, `icon`, `intro_text`, `description`, `image`, `gallery`, `video`, `language_id`, `content_type_id`, `is_featured`, `is_sticky`, `homepage`, `sponsored_by_id`, `comments_lock`, `extra_fields_group_id`, `extra_fields_values`, `social_media_title`, `meta_title`, `meta_description`, `robots`, `published_on`, `finished_on`) VALUES (75, 'App Logs', 'icon-layers', '', 'App Logs Page', '', 0, '', 1, 3, 0, 0, 0, NULL, 0, NULL, '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Triggers structure for table nodes
-- ----------------------------
DROP TRIGGER IF EXISTS `update_node`;
delimiter ;;
CREATE TRIGGER `update_node` AFTER UPDATE ON `nodes` FOR EACH ROW BEGIN
DECLARE old_path_len INT;
SET @type = (SELECT content_type_id FROM nodes_content WHERE node_id = OLD.id);

IF NEW.parent_id != OLD.parent_id OR NEW.parent_id IS NULL OR OLD.parent_id IS NULL THEN
if OLD.parent_id IS NOT NULL THEN
DELETE t2
FROM nodes_closure t1
JOIN nodes_closure t2 ON t1.descendant = t2.descendant
WHERE t1.ancestor = OLD.id
AND t2.depth > t1.depth;
END IF;

IF NEW.parent_id IS NOT NULL THEN
INSERT INTO nodes_closure (ancestor, descendant, depth, path)
SELECT t1.ancestor, t2.descendant, t1.depth + t2.depth + 1, CONCAT(t1.path, t2.path)
FROM nodes_closure t1, nodes_closure t2
WHERE t1.descendant = NEW.parent_id
AND t2.ancestor = OLD.id;
END IF;
END IF;

IF NEW.name != OLD.name THEN
SELECT CHAR_LENGTH(path) INTO old_path_len
FROM nodes_closure
WHERE descendant = OLD.id
AND DEPTH = 0;

IF old_path_len > 0 THEN

IF @type = 1 THEN
UPDATE nodes_closure t1
JOIN nodes_closure t2 ON t1.descendant = t2.descendant SET t2.path = CONCAT( SUBSTRING(t2.path, 1, CHAR_LENGTH(t2.path) - CHAR_LENGTH(t1.path)), OLD.id, "-", CONCAT(NEW.name, SUBSTRING(t1.path, old_path_len))
)
WHERE t1.ancestor = OLD.id
AND t2.depth >= t1.depth;
END IF;

IF @type = 4 THEN
UPDATE nodes_closure t1
JOIN nodes_closure t2 ON t1.descendant = t2.descendant SET t2.path = CONCAT( SUBSTRING(t2.path, 1, CHAR_LENGTH(t2.path) - CHAR_LENGTH(t1.path)), 'tag', "/", CONCAT(NEW.name, SUBSTRING(t1.path, old_path_len))
)
WHERE t1.ancestor = OLD.id
AND t2.depth >= t1.depth;
END IF;

IF @type <> 1 AND @type <> 4 THEN
UPDATE nodes_closure t1
JOIN nodes_closure t2 ON t1.descendant = t2.descendant SET t2.path = CONCAT( SUBSTRING(t2.path, 1, CHAR_LENGTH(t2.path) - CHAR_LENGTH(t1.path)), CONCAT(NEW.name, SUBSTRING(t1.path, old_path_len))
)
WHERE t1.ancestor = OLD.id
AND t2.depth >= t1.depth;
END IF;


END IF;
END IF;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table nodes_content
-- ----------------------------
DROP TRIGGER IF EXISTS `insert_node`;
delimiter ;;
CREATE TRIGGER `insert_node` AFTER INSERT ON `nodes_content` FOR EACH ROW BEGIN

SET @alias = (SELECT `name` FROM nodes WHERE id = NEW.node_id);
SET @parent = (SELECT `parent_id` FROM nodes WHERE id = NEW.node_id);

IF NEW.content_type_id <> 1 AND NEW.content_type_id <> 4 THEN
INSERT INTO nodes_closure (ancestor, descendant, depth, path) VALUES (NEW.node_id, NEW.node_id, 0, CONCAT(@alias,"/"));

INSERT INTO nodes_closure (ancestor, descendant, depth, path) SELECT ancestor, NEW.node_id, depth + 1, CONCAT(path, @alias, "/") FROM nodes_closure WHERE descendant = @parent; 
END IF;

IF NEW.content_type_id = 1 THEN
INSERT INTO nodes_closure (ancestor, descendant, depth, path) VALUES (NEW.node_id, NEW.node_id, 0, CONCAT(NEW.node_id, '-', @alias,"/"));

INSERT INTO nodes_closure (ancestor, descendant, depth, path) SELECT ancestor, NEW.node_id, depth + 1, CONCAT(path, NEW.node_id, "-", @alias, "/") FROM nodes_closure WHERE descendant = @parent; 
END IF;

IF NEW.content_type_id = 4 THEN
INSERT INTO nodes_closure (ancestor, descendant, depth, path) VALUES (NEW.node_id, NEW.node_id, 0, CONCAT('tag', '/', @alias,"/"));

INSERT INTO nodes_closure (ancestor, descendant, depth, path) SELECT ancestor, NEW.node_id, depth + 1, CONCAT(path, 'tag', "/", @alias, "/") FROM nodes_closure WHERE descendant = @parent; 
END IF;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table access_levels
-- ----------------------------
DROP TRIGGER IF EXISTS `insert_access_level`;
delimiter ;;
CREATE TRIGGER `insert_access_level` AFTER INSERT ON `access_levels` FOR EACH ROW BEGIN
INSERT INTO access_levels_tree (parent_id, child_id, depth, path)
VALUES (NEW.id, NEW.id, 0, CONCAT(NEW.id,"/"));

INSERT INTO access_levels_tree (parent_id, child_id, depth, path)
SELECT parent_id, NEW.id, depth + 1, CONCAT(path, NEW.id, "/")
FROM access_levels_tree
WHERE child_id = NEW.parent_id;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table access_levels
-- ----------------------------
DROP TRIGGER IF EXISTS `update_access_level`;
delimiter ;;
CREATE TRIGGER `update_access_level` AFTER UPDATE ON `access_levels` FOR EACH ROW BEGIN
DECLARE old_path_len INT;
IF NEW.parent_id != OLD.parent_id OR NEW.parent_id IS NULL OR OLD.parent_id IS NULL THEN
if OLD.parent_id IS NOT NULL THEN
DELETE t2
FROM access_levels_tree t1
JOIN access_levels_tree t2 ON t1.child_id = t2.child_id
WHERE t1.parent_id = OLD.id
AND t2.depth > t1.depth;
END IF;

IF NEW.parent_id IS NOT NULL THEN
INSERT INTO access_levels_tree (parent_id, child_id, depth, path)
SELECT t1.parent_id, t2.child_id, t1.depth + t2.depth + 1, CONCAT(t1.path, t2.path)
FROM access_levels_tree t1, access_levels_tree t2
WHERE t1.child_id = NEW.parent_id
AND t2.parent_id = OLD.id;
END IF;
END IF;

IF NEW.title != OLD.title THEN
SELECT CHAR_LENGTH(path) INTO old_path_len
FROM access_levels_tree
WHERE child_id = OLD.id
AND DEPTH = 0;

IF old_path_len > 0 THEN
UPDATE access_levels_tree t1
JOIN access_levels_tree t2 ON t1.child_id = t2.child_id
SET t2.path = CONCAT(
SUBSTRING(t2.path, 1, CHAR_LENGTH(t2.path) - CHAR_LENGTH(t1.path)),
CONCAT(NEW.id, SUBSTRING(t1.path, old_path_len))
)
WHERE t1.parent_id = OLD.id
AND t2.depth >= t1.depth;
END IF;
END IF;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table modules
-- ----------------------------
DROP TRIGGER IF EXISTS `insert_module`;
delimiter ;;
CREATE TRIGGER `insert_module` AFTER INSERT ON `modules` FOR EACH ROW BEGIN
INSERT INTO modules_tree (parent_id, child_id, depth, path)
VALUES (NEW.id, NEW.id, 0, CONCAT(NEW.title,"/"));

INSERT INTO modules_tree (parent_id, child_id, depth, path)
SELECT parent_id, NEW.id, depth + 1, CONCAT(path, NEW.title, "/")
FROM modules_tree
WHERE child_id = NEW.parent_id;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table modules
-- ----------------------------
DROP TRIGGER IF EXISTS `update_module`;
delimiter ;;
CREATE TRIGGER `update_module` AFTER UPDATE ON `modules` FOR EACH ROW BEGIN
DECLARE old_path_len INT;
IF NEW.parent_id != OLD.parent_id OR NEW.parent_id IS NULL OR OLD.parent_id IS NULL THEN
if OLD.parent_id IS NOT NULL THEN
DELETE t2
FROM modules_tree t1
JOIN modules_tree t2 ON t1.child_id = t2.child_id
WHERE t1.parent_id = OLD.id
AND t2.depth > t1.depth;
END IF;

IF NEW.parent_id IS NOT NULL THEN
INSERT INTO modules_tree (parent_id, child_id, depth, path)
SELECT t1.parent_id, t2.child_id, t1.depth + t2.depth + 1, CONCAT(t1.path, t2.path)
FROM modules_tree t1, modules_tree t2
WHERE t1.child_id = NEW.parent_id
AND t2.parent_id = OLD.id;
END IF;
END IF;

IF NEW.title != OLD.title THEN
SELECT CHAR_LENGTH(path) INTO old_path_len
FROM modules_tree
WHERE child_id = OLD.id
AND DEPTH = 0;

IF old_path_len > 0 THEN
UPDATE modules_tree t1
JOIN modules_tree t2 ON t1.child_id = t2.child_id
SET t2.path = CONCAT(
SUBSTRING(t2.path, 1, CHAR_LENGTH(t2.path) - CHAR_LENGTH(t1.path)),
CONCAT(NEW.title, SUBSTRING(t1.path, old_path_len))
)
WHERE t1.parent_id = OLD.id
AND t2.depth >= t1.depth;
END IF;
END IF;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table roles
-- ----------------------------
DROP TRIGGER IF EXISTS `insert_role`;
delimiter ;;
CREATE TRIGGER `insert_role` AFTER INSERT ON `roles` FOR EACH ROW BEGIN
INSERT INTO roles_tree (parent_id, child_id, depth, path)
VALUES (NEW.id, NEW.id, 0, CONCAT(NEW.id,"/"));

INSERT INTO roles_tree (parent_id, child_id, depth, path)
SELECT parent_id, NEW.id, depth + 1, CONCAT(path, NEW.id, "/")
FROM roles_tree
WHERE child_id = NEW.parent_id;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table roles
-- ----------------------------
DROP TRIGGER IF EXISTS `update_role`;
delimiter ;;
CREATE TRIGGER `update_role` AFTER UPDATE ON `roles` FOR EACH ROW BEGIN
DECLARE old_path_len INT;
IF NEW.parent_id != OLD.parent_id OR NEW.parent_id IS NULL OR OLD.parent_id IS NULL THEN
if OLD.parent_id IS NOT NULL THEN
DELETE t2
FROM roles_tree t1
JOIN roles_tree t2 ON t1.child_id = t2.child_id
WHERE t1.parent_id = OLD.id
AND t2.depth > t1.depth;
END IF;

IF NEW.parent_id IS NOT NULL THEN
INSERT INTO roles_tree (parent_id, child_id, depth, path)
SELECT t1.parent_id, t2.child_id, t1.depth + t2.depth + 1, CONCAT(t1.path, t2.path)
FROM roles_tree t1, roles_tree t2
WHERE t1.child_id = NEW.parent_id
AND t2.parent_id = OLD.id;
END IF;
END IF;

IF NEW.title != OLD.title THEN
SELECT CHAR_LENGTH(path) INTO old_path_len
FROM roles_tree
WHERE child_id = OLD.id
AND DEPTH = 0;

IF old_path_len > 0 THEN
UPDATE roles_tree t1
JOIN roles_tree t2 ON t1.child_id = t2.child_id
SET t2.path = CONCAT(
SUBSTRING(t2.path, 1, CHAR_LENGTH(t2.path) - CHAR_LENGTH(t1.path)),
CONCAT(NEW.id, SUBSTRING(t1.path, old_path_len))
)
WHERE t1.parent_id = OLD.id
AND t2.depth >= t1.depth;
END IF;
END IF;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
