# README #

### DISCLAIMER ###
This is sample code and may not be fully functional.

### Requirements ###

PHP 7

### What is this repository for? ###

Custom cms created in slim micro framework 3. 

### How do I get set up? ###

1) Run composer update in order to download dependencies.
2) Rename .env.example to .env and fill in DATABASE and APP section to get started.
3) Rename config.json.example to config.json. This file can be edited later through the app interface.
3) Run cms.sql to setup the database.
4) Access backend through yourdomain.com/panel using as username and password: demouser.
